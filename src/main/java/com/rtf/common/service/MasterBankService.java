/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.service;

import java.util.List;

import com.rtf.common.dto.MasterBankDTO;
import com.rtf.common.sql.dao.QuestionBankMasterDAO;
import com.rtf.common.sql.dvo.QuestionBankMasterDVO;
import com.rtf.common.util.GridHeaderFilterUtil;
import com.rtf.saas.util.PaginationUtil;

/**
 * The Class MasterBankService.
 */
public class MasterBankService {
	/**
	 * Fetch master question bank list with filter.
	 *
	 * @param dto        the dto
	 * @param pgNo       the pg no
	 * @param filter     the filter
	 * @param activeFlag the active flag
	 * @return the master bank DTO
	 */
	public static MasterBankDTO fetchMasterBankListWithFilter(MasterBankDTO dto, String pgNo, String filter,
			String activeFlag) {
		List<QuestionBankMasterDVO> qbList = null;
		try {
			QuestionBankMasterDVO dvo = dto.getDvo();
			String clientId = dvo.getClintid();
			String filterQuery = GridHeaderFilterUtil.getAllMasterQbankFilterQuery(filter);

			qbList = QuestionBankMasterDAO.getAllMasterBankWithFilter(clientId, pgNo, filterQuery, activeFlag);
			Integer count = QuestionBankMasterDAO.getAllMasterBankCountWithFilter(clientId, filterQuery, activeFlag);
			dto.setPagination(PaginationUtil.calculatePaginationParameter(count, pgNo));
			dto.setMqblist(qbList);
			dto.setSts(1);

		} catch (Exception ex) {
			dto.setSts(0);
			ex.printStackTrace();
		}
		return dto;
	}

	/**
	 * Fetch all master question bank list.
	 *
	 * @param dto        the dto
	 * @param activeFlag the active flag
	 * @return the master bank DTO
	 */
	public static MasterBankDTO fetchAllMasterBankList(MasterBankDTO dto, String activeFlag) {
		List<QuestionBankMasterDVO> qbList = null;
		try {
			QuestionBankMasterDVO dvo = dto.getDvo();
			String clientId = dvo.getClintid();
			qbList = QuestionBankMasterDAO.getAllMasterBankList(clientId, activeFlag);
			dto.setMqblist(qbList);
			dto.setSts(1);

		} catch (Exception ex) {
			dto.setSts(0);
			ex.printStackTrace();
		}
		return dto;
	}

	/**
	 * Creates the new master Question bank.
	 *
	 * @param dvo     the dvo
	 * @param respDTO the resp DTO
	 * @return the master bank DTO
	 * @throws Exception the exception
	 */
	public static MasterBankDTO createMasterQBank(QuestionBankMasterDVO dvo, MasterBankDTO respDTO) throws Exception {
		Integer sts = 0;
		try {
			sts = QuestionBankMasterDAO.createQuestionBank(dvo.getClintid(), dvo);
		} catch (Exception e) {
			e.printStackTrace();
			sts = 0;
		}
		respDTO.setSts(sts);
		return respDTO;
	}

	/**
	 * Delete master question bank.
	 *
	 * @param dvo     the dvo
	 * @param respDTO the resp DTO
	 * @return the master bank DTO
	 * @throws Exception the exception
	 */
	public static MasterBankDTO deleteBank(QuestionBankMasterDVO dvo, MasterBankDTO respDTO) throws Exception {
		Integer sts = 0;
		try {
			sts = QuestionBankMasterDAO.deleteMasterBankType(dvo);
		} catch (Exception e) {
			e.printStackTrace();
			sts = 0;
		}
		respDTO.setSts(sts);
		return respDTO;
	}

	/**
	 * Update master question bank.
	 *
	 * @param dvo     the dvo
	 * @param respDTO the resp DTO
	 * @return the master bank DTO
	 * @throws Exception the exception
	 */
	public static MasterBankDTO updateMasterBank(QuestionBankMasterDVO dvo, MasterBankDTO respDTO) throws Exception {
		Integer sts = 0;
		try {

			if (QuestionBankMasterDAO.findDuplicateRecordByName(dvo)) {
				respDTO.setSts(2);
				return respDTO;
			}
			sts = QuestionBankMasterDAO.updateMasterBankType(dvo);
		} catch (Exception e) {
			e.printStackTrace();
			sts = 0;
		}
		respDTO.setSts(sts);
		return respDTO;
	}

}
