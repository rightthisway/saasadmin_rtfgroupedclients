/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rtf.common.util.GsonCustomConfig;
import com.rtf.livt.dto.LivtCommonDTO;
import com.rtf.livt.dvo.ClusterNodeConfigDVO;
import com.rtf.livt.service.ClusterNodeConfigService;
import com.rtf.livt.util.HTTPUtil;

/**
 * The Class NodeCacheService.
 */
public class NodeCacheService {	
	
	/** The update firestore cache api. */
	public static String UPDATE_FIRESTORE_CACHE_API = "RefreshApplicationCache.json";
	
	/**
	 * Update the firestore all configured node cache data.
	 *
	 * @param dto the dto
	 * @return the livt common DTO
	 * @throws Exception the exception
	 */
	public static LivtCommonDTO updateNodeCacheData(LivtCommonDTO dto) throws Exception {
		
		String data = null;		
		try {
			dto.setSts(0);
			Map<String, String> dataMap = new HashMap<String, String>();
			dataMap.put("clId", dto.getClId());			
			dataMap.put("platForm","WEB"); 
			dataMap.put("type","CLIENTFIRESTORECONFIG");			
			List<ClusterNodeConfigDVO> configs = ClusterNodeConfigService.getAllActiveClusterNodeConfig(dto.getClId());
			Integer nodeCnt = 1;
			LocalDateTime starttime = LocalDateTime.now();  
			for(ClusterNodeConfigDVO conf : configs){				
				data = HTTPUtil.execute(dataMap, conf.getUrl()+UPDATE_FIRESTORE_CACHE_API);
				Gson gson = GsonCustomConfig.getGsonBuilder();		
				JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
				dto = gson.fromJson(((JsonObject)jsonObject.get("livtresp")), LivtCommonDTO.class);				
				if(dto.getSts() == 0){					
					dto.setMsg("CACHE CONF FAILED ON URL : "+conf.getUrl()+UPDATE_FIRESTORE_CACHE_API+" \n"+data);	
					return dto;
				}
				nodeCnt++;
			}
			LocalDateTime endTime = LocalDateTime.now();
			dto.setMsg("updated cache for [ " + nodeCnt + " ] NODES : [START TIME-"+ starttime + " ] && [END TIME- " + endTime + " ] "  );
			dto.setSts(1);
		} catch (Exception e) {
			e.printStackTrace();
			dto.setMsg(e.getMessage());
			return dto;
		}		
		return dto;
	}

}
