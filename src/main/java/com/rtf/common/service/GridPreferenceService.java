/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.service;

import java.util.List;

import com.rtf.common.sql.dao.GridPreferenceDAO;
import com.rtf.common.sql.dvo.GridPreferenceDVO;

/**
 * The Class GridPreferenceService.
 */
public class GridPreferenceService {

	/**
	 * Gets the user all grid preferences.
	 *
	 * @param clientId the client id
	 * @param userId   the user id
	 * @return the user all grid preferences
	 */
	public static List<GridPreferenceDVO> getUserAllGridPreferences(String clientId, String userId) {
		List<GridPreferenceDVO> list = null;
		try {
			list = GridPreferenceDAO.getUserAllGridPreferences(clientId, userId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	/**
	 * Gets the user grid preference.
	 *
	 * @param clientId the client id
	 * @param userId   the user id
	 * @param gridName the grid name
	 * @return the user grid preference
	 */
	public static GridPreferenceDVO getUserGridPreference(String clientId, String userId, String gridName) {
		GridPreferenceDVO dvo = null;
		try {
			dvo = GridPreferenceDAO.getUserGridPreference(clientId, userId, gridName);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dvo;
	}

	/**
	 * Save user grid preference.
	 *
	 * @param GridPreferenceDVO the pref
	 * @return the boolean
	 */
	public static Boolean savePreference(GridPreferenceDVO pref) {
		Boolean isInsert = false;
		try {
			isInsert = GridPreferenceDAO.savePreference(pref);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isInsert;
	}

	/**
	 * Update user grid preference.
	 *
	 * @param pref the pref
	 * @return the boolean
	 */
	public static Boolean updateGridPreference(GridPreferenceDVO pref) {
		Boolean isUpd = false;
		try {
			isUpd = GridPreferenceDAO.updateGridPreference(pref);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isUpd;
	}
}
