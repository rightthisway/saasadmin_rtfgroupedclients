/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.service;

import java.util.List;

import com.rtf.common.cass.dao.CommonQuestionBankCassDAO;
import com.rtf.common.dto.CommonQuestionDTO;
import com.rtf.common.sql.dao.CommonQuestionBankSQLDAO;
import com.rtf.common.sql.dvo.CommonQuestionBankDVO;
import com.rtf.ott.dto.OTTQuestionDTO;
import com.rtf.ott.sql.dao.QuestionBankSQLDAO;
import com.rtf.ott.sql.dvo.QuestionBankDVO;
import com.rtf.ott.util.GridHeaderFilterUtil;
import com.rtf.saas.util.PaginationUtil;
import com.rtf.saas.util.SAASMessages;

/**
 * The Class CommonQuestionBankService.
 */
public class CommonQuestionBankService {

	/**
	 * Fetch question bank list specified search criteria.
	 *
	 * @param clientId  the client id
	 * @param dto       the dto
	 * @param pgNo      the pg no
	 * @param filter    the filter
	 * @param mstBankId the mst bank id
	 * @return the common question DTO
	 */
	public static CommonQuestionDTO fetchQuestionBankList(String clientId, CommonQuestionDTO dto, String pgNo,
			String filter, Integer mstBankId) {
		List<CommonQuestionBankDVO> qbList = null;
		try {
			Integer count = 0;

			String filterQuery = GridHeaderFilterUtil.getAllQuestionBankQuestionsForContestWithFilterQuery(filter);
			qbList = CommonQuestionBankSQLDAO.getAllQuestionsFromQBankWithFilter(clientId, pgNo, filterQuery,
					mstBankId);
			if (qbList == null || qbList.size() == 0) {
				dto.setSts(0);
				dto.setMsg(SAASMessages.ADMIN_QB_NOQUESTIONS);
			} else {
				count = CommonQuestionBankSQLDAO.getAllQuestionsCountFromQBankWithFilter(clientId, filterQuery,
						mstBankId);
			}
			dto.setPagination(PaginationUtil.calculatePaginationParameter(count, pgNo));
			dto.setQbList(qbList);
			dto.setSts(1);
		} catch (Exception ex) {
			dto.setSts(0);
			ex.printStackTrace();
		}
		return dto;
	}

	/**
	 * Gets the question bank data to export with specified search criteria.
	 *
	 * @param clientId the client id
	 * @param filter   the filter
	 * @return the question bank data to export
	 */
	public static List<QuestionBankDVO> getQuestionBankDataToExport(String clientId, String filter) {
		List<QuestionBankDVO> qbList = null;
		try {
			String filterQuery = GridHeaderFilterUtil.getAllQuestionBankQuestionsForContestWithFilterQuery(filter);
			qbList = CommonQuestionBankSQLDAO.getQuestionBankDataToExport(clientId, filterQuery);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return qbList;
	}

	/**
	 * Fetch Offline text trivia question bank list for given contest id.
	 *
	 * @param respDTO    the resp DTO
	 * @param filter     the filter
	 * @param catType    the cat type
	 * @param subCatType the sub cat type
	 * @param pgNo       the pg no
	 * @return the OTT question DTO
	 */
	public static OTTQuestionDTO fetchOTTQuestionBankListforContestId(OTTQuestionDTO respDTO, String filter,
			String catType, String subCatType, String pgNo) {
		List<QuestionBankDVO> qbList = null;
		try {

			Integer count = 0;
			String filterQuery = GridHeaderFilterUtil.getAllQuestionBankQuestionsForContestWithFilterQuery(filter);
			qbList = QuestionBankSQLDAO.getAllOLTXQuestionsFromQBankByContestIdWithFilter(respDTO.getClId(),
					respDTO.getCoId(), catType, subCatType, filter, pgNo,filter);
			if (qbList == null || qbList.size() == 0) {
				respDTO.setSts(0);
				respDTO.setMsg(SAASMessages.ADMIN_QB_NOQUESTIONS);
			} else {
				count = QuestionBankSQLDAO.getAllOLTXQuestionsCountFromQBankByContestIdWithFilter(respDTO.getClId(),
						respDTO.getCoId(), catType, subCatType, filterQuery);

			}
			respDTO.setPagination(PaginationUtil.calculatePaginationParameter(count, pgNo));
			respDTO.setQbList(qbList);
			respDTO.setSts(1);
		} catch (Exception ex) {
			respDTO.setSts(0);
			ex.printStackTrace();
		}
		return respDTO;
	}

	/**
	 * Create new question in specified question bank
	 *
	 * @param clientid the client id
	 * @param dto      the dto
	 * @return the common question DTO
	 * @throws Exception the exception
	 */
	public static CommonQuestionDTO createQuestion(String clientid, CommonQuestionDTO dto) throws Exception {
		try {
			CommonQuestionBankDVO dvo = dto.getDvo();
			dvo = CommonQuestionBankSQLDAO.createQuestion(clientid, dvo);
			if (dvo.getQsnbnkid() == null) {
				dto.setSts(0);
			} else {
				dto.setSts(1);
			}
			if (1 == dto.getSts()) {
				dvo.setClintid(clientid);
				CommonQuestionBankCassDAO.saveQuestionBank(dvo);
			}

		} catch (Exception ex) {
			dto.setSts(0);
			ex.printStackTrace();
		}
		return dto;
	}

	/**
	 * Update question in specified question bank.
	 *
	 * @param clientid the clientid
	 * @param dto      the dto
	 * @return the common question DTO
	 * @throws Exception the exception
	 */
	public static CommonQuestionDTO updateQuestion(String clientid, CommonQuestionDTO dto) throws Exception {
		dto.setSts(0);
		try {
			CommonQuestionBankDVO dvo = dto.getDvo();
			CommonQuestionBankDVO dvoOrig = CommonQuestionBankSQLDAO.getQuestionsFromQBankById(clientid,
					dvo.getQsnbnkid(), dvo.getMstqbankId());

			Integer updCnt = CommonQuestionBankSQLDAO.updateQuestion(clientid, dvo);
			dto.setSts(updCnt);
			if (1 == updCnt) {
				dvo.setClintid(clientid);
				CommonQuestionBankCassDAO.deleteQuestionBank(dvoOrig);
				CommonQuestionBankCassDAO.saveQuestionBank(dvo);
			}
		} catch (Exception ex) {
			dto.setSts(0);
			ex.printStackTrace();
		}
		return dto;
	}

	/**
	 * Delete question from specified question bank.
	 *
	 * @param clientid the clientid
	 * @param dto      the dto
	 * @return the common question DTO
	 * @throws Exception the exception
	 */
	public static CommonQuestionDTO deleteQuestion(String clientid, CommonQuestionDTO dto) throws Exception {
		try {
			dto.setSts(0);
			CommonQuestionBankDVO dvo = dto.getDvo();
			CommonQuestionBankDVO dvoOrig = CommonQuestionBankSQLDAO.getQuestionsFromQBankById(clientid,
					dvo.getQsnbnkid(), dvo.getMstqbankId());
			Integer updCnt = CommonQuestionBankSQLDAO.deleteQuestion(clientid, dvo);
			if (updCnt == 1) {
				dto.setSts(1);
				dvo.setClintid(clientid);
				CommonQuestionBankCassDAO.deleteQuestionBank(dvoOrig);
			}
		} catch (Exception ex) {
			dto.setSts(0);
			ex.printStackTrace();
		}
		return dto;
	}

	/**
	 * Gets the questions count by category subcategory and question type.
	 *
	 * @param clientId the client id
	 * @param category the category
	 * @param subCat   the sub cat
	 * @param qMode    the q mode
	 * @return Integer the questions count by category sub category and question type
	 * @throws Exception the exception
	 */
	public static Integer getQuestionsCountByCategorySubcategoryAndQuestionType(String clientId, String category,
			String subCat, String qMode) throws Exception {
		Integer count = 0;
		try {
			count = QuestionBankSQLDAO.getQuestionsCountByCategorySubcategoryAndQuestionType(clientId, category, subCat,
					qMode);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return count;
	}

}
