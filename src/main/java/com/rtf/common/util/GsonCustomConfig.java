/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.util;

import java.util.Date;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * The Class GsonCustomConfig.
 */
public class GsonCustomConfig {

	/**
	 * Gets the Gson object with custom date serialized builder.
	 *
	 * @return the gson builder
	 */
	public static Gson getGsonBuilder() {
		Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new GsonCustomDateSerializer()).create();
		return gson;
	}
}
