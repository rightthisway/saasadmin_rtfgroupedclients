package com.rtf.common.util;

/**
 * The Class GridSortingUtil.
 */
public class GridSortingUtil {

	/**
	 * Generate the contest grid sorting query.
	 *
	 * @param filter
	 *            the filter
	 * @return the contest grid sorting query
	 */
	public static String getContestSortingQuery(String filter) {
		String sortingStr = null;
		try {
			GridSorting sorting = getSortingParams(filter);
			if (sorting == null) {
				return "";
			}
			if (sorting.getColumn().equalsIgnoreCase("name")) {
				sortingStr = " ORDER BY c.conname " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("extName")) {
				sortingStr = " ORDER BY c.extconname " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("stDateTimeStr")) {
				sortingStr = " ORDER BY c.consrtdate " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("coType")) {
				sortingStr = " ORDER BY c.contype " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("cat")) {
				sortingStr = " ORDER BY c.cattype " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("subCat")) {
				sortingStr = " ORDER BY c.subcattype " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("sumRwdType")) {
				sortingStr = " ORDER BY c.sumryrwdtype " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("ansType")) {
				sortingStr = " ORDER BY c.anstype " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("isElimination")) {
				sortingStr = " ORDER BY c.iselimtype " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("isSplitSummary")) {
				sortingStr = " ORDER BY c.issumrysplitable " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("isLotEnbl")) {
				sortingStr = " ORDER BY c.islotryenabled " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("isPwd")) {
				sortingStr = " ORDER BY c.ispwd " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("conpwd")) {
				sortingStr = " ORDER BY c.conpwd " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("qSize")) {
				sortingStr = " ORDER BY c.noofqns " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("seqNo")) {
				sortingStr = " ORDER BY c.cardseqno " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("crBy")) {
				sortingStr = " ORDER BY c.creby " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("crDateTimeStr")) {
				sortingStr = " ORDER BY c.credate " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("upBy")) {
				sortingStr = " ORDER BY c.updby " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("upDateTimeStr")) {
				sortingStr = " ORDER BY c.upddate " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("status")) {
				sortingStr = " ORDER BY s.isconactivetext " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("sumRwdVal")) {
				sortingStr = " ORDER BY c.sumryrwdval " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("winRwdVal")) {
				sortingStr = " ORDER BY c.lotryrwdval " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("partiRwdVal")) {
				sortingStr = " ORDER BY c.participantrwdval " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("winRwdType")) {
				sortingStr = " ORDER BY c.lotryrwdtype " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("partiRwdType")) {
				sortingStr = " ORDER BY c.participantrwdtype " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("migStatus")) {
				sortingStr = " ORDER BY c.migrstatus " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("lastAction")) {
				sortingStr = " ORDER BY c.lastaction " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("queRwdType")) {
				sortingStr = " ORDER BY c.qsnrwdtype " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("lastQue")) {
				sortingStr = " ORDER BY c.lastqsn " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("expPartCntStr")) {
				sortingStr = " ORDER BY pc.pcdisplay " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("winnerCount")) {
				sortingStr = " ORDER BY c.ngrndwnrs " + sorting.getOrder();
			}else if (sorting.getColumn().equalsIgnoreCase("hostedBy")) {
				sortingStr = " ORDER BY c.hostname " + sorting.getOrder();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sortingStr;
	}

	/**
	 * Generate the contest question grid sorting query.
	 *
	 * @param filter
	 *            the filter
	 * @return the contest questions grid sorting query
	 */
	public static String getContestQuestionsSortingQuery(String filter) {
		String sortingStr = null;
		try {
			GridSorting sorting = getSortingParams(filter);
			if (sorting == null) {
				return "";
			}
			if (sorting.getColumn().equalsIgnoreCase("qsntext")) {
				sortingStr = " ORDER BY  qsntext " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("opa")) {
				sortingStr = " ORDER BY  ansopta " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("opb")) {
				sortingStr = " ORDER BY  ansoptb " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("opc")) {
				sortingStr = " ORDER BY  ansoptc " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("opd")) {
				sortingStr = " ORDER BY  ansoptd " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("corans")) {
				sortingStr = "ORDER BY  corans " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("qsnseqno")) {
				sortingStr = "ORDER BY  qsnseqno " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("qsnorient")) {
				sortingStr = "ORDER BY  qsnorient " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("ansRwdVal")) {
				sortingStr = "ORDER BY  rwdval " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("mjRwdType")) {
				sortingStr = "ORDER BY  jackpottype " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("mjRwdVal")) {
				sortingStr = "ORDER BY  maxqtyperwinner " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("mjWinnersCount")) {
				sortingStr = "ORDER BY  maxwinners " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("qDifLevel")) {
				sortingStr = "ORDER BY  difficultlevel " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("crBy")) {
				sortingStr = " ORDER BY  creby " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("crDate")) {
				sortingStr = " ORDER BY  credate " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("upBy")) {
				sortingStr = " ORDER BY  updby " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("upDate")) {
				sortingStr = " ORDER BY  upddate " + sorting.getOrder();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sortingStr;
	}

	/**
	 * Generate the contest product sets grid sorting query.
	 *
	 * @param filter
	 *            the filter
	 * @return the contest product sets grid sorting query
	 */
	public static String getContestProductSetsSortingQuery(String filter) {
		String sortingStr = null;
		try {
			GridSorting sorting = getSortingParams(filter);
			if (sorting == null) {
				return "";
			}
			if (sorting.getColumn().equalsIgnoreCase("clintId")) {
				sortingStr = " ORDER BY clintid " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("mercId")) {
				sortingStr = " ORDER BY mercid " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("conId")) {
				sortingStr = " ORDER BY conid " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("mercProdid")) {
				sortingStr = " ORDER BY mercprodid " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("dispSeq")) {
				sortingStr = " ORDER BY dispseq " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("prodName")) {
				sortingStr = " ORDER BY prodname " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("prodDesc")) {
				sortingStr = " ORDER BY proddesc " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("unitValue")) {
				sortingStr = " ORDER BY unitvalue " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("pricePerUnit")) {
				sortingStr = " ORDER BY priceperunit " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("unitType")) {
				sortingStr = " ORDER BY unittype " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("dispStatus")) {
				sortingStr = " ORDER BY dispstatus " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("dispTime")) {
				sortingStr = " ORDER BY disptime " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("creby")) {
				sortingStr = " ORDER BY creby " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("credate")) {
				sortingStr = " ORDER BY credate " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("updby")) {
				sortingStr = " ORDER BY updby " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("upddate")) {
				sortingStr = " ORDER BY upddate " + sorting.getOrder();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sortingStr;
	}

	/**
	 * Generate Seller/Merchant grid sorting query.
	 *
	 * @param filter
	 *            the filter
	 * @return the Seller/Merchant grid sorting query
	 */
	public static String getMerchantSortingQuery(String filter) {
		String sortingStr = null;
		try {
			GridSorting sorting = getSortingParams(filter);
			if (sorting == null) {
				return "";
			}
			if (sorting.getColumn().equalsIgnoreCase("slerId")) {
				sortingStr = " ORDER BY sler_id " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("firstName")) {
				sortingStr = " ORDER BY first_name " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("email")) {
				sortingStr = " ORDER BY email " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("phone")) {
				sortingStr = " ORDER BY phone " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("compName")) {
				sortingStr = " ORDER BY comp_name " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("slerStatus")) {
				sortingStr = " ORDER BY sler_status " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("creby")) {
				sortingStr = " ORDER BY lupd_by " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("lupdBy")) {
				sortingStr = " ORDER BY lupd_by " + sorting.getOrder();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sortingStr;
	}

	/**
	 * Generate product grid sorting query.
	 *
	 * @param filter
	 *            the filter
	 * @return the product grid sorting query
	 */
	public static String getProductSortingQuery(String filter) {
		String sortingStr = null;
		try {
			GridSorting sorting = getSortingParams(filter);
			if (sorting == null) {
				return "";
			}
			if (sorting.getColumn().equalsIgnoreCase("slerId")) {
				sortingStr = " ORDER BY sler_id " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("slrpitmsId")) {
				sortingStr = " ORDER BY slrpitms_id " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("pname")) {
				sortingStr = " ORDER BY pname " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("prodPriceDtl")) {
				sortingStr = " ORDER BY pprc_dtl " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("pdesc")) {
				sortingStr = " ORDER BY pdesc " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("pregMinPrc")) {
				sortingStr = " ORDER BY preg_min_prc " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("pselMinPrc")) {
				sortingStr = " ORDER BY psel_min_prc " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("pstatus")) {
				sortingStr = " ORDER BY pstatus " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("lupdBy")) {
				sortingStr = " ORDER BY lupd_by " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("lupdDate")) {
				sortingStr = " ORDER BY lupd_date " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("maxCustQty")) {
				sortingStr = " ORDER BY max_cust_qty " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("priceType")) {
				sortingStr = " ORDER BY price_type " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("isVariant")) {
				sortingStr = " ORDER BY is_variant " + sorting.getOrder();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sortingStr;
	}

	/**
	 * Generate OTT contest grid sorting query.
	 *
	 * @param filter
	 *            the filter
	 * @return the OTT contest grid sorting query
	 */
	public static String getOTTContestSortingQuery(String filter) {
		String sortingStr = null;
		try {
			GridSorting sorting = getSortingParams(filter);
			if (sorting == null) {
				return "";
			}
			if (sorting.getColumn().equalsIgnoreCase("name")) {
				sortingStr = " ORDER BY  c.conname " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("stDateTimeStr")) {
				sortingStr = " ORDER BY c.consrtdate " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("enDateTimeStr")) {
				sortingStr = " ORDER BY c.conenddate " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("cat")) {
				sortingStr = " ORDER BY  c.cattype " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("subCat")) {
				sortingStr = " ORDER BY  c.subcattype " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("qMode")) {
				sortingStr = "ORDER BY  c.qsnmode " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("ansType")) {
				sortingStr = " ORDER BY  c.anstype " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("isElimination")) {
				sortingStr = " ORDER BY  c.iselimtype " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("qSize")) {
				sortingStr = " ORDER BY  c.noofqns " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("seqNo")) {
				sortingStr = " ORDER BY  c.cardseqno " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("crBy")) {
				sortingStr = " ORDER BY  c.creby " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("crDateTimeStr")) {
				sortingStr = " ORDER BY c.credate " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("upBy")) {
				sortingStr = " ORDER BY  c.updby " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("upDateTimeStr")) {
				sortingStr = " ORDER BY c.upddate " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("status")) {
				sortingStr = " ORDER BY  s.isconactivetext  " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("playinterval")) {
				sortingStr = " ORDER BY  c.playinterval " + sorting.getOrder();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sortingStr;
	}

	/**
	 * Generate OTT contest questions sorting query.
	 *
	 * @param filter
	 *            the filter
	 * @return the OTT contest questions sorting query
	 */
	public static String getOTTContestQuestionsSortingQuery(String filter) {
		String sortingStr = null;
		try {
			GridSorting sorting = getSortingParams(filter);
			if (sorting == null) {
				return "";
			}
			if (sorting.getColumn().equalsIgnoreCase("qsntext")) {
				sortingStr = " ORDER BY  qsntext " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("opa")) {
				sortingStr = " ORDER BY  ansopta " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("opb")) {
				sortingStr = " ORDER BY  ansoptb " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("opc")) {
				sortingStr = " ORDER BY  ansoptc " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("opd")) {
				sortingStr = " ORDER BY  ansoptd " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("corans")) {
				sortingStr = "ORDER BY  corans " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("crBy")) {
				sortingStr = " ORDER BY  creby " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("credateStr")) {
				sortingStr = " ORDER BY credate " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("upBy")) {
				sortingStr = " ORDER BY  updby " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("upddateStr")) {
				sortingStr = " ORDER BY upddate " + sorting.getOrder();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sortingStr;
	}

	/**
	 * Generate SNW contest grid sorting query.
	 *
	 * @param filter
	 *            the filter
	 * @return the SNW contest grid sorting query
	 */
	public static String getSNWContestSortingQuery(String filter) {
		String sortingStr = null;
		try {
			GridSorting sorting = getSortingParams(filter);
			if (sorting == null) {
				return "";
			}
			if (sorting.getColumn().equalsIgnoreCase("name")) {
				sortingStr = " ORDER BY  c.conname " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("stDateTimeStr")) {
				sortingStr = " ORDER BY  c.consrtdate " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("enDateTimeStr")) {
				sortingStr = " ORDER BY c.conenddate " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("cat")) {
				sortingStr = " ORDER BY  c.cattype " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("subCat")) {
				sortingStr = " ORDER BY  c.subcattype " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("qMode")) {
				sortingStr = " ORDER BY  c.qsnmode " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("ansType")) {
				sortingStr = " ORDER BY  c.anstype " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("isElimination")) {
				sortingStr = " ORDER BY  c.iselimtype " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("qSize")) {
				sortingStr = " ORDER BY  c.noofqns " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("seqNo")) {
				sortingStr = " ORDER BY  c.cardseqno " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("crBy")) {
				sortingStr = " ORDER BY  c.creby " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("crDateTimeStr")) {
				sortingStr = " ORDER BY c.credate " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("upBy")) {
				sortingStr = " ORDER BY  c.updby " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("upDateTimeStr")) {
				sortingStr = " ORDER BY c.upddate " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("status")) {
				sortingStr = " ORDER BY  s.isconactivetext " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("playinterval")) {
				sortingStr = " ORDER BY  c.playinterval " + sorting.getOrder();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sortingStr;
	}

	/**
	 * Generate SNW contest questions grid sorting query.
	 *
	 * @param filter
	 *            the filter
	 * @return the SNW contest questions grid sorting query
	 */
	public static String getSNWContestQuestionsSortingQuery(String filter) {
		String sortingStr = null;
		try {
			GridSorting sorting = getSortingParams(filter);
			if (sorting == null) {
				return "";
			}
			if (sorting.getColumn().equalsIgnoreCase("qsntext")) {
				sortingStr = " ORDER BY qsntext " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("opa")) {
				sortingStr = " ORDER BY ansopta " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("opb")) {
				sortingStr = " ORDER BY ansoptb " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("opc")) {
				sortingStr = " ORDER BY ansoptc " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("opd")) {
				sortingStr = " ORDER BY ansoptd " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("corans")) {
				sortingStr = " ORDER BY corans " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("qsnorient")) {
				sortingStr = " ORDER BY qsnorient " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("crBy")) {
				sortingStr = " ORDER BY creby " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("credateStr")) {
				sortingStr = " ORDER BY credate " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("upBy")) {
				sortingStr = " ORDER BY updby " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("upddateStr")) {
				sortingStr = " ORDER BY upddate " + sorting.getOrder();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sortingStr;
	}

	/**
	 * Generate question bank grid sorting query.
	 *
	 * @param filter
	 *            the filter
	 * @return the question bank grid sorting query
	 */
	public static String getQuestionBankSortingQuery(String filter) {
		String sortingStr = null;
		try {
			GridSorting sorting = getSortingParams(filter);
			if (sorting == null) {
				return "";
			}
			if (sorting.getColumn().equalsIgnoreCase("qtx")) {
				sortingStr = " ORDER BY qsntext " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("opa")) {
				sortingStr = " ORDER BY ansopta " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("opb")) {
				sortingStr = " ORDER BY ansoptb " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("opc")) {
				sortingStr = " ORDER BY ansoptc " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("opd")) {
				sortingStr = " ORDER BY ansoptd " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("cans")) {
				sortingStr = " ORDER BY corans " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("anstype")) {
				sortingStr = " ORDER BY anstype " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("qOrientn")) {
				sortingStr = " ORDER BY qsnorient " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("ctyp")) {
				sortingStr = " ORDER BY cattype " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("sctyp")) {
				sortingStr = " ORDER BY subcattype " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("creBy")) {
				sortingStr = " ORDER BY creby " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("crDateTimeStr")) {
				sortingStr = " ORDER BY credate " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("updBy")) {
				sortingStr = " ORDER BY updby " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("upDateTimeStr")) {
				sortingStr = " ORDER BY upddate " + sorting.getOrder();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sortingStr;
	}

	/**
	 * Generate reward types grid sorting query.
	 *
	 * @param filter
	 *            the filter
	 * @return the reward types grid sorting query
	 */
	public static String getRewardTypesSortingQuery(String filter) {
		String sortingStr = null;
		try {
			GridSorting sorting = getSortingParams(filter);
			if (sorting == null) {
				return "";
			}
			if (sorting.getColumn().equalsIgnoreCase("rwdtype")) {
				sortingStr = " ORDER BY  rwdtype " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("rwddesc")) {
				sortingStr = " ORDER BY  rwddesc " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("rwdunimesr")) {
				sortingStr = " ORDER BY  rwdunimesr " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("creby")) {
				sortingStr = " ORDER BY  creby " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("crDateTimeStr")) {
				sortingStr = " ORDER BY credate " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("updby")) {
				sortingStr = " ORDER BY updby " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("upDateTimeStr")) {
				sortingStr = " ORDER BY upddate " + sorting.getOrder();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return sortingStr;
	}

	/**
	 * Generate categories grid sorting query.
	 *
	 * @param filter
	 *            the filter
	 * @return categories grid sorting query
	 */
	public static String getCategoriesSortingQuery(String filter) {
		String sortingStr = null;
		try {
			GridSorting sorting = getSortingParams(filter);
			if (sorting == null) {
				return "";
			}
			if (sorting.getColumn().equalsIgnoreCase("cattype")) {
				sortingStr = " ORDER BY cattype " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("catdesc")) {
				sortingStr = " ORDER BY catdesc " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("creby")) {
				sortingStr = " ORDER BY creby " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("crDateTimeStr")) {
				sortingStr = " ORDER BY credate " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("updby")) {
				sortingStr = " ORDER BY updby " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("upDateTimeStr")) {
				sortingStr = " ORDER BY upddate " + sorting.getOrder();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return sortingStr;
	}

	/**
	 * Generate sub categories grid sorting query.
	 *
	 * @param filter
	 *            the filter
	 * @return sub categories grid sorting query
	 */
	public static String getSubCategoriesSortingQuery(String filter) {
		String sortingStr = null;
		try {
			GridSorting sorting = getSortingParams(filter);
			if (sorting == null) {
				return "";
			}
			if (sorting.getColumn().equalsIgnoreCase("cattype")) {
				sortingStr = " ORDER BY  cattype " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("subcattype")) {
				sortingStr = " ORDER BY  subcattype " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("subcatdesc")) {
				sortingStr = " ORDER BY  subcatdesc " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("creby")) {
				sortingStr = " ORDER BY  creby " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("crDateTimeStr")) {
				sortingStr = " ORDER BY credate " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("updby")) {
				sortingStr = " ORDER BY  updby " + sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("upDateTimeStr")) {
				sortingStr = " ORDER BY upddate " + sorting.getOrder();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return sortingStr;
	}

	/**
	 * Generate users grid sorting query.
	 *
	 * @param filter
	 *            the filter
	 * @return users grid sorting query
	 */
	public static String getUserSortingQuery(String filter) {
		String sortingStr = null;
		try {
			GridSorting sorting = getSortingParams(filter);
			if (sorting == null) {
				return "";
			}
			if (sorting.getColumn().equalsIgnoreCase("userid")) {
				sortingStr = " ORDER BY  c.userid "+sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("userfname")) {
				sortingStr = " ORDER BY  c.userfname "+sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("userlname")) {
				sortingStr = " ORDER BY  c.userlname "+sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("userphone")) {
				sortingStr = " ORDER BY  c.userphone "+sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("userrole")) {
				sortingStr = " ORDER BY  c.userrole "+sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("isactive")) {
				sortingStr = " ORDER BY  c.isactive "+sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("creby")) {
				sortingStr = " ORDER BY  c.creby "+sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("credateStr")) {
				sortingStr = " ORDER BY c.credate "+sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("updby")) {
				sortingStr = " ORDER BY  c.updby "+sorting.getOrder();
			} else if (sorting.getColumn().equalsIgnoreCase("upddateStr")) {
				sortingStr = " ORDER BY c.upddate "+sorting.getOrder();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return sortingStr;
	}

	private static GridSorting getSortingParams(String filter) {

		GridSorting sorting = new GridSorting();
		if (filter != null && !filter.isEmpty() && filter.contains(":") && filter.contains(",")) {
			String params[] = filter.split(",");
			for (String param : params) {
				String nameValue[] = param.split(":");
				if (nameValue.length == 2) {
					String name = nameValue[0];
					String value = nameValue[1];
					if(name != null && name.equalsIgnoreCase("SORTINGCOLUMN") && value != null
							&& !value.trim().isEmpty() && !value.equalsIgnoreCase("undefined")) {
						sorting.setColumn(value);
					}
					if(name != null && name.equalsIgnoreCase("SORTINGORDER") && value != null
							&& !value.trim().isEmpty() && !value.equalsIgnoreCase("undefined")) {
						sorting.setOrder(value);
					}
				}
			}
		}
		if(sorting.getColumn()==null || sorting.getColumn().trim().isEmpty()){
			return null;
		}
		if(sorting.getOrder() == null || sorting.getOrder().trim().isEmpty()){
			return null;
		}
		return sorting;
	}
}
