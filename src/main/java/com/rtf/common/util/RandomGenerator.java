/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.util;

/**
 * The Class RandomGenerator.
 */
public class RandomGenerator {

	/**
	 * Gets the random alpha numeric string.
	 *
	 * @param n the n
	 * @return the alpha numeric string
	 */
	public static String getAlphaNumericString(int n) {
		String alphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "0123456789" + "abcdefghijklmnopqrstuvxyz";
		StringBuilder sb = new StringBuilder(n);
		for (int i = 0; i < n; i++) {
			int index = (int) (alphaNumericString.length() * Math.random());
			sb.append(alphaNumericString.charAt(index));
		}
		return sb.toString();
	}

	/**
	 * Gets the random numeric string.
	 *
	 * @param n the n
	 * @return the numeric string
	 */
	public static Integer getnumericString(int n) {
		String numericString = "0123456789";
		StringBuilder sb = new StringBuilder(n);
		for (int i = 0; i < n; i++) {
			int index = (int) (numericString.length() * Math.random());
			sb.append(numericString.charAt(index));
		}

		return Integer.valueOf(sb.toString());
	}

	/**
	 * Gets the alpha numeric string for coupon code.
	 *
	 * @param n the n
	 * @return the coupon code alpha numeric string
	 */
	public static String getCouponCodeAlphaNumericString(int n) {
		String alphaNumericString = "ABCDEFGHJKPQRTUVXYZ" + "0123456789";
		StringBuilder sb = new StringBuilder(n);
		for (int i = 0; i < n; i++) {
			int index = (int) (alphaNumericString.length() * Math.random());
			sb.append(alphaNumericString.charAt(index));
		}
		return sb.toString();
	}
}