/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.util;

import com.rtf.saas.cass.db.CassandraConnector;
import com.rtf.saas.sql.db.DatabaseConnections;

/**
 * The Class CommonCacheUtil.
 */
public class CommonCacheUtil {

	/**
	 * Load application configuration values on startup.
	 *
	 * @throws Exception the exception
	 */
	public static void loadApplicationValues() throws Exception {
		
		DatabaseConnections.loadApplicationValues();
		CassandraConnector.loadApplicationValues();
		SaaSAdminAppCache.loadApplicationValues();
	}
}