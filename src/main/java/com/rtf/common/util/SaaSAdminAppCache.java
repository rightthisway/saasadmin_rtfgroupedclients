/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.util;

import java.util.ResourceBundle;

/**
 * The Class SaaSAdminAppCache.
 */
public class SaaSAdminAppCache {

	/** The env folder type. */
	public static String envfoldertype = null;

	/** The s3 static bucket. */
	public static String s3staticbucket = null;

	/** The s3 rewards folder. */
	public static String s3rewardsfolder = null;
	
	/** The s3 common saas assets folder. */
	public static String s3saasfolder = null;

	/** The s3 shop products folder. */
	public static String s3shopproductsfolder = null;

	/** The cf static url. */
	public static String cfstaticurl = null;

	/** The tmp folder. */
	public static String TMP_FOLDER = null;

	/** The s3 inventory poducts folder. */
	public static String s3inventoryoductsfolder = null;
	
	/** The S3 secured bucket. */
	public static String s3SecuredBucket = null;
	
	/** The S3 RTF media bucket. */
	public static String s3RtfMediaBucket = null;
	
	/** The s3 Contest script folder. */
	public static String s3ContestScriptFolder = null;

	/**
	 * Load application configuration values.
	 */
	public static void loadApplicationValues() {
		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		envfoldertype = resourceBundle.getString("s3.env.foldertype");
		s3staticbucket = resourceBundle.getString("s3.staticassets.bucketurl");
		s3SecuredBucket  =  resourceBundle.getString("s3.secureassets.bucketurl");
		s3rewardsfolder = resourceBundle.getString("s3.rewards.folder");
		cfstaticurl = resourceBundle.getString("cf.staticassets.baserurl");
		s3shopproductsfolder = resourceBundle.getString("s3.shopproducts.folder");
		s3inventoryoductsfolder = resourceBundle.getString("s3.inventoryproducts.folder");
		s3ContestScriptFolder  = resourceBundle.getString("s3.contestscript.folder");
		s3RtfMediaBucket = resourceBundle.getString("s3.rtfmedia.bucketurl");
		s3saasfolder = resourceBundle.getString("s3.saasassets.folder");
		

		TMP_FOLDER = System.getProperty("java.io.tmpdir");
	}
}
