/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.sql.dvo;

/**
 * The Class GridPreferenceDVO.
 */
public class GridPreferenceDVO {

	
	/** The id. */
	private Integer id;
	
	/** The Client Id */
	private String clId;
	
	/** The user id. */
	private String userId;
	
	/** The grid name. */
	private String gridName;
	
	/** The column name string. */
	private String columnString;
	
	
	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	
	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	/**
	 * Gets the client id.
	 *
	 * @return the cl id
	 */
	public String getClId() {
		return clId;
	}
	
	/**
	 * Sets the client id.
	 *
	 * @param clId the new cl id
	 */
	public void setClId(String clId) {
		this.clId = clId;
	}
	
	/**
	 * Gets the user id.
	 *
	 * @return the user id
	 */
	public String getUserId() {
		return userId;
	}
	
	/**
	 * Sets the user id.
	 *
	 * @param userId the new user id
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	/**
	 * Gets the grid name.
	 *
	 * @return the grid name
	 */
	public String getGridName() {
		return gridName;
	}
	
	/**
	 * Sets the grid name.
	 *
	 * @param gridName the new grid name
	 */
	public void setGridName(String gridName) {
		this.gridName = gridName;
	}
	
	/**
	 * Gets the column name string.
	 *
	 * @return the column string
	 */
	public String getColumnString() {
		return columnString;
	}
	
	/**
	 * Sets the column name string.
	 *
	 * @param columnString the new column string
	 */
	public void setColumnString(String columnString) {
		this.columnString = columnString;
	}
}
