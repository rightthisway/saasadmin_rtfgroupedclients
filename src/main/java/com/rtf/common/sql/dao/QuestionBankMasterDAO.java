/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.sql.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.rtf.common.sql.dvo.QuestionBankMasterDVO;
import com.rtf.saas.sql.db.DatabaseConnections;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.PaginationUtil;

/**
 * The Class QuestionBankMasterDAO.
 */
public class QuestionBankMasterDAO {

	/**
	 * Gets the all master question bank with filter.
	 *
	 * @param clientId the client id
	 * @param pgNo the pg no
	 * @param filterQuery the filter query
	 * @param activeFlag the active flag
	 * @return the all master bank with filter
	 * @throws Exception the exception
	 */
	public static List<QuestionBankMasterDVO> getAllMasterBankWithFilter(String clientId, String pgNo,
			String filterQuery, String activeFlag) throws Exception {

		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		List<QuestionBankMasterDVO> masterBankList = null;

		try {
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT * from sd_mst_question_bank_type  with(nolock) where clintid = ? and isactive = ? ");
			String paginationQuery = PaginationUtil.getPaginationQuery(pgNo);
			if (filterQuery != null && !filterQuery.isEmpty()) {
				sql.append(filterQuery);
			}
			sql.append(" ORDER BY credate desc ");
			sql.append(paginationQuery);		
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);			
			Boolean isactive = GenUtil.getActiveBitFlag( activeFlag);
			ps.setBoolean(2, isactive);
			rs = ps.executeQuery();
			masterBankList = new ArrayList<QuestionBankMasterDVO>();
			while (rs.next()) {
				Timestamp timestamp = null;
				QuestionBankMasterDVO vo = new QuestionBankMasterDVO();
				vo.setMstqsnbnkid(rs.getInt("mstqsnbnkid"));
				vo.setClintid(rs.getString("clintid"));
				vo.setMstqsnbnk(rs.getString("mstqsnbnk"));
				vo.setMstqsnbnkdesc(rs.getString("mstqsnbnkdesc"));
				vo.setCreby(rs.getString("creby"));
				vo.setUpdby(rs.getString("updby"));
				timestamp = rs.getTimestamp("credate");
				vo.setCredate(new java.util.Date(timestamp != null ? timestamp.getTime() : -10));
				timestamp = rs.getTimestamp("upddate");
				vo.setUpddate(new java.util.Date(timestamp != null ? timestamp.getTime() : -10));
				vo.setIsactive(rs.getBoolean("isactive"));
				masterBankList.add(vo);
			}
			return masterBankList;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;

	}

	/**
	 * Gets the all master question  bank list.
	 *
	 * @param clientId the client id
	 * @param activeFlag the active flag
	 * @return the all master bank list
	 * @throws Exception the exception
	 */
	public static List<QuestionBankMasterDVO> getAllMasterBankList(String clientId, String activeFlag) throws Exception {

		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		List<QuestionBankMasterDVO> masterBankList = null;

		try {
			String sql = " SELECT * from sd_mst_question_bank_type  with(nolock) where clintid = ? and isactive = ? ";
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, clientId);			
			Boolean isactive = GenUtil.getActiveBitFlag( activeFlag);
			ps.setBoolean(2, isactive);
			rs = ps.executeQuery();
			masterBankList = new ArrayList<QuestionBankMasterDVO>();
			while (rs.next()) {
				Timestamp timestamp = null;
				QuestionBankMasterDVO vo = new QuestionBankMasterDVO();
				vo.setMstqsnbnkid(rs.getInt("mstqsnbnkid"));
				vo.setClintid(rs.getString("clintid"));
				vo.setMstqsnbnk(rs.getString("mstqsnbnk"));
				vo.setMstqsnbnkdesc(rs.getString("mstqsnbnkdesc"));
				vo.setCreby(rs.getString("creby"));
				vo.setUpdby(rs.getString("updby"));
				timestamp = rs.getTimestamp("credate");
				vo.setCredate(new java.util.Date(timestamp != null ? timestamp.getTime() : -10));
				timestamp = rs.getTimestamp("upddate");
				vo.setUpddate(new java.util.Date(timestamp != null ? timestamp.getTime() : -10));
				vo.setIsactive(rs.getBoolean("isactive"));
				masterBankList.add(vo);
			}
			return masterBankList;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;

	}
	

	/**
	 * Gets the all master question bank count with given filter condition.
	 *
	 * @param clientId the client id
	 * @param filterQuery the filter query
	 * @param activeFlag the active flag
	 * @return the all master bank count with filter
	 * @throws Exception the exception
	 */
	public static Integer getAllMasterBankCountWithFilter(String clientId, String filterQuery, String activeFlag)
			throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Integer count = 0;
		try {

			StringBuffer sql = new StringBuffer();
			sql.append("SELECT clintid from sd_mst_question_bank_type  with(nolock) where clintid = ? and isactive = ? ");

			if (filterQuery != null && !filterQuery.isEmpty()) {
				sql.append(filterQuery);
			}
			sql.append(" ORDER BY credate desc ");	
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			Boolean isactive = Boolean.valueOf(activeFlag);
			ps.setBoolean(2, isactive);		
			rs = ps.executeQuery();
			while (rs.next()) {
				count = rs.getInt(1);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return count;
	}

	/**
	 * Creates the new question bank.
	 *
	 * @param clientid the client id
	 * @param dvo the dvo
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer createQuestionBank(String clientid, QuestionBankMasterDVO dvo) throws Exception {
		
		Boolean isRecPresent =  findRecordByNameMasterBank( clientid, dvo.getMstqsnbnk());
		if(isRecPresent)  return 2;
		
		Connection conn = null;	
		PreparedStatement ps = null;
		int affectedRows = 0;	
		StringBuffer sql = new StringBuffer();
		sql.append(" insert into  sd_mst_question_bank_type" + " ( clintid ,mstqsnbnk")
		.append(",mstqsnbnkdesc,creby,credate,isactive,upddate,updby )  ")				
		.append("values(?,?,?,?,getdate(),?,getDate(),?) ");

		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, clientid);
			ps.setString(2, dvo.getMstqsnbnk());
			ps.setString(3, dvo.getMstqsnbnkdesc());
			ps.setString(4, dvo.getCreby());
			ps.setBoolean(5, Boolean.TRUE);
			ps.setString(6, dvo.getCreby());
			affectedRows = ps.executeUpdate();
			return affectedRows;

		} catch (SQLException se) {
			se.printStackTrace();
			affectedRows = 0;
		} catch (Exception e) {
			e.printStackTrace();
			affectedRows = 0;
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return affectedRows;
	}
	
	
	/**
	 * Check is master question bank is exist with give name
	 *
	 * @param clientId the client id
	 * @param mstqsnbnk the mstqsnbnk
	 * @return the boolean
	 * @throws Exception the exception
	 */
	public static Boolean findRecordByNameMasterBank(String clientId,  String mstqsnbnk) throws Exception {

		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		Boolean isRecordPresent = Boolean.FALSE;

		try {
			String sql = " SELECT * from sd_mst_question_bank_type  with(nolock) where clintid = ? and mstqsnbnk = ? ";	
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, clientId);			
			ps.setString(2, mstqsnbnk);
			rs = ps.executeQuery();		
			while (rs.next()) {
				isRecordPresent = Boolean.TRUE;			
			}
			return isRecordPresent;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return isRecordPresent;
	}
	
	
	/**
	 * Get the master question bank by given question bank id.
	 *
	 * @param clientId the client id
	 * @param mstqsnbnkId the mstqsnbnk id
	 * @return the question bank master DVO
	 * @throws Exception the exception
	 */
	public static QuestionBankMasterDVO findByNameMasterBankId(String clientId,  Integer mstqsnbnkId) throws Exception {

		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;		
		QuestionBankMasterDVO vo = null;
		try {
			String sql = " SELECT * from sd_mst_question_bank_type  with(nolock) where clintid = ? and mstqsnbnkid = ? ";			
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, clientId);			
			ps.setInt(2, mstqsnbnkId);
			rs = ps.executeQuery();			
			while (rs.next()) {
				Timestamp timestamp = null;
				vo = new QuestionBankMasterDVO();
				vo.setMstqsnbnkid(rs.getInt("mstqsnbnkid"));
				vo.setClintid(rs.getString("clintid"));
				vo.setMstqsnbnk(rs.getString("mstqsnbnk"));
				vo.setMstqsnbnkdesc(rs.getString("mstqsnbnkdesc"));
				vo.setCreby(rs.getString("creby"));
				vo.setUpdby(rs.getString("updby"));
				timestamp = rs.getTimestamp("credate");
				vo.setCredate(new java.util.Date(timestamp != null ? timestamp.getTime() : -10));
				timestamp = rs.getTimestamp("upddate");
				vo.setUpddate(new java.util.Date(timestamp != null ? timestamp.getTime() : -10));
				vo.setIsactive(rs.getBoolean("isactive"));
			}
			
			return vo;
			
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return vo;
	}


	/**
	 * Delete master question bank type.
	 *
	 * @param dvo the dvo
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer deleteMasterBankType( QuestionBankMasterDVO dvo) throws Exception {

		Connection conn = null;		
		PreparedStatement ps = null;
		Integer affectedRows = 0;
		StringBuffer sql = new StringBuffer();
		sql.append("update  sd_mst_question_bank_type set  isactive = ? ")
		.append(" ,updby =  ? ,upddate  = getDate()  ")
		.append(" where clintid =  ? and mstqsnbnkid = ? ");

		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setBoolean(1, Boolean.FALSE);
			ps.setString(2, dvo.getUpdby());
			ps.setString(3, dvo.getClintid());
			ps.setInt(4, dvo.getMstqsnbnkid());
			affectedRows = ps.executeUpdate();			
			return affectedRows;
		} catch (SQLException se) {
			se.printStackTrace();			
		} catch (Exception e) {
			e.printStackTrace();		
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return affectedRows;
	}
	
	

	/**
	 * Update master question bank type.
	 *
	 * @param dvo the dvo
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer updateMasterBankType( QuestionBankMasterDVO dvo) throws Exception {
	   
		Connection conn = null;		
		PreparedStatement ps = null;
		Integer affectedRows = 0;
		StringBuffer sql = new StringBuffer();
		sql.append("update  sd_mst_question_bank_type set  mstqsnbnk = ? ")
		.append(" , mstqsnbnkdesc = ? ")
		.append(" ,updby =  ? ,  upddate  = getDate()  ")
		.append(" where clintid =  ? and mstqsnbnkid = ? ");

		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, dvo.getMstqsnbnk());
			ps.setString(2, dvo.getMstqsnbnkdesc());
			ps.setString(3, dvo.getUpdby());
			ps.setString(4, dvo.getClintid());
			ps.setInt(5, dvo.getMstqsnbnkid());
			affectedRows = ps.executeUpdate();			
			return affectedRows;

		} catch (SQLException se) {
			se.printStackTrace();		
		} catch (Exception e) {
			e.printStackTrace();		
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return affectedRows;
	}
	
	/**
	 * Check if duplicate master question bank exists.
	 *
	 * @param dvo the dvo
	 * @return the boolean
	 */
	public static  Boolean  findDuplicateRecordByName( QuestionBankMasterDVO dvo) {
		
		
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		Boolean isRecordPresent = Boolean.FALSE;

		try {
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT mstqsnbnkid from sd_mst_question_bank_type  ")
			.append("with(nolock) where clintid = ? and mstqsnbnk = ?  and mstqsnbnkid != ? ");
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, dvo.getClintid());			
			ps.setString(2, dvo.getMstqsnbnk());
			ps.setInt(3, dvo.getMstqsnbnkid());
			rs = ps.executeQuery();		
			while (rs.next()) {
				isRecordPresent = Boolean.TRUE;			
			}
			return isRecordPresent;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return isRecordPresent;		 
	 }
}

