/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.sql.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.rtf.common.sql.dvo.CommonQuestionBankDVO;
import com.rtf.ott.sql.dvo.QuestionBankDVO;
import com.rtf.ott.util.StatusEnums;
import com.rtf.saas.sql.db.DatabaseConnections;
import com.rtf.saas.util.PaginationUtil;

/**
 * The Class CommonQuestionBankSQLDAO.
 */
public class CommonQuestionBankSQLDAO {

	/**
	 * Gets the all questions from question bank with filter.
	 *
	 * @param clientId    the client id
	 * @param pgNo        the pg no
	 * @param filterQuery the filter query
	 * @param mstBankId   the mst bank id
	 * @return the all questions from Q bank with filter
	 * @throws Exception the exception
	 */
	public static List<CommonQuestionBankDVO> getAllQuestionsFromQBankWithFilter(String clientId, String pgNo,
			String filterQuery, Integer mstBankId) throws Exception {

		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		List<CommonQuestionBankDVO> queList = null;

		try {
			StringBuffer sql = new StringBuffer();
			sql.append(" SELECT * from pa_commn_question_bank  with(nolock) where clintid = ? and isactive = ?  and mstqsnbnkid = ? ");
			String paginationQuery = PaginationUtil.getPaginationQuery(pgNo);
			if (filterQuery != null && !filterQuery.isEmpty()) {
				sql.append(filterQuery);
			}
			sql.append(" ORDER BY qsnbnkid desc ");
			sql.append(paginationQuery);
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			ps.setBoolean(2, Boolean.TRUE);
			ps.setInt(3, mstBankId);
			rs = ps.executeQuery();

			queList = new ArrayList<CommonQuestionBankDVO>();
			while (rs.next()) {
				Timestamp timestamp = null;
				CommonQuestionBankDVO vo = new CommonQuestionBankDVO();
				vo.setQsnbnkid(rs.getInt("qsnbnkid"));
				vo.setQtx(rs.getString("qsntext"));
				vo.setOpa(rs.getString("ansopta"));
				vo.setOpb(rs.getString("ansoptb"));
				vo.setOpc(rs.getString("ansoptc"));
				vo.setOpd(rs.getString("ansoptd"));
				vo.setAnstype(rs.getString("anstype"));
				vo.setCtyp(rs.getString("cattype"));
				vo.setSctyp(rs.getString("subcattype"));
				vo.setCans(rs.getString("corans"));
				vo.setqOrientn(rs.getString("qsnorient"));
				vo.setCreBy(rs.getString("creby"));
				vo.setUpdBy(rs.getString("updby"));
				timestamp = rs.getTimestamp("credate");
				vo.setCreDate(new java.util.Date(timestamp != null ? timestamp.getTime() : -10));
				timestamp = rs.getTimestamp("upddate");
				vo.setUpdDt(new java.util.Date(timestamp != null ? timestamp.getTime() : -10));
				vo.setIsActive(rs.getBoolean("isactive"));
				vo.setMstqbankId(rs.getInt("mstqsnbnkid"));
				queList.add(vo);

			}

			return queList;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;

	}

	/**
	 * Gets the all questions count from question bank with filter.
	 *
	 * @param clientId    the client id
	 * @param filterQuery the filter query
	 * @param mstBankId   the mst bank id
	 * @return the all questions count from Q bank with filter
	 * @throws Exception the exception
	 */
	public static Integer getAllQuestionsCountFromQBankWithFilter(String clientId, String filterQuery,
			Integer mstBankId) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Integer count = 0;
		try {

			StringBuffer sql = new StringBuffer();
			sql.append(" SELECT count(*) from pa_ofltx_question_bank  with(nolock) where clintid = ? and isactive = ? and mstqsnbnkid = ? ");
			if (filterQuery != null && !filterQuery.isEmpty()) {
				sql.append(filterQuery);
			}
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			ps.setString(2, StatusEnums.TRUE.name());
			ps.setInt(3, mstBankId);
			rs = ps.executeQuery();
			while (rs.next()) {
				count = rs.getInt(1);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return count;
	}

	/**
	 * Gets the all Offline text trivia questions from question bank by contest id with filter.
	 *
	 * @param clientId    the client id
	 * @param contestId   the contest id
	 * @param catType     the cat type
	 * @param subCatType  the sub cat type
	 * @param filterQuery the filter query
	 * @param pgNo        the pg no
	 * @return the all OLTX questions from Q bank by contest id with filter
	 * @throws Exception the exception
	 */
	public static List<QuestionBankDVO> getAllOLTXQuestionsFromQBankByContestIdWithFilter(String clientId,
			String contestId, String catType, String subCatType, String filterQuery, String pgNo) throws Exception {

		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		List<QuestionBankDVO> queList = null;
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT * from pa_ofltx_question_bank  with(nolock) where clintid = ? and isactive = ? ");
		if (catType != null) {
			sql.append(" and cattype = '" + catType + "'");
		}
		if (subCatType != null) {
			sql.append(" and subcattype = '" + subCatType + "'");
		}
		String paginationQuery = PaginationUtil.getPaginationQuery(pgNo);
		if (filterQuery != null && !filterQuery.isEmpty()) {
			sql.append(filterQuery);
		}
		sql.append(" and qsnbnkid not in (select qsnbnkid from pa_ofltx_contest_question with(nolock) where  clintid='")
				.append(clientId + "' and conid='" + contestId + "' and qsnbnkid is not null)")
				.append("  order by qsnbnkid desc")
				.append(paginationQuery);
		try {
			conn = DatabaseConnections.getSaasAdminConnection();

			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			ps.setString(2, StatusEnums.TRUE.name());
			rs = ps.executeQuery();
			queList = new ArrayList<QuestionBankDVO>();
			while (rs.next()) {
				QuestionBankDVO vo = new QuestionBankDVO();
				vo.setQsnbnkid(rs.getInt("qsnbnkid"));
				vo.setQtx(rs.getString("qsntext"));
				vo.setOpa(rs.getString("ansopta"));
				vo.setOpb(rs.getString("ansoptb"));
				vo.setOpc(rs.getString("ansoptc"));
				vo.setOpd(rs.getString("ansoptd"));
				vo.setAnstype(rs.getString("anstype"));
				vo.setCtyp(rs.getString("cattype"));
				vo.setSctyp(rs.getString("subcattype"));
				vo.setCans(rs.getString("corans"));
				vo.setqOrientn(rs.getString("qsnorient"));
				vo.setCreBy(rs.getString("creby"));
				vo.setUpdBy(rs.getString("updby"));
				vo.setCreDate(rs.getDate("credate"));
				vo.setUpdDt(rs.getDate("upddate"));
				vo.setIsActive(rs.getBoolean("isactive"));
				queList.add(vo);

			}

			return queList;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;

	}

	/**
	 * Gets the all Offline text trivia questions count from question bank by contest id with filter.
	 *
	 * @param clientId    the client id
	 * @param contestId   the contest id
	 * @param catType     the cat type
	 * @param subCatType  the sub cat type
	 * @param filterQuery the filter query
	 * @return the all OLTX questions count from Q bank by contest id with filter
	 * @throws Exception the exception
	 */
	public static Integer getAllOLTXQuestionsCountFromQBankByContestIdWithFilter(String clientId, String contestId,
			String catType, String subCatType, String filterQuery) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Integer count = 0;
		try {
			StringBuffer sql = new StringBuffer();
			sql.append(" SELECT count(*) from pa_ofltx_question_bank  with(nolock) where clintid = ? and isactive = ? ");
			if (catType != null) {
				sql.append(" and cattype = '" + catType + "'");
			}
			if (subCatType != null) {
				sql.append(" and subcattype = '" + subCatType + "'");
			}
			if (filterQuery != null && !filterQuery.isEmpty()) {
				sql.append(filterQuery);
			}
			sql.append(" and qsnbnkid not in (select qsnbnkid from pa_ofltx_contest_question with(nolock) where  clintid='")
					.append(clientId + "' and conid='" + contestId + "' and qsnbnkid is not null)");

			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			ps.setString(2, StatusEnums.TRUE.name());

			rs = ps.executeQuery();
			while (rs.next()) {
				count = rs.getInt(1);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return count;
	}

	/**
	 * Gets the Offline text trivia questions from question bank by question id.
	 *
	 * @param clientId the client id
	 * @param qbnkId   the qbnk id
	 * @return the OLTX questions from Q bank byquestion id
	 * @throws Exception the exception
	 */
	public static QuestionBankDVO getOLTXQuestionsFromQBankByquestionId(String clientId, Integer qbnkId)
			throws Exception {

		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		QuestionBankDVO vo = null;
		String sql = " SELECT * from pa_ofltx_question_bank  with(nolock) where clintid = ? and isactive = ? and qsnbnkid=?";

		try {
			conn = DatabaseConnections.getSaasAdminConnection();

			ps = conn.prepareStatement(sql);
			ps.setString(1, clientId);
			ps.setBoolean(2, Boolean.TRUE);
			ps.setInt(3, qbnkId);
			rs = ps.executeQuery();

			while (rs.next()) {
				vo = new QuestionBankDVO();
				vo.setQsnbnkid(rs.getInt("qsnbnkid"));
				vo.setQtx(rs.getString("qsntext"));
				vo.setOpa(rs.getString("ansopta"));
				vo.setOpb(rs.getString("ansoptb"));
				vo.setOpc(rs.getString("ansoptc"));
				vo.setOpd(rs.getString("ansoptd"));
				vo.setAnstype(rs.getString("anstype"));
				vo.setCtyp(rs.getString("cattype"));
				vo.setSctyp(rs.getString("subcattype"));
				vo.setCans(rs.getString("corans"));
				vo.setqOrientn(rs.getString("qsnorient"));
				vo.setCreBy(rs.getString("creby"));
				vo.setUpdBy(rs.getString("updby"));
				vo.setCreDate(rs.getDate("credate"));
				vo.setUpdDt(rs.getDate("upddate"));
				vo.setIsActive(rs.getBoolean("isactive"));

			}

			return vo;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;

	}

	/**
	 * Creates the new question in specified question bank.
	 *
	 * @param clientid the client id
	 * @param dvo      the dvo
	 * @return the common question bank DVO
	 * @throws Exception the exception
	 */
	public static CommonQuestionBankDVO createQuestion(String clientid, CommonQuestionBankDVO dvo) throws Exception {

		Connection conn = null;
		PreparedStatement ps = null;
		StringBuffer sql = new StringBuffer();
		sql.append(" insert into  pa_commn_question_bank (clintid,ansopta,ansoptb,ansoptc,ansoptd,anstype,")
		.append("cattype,corans,isactive,qsnorient,qsntext,subcattype,creby,credate,mstqsnbnkid,upddate,updby )")
		.append("values(?,?,?,?,?,?,?,?,?,?,?,?,?,getdate(),?,getdate(),?) ");
		try {
			conn = DatabaseConnections.getSaasAdminConnection();

			ps = conn.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, clientid);
			ps.setString(2, dvo.getOpa());
			ps.setString(3, dvo.getOpb());
			ps.setString(4, dvo.getOpc());
			ps.setString(5, dvo.getOpd());
			ps.setString(6, dvo.getAnstype());
			ps.setString(7, dvo.getCtyp());
			ps.setString(8, dvo.getCans());
			ps.setBoolean(9, Boolean.TRUE);
			ps.setString(10, dvo.getqOrientn());
			ps.setString(11, dvo.getQtx());
			ps.setString(12, dvo.getSctyp());
			ps.setString(13, dvo.getCreBy());
			ps.setInt(14, dvo.getMstqbankId());
			ps.setString(15, dvo.getCreBy());

			int affectedRows = ps.executeUpdate();
			if (affectedRows == 0) {
				throw new SQLException("Creating Question Bank record failed, no rows affected.");
			}
			ResultSet generatedKeys;
			try {
				generatedKeys = ps.getGeneratedKeys();
				if (generatedKeys.next()) {
					dvo.setQsnbnkid(generatedKeys.getInt(1));
				} else {
					throw new SQLException("Creating Bank record failed, no ID obtained.");
				}
				generatedKeys.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}

			return dvo;

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * Update question in specified question bank.
	 *
	 * @param clientid the clientid
	 * @param dvo      the dvo
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer updateQuestion(String clientid, CommonQuestionBankDVO dvo) throws Exception {

		Connection conn = null;
		PreparedStatement ps = null;
		Integer affectedRows = 0;
		StringBuffer sql = new StringBuffer();
		sql.append(" update  pa_commn_question_bank" + " 			set " + "   		ansopta = ? ")
		.append("           ,ansoptb = ? " + "           ,ansoptc = ? " + "           ,ansoptd = ? ")
		.append("           ,anstype = ? " + "           ,cattype = ? " + "           ,corans = ? ")
		.append("           ,isactive = ? " + "           ,qsnorient = ? " + "           ,qsntext = ? ")
		.append("           ,subcattype = ? " + "           ,updby =  ? " + "			,upddate  = getDate()   ")
		.append(" where clintid =  ? and qsnbnkid = ? and mstqsnbnkid = ?");
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, dvo.getOpa());
			ps.setString(2, dvo.getOpb());
			ps.setString(3, dvo.getOpc());
			ps.setString(4, dvo.getOpd());
			ps.setString(5, dvo.getAnstype());
			ps.setString(6, dvo.getCtyp());
			ps.setString(7, dvo.getCans());
			ps.setBoolean(8, dvo.getIsActive());
			ps.setString(9, dvo.getqOrientn());
			ps.setString(10, dvo.getQtx());
			ps.setString(11, dvo.getSctyp());
			ps.setString(12, dvo.getUpdBy());
			ps.setString(13, clientid);
			ps.setInt(14, dvo.getQsnbnkid());
			ps.setInt(15, dvo.getMstqbankId());
			affectedRows = ps.executeUpdate();
			if (affectedRows == 0) {
				throw new SQLException(
						"Updating Question Bank record failed, no rows affected. for QBID " + dvo.getQsnbnkid());
			}
			return affectedRows;

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return affectedRows;
	}

	/**
	 * Delete question in specified question bank.
	 *
	 * @param clientid the clientid
	 * @param dvo      the dvo
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer deleteQuestion(String clientid, CommonQuestionBankDVO dvo) throws Exception {

		Connection conn = null;
		PreparedStatement ps = null;
		Integer affectedRows = 0;
		StringBuffer sql = new StringBuffer();
		sql.append("update  pa_commn_question_bank" + " 			set " + "           isactive = ? ")
		.append("           ,updby =  ? " + "			,upddate  = getDate()  ")
		.append(" where clintid =  ? and qsnbnkid = ? and mstqsnbnkid = ?");

		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setBoolean(1, Boolean.FALSE);
			ps.setString(2, dvo.getUpdBy());
			ps.setString(3, clientid);
			ps.setInt(4, dvo.getQsnbnkid());
			ps.setInt(5, dvo.getMstqbankId());
			affectedRows = ps.executeUpdate();
			if (affectedRows == 0) {
				throw new SQLException(
						"DELETING Question Bank record failed, no rows affected. for QBID " + dvo.getQsnbnkid());
			}
			return affectedRows;

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return affectedRows;
	}

	/**
	 * Gets the question from question bank by id.
	 *
	 * @param clientId  the client id
	 * @param qId       the q id
	 * @param mstBankId the mst bank id
	 * @return the questions from Q bank by id
	 * @throws Exception the exception
	 */
	public static CommonQuestionBankDVO getQuestionsFromQBankById(String clientId, Integer qId, Integer mstBankId)
			throws Exception {

		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		CommonQuestionBankDVO vo = null;

		try {
			String sql = " SELECT * from pa_commn_question_bank   with(nolock) where clintid = ?  and mstqsnbnkid = ? and qsnbnkid = ? ";
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, clientId);
			ps.setInt(2, mstBankId);
			ps.setInt(3, qId);
			rs = ps.executeQuery();
			while (rs.next()) {
				vo = new CommonQuestionBankDVO();
				Timestamp timestamp = null;
				vo.setQsnbnkid(rs.getInt("qsnbnkid"));
				vo.setQtx(rs.getString("qsntext"));
				vo.setOpa(rs.getString("ansopta"));
				vo.setOpb(rs.getString("ansoptb"));
				vo.setOpc(rs.getString("ansoptc"));
				vo.setOpd(rs.getString("ansoptd"));
				vo.setAnstype(rs.getString("anstype"));
				vo.setCtyp(rs.getString("cattype"));
				vo.setSctyp(rs.getString("subcattype"));
				vo.setCans(rs.getString("corans"));
				vo.setqOrientn(rs.getString("qsnorient"));
				vo.setCreBy(rs.getString("creby"));
				vo.setUpdBy(rs.getString("updby"));
				timestamp = rs.getTimestamp("credate");
				vo.setCreDate(new java.util.Date(timestamp != null ? timestamp.getTime() : -10));
				timestamp = rs.getTimestamp("upddate");
				vo.setUpdDt(new java.util.Date(timestamp != null ? timestamp.getTime() : -10));
				vo.setIsActive(rs.getBoolean("isactive"));
				vo.setMstqbankId(rs.getInt("mstqsnbnkid"));

			}
			return vo;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;

	}

	/**
	 * Gets the questions count by category sub category and question type.
	 *
	 * @param clientId the client id
	 * @param category the category
	 * @param subCat   the sub cat
	 * @param qMode    the q mode
	 * @return the questions count by category subcategory and question type
	 */
	public static Integer getQuestionsCountByCategorySubcategoryAndQuestionType(String clientId, String category,
			String subCat, String qMode) {

		String saasAdminLinkedServer = DatabaseConnections.getSaasAdminLinkedServer(clientId) + ".dbo.";
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT  count(*) as qCount FROM "+ saasAdminLinkedServer)
		.append("pa_ofltx_question_bank where isactive=1 AND  clintid='" + clientId + "' and anstype = '" + qMode+"' ")
		.append(" AND cattype like '" + category + "' ");
		if (subCat != null && !subCat.isEmpty()) {
			sql.append(" AND subcattype like '" + subCat + "' ");
		}
		Integer qsize = null;
		try {

			Connection conn = DatabaseConnections.getSaasAdminConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql.toString());
			while (rs.next()) {
				qsize = rs.getInt("qCount");
			}
			rs.close();
			stmt.close();
			DatabaseConnections.closeConnection(conn);
			return qsize;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return qsize;
	}

	/**
	 * Gets the question bank data to export.
	 *
	 * @param clientId    the client id
	 * @param filterQuery the filter query
	 * @return the question bank data to export
	 * @throws Exception the exception
	 */
	public static List<QuestionBankDVO> getQuestionBankDataToExport(String clientId, String filterQuery)
			throws Exception {

		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		List<QuestionBankDVO> queList = null;

		try {
			StringBuffer sql = new StringBuffer();
			sql.append(" SELECT * from pa_ofltx_question_bank  with(nolock) where clintid = ? and isactive = ? ");
			if (filterQuery != null && !filterQuery.isEmpty()) {
				sql.append(filterQuery);
			}
			sql.append(" ORDER BY qsnbnkid desc ");
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			ps.setBoolean(2, Boolean.TRUE);
			rs = ps.executeQuery();

			queList = new ArrayList<QuestionBankDVO>();
			while (rs.next()) {
				Timestamp timestamp = null;
				QuestionBankDVO vo = new QuestionBankDVO();
				vo.setClintid(rs.getString("clintid"));
				vo.setQsnbnkid(rs.getInt("qsnbnkid"));
				vo.setQtx(rs.getString("qsntext"));
				vo.setOpa(rs.getString("ansopta"));
				vo.setOpb(rs.getString("ansoptb"));
				vo.setOpc(rs.getString("ansoptc"));
				vo.setOpd(rs.getString("ansoptd"));
				vo.setAnstype(rs.getString("anstype"));
				vo.setCtyp(rs.getString("cattype"));
				vo.setSctyp(rs.getString("subcattype"));
				vo.setCans(rs.getString("corans"));
				vo.setqOrientn(rs.getString("qsnorient"));
				vo.setCreBy(rs.getString("creby"));
				vo.setUpdBy(rs.getString("updby"));
				timestamp = rs.getTimestamp("credate");
				vo.setCreDate(new java.util.Date(timestamp != null ? timestamp.getTime() : -10));
				timestamp = rs.getTimestamp("upddate");
				vo.setUpdDt(new java.util.Date(timestamp != null ? timestamp.getTime() : -10));
				vo.setIsActive(rs.getBoolean("isactive"));
				queList.add(vo);

			}

			return queList;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;

	}

}
