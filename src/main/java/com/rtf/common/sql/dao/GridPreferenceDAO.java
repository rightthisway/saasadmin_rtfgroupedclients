/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.sql.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.rtf.common.sql.dvo.GridPreferenceDVO;
import com.rtf.saas.sql.db.DatabaseConnections;

/**
 * The Class GridPreferenceDAO.
 */
public class GridPreferenceDAO {

	/**
	 * Gets the user all grid preferences.
	 *
	 * @param clientId the client id
	 * @param userId   the user id
	 * @return the user all grid preferences
	 * @throws Exception the exception
	 */
	public static List<GridPreferenceDVO> getUserAllGridPreferences(String clientId, String userId) throws Exception {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		List<GridPreferenceDVO> list = null;
		String sql = " SELECT * from saas_prod_admin_user_preference  with(nolock) where clintid = ? and userid = ? ";
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, clientId);
			ps.setString(2, userId);
			rs = ps.executeQuery();

			list = new ArrayList<GridPreferenceDVO>();
			while (rs.next()) {
				GridPreferenceDVO vo = new GridPreferenceDVO();
				vo.setClId(rs.getString("clintid"));
				vo.setColumnString(rs.getString("column_orders"));
				vo.setGridName(rs.getString("grid_name"));
				vo.setId(rs.getInt("id"));
				vo.setUserId(rs.getString("userid"));
				list.add(vo);
			}
			return list;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * Gets the user grid preference.
	 *
	 * @param clientId the client id
	 * @param userId   the user id
	 * @param gridName the grid name
	 * @return the user grid preference
	 * @throws Exception the exception
	 */
	public static GridPreferenceDVO getUserGridPreference(String clientId, String userId, String gridName)
			throws Exception {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		String sql = " SELECT * from saas_prod_admin_user_preference with(nolock) where clintid = ? and userid = ? AND grid_name=?";
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, clientId);
			ps.setString(2, userId);
			ps.setString(3, gridName);
			rs = ps.executeQuery();
			GridPreferenceDVO vo = null;
			while (rs.next()) {
				vo = new GridPreferenceDVO();
				vo.setClId(rs.getString("clintid"));
				vo.setColumnString(rs.getString("column_orders"));
				vo.setGridName(rs.getString("grid_name"));
				vo.setId(rs.getInt("id"));
				vo.setUserId(rs.getString("userid"));
				return vo;
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * Save user grid preference.
	 *
	 * @param pref the pref
	 * @return the boolean
	 * @throws Exception the exception
	 */
	public static Boolean savePreference(GridPreferenceDVO pref) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		Boolean isInserted = false;
		String sql = " INSERT INTO saas_prod_admin_user_preference (clintid,userid,grid_name,column_orders) VALUES (?,?,?,?) ";
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, pref.getClId());
			ps.setString(2, pref.getUserId());
			ps.setString(3, pref.getGridName());
			ps.setString(4, pref.getColumnString());

			int affectedRows = ps.executeUpdate();
			if (affectedRows > 0) {
				isInserted = true;
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return isInserted;
	}

	/**
	 * Update user grid preference.
	 *
	 * @param pref the pref
	 * @return the boolean
	 * @throws Exception the exception
	 */
	public static Boolean updateGridPreference(GridPreferenceDVO pref) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		Boolean isUpd = false;
		String sql = " UPDATE  saas_prod_admin_user_preference set column_orders=? WHERE clintid=? AND userid=? AND grid_name=? ";
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, pref.getColumnString());
			ps.setString(2, pref.getClId());
			ps.setString(3, pref.getUserId());
			ps.setString(4, pref.getGridName());

			int affectedRows = ps.executeUpdate();
			if (affectedRows > 0) {
				isUpd = true;
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return isUpd;
	}

}
