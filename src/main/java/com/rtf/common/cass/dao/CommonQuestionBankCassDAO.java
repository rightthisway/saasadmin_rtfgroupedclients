/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.cass.dao;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.exceptions.DriverException;
import com.rtf.common.sql.dvo.CommonQuestionBankDVO;
import com.rtf.saas.cass.db.CassandraConnector;

/**
 * The Class CommonQuestionBankCassDAO.
 */
public class CommonQuestionBankCassDAO {

	
	
	/**
	 * Save new question bank object.
	 *
	 * @param dvo the dvo
	 * @return the boolean
	 * @throws Exception the exception
	 */
	public static Boolean saveQuestionBank(CommonQuestionBankDVO dvo) throws Exception {
		boolean isInserted = false;
		Session session = CassandraConnector.getSession();
		StringBuffer sql = new StringBuffer();
		sql.append(" insert into pa_commn_question_bank  " )
		.append(" (clintid,cattype,subcattype,qsntext  ")
		.append(",ansopta,ansoptb,ansoptc,ansoptd , anstype " )
		.append(" ,corans ,qsnbnkid,qsnorient,mstqsnbnkid) ")
		.append(" values (?,?,?,?,?,?,?,?,?,?,?,?,?)");
	try { 	
		PreparedStatement statement = session.prepare(sql.toString());
		BoundStatement boundStatement = statement.bind(				
			dvo.getClintid(),
			dvo.getCtyp(),
			dvo.getSctyp(),
			dvo.getQtx() ,			
			dvo.getOpa(),
			dvo.getOpb(),
			dvo.getOpc(), 
			dvo.getOpd(), 
			dvo.getAnstype(),			
			dvo.getCans(),			
			dvo.getQsnbnkid(),
			dvo.getqOrientn(),
			dvo.getMstqbankId()
				);
		session.execute(boundStatement);			 				
			isInserted = true;
			} catch (final DriverException de) {
				de.printStackTrace();
				isInserted = false;				
			}catch(Exception ex) {
				ex.printStackTrace();			
				isInserted = false;	
			}
			finally {		
			}
			return isInserted;
		}
	

	/**
	 * Update the question bank.
	 *
	 * @param dvo the dvo
	 * @return the boolean
	 * @throws Exception the exception
	 */
	public static Boolean updateQuestionBank(CommonQuestionBankDVO dvo) throws Exception {
		boolean isInserted = false;
		Session session = CassandraConnector.getSession();
		StringBuffer sql = new StringBuffer();
		sql.append(" update  pa_commn_question_bank " )	
		.append(" set ansopta=? ,ansoptb=?,ansoptc=?,ansoptd=? , anstype=? , qsntext = ? " )
		.append(" , corans=? ,qsnorient = ? where  ")
		.append(" clintid = ? and cattype=? and subcattype= ? and qsnbnkid = ? " ) ;
		 
	try { 	
		PreparedStatement statement = session.prepare(sql.toString());
		BoundStatement boundStatement = statement.bind(			
					
			dvo.getOpa(),
			dvo.getOpb(),
			dvo.getOpc(), 
			dvo.getOpd(), 
			dvo.getAnstype(),	
			dvo.getQtx() ,
			dvo.getCans(),
			dvo.getqOrientn() , 
			dvo.getClintid(),
			dvo.getCtyp(),
			dvo.getSctyp(),	
			dvo.getQsnbnkid()
				
				);
		session.execute(boundStatement);			 				
			isInserted = true;
			} catch (final DriverException de) {
				de.printStackTrace();
				isInserted = false;			
			}catch(Exception ex) {
				ex.printStackTrace();			
				isInserted = false;	
			}
			finally {		
			}
			return isInserted;
		}
	
	
	/**
	 * Delete the question bank.
	 *
	 * @param dvo the dvo
	 * @return the boolean
	 * @throws Exception the exception
	 */
	public static Boolean deleteQuestionBank(CommonQuestionBankDVO dvo) throws Exception {
		boolean isInserted = false;
		Session session = CassandraConnector.getSession();
		StringBuffer sql = new StringBuffer();
		sql.append(" delete from  pa_commn_question_bank " )
		.append("  where  ")
		.append(" clintid = ? and cattype=? and subcattype= ? and qsnbnkid = ? and mstqsnbnkid = ? and anstype=? " ) ;
		
	try { 	
		PreparedStatement statement = session.prepare(sql.toString());
		BoundStatement boundStatement = statement.bind(
			dvo.getClintid(),
			dvo.getCtyp(),
			dvo.getSctyp(),	
			dvo.getQsnbnkid(),
			dvo.getMstqbankId(),
			dvo.getAnstype()
				);
		session.execute(boundStatement);			 				
			isInserted = true;
			} catch (final DriverException de) {
				de.printStackTrace();
				isInserted = false;			
			}catch(Exception ex) {
				ex.printStackTrace();			
				isInserted = false;	
			}
			finally {			
			}
			return isInserted;
		}
		

	
}
