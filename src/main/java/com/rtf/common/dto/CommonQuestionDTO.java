/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.dto;

import java.util.List;

import com.rtf.common.sql.dvo.CommonQuestionBankDVO;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.dto.PaginationDTO;

/**
 * The Class CommonQuestionDTO.
 */
public class CommonQuestionDTO extends RtfSaasBaseDTO {

	/** The CommonQuestionBankDVO. */
	private CommonQuestionBankDVO dvo;

	/** The CommonQuestionBankDVO list qbList. */
	private List<CommonQuestionBankDVO> qbList;

	/** The PaginationDTO pagination. */
	private PaginationDTO pagination;

	/**
	 * Gets the CommonQuestionBankDVO list.
	 *
	 * @return the qb list
	 */
	public List<CommonQuestionBankDVO> getQbList() {
		return qbList;
	}

	/**
	 * Sets the CommonQuestionBankDVO list.
	 *
	 * @param qbList the new qb list
	 */
	public void setQbList(List<CommonQuestionBankDVO> qbList) {
		this.qbList = qbList;
	}

	/** The hme. */
	private Boolean hme;

	/**
	 * Gets the hme.
	 *
	 * @return the hme
	 */
	public Boolean getHme() {
		if (hme == null) {
			hme = false;
		}
		return hme;

	}

	/**
	 * Sets the hme.
	 *
	 * @param hme the new hme
	 */
	public void setHme(Boolean hme) {
		this.hme = hme;
	}

	/**
	 * Gets the PaginationDTO.
	 *
	 * @return the pagination
	 */
	public PaginationDTO getPagination() {
		return pagination;
	}

	/**
	 * Sets the PaginationDTO.
	 *
	 * @param pagination the new pagination
	 */
	public void setPagination(PaginationDTO pagination) {
		this.pagination = pagination;
	}

	/**
	 * Gets the CommonQuestionBankDVO.
	 *
	 * @return the dvo
	 */
	public CommonQuestionBankDVO getDvo() {
		return dvo;
	}

	/**
	 * Sets the CommonQuestionBankDVO.
	 *
	 * @param dvo the new dvo
	 */
	public void setDvo(CommonQuestionBankDVO dvo) {
		this.dvo = dvo;
	}

}
