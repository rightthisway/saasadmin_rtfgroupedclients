/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.dto;

import com.rtf.common.sql.dvo.GridPreferenceDVO;
import com.rtf.saas.arch.RtfSaasBaseDTO;

/**
 * The Class GridPreferenceDTO.
 */
public class GridPreferenceDTO extends RtfSaasBaseDTO {

	/** The GridPreferenceDVO preference. */
	private GridPreferenceDVO preference;

	/**
	 * Gets the GridPreferenceDVO preference.
	 *
	 * @return the preference
	 */
	public GridPreferenceDVO getPreference() {
		return preference;
	}

	/**
	 * Sets the GridPreferenceDVO preference.
	 *
	 * @param preference the new preference
	 */
	public void setPreference(GridPreferenceDVO preference) {
		this.preference = preference;
	}

}
