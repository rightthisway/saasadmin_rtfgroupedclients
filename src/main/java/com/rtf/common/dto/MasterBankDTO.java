/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.dto;

import java.util.List;

import com.rtf.common.sql.dvo.QuestionBankMasterDVO;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.dto.PaginationDTO;

/**
 * The Class MasterBankDTO.
 */
public class MasterBankDTO extends RtfSaasBaseDTO {

	/** The QuestionBankMasterDVO dvo. */
	private QuestionBankMasterDVO dvo;

	/** The PaginationDTO pagination. */
	private PaginationDTO pagination;

	/** The QuestionBankMasterDVO list. */
	private List<QuestionBankMasterDVO> mqblist;

	/**
	 * Gets the QuestionBankMasterDVO.
	 *
	 * @return the dvo
	 */
	public QuestionBankMasterDVO getDvo() {
		return dvo;
	}

	/**
	 * Sets the QuestionBankMasterDVO.
	 *
	 * @param dvo the new dvo
	 */
	public void setDvo(QuestionBankMasterDVO dvo) {
		this.dvo = dvo;
	}

	/**
	 * Gets the PaginationDTO.
	 *
	 * @return the pagination
	 */
	public PaginationDTO getPagination() {
		return pagination;
	}

	/**
	 * Sets the PaginationDTO.
	 *
	 * @param pagination the new pagination
	 */
	public void setPagination(PaginationDTO pagination) {
		this.pagination = pagination;
	}

	/**
	 * Gets the QuestionBankMasterDVO list.
	 *
	 * @return the mqblist
	 */
	public List<QuestionBankMasterDVO> getMqblist() {
		return mqblist;
	}

	/**
	 * Sets the QuestionBankMasterDVO list.
	 *
	 * @param mqblist the new mqblist
	 */
	public void setMqblist(List<QuestionBankMasterDVO> mqblist) {
		this.mqblist = mqblist;
	}

}
