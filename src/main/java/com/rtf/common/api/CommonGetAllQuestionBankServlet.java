/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.common.dto.CommonQuestionDTO;
import com.rtf.common.service.CommonQuestionBankService;
import com.rtf.common.sql.dvo.CommonQuestionBankDVO;
import com.rtf.common.util.MessageConstant;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.Constant;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;
import com.rtf.saas.util.SAASMessages;

/**
 * The Class CommonGetAllQuestionBankServlet.
 */
@WebServlet("/commgetallquebank.json")
public class CommonGetAllQuestionBankServlet extends RtfSaasBaseServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1572791317046245768L;

	/**
	 * Generate Http Response for Client.Response, data is sent in JSON format.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * Get all question for specified master question bank
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String clId = request.getParameter("clId");
		String sts = request.getParameter("sts");
		String pgNo = request.getParameter("pgNo");
		String filter = request.getParameter("hfilter");
		String mstqbnkid = request.getParameter("mqbId");
		Integer mqbId = null;
		CommonQuestionDTO respDTO = new CommonQuestionDTO();
		respDTO.setSts(0);
		respDTO.setClId(clId);

		try {

			if (GenUtil.isNullOrEmpty(mstqbnkid)) {
				setClientMessage(respDTO, MessageConstant.INVALID_QUESTION_BANK, null);
				generateResponse(request, response, respDTO);
				return;
			}

			try {
				mqbId = Integer.parseInt(mstqbnkid);
			} catch (Exception ex) {
				setClientMessage(respDTO, MessageConstant.INVALID_QUESTION_BANK, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (GenUtil.isNullOrEmpty(clId)) {
				setClientMessage(respDTO, MessageConstant.INVALID_CLIENT, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (GenUtil.isNullOrEmpty(sts)) {
				setClientMessage(respDTO, MessageConstant.INVALID_QUESTION_BANK_STATUS, null);
				generateResponse(request, response, respDTO);
				return;
			}

			if (GenUtil.isNullOrEmpty(pgNo)) {
				pgNo = "1";
			}

			respDTO = CommonQuestionBankService.fetchQuestionBankList(clId, respDTO, pgNo, filter, mqbId);
			if (respDTO.getSts() == 0) {
				if (respDTO.getMsg() == null || respDTO.getMsg().isEmpty()) {
					setClientMessage(respDTO, null, null);
				}
				generateResponse(request, response, respDTO);
				return;
			}

			List<CommonQuestionBankDVO> qList = respDTO.getQbList();
			if (qList.size() >= Constant.NoOfRecordsPerScreen) {
				respDTO.setHme(Boolean.TRUE);
			}

			if (respDTO.getSts() == 1) {
				if (qList == null || qList.size() == 0)
					respDTO.setMsg(SAASMessages.ADMIN_QB_NOQUESTIONS);
			}
			generateResponse(request, response, respDTO);
			return;

		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(respDTO, null, null);
			generateResponse(request, response, respDTO);
		}
		return;
	}

}
