/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.common.util.CommonCacheUtil;
import com.rtf.common.util.MessageConstant;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class LoadApplicationValuesServlet.
 */
@WebServlet(urlPatterns = "/loadAppsettingscache.json", loadOnStartup = 1)
public class LoadApplicationValuesServlet extends HttpServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -9181759283202458927L;

	/**
	 * Instantiates a new load application values servlet.
	 */
	public LoadApplicationValuesServlet() {
		super();
	}

	/**
	 * This method get called on start up of application to load application configuration values.
	 *
	 * @throws ServletException the servlet exception
	 */
	@Override
	public void init() throws ServletException {
		try {
			executeLoadApppValues();
		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	/**
	 * Do get.
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		process(request, response);
	}

	/**
	 * Do post.
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		process(request, response);
	}

	/**
	 * Execute load appp values.
	 *
	 * @throws Exception the exception
	 */
	public static void executeLoadApppValues() throws Exception {
		CommonCacheUtil.loadApplicationValues();
	}

	/**
	 * Loads application configuration values and set it on startup of the application.
	 *
	 * @param request  the request
	 * @param response the response
	 * @return the http servlet response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String resMsg = "";
		try {
			executeLoadApppValues();
			resMsg = MessageConstant.APP_CACHE_LOADED;
		} catch (Exception e) {
			resMsg = MessageConstant.APP_CACHE_LOADING_ERROR;
			e.printStackTrace();
			generateResponse(response, resMsg);
			return response;

		} finally {
		}
		generateResponse(response, resMsg);
		return response;
	}

	/**
	 * Generate Http Response for Client.Response, data is sent in JSON format.
	 *
	 * @param response       the response
	 * @param commonRespInfo the common resp info
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	public void generateResponse(HttpServletResponse response, String commonRespInfo)
			throws ServletException, IOException {
		Map<String, String> map = new HashMap<String, String>();
		map.put("commonRespInfo", commonRespInfo);
		String jsondashboardInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		out.print(jsondashboardInfo);
		out.flush();
	}
}
