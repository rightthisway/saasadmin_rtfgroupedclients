/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.common.service.NodeCacheService;
import com.rtf.common.util.MessageConstant;
import com.rtf.livt.dto.LivtCommonDTO;
import com.rtf.livt.dvo.ContestDVO;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.service.SaaSApiTrackingService;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class LiveUpdateFireStoreConfigNodeCacheServlet.
 */
@WebServlet("/updfstorecache.json")
public class LiveUpdateFireStoreConfigNodeCacheServlet extends RtfSaasBaseServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 141312500L;

	/**
	 * Generate Http Response for Client.Response, data is sent in JSON format.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * Update firestore configured node cached data on client API
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String clId = request.getParameter("clId");
		LivtCommonDTO respDTO = new LivtCommonDTO();
		respDTO.setSts(0);
		String msg = null;
		ContestDVO contest = null;
		try {
			if (clId == null || clId.isEmpty()) {
				msg = MessageConstant.INVALID_CLIENT;
				setClientMessage(respDTO, msg, null);
				generateResponse(request, response, respDTO);
				return;
			}
			respDTO.setClId(clId);
			respDTO = NodeCacheService.updateNodeCacheData(respDTO);
		} catch (Exception e) {
			msg = MessageConstant.FIRESTORE_CACHE_ERROR;
			e.printStackTrace();
			setClientMessage(respDTO, null, null);
			generateResponse(request, response, respDTO);
		} finally {
			SaaSApiTrackingService.saveContestTracking(clId, "", "FSCACHENODEUPDATE", "admin", "WEB",
					request.getRemoteAddr(), "FSCACHENODEUPDATE", msg, respDTO.getSts(),
					contest != null ? contest.getLastQue() : 0);
		}
		return;
	}
}
