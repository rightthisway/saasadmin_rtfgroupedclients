/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import com.rtf.common.service.CommonQuestionBankService;
import com.rtf.common.util.MessageConstant;
import com.rtf.ott.sql.dvo.QuestionBankDVO;
import com.rtf.reports.utils.ExcelUtil;
import com.rtf.reports.utils.ReportConstants;
import com.rtf.reports.utils.ReportsUtil;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class CommonExportQuestionBankToExcelServlet.
 */
@WebServlet("/comngetquebankexcel.json")
public class CommonExportQuestionBankToExcelServlet extends RtfSaasBaseServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 443198847627011832L;

	/**
	 * Generate Http Response for Client.Response, data is sent in JSON format.
	 *
	 * @param request the request
	 * @param response the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * Generate Http Response for Client.Response, with excel file
	 *
	 * @param response the response
	 * @param workbook the workbook
	 * @throws Exception the exception
	 */
	public static void generateExcelResponse(HttpServletResponse response,SXSSFWorkbook workbook) throws Exception {
		response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition","attachment; filename=Question_Bank." + ReportsUtil.EXCEL_EXTENSION); 
		workbook.write(response.getOutputStream());
		workbook.close();
	}
	
	/**
	 * Generate excel file of questions from common question bank.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
		String clId = request.getParameter("clId");
		String filter = request.getParameter("hfilter");
		String userName = request.getParameter("cau");
		
		
		int startRowCnt = 0;
		RtfSaasBaseDTO respDTO = new RtfSaasBaseDTO();
		respDTO.setSts(0);		
		
		try {
			if(GenUtil.isNullOrEmpty(clId)) {		
				setClientMessage(respDTO , MessageConstant.INVALID_CLIENT , null);
				generateResponse(request, response , respDTO );
				return;
			}
			if(GenUtil.isNullOrEmpty(userName)) {			
				setClientMessage(respDTO , MessageConstant.INVALID_USER , null);
				generateResponse(request, response , respDTO );
				return;
			}
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet = workbook.createSheet("Question_Bank");
			
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			List<String> headerList = new ArrayList<String>();
			headerList = new ArrayList<String>();
			headerList.add("Client ID");
			headerList.add("Question ID");
			headerList.add("Question Text");
			headerList.add("Option A");
			headerList.add("Option B");
			headerList.add("Option C");
			headerList.add("Option D");
			headerList.add("Correct Answer");
			headerList.add("Nature Of Question");
			headerList.add("Category");
			headerList.add("Sub Category");
			headerList.add("Display Orientation");
			headerList.add("Created By");
			headerList.add("Created Date");
			headerList.add("Updated By");
			headerList.add("Updated Date");
			ExcelUtil.generateExcelHeaderRowWithStyles(headerList, ssSheet, workbook, startRowCnt);
			
			List<QuestionBankDVO> qbList = CommonQuestionBankService.getQuestionBankDataToExport(clId, filter);
			if(qbList==null || qbList.isEmpty()){
				ExcelUtil.generateErrorMessageExcel(ssSheet, workbook, startRowCnt++, ReportConstants.NO_RECS_FOUND);
				generateExcelResponse(response, workbook);
				return;
			}
			
			int j = 0;
			int rowCount = startRowCnt + 1;
			for (QuestionBankDVO dvo : qbList) {
				Row rowhead = ssSheet.createRow((int) rowCount);
				rowCount++;
				j = 0;
				ReportsUtil.getExcelStringCell(dvo.getClintid(), j, rowhead);
				j++;
				ReportsUtil.getExcelIntegerCell(dvo.getQsnbnkid(), j, rowhead);
				j++;
				ReportsUtil.getExcelStringCell(dvo.getQtx(), j, rowhead);
				j++;
				ReportsUtil.getExcelStringCell(dvo.getOpa(), j, rowhead);
				j++;
				ReportsUtil.getExcelStringCell(dvo.getOpb(), j, rowhead);
				j++;
				ReportsUtil.getExcelStringCell(dvo.getOpc(), j, rowhead);
				j++;
				ReportsUtil.getExcelStringCell(dvo.getOpd(), j, rowhead);
				j++;
				ReportsUtil.getExcelStringCell(dvo.getCans(), j, rowhead);
				j++;
				ReportsUtil.getExcelStringCell(dvo.getAnstype(), j, rowhead);
				j++;
				ReportsUtil.getExcelStringCell(dvo.getCtyp(), j, rowhead);
				j++;
				ReportsUtil.getExcelStringCell(dvo.getSctyp(), j, rowhead);
				j++;
				ReportsUtil.getExcelStringCell(dvo.getqOrientn(), j, rowhead);
				j++;
				ReportsUtil.getExcelStringCell(dvo.getCreBy(), j, rowhead);
				j++;
				ReportsUtil.getExcelDateCell(dvo.getCrDateTimeStr(), j, rowhead,cellStyleWithHourMinute);
				j++;
				ReportsUtil.getExcelStringCell(dvo.getUpdBy(), j, rowhead);
				j++;
				ReportsUtil.getExcelDateCell(dvo.getUpDateTimeStr(), j, rowhead,cellStyleWithHourMinute);
				j++;
			}
			generateExcelResponse(response, workbook);
			return;
		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(respDTO , null , null);
			generateResponse(request, response , respDTO );
		}
		return;
	}
	
	
}
