/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.reports.utils;

/**
 * The Class ReportConstants.
 */
public class ReportConstants {

	/** The no recs found. */
	public static String NO_RECS_FOUND = "No Records Found";

	/** The invalid client id. */
	public static String INVALID_CLIENT_ID = "Client Id is Invalid";

	/** The invalid contest id. */
	public static String INVALID_CONTEST_ID = "Contest Id is Invalid";

	/** The invalid user id. */
	public static String INVALID_USER_ID = "User Id is Invalid";

	/** The processing error. */
	public static String PROCESSING_ERROR = "Somethinbg went wrong while Generating Report";

	/** The grandwinner by contest id report. */
	public static String GRANDWINNER_BY_CONTEST_ID_REPORT = "Contest Grand Winners Report";

	/** The winner by contest id report. */
	public static String WINNER_BY_CONTEST_ID_REPORT = "Contest Winners Report";

	/** The customer contest stats for business report. */
	public static String CUSTOMER_CONTEST_STATS_FOR_BUSINESS_REPORT = "Customer Statistics For Business Report";

	/** The customer contest stats for business report fname. */
	public static String CUSTOMER_CONTEST_STATS_FOR_BUSINESS_REPORT_FNAME = "CustStatsForBusiness";

	/** The customer contest stats for business report sheetname. */
	public static String CUSTOMER_CONTEST_STATS_FOR_BUSINESS_REPORT_SHEETNAME = "CuStatsForBusiness";

	/** The monthlycustomer ptcp cnt brkdown stats report. */
	public static String MONTHLYCUSTOMER_PTCP_CNT_BRKDOWN_STATS_REPORT = "Monthly Participants Statistics Report";

	/** The monthlycustomer ptcp cnt brkdown stats report fname. */
	public static String MONTHLYCUSTOMER_PTCP_CNT_BRKDOWN_STATS_REPORT_FNAME = "MonthlyCustomerParticipantsCountBreakdownStatisticsReport";

	/** The monthlycustomer ptcp cnt brkdown stats report sheetname. */
	public static String MONTHLYCUSTOMER_PTCP_CNT_BRKDOWN_STATS_REPORT_SHEETNAME = "ParticipantsStatistics";

}
