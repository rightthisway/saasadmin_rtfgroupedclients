/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.reports.utils;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddress;

/**
 * The Class ReportsUtil.
 */
public class ReportsUtil {

	/** The df 2. */
	private static DecimalFormat df2 = new DecimalFormat(".##");

	/** The Constant CSV_EXTENSION. */
	public static final String CSV_EXTENSION = "csv";

	/** The Constant EXCEL_EXTENSION. */
	public static final String EXCEL_EXTENSION = "xlsx";

	/** The Constant NEW_LINE_DELIMITER. */
	public static final String NEW_LINE_DELIMITER = "\n";

	/**
	 * Gets the rounded value.
	 *
	 * @param value the value
	 * @return the rounded value
	 * @throws Exception the exception
	 */
	public static Double getRoundedValue(Double value) throws Exception {
		df2.setRoundingMode(RoundingMode.DOWN);
		return Double.valueOf(df2.format(value));
	}

	/**
	 * Gets the rounded value string.
	 *
	 * @param value the value
	 * @return the rounded value string
	 * @throws Exception the exception
	 */
	public static String getRoundedValueString(Double value) throws Exception {
		return String.format("%.2f", value);
	}

	/**
	 * Gets the excel string cell.
	 *
	 * @param o       the o
	 * @param j       the j
	 * @param rowhead the rowhead
	 * @return the excel string cell
	 */
	public static void getExcelStringCell(Object o, int j, Row rowhead) {
		try {
			if (o != null) {
				rowhead.createCell((int) j).setCellValue(o.toString());
			} else {
				rowhead.createCell((int) j).setCellValue("");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Gets the excel string cell red.
	 *
	 * @param o       the o
	 * @param j       the j
	 * @param rowhead the rowhead
	 * @param style   the style
	 * @return the excel string cell red
	 */
	public static void getExcelStringCellRed(Object o, int j, Row rowhead, CellStyle style) {
		try {
			if (o != null) {
				Cell cell = rowhead.createCell((int) j);
				cell.setCellValue(o.toString());
				cell.setCellStyle(style);
			} else {
				Cell cell = rowhead.createCell((int) j);
				cell.setCellValue("");
				cell.setCellStyle(style);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Gets the excel date cell.
	 *
	 * @param o       the o
	 * @param j       the j
	 * @param rowhead the rowhead
	 * @param style   the style
	 * @return the excel date cell
	 */
	public static void getExcelDateCell(Object o, int j, Row rowhead, CellStyle style) {
		try {

			if (o != null) {
				Cell cell = rowhead.createCell((int) j);
				cell.setCellValue(o.toString());
				cell.setCellStyle(style);
			} else {
				rowhead.createCell((int) j).setCellValue("");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Gets the excel decimal cell.
	 *
	 * @param o       the o
	 * @param j       the j
	 * @param rowhead the rowhead
	 * @return the excel decimal cell
	 */
	public static void getExcelDecimalCell(Object o, int j, Row rowhead) {
		try {
			if (o != null) {
				rowhead.createCell((int) j)
						.setCellValue(ReportsUtil.getRoundedValue(Double.parseDouble(o.toString().trim())));
			} else {
				rowhead.createCell((int) j).setCellValue("");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Gets the excel decimal cell red.
	 *
	 * @param o       the o
	 * @param j       the j
	 * @param rowhead the rowhead
	 * @param style   the style
	 * @return the excel decimal cell red
	 */
	public static void getExcelDecimalCellRed(Object o, int j, Row rowhead, CellStyle style) {
		try {
			if (o != null) {
				Cell cell = rowhead.createCell((int) j);
				cell.setCellValue(ReportsUtil.getRoundedValue(Double.parseDouble(o.toString().trim())));
				cell.setCellStyle(style);
			} else {
				Cell cell = rowhead.createCell((int) j);
				cell.setCellValue("");
				cell.setCellStyle(style);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Gets the excel header cell with BG.
	 *
	 * @param o       the o
	 * @param j       the j
	 * @param rowhead the rowhead
	 * @param style   the style
	 * @return the excel header cell with BG
	 */
	public static void getExcelHeaderCellWithBG(Object o, int j, Row rowhead, CellStyle style) {
		try {
			Cell cell = rowhead.createCell((int) j);
			cell.setCellValue(o.toString());
			cell.setCellStyle(style);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Gets the excel integer cell.
	 *
	 * @param o       the o
	 * @param j       the j
	 * @param rowhead the rowhead
	 * @return the excel integer cell
	 */
	public static void getExcelIntegerCell(Object o, int j, Row rowhead) {
		try {
			if (o != null) {
				rowhead.createCell((int) j).setCellValue(Integer.parseInt(o.toString().trim()));
			} else {
				rowhead.createCell((int) j).setCellValue("");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Gets the merged region.
	 *
	 * @param c the c
	 * @return the merged region
	 */
	public static CellRangeAddress getMergedRegion(Cell c) {
		try {
			Sheet sheet = c.getSheet();
			for (CellRangeAddress range : sheet.getMergedRegions()) {
				if (range.isInRange(c.getRowIndex(), c.getColumnIndex())) {
					return range;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Gets the hour from date.
	 *
	 * @param date the date
	 * @return the hour from date
	 */
	public static String getHourFromDate(Date date) {
		try {
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			int hour = cal.get(Calendar.HOUR_OF_DAY);
			return String.valueOf(hour);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Gets the minutes from date.
	 *
	 * @param date the date
	 * @return the minutes from date
	 */
	public static String getMinutesFromDate(Date date) {
		try {
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			int minutes = cal.get(Calendar.MINUTE);
			return String.valueOf(minutes);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}