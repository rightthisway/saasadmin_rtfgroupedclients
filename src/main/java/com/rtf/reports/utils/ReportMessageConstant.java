package com.rtf.reports.utils;

public class ReportMessageConstant {
	
	
	public static final String INVALID_CONTEST_ID = "Invalid Contest id.";
	
	public static final String INVALID_FROM_DATE = "Invalid From Date.";
	
	public static final String INVALID_TO_DATE = "Invalid To Date.";
	
	

}
