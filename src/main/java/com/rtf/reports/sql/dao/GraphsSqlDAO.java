/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.reports.sql.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.rtf.reports.dvo.CustEngageGrowthPercGrpDVO;
import com.rtf.reports.dvo.CustEngageMonthWiseGrpDVO;
import com.rtf.reports.dvo.NoOfShoppableGamesGrpDVO;
import com.rtf.reports.dvo.NoOfTriviaGamesGrpDVO;
import com.rtf.reports.dvo.PartTrivEngageGrpDVO;
import com.rtf.reports.dvo.ParticipantsShopEngageGrpDVO;
import com.rtf.reports.dvo.ParticipantsTrivEngageGrpDVO;

/**
 * The Class GraphsSqlDAO.
 */
public class GraphsSqlDAO {

	/**
	 * Gets the cust engage month wise graphs data.
	 *
	 * @param clientId
	 *            the client id
	 * @param conn
	 *            the conn
	 * @return the cust engage month wise graphs data
	 * @throws Exception
	 *             the exception
	 */
	public static List<CustEngageMonthWiseGrpDVO> getCustEngageMonthWiseGraphsData(String clientId, Connection conn)
			throws Exception {
		List<CustEngageMonthWiseGrpDVO> list = new ArrayList<CustEngageMonthWiseGrpDVO>();

		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer sql = new StringBuffer();
		sql.append("select conyearmnthstr, viewers  as passiveViewers, players, finishers as finalists, winners")
				.append(" from grh_rep_ason_month_customer_engagements where clintid=? order by conyear, conmnth");
		try {

			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			rs = ps.executeQuery();
			CustEngageMonthWiseGrpDVO obj = null;
			while (rs.next()) {
				obj = new CustEngageMonthWiseGrpDVO();
				obj.setName(rs.getString("conyearmnthstr"));
				obj.setPassiveViewers(rs.getInt("passiveViewers"));
				obj.setPlayers(rs.getInt("players"));
				obj.setFinalists(rs.getInt("finalists"));
				obj.setWinners(rs.getInt("winners"));
				list.add(obj);
			}

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return list;
	}

	/**
	 * Gets the no of trivia games graphs data.
	 *
	 * @param clientId
	 *            the client id
	 * @param conn
	 *            the conn
	 * @return the no of trivia games graphs data
	 * @throws Exception
	 *             the exception
	 */
	public static List<NoOfTriviaGamesGrpDVO> getNoOfTriviaGamesGraphsData(String clientId, Connection conn)
			throws Exception {
		List<NoOfTriviaGamesGrpDVO> list = new ArrayList<NoOfTriviaGamesGrpDVO>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer sql = new StringBuffer("select triviaGames,  noofTriviaGames  from grh_rep_noof_trivia_games with(nolock) where clintid=? order by triviaGames;");
		try {
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			rs = ps.executeQuery();
			NoOfTriviaGamesGrpDVO obj = null;
			while (rs.next()) {
				obj = new NoOfTriviaGamesGrpDVO();
				obj.setName(rs.getString("triviaGames"));
				obj.setNotrivs(rs.getInt("noofTriviaGames"));
				list.add(obj);
			}

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return list;
	}

	/**
	 * Gets the participants trivia engage graphs data.
	 *
	 * @param clientId
	 *            the client id
	 * @param conn
	 *            the conn
	 * @return the participants trivia engage graphs data
	 * @throws Exception
	 *             the exception
	 */
	public static List<ParticipantsTrivEngageGrpDVO> getParticipantsTriviaEngageGraphsData(String clientId,
			Connection conn) throws Exception {
		List<ParticipantsTrivEngageGrpDVO> list = new ArrayList<ParticipantsTrivEngageGrpDVO>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer sql = new StringBuffer("select triviaGames, pctviewers, pctplayers, pctfinishers from grh_rep_noof_trivia_games with(nolock) where clintid=? order by triviaGames;");
		try {
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			rs = ps.executeQuery();

			ParticipantsTrivEngageGrpDVO obj = null;
			while (rs.next()) {
				obj = new ParticipantsTrivEngageGrpDVO();
				obj.setName(rs.getString("triviaGames"));
				obj.setPassiveViewers(rs.getDouble("pctviewers"));
				obj.setPlayers(rs.getDouble("pctplayers"));
				obj.setFinishers(rs.getDouble("pctfinishers"));

				list.add(obj);

			}

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return list;
	}

	/**
	 * Gets the part trivia engage graphs data.
	 *
	 * @param clientId
	 *            the client id
	 * @param conn
	 *            the conn
	 * @return the part trivia engage graphs data
	 * @throws Exception
	 *             the exception
	 */
	public static List<PartTrivEngageGrpDVO> getPartTriviaEngageGraphsData(String clientId, Connection conn)
			throws Exception {
		List<PartTrivEngageGrpDVO> list = new ArrayList<PartTrivEngageGrpDVO>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer sql = new StringBuffer();
		sql.append("select name, label, yavalue")
				.append(" from ( select 1 as ord, 'Viewers' as name, triviaGames as label, CONVERT(varchar(10),pctviewers)+'%' as yavalue")
				.append(" from grh_rep_noof_trivia_games").append(" where clintid=?").append(" UNION")
				.append(" select 2 as ord, 'Players' as name, triviaGames as label, CONVERT(varchar(10),pctplayers)+'%' as yavalue")
				.append(" from grh_rep_noof_trivia_games").append(" where clintid=?").append(" UNION")
				.append(" select 3 as ord, 'Finishers' as name, triviaGames as label, CONVERT(varchar(10),pctfinishers)+'%' as yavalue")
				.append(" from grh_rep_noof_trivia_games").append(" where clintid=?").append(" ) tabA")
				.append(" order by ord, name;");
		try {
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			ps.setString(2, clientId);
			ps.setString(3, clientId);
			rs = ps.executeQuery();

			PartTrivEngageGrpDVO obj = null;
			while (rs.next()) {
				obj = new PartTrivEngageGrpDVO();
				obj.setName(rs.getString("name"));
				obj.setLabel(rs.getString("label"));
				obj.setValue(rs.getString("yavalue"));
				list.add(obj);

			}

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return list;
	}

	/**
	 * Gets the no of shoppable games graphs data.
	 *
	 * @param clientId
	 *            the client id
	 * @param conn
	 *            the conn
	 * @return the no of shoppable games graphs data
	 * @throws Exception
	 *             the exception
	 */
	public static List<NoOfShoppableGamesGrpDVO> getNoOfShoppableGamesGraphsData(String clientId, Connection conn)
			throws Exception {
		List<NoOfShoppableGamesGrpDVO> list = new ArrayList<NoOfShoppableGamesGrpDVO>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer sql = new StringBuffer("select shoppableGames,  noofShoppableGames from grh_rep_noof_shoppable_games with(nolock) where clintid=? order by shoppableGames;");
		try {

			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			rs = ps.executeQuery();
			NoOfShoppableGamesGrpDVO obj = null;
			while (rs.next()) {
				obj = new NoOfShoppableGamesGrpDVO();
				obj.setName(rs.getString("shoppableGames"));
				obj.setNotrivs(rs.getInt("noofShoppableGames"));
				list.add(obj);

			}

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return list;
	}

	/**
	 * Gets the participants shoppable engage graphs data.
	 *
	 * @param clientId
	 *            the client id
	 * @param conn
	 *            the conn
	 * @return the participants shoppable engage graphs data
	 * @throws Exception
	 *             the exception
	 */
	public static List<ParticipantsShopEngageGrpDVO> getParticipantsShoppableEngageGraphsData(String clientId,
			Connection conn) throws Exception {
		List<ParticipantsShopEngageGrpDVO> list = new ArrayList<ParticipantsShopEngageGrpDVO>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer sql = new StringBuffer("select shoppableGames, pctviewers, pctplayers, pctfinishers from grh_rep_noof_shoppable_games with(nolock) where clintid=? order by shoppableGames");
		try {

			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			rs = ps.executeQuery();
			ParticipantsShopEngageGrpDVO obj = null;
			while (rs.next()) {
				obj = new ParticipantsShopEngageGrpDVO();
				obj.setName(rs.getString("shoppableGames"));
				obj.setPassiveViewers(rs.getDouble("pctviewers"));
				obj.setPlayers(rs.getDouble("pctplayers"));
				obj.setFinishers(rs.getDouble("pctfinishers"));
				list.add(obj);

			}

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return list;
	}

	/**
	 * Gets the cust engage growth perc graphs data.
	 *
	 * @param clientId
	 *            the client id
	 * @param conn
	 *            the conn
	 * @return the cust engage growth perc graphs data
	 * @throws Exception
	 *             the exception
	 */
	public static List<CustEngageGrowthPercGrpDVO> getCustEngageGrowthPercGraphsData(String clientId, Connection conn)
			throws Exception {
		List<CustEngageGrowthPercGrpDVO> list = new ArrayList<CustEngageGrowthPercGrpDVO>();

		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer sql = new StringBuffer();
		sql.append("select conyearmnthstr as 'YearMonth',")
				.append(" pctviewersgrowth  as 'PassiveViewersGrowthPerc', pctplayersgrowth as 'PlayersGrowthPerc',")
				.append(" pctfinishersgrowth as 'FinalistGrowthPerc', pctwinnergrowth as 'WinnersGrowthPerc',")
				.append(" pctgamesgrowth as 'GamesGrowthPerc'")
				.append(" from grh_rep_ason_month_customer_engagements with(nolock)").append(" where clintid=?")
				.append(" order by conyear, conmnth");
		try {
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			rs = ps.executeQuery();
			CustEngageGrowthPercGrpDVO obj = null;
			while (rs.next()) {
				obj = new CustEngageGrowthPercGrpDVO();
				obj.setName(rs.getString("YearMonth"));
				obj.setPassiveViewers(rs.getDouble("PassiveViewersGrowthPerc"));
				obj.setPlayers(rs.getDouble("PlayersGrowthPerc"));
				obj.setFinalists(rs.getDouble("FinalistGrowthPerc"));
				obj.setWinners(rs.getDouble("WinnersGrowthPerc"));
				obj.setGames(rs.getDouble("GamesGrowthPerc"));
				list.add(obj);

			}

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return list;
	}

	/**
	 * Gets the products trivia engagement rate graphs data.
	 *
	 * @param clientId
	 *            the client id
	 * @param prodTypeStr
	 *            the prod type str
	 * @param conn
	 *            the conn
	 * @return the products trivia engagement rate graphs data
	 * @throws Exception
	 *             the exception
	 */
	public static Map<String, Map<String, String>> getProductsTriviaEngagementRateGraphsData(String clientId,
			String prodTypeStr, Connection conn) throws Exception {
		Map<String, Map<String, String>> map = new HashMap<String, Map<String, String>>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer sql = new StringBuffer();
		sql.append("select prodtypename, zeroToOneK , oneToFiveK, fiveToTenK, tenToFiftyK,")
				.append(" fiftyToHundredK, hundredToFiveHundredK, fiveHundredToOneKK,greaterToOneKK,")
				.append(" prodtype").append(" from grh_rep_all_prod_engagement_rate with(nolock)")
				.append(" where clintid=?").append(" and prodtype in (").append(prodTypeStr).append(")");
		try {
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			rs = ps.executeQuery();

			Map<String, String> tempMap;
			while (rs.next()) {
				tempMap = new LinkedHashMap<String, String>();
				tempMap.put("prodtypename", rs.getString("prodtypename"));
				tempMap.put("0-1k", rs.getString("zeroToOneK"));
				tempMap.put("1k-5k", rs.getString("oneToFiveK"));
				tempMap.put("5k-10k", rs.getString("fiveToTenK"));
				tempMap.put("10k-50k", rs.getString("tenToFiftyK"));
				tempMap.put("50k-100k", rs.getString("fiftyToHundredK"));
				tempMap.put("100k-500k", rs.getString("hundredToFiveHundredK"));
				tempMap.put("500k-1kk", rs.getString("fiveHundredToOneKK"));
				tempMap.put(">1kk", rs.getString("greaterToOneKK"));
				map.put(rs.getString("prodtype"), tempMap);
			}

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return map;
	}

	/**
	 * Gets the cust part engage demography graphs data.
	 *
	 * @param clientId
	 *            the client id
	 * @param conn
	 *            the conn
	 * @return the cust part engage demography graphs data
	 * @throws Exception
	 *             the exception
	 */
	public static Map<String, String> getCustPartEngageDemographyGraphsData(String clientId, Connection conn)
			throws Exception {
		Map<String, String> tempMap = new LinkedHashMap<String, String>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer sql = new StringBuffer("select prodtypename, ag10to15, ag16to20 , ag21to25, ag26to35, ag36to45, ag46to55, ag56to65, aggrt65" + " from grh_rep_all_prod_engagement_rate  where clintid=? and prodtype='ALL'");
		try {
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			rs = ps.executeQuery();

			while (rs.next()) {
				tempMap.put("prodtypename", rs.getString("prodtypename"));
				tempMap.put("10 to 15", rs.getString("ag10to15"));
				tempMap.put("16 to 20", rs.getString("ag16to20"));
				tempMap.put("21 to 25", rs.getString("ag21to25"));
				tempMap.put("26 to 35", rs.getString("ag26to35"));
				tempMap.put("36 to 45", rs.getString("ag36to45"));
				tempMap.put("46 to 55", rs.getString("ag46to55"));
				tempMap.put("56 to 65", rs.getString("ag56to65"));
				tempMap.put("> 65", rs.getString("aggrt65"));
			}

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return tempMap;
	}

	/**
	 * Gets the trivias engage demography graphs data.
	 *
	 * @param clientId
	 *            the client id
	 * @param conn
	 *            the conn
	 * @return the trivias engage demography graphs data
	 * @throws Exception
	 *             the exception
	 */
	public static List<Map<String, String>> getTriviasEngageDemographyGraphsData(String clientId, Connection conn)
			throws Exception {
		List<Map<String, String>> list = new ArrayList<Map<String, String>>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer sql = new StringBuffer("select triviaGames, ag10to15, ag16to20, ag21to25, ag26to35, ag36to45, ag46to55, ag56to65, aggrt65 from grh_rep_noof_trivia_games where clintid=?");
		try {
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			rs = ps.executeQuery();
			Map<String, String> tempMap;
			while (rs.next()) {
				tempMap = new LinkedHashMap<String, String>();
				tempMap.put("triviaGames", rs.getString("triviaGames"));
				tempMap.put("10 to 15", rs.getString("ag10to15"));
				tempMap.put("16 to 20", rs.getString("ag16to20"));
				tempMap.put("21 to 25", rs.getString("ag21to25"));
				tempMap.put("26 to 35", rs.getString("ag26to35"));
				tempMap.put("36 to 45", rs.getString("ag36to45"));
				tempMap.put("46 to 55", rs.getString("ag46to55"));
				tempMap.put("56 to 65", rs.getString("ag56to65"));
				tempMap.put("> 65", rs.getString("aggrt65"));
				list.add(tempMap);
			}

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return list;
	}

	/**
	 * Gets the shopp games engage demography graphs data.
	 *
	 * @param clientId
	 *            the client id
	 * @param conn
	 *            the conn
	 * @return the shopp games engage demography graphs data
	 * @throws Exception
	 *             the exception
	 */
	public static List<Map<String, String>> getShoppGamesEngageDemographyGraphsData(String clientId, Connection conn)
			throws Exception {
		List<Map<String, String>> list = new ArrayList<Map<String, String>>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer sql = new StringBuffer("select shoppableGames,ag10to15, ag16to20, ag21to25, ag26to35, ag36to45, ag46to55, ag56to65, aggrt65  from grh_rep_noof_shoppable_games where clintid=?");
		try {
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			rs = ps.executeQuery();
			Map<String, String> tempMap;
			while (rs.next()) {
				tempMap = new LinkedHashMap<String, String>();
				tempMap.put("shoppableGames", rs.getString("shoppableGames"));
				tempMap.put("10 to 15", rs.getString("ag10to15"));
				tempMap.put("16 to 20", rs.getString("ag16to20"));
				tempMap.put("21 to 25", rs.getString("ag21to25"));
				tempMap.put("26 to 35", rs.getString("ag26to35"));
				tempMap.put("36 to 45", rs.getString("ag36to45"));
				tempMap.put("46 to 55", rs.getString("ag46to55"));
				tempMap.put("56 to 65", rs.getString("ag56to65"));
				tempMap.put("> 65", rs.getString("aggrt65"));
				list.add(tempMap);
			}

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return list;
	}
}
