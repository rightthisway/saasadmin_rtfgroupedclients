/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.reports.dto;

import java.util.List;

import com.rtf.reports.dvo.NoOfTriviaGamesGrpDVO;
import com.rtf.saas.arch.RtfSaasBaseDTO;

/**
 * The Class NoOfTriviaGamesGrpDTO.
 */
public class NoOfTriviaGamesGrpDTO extends RtfSaasBaseDTO {
	
	/** The list. */
	List<NoOfTriviaGamesGrpDVO> list;

	/**
	 * Gets the list.
	 *
	 * @return the list
	 */
	public List<NoOfTriviaGamesGrpDVO> getList() {
		return list;
	}

	/**
	 * Sets the list.
	 *
	 * @param list the new list
	 */
	public void setList(List<NoOfTriviaGamesGrpDVO> list) {
		this.list = list;
	}
	
}
