/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.reports.dto;

import java.util.List;

import com.rtf.reports.dvo.CustEngageMonthWiseGrpDVO;
import com.rtf.saas.arch.RtfSaasBaseDTO;

/**
 * The Class CustEngageMonthWiseDTO.
 */
public class CustEngageMonthWiseDTO extends RtfSaasBaseDTO {

	/** The list. */
	List<CustEngageMonthWiseGrpDVO> list;

	/**
	 * Gets the list.
	 *
	 * @return the list
	 */
	public List<CustEngageMonthWiseGrpDVO> getList() {
		return list;
	}

	/**
	 * Sets the list.
	 *
	 * @param list the new list
	 */
	public void setList(List<CustEngageMonthWiseGrpDVO> list) {
		this.list = list;
	}

}
