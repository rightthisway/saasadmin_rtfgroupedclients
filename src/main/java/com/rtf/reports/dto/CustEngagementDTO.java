/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.reports.dto;

import java.util.List;
import java.util.Map;

import com.rtf.reports.dvo.CustEngageGrowthPercGrpDVO;
import com.rtf.reports.dvo.CustEngageMonthWiseGrpDVO;
import com.rtf.saas.arch.RtfSaasBaseDTO;

/**
 * The Class CustEngagementDTO.
 */
public class CustEngagementDTO extends RtfSaasBaseDTO {
	
	/** The cuenglist. */
	List<CustEngageMonthWiseGrpDVO> cuenglist;
	
	/** The cugrowlist. */
	List<CustEngageGrowthPercGrpDVO> cugrowlist;
	
	/** The cu eng dmgrpy. */
	Map<String,String> cuEngDmgrpy;
	
	
	/**
	 * Gets the cuenglist.
	 *
	 * @return the cuenglist
	 */
	public List<CustEngageMonthWiseGrpDVO> getCuenglist() {
		return cuenglist;
	}
	
	/**
	 * Sets the cuenglist.
	 *
	 * @param cuenglist the new cuenglist
	 */
	public void setCuenglist(List<CustEngageMonthWiseGrpDVO> cuenglist) {
		this.cuenglist = cuenglist;
	}
	
	/**
	 * Gets the cugrowlist.
	 *
	 * @return the cugrowlist
	 */
	public List<CustEngageGrowthPercGrpDVO> getCugrowlist() {
		return cugrowlist;
	}
	
	/**
	 * Sets the cugrowlist.
	 *
	 * @param cugrowlist the new cugrowlist
	 */
	public void setCugrowlist(List<CustEngageGrowthPercGrpDVO> cugrowlist) {
		this.cugrowlist = cugrowlist;
	}
	
	/**
	 * Gets the cu eng dmgrpy.
	 *
	 * @return the cu eng dmgrpy
	 */
	public Map<String, String> getCuEngDmgrpy() {
		return cuEngDmgrpy;
	}
	
	/**
	 * Sets the cu eng dmgrpy.
	 *
	 * @param cuEngDmgrpy the cu eng dmgrpy
	 */
	public void setCuEngDmgrpy(Map<String, String> cuEngDmgrpy) {
		this.cuEngDmgrpy = cuEngDmgrpy;
	}

	
	
}
