/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.reports.dto;

import java.util.List;

import com.rtf.reports.dvo.ParticipantsTrivEngageGrpDVO;
import com.rtf.saas.arch.RtfSaasBaseDTO;

/**
 * The Class ParticipantsTrivEngageGrpDTO.
 */
public class ParticipantsTrivEngageGrpDTO extends RtfSaasBaseDTO {
	
	/** The list. */
	List<ParticipantsTrivEngageGrpDVO> list;

	/**
	 * Gets the list.
	 *
	 * @return the list
	 */
	public List<ParticipantsTrivEngageGrpDVO> getList() {
		return list;
	}

	/**
	 * Sets the list.
	 *
	 * @param list the new list
	 */
	public void setList(List<ParticipantsTrivEngageGrpDVO> list) {
		this.list = list;
	}
	
}
