/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.reports.dto;

import java.util.List;
import java.util.Map;

import com.rtf.reports.dvo.NoOfTriviaGamesGrpDVO;
import com.rtf.reports.dvo.PartTrivEngageGrpDVO;
import com.rtf.reports.dvo.ParticipantsTrivEngageGrpDVO;
import com.rtf.saas.arch.RtfSaasBaseDTO;

/**
 * The Class TriviaGamesGrpDTO.
 */
public class TriviaGamesGrpDTO extends RtfSaasBaseDTO {
	
	/** The trivslist. */
	List<NoOfTriviaGamesGrpDVO> trivslist;
	
	/** The prtslist. */
	List<ParticipantsTrivEngageGrpDVO> prtslist;
	
	/** The prts trivlist. */
	List<PartTrivEngageGrpDVO> prtsTrivlist;
	
	/** The livt engage rate. */
	Map<String,String> livtEngageRate;
	
	/** The ott engage rate. */
	Map<String,String> ottEngageRate;
	
	/** The snw engage rate. */
	Map<String,String> snwEngageRate;
	
	/** The triv gams dmgrpy. */
	List<Map<String, String>> trivGamsDmgrpy;
	
	/**
	 * Gets the trivslist.
	 *
	 * @return the trivslist
	 */
	public List<NoOfTriviaGamesGrpDVO> getTrivslist() {
		return trivslist;
	}
	
	/**
	 * Sets the trivslist.
	 *
	 * @param trivslist the new trivslist
	 */
	public void setTrivslist(List<NoOfTriviaGamesGrpDVO> trivslist) {
		this.trivslist = trivslist;
	}
	
	/**
	 * Gets the prtslist.
	 *
	 * @return the prtslist
	 */
	public List<ParticipantsTrivEngageGrpDVO> getPrtslist() {
		return prtslist;
	}
	
	/**
	 * Sets the prtslist.
	 *
	 * @param prtslist the new prtslist
	 */
	public void setPrtslist(List<ParticipantsTrivEngageGrpDVO> prtslist) {
		this.prtslist = prtslist;
	}
	
	/**
	 * Gets the livt engage rate.
	 *
	 * @return the livt engage rate
	 */
	public Map<String, String> getLivtEngageRate() {
		return livtEngageRate;
	}
	
	/**
	 * Sets the livt engage rate.
	 *
	 * @param livtEngageRate the livt engage rate
	 */
	public void setLivtEngageRate(Map<String, String> livtEngageRate) {
		this.livtEngageRate = livtEngageRate;
	}
	
	/**
	 * Gets the ott engage rate.
	 *
	 * @return the ott engage rate
	 */
	public Map<String, String> getOttEngageRate() {
		return ottEngageRate;
	}
	
	/**
	 * Sets the ott engage rate.
	 *
	 * @param ottEngageRate the ott engage rate
	 */
	public void setOttEngageRate(Map<String, String> ottEngageRate) {
		this.ottEngageRate = ottEngageRate;
	}
	
	/**
	 * Gets the snw engage rate.
	 *
	 * @return the snw engage rate
	 */
	public Map<String, String> getSnwEngageRate() {
		return snwEngageRate;
	}
	
	/**
	 * Sets the snw engage rate.
	 *
	 * @param snwEngageRate the snw engage rate
	 */
	public void setSnwEngageRate(Map<String, String> snwEngageRate) {
		this.snwEngageRate = snwEngageRate;
	}
	
	/**
	 * Gets the triv gams dmgrpy.
	 *
	 * @return the triv gams dmgrpy
	 */
	public List<Map<String, String>> getTrivGamsDmgrpy() {
		return trivGamsDmgrpy;
	}
	
	/**
	 * Sets the triv gams dmgrpy.
	 *
	 * @param trivGamsDmgrpy the triv gams dmgrpy
	 */
	public void setTrivGamsDmgrpy(List<Map<String, String>> trivGamsDmgrpy) {
		this.trivGamsDmgrpy = trivGamsDmgrpy;
	}
	
	/**
	 * Gets the prts trivlist.
	 *
	 * @return the prts trivlist
	 */
	public List<PartTrivEngageGrpDVO> getPrtsTrivlist() {
		return prtsTrivlist;
	}
	
	/**
	 * Sets the prts trivlist.
	 *
	 * @param prtsTrivlist the new prts trivlist
	 */
	public void setPrtsTrivlist(List<PartTrivEngageGrpDVO> prtsTrivlist) {
		this.prtsTrivlist = prtsTrivlist;
	}


	
}
