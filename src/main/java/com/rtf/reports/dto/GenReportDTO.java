/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.reports.dto;

import java.util.List;

import com.rtf.saas.arch.RtfSaasBaseDTO;

/**
 * The Class CustAnswerDTO.
 */
public class GenReportDTO extends RtfSaasBaseDTO {

	
	private String coId;
	private String clId ;	
	private List<String> colNameLst;
	private List<List<String>> datasArryList;
	private List<String> dynamicColList;
	String conName;
	String conDate;
	
	public String getCoId() {
		return coId;
	}
	public void setCoId(String coId) {
		this.coId = coId;
	}
	public String getClId() {
		return clId;
	}
	public void setClId(String clId) {
		this.clId = clId;
	}	
	public List<String> getColNameLst() {
		return colNameLst;
	}
	public void setColNameLst(List<String> colNameLst) {
		this.colNameLst = colNameLst;
	}
	public List<List<String>> getDatasArryList() {
		return datasArryList;
	}
	public void setDatasArryList(List<List<String>> datasArryList) {
		this.datasArryList = datasArryList;
	}
	public List<String> getDynamicColList() {
		return dynamicColList;
	}
	public void setDynamicColList(List<String> dynamicColList) {
		this.dynamicColList = dynamicColList;
	}
	public String getConName() {
		return conName;
	}
	public void setConName(String conName) {
		this.conName = conName;
	}
	public String getConDate() {
		return conDate;
	}
	public void setConDate(String conDate) {
		this.conDate = conDate;
	}
	
}
