/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.reports.dto;

import java.util.List;

import com.rtf.reports.dvo.ParticipantsShopEngageGrpDVO;
import com.rtf.saas.arch.RtfSaasBaseDTO;

/**
 * The Class ParticipantsShopEngageGrpDTO.
 */
public class ParticipantsShopEngageGrpDTO extends RtfSaasBaseDTO {

	/** The list. */
	List<ParticipantsShopEngageGrpDVO> list;

	/**
	 * Gets the list.
	 *
	 * @return the list
	 */
	public List<ParticipantsShopEngageGrpDVO> getList() {
		return list;
	}

	/**
	 * Sets the list.
	 *
	 * @param list the new list
	 */
	public void setList(List<ParticipantsShopEngageGrpDVO> list) {
		this.list = list;
	}

}
