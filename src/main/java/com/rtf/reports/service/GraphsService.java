/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.reports.service;

import java.sql.Connection;
import java.util.List;
import java.util.Map;

import com.rtf.reports.dto.CustEngagementDTO;
import com.rtf.reports.dto.ShoppalbeGamesGrpDTO;
import com.rtf.reports.dto.TriviaGamesGrpDTO;
import com.rtf.reports.dvo.CustEngageGrowthPercGrpDVO;
import com.rtf.reports.dvo.CustEngageMonthWiseGrpDVO;
import com.rtf.reports.dvo.NoOfShoppableGamesGrpDVO;
import com.rtf.reports.dvo.NoOfTriviaGamesGrpDVO;
import com.rtf.reports.dvo.PartTrivEngageGrpDVO;
import com.rtf.reports.dvo.ParticipantsShopEngageGrpDVO;
import com.rtf.reports.dvo.ParticipantsTrivEngageGrpDVO;
import com.rtf.reports.sql.dao.GraphsSqlDAO;
import com.rtf.saas.sql.db.DatabaseConnections;

/**
 * The Class GraphsService.
 */
public class GraphsService {

	/**
	 * Gets the custoemr engament graphs data.
	 *
	 * @param clId    the cl id
	 * @param respDTO the resp DTO
	 * @return the custoemr engament graphs data
	 * @throws Exception the exception
	 */
	public static CustEngagementDTO getCustoemrEngamentGraphsData(String clId, CustEngagementDTO respDTO)
			throws Exception {

		Connection conn = null;
		try {
			conn = DatabaseConnections.getSaasAdminReportDBConnection();

			List<CustEngageMonthWiseGrpDVO> engageList = GraphsSqlDAO.getCustEngageMonthWiseGraphsData(clId, conn);
			respDTO.setCuenglist(engageList);

			List<CustEngageGrowthPercGrpDVO> growthList = GraphsSqlDAO.getCustEngageGrowthPercGraphsData(clId, conn);
			respDTO.setCugrowlist(growthList);

			Map<String, String> map = GraphsSqlDAO.getCustPartEngageDemographyGraphsData(clId, conn);
			respDTO.setCuEngDmgrpy(map);

			respDTO.setSts(1);

		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;

		} finally {
			try {
				DatabaseConnections.closeConnection(conn);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return respDTO;

	}

	/**
	 * Gets the trivia games graphs data.
	 *
	 * @param clId    the cl id
	 * @param respDTO the resp DTO
	 * @return the trivia games graphs data
	 * @throws Exception the exception
	 */
	public static TriviaGamesGrpDTO getTriviaGamesGraphsData(String clId, TriviaGamesGrpDTO respDTO) throws Exception {

		Connection conn = null;
		try {
			conn = DatabaseConnections.getSaasAdminReportDBConnection();

			List<NoOfTriviaGamesGrpDVO> triviaslist = GraphsSqlDAO.getNoOfTriviaGamesGraphsData(clId, conn);
			respDTO.setTrivslist(triviaslist);

			List<ParticipantsTrivEngageGrpDVO> partslist = GraphsSqlDAO.getParticipantsTriviaEngageGraphsData(clId,
					conn);
			respDTO.setPrtslist(partslist);

			List<PartTrivEngageGrpDVO> partlist = GraphsSqlDAO.getPartTriviaEngageGraphsData(clId, conn);
			respDTO.setPrtsTrivlist(partlist);

			String prodTypeStr = "'LVT','OTT','SNW'";
			Map<String, Map<String, String>> map = GraphsSqlDAO.getProductsTriviaEngagementRateGraphsData(clId,
					prodTypeStr, conn);
			respDTO.setLivtEngageRate(map.get("LVT"));
			respDTO.setOttEngageRate(map.get("OTT"));
			respDTO.setSnwEngageRate(map.get("SNW"));

			List<Map<String, String>> listDmGrpy = GraphsSqlDAO.getTriviasEngageDemographyGraphsData(clId, conn);
			respDTO.setTrivGamsDmgrpy(listDmGrpy);

			respDTO.setSts(1);

		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		} finally {
			try {
				DatabaseConnections.closeConnection(conn);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return respDTO;

	}

	/**
	 * Gets the shoppable games graphs data.
	 *
	 * @param clId    the cl id
	 * @param respDTO the resp DTO
	 * @return the shoppable games graphs data
	 * @throws Exception the exception
	 */
	public static ShoppalbeGamesGrpDTO getShoppableGamesGraphsData(String clId, ShoppalbeGamesGrpDTO respDTO)
			throws Exception {

		Connection conn = null;
		try {
			conn = DatabaseConnections.getSaasAdminReportDBConnection();

			List<NoOfShoppableGamesGrpDVO> triviaslist = GraphsSqlDAO.getNoOfShoppableGamesGraphsData(clId, conn);
			respDTO.setTrivslist(triviaslist);

			List<ParticipantsShopEngageGrpDVO> partslist = GraphsSqlDAO.getParticipantsShoppableEngageGraphsData(clId,
					conn);
			respDTO.setPrtslist(partslist);

			String prodTypeStr = "'SVT'";
			Map<String, Map<String, String>> map = GraphsSqlDAO.getProductsTriviaEngagementRateGraphsData(clId,
					prodTypeStr, conn);
			respDTO.setSvtEngageRate(map.get("SVT"));

			List<Map<String, String>> shopGamsDmgrpy = GraphsSqlDAO.getShoppGamesEngageDemographyGraphsData(clId, conn);
			respDTO.setShopGamsDmgrpy(shopGamsDmgrpy);

			respDTO.setSts(1);

		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;

		} finally {
			try {
				DatabaseConnections.closeConnection(conn);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return respDTO;

	}
}
