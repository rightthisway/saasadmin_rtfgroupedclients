/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.reports.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import com.rtf.reports.sql.dao.LivtContestReportsDAO;
import com.rtf.reports.utils.ExcelUtil;
import com.rtf.reports.utils.ReportConstants;
import com.rtf.reports.utils.ReportMessageConstant;
import com.rtf.reports.utils.ReportsUtil;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;
import com.rtf.user.util.UserMsgConstants;

/**
 * The Class CustomerAvgTimePerContestStatsReport.
 */
@WebServlet("/custavgtimepercontstatsreport.json")
public class CustomerAvgTimePerContestStatsReport extends RtfSaasBaseServlet {

	private static final long serialVersionUID = -3228982056601411815L;

	/**
	 * Generate Http Response for Rtf base Dto data is sent in JSON format.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * Get customers average time spent in contest report
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String clId = request.getParameter("clId");
		String fromDate = request.getParameter("fromDate");
		String toDate = request.getParameter("toDate");
		String cau = request.getParameter("cau");

		RtfSaasBaseDTO respDTO = new RtfSaasBaseDTO();
		respDTO.setSts(0);
		String msg = null;
		try {
			if (clId == null || clId.isEmpty()) {
				msg = UserMsgConstants.INVALID_CLIENT;
				setClientMessage(respDTO, msg, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (fromDate == null || fromDate.isEmpty()) {
				msg = ReportMessageConstant.INVALID_FROM_DATE;
				setClientMessage(respDTO, msg, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (toDate == null || toDate.isEmpty()) {
				msg = "Invalid To Date.";
				setClientMessage(respDTO, msg, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (cau == null || cau.isEmpty()) {
				msg = UserMsgConstants.INVALID_ADMIN_USER_ID;
				setClientMessage(respDTO, msg, null);
				generateResponse(request, response, respDTO);
				return;
			}

			List<String> headerList = new ArrayList<String>();
			headerList.add("Contest ID");
			headerList.add("Contest Name");
			headerList.add("Contest Date");
			headerList.add("Overall Participants Count");
			headerList.add("Overall Average Minutes Spent On Contest");
			headerList.add("Participants Count who Spent 0 to 5 Mins");
			headerList.add("Average Participants Count who Spent 0 to 5 Mins");
			headerList.add("Participants Count who Spent 5 to 10 Mins");
			headerList.add("Average Participants Count who Spent 5 to 10 Mins");
			headerList.add("Participants Count who Spent 10 to 15 Mins");
			headerList.add("Average Participants Count who Spent 10 to 15 Mins");
			headerList.add("Participants Count who Spent 15 Mins above");
			headerList.add("Average Participants Count who Spent 15 Mins above");
			headerList.add("Participants Count who Spent 0 to 11 Mins");
			headerList.add("Average Participants Count who Spent 0 to 11 Mins");
			headerList.add("Participants Count who Spent 11 Mins above");
			headerList.add("Average Participants Count who Spent 11 Mins above");

			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet = workbook.createSheet("TimeSpentStatisticsOnEachContest");

			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));

			Map<String, String> reportNameMap = new LinkedHashMap<String, String>();
			reportNameMap.put("Report Name", "Customer Average Time Taken per Contest Statistics Report");

			int startRowCnt = 0;
			ExcelUtil.addReportTitle(reportNameMap, ssSheet, workbook, startRowCnt);

			startRowCnt = startRowCnt + 2;
			ExcelUtil.generateExcelHeaderRowWithStyles(headerList, ssSheet, workbook, startRowCnt);

			List<Object[]> dataList = LivtContestReportsDAO.getCustomerAverageTimeTakenperContestStatistics(clId,
					fromDate, toDate);
			if (dataList == null || dataList.size() <= 0) {
				ExcelUtil.generateErrorMessageExcel(ssSheet, workbook, startRowCnt, ReportConstants.NO_RECS_FOUND);
				generateExcelResponse(response, workbook);
				return;
			}
			int rowCount = startRowCnt + 1;
			int j = 0;
			int totalContest = 0; // 0
			Integer totalParticipants = 0; // 3
			Double totalAverage = 0.00; // 4
			Double totalAvgPrtiZtoFive = 0.00; // 6
			Double totalAvgPrtiFivetoTen = 0.00; // 8
			Double totalAvgPrtiTentoFifteen = 0.00; // 10
			Double totalAvgPrtiFifteenAbove = 0.00; // 12
			Double totalAvgPrtiZtoEleven = 0.00; // 14
			Double totalAvgPrtiElevenAbove = 0.00; // 16

			for (Object[] dataObj : dataList) {

				Row rowhead = ssSheet.createRow(rowCount);
				rowCount++;
				j = 0;
				ReportsUtil.getExcelStringCell(dataObj[0], j, rowhead);
				j++;
				ReportsUtil.getExcelStringCell(dataObj[1], j, rowhead);
				j++;
				ReportsUtil.getExcelStringCell(dataObj[2], j, rowhead);
				j++;
				ReportsUtil.getExcelIntegerCell(dataObj[3], j, rowhead);
				j++;
				ReportsUtil.getExcelDecimalCell(dataObj[4], j, rowhead);
				j++;
				ReportsUtil.getExcelIntegerCell(dataObj[5], j, rowhead);
				j++;
				ReportsUtil.getExcelDecimalCell(dataObj[6], j, rowhead);
				j++;
				ReportsUtil.getExcelIntegerCell(dataObj[7], j, rowhead);
				j++;
				ReportsUtil.getExcelDecimalCell(dataObj[8], j, rowhead);
				j++;
				ReportsUtil.getExcelIntegerCell(dataObj[9], j, rowhead);
				j++;
				ReportsUtil.getExcelDecimalCell(dataObj[10], j, rowhead);
				j++;
				ReportsUtil.getExcelIntegerCell(dataObj[11], j, rowhead);
				j++;
				ReportsUtil.getExcelDecimalCell(dataObj[12], j, rowhead);
				j++;
				ReportsUtil.getExcelIntegerCell(dataObj[13], j, rowhead);
				j++;
				ReportsUtil.getExcelDecimalCell(dataObj[14], j, rowhead);
				j++;
				ReportsUtil.getExcelIntegerCell(dataObj[15], j, rowhead);
				j++;
				ReportsUtil.getExcelDecimalCell(dataObj[16], j, rowhead);
				j++;

				if (dataObj[0] != null) {
					totalContest++;
				}
				if (dataObj[3] != null) {
					totalParticipants += Integer.parseInt(dataObj[3].toString());
				}
				if (dataObj[4] != null) {
					totalAverage += Double.parseDouble(dataObj[4].toString());
				}
				if (dataObj[6] != null) {
					totalAvgPrtiZtoFive += Double.parseDouble(dataObj[6].toString());
				}
				if (dataObj[8] != null) {
					totalAvgPrtiFivetoTen += Double.parseDouble(dataObj[8].toString());
				}
				if (dataObj[10] != null) {
					totalAvgPrtiTentoFifteen += Double.parseDouble(dataObj[10].toString());
				}
				if (dataObj[12] != null) {
					totalAvgPrtiFifteenAbove += Double.parseDouble(dataObj[12].toString());
				}
				if (dataObj[14] != null) {
					totalAvgPrtiZtoEleven += Double.parseDouble(dataObj[14].toString());
				}
				if (dataObj[16] != null) {
					totalAvgPrtiElevenAbove += Double.parseDouble(dataObj[16].toString());
				}
			}

			if (totalContest > 0) {
				rowCount++;
				ssSheet.createRow(rowCount);
				Row rowhead = ssSheet.createRow(rowCount);
				ReportsUtil.getExcelStringCell("Total No. Of Contest:", 0, rowhead);
				ReportsUtil.getExcelIntegerCell(totalContest, 1, rowhead);

				ReportsUtil.getExcelStringCell("Total Participants:", 2, rowhead);
				if (totalParticipants > 0) {
					ReportsUtil.getExcelIntegerCell(totalParticipants, 3, rowhead);
				}
				if (totalAverage > 0) {
					Double x = totalAverage / totalContest;
					ReportsUtil.getExcelDecimalCell(x, 4, rowhead);
				}
				if (totalAvgPrtiZtoFive > 0) {
					Double x = totalAvgPrtiZtoFive / totalContest;
					ReportsUtil.getExcelDecimalCell(x, 6, rowhead);
				}
				if (totalAvgPrtiFivetoTen > 0) {
					Double x = totalAvgPrtiFivetoTen / totalContest;
					ReportsUtil.getExcelDecimalCell(x, 8, rowhead);
				}
				if (totalAvgPrtiTentoFifteen > 0) {
					Double x = totalAvgPrtiTentoFifteen / totalContest;
					ReportsUtil.getExcelDecimalCell(x, 10, rowhead);
				}
				if (totalAvgPrtiFifteenAbove > 0) {
					Double x = totalAvgPrtiFifteenAbove / totalContest;
					ReportsUtil.getExcelDecimalCell(x, 12, rowhead);
				}
				if (totalAvgPrtiZtoEleven > 0) {
					Double x = totalAvgPrtiZtoEleven / totalContest;
					ReportsUtil.getExcelDecimalCell(x, 14, rowhead);
				}
				if (totalAvgPrtiElevenAbove > 0) {
					Double x = totalAvgPrtiElevenAbove / totalContest;
					ReportsUtil.getExcelDecimalCell(x, 16, rowhead);
				}
			}

			generateExcelResponse(response, workbook);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Generate excel response.
	 *
	 * @param response the response
	 * @param workbook the workbook
	 * @throws Exception the exception
	 */
	public static void generateExcelResponse(HttpServletResponse response, SXSSFWorkbook workbook) throws Exception {
		response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=CustomerAverageTimeTakenperContestStatisticsReport."
						+ ReportsUtil.EXCEL_EXTENSION);
		workbook.write(response.getOutputStream());
		workbook.close();
	}

}
