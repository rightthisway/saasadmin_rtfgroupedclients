/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.reports.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import com.rtf.reports.sql.dao.LivtContestReportsDAO;
import com.rtf.reports.utils.ExcelUtil;
import com.rtf.reports.utils.ReportConstants;
import com.rtf.reports.utils.ReportsUtil;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;
import com.rtf.user.util.UserMsgConstants;

/**
 * The Class MonthlyCustPartcpCntBrkDwnReport.
 */
@WebServlet("/mnthcustpartcpbrkdwnreport.json")
public class MonthlyCustPartcpCntBrkDwnReport extends RtfSaasBaseServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 141312500L;

	/**
	 * Generate Http Response for Rtf base Dto data is sent in JSON format.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * Get month wise contest participants break down report
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String clId = request.getParameter("clId");
		String cau = request.getParameter("cau");

		RtfSaasBaseDTO respDTO = new RtfSaasBaseDTO();
		respDTO.setSts(0);
		String msg = null;
		try {
			if (clId == null || clId.isEmpty()) {
				msg = UserMsgConstants.INVALID_CLIENT;
				setClientMessage(respDTO, msg, null);
				generateResponse(request, response, respDTO);
				return;
			}

			if (cau == null || cau.isEmpty()) {
				msg = UserMsgConstants.INVALID_ADMIN_USER_ID;
				setClientMessage(respDTO, msg, null);
				generateResponse(request, response, respDTO);
				return;
			}

			// For Sheet 1
			List<String> headerList = new ArrayList<String>();
			headerList.add("Month");
			headerList.add("Registered Users");
			headerList.add("No. Of Shows");
			headerList.add("Never Played / Viewed %");
			headerList.add("Never Played / Viewed");
			headerList.add("One Show Then InActive %");
			headerList.add("One Show Then InActive");
			headerList.add("One Show Per Month %");
			headerList.add("One Show Per Month");
			headerList.add("One Show Per Week %");
			headerList.add("One Show Per Week");
			headerList.add("One Show Per Day %");
			headerList.add("One Show Per Day");

			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet = workbook
					.createSheet(ReportConstants.MONTHLYCUSTOMER_PTCP_CNT_BRKDOWN_STATS_REPORT_SHEETNAME);

			Map<String, String> reportNameMap = new LinkedHashMap<String, String>();
			reportNameMap.put("Report Name", ReportConstants.MONTHLYCUSTOMER_PTCP_CNT_BRKDOWN_STATS_REPORT);

			int startRowCnt = 0;
			ExcelUtil.addReportTitle(reportNameMap, ssSheet, workbook, startRowCnt);
			startRowCnt = startRowCnt + 2;

			List<Object[]> dataList = LivtContestReportsDAO
					.getMonthlyCustomerParticipantsCountBreakdownStatistics(clId);
			if (dataList == null || dataList.size() == 0) {
				ExcelUtil.generateErrorMessageExcel(ssSheet, workbook, startRowCnt, ReportConstants.NO_RECS_FOUND);
				generateExcelResponse(response, workbook);
				return;
			}
			ExcelUtil.generateExcelHeaderRowWithStyles(headerList, ssSheet, workbook, startRowCnt);

			int j = 0;
			int rowCount = startRowCnt + 1;
			Integer totalRegisteredUsers = 0;
			Integer totalNoOfShows = 0;

			for (Object[] dataObj : dataList) {

				Row rowhead = ssSheet.createRow(rowCount);
				rowCount++;
				j = 0;

				ReportsUtil.getExcelStringCell(dataObj[0], j, rowhead);
				j++;
				ReportsUtil.getExcelIntegerCell(dataObj[1], j, rowhead);
				j++;
				ReportsUtil.getExcelIntegerCell(dataObj[2], j, rowhead);
				j++;
				ReportsUtil.getExcelDecimalCell(dataObj[3], j, rowhead);
				j++;
				ReportsUtil.getExcelIntegerCell(dataObj[4], j, rowhead);
				j++;
				ReportsUtil.getExcelDecimalCell(dataObj[5], j, rowhead);
				j++;
				ReportsUtil.getExcelIntegerCell(dataObj[6], j, rowhead);
				j++;
				ReportsUtil.getExcelDecimalCell(dataObj[7], j, rowhead);
				j++;
				ReportsUtil.getExcelIntegerCell(dataObj[8], j, rowhead);
				j++;
				ReportsUtil.getExcelDecimalCell(dataObj[9], j, rowhead);
				j++;
				ReportsUtil.getExcelIntegerCell(dataObj[10], j, rowhead);
				j++;
				ReportsUtil.getExcelDecimalCell(dataObj[11], j, rowhead);
				j++;
				ReportsUtil.getExcelIntegerCell(dataObj[12], j, rowhead);
				j++;

				if (dataObj[1] != null) {
					totalRegisteredUsers += Integer.parseInt(dataObj[1].toString());
				}
				if (dataObj[2] != null) {
					totalNoOfShows += Integer.parseInt(dataObj[2].toString());
				}

			}

			if (totalRegisteredUsers > 0) {
				rowCount++;
				ssSheet.createRow(rowCount);
				Row rowhead = ssSheet.createRow(rowCount);
				ReportsUtil.getExcelStringCell("Total Registered Users", 0, rowhead);
				ReportsUtil.getExcelIntegerCell(totalRegisteredUsers, 1, rowhead);
			}
			if (totalNoOfShows > 0) {
				rowCount++;
				ssSheet.createRow(rowCount);
				Row rowhead = ssSheet.createRow(rowCount);
				ReportsUtil.getExcelStringCell("Total No. Of Shows", 0, rowhead);
				ReportsUtil.getExcelIntegerCell(totalNoOfShows, 1, rowhead);
			}

			generateExcelResponse(response, workbook);

		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(respDTO, null, null);
			generateResponse(request, response, respDTO);
		}
	}

	/**
	 * Generate excel response.
	 *
	 * @param response the response
	 * @param workbook the workbook
	 * @throws Exception the exception
	 */
	public static void generateExcelResponse(HttpServletResponse response, SXSSFWorkbook workbook) throws Exception {
		response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=" + ReportConstants.MONTHLYCUSTOMER_PTCP_CNT_BRKDOWN_STATS_REPORT_FNAME + "."
						+ ReportsUtil.EXCEL_EXTENSION);
		workbook.write(response.getOutputStream());
		workbook.close();

	}
}
