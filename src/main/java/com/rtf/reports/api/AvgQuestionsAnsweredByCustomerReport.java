/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.reports.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import com.rtf.reports.sql.dao.LivtContestReportsDAO;
import com.rtf.reports.utils.ExcelUtil;
import com.rtf.reports.utils.ReportConstants;
import com.rtf.reports.utils.ReportsUtil;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;
import com.rtf.user.util.UserMsgConstants;

/**
 * The Class AvgQuestionsAnsweredByCustomerReport.
 */
@WebServlet("/avgquestansbycustreport.json")
public class AvgQuestionsAnsweredByCustomerReport extends RtfSaasBaseServlet {

	private static final long serialVersionUID = -3243259011748692426L;

	/**
	 * Generate Http Response for Rtf base Dto data is sent in JSON format.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * Process request.
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String clId = request.getParameter("clId");
		String cau = request.getParameter("cau");

		RtfSaasBaseDTO respDTO = new RtfSaasBaseDTO();
		respDTO.setSts(0);
		String msg = null;
		try {
			if (clId == null || clId.isEmpty()) {
				msg = UserMsgConstants.INVALID_CLIENT;
				setClientMessage(respDTO, msg, null);
				generateResponse(request, response, respDTO);
				return;
			}

			if (cau == null || cau.isEmpty()) {
				msg = UserMsgConstants.INVALID_ADMIN_USER_ID;
				setClientMessage(respDTO, msg, null);
				generateResponse(request, response, respDTO);
				return;
			}

			List<String> headerList = new ArrayList<String>();
			headerList.add("Customer Name");
			headerList.add("Customer UserID");
			headerList.add("Customer Email");
			headerList.add("No oF Games Played By Answering");
			headerList.add("Total No of Questions from Played Games");
			headerList.add("Total No of Questions Answered");
			headerList.add("Average Percentage Answered by Customer");

			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet = workbook.createSheet("AverageNoOfQuestionAnsweredByCustomer");

			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));

			Map<String, String> reportNameMap = new LinkedHashMap<String, String>();
			reportNameMap.put("Report Name", "Average No Of Questions Answered By Customer");

			int startRowCnt = 0;
			ExcelUtil.addReportTitle(reportNameMap, ssSheet, workbook, startRowCnt);
			startRowCnt = startRowCnt + 2;

			List<Object[]> dataList = LivtContestReportsDAO.getAvgQuestionsAnsweredByCustomerReportData(clId);
			if (dataList == null || dataList.size() <= 0) {
				ExcelUtil.generateErrorMessageExcel(ssSheet, workbook, startRowCnt, ReportConstants.NO_RECS_FOUND);
				generateExcelResponse(response, workbook);
				return;
			}

			ExcelUtil.generateExcelHeaderRowWithStyles(headerList, ssSheet, workbook, startRowCnt);

			int j = 0;
			int rowCount = startRowCnt + 1;
			int totalCustomerCount = 0;
			int totalGames = 0;
			int totalQuestions = 0;
			int totalAnsweredQuestions = 0;
			for (Object[] dataObj : dataList) {

				Row rowhead = ssSheet.createRow(rowCount);
				rowCount++;
				j = 0;

				ReportsUtil.getExcelStringCell(dataObj[0], j, rowhead);
				j++;
				ReportsUtil.getExcelStringCell(dataObj[1], j, rowhead);
				j++;
				ReportsUtil.getExcelStringCell(dataObj[2], j, rowhead);
				j++;
				ReportsUtil.getExcelIntegerCell(dataObj[3], j, rowhead);
				j++;
				if (dataObj[3] != null) {
					totalGames += Integer.parseInt(dataObj[3].toString());
				}
				ReportsUtil.getExcelIntegerCell(dataObj[4], j, rowhead);
				j++;
				if (dataObj[4] != null) {
					totalQuestions += Integer.parseInt(dataObj[4].toString());
				}
				ReportsUtil.getExcelIntegerCell(dataObj[5], j, rowhead);
				j++;
				if (dataObj[5] != null) {
					totalAnsweredQuestions += Integer.parseInt(dataObj[5].toString());
				}
				ReportsUtil.getExcelDecimalCell(dataObj[6], j, rowhead);
				j++;
				totalCustomerCount++;

			}

			ssSheet.createRow(rowCount);
			rowCount++;
			Row rowhead1 = ssSheet.createRow(rowCount);
			ReportsUtil.getExcelStringCell(
					"(A1) Total No OF Customer who have played Game atleast by answering one question:", 0, rowhead1);
			ReportsUtil.getExcelIntegerCell(totalCustomerCount, 1, rowhead1);

			rowCount++;
			Row rowhead2 = ssSheet.createRow(rowCount);
			ReportsUtil.getExcelStringCell("(B1) Total No Of Games Played by All Customers: (sum of Column E) ", 0,
					rowhead2);
			ReportsUtil.getExcelIntegerCell(totalGames, 1, rowhead2);

			rowCount++;
			Row rowhead3 = ssSheet.createRow(rowCount);
			ReportsUtil.getExcelStringCell(
					"(C1) Total No Of Question From all Games Played by All Customers: (sum of column F) ", 0,
					rowhead3);
			ReportsUtil.getExcelIntegerCell(totalQuestions, 1, rowhead3);

			rowCount++;
			Row rowhead4 = ssSheet.createRow(rowCount);
			ReportsUtil.getExcelStringCell(
					"(D1) Total No Of Question Answered From all Games Played by All Customers: (sum of column G) ", 0,
					rowhead4);
			ReportsUtil.getExcelIntegerCell(totalAnsweredQuestions, 1, rowhead4);

			rowCount++;
			Double totalAnsweredQuestionsAsDouble = Double.parseDouble(String.valueOf(totalAnsweredQuestions));
			ssSheet.createRow(rowCount);
			rowCount++;
			Row rowhead5 = ssSheet.createRow(rowCount);
			ReportsUtil.getExcelStringCell("(E1) Overall Average Percentage of Questions Answered: ((D1/C1)*100)", 0,
					rowhead5);
			ReportsUtil.getExcelDecimalCell(((totalAnsweredQuestionsAsDouble / totalQuestions) * 100), 1, rowhead5);

			generateExcelResponse(response, workbook);

		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(respDTO, null, null);
			generateResponse(request, response, respDTO);
		}
	}

	/**
	 * Generate excel response.
	 *
	 * @param response the response
	 * @param workbook the workbook
	 * @throws Exception the exception
	 */
	public static void generateExcelResponse(HttpServletResponse response, SXSSFWorkbook workbook) throws Exception {
		response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=AverageNoOfQuestionAnsweredByCustomerReport." + ReportsUtil.EXCEL_EXTENSION);
		workbook.write(response.getOutputStream());
		workbook.close();
	}

}
