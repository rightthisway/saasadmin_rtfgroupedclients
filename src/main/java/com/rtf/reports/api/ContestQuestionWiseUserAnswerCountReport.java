/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.reports.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import com.rtf.reports.sql.dao.LivtContestReportsDAO;
import com.rtf.reports.utils.ExcelUtil;
import com.rtf.reports.utils.ReportConstants;
import com.rtf.reports.utils.ReportMessageConstant;
import com.rtf.reports.utils.ReportsUtil;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;
import com.rtf.user.util.UserMsgConstants;

/**
 * The Class ContestQuestionWiseUserAnswerCountReport.
 */
@WebServlet("/contquestwiseuseransreport.json")
public class ContestQuestionWiseUserAnswerCountReport extends RtfSaasBaseServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 141312500L;

	/**
	 * Generate Http Response for Rtf base Dto data is sent in JSON format.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * Get Contest question wise users answers count report
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String clId = request.getParameter("clId");
		String fromDate = request.getParameter("fromDate");
		String toDate = request.getParameter("toDate");
		String cau = request.getParameter("cau");

		RtfSaasBaseDTO respDTO = new RtfSaasBaseDTO();
		respDTO.setSts(0);
		String msg = null;
		try {
			if (clId == null || clId.isEmpty()) {
				msg = UserMsgConstants.INVALID_CLIENT;
				setClientMessage(respDTO, msg, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (fromDate == null || fromDate.isEmpty()) {
				msg = ReportMessageConstant.INVALID_FROM_DATE;
				setClientMessage(respDTO, msg, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (toDate == null || toDate.isEmpty()) {
				msg = ReportMessageConstant.INVALID_TO_DATE;
				setClientMessage(respDTO, msg, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (cau == null || cau.isEmpty()) {
				msg = UserMsgConstants.INVALID_ADMIN_USER_ID;
				setClientMessage(respDTO, msg, null);
				generateResponse(request, response, respDTO);
				return;
			}

			List<String> headerList = new ArrayList<String>();
			headerList.add("Contest Id");
			headerList.add("Contest Name");
			headerList.add("Contest Start Date Time");
			headerList.add("Total Participants");
			headerList.add("No Of participants Attempted");
			headerList.add("1st Question Attempted count");
			headerList.add("1st Question Aswered count");
			headerList.add("1st Question Attempted Percentage");
			headerList.add("1st Question Aswered Percentage");
			headerList.add("2nd Question Attempted count");
			headerList.add("2nd Question Aswered count");
			headerList.add("2nd Question Attempted Percentage");
			headerList.add("2nd Question Aswered Percentage");
			headerList.add("3rd Question Attempted count");
			headerList.add("3rd Question Aswered count");
			headerList.add("3rd Question Attempted Percentage");
			headerList.add("3rd Question Aswered Percentage");
			headerList.add("4th Question Attempted count");
			headerList.add("4th Question Aswered count");
			headerList.add("4th Question Attempted Percentage");
			headerList.add("4th Question Aswered Percentage");
			headerList.add("5th Question Attempted count");
			headerList.add("5th Question Aswered count");
			headerList.add("5th Question Attempted Percentage");
			headerList.add("5th Question Aswered Percentage");
			headerList.add("6th Question Attempted count");
			headerList.add("6th Question Aswered count");
			headerList.add("6th Question Attempted Percentage");
			headerList.add("6th Question Aswered Percentage");
			headerList.add("7th Question Attempted count");
			headerList.add("7th Question Aswered count");
			headerList.add("7th Question Attempted Percentage");
			headerList.add("7th Question Aswered Percentage");
			headerList.add("8th Question Attempted count");
			headerList.add("8th Question Aswered count");
			headerList.add("8th Question Attempted Percentage");
			headerList.add("8th Question Aswered Percentage");
			headerList.add("9th Question Attempted count");
			headerList.add("9th Question Aswered count");
			headerList.add("9th Question Attempted Percentage");
			headerList.add("9th Question Aswered Percentage");
			headerList.add("10th Question Attempted count");
			headerList.add("10th Question Aswered count");
			headerList.add("10th Question Attempted Percentage");
			headerList.add("10th Question Aswered Percentage");
			headerList.add("Total Question Answered");
			headerList.add("Total Question Attempted");
			headerList.add("Overall Aswer Percentage");

			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet = workbook.createSheet("ContestQuestionWiseUserAnswerCount");

			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));

			Map<String, String> reportNameMap = new LinkedHashMap<String, String>();
			reportNameMap.put("Report Name", "Contest Question Wise User Answer Count");

			int startRowCnt = 0;
			ExcelUtil.addReportTitle(reportNameMap, ssSheet, workbook, startRowCnt);

			startRowCnt = startRowCnt + 2;

			List<Object[]> dataList = LivtContestReportsDAO.getContestQuestionWiseUserAnswerCountReportData(clId,
					fromDate, toDate);
			if (dataList == null || dataList.size() <= 0) {
				ExcelUtil.generateErrorMessageExcel(ssSheet, workbook, startRowCnt, ReportConstants.NO_RECS_FOUND);
				generateExcelResponse(response, workbook);
				return;
			}
			ExcelUtil.generateExcelHeaderRowWithStyles(headerList, ssSheet, workbook, startRowCnt);

			int j = 0;
			int rowCount = startRowCnt + 1;

			for (Object[] dataObj : dataList) {

				Row rowhead = ssSheet.createRow(rowCount);
				rowCount++;
				j = 0;

				ReportsUtil.getExcelIntegerCell(dataObj[0], j, rowhead);
				j++;
				ReportsUtil.getExcelStringCell(dataObj[1], j, rowhead);
				j++;
				ReportsUtil.getExcelDateCell(dataObj[2], j, rowhead, cellStyleWithHourMinute);
				j++;
				ReportsUtil.getExcelIntegerCell(dataObj[3], j, rowhead);
				j++;
				ReportsUtil.getExcelIntegerCell(dataObj[4], j, rowhead);
				j++;

				ReportsUtil.getExcelIntegerCell(dataObj[5], j, rowhead);
				j++;
				ReportsUtil.getExcelIntegerCell(dataObj[6], j, rowhead);
				j++;
				ReportsUtil.getExcelDecimalCell(dataObj[7], j, rowhead);
				j++;
				ReportsUtil.getExcelDecimalCell(dataObj[8], j, rowhead);
				j++;

				ReportsUtil.getExcelIntegerCell(dataObj[9], j, rowhead);
				j++;
				ReportsUtil.getExcelIntegerCell(dataObj[10], j, rowhead);
				j++;
				ReportsUtil.getExcelDecimalCell(dataObj[11], j, rowhead);
				j++;
				ReportsUtil.getExcelDecimalCell(dataObj[12], j, rowhead);
				j++;

				ReportsUtil.getExcelIntegerCell(dataObj[13], j, rowhead);
				j++;
				ReportsUtil.getExcelIntegerCell(dataObj[14], j, rowhead);
				j++;
				ReportsUtil.getExcelDecimalCell(dataObj[15], j, rowhead);
				j++;
				ReportsUtil.getExcelDecimalCell(dataObj[16], j, rowhead);
				j++;

				ReportsUtil.getExcelIntegerCell(dataObj[17], j, rowhead);
				j++;
				ReportsUtil.getExcelIntegerCell(dataObj[18], j, rowhead);
				j++;
				ReportsUtil.getExcelDecimalCell(dataObj[19], j, rowhead);
				j++;
				ReportsUtil.getExcelDecimalCell(dataObj[20], j, rowhead);
				j++;

				ReportsUtil.getExcelIntegerCell(dataObj[21], j, rowhead);
				j++;
				ReportsUtil.getExcelIntegerCell(dataObj[22], j, rowhead);
				j++;
				ReportsUtil.getExcelDecimalCell(dataObj[23], j, rowhead);
				j++;
				ReportsUtil.getExcelDecimalCell(dataObj[24], j, rowhead);
				j++;

				ReportsUtil.getExcelIntegerCell(dataObj[25], j, rowhead);
				j++;
				ReportsUtil.getExcelIntegerCell(dataObj[26], j, rowhead);
				j++;
				ReportsUtil.getExcelDecimalCell(dataObj[27], j, rowhead);
				j++;
				ReportsUtil.getExcelDecimalCell(dataObj[28], j, rowhead);
				j++;

				ReportsUtil.getExcelIntegerCell(dataObj[29], j, rowhead);
				j++;
				ReportsUtil.getExcelIntegerCell(dataObj[30], j, rowhead);
				j++;
				ReportsUtil.getExcelDecimalCell(dataObj[31], j, rowhead);
				j++;
				ReportsUtil.getExcelDecimalCell(dataObj[32], j, rowhead);
				j++;

				ReportsUtil.getExcelIntegerCell(dataObj[33], j, rowhead);
				j++;
				ReportsUtil.getExcelIntegerCell(dataObj[34], j, rowhead);
				j++;
				ReportsUtil.getExcelDecimalCell(dataObj[35], j, rowhead);
				j++;
				ReportsUtil.getExcelDecimalCell(dataObj[36], j, rowhead);
				j++;

				ReportsUtil.getExcelIntegerCell(dataObj[37], j, rowhead);
				j++;
				ReportsUtil.getExcelIntegerCell(dataObj[38], j, rowhead);
				j++;
				ReportsUtil.getExcelDecimalCell(dataObj[39], j, rowhead);
				j++;
				ReportsUtil.getExcelDecimalCell(dataObj[40], j, rowhead);
				j++;

				ReportsUtil.getExcelIntegerCell(dataObj[41], j, rowhead);
				j++;
				ReportsUtil.getExcelIntegerCell(dataObj[42], j, rowhead);
				j++;
				ReportsUtil.getExcelDecimalCell(dataObj[43], j, rowhead);
				j++;
				ReportsUtil.getExcelDecimalCell(dataObj[44], j, rowhead);
				j++;

				ReportsUtil.getExcelIntegerCell(dataObj[45], j, rowhead);
				j++;
				ReportsUtil.getExcelIntegerCell(dataObj[46], j, rowhead);
				j++;
				ReportsUtil.getExcelDecimalCell(dataObj[47], j, rowhead);
				j++;
			}

			generateExcelResponse(response, workbook);

		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(respDTO, null, null);
			generateResponse(request, response, respDTO);
		}
	}

	/**
	 * Generate excel response.
	 *
	 * @param response the response
	 * @param workbook the workbook
	 * @throws Exception the exception
	 */
	public static void generateExcelResponse(HttpServletResponse response, SXSSFWorkbook workbook) throws Exception {
		response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=ContestQuestionWiseUserAnswerCount." + ReportsUtil.EXCEL_EXTENSION);
		workbook.write(response.getOutputStream());
		workbook.close();
	}
}
