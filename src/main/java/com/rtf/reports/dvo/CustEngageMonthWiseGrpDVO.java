/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.reports.dvo;

import java.io.Serializable;

/**
 * The Class CustEngageMonthWiseGrpDVO.
 */
public class CustEngageMonthWiseGrpDVO implements Serializable {

	/** The name. */
	private String name;
	
	/** The subscribers. */
	private Integer subscribers;
	
	/** The participants. */
	private Integer participants;
	
	/** The players. */
	private Integer players;
	
	/** The finishers. */
	private Integer finishers;
	
	/** The winners. */
	private Integer winners;
	
	/** The passive viewers. */
	private Integer passiveViewers;
	
	/** The finalists. */
	private Integer finalists;
	
	
	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Gets the subscribers.
	 *
	 * @return the subscribers
	 */
	public Integer getSubscribers() {
		return subscribers;
	}
	
	/**
	 * Sets the subscribers.
	 *
	 * @param subscribers the new subscribers
	 */
	public void setSubscribers(Integer subscribers) {
		this.subscribers = subscribers;
	}
	
	/**
	 * Gets the participants.
	 *
	 * @return the participants
	 */
	public Integer getParticipants() {
		return participants;
	}
	
	/**
	 * Sets the participants.
	 *
	 * @param participants the new participants
	 */
	public void setParticipants(Integer participants) {
		this.participants = participants;
	}
	
	/**
	 * Gets the players.
	 *
	 * @return the players
	 */
	public Integer getPlayers() {
		return players;
	}
	
	/**
	 * Sets the players.
	 *
	 * @param players the new players
	 */
	public void setPlayers(Integer players) {
		this.players = players;
	}
	
	/**
	 * Gets the finishers.
	 *
	 * @return the finishers
	 */
	public Integer getFinishers() {
		return finishers;
	}
	
	/**
	 * Sets the finishers.
	 *
	 * @param finishers the new finishers
	 */
	public void setFinishers(Integer finishers) {
		this.finishers = finishers;
	}
	
	/**
	 * Gets the winners.
	 *
	 * @return the winners
	 */
	public Integer getWinners() {
		return winners;
	}
	
	/**
	 * Sets the winners.
	 *
	 * @param winners the new winners
	 */
	public void setWinners(Integer winners) {
		this.winners = winners;
	}
	
	/**
	 * Gets the passive viewers.
	 *
	 * @return the passive viewers
	 */
	public Integer getPassiveViewers() {
		return passiveViewers;
	}
	
	/**
	 * Sets the passive viewers.
	 *
	 * @param passiveViewers the new passive viewers
	 */
	public void setPassiveViewers(Integer passiveViewers) {
		this.passiveViewers = passiveViewers;
	}
	
	/**
	 * Gets the finalists.
	 *
	 * @return the finalists
	 */
	public Integer getFinalists() {
		return finalists;
	}
	
	/**
	 * Sets the finalists.
	 *
	 * @param finalists the new finalists
	 */
	public void setFinalists(Integer finalists) {
		this.finalists = finalists;
	}
	
}
