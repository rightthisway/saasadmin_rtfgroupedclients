/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.shop.sql.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.rtf.saas.sql.db.DatabaseConnections;
import com.rtf.saas.util.StatusEnums;
import com.rtf.shop.dvo.CustomerCartDVO;

/**
 * The Class CustomerCartDAO.
 */
public class CustomerCartDAO {

	/**
	 * Adds the to cart.
	 *
	 * @param dvo    the dvo
	 * @param status the status
	 * @return the integer
	 */
	public static Integer addToCart(CustomerCartDVO dvo, String status) {
		Connection conn = null;
		PreparedStatement ps = null;
		Integer affectedRows = 0;
		StringBuffer sql = new StringBuffer("  insert into pt_shopm_customer_shopping_cart ( clintid,mercid,custid,mercprodid,conid ");
		sql.append(" , buyunits,buyprice,cartstatus,unittype " + " ,credate) values " + "  (? , ? , ? , ? , ? , ");
		sql.append("  ? , ? , ? , ? ," + " getDate()) ");
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, dvo.getClintId());
			ps.setInt(2, dvo.getMercId());
			ps.setString(3, dvo.getCustId());
			ps.setInt(4, dvo.getMercProdid());
			ps.setString(5, dvo.getConId());
			ps.setInt(6, dvo.getBuyUnits());
			ps.setDouble(7, dvo.getBuyPrice());
			ps.setString(8, status);
			ps.setString(9, dvo.getUnitType());
			affectedRows = ps.executeUpdate();
			return affectedRows;

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return affectedRows;
	}

	/**
	 * Update current contest cart item status.
	 *
	 * @param dvo    the dvo
	 * @param status the status
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer updateCurrentContestCartItemStatus(CustomerCartDVO dvo, String status) throws Exception {
		Connection conn = null;
		Integer affectedRows = 0;
		PreparedStatement ps = null;
		StringBuffer sql = new StringBuffer(" update  pt_shopm_customer_shopping_cart set " + " cartstatus =?, updby = ? , ");
		sql.append(" upddate=getDate()  where clintid =  ? and mercid = ? ");
		sql.append(" and mercprodid=? and custid = ? and conid=? ");
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, status);
			ps.setString(2, dvo.getUpdby());
			ps.setString(3, dvo.getClintId());
			ps.setInt(4, dvo.getMercId());
			ps.setInt(5, dvo.getMercProdid());
			ps.setString(6, dvo.getCustId());
			ps.setString(7, dvo.getConId());
			affectedRows = ps.executeUpdate();
			return affectedRows;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return affectedRows;
	}

	/**
	 * Delete item in cart.
	 *
	 * @param dvo    the dvo
	 * @param status the status
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer deleteItemInCart(CustomerCartDVO dvo, String status) throws Exception {
		Connection conn = null;
		Integer affectedRows = 0;
		PreparedStatement ps = null;
		StringBuffer sql = new StringBuffer(" update  pt_shopm_customer_shopping_cart set " + " cartstatus =?, updby = ? , ");
		sql.append(" upddate=getDate()  where clintid =  ? and mercid = ? ");
		sql.append(" and mercprodid=? and custid = ? and conid=? ");
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, status);
			ps.setString(2, dvo.getUpdby());
			ps.setString(3, dvo.getClintId());
			ps.setInt(4, dvo.getMercId());
			ps.setInt(5, dvo.getMercProdid());
			ps.setString(6, dvo.getCustId());
			ps.setString(7, dvo.getConId());
			affectedRows = ps.executeUpdate();
			return affectedRows;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return affectedRows;
	}

	/**
	 * Update previous contest active cart item status.
	 *
	 * @param dvo    the dvo
	 * @param status the status
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer updatePreviousContestActiveCartItemStatus(CustomerCartDVO dvo, String status)
			throws Exception {
		Connection conn = null;
		Integer affectedRows = 0;
		PreparedStatement ps = null;
		StringBuffer sql = new StringBuffer(" update  pt_shopm_customer_shopping_cart set " + " cartstatus =?, updby = ? , ");
		sql.append(" upddate=getDate()  where clintid =  ? and mercid = ? ");
		sql.append(" and mercprodid=? and custid = ? and conid NOT IN (?) and cartstatus='ACTIVE'  ");
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, status);
			ps.setString(2, dvo.getUpdby());
			ps.setString(3, dvo.getClintId());
			ps.setInt(4, dvo.getMercId());
			ps.setInt(5, dvo.getMercProdid());
			ps.setString(6, dvo.getCustId());
			ps.setString(7, dvo.getConId());
			affectedRows = ps.executeUpdate();
			return affectedRows;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return affectedRows;
	}

	/**
	 * Check if item exist as active in cart.
	 *
	 * @param dvo the dvo
	 * @return the boolean
	 * @throws Exception the exception
	 */
	public static Boolean checkIfItemExistAsActiveInCart(CustomerCartDVO dvo) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Boolean isExist = Boolean.FALSE;
		StringBuffer sql = new StringBuffer(" select cartstatus from  pt_shopm_customer_shopping_cart with(nolock) where  ");
		sql.append(" cartstatus = ? and  " + " clintid =  ? and mercid = ? " + " and mercprodid=? and custid = ? ");
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, StatusEnums.ACTIVE.name());
			ps.setString(2, dvo.getClintId());
			ps.setInt(3, dvo.getMercId());
			ps.setInt(4, dvo.getMercProdid());
			ps.setString(5, dvo.getCustId());
			rs = ps.executeQuery();
			while (rs.next()) {
				isExist = Boolean.TRUE;
			}
			return isExist;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * Check if item exist for contest as active in cart.
	 *
	 * @param dvo the dvo
	 * @return the boolean
	 * @throws Exception the exception
	 */
	public static Boolean checkIfItemExistForContestAsActiveInCart(CustomerCartDVO dvo) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Boolean isExist = Boolean.FALSE;
		StringBuffer sql = new StringBuffer(" select cartstatus from  pt_shopm_customer_shopping_cart with(nolock) where  ");
		sql.append(" cartstatus = ? and  " + " clintid =  ? and mercid = ? ");
		sql.append(" and mercprodid=? and custid = ? and conid = ?");
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, StatusEnums.ACTIVE.name());
			ps.setString(2, dvo.getClintId());
			ps.setInt(3, dvo.getMercId());
			ps.setInt(4, dvo.getMercProdid());
			ps.setString(5, dvo.getCustId());
			ps.setString(6, dvo.getConId());
			rs = ps.executeQuery();
			while (rs.next()) {
				isExist = Boolean.TRUE;
			}
			return isExist;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * Update cart multi item status.
	 *
	 * @param dvos   the dvos
	 * @param status the status
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer updateCartMultiItemStatus(List<CustomerCartDVO> dvos, String status) throws Exception {
		Connection conn = null;
		Integer affectedRows = 0;
		PreparedStatement ps = null;
		StringBuffer sql = new StringBuffer(" update  pt_shopm_customer_shopping_cart set " + " cartstatus =?, updby = ? , ");
		sql.append(" upddate=getDate()  where clintid =  ? and mercid = ? " + " and mercprodid=? and custid = ? ");
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			for (CustomerCartDVO dvo : dvos) {
				ps.setString(1, status);
				ps.setString(2, dvo.getUpdby());
				ps.setString(3, dvo.getClintId());
				ps.setInt(4, dvo.getMercId());
				ps.setInt(5, dvo.getMercProdid());
				ps.setString(6, dvo.getCustId());
				affectedRows = +ps.executeUpdate();
			}
			return affectedRows;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return affectedRows;
	}

	/**
	 * Fetch customer cart.
	 *
	 * @param dvo the dvo
	 * @return the list
	 * @throws Exception the exception
	 */
	public static List<CustomerCartDVO> fetchCustomerCart(CustomerCartDVO dvo) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<CustomerCartDVO> cartList = null;

		StringBuffer sql = new StringBuffer(" select a.custid as custid, a.mercid as mercid, a.clintid as clintid, a.mercprodid as mercprodid ,a.buyunits as buyunits,");
		sql.append(" b.priceperunit as buyprice ,b.unittype as unittype , ");
		sql.append(" b.prodname as prodname , b.prodidselrsite  as prodidselrsite," + " b.prodimageurl as prodimageurl, ");
		sql.append(" c.mrktplce as mrktplce, c.mercselrsite as mercselrsite from ");
		sql.append(" pt_shopm_customer_shopping_cart a " + " join pa_shopm_merchant_products b ");
		sql.append(" on a.mercid = b.mercid and a.clintid=b.clintid ");
		sql.append(" join pr_shopm_client_merchant c on  a.mercid=c.mercid and " + " a.clintid=b.clintid  ");
		sql.append(" where a.cartstatus = ? and  " + " a.clintid =  ? and a.custid = ?  ");

		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, StatusEnums.ACTIVE.name());
			ps.setString(2, dvo.getClintId());
			ps.setString(3, dvo.getCustId());
			rs = ps.executeQuery();
			cartList = new ArrayList<CustomerCartDVO>();
			while (rs.next()) {
				CustomerCartDVO cdvo = new CustomerCartDVO();
				cdvo.setMercProdid(rs.getInt("mercprodid"));
				cdvo.setBuyUnits(rs.getInt("buyunits"));
				cdvo.setBuyPrice(rs.getDouble("buyprice"));
				cdvo.setUnitType(rs.getString("unittype"));
				cdvo.setProdName("prodname");
				cdvo.setProdIdSelrsite("prodidselrsite");
				cdvo.setProdImgUrl("prodimageurl");
				cdvo.setMrktPlce("mrktplce");
				cdvo.setMercSelrSite("mercselrsite");
				cdvo.setClintId(rs.getString("clintid"));
				cdvo.setMercId(rs.getInt("mercid"));
				cdvo.setCustId(rs.getString("custid"));
				cartList.add(cdvo);
			}
			return cartList;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return cartList;
	}

	/**
	 * Fetch all customer in cart.
	 *
	 * @return the map
	 * @throws Exception the exception
	 */
	public static Map<String, String> fetchAllCustomerInCart() throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Map<String, String> cartCustMap = null;

		StringBuffer sql = new StringBuffer(" select distinct (custid ) as custid  , clintid from " + " pt_shopm_customer_shopping_cart  ");
		sql.append(" where cartstatus =? ");

		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, StatusEnums.ACTIVE.name());
			rs = ps.executeQuery();
			cartCustMap = new HashMap<String, String>();
			while (rs.next()) {
				cartCustMap.put(rs.getString("custid"), rs.getString("clintid"));
			}
			return cartCustMap;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return cartCustMap;
	}

}
