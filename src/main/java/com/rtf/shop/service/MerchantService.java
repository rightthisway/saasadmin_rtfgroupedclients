/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.shop.service;

import java.util.List;

import com.rtf.shop.dto.MerchantDTO;
import com.rtf.shop.dvo.MerchantDVO;
import com.rtf.shop.sql.dao.MerchantDAO;

/**
 * The Class MerchantService.
 */
public class MerchantService {

	/**
	 * Fetch merchant list.
	 *
	 * @param dto    the dto
	 * @param filter the filter
	 * @param pgNo   the pg no
	 * @return the merchant DTO
	 */
	public static MerchantDTO fetchMerchantList(MerchantDTO dto, String filter, String pgNo) {

		List<MerchantDVO> lst = null;
		try {

			lst = MerchantDAO.getAllMerchants(dto.getClId());
			dto.setMerlist(lst);
			dto.setSts(1);
		} catch (Exception ex) {
			dto.setSts(0);
			ex.printStackTrace();
		}
		return dto;
	}

	/**
	 * Gets the all merchant data to export.
	 *
	 * @param clId   the cl id
	 * @param filter the filter
	 * @return the all merchant data to export
	 */
	public static List<MerchantDVO> getAllMerchantDataToExport(String clId, String filter) {
		List<MerchantDVO> list = null;
		try {
			list = MerchantDAO.getAllMerchantDataToExport(clId, filter);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return list;
	}

	/**
	 * Delete merchant.
	 *
	 * @param dto the dto
	 * @return the merchant DTO
	 * @throws Exception the exception
	 */
	public static MerchantDTO deleteMerchant(MerchantDTO dto) throws Exception {
		try {
			dto.setSts(0);
			MerchantDVO dvo = dto.getDvo();
			Integer updCnt = MerchantDAO.deleteMerchant(dvo);
			dto.setSts(updCnt);
		} catch (Exception ex) {
			dto.setSts(0);
			ex.printStackTrace();
		}
		return dto;
	}

	/**
	 * Update merchant.
	 *
	 * @param dto the dto
	 * @return the merchant DTO
	 * @throws Exception the exception
	 */
	public static MerchantDTO updateMerchant(MerchantDTO dto) throws Exception {
		try {
			dto.setSts(0);
			MerchantDVO dvo = dto.getDvo();
			Integer updCnt = MerchantDAO.updateMerchant(dvo);
			dto.setSts(updCnt);
		} catch (Exception ex) {
			dto.setSts(0);
			ex.printStackTrace();
		}
		return dto;
	}

	/**
	 * Creates the merchant.
	 *
	 * @param dto the dto
	 * @return the merchant DTO
	 * @throws Exception the exception
	 */
	public static MerchantDTO createMerchant(MerchantDTO dto) throws Exception {
		try {
			dto.setSts(0);
			MerchantDVO dvo = dto.getDvo();
			Integer updCnt = MerchantDAO.createMerchant(dvo);
			dto.setSts(updCnt);
		} catch (Exception ex) {
			dto.setSts(0);
			ex.printStackTrace();
		}
		return dto;
	}

}
