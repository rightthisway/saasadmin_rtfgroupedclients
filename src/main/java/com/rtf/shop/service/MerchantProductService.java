/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.shop.service;

import java.util.List;

import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.PaginationUtil;
import com.rtf.shop.dto.MerchantProductDTO;
import com.rtf.shop.dvo.ProductsDVO;
import com.rtf.shop.sql.dao.MerchantProductsDAO;
import com.rtf.shop.util.ShopGridHeaderFilterUtil;

/**
 * The Class MerchantProductService.
 */
public class MerchantProductService {

	/**
	 * Fetch merchant product list.
	 *
	 * @param dto    the dto
	 * @param filter the filter
	 * @param pgNo   the pg no
	 * @param mercId the merc id
	 * @param status the status
	 * @return the merchant product DTO
	 */
	public static MerchantProductDTO fetchMerchantProductList(MerchantProductDTO dto, String filter, String pgNo,
			Integer mercId, String status) {

		List<ProductsDVO> lst = null;
		try {
			String filterQuery = ShopGridHeaderFilterUtil.getMerchantProductFilterQuery(filter);
			lst = MerchantProductsDAO.getAllMerchantProductswithFilter(dto.getClId(), mercId, filterQuery, pgNo,
					status);
			Integer count = MerchantProductsDAO.getAllMerchantProductCountwithFilter(dto.getClId(), mercId, filterQuery,
					status);

			dto.setPagination(PaginationUtil.calculatePaginationParameter(count, pgNo));
			dto.setProdlist(lst);
			dto.setSts(1);
		} catch (Exception ex) {
			dto.setSts(0);
			ex.printStackTrace();
		}
		return dto;
	}

	/**
	 * Gets the merchant product by id.
	 *
	 * @param clientId the client id
	 * @param mercId   the merc id
	 * @param prodId   the prod id
	 * @return the merchant product by id
	 */
	public static ProductsDVO getMerchantProductById(String clientId, Integer mercId, Integer prodId) {
		ProductsDVO product = null;
		try {
			product = MerchantProductsDAO.getMerchantProductById(clientId, mercId, prodId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return product;
	}

	/**
	 * Fetch all merchant product list.
	 *
	 * @param dto    the dto
	 * @param mercId the merc id
	 * @param status the status
	 * @param filter the filter
	 * @return the merchant product DTO
	 */
	public static MerchantProductDTO fetchAllMerchantProductList(MerchantProductDTO dto, Integer mercId, String status,
			String filter) {

		List<ProductsDVO> lst = null;
		try {
			lst = MerchantProductsDAO.getAllMerchantProducts(dto.getClId(), mercId, status, filter);

			dto.setProdlist(lst);
			dto.setSts(1);
		} catch (Exception ex) {
			dto.setSts(0);
			ex.printStackTrace();
		}
		return dto;
	}

	/**
	 * Gets the merchant product data to export.
	 *
	 * @param clId   the cl id
	 * @param mercId the merc id
	 * @param status the status
	 * @param filter the filter
	 * @return the merchant product data to export
	 */
	public static List<ProductsDVO> getMerchantProductDataToExport(String clId, Integer mercId, String status,
			String filter) {
		List<ProductsDVO> lst = null;
		try {
			lst = MerchantProductsDAO.getAllMerchantProducts(clId, mercId, status, filter);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return lst;
	}

	/**
	 * Delete merchant product.
	 *
	 * @param dto the dto
	 * @return the merchant product DTO
	 * @throws Exception the exception
	 */
	public static MerchantProductDTO deleteMerchantProduct(MerchantProductDTO dto) throws Exception {
		try {
			dto.setSts(0);
			ProductsDVO dvo = dto.getDvo();
			Integer updCnt = MerchantProductsDAO.deleteMerchantProduct(dvo);
			dto.setSts(updCnt);
		} catch (Exception ex) {
			dto.setSts(0);
			ex.printStackTrace();
		}
		return dto;
	}

	/**
	 * Update merchant product.
	 *
	 * @param dto the dto
	 * @return the merchant product DTO
	 * @throws Exception the exception
	 */
	public static MerchantProductDTO updateMerchantProduct(MerchantProductDTO dto) throws Exception {
		try {
			dto.setSts(0);
			ProductsDVO dvo = dto.getDvo();
			Integer updCnt = 0;
			String imgUrl = dvo.getProdImgUrl();
			if (GenUtil.isNullOrEmpty(imgUrl)) {
				updCnt = MerchantProductsDAO.updateMerchantProductWithOutImage(dvo);
			} else {
				updCnt = MerchantProductsDAO.updateMerchantProduct(dvo);
			}

			dto.setSts(updCnt);

		} catch (Exception ex) {
			dto.setSts(0);
			ex.printStackTrace();
		}
		return dto;
	}

	/**
	 * Creates the merchant product.
	 *
	 * @param dto the dto
	 * @return the merchant product DTO
	 * @throws Exception the exception
	 */
	public static MerchantProductDTO createMerchantProduct(MerchantProductDTO dto) throws Exception {
		try {
			dto.setSts(0);
			ProductsDVO dvo = dto.getDvo();
			Integer updCnt = MerchantProductsDAO.createMerchantProduct(dvo);
			dto.setSts(updCnt);
		} catch (Exception ex) {
			dto.setSts(0);
			ex.printStackTrace();
		}
		return dto;
	}

}
