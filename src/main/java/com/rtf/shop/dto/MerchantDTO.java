/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.shop.dto;

import java.util.List;

import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.dto.PaginationDTO;
import com.rtf.shop.dvo.MerchantDVO;

/**
 * The Class MerchantDTO.
 */
public class MerchantDTO extends RtfSaasBaseDTO {

	/** The merlist. */
	private List<MerchantDVO> merlist;

	/** The dvo. */
	private MerchantDVO dvo;

	/** The pagination. */
	private PaginationDTO pagination;

	/**
	 * Gets the merlist.
	 *
	 * @return the merlist
	 */
	public List<MerchantDVO> getMerlist() {
		return merlist;
	}

	/**
	 * Sets the merlist.
	 *
	 * @param merlist the new merlist
	 */
	public void setMerlist(List<MerchantDVO> merlist) {
		this.merlist = merlist;
	}

	/**
	 * Gets the dvo.
	 *
	 * @return the dvo
	 */
	public MerchantDVO getDvo() {
		return dvo;
	}

	/**
	 * Sets the dvo.
	 *
	 * @param dvo the new dvo
	 */
	public void setDvo(MerchantDVO dvo) {
		this.dvo = dvo;
	}

	/**
	 * Gets the pagination.
	 *
	 * @return the pagination
	 */
	public PaginationDTO getPagination() {
		return pagination;
	}

	/**
	 * Sets the pagination.
	 *
	 * @param pagination the new pagination
	 */
	public void setPagination(PaginationDTO pagination) {
		this.pagination = pagination;
	}

}
