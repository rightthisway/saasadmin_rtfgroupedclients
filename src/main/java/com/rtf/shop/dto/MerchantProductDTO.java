/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.shop.dto;

import java.util.List;

import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.dto.PaginationDTO;
import com.rtf.shop.dvo.ProductsDVO;

/**
 * The Class MerchantProductDTO.
 */
public class MerchantProductDTO extends RtfSaasBaseDTO {

	/** The prodlist. */
	private List<ProductsDVO> prodlist;

	/** The dvo. */
	private ProductsDVO dvo;

	/** The pagination. */
	private PaginationDTO pagination;

	/**
	 * Gets the prodlist.
	 *
	 * @return the prodlist
	 */
	public List<ProductsDVO> getProdlist() {
		return prodlist;
	}

	/**
	 * Sets the prodlist.
	 *
	 * @param prodlist the new prodlist
	 */
	public void setProdlist(List<ProductsDVO> prodlist) {
		this.prodlist = prodlist;
	}

	/**
	 * Gets the dvo.
	 *
	 * @return the dvo
	 */
	public ProductsDVO getDvo() {
		return dvo;
	}

	/**
	 * Sets the dvo.
	 *
	 * @param dvo the new dvo
	 */
	public void setDvo(ProductsDVO dvo) {
		this.dvo = dvo;
	}

	/**
	 * Gets the pagination.
	 *
	 * @return the pagination
	 */
	public PaginationDTO getPagination() {
		return pagination;
	}

	/**
	 * Sets the pagination.
	 *
	 * @param pagination the new pagination
	 */
	public void setPagination(PaginationDTO pagination) {
		this.pagination = pagination;
	}

}
