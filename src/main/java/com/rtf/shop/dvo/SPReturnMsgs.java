package com.rtf.shop.dvo;

public class SPReturnMsgs {
	
	public String getErrNum() {
		return errNum;
	}

	public void setErrNum(String errNum) {
		this.errNum = errNum;
	}

	public String getErrTxt() {
		return errTxt;
	}

	public void setErrTxt(String errTxt) {
		this.errTxt = errTxt;
	}

	private String errNum;
	
	private String errTxt;

}
