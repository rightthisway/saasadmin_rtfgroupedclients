/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.shop.dvo;

import com.rtf.saas.util.DateFormatUtil;

/**
 * The Class MerchantDVO.
 */
public class MerchantDVO {

	/** The clintid. */
	private String clintid;

	/** The merc id. */
	private Integer mercId;

	/** The merc selr site. */
	private String mercSelrSite;

	/** The merc fname. */
	private String mercFname;

	/** The merc lname. */
	private String mercLname;

	/** The merc email. */
	private String mercEmail;

	/** The merc phone. */
	private String mercPhone;

	/** The addra one. */
	private String addraOne;

	/** The addrb two. */
	private String addrbTwo;

	/** The addrc three. */
	private String addrcThree;

	/** The addrd city. */
	private String addrdCity;

	/** The addre state. */
	private String addreState;

	/** The addrf cntry. */
	private String addrfCntry;

	/** The mrkt plce. */
	private String mrktPlce;

	/** The merc url. */
	private String mercUrl;

	/** The merc status. */
	private String mercStatus;

	/** The creby. */
	private String creby;

	/** The credate. */
	private java.util.Date credate;

	/** The updby. */
	private String updby;

	/** The upddate. */
	private java.util.Date upddate;

	/** The cr date time str. */
	private String crDateTimeStr;

	/** The up date time str. */
	private String upDateTimeStr;

	/**
	 * Gets the clintid.
	 *
	 * @return the clintid
	 */
	public String getClintid() {
		return clintid;
	}

	/**
	 * Sets the clintid.
	 *
	 * @param clintid the new clintid
	 */
	public void setClintid(String clintid) {
		this.clintid = clintid;
	}

	/**
	 * Gets the merc id.
	 *
	 * @return the merc id
	 */
	public Integer getMercId() {
		return mercId;
	}

	/**
	 * Sets the merc id.
	 *
	 * @param mercId the new merc id
	 */
	public void setMercId(Integer mercId) {
		this.mercId = mercId;
	}

	/**
	 * Gets the merc selr site.
	 *
	 * @return the merc selr site
	 */
	public String getMercSelrSite() {
		return mercSelrSite;
	}

	/**
	 * Sets the merc selr site.
	 *
	 * @param mercSelrSite the new merc selr site
	 */
	public void setMercSelrSite(String mercSelrSite) {
		this.mercSelrSite = mercSelrSite;
	}

	/**
	 * Gets the merc fname.
	 *
	 * @return the merc fname
	 */
	public String getMercFname() {
		return mercFname;
	}

	/**
	 * Sets the merc fname.
	 *
	 * @param mercFname the new merc fname
	 */
	public void setMercFname(String mercFname) {
		this.mercFname = mercFname;
	}

	/**
	 * Gets the merc lname.
	 *
	 * @return the merc lname
	 */
	public String getMercLname() {
		return mercLname;
	}

	/**
	 * Sets the merc lname.
	 *
	 * @param mercLname the new merc lname
	 */
	public void setMercLname(String mercLname) {
		this.mercLname = mercLname;
	}

	/**
	 * Gets the merc email.
	 *
	 * @return the merc email
	 */
	public String getMercEmail() {
		return mercEmail;
	}

	/**
	 * Sets the merc email.
	 *
	 * @param mercEmail the new merc email
	 */
	public void setMercEmail(String mercEmail) {
		this.mercEmail = mercEmail;
	}

	/**
	 * Gets the merc phone.
	 *
	 * @return the merc phone
	 */
	public String getMercPhone() {
		return mercPhone;
	}

	/**
	 * Sets the merc phone.
	 *
	 * @param mercPhone the new merc phone
	 */
	public void setMercPhone(String mercPhone) {
		this.mercPhone = mercPhone;
	}

	/**
	 * Gets the addra one.
	 *
	 * @return the addra one
	 */
	public String getAddraOne() {
		return addraOne;
	}

	/**
	 * Sets the addra one.
	 *
	 * @param addraOne the new addra one
	 */
	public void setAddraOne(String addraOne) {
		this.addraOne = addraOne;
	}

	/**
	 * Gets the addrb two.
	 *
	 * @return the addrb two
	 */
	public String getAddrbTwo() {
		return addrbTwo;
	}

	/**
	 * Sets the addrb two.
	 *
	 * @param addrbTwo the new addrb two
	 */
	public void setAddrbTwo(String addrbTwo) {
		this.addrbTwo = addrbTwo;
	}

	/**
	 * Gets the addrc three.
	 *
	 * @return the addrc three
	 */
	public String getAddrcThree() {
		return addrcThree;
	}

	/**
	 * Sets the addrc three.
	 *
	 * @param addrcThree the new addrc three
	 */
	public void setAddrcThree(String addrcThree) {
		this.addrcThree = addrcThree;
	}

	/**
	 * Gets the addrd city.
	 *
	 * @return the addrd city
	 */
	public String getAddrdCity() {
		return addrdCity;
	}

	/**
	 * Sets the addrd city.
	 *
	 * @param addrdCity the new addrd city
	 */
	public void setAddrdCity(String addrdCity) {
		this.addrdCity = addrdCity;
	}

	/**
	 * Gets the addre state.
	 *
	 * @return the addre state
	 */
	public String getAddreState() {
		return addreState;
	}

	/**
	 * Sets the addre state.
	 *
	 * @param addreState the new addre state
	 */
	public void setAddreState(String addreState) {
		this.addreState = addreState;
	}

	/**
	 * Gets the addrf cntry.
	 *
	 * @return the addrf cntry
	 */
	public String getAddrfCntry() {
		return addrfCntry;
	}

	/**
	 * Sets the addrf cntry.
	 *
	 * @param addrfCntry the new addrf cntry
	 */
	public void setAddrfCntry(String addrfCntry) {
		this.addrfCntry = addrfCntry;
	}

	/**
	 * Gets the mrkt plce.
	 *
	 * @return the mrkt plce
	 */
	public String getMrktPlce() {
		return mrktPlce;
	}

	/**
	 * Sets the mrkt plce.
	 *
	 * @param mrktPlce the new mrkt plce
	 */
	public void setMrktPlce(String mrktPlce) {
		this.mrktPlce = mrktPlce;
	}

	/**
	 * Gets the merc url.
	 *
	 * @return the merc url
	 */
	public String getMercUrl() {
		return mercUrl;
	}

	/**
	 * Sets the merc url.
	 *
	 * @param mercUrl the new merc url
	 */
	public void setMercUrl(String mercUrl) {
		this.mercUrl = mercUrl;
	}

	/**
	 * Gets the merc status.
	 *
	 * @return the merc status
	 */
	public String getMercStatus() {
		return mercStatus;
	}

	/**
	 * Sets the merc status.
	 *
	 * @param mercStatus the new merc status
	 */
	public void setMercStatus(String mercStatus) {
		this.mercStatus = mercStatus;
	}

	/**
	 * Gets the creby.
	 *
	 * @return the creby
	 */
	public String getCreby() {
		return creby;
	}

	/**
	 * Sets the creby.
	 *
	 * @param creby the new creby
	 */
	public void setCreby(String creby) {
		this.creby = creby;
	}

	/**
	 * Gets the credate.
	 *
	 * @return the credate
	 */
	public java.util.Date getCredate() {
		return credate;
	}

	/**
	 * Sets the credate.
	 *
	 * @param credate the new credate
	 */
	public void setCredate(java.util.Date credate) {
		this.credate = credate;
	}

	/**
	 * Gets the updby.
	 *
	 * @return the updby
	 */
	public String getUpdby() {
		return updby;
	}

	/**
	 * Sets the updby.
	 *
	 * @param updby the new updby
	 */
	public void setUpdby(String updby) {
		this.updby = updby;
	}

	/**
	 * Gets the upddate.
	 *
	 * @return the upddate
	 */
	public java.util.Date getUpddate() {
		return upddate;
	}

	/**
	 * Sets the upddate.
	 *
	 * @param upddate the new upddate
	 */
	public void setUpddate(java.util.Date upddate) {
		this.upddate = upddate;
	}

	/**
	 * Gets the cr date time str.
	 *
	 * @return the cr date time str
	 */
	public String getCrDateTimeStr() {
		crDateTimeStr = DateFormatUtil.getDateMMDDYYYYYHHmmss(credate);
		return crDateTimeStr;
	}

	/**
	 * Sets the cr date time str.
	 *
	 * @param crDateTimeStr the new cr date time str
	 */
	public void setCrDateTimeStr(String crDateTimeStr) {
		this.crDateTimeStr = crDateTimeStr;
	}

	/**
	 * Gets the up date time str.
	 *
	 * @return the up date time str
	 */
	public String getUpDateTimeStr() {
		upDateTimeStr = DateFormatUtil.getDateMMDDYYYYYHHmmss(upddate);
		return upDateTimeStr;
	}

	/**
	 * Sets the up date time str.
	 *
	 * @param upDateTimeStr the new up date time str
	 */
	public void setUpDateTimeStr(String upDateTimeStr) {
		this.upDateTimeStr = upDateTimeStr;
	}

}