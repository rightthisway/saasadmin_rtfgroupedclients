/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.shop.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import com.rtf.inventory.seller.dvo.SellerDVO;
import com.rtf.inventory.seller.service.SellerService;
import com.rtf.livt.firestore.FireStoreReferenceHolder;
import com.rtf.livt.firestore.LiveFirestore;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.sql.dao.RewardTypeDAO;
import com.rtf.saas.sql.dvo.RewardTypeDVO;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;
import com.rtf.shop.dto.ContestProductDTO;
import com.rtf.shop.dvo.ContestProductSetsDVO;
import com.rtf.shop.service.ShopContestProductSetService;
import com.rtf.shop.util.ShopMessageConstant;
import com.rtf.user.util.UserMsgConstants;

/**
 * The Class ShopDisplayContestProductsServlet.
 */
@WebServlet("/shopdisplaycontestproducts.json")
public class ShopDisplayContestProductsServlet extends RtfSaasBaseServlet {

	/**
	 * Serial version id
	 */
	private static final long serialVersionUID = 2250812800805571959L;

	/**
	 * Generate Http Response for contest Products Response data is sent in JSON format.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * display selected contest products in live game.
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String clId = request.getParameter("clId");
		String coId = request.getParameter("coId");
		String merchId = request.getParameter("merchId");
		String prodIdsStr = request.getParameter("prodIds");
		String cau = request.getParameter("cau");

		ContestProductDTO respDTO = new ContestProductDTO();
		respDTO.setSts(0);
		respDTO.setClId(clId);

		try {
			if (GenUtil.isNullOrEmpty(clId)) {
				setClientMessage(respDTO, UserMsgConstants.INVALID_CLIENT, null);
				generateResponse(request, response, respDTO);
				return;
			}

			if (GenUtil.isNullOrEmpty(coId)) {
				setClientMessage(respDTO, ShopMessageConstant.INVALID_CONTEST_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}

			if (GenUtil.isNullOrEmpty(prodIdsStr)) {
				setClientMessage(respDTO, ShopMessageConstant.INVALID_PRODUCT_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}

			if (GenUtil.isNullOrEmpty(merchId)) {
				setClientMessage(respDTO, ShopMessageConstant.INVALID_MERCHANT_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}

			Integer mercId = 0;
			try {
				mercId = Integer.parseInt(merchId);
			} catch (Exception e) {
				e.printStackTrace();
				setClientMessage(respDTO, ShopMessageConstant.INVALID_MERCHANT_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}

			String[] prodIds = prodIdsStr.split(",");
			if (prodIds.length == 0) {
				setClientMessage(respDTO, ShopMessageConstant.INVALID_PRODUCT_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}
			List<ContestProductSetsDVO> products = new ArrayList<ContestProductSetsDVO>();
			Date today = new Date();
			for (String strId : prodIds) {
				ContestProductSetsDVO prod = ShopContestProductSetService.getContestProductSetsByProdId(clId, coId,
						mercId, Integer.parseInt(strId));
				if (prod == null) {
					continue;
				}
				prod.setDispTime(today);
				prod.setUpdby(cau);
				products.add(prod);
				ShopContestProductSetService.updateContestProductDisplayTime(prod);
			}
			if (!products.isEmpty()) {
				
				
				Integer slerId = products.get(0).getMercId();
				System.out.println("[XXXXXXXXX]  Seller Id for Product from Contest is " + slerId);
				SellerDVO dvo = new SellerDVO();
				dvo.setClintId(clId);
				dvo.setSlerId(slerId);
				dvo = SellerService.fetchSellerDetsBySlerId( dvo) ;
				Map<String, Object> datamap = new HashMap<String, Object>();
				String shopifySellerType = dvo.getSellerType();
				String storeName = dvo.getApibaseurl();
				if(storeName == null ) storeName = StringUtils.EMPTY;
				storeName = storeName.trim();
				if(shopifySellerType == null ) shopifySellerType = StringUtils.EMPTY;
				if(shopifySellerType.equalsIgnoreCase("SHOPIFY") || shopifySellerType.equalsIgnoreCase("SHOPIFYPRIVATE") ) 
				{
					shopifySellerType = "SHOPIFY";
				}
				datamap.put("sellerType", shopifySellerType);
				datamap.put("storeURL", "https://" + storeName);
				System.out.println(" {****** SELLER INFO MAP } " + datamap );
				Boolean isDec = true;
				if (products.get(0).getPriceType() != null && !products.get(0).getPriceType().isEmpty()) {
					RewardTypeDVO rType = RewardTypeDAO.getRewardType(products.get(0).getClintId(),
							products.get(0).getPriceType());
					if (rType != null && rType.getUnit() != null && rType.getUnit().equalsIgnoreCase("NUMBERS")) {
						isDec = false;
					}
				}
				List<LiveFirestore> firestores = FireStoreReferenceHolder.getReference(clId);
				if (firestores == null || firestores.isEmpty()) {
					setClientMessage(respDTO, ShopMessageConstant.FIRESTORE_CONFIG_NOT_EXISTS, null);
					generateResponse(request, response, respDTO);
					return;
				}
				for (LiveFirestore firestore : firestores) {
					firestore.setShopingProducts(products, isDec,datamap);
					firestore.updateProductForHost(true);
					//firestore.updateSellerDets(datamap);
				}
				respDTO.setSts(1);
				setClientMessage(respDTO, null, ShopMessageConstant.PRODUCT_DISPLAYED_SUCCESS_MSG);
				generateResponse(request, response, respDTO);
				return;
			} else {
				setClientMessage(respDTO, ShopMessageConstant.PRODUCT_ID_NOT_EXIST, null);
				generateResponse(request, response, respDTO);
				return;
			}

		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(respDTO, null, null);
			generateResponse(request, response, respDTO);
		}
		return;
	}

}
