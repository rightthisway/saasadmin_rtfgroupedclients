package com.rtf.shop.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;
import com.rtf.shop.dto.ContestProductDTO;
import com.rtf.shop.dvo.SPReturnMsgs;
import com.rtf.shop.service.ShopContestProductSetService;
import com.rtf.shop.sql.ContestProductSetSQLDAO;
import com.rtf.shop.util.ShopMessageConstant;
import com.rtf.user.util.UserMsgConstants;

/**
 * The Class ShopAddContestProductSetServlet.
 */
@WebServlet("/rtfsaasactivatecontestproducts.json")
public class RTFActivateContestProducts  extends RtfSaasBaseServlet{

	/**
	 * Generate HTTP Response for contest Products Response data is sent in JSON format.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the RTF SaaS base DTO
	 * @throws ServletException the SERVLET exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}
	
	
	/**
	 * Add products to contest
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String clId = request.getParameter("clId");
		String coId = request.getParameter("coId");
		String cau = request.getParameter("cau");
		Integer prodCount = 0;
		Integer dateDiffMins = 0;
		Integer minMins = -60;
		Integer maxMins = 40;
		String errmsg = null;

		ContestProductDTO respDTO = new ContestProductDTO();
		respDTO.setSts(0);
		respDTO.setClId(clId);
		
		try {
			if (GenUtil.isNullOrEmpty(clId)) {
				setClientMessage(respDTO, UserMsgConstants.INVALID_CLIENT, null);
				generateResponse(request, response, respDTO);
				return;
			}

			if (GenUtil.isNullOrEmpty(coId)) {
				setClientMessage(respDTO, ShopMessageConstant.INVALID_CONTEST_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}

			if (GenUtil.isNullOrEmpty(cau)) {
				setClientMessage(respDTO, ShopMessageConstant.INVALID_USER_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}
			
			dateDiffMins = ContestProductSetSQLDAO.getContNServrDateDiffinHRS(clId, coId);
			if ( (dateDiffMins < minMins) || (dateDiffMins > maxMins) ){
				setClientMessage(respDTO, ShopMessageConstant.PRODUCTS_ACTIVATION_TIME, null);
				generateResponse(request, response, respDTO);
				return;
			}

			prodCount = ContestProductSetSQLDAO.getContestProductsCnt(clId, coId);
			if (prodCount != 0){
				System.out.println("BEFORE SERVICE CALL ShopContestProductSetService.activateContestProducts  : "+coId+" | "+cau);
				SPReturnMsgs msg = ShopContestProductSetService.activateContestProducts(clId,coId, cau);
				respDTO.setSpMsgDVO(msg);
				if (msg!= null ) {
					String status = msg.getErrNum();
					if ("0".equals(status)){
						respDTO.setSts(1);
					} else {
						errmsg = msg.getErrTxt();
					}
				}
			} else {
				errmsg = ShopMessageConstant.NO_PRODUCT_FOUNF_FOR_CONTEST;
			}
			setClientMessage(respDTO, errmsg, ShopMessageConstant.PRODUCTS_ACTIVATED);
			generateResponse(request, response, respDTO);
			return;
		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(respDTO, UserMsgConstants.COMMON_ERR_MSG, null);
			generateResponse(request, response, respDTO);
		}
		return;
	}
		
		
}

