/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.shop.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;
import com.rtf.shop.dto.CustomerCartDTO;
import com.rtf.shop.dvo.CustomerCartDVO;
import com.rtf.shop.service.CustomerCartService;
import com.rtf.shop.util.ShopMessageConstant;
import com.rtf.user.util.UserMsgConstants;

/**
 * The Class ShopAddToCustomerCartServlet.
 */
@WebServlet("/shopaddtocustcart.json")
public class ShopAddToCustomerCartServlet extends RtfSaasBaseServlet {

	/**
	 * Serial version id
	 */
	private static final long serialVersionUID = -9119343078100117063L;

	/**
	 * Generate Http Response for contest Products Response data is sent in JSON format.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * Add product to customer cart to purchase .
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String clId = request.getParameter("clId");
		String prodId = request.getParameter("prodId");
		String userId = request.getParameter("cau");
		String conId = request.getParameter("conId");
		String cuId = request.getParameter("cuId");
		Integer mercprodId = null;

		CustomerCartDTO dto = new CustomerCartDTO();
		dto.setSts(0);
		dto.setClId(clId);

		try {

			if (GenUtil.isNullOrEmpty(clId)) {
				setClientMessage(dto, UserMsgConstants.INVALID_CLIENT, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(userId)) {
				setClientMessage(dto, UserMsgConstants.INVALID_ADMIN_USER_ID, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(prodId)) {
				setClientMessage(dto, ShopMessageConstant.PRODUCT_ID_IS_MANDATORY, null);
				generateResponse(request, response, dto);
				return;
			}
			try {
				mercprodId = Integer.parseInt(prodId);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			if (GenUtil.isNullOrEmpty(conId)) {
				setClientMessage(dto,  ShopMessageConstant.CONTEST_ID_IS_MANDATORY, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(cuId)) {
				setClientMessage(dto, ShopMessageConstant.CUST_ID_IS_MANDATORY, null);
				generateResponse(request, response, dto);
				return;
			}

			CustomerCartDVO dvo = new CustomerCartDVO();
			dvo.setClintId(clId);
			dvo.setMercProdid(mercprodId);
			dvo.setConId(conId);
			dvo.setCustId(cuId);
			dto.setDvo(dvo);
			dto = CustomerCartService.addToCustomerCart(dto);
			if (dto.getSts() == 0) {
				setClientMessage(dto, null, null);
				generateResponse(request, response, dto);
				return;
			}
			generateResponse(request, response, dto);
			return;
		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(dto, null, null);
			generateResponse(request, response, dto);
		}
		return;
	}
}