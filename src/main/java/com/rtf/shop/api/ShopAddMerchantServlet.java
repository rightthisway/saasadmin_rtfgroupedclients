/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.shop.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;
import com.rtf.shop.dto.MerchantDTO;
import com.rtf.shop.dvo.MerchantDVO;
import com.rtf.shop.service.MerchantService;
import com.rtf.shop.util.ShopMessageConstant;
import com.rtf.user.util.UserMsgConstants;

/**
 * The Class ShopAddMerchantServlet.
 */
@WebServlet("/shopaddmerchant.json")
public class ShopAddMerchantServlet extends RtfSaasBaseServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3395464741348142292L;

	/**
	 * Generate Http Response for contest Products Response data is sent in JSON format.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * Add Merchant in system
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String clId = request.getParameter("clId");
		String mercselrsite = request.getParameter("mercselrsite");
		String mrktplce = request.getParameter("mrktplce");
		String mercurl = request.getParameter("mercurl");
		String mercemail = request.getParameter("mercemail");
		String mercphone = request.getParameter("mercphone");
		String mercfname = request.getParameter("mercfname");
		String merclname = request.getParameter("merclname");
		String userId = request.getParameter("cau");

		MerchantDTO dto = new MerchantDTO();
		dto.setSts(0);
		dto.setClId(clId);

		try {

			if (GenUtil.isNullOrEmpty(clId)) {
				setClientMessage(dto, UserMsgConstants.INVALID_CLIENT, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(userId)) {
				setClientMessage(dto, UserMsgConstants.INVALID_ADMIN_USER_ID, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(mercemail)) {
				setClientMessage(dto, ShopMessageConstant.EMAIL_IS_MANDATORY, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(mercphone)) {
				setClientMessage(dto, ShopMessageConstant.PHONE_IS_MANDATORY, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(mrktplce)) {
				setClientMessage(dto, ShopMessageConstant.MARKET_PLACE_IS_MANDATORY, null);
				generateResponse(request, response, dto);
				return;
			}

			MerchantDVO dvo = new MerchantDVO();
			dvo.setClintid(clId);
			dvo.setMercSelrSite(mercselrsite);
			dvo.setMrktPlce(mrktplce);
			dvo.setMercUrl(mercurl);
			dvo.setMercEmail(mercemail);
			dvo.setMercPhone(mercphone);
			dvo.setMercFname(mercfname);
			dvo.setMercLname(merclname);
			dvo.setCreby(userId);
			dto.setDvo(dvo);
			dto = MerchantService.createMerchant(dto);
			if (dto.getSts() == 0) {
				setClientMessage(dto, UserMsgConstants.COMMON_ERR_MSG, null);
				generateResponse(request, response, dto);
				return;
			}
			generateResponse(request, response, dto);
			return;
		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(dto, UserMsgConstants.COMMON_ERR_MSG, null);
			generateResponse(request, response, dto);
		}
		return;
	}

}
