/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.shop.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.inventory.product.dao.ProductItemMasterDAO;
import com.rtf.inventory.product.dvo.ProductItemMasterDVO;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.sql.db.DatabaseConnections;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;
import com.rtf.saas.util.PaginationUtil;
import com.rtf.shop.dto.ContestProductDTO;
import com.rtf.shop.dvo.ContestProductSetsDVO;
import com.rtf.shop.service.ShopContestProductSetService;
import com.rtf.shop.util.ShopMessageConstant;
import com.rtf.user.util.UserMsgConstants;

/**
 * The Class ShopAddContestProductSetServlet.
 */
@WebServlet("/shopaddcontestproductsets.json")
public class ShopAddContestProductSetServlet extends RtfSaasBaseServlet {

	/**
	* Serial version id
	 */
	private static final long serialVersionUID = 3933483521762837530L;

	/**
	 * Generate Http Response for contest Products Response data is sent in JSON format.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * Add products to contest
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String clId = request.getParameter("clId");
		String coId = request.getParameter("coId");
		String merchId = request.getParameter("merchId");
		String prodIdsStr = request.getParameter("prodIds");
		String cau = request.getParameter("cau");
		Integer itemsId = null;

		ContestProductDTO respDTO = new ContestProductDTO();
		respDTO.setSts(0);
		respDTO.setClId(clId);

		try {
			if (GenUtil.isNullOrEmpty(clId)) {
				setClientMessage(respDTO, UserMsgConstants.INVALID_CLIENT, null);
				generateResponse(request, response, respDTO);
				return;
			}

			if (GenUtil.isNullOrEmpty(coId)) {
				setClientMessage(respDTO, ShopMessageConstant.INVALID_CONTEST_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}

			if (GenUtil.isNullOrEmpty(prodIdsStr)) {
				setClientMessage(respDTO, ShopMessageConstant.INVALID_PRODUCT_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}

			if (GenUtil.isNullOrEmpty(merchId)) {
				setClientMessage(respDTO, ShopMessageConstant.INVALID_MERCHANT_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}

			Integer mercId = 0;
			try {
				mercId = Integer.parseInt(merchId);
			} catch (Exception e) {
				e.printStackTrace();
				setClientMessage(respDTO, ShopMessageConstant.INVALID_MERCHANT_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}

			String[] prodIds = prodIdsStr.split(",");
			if (prodIds.length == 0) {
				setClientMessage(respDTO, ShopMessageConstant.INVALID_PRODUCT_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}
			List<ProductItemMasterDVO> products = new ArrayList<ProductItemMasterDVO>();
			Connection conn = DatabaseConnections.getSaasAdminConnection();
			for (String idStr : prodIds) {
				if (idStr.trim().isEmpty()) {
					continue;
				}
				itemsId = Integer.parseInt(idStr);
				ProductItemMasterDVO rdvo = new ProductItemMasterDVO();
				rdvo.setClintId(clId);
				rdvo.setSlerId(mercId);
				rdvo.setSlrpitmsId(idStr);
				rdvo.setItemsId(itemsId);

				ProductItemMasterDVO product = ProductItemMasterDAO.fetchProductItemForPK(rdvo, conn);
				if (product == null) {
					setClientMessage(respDTO, ShopMessageConstant.PRODUCT_ID_NOT_EXIST, null);
					generateResponse(request, response, respDTO);
					return;
				}
				products.add(product);
			}
			DatabaseConnections.closeConnection(conn);
			List<ContestProductSetsDVO> extProducts = ShopContestProductSetService
					.getContestProductSetsByContestId(clId, coId, null);
			int seq = extProducts.size();
			for (ProductItemMasterDVO pro : products) {
				seq++;
				ContestProductSetsDVO cPro = new ContestProductSetsDVO();
				cPro.setClintId(pro.getClintId());
				cPro.setConId(coId);
				cPro.setCreby(cau);
				cPro.setDispSeq(seq);
				cPro.setDispStatus(ShopMessageConstant.PRODUCT_STATUS_ACTIVE);
				cPro.setPriceType(pro.getPriceType());
				cPro.setMercId(pro.getSlerId());
				cPro.setMercProdid(String.valueOf(pro.getItemsId()));
				cPro.setPricePerUnit(pro.getPselMinPrc());
				cPro.setProdDesc(pro.getPdesc());
				cPro.setProdImgUrl(pro.getPimg());
				cPro.setProdName(pro.getPname());
				cPro.setRegPrice(pro.getPregMinPrc());
				cPro.setProdPriceDtl(pro.getProdPriceDtl());
				cPro.setMerexternalRefProdId(pro.getSlrpitmsId());
				// cPro.setUnitType(pro.getUnitType());
				cPro.setUnitValue(0.0);
				ShopContestProductSetService.saveContestProduct(cPro);

			}
			extProducts = ShopContestProductSetService.getContestProductSetsByContestId(clId, coId, null);

			respDTO.setSts(1);
			respDTO.setProducts(extProducts);
			respDTO.setPagination(PaginationUtil.calculatePaginationParameter(extProducts.size(), "1"));
			setClientMessage(respDTO, null, ShopMessageConstant.PRODUCT_ADD_TO_CONTEST_SUCCESS_MSG);
			generateResponse(request, response, respDTO);
			return;
		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(respDTO, UserMsgConstants.COMMON_ERR_MSG, null);
			generateResponse(request, response, respDTO);
		}
		return;
	}

}
