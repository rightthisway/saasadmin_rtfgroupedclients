/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.time.OffsetDateTime;

/**
 * The Class Url.
 */
@ApiModel(description = "")
public class Url {
  
  /** The bitrate. */
  @SerializedName("bitrate")
  private Integer bitrate = null;

  /** The created at. */
  @SerializedName("created_at")
  private OffsetDateTime createdAt = null;

  /** The height. */
  @SerializedName("height")
  private Integer height = null;

  /** The id. */
  @SerializedName("id")
  private String id = null;

  /** The label. */
  @SerializedName("label")
  private String label = null;

  /** The player id. */
  @SerializedName("player_id")
  private String playerId = null;

  /** The updated at. */
  @SerializedName("updated_at")
  private OffsetDateTime updatedAt = null;

  /** The url. */
  @SerializedName("url")
  private String url = null;

  /** The width. */
  @SerializedName("width")
  private Integer width = null;

  /**
   * Bitrate.
   *
   * @param bitrate the bitrate
   * @return the url
   */
  public Url bitrate(Integer bitrate) {
    this.bitrate = bitrate;
    return this;
  }

   /**
    * Gets the bitrate.
    *
    * @return the bitrate
    */
  @ApiModelProperty(example = "", value = "The video bitrate, in kilobits per second (Kbps), of the output rendition that will be played at the URL. May correspond to the bitrate of an output rendition being used by the live stream or transcoder. Must be greater than <strong>0</strong>.")
  public Integer getBitrate() {
    return bitrate;
  }

  /**
   * Sets the bitrate.
   *
   * @param bitrate the new bitrate
   */
  public void setBitrate(Integer bitrate) {
    this.bitrate = bitrate;
  }

  /**
   * Created at.
   *
   * @param createdAt the created at
   * @return the url
   */
  public Url createdAt(OffsetDateTime createdAt) {
    this.createdAt = createdAt;
    return this;
  }

   /**
    * Gets the created at.
    *
    * @return the created at
    */
  @ApiModelProperty(example = "", value = "The date and time that the player URL was created.")
  public OffsetDateTime getCreatedAt() {
    return createdAt;
  }

  /**
   * Sets the created at.
   *
   * @param createdAt the new created at
   */
  public void setCreatedAt(OffsetDateTime createdAt) {
    this.createdAt = createdAt;
  }

  /**
   * Height.
   *
   * @param height the height
   * @return the url
   */
  public Url height(Integer height) {
    this.height = height;
    return this;
  }

   /**
    * Gets the height.
    *
    * @return the height
    */
  @ApiModelProperty(example = "", value = "The height, in pixels, of the output rendition that will be played at the URL. May correspond to the height of an output rendition being used by the live stream or transcoder. Must be greater than <strong>0</strong>.")
  public Integer getHeight() {
    return height;
  }

  /**
   * Sets the height.
   *
   * @param height the new height
   */
  public void setHeight(Integer height) {
    this.height = height;
  }

  /**
   * Id.
   *
   * @param id the id
   * @return the url
   */
  public Url id(String id) {
    this.id = id;
    return this;
  }

   /**
    * Gets the id.
    *
    * @return the id
    */
  @ApiModelProperty(example = "", value = "The unique alphanumeric string that identifies the player URL.")
  public String getId() {
    return id;
  }

  /**
   * Sets the id.
   *
   * @param id the new id
   */
  public void setId(String id) {
    this.id = id;
  }

  /**
   * Label.
   *
   * @param label the label
   * @return the url
   */
  public Url label(String label) {
    this.label = label;
    return this;
  }

   /**
    * Gets the label.
    *
    * @return the label
    */
  @ApiModelProperty(example = "", value = "A descriptive name for the player URL. Maximum 255 characters.")
  public String getLabel() {
    return label;
  }

  /**
   * Sets the label.
   *
   * @param label the new label
   */
  public void setLabel(String label) {
    this.label = label;
  }

  /**
   * Player id.
   *
   * @param playerId the player id
   * @return the url
   */
  public Url playerId(String playerId) {
    this.playerId = playerId;
    return this;
  }

   /**
    * Gets the player id.
    *
    * @return the player id
    */
  @ApiModelProperty(example = "", value = "The unique alphanumeric string that identifies the player.")
  public String getPlayerId() {
    return playerId;
  }

  /**
   * Sets the player id.
   *
   * @param playerId the new player id
   */
  public void setPlayerId(String playerId) {
    this.playerId = playerId;
  }

  /**
   * Updated at.
   *
   * @param updatedAt the updated at
   * @return the url
   */
  public Url updatedAt(OffsetDateTime updatedAt) {
    this.updatedAt = updatedAt;
    return this;
  }

   /**
    * Gets the updated at.
    *
    * @return the updated at
    */
  @ApiModelProperty(example = "", value = "The date and time that the player URL was updated.")
  public OffsetDateTime getUpdatedAt() {
    return updatedAt;
  }

  /**
   * Sets the updated at.
   *
   * @param updatedAt the new updated at
   */
  public void setUpdatedAt(OffsetDateTime updatedAt) {
    this.updatedAt = updatedAt;
  }

  /**
   * Url.
   *
   * @param url the url
   * @return the url
   */
  public Url url(String url) {
    this.url = url;
    return this;
  }

   /**
    * Gets the url.
    *
    * @return the url
    */
  @ApiModelProperty(example = "", value = "The URL of the player. If using a Wowza CDN target, the URL format is `http://[wowzasubdomain]-f.akamaihd.net/z/[stream_name]_[angle]@[stream_id]/manifest.f4m` for Adobe HDS playback or `http://[wowzasubdomain]-f.akamaihd.net/i/[stream_name]_[angle]@[stream_id]/master.m3u8` for Apple HLS playback.")
  public String getUrl() {
    return url;
  }

  /**
   * Sets the url.
   *
   * @param url the new url
   */
  public void setUrl(String url) {
    this.url = url;
  }

  /**
   * Width.
   *
   * @param width the width
   * @return the url
   */
  public Url width(Integer width) {
    this.width = width;
    return this;
  }

   /**
    * Gets the width.
    *
    * @return the width
    */
  @ApiModelProperty(example = "", value = "The width, in pixels, of the output rendition that will be played at the URL. May correspond to the width of an output rendition being used by the live stream or transcoder. Must be greater than <strong>0</strong>.")
  public Integer getWidth() {
    return width;
  }

  /**
   * Sets the width.
   *
   * @param width the new width
   */
  public void setWidth(Integer width) {
    this.width = width;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Url url = (Url) o;
    return Objects.equals(this.bitrate, url.bitrate) &&
        Objects.equals(this.createdAt, url.createdAt) &&
        Objects.equals(this.height, url.height) &&
        Objects.equals(this.id, url.id) &&
        Objects.equals(this.label, url.label) &&
        Objects.equals(this.playerId, url.playerId) &&
        Objects.equals(this.updatedAt, url.updatedAt) &&
        Objects.equals(this.url, url.url) &&
        Objects.equals(this.width, url.width);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(bitrate, createdAt, height, id, label, playerId, updatedAt, url, width);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Url {\n");
    
    sb.append("    bitrate: ").append(toIndentedString(bitrate)).append("\n");
    sb.append("    createdAt: ").append(toIndentedString(createdAt)).append("\n");
    sb.append("    height: ").append(toIndentedString(height)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    label: ").append(toIndentedString(label)).append("\n");
    sb.append("    playerId: ").append(toIndentedString(playerId)).append("\n");
    sb.append("    updatedAt: ").append(toIndentedString(updatedAt)).append("\n");
    sb.append("    url: ").append(toIndentedString(url)).append("\n");
    sb.append("    width: ").append(toIndentedString(width)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

