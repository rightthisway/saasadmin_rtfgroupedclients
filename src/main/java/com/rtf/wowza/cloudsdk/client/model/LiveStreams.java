/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.rtf.wowza.cloudsdk.client.model.IndexLiveStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class LiveStreams.
 */
@ApiModel(description = "")
public class LiveStreams {
  
  /** The live streams. */
  @SerializedName("live_streams")
  private List<IndexLiveStream> liveStreams = new ArrayList<IndexLiveStream>();

  /**
   * Live streams.
   *
   * @param liveStreams the live streams
   * @return the live streams
   */
  public LiveStreams liveStreams(List<IndexLiveStream> liveStreams) {
    this.liveStreams = liveStreams;
    return this;
  }

  /**
   * Adds the live streams item.
   *
   * @param liveStreamsItem the live streams item
   * @return the live streams
   */
  public LiveStreams addLiveStreamsItem(IndexLiveStream liveStreamsItem) {
    this.liveStreams.add(liveStreamsItem);
    return this;
  }

   /**
    * Gets the live streams.
    *
    * @return the live streams
    */
  @ApiModelProperty(required = true, value = "")
  public List<IndexLiveStream> getLiveStreams() {
    return liveStreams;
  }

  /**
   * Sets the live streams.
   *
   * @param liveStreams the new live streams
   */
  public void setLiveStreams(List<IndexLiveStream> liveStreams) {
    this.liveStreams = liveStreams;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    LiveStreams liveStreams = (LiveStreams) o;
    return Objects.equals(this.liveStreams, liveStreams.liveStreams);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(liveStreams);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class LiveStreams {\n");
    
    sb.append("    liveStreams: ").append(toIndentedString(liveStreams)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

