/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.time.OffsetDateTime;

/**
 * The Class IndexTranscoder.
 */
@ApiModel(description = "")
public class IndexTranscoder {
  
  /** The created at. */
  @SerializedName("created_at")
  private OffsetDateTime createdAt = null;

  /** The id. */
  @SerializedName("id")
  private String id = null;

  /** The name. */
  @SerializedName("name")
  private String name = null;

  /**
   * The Enum WorkflowEnum.
   */
  @JsonAdapter(WorkflowEnum.Adapter.class)
  public enum WorkflowEnum {
    
    /** The live stream. */
    LIVE_STREAM("live_stream"),
    
    /** The transcoder. */
    TRANSCODER("transcoder");

    /** The value. */
    private String value;

    /**
     * Instantiates a new workflow enum.
     *
     * @param value the value
     */
    WorkflowEnum(String value) {
      this.value = value;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
      return value;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString() {
      return String.valueOf(value);
    }

    /**
     * From value.
     *
     * @param text the text
     * @return the workflow enum
     */
    public static WorkflowEnum fromValue(String text) {
      for (WorkflowEnum b : WorkflowEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    /**
     * The Class Adapter.
     */
    public static class Adapter extends TypeAdapter<WorkflowEnum> {
      
      /**
       * Write.
       *
       * @param jsonWriter the json writer
       * @param enumeration the enumeration
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public void write(final JsonWriter jsonWriter, final WorkflowEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      /**
       * Read.
       *
       * @param jsonReader the json reader
       * @return the workflow enum
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public WorkflowEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return WorkflowEnum.fromValue(String.valueOf(value));
      }
    }
  }

  /** The workflow. */
  @SerializedName("workflow")
  private WorkflowEnum workflow = null;

  /** The updated at. */
  @SerializedName("updated_at")
  private OffsetDateTime updatedAt = null;

  /**
   * Created at.
   *
   * @param createdAt the created at
   * @return the index transcoder
   */
  public IndexTranscoder createdAt(OffsetDateTime createdAt) {
    this.createdAt = createdAt;
    return this;
  }

   /**
    * Gets the created at.
    *
    * @return the created at
    */
  @ApiModelProperty(example = "", value = "The date and time that the transcoder was created.")
  public OffsetDateTime getCreatedAt() {
    return createdAt;
  }

  /**
   * Sets the created at.
   *
   * @param createdAt the new created at
   */
  public void setCreatedAt(OffsetDateTime createdAt) {
    this.createdAt = createdAt;
  }

  /**
   * Id.
   *
   * @param id the id
   * @return the index transcoder
   */
  public IndexTranscoder id(String id) {
    this.id = id;
    return this;
  }

   /**
    * Gets the id.
    *
    * @return the id
    */
  @ApiModelProperty(example = "", value = "The unique alphanumeric string that identifies the transcoder.")
  public String getId() {
    return id;
  }

  /**
   * Sets the id.
   *
   * @param id the new id
   */
  public void setId(String id) {
    this.id = id;
  }

  /**
   * Name.
   *
   * @param name the name
   * @return the index transcoder
   */
  public IndexTranscoder name(String name) {
    this.name = name;
    return this;
  }

   /**
    * Gets the name.
    *
    * @return the name
    */
  @ApiModelProperty(example = "", value = "A descriptive name for the transcoder. Maximum 200 characters.")
  public String getName() {
    return name;
  }

  /**
   * Sets the name.
   *
   * @param name the new name
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Workflow.
   *
   * @param workflow the workflow
   * @return the index transcoder
   */
  public IndexTranscoder workflow(WorkflowEnum workflow) {
    this.workflow = workflow;
    return this;
  }

   /**
    * Gets the workflow.
    *
    * @return the workflow
    */
  @ApiModelProperty(example = "", value = "The method by which the transcoder was created, either <strong>transcoder</strong> for a transcoder created through the transcoder workflow or <strong>live_stream</strong> for a transcoder created automatically as part of the live stream workflow.")
  public WorkflowEnum getWorkflow() {
    return workflow;
  }

  /**
   * Sets the workflow.
   *
   * @param workflow the new workflow
   */
  public void setWorkflow(WorkflowEnum workflow) {
    this.workflow = workflow;
  }

  /**
   * Updated at.
   *
   * @param updatedAt the updated at
   * @return the index transcoder
   */
  public IndexTranscoder updatedAt(OffsetDateTime updatedAt) {
    this.updatedAt = updatedAt;
    return this;
  }

   /**
    * Gets the updated at.
    *
    * @return the updated at
    */
  @ApiModelProperty(example = "", value = "The date and time that the transcoder was updated.")
  public OffsetDateTime getUpdatedAt() {
    return updatedAt;
  }

  /**
   * Sets the updated at.
   *
   * @param updatedAt the new updated at
   */
  public void setUpdatedAt(OffsetDateTime updatedAt) {
    this.updatedAt = updatedAt;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    IndexTranscoder indexTranscoder = (IndexTranscoder) o;
    return Objects.equals(this.createdAt, indexTranscoder.createdAt) &&
        Objects.equals(this.id, indexTranscoder.id) &&
        Objects.equals(this.name, indexTranscoder.name) &&
        Objects.equals(this.workflow, indexTranscoder.workflow) &&
        Objects.equals(this.updatedAt, indexTranscoder.updatedAt);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(createdAt, id, name, workflow, updatedAt);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class IndexTranscoder {\n");
    
    sb.append("    createdAt: ").append(toIndentedString(createdAt)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    workflow: ").append(toIndentedString(workflow)).append("\n");
    sb.append("    updatedAt: ").append(toIndentedString(updatedAt)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

