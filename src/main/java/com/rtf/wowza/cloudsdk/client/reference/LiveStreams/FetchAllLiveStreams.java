 /*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/
package com.rtf.wowza.cloudsdk.client.reference.LiveStreams;

import com.rtf.wowza.cloudsdk.client.*;
import com.rtf.wowza.cloudsdk.client.auth.*;
import com.rtf.wowza.cloudsdk.client.model.*;
import com.rtf.wowza.cloudsdk.client.api.LiveStreamsApi;

import java.io.File;
import java.util.*;

/**
 * The Class FetchAllLiveStreams.
 */
public class FetchAllLiveStreams {

    /**
     * The main method.
     *
     * @param args the arguments
     * @throws Exception the exception
     */
    public static void main(String[] args) throws Exception {
    	Integer page = 1; 
    	Integer perPage = 100; 
    	fetchAllLiveStreams(page, perPage);
    }
    
    /**
     * Fetch all live streams.
     *
     * @param page the page
     * @param perPage the per page
     * @return the live streams
     * @throws Exception the exception
     */
    public static LiveStreams fetchAllLiveStreams(Integer page,Integer perPage) throws Exception { 
        ApiClient defaultClient = Configuration.getDefaultApiClient();

        // Configure API key authorization: wsc-access-key
        ApiKeyAuth wscaccesskey = (ApiKeyAuth)defaultClient.getAuthentication("wsc-access-key");
        wscaccesskey.setApiKey(defaultClient.wscaccesskey);
        // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
        //wsc-access-key.setApiKeyPrefix("Token");

        // Configure API key authorization: wsc-api-key
        ApiKeyAuth wscapikey = (ApiKeyAuth)defaultClient.getAuthentication("wsc-api-key");
        wscapikey.setApiKey(defaultClient.wscapikey);
        // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
        //wsc-api-key.setApiKeyPrefix("Token");

	LiveStreamsApi apiInstance = new LiveStreamsApi();
	
	try {
	    LiveStreams result = apiInstance.listLiveStreams(page, perPage);
	    System.out.println(result);
	    return result;
	    
	} catch (ApiException e) {
	    System.err.println("Exception when calling LiveStreamsApi#listLiveStreams");
	    e.printStackTrace();
	    throw e;
	}
	
    }

}

