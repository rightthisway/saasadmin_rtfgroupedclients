/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import java.util.HashMap;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.rtf.wowza.cloudsdk.client.model.Output;
import com.rtf.wowza.cloudsdk.client.model.PlaybackUrl;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.time.OffsetDateTime;

/**
 * The Class Transcoder.
 */
@ApiModel(description = "")
@javax.annotation.Generated(value ="com.wowza.cloudsdk.JavaCreate", date = "2019-03-02T10:45:24.077Z")
public class Transcoder {
  
  /** The application name. */
  @SerializedName("application_name")
  private String applicationName = null;

  /**
   * The Enum BillingModeEnum.
   */
  @JsonAdapter(BillingModeEnum.Adapter.class)
  public enum BillingModeEnum {
    
    /** The pay as you go. */
    PAY_AS_YOU_GO("pay_as_you_go"),
    
    /** The twentyfour seven. */
    TWENTYFOUR_SEVEN("twentyfour_seven");

    /** The value. */
    private String value;

    /**
     * Instantiates a new billing mode enum.
     *
     * @param value the value
     */
    BillingModeEnum(String value) {
      this.value = value;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
      return value;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString() {
      return String.valueOf(value);
    }

    /**
     * From value.
     *
     * @param text the text
     * @return the billing mode enum
     */
    public static BillingModeEnum fromValue(String text) {
      for (BillingModeEnum b : BillingModeEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    /**
     * The Class Adapter.
     */
    public static class Adapter extends TypeAdapter<BillingModeEnum> {
      
      /**
       * Write.
       *
       * @param jsonWriter the json writer
       * @param enumeration the enumeration
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public void write(final JsonWriter jsonWriter, final BillingModeEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      /**
       * Read.
       *
       * @param jsonReader the json reader
       * @return the billing mode enum
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public BillingModeEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return BillingModeEnum.fromValue(String.valueOf(value));
      }
    }
  }

  /** The billing mode. */
  @SerializedName("billing_mode")
  private BillingModeEnum billingMode = null;

  /**
   * The Enum BroadcastLocationEnum.
   */
  @JsonAdapter(BroadcastLocationEnum.Adapter.class)
  public enum BroadcastLocationEnum {
    
    /** The asia pacific australia. */
    ASIA_PACIFIC_AUSTRALIA("asia_pacific_australia"),
    
    /** The asia pacific japan. */
    ASIA_PACIFIC_JAPAN("asia_pacific_japan"),
    
    /** The asia pacific singapore. */
    ASIA_PACIFIC_SINGAPORE("asia_pacific_singapore"),
    
    /** The asia pacific taiwan. */
    ASIA_PACIFIC_TAIWAN("asia_pacific_taiwan"),
    
    /** The eu belgium. */
    EU_BELGIUM("eu_belgium"),
    
    /** The eu germany. */
    EU_GERMANY("eu_germany"),
    
    /** The eu ireland. */
    EU_IRELAND("eu_ireland"),
    
    /** The south america brazil. */
    SOUTH_AMERICA_BRAZIL("south_america_brazil"),
    
    /** The us central iowa. */
    US_CENTRAL_IOWA("us_central_iowa"),
    
    /** The us east virginia. */
    US_EAST_VIRGINIA("us_east_virginia"),
    
    /** The us west california. */
    US_WEST_CALIFORNIA("us_west_california"),
    
    /** The us west oregon. */
    US_WEST_OREGON("us_west_oregon");

    /** The value. */
    private String value;

    /**
     * Instantiates a new broadcast location enum.
     *
     * @param value the value
     */
    BroadcastLocationEnum(String value) {
      this.value = value;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
      return value;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString() {
      return String.valueOf(value);
    }

    /**
     * From value.
     *
     * @param text the text
     * @return the broadcast location enum
     */
    public static BroadcastLocationEnum fromValue(String text) {
      for (BroadcastLocationEnum b : BroadcastLocationEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    /**
     * The Class Adapter.
     */
    public static class Adapter extends TypeAdapter<BroadcastLocationEnum> {
      
      /**
       * Write.
       *
       * @param jsonWriter the json writer
       * @param enumeration the enumeration
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public void write(final JsonWriter jsonWriter, final BroadcastLocationEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      /**
       * Read.
       *
       * @param jsonReader the json reader
       * @return the broadcast location enum
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public BroadcastLocationEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return BroadcastLocationEnum.fromValue(String.valueOf(value));
      }
    }
  }

  /** The broadcast location. */
  @SerializedName("broadcast_location")
  private BroadcastLocationEnum broadcastLocation = null;

  /**
   * The Enum BufferSizeEnum.
   */
  @JsonAdapter(BufferSizeEnum.Adapter.class)
  public enum BufferSizeEnum {
    
    /** The number 0. */
    NUMBER_0(0),
    
    /** The number 1000. */
    NUMBER_1000(1000),
    
    /** The number 2000. */
    NUMBER_2000(2000),
    
    /** The number 3000. */
    NUMBER_3000(3000),
    
    /** The number 4000. */
    NUMBER_4000(4000),
    
    /** The number 5000. */
    NUMBER_5000(5000),
    
    /** The number 6000. */
    NUMBER_6000(6000),
    
    /** The number 7000. */
    NUMBER_7000(7000),
    
    /** The number 8000. */
    NUMBER_8000(8000);

    /** The value. */
    private Integer value;

    /**
     * Instantiates a new buffer size enum.
     *
     * @param value the value
     */
    BufferSizeEnum(Integer value) {
      this.value = value;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public Integer getValue() {
      return value;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString() {
      return String.valueOf(value);
    }

    /**
     * From value.
     *
     * @param text the text
     * @return the buffer size enum
     */
    public static BufferSizeEnum fromValue(String text) {
      for (BufferSizeEnum b : BufferSizeEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    /**
     * The Class Adapter.
     */
    public static class Adapter extends TypeAdapter<BufferSizeEnum> {
      
      /**
       * Write.
       *
       * @param jsonWriter the json writer
       * @param enumeration the enumeration
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public void write(final JsonWriter jsonWriter, final BufferSizeEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      /**
       * Read.
       *
       * @param jsonReader the json reader
       * @return the buffer size enum
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public BufferSizeEnum read(final JsonReader jsonReader) throws IOException {
        Integer value = jsonReader.nextInt();
        return BufferSizeEnum.fromValue(String.valueOf(value));
      }
    }
  }

  /** The buffer size. */
  @SerializedName("buffer_size")
  private BufferSizeEnum bufferSize = null;

  /**
   * The Enum ClosedCaptionTypeEnum.
   */
  @JsonAdapter(ClosedCaptionTypeEnum.Adapter.class)
  public enum ClosedCaptionTypeEnum {
    
    /** The none. */
    NONE("none"),
    
    /** The cea. */
    CEA("cea"),
    
    /** The on text. */
    ON_TEXT("on_text"),
    
    /** The both. */
    BOTH("both");

    /** The value. */
    private String value;

    /**
     * Instantiates a new closed caption type enum.
     *
     * @param value the value
     */
    ClosedCaptionTypeEnum(String value) {
      this.value = value;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
      return value;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString() {
      return String.valueOf(value);
    }

    /**
     * From value.
     *
     * @param text the text
     * @return the closed caption type enum
     */
    public static ClosedCaptionTypeEnum fromValue(String text) {
      for (ClosedCaptionTypeEnum b : ClosedCaptionTypeEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    /**
     * The Class Adapter.
     */
    public static class Adapter extends TypeAdapter<ClosedCaptionTypeEnum> {
      
      /**
       * Write.
       *
       * @param jsonWriter the json writer
       * @param enumeration the enumeration
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public void write(final JsonWriter jsonWriter, final ClosedCaptionTypeEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      /**
       * Read.
       *
       * @param jsonReader the json reader
       * @return the closed caption type enum
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public ClosedCaptionTypeEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return ClosedCaptionTypeEnum.fromValue(String.valueOf(value));
      }
    }
  }

  /** The closed caption type. */
  @SerializedName("closed_caption_type")
  private ClosedCaptionTypeEnum closedCaptionType = null;

  /** The created at. */
  @SerializedName("created_at")
  private OffsetDateTime createdAt = null;

  /**
   * The Enum DeliveryMethodEnum.
   */
  @JsonAdapter(DeliveryMethodEnum.Adapter.class)
  public enum DeliveryMethodEnum {
    
    /** The pull. */
    PULL("pull"),
    
    /** The cdn. */
    CDN("cdn"),
    
    /** The push. */
    PUSH("push");

    /** The value. */
    private String value;

    /**
     * Instantiates a new delivery method enum.
     *
     * @param value the value
     */
    DeliveryMethodEnum(String value) {
      this.value = value;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
      return value;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString() {
      return String.valueOf(value);
    }

    /**
     * From value.
     *
     * @param text the text
     * @return the delivery method enum
     */
    public static DeliveryMethodEnum fromValue(String text) {
      for (DeliveryMethodEnum b : DeliveryMethodEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    /**
     * The Class Adapter.
     */
    public static class Adapter extends TypeAdapter<DeliveryMethodEnum> {
      
      /**
       * Write.
       *
       * @param jsonWriter the json writer
       * @param enumeration the enumeration
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public void write(final JsonWriter jsonWriter, final DeliveryMethodEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      /**
       * Read.
       *
       * @param jsonReader the json reader
       * @return the delivery method enum
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public DeliveryMethodEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return DeliveryMethodEnum.fromValue(String.valueOf(value));
      }
    }
  }

  /** The delivery method. */
  @SerializedName("delivery_method")
  private DeliveryMethodEnum deliveryMethod = null;

  /** The delivery protocols. */
  @SerializedName("delivery_protocols")
  private List<String> deliveryProtocols = null;

  /** The description. */
  @SerializedName("description")
  private String description = null;

  /** The direct playback urls. */
  @SerializedName("direct_playback_urls")
  private HashMap<String,List<PlaybackUrl>> directPlaybackUrls = null;

  /** The disable authentication. */
  @SerializedName("disable_authentication")
  private Boolean disableAuthentication = null;

  /** The domain name. */
  @SerializedName("domain_name")
  private String domainName = null;

  /** The id. */
  @SerializedName("id")
  private String id = null;

  /** The idle timeout. */
  @SerializedName("idle_timeout")
  private Integer idleTimeout = null;

  /** The low latency. */
  @SerializedName("low_latency")
  private Boolean lowLatency = null;

  /** The name. */
  @SerializedName("name")
  private String name = null;

  /** The outputs. */
  @SerializedName("outputs")
  private List<Output> outputs = null;

  /** The password. */
  @SerializedName("password")
  private String password = null;

  /**
   * The Enum PlayMaximumConnectionsEnum.
   */
  @JsonAdapter(PlayMaximumConnectionsEnum.Adapter.class)
  public enum PlayMaximumConnectionsEnum {
    
    /** The number 10. */
    NUMBER_10(10),
    
    /** The number 11. */
    NUMBER_11(11),
    
    /** The number 12. */
    NUMBER_12(12),
    
    /** The number 13. */
    NUMBER_13(13),
    
    /** The number 14. */
    NUMBER_14(14),
    
    /** The number 15. */
    NUMBER_15(15),
    
    /** The number 16. */
    NUMBER_16(16),
    
    /** The number 17. */
    NUMBER_17(17),
    
    /** The number 18. */
    NUMBER_18(18),
    
    /** The number 19. */
    NUMBER_19(19),
    
    /** The number 20. */
    NUMBER_20(20),
    
    /** The number 21. */
    NUMBER_21(21),
    
    /** The number 22. */
    NUMBER_22(22),
    
    /** The number 23. */
    NUMBER_23(23),
    
    /** The number 24. */
    NUMBER_24(24),
    
    /** The number 25. */
    NUMBER_25(25),
    
    /** The number 26. */
    NUMBER_26(26),
    
    /** The number 27. */
    NUMBER_27(27),
    
    /** The number 28. */
    NUMBER_28(28),
    
    /** The number 29. */
    NUMBER_29(29),
    
    /** The number 30. */
    NUMBER_30(30),
    
    /** The number 31. */
    NUMBER_31(31),
    
    /** The number 32. */
    NUMBER_32(32),
    
    /** The number 33. */
    NUMBER_33(33),
    
    /** The number 34. */
    NUMBER_34(34),
    
    /** The number 35. */
    NUMBER_35(35),
    
    /** The number 36. */
    NUMBER_36(36),
    
    /** The number 37. */
    NUMBER_37(37),
    
    /** The number 38. */
    NUMBER_38(38),
    
    /** The number 39. */
    NUMBER_39(39),
    
    /** The number 40. */
    NUMBER_40(40),
    
    /** The number 41. */
    NUMBER_41(41),
    
    /** The number 42. */
    NUMBER_42(42),
    
    /** The number 43. */
    NUMBER_43(43),
    
    /** The number 44. */
    NUMBER_44(44),
    
    /** The number 45. */
    NUMBER_45(45),
    
    /** The number 46. */
    NUMBER_46(46),
    
    /** The number 47. */
    NUMBER_47(47),
    
    /** The number 48. */
    NUMBER_48(48),
    
    /** The number 49. */
    NUMBER_49(49),
    
    /** The number 50. */
    NUMBER_50(50),
    
    /** The number 51. */
    NUMBER_51(51),
    
    /** The number 52. */
    NUMBER_52(52),
    
    /** The number 53. */
    NUMBER_53(53),
    
    /** The number 54. */
    NUMBER_54(54),
    
    /** The number 55. */
    NUMBER_55(55),
    
    /** The number 56. */
    NUMBER_56(56),
    
    /** The number 57. */
    NUMBER_57(57),
    
    /** The number 58. */
    NUMBER_58(58),
    
    /** The number 59. */
    NUMBER_59(59),
    
    /** The number 60. */
    NUMBER_60(60),
    
    /** The number 61. */
    NUMBER_61(61),
    
    /** The number 62. */
    NUMBER_62(62),
    
    /** The number 63. */
    NUMBER_63(63),
    
    /** The number 64. */
    NUMBER_64(64),
    
    /** The number 65. */
    NUMBER_65(65),
    
    /** The number 66. */
    NUMBER_66(66),
    
    /** The number 67. */
    NUMBER_67(67),
    
    /** The number 68. */
    NUMBER_68(68),
    
    /** The number 69. */
    NUMBER_69(69),
    
    /** The number 70. */
    NUMBER_70(70),
    
    /** The number 71. */
    NUMBER_71(71),
    
    /** The number 72. */
    NUMBER_72(72),
    
    /** The number 73. */
    NUMBER_73(73),
    
    /** The number 74. */
    NUMBER_74(74),
    
    /** The number 75. */
    NUMBER_75(75),
    
    /** The number 76. */
    NUMBER_76(76),
    
    /** The number 77. */
    NUMBER_77(77),
    
    /** The number 78. */
    NUMBER_78(78),
    
    /** The number 79. */
    NUMBER_79(79),
    
    /** The number 80. */
    NUMBER_80(80),
    
    /** The number 81. */
    NUMBER_81(81),
    
    /** The number 82. */
    NUMBER_82(82),
    
    /** The number 83. */
    NUMBER_83(83),
    
    /** The number 84. */
    NUMBER_84(84),
    
    /** The number 85. */
    NUMBER_85(85),
    
    /** The number 86. */
    NUMBER_86(86),
    
    /** The number 87. */
    NUMBER_87(87),
    
    /** The number 88. */
    NUMBER_88(88),
    
    /** The number 89. */
    NUMBER_89(89),
    
    /** The number 90. */
    NUMBER_90(90),
    
    /** The number 91. */
    NUMBER_91(91),
    
    /** The number 92. */
    NUMBER_92(92),
    
    /** The number 93. */
    NUMBER_93(93),
    
    /** The number 94. */
    NUMBER_94(94),
    
    /** The number 95. */
    NUMBER_95(95),
    
    /** The number 96. */
    NUMBER_96(96),
    
    /** The number 97. */
    NUMBER_97(97),
    
    /** The number 98. */
    NUMBER_98(98),
    
    /** The number 99. */
    NUMBER_99(99),
    
    /** The number 100. */
    NUMBER_100(100);

    /** The value. */
    private Integer value;

    /**
     * Instantiates a new play maximum connections enum.
     *
     * @param value the value
     */
    PlayMaximumConnectionsEnum(Integer value) {
      this.value = value;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public Integer getValue() {
      return value;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString() {
      return String.valueOf(value);
    }

    /**
     * From value.
     *
     * @param text the text
     * @return the play maximum connections enum
     */
    public static PlayMaximumConnectionsEnum fromValue(String text) {
      for (PlayMaximumConnectionsEnum b : PlayMaximumConnectionsEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    /**
     * The Class Adapter.
     */
    public static class Adapter extends TypeAdapter<PlayMaximumConnectionsEnum> {
      
      /**
       * Write.
       *
       * @param jsonWriter the json writer
       * @param enumeration the enumeration
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public void write(final JsonWriter jsonWriter, final PlayMaximumConnectionsEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      /**
       * Read.
       *
       * @param jsonReader the json reader
       * @return the play maximum connections enum
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public PlayMaximumConnectionsEnum read(final JsonReader jsonReader) throws IOException {
        Integer value = jsonReader.nextInt();
        return PlayMaximumConnectionsEnum.fromValue(String.valueOf(value));
      }
    }
  }

  /** The play maximum connections. */
  @SerializedName("play_maximum_connections")
  private PlayMaximumConnectionsEnum playMaximumConnections = null;

  /**
   * The Enum ProtocolEnum.
   */
  @JsonAdapter(ProtocolEnum.Adapter.class)
  public enum ProtocolEnum {
    
    /** The rtmp. */
    RTMP("rtmp"),
    
    /** The rtsp. */
    RTSP("rtsp");

    /** The value. */
    private String value;

    /**
     * Instantiates a new protocol enum.
     *
     * @param value the value
     */
    ProtocolEnum(String value) {
      this.value = value;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
      return value;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString() {
      return String.valueOf(value);
    }

    /**
     * From value.
     *
     * @param text the text
     * @return the protocol enum
     */
    public static ProtocolEnum fromValue(String text) {
      for (ProtocolEnum b : ProtocolEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    /**
     * The Class Adapter.
     */
    public static class Adapter extends TypeAdapter<ProtocolEnum> {
      
      /**
       * Write.
       *
       * @param jsonWriter the json writer
       * @param enumeration the enumeration
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public void write(final JsonWriter jsonWriter, final ProtocolEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      /**
       * Read.
       *
       * @param jsonReader the json reader
       * @return the protocol enum
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public ProtocolEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return ProtocolEnum.fromValue(String.valueOf(value));
      }
    }
  }

  /** The protocol. */
  @SerializedName("protocol")
  private ProtocolEnum protocol = null;

  /** The recording. */
  @SerializedName("recording")
  private Boolean recording = null;

  /** The source port. */
  @SerializedName("source_port")
  private Integer sourcePort = null;

  /** The source url. */
  @SerializedName("source_url")
  private String sourceUrl = null;

  /** The stream extension. */
  @SerializedName("stream_extension")
  private String streamExtension = null;

  /** The stream name. */
  @SerializedName("stream_name")
  private String streamName = null;

  /** The stream smoother. */
  @SerializedName("stream_smoother")
  private Boolean streamSmoother = null;

  /** The stream source id. */
  @SerializedName("stream_source_id")
  private String streamSourceId = null;

  /** The suppress stream target start. */
  @SerializedName("suppress_stream_target_start")
  private Boolean suppressStreamTargetStart = null;

  /**
   * The Enum TranscoderTypeEnum.
   */
  @JsonAdapter(TranscoderTypeEnum.Adapter.class)
  public enum TranscoderTypeEnum {
    
    /** The transcoded. */
    TRANSCODED("transcoded"),
    
    /** The passthrough. */
    PASSTHROUGH("passthrough");

    /** The value. */
    private String value;

    /**
     * Instantiates a new transcoder type enum.
     *
     * @param value the value
     */
    TranscoderTypeEnum(String value) {
      this.value = value;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
      return value;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString() {
      return String.valueOf(value);
    }

    /**
     * From value.
     *
     * @param text the text
     * @return the transcoder type enum
     */
    public static TranscoderTypeEnum fromValue(String text) {
      for (TranscoderTypeEnum b : TranscoderTypeEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    /**
     * The Class Adapter.
     */
    public static class Adapter extends TypeAdapter<TranscoderTypeEnum> {
      
      /**
       * Write.
       *
       * @param jsonWriter the json writer
       * @param enumeration the enumeration
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public void write(final JsonWriter jsonWriter, final TranscoderTypeEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      /**
       * Read.
       *
       * @param jsonReader the json reader
       * @return the transcoder type enum
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public TranscoderTypeEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return TranscoderTypeEnum.fromValue(String.valueOf(value));
      }
    }
  }

  /** The transcoder type. */
  @SerializedName("transcoder_type")
  private TranscoderTypeEnum transcoderType = null;

  /** The updated at. */
  @SerializedName("updated_at")
  private OffsetDateTime updatedAt = null;

  /** The username. */
  @SerializedName("username")
  private String username = null;

  /** The watermark. */
  @SerializedName("watermark")
  private Boolean watermark = null;

  /** The watermark height. */
  @SerializedName("watermark_height")
  private Integer watermarkHeight = null;

  /** The watermark image url. */
  @SerializedName("watermark_image_url")
  private String watermarkImageUrl = null;

  /**
   * The Enum WatermarkOpacityEnum.
   */
  @JsonAdapter(WatermarkOpacityEnum.Adapter.class)
  public enum WatermarkOpacityEnum {
    
    /** The number 0. */
    NUMBER_0(0),
    
    /** The number 1. */
    NUMBER_1(1),
    
    /** The number 2. */
    NUMBER_2(2),
    
    /** The number 3. */
    NUMBER_3(3),
    
    /** The number 4. */
    NUMBER_4(4),
    
    /** The number 5. */
    NUMBER_5(5),
    
    /** The number 6. */
    NUMBER_6(6),
    
    /** The number 7. */
    NUMBER_7(7),
    
    /** The number 8. */
    NUMBER_8(8),
    
    /** The number 9. */
    NUMBER_9(9),
    
    /** The number 10. */
    NUMBER_10(10),
    
    /** The number 11. */
    NUMBER_11(11),
    
    /** The number 12. */
    NUMBER_12(12),
    
    /** The number 13. */
    NUMBER_13(13),
    
    /** The number 14. */
    NUMBER_14(14),
    
    /** The number 15. */
    NUMBER_15(15),
    
    /** The number 16. */
    NUMBER_16(16),
    
    /** The number 17. */
    NUMBER_17(17),
    
    /** The number 18. */
    NUMBER_18(18),
    
    /** The number 19. */
    NUMBER_19(19),
    
    /** The number 20. */
    NUMBER_20(20),
    
    /** The number 21. */
    NUMBER_21(21),
    
    /** The number 22. */
    NUMBER_22(22),
    
    /** The number 23. */
    NUMBER_23(23),
    
    /** The number 24. */
    NUMBER_24(24),
    
    /** The number 25. */
    NUMBER_25(25),
    
    /** The number 26. */
    NUMBER_26(26),
    
    /** The number 27. */
    NUMBER_27(27),
    
    /** The number 28. */
    NUMBER_28(28),
    
    /** The number 29. */
    NUMBER_29(29),
    
    /** The number 30. */
    NUMBER_30(30),
    
    /** The number 31. */
    NUMBER_31(31),
    
    /** The number 32. */
    NUMBER_32(32),
    
    /** The number 33. */
    NUMBER_33(33),
    
    /** The number 34. */
    NUMBER_34(34),
    
    /** The number 35. */
    NUMBER_35(35),
    
    /** The number 36. */
    NUMBER_36(36),
    
    /** The number 37. */
    NUMBER_37(37),
    
    /** The number 38. */
    NUMBER_38(38),
    
    /** The number 39. */
    NUMBER_39(39),
    
    /** The number 40. */
    NUMBER_40(40),
    
    /** The number 41. */
    NUMBER_41(41),
    
    /** The number 42. */
    NUMBER_42(42),
    
    /** The number 43. */
    NUMBER_43(43),
    
    /** The number 44. */
    NUMBER_44(44),
    
    /** The number 45. */
    NUMBER_45(45),
    
    /** The number 46. */
    NUMBER_46(46),
    
    /** The number 47. */
    NUMBER_47(47),
    
    /** The number 48. */
    NUMBER_48(48),
    
    /** The number 49. */
    NUMBER_49(49),
    
    /** The number 50. */
    NUMBER_50(50),
    
    /** The number 51. */
    NUMBER_51(51),
    
    /** The number 52. */
    NUMBER_52(52),
    
    /** The number 53. */
    NUMBER_53(53),
    
    /** The number 54. */
    NUMBER_54(54),
    
    /** The number 55. */
    NUMBER_55(55),
    
    /** The number 56. */
    NUMBER_56(56),
    
    /** The number 57. */
    NUMBER_57(57),
    
    /** The number 58. */
    NUMBER_58(58),
    
    /** The number 59. */
    NUMBER_59(59),
    
    /** The number 60. */
    NUMBER_60(60),
    
    /** The number 61. */
    NUMBER_61(61),
    
    /** The number 62. */
    NUMBER_62(62),
    
    /** The number 63. */
    NUMBER_63(63),
    
    /** The number 64. */
    NUMBER_64(64),
    
    /** The number 65. */
    NUMBER_65(65),
    
    /** The number 66. */
    NUMBER_66(66),
    
    /** The number 67. */
    NUMBER_67(67),
    
    /** The number 68. */
    NUMBER_68(68),
    
    /** The number 69. */
    NUMBER_69(69),
    
    /** The number 70. */
    NUMBER_70(70),
    
    /** The number 71. */
    NUMBER_71(71),
    
    /** The number 72. */
    NUMBER_72(72),
    
    /** The number 73. */
    NUMBER_73(73),
    
    /** The number 74. */
    NUMBER_74(74),
    
    /** The number 75. */
    NUMBER_75(75),
    
    /** The number 76. */
    NUMBER_76(76),
    
    /** The number 77. */
    NUMBER_77(77),
    
    /** The number 78. */
    NUMBER_78(78),
    
    /** The number 79. */
    NUMBER_79(79),
    
    /** The number 80. */
    NUMBER_80(80),
    
    /** The number 81. */
    NUMBER_81(81),
    
    /** The number 82. */
    NUMBER_82(82),
    
    /** The number 83. */
    NUMBER_83(83),
    
    /** The number 84. */
    NUMBER_84(84),
    
    /** The number 85. */
    NUMBER_85(85),
    
    /** The number 86. */
    NUMBER_86(86),
    
    /** The number 87. */
    NUMBER_87(87),
    
    /** The number 88. */
    NUMBER_88(88),
    
    /** The number 89. */
    NUMBER_89(89),
    
    /** The number 90. */
    NUMBER_90(90),
    
    /** The number 91. */
    NUMBER_91(91),
    
    /** The number 92. */
    NUMBER_92(92),
    
    /** The number 93. */
    NUMBER_93(93),
    
    /** The number 94. */
    NUMBER_94(94),
    
    /** The number 95. */
    NUMBER_95(95),
    
    /** The number 96. */
    NUMBER_96(96),
    
    /** The number 97. */
    NUMBER_97(97),
    
    /** The number 98. */
    NUMBER_98(98),
    
    /** The number 99. */
    NUMBER_99(99),
    
    /** The number 100. */
    NUMBER_100(100);

    /** The value. */
    private Integer value;

    /**
     * Instantiates a new watermark opacity enum.
     *
     * @param value the value
     */
    WatermarkOpacityEnum(Integer value) {
      this.value = value;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public Integer getValue() {
      return value;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString() {
      return String.valueOf(value);
    }

    /**
     * From value.
     *
     * @param text the text
     * @return the watermark opacity enum
     */
    public static WatermarkOpacityEnum fromValue(String text) {
      for (WatermarkOpacityEnum b : WatermarkOpacityEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    /**
     * The Class Adapter.
     */
    public static class Adapter extends TypeAdapter<WatermarkOpacityEnum> {
      
      /**
       * Write.
       *
       * @param jsonWriter the json writer
       * @param enumeration the enumeration
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public void write(final JsonWriter jsonWriter, final WatermarkOpacityEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      /**
       * Read.
       *
       * @param jsonReader the json reader
       * @return the watermark opacity enum
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public WatermarkOpacityEnum read(final JsonReader jsonReader) throws IOException {
        Integer value = jsonReader.nextInt();
        return WatermarkOpacityEnum.fromValue(String.valueOf(value));
      }
    }
  }

  /** The watermark opacity. */
  @SerializedName("watermark_opacity")
  private WatermarkOpacityEnum watermarkOpacity = null;

  /**
   * The Enum WatermarkPositionEnum.
   */
  @JsonAdapter(WatermarkPositionEnum.Adapter.class)
  public enum WatermarkPositionEnum {
    
    /** The top left. */
    TOP_LEFT("top-left"),
    
    /** The top right. */
    TOP_RIGHT("top-right"),
    
    /** The bottom left. */
    BOTTOM_LEFT("bottom-left"),
    
    /** The bottom right. */
    BOTTOM_RIGHT("bottom-right");

    /** The value. */
    private String value;

    /**
     * Instantiates a new watermark position enum.
     *
     * @param value the value
     */
    WatermarkPositionEnum(String value) {
      this.value = value;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
      return value;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString() {
      return String.valueOf(value);
    }

    /**
     * From value.
     *
     * @param text the text
     * @return the watermark position enum
     */
    public static WatermarkPositionEnum fromValue(String text) {
      for (WatermarkPositionEnum b : WatermarkPositionEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    /**
     * The Class Adapter.
     */
    public static class Adapter extends TypeAdapter<WatermarkPositionEnum> {
      
      /**
       * Write.
       *
       * @param jsonWriter the json writer
       * @param enumeration the enumeration
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public void write(final JsonWriter jsonWriter, final WatermarkPositionEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      /**
       * Read.
       *
       * @param jsonReader the json reader
       * @return the watermark position enum
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public WatermarkPositionEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return WatermarkPositionEnum.fromValue(String.valueOf(value));
      }
    }
  }

  /** The watermark position. */
  @SerializedName("watermark_position")
  private WatermarkPositionEnum watermarkPosition = null;

  /** The watermark width. */
  @SerializedName("watermark_width")
  private Integer watermarkWidth = null;

  /**
   * Application name.
   *
   * @param applicationName the application name
   * @return the transcoder
   */
  public Transcoder applicationName(String applicationName) {
    this.applicationName = applicationName;
    return this;
  }

   /**
    * Gets the application name.
    *
    * @return the application name
    */
  @ApiModelProperty(example = "", value = "The application name from the pull stream source URL.")
  public String getApplicationName() {
    return applicationName;
  }

  /**
   * Sets the application name.
   *
   * @param applicationName the new application name
   */
  public void setApplicationName(String applicationName) {
    this.applicationName = applicationName;
  }

  /**
   * Billing mode.
   *
   * @param billingMode the billing mode
   * @return the transcoder
   */
  public Transcoder billingMode(BillingModeEnum billingMode) {
    this.billingMode = billingMode;
    return this;
  }

   /**
    * Gets the billing mode.
    *
    * @return the billing mode
    */
  @ApiModelProperty(example = "", value = "The billing mode for the transcoder. The default is <strong>pay_as_you_go</strong>.")
  public BillingModeEnum getBillingMode() {
    return billingMode;
  }

  /**
   * Sets the billing mode.
   *
   * @param billingMode the new billing mode
   */
  public void setBillingMode(BillingModeEnum billingMode) {
    this.billingMode = billingMode;
  }

  /**
   * Broadcast location.
   *
   * @param broadcastLocation the broadcast location
   * @return the transcoder
   */
  public Transcoder broadcastLocation(BroadcastLocationEnum broadcastLocation) {
    this.broadcastLocation = broadcastLocation;
    return this;
  }

   /**
    * Gets the broadcast location.
    *
    * @return the broadcast location
    */
  @ApiModelProperty(example = "", value = "The location where Wowza Streaming Cloud transcodes your stream. Choose a location as close as possible to your video source.")
  public BroadcastLocationEnum getBroadcastLocation() {
    return broadcastLocation;
  }

  /**
   * Sets the broadcast location.
   *
   * @param broadcastLocation the new broadcast location
   */
  public void setBroadcastLocation(BroadcastLocationEnum broadcastLocation) {
    this.broadcastLocation = broadcastLocation;
  }

  /**
   * Buffer size.
   *
   * @param bufferSize the buffer size
   * @return the transcoder
   */
  public Transcoder bufferSize(BufferSizeEnum bufferSize) {
    this.bufferSize = bufferSize;
    return this;
  }

   /**
    * Gets the buffer size.
    *
    * @return the buffer size
    */
  @ApiModelProperty(example = "", value = "The size, in milliseconds, of the incoming buffer. <strong>0</strong> means no buffer. The default is <strong>4000</strong> (4 seconds).")
  public BufferSizeEnum getBufferSize() {
    return bufferSize;
  }

  /**
   * Sets the buffer size.
   *
   * @param bufferSize the new buffer size
   */
  public void setBufferSize(BufferSizeEnum bufferSize) {
    this.bufferSize = bufferSize;
  }

  /**
   * Closed caption type.
   *
   * @param closedCaptionType the closed caption type
   * @return the transcoder
   */
  public Transcoder closedCaptionType(ClosedCaptionTypeEnum closedCaptionType) {
    this.closedCaptionType = closedCaptionType;
    return this;
  }

   /**
    * Gets the closed caption type.
    *
    * @return the closed caption type
    */
  @ApiModelProperty(example = "", value = "The type of closed caption data being passed from the source. The default, <strong>none</strong>, indicates that no data is being provided. <strong>cea</strong> indicates that a CEA closed captioning data stream is being provided. <strong>on_text</strong> indicates that an onTextData closed captioning data stream is being provided. <strong>both</strong> indicates that both CEA and onTextData closed captioning data streams are being provided.")
  public ClosedCaptionTypeEnum getClosedCaptionType() {
    return closedCaptionType;
  }

  /**
   * Sets the closed caption type.
   *
   * @param closedCaptionType the new closed caption type
   */
  public void setClosedCaptionType(ClosedCaptionTypeEnum closedCaptionType) {
    this.closedCaptionType = closedCaptionType;
  }

  /**
   * Created at.
   *
   * @param createdAt the created at
   * @return the transcoder
   */
  public Transcoder createdAt(OffsetDateTime createdAt) {
    this.createdAt = createdAt;
    return this;
  }

   /**
    * Gets the created at.
    *
    * @return the created at
    */
  @ApiModelProperty(example = "", value = "The date and time that the transcoder was created.")
  public OffsetDateTime getCreatedAt() {
    return createdAt;
  }

  /**
   * Sets the created at.
   *
   * @param createdAt the new created at
   */
  public void setCreatedAt(OffsetDateTime createdAt) {
    this.createdAt = createdAt;
  }

  /**
   * Delivery method.
   *
   * @param deliveryMethod the delivery method
   * @return the transcoder
   */
  public Transcoder deliveryMethod(DeliveryMethodEnum deliveryMethod) {
    this.deliveryMethod = deliveryMethod;
    return this;
  }

   /**
    * Gets the delivery method.
    *
    * @return the delivery method
    */
  @ApiModelProperty(example = "", value = "The type of connection between the source encoder and the transcoder. The default, <strong>pull</strong>, instructs the transcoder to pull the video from the source. <strong>push</strong> instructs the source to push the stream to the transcoder. <strong>cdn</strong> uses a stream source to deliver the stream to the transcoder.")
  public DeliveryMethodEnum getDeliveryMethod() {
    return deliveryMethod;
  }

  /**
   * Sets the delivery method.
   *
   * @param deliveryMethod the new delivery method
   */
  public void setDeliveryMethod(DeliveryMethodEnum deliveryMethod) {
    this.deliveryMethod = deliveryMethod;
  }

  /**
   * Delivery protocols.
   *
   * @param deliveryProtocols the delivery protocols
   * @return the transcoder
   */
  public Transcoder deliveryProtocols(List<String> deliveryProtocols) {
    this.deliveryProtocols = deliveryProtocols;
    return this;
  }

  /**
   * Adds the delivery protocols item.
   *
   * @param deliveryProtocolsItem the delivery protocols item
   * @return the transcoder
   */
  public Transcoder addDeliveryProtocolsItem(String deliveryProtocolsItem) {
    if (this.deliveryProtocols == null) {
      this.deliveryProtocols = new ArrayList<String>();
    }
    this.deliveryProtocols.add(deliveryProtocolsItem);
    return this;
  }

   /**
    * Gets the delivery protocols.
    *
    * @return the delivery protocols
    */
  @ApiModelProperty(example = "\"\"", value = "An array of playback protocols enabled for this transcoder. By default, <strong>rtmp</strong>, <strong>rtsp</strong>, and <strong>wowz</strong> are returned.")
  public List<String> getDeliveryProtocols() {
    return deliveryProtocols;
  }

  /**
   * Sets the delivery protocols.
   *
   * @param deliveryProtocols the new delivery protocols
   */
  public void setDeliveryProtocols(List<String> deliveryProtocols) {
    this.deliveryProtocols = deliveryProtocols;
  }

  /**
   * Description.
   *
   * @param description the description
   * @return the transcoder
   */
  public Transcoder description(String description) {
    this.description = description;
    return this;
  }

   /**
    * Gets the description.
    *
    * @return the description
    */
  @ApiModelProperty(example = "", value = "An optional description of the transcoder.")
  public String getDescription() {
    return description;
  }

  /**
   * Sets the description.
   *
   * @param description the new description
   */
  public void setDescription(String description) {
    this.description = description;
  }

  /**
   * Direct playback urls.
   *
   * @param directPlaybackUrls the direct playback urls
   * @return the transcoder
   */
  public Transcoder directPlaybackUrls(HashMap<String,List<PlaybackUrl>> directPlaybackUrls) {
    this.directPlaybackUrls = directPlaybackUrls;
    return this;
  }

  /**
   * Adds the direct playback urls item.
   *
   * @param directPlaybackUrlsItem the direct playback urls item
   * @return the transcoder
   */
  public Transcoder addDirectPlaybackUrlsItem(PlaybackUrl directPlaybackUrlsItem) {
    if (this.directPlaybackUrls == null) {
      this.directPlaybackUrls = new HashMap<String,List<PlaybackUrl>>();
    }
        if ( !this.directPlaybackUrls.containsKey(name) )
                {
                List<PlaybackUrl> newList = new ArrayList<PlaybackUrl>();
                newList.add(directPlaybackUrlsItem);
                this.directPlaybackUrls.put(name,newList);
                }
        else
                {
                List<PlaybackUrl> current = this.directPlaybackUrls.get(name);
                current.add(directPlaybackUrlsItem);
                this.directPlaybackUrls.put(name,current);
                }
    return this;
  }

   /**
    * Gets the direct playback urls.
    *
    * @return the direct playback urls
    */
  @ApiModelProperty(example = "\"\"", value = "An array of direct playback URLs the transcoder's delivery protocols. Each protocol has a URL for the source and a URL for each output rendition.")
  public HashMap<String,List<PlaybackUrl>> getDirectPlaybackUrls() {
    return directPlaybackUrls;
  }

  /**
   * Sets the direct playback urls.
   *
   * @param directPlaybackUrls the direct playback urls
   */
  public void setDirectPlaybackUrls(HashMap<String,List<PlaybackUrl>> directPlaybackUrls) {
    this.directPlaybackUrls = directPlaybackUrls;
  }

  /**
   * Disable authentication.
   *
   * @param disableAuthentication the disable authentication
   * @return the transcoder
   */
  public Transcoder disableAuthentication(Boolean disableAuthentication) {
    this.disableAuthentication = disableAuthentication;
    return this;
  }

   /**
    * Checks if is disable authentication.
    *
    * @return the boolean
    */
  @ApiModelProperty(value = "Authentication is required by default for RTMP and RTSP push connections from a video source to the transcoder. Specify <strong>true</strong> to disable authentication with the video source.")
  public Boolean isDisableAuthentication() {
    return disableAuthentication;
  }

  /**
   * Sets the disable authentication.
   *
   * @param disableAuthentication the new disable authentication
   */
  public void setDisableAuthentication(Boolean disableAuthentication) {
    this.disableAuthentication = disableAuthentication;
  }

  /**
   * Domain name.
   *
   * @param domainName the domain name
   * @return the transcoder
   */
  public Transcoder domainName(String domainName) {
    this.domainName = domainName;
    return this;
  }

   /**
    * Gets the domain name.
    *
    * @return the domain name
    */
  @ApiModelProperty(example = "", value = "The domain name from the pull stream source URL.")
  public String getDomainName() {
    return domainName;
  }

  /**
   * Sets the domain name.
   *
   * @param domainName the new domain name
   */
  public void setDomainName(String domainName) {
    this.domainName = domainName;
  }

  /**
   * Id.
   *
   * @param id the id
   * @return the transcoder
   */
  public Transcoder id(String id) {
    this.id = id;
    return this;
  }

   /**
    * Gets the id.
    *
    * @return the id
    */
  @ApiModelProperty(example = "", value = "The unique alphanumeric string that identifies the transcoder.")
  public String getId() {
    return id;
  }

  /**
   * Sets the id.
   *
   * @param id the new id
   */
  public void setId(String id) {
    this.id = id;
  }

  /**
   * Idle timeout.
   *
   * @param idleTimeout the idle timeout
   * @return the transcoder
   */
  public Transcoder idleTimeout(Integer idleTimeout) {
    this.idleTimeout = idleTimeout;
    return this;
  }

   /**
    * Gets the idle timeout.
    *
    * @return the idle timeout
    */
  @ApiModelProperty(example = "", value = "The amount of idle time, in seconds, before the transcoder automatically shuts down. Valid values are the integers <strong>0</strong> (never shuts down) to <strong>172800</strong> (48 hours). The default is <strong>1200</strong> (20 minutes).")
  public Integer getIdleTimeout() {
    return idleTimeout;
  }

  /**
   * Sets the idle timeout.
   *
   * @param idleTimeout the new idle timeout
   */
  public void setIdleTimeout(Integer idleTimeout) {
    this.idleTimeout = idleTimeout;
  }

  /**
   * Low latency.
   *
   * @param lowLatency the low latency
   * @return the transcoder
   */
  public Transcoder lowLatency(Boolean lowLatency) {
    this.lowLatency = lowLatency;
    return this;
  }

   /**
    * Checks if is low latency.
    *
    * @return the boolean
    */
  @ApiModelProperty(value = "If <strong>true</strong>, turns off the sort packet buffer and speeds the time it takes to decode and deliver video data to the player. The default is <strong>false</strong>.")
  public Boolean isLowLatency() {
    return lowLatency;
  }

  /**
   * Sets the low latency.
   *
   * @param lowLatency the new low latency
   */
  public void setLowLatency(Boolean lowLatency) {
    this.lowLatency = lowLatency;
  }

  /**
   * Name.
   *
   * @param name the name
   * @return the transcoder
   */
  public Transcoder name(String name) {
    this.name = name;
    return this;
  }

   /**
    * Gets the name.
    *
    * @return the name
    */
  @ApiModelProperty(example = "", value = "A descriptive name for the transcoder. Maximum 200 characters.")
  public String getName() {
    return name;
  }

  /**
   * Sets the name.
   *
   * @param name the new name
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Outputs.
   *
   * @param outputs the outputs
   * @return the transcoder
   */
  public Transcoder outputs(List<Output> outputs) {
    this.outputs = outputs;
    return this;
  }

  /**
   * Adds the outputs item.
   *
   * @param outputsItem the outputs item
   * @return the transcoder
   */
  public Transcoder addOutputsItem(Output outputsItem) {
    if (this.outputs == null) {
      this.outputs = new ArrayList<Output>();
    }
    this.outputs.add(outputsItem);
    return this;
  }

   /**
    * Gets the outputs.
    *
    * @return the outputs
    */
  @ApiModelProperty(value = "Output renditions associated with the transcoder.")
  public List<Output> getOutputs() {
    return outputs;
  }

  /**
   * Sets the outputs.
   *
   * @param outputs the new outputs
   */
  public void setOutputs(List<Output> outputs) {
    this.outputs = outputs;
  }

  /**
   * Password.
   *
   * @param password the password
   * @return the transcoder
   */
  public Transcoder password(String password) {
    this.password = password;
    return this;
  }

   /**
    * Gets the password.
    *
    * @return the password
    */
  @ApiModelProperty(example = "", value = "A password for authenticating an RTMP or RTSP push connection. Can contain only uppercase and lowercase letters; numbers; and the period (.), underscore (_), and hyphen (-) characters. No other special characters can be used.")
  public String getPassword() {
    return password;
  }

  /**
   * Sets the password.
   *
   * @param password the new password
   */
  public void setPassword(String password) {
    this.password = password;
  }

  /**
   * Play maximum connections.
   *
   * @param playMaximumConnections the play maximum connections
   * @return the transcoder
   */
  public Transcoder playMaximumConnections(PlayMaximumConnectionsEnum playMaximumConnections) {
    this.playMaximumConnections = playMaximumConnections;
    return this;
  }

   /**
    * Gets the play maximum connections.
    *
    * @return the play maximum connections
    */
  @ApiModelProperty(example = "", value = "The number of users who are allowed to connect directly to the transcoder.")
  public PlayMaximumConnectionsEnum getPlayMaximumConnections() {
    return playMaximumConnections;
  }

  /**
   * Sets the play maximum connections.
   *
   * @param playMaximumConnections the new play maximum connections
   */
  public void setPlayMaximumConnections(PlayMaximumConnectionsEnum playMaximumConnections) {
    this.playMaximumConnections = playMaximumConnections;
  }

  /**
   * Protocol.
   *
   * @param protocol the protocol
   * @return the transcoder
   */
  public Transcoder protocol(ProtocolEnum protocol) {
    this.protocol = protocol;
    return this;
  }

   /**
    * Gets the protocol.
    *
    * @return the protocol
    */
  @ApiModelProperty(example = "", value = "The transport protocol for the source video. The default is <strong>rtmp</strong>.")
  public ProtocolEnum getProtocol() {
    return protocol;
  }

  /**
   * Sets the protocol.
   *
   * @param protocol the new protocol
   */
  public void setProtocol(ProtocolEnum protocol) {
    this.protocol = protocol;
  }

  /**
   * Recording.
   *
   * @param recording the recording
   * @return the transcoder
   */
  public Transcoder recording(Boolean recording) {
    this.recording = recording;
    return this;
  }

   /**
    * Checks if is recording.
    *
    * @return the boolean
    */
  @ApiModelProperty(value = "If <strong>true</strong>, creates a recording of the transcoded output. The default is <strong>false</strong>.")
  public Boolean isRecording() {
    return recording;
  }

  /**
   * Sets the recording.
   *
   * @param recording the new recording
   */
  public void setRecording(Boolean recording) {
    this.recording = recording;
  }

  /**
   * Source port.
   *
   * @param sourcePort the source port
   * @return the transcoder
   */
  public Transcoder sourcePort(Integer sourcePort) {
    this.sourcePort = sourcePort;
    return this;
  }

   /**
    * Gets the source port.
    *
    * @return the source port
    */
  @ApiModelProperty(example = "", value = "The port used for RTMP pull connections to Wowza Streaming Cloud.")
  public Integer getSourcePort() {
    return sourcePort;
  }

  /**
   * Sets the source port.
   *
   * @param sourcePort the new source port
   */
  public void setSourcePort(Integer sourcePort) {
    this.sourcePort = sourcePort;
  }

  /**
   * Source url.
   *
   * @param sourceUrl the source url
   * @return the transcoder
   */
  public Transcoder sourceUrl(String sourceUrl) {
    this.sourceUrl = sourceUrl;
    return this;
  }

   /**
    * Gets the source url.
    *
    * @return the source url
    */
  @ApiModelProperty(example = "", value = "For the <em>delivery_method</em> <strong>pull</strong>. Enter the source's web address without the preceding protocol or the trailing slash (/).")
  public String getSourceUrl() {
    return sourceUrl;
  }

  /**
   * Sets the source url.
   *
   * @param sourceUrl the new source url
   */
  public void setSourceUrl(String sourceUrl) {
    this.sourceUrl = sourceUrl;
  }

  /**
   * Stream extension.
   *
   * @param streamExtension the stream extension
   * @return the transcoder
   */
  public Transcoder streamExtension(String streamExtension) {
    this.streamExtension = streamExtension;
    return this;
  }

   /**
    * Gets the stream extension.
    *
    * @return the stream extension
    */
  @ApiModelProperty(example = "", value = "For the <em>delivery_method</em> <strong>push</strong>. Some encoders append an extension to their stream names. If the device you're using does this, enter the extension.")
  public String getStreamExtension() {
    return streamExtension;
  }

  /**
   * Sets the stream extension.
   *
   * @param streamExtension the new stream extension
   */
  public void setStreamExtension(String streamExtension) {
    this.streamExtension = streamExtension;
  }

  /**
   * Stream name.
   *
   * @param streamName the stream name
   * @return the transcoder
   */
  public Transcoder streamName(String streamName) {
    this.streamName = streamName;
    return this;
  }

   /**
    * Gets the stream name.
    *
    * @return the stream name
    */
  @ApiModelProperty(example = "", value = "The stream name from the pull stream source URL.")
  public String getStreamName() {
    return streamName;
  }

  /**
   * Sets the stream name.
   *
   * @param streamName the new stream name
   */
  public void setStreamName(String streamName) {
    this.streamName = streamName;
  }

  /**
   * Stream smoother.
   *
   * @param streamSmoother the stream smoother
   * @return the transcoder
   */
  public Transcoder streamSmoother(Boolean streamSmoother) {
    this.streamSmoother = streamSmoother;
    return this;
  }

   /**
    * Checks if is stream smoother.
    *
    * @return the boolean
    */
  @ApiModelProperty(value = "A dynamic buffer that helps stabilize streams in rough network conditions, but adds latency. Specify <strong>true</strong> to enable stream smoothing. The default is <strong>false</strong>.")
  public Boolean isStreamSmoother() {
    return streamSmoother;
  }

  /**
   * Sets the stream smoother.
   *
   * @param streamSmoother the new stream smoother
   */
  public void setStreamSmoother(Boolean streamSmoother) {
    this.streamSmoother = streamSmoother;
  }

  /**
   * Stream source id.
   *
   * @param streamSourceId the stream source id
   * @return the transcoder
   */
  public Transcoder streamSourceId(String streamSourceId) {
    this.streamSourceId = streamSourceId;
    return this;
  }

   /**
    * Gets the stream source id.
    *
    * @return the stream source id
    */
  @ApiModelProperty(example = "", value = "For the <em>delivery_method</em> <strong>cdn</strong>. The alphanumeric string that identifies the stream source that you want to use to deliver the stream to the transcoder.")
  public String getStreamSourceId() {
    return streamSourceId;
  }

  /**
   * Sets the stream source id.
   *
   * @param streamSourceId the new stream source id
   */
  public void setStreamSourceId(String streamSourceId) {
    this.streamSourceId = streamSourceId;
  }

  /**
   * Suppress stream target start.
   *
   * @param suppressStreamTargetStart the suppress stream target start
   * @return the transcoder
   */
  public Transcoder suppressStreamTargetStart(Boolean suppressStreamTargetStart) {
    this.suppressStreamTargetStart = suppressStreamTargetStart;
    return this;
  }

   /**
    * Checks if is suppress stream target start.
    *
    * @return the boolean
    */
  @ApiModelProperty(value = "If <strong>true</strong>, disables stream targets when the transcoder starts. If <strong>false</strong> (the default), the targets start when the transcoder starts.")
  public Boolean isSuppressStreamTargetStart() {
    return suppressStreamTargetStart;
  }

  /**
   * Sets the suppress stream target start.
   *
   * @param suppressStreamTargetStart the new suppress stream target start
   */
  public void setSuppressStreamTargetStart(Boolean suppressStreamTargetStart) {
    this.suppressStreamTargetStart = suppressStreamTargetStart;
  }

  /**
   * Transcoder type.
   *
   * @param transcoderType the transcoder type
   * @return the transcoder
   */
  public Transcoder transcoderType(TranscoderTypeEnum transcoderType) {
    this.transcoderType = transcoderType;
    return this;
  }

   /**
    * Gets the transcoder type.
    *
    * @return the transcoder type
    */
  @ApiModelProperty(example = "", value = "The type of transcoder, either <strong>transcoded</strong> for streams that are transcoded into adaptive bitrate renditions or <strong>passthrough</strong> for streams that aren't processed by the transcoder. The default is <strong>transcoded</strong>.")
  public TranscoderTypeEnum getTranscoderType() {
    return transcoderType;
  }

  /**
   * Sets the transcoder type.
   *
   * @param transcoderType the new transcoder type
   */
  public void setTranscoderType(TranscoderTypeEnum transcoderType) {
    this.transcoderType = transcoderType;
  }

  /**
   * Updated at.
   *
   * @param updatedAt the updated at
   * @return the transcoder
   */
  public Transcoder updatedAt(OffsetDateTime updatedAt) {
    this.updatedAt = updatedAt;
    return this;
  }

   /**
    * Gets the updated at.
    *
    * @return the updated at
    */
  @ApiModelProperty(example = "", value = "The date and time that the transcoder was updated.")
  public OffsetDateTime getUpdatedAt() {
    return updatedAt;
  }

  /**
   * Sets the updated at.
   *
   * @param updatedAt the new updated at
   */
  public void setUpdatedAt(OffsetDateTime updatedAt) {
    this.updatedAt = updatedAt;
  }

  /**
   * Username.
   *
   * @param username the username
   * @return the transcoder
   */
  public Transcoder username(String username) {
    this.username = username;
    return this;
  }

   /**
    * Gets the username.
    *
    * @return the username
    */
  @ApiModelProperty(example = "", value = "A username for authenticating an RTMP or RTSP push connection. Can contain only uppercase and lowercase letters; numbers; and the period (.), underscore (_), and hyphen (-) characters. No other special characters can be used.")
  public String getUsername() {
    return username;
  }

  /**
   * Sets the username.
   *
   * @param username the new username
   */
  public void setUsername(String username) {
    this.username = username;
  }

  /**
   * Watermark.
   *
   * @param watermark the watermark
   * @return the transcoder
   */
  public Transcoder watermark(Boolean watermark) {
    this.watermark = watermark;
    return this;
  }

   /**
    * Checks if is watermark.
    *
    * @return the boolean
    */
  @ApiModelProperty(value = "Embeds an image into the transcoded stream for copyright protection. Specify <strong>true</strong> to embed a watermark image.")
  public Boolean isWatermark() {
    return watermark;
  }

  /**
   * Sets the watermark.
   *
   * @param watermark the new watermark
   */
  public void setWatermark(Boolean watermark) {
    this.watermark = watermark;
  }

  /**
   * Watermark height.
   *
   * @param watermarkHeight the watermark height
   * @return the transcoder
   */
  public Transcoder watermarkHeight(Integer watermarkHeight) {
    this.watermarkHeight = watermarkHeight;
    return this;
  }

   /**
    * Gets the watermark height.
    *
    * @return the watermark height
    */
  @ApiModelProperty(example = "", value = "The height, in pixels, of the watermark image. If blank, Wowza Streaming Cloud uses the original image height.")
  public Integer getWatermarkHeight() {
    return watermarkHeight;
  }

  /**
   * Sets the watermark height.
   *
   * @param watermarkHeight the new watermark height
   */
  public void setWatermarkHeight(Integer watermarkHeight) {
    this.watermarkHeight = watermarkHeight;
  }

  /**
   * Watermark image url.
   *
   * @param watermarkImageUrl the watermark image url
   * @return the transcoder
   */
  public Transcoder watermarkImageUrl(String watermarkImageUrl) {
    this.watermarkImageUrl = watermarkImageUrl;
    return this;
  }

   /**
    * Gets the watermark image url.
    *
    * @return the watermark image url
    */
  @ApiModelProperty(example = "", value = "The path to a GIF, JPEG, or PNG image that is embedded in all bitrate renditions of the stream. Watermark image files must be 2.5 MB or smaller.")
  public String getWatermarkImageUrl() {
    return watermarkImageUrl;
  }

  /**
   * Sets the watermark image url.
   *
   * @param watermarkImageUrl the new watermark image url
   */
  public void setWatermarkImageUrl(String watermarkImageUrl) {
    this.watermarkImageUrl = watermarkImageUrl;
  }

  /**
   * Watermark opacity.
   *
   * @param watermarkOpacity the watermark opacity
   * @return the transcoder
   */
  public Transcoder watermarkOpacity(WatermarkOpacityEnum watermarkOpacity) {
    this.watermarkOpacity = watermarkOpacity;
    return this;
  }

   /**
    * Gets the watermark opacity.
    *
    * @return the watermark opacity
    */
  @ApiModelProperty(example = "", value = "The opacity, or percentage of transparency, of the watermark. <strong>0</strong> is fully transparent; <strong>100</strong> is fully opaque.")
  public WatermarkOpacityEnum getWatermarkOpacity() {
    return watermarkOpacity;
  }

  /**
   * Sets the watermark opacity.
   *
   * @param watermarkOpacity the new watermark opacity
   */
  public void setWatermarkOpacity(WatermarkOpacityEnum watermarkOpacity) {
    this.watermarkOpacity = watermarkOpacity;
  }

  /**
   * Watermark position.
   *
   * @param watermarkPosition the watermark position
   * @return the transcoder
   */
  public Transcoder watermarkPosition(WatermarkPositionEnum watermarkPosition) {
    this.watermarkPosition = watermarkPosition;
    return this;
  }

   /**
    * Gets the watermark position.
    *
    * @return the watermark position
    */
  @ApiModelProperty(example = "", value = "The corner of the video frame in which you want the watermark to appear. The default is <strong>top-left</strong>.")
  public WatermarkPositionEnum getWatermarkPosition() {
    return watermarkPosition;
  }

  /**
   * Sets the watermark position.
   *
   * @param watermarkPosition the new watermark position
   */
  public void setWatermarkPosition(WatermarkPositionEnum watermarkPosition) {
    this.watermarkPosition = watermarkPosition;
  }

  /**
   * Watermark width.
   *
   * @param watermarkWidth the watermark width
   * @return the transcoder
   */
  public Transcoder watermarkWidth(Integer watermarkWidth) {
    this.watermarkWidth = watermarkWidth;
    return this;
  }

   /**
    * Gets the watermark width.
    *
    * @return the watermark width
    */
  @ApiModelProperty(example = "", value = "The width, in pixels, of the watermark image. If blank, Wowza Streaming Cloud uses the original image width.")
  public Integer getWatermarkWidth() {
    return watermarkWidth;
  }

  /**
   * Sets the watermark width.
   *
   * @param watermarkWidth the new watermark width
   */
  public void setWatermarkWidth(Integer watermarkWidth) {
    this.watermarkWidth = watermarkWidth;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Transcoder transcoder = (Transcoder) o;
    return Objects.equals(this.applicationName, transcoder.applicationName) &&
        Objects.equals(this.billingMode, transcoder.billingMode) &&
        Objects.equals(this.broadcastLocation, transcoder.broadcastLocation) &&
        Objects.equals(this.bufferSize, transcoder.bufferSize) &&
        Objects.equals(this.closedCaptionType, transcoder.closedCaptionType) &&
        Objects.equals(this.createdAt, transcoder.createdAt) &&
        Objects.equals(this.deliveryMethod, transcoder.deliveryMethod) &&
        Objects.equals(this.deliveryProtocols, transcoder.deliveryProtocols) &&
        Objects.equals(this.description, transcoder.description) &&
        Objects.equals(this.directPlaybackUrls, transcoder.directPlaybackUrls) &&
        Objects.equals(this.disableAuthentication, transcoder.disableAuthentication) &&
        Objects.equals(this.domainName, transcoder.domainName) &&
        Objects.equals(this.id, transcoder.id) &&
        Objects.equals(this.idleTimeout, transcoder.idleTimeout) &&
        Objects.equals(this.lowLatency, transcoder.lowLatency) &&
        Objects.equals(this.name, transcoder.name) &&
        Objects.equals(this.outputs, transcoder.outputs) &&
        Objects.equals(this.password, transcoder.password) &&
        Objects.equals(this.playMaximumConnections, transcoder.playMaximumConnections) &&
        Objects.equals(this.protocol, transcoder.protocol) &&
        Objects.equals(this.recording, transcoder.recording) &&
        Objects.equals(this.sourcePort, transcoder.sourcePort) &&
        Objects.equals(this.sourceUrl, transcoder.sourceUrl) &&
        Objects.equals(this.streamExtension, transcoder.streamExtension) &&
        Objects.equals(this.streamName, transcoder.streamName) &&
        Objects.equals(this.streamSmoother, transcoder.streamSmoother) &&
        Objects.equals(this.streamSourceId, transcoder.streamSourceId) &&
        Objects.equals(this.suppressStreamTargetStart, transcoder.suppressStreamTargetStart) &&
        Objects.equals(this.transcoderType, transcoder.transcoderType) &&
        Objects.equals(this.updatedAt, transcoder.updatedAt) &&
        Objects.equals(this.username, transcoder.username) &&
        Objects.equals(this.watermark, transcoder.watermark) &&
        Objects.equals(this.watermarkHeight, transcoder.watermarkHeight) &&
        Objects.equals(this.watermarkImageUrl, transcoder.watermarkImageUrl) &&
        Objects.equals(this.watermarkOpacity, transcoder.watermarkOpacity) &&
        Objects.equals(this.watermarkPosition, transcoder.watermarkPosition) &&
        Objects.equals(this.watermarkWidth, transcoder.watermarkWidth);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(applicationName, billingMode, broadcastLocation, bufferSize, closedCaptionType, createdAt, deliveryMethod, deliveryProtocols, description, directPlaybackUrls, disableAuthentication, domainName, id, idleTimeout, lowLatency, name, outputs, password, playMaximumConnections, protocol, recording, sourcePort, sourceUrl, streamExtension, streamName, streamSmoother, streamSourceId, suppressStreamTargetStart, transcoderType, updatedAt, username, watermark, watermarkHeight, watermarkImageUrl, watermarkOpacity, watermarkPosition, watermarkWidth);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Transcoder {\n");
    
    sb.append("    applicationName: ").append(toIndentedString(applicationName)).append("\n");
    sb.append("    billingMode: ").append(toIndentedString(billingMode)).append("\n");
    sb.append("    broadcastLocation: ").append(toIndentedString(broadcastLocation)).append("\n");
    sb.append("    bufferSize: ").append(toIndentedString(bufferSize)).append("\n");
    sb.append("    closedCaptionType: ").append(toIndentedString(closedCaptionType)).append("\n");
    sb.append("    createdAt: ").append(toIndentedString(createdAt)).append("\n");
    sb.append("    deliveryMethod: ").append(toIndentedString(deliveryMethod)).append("\n");
    sb.append("    deliveryProtocols: ").append(toIndentedString(deliveryProtocols)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    directPlaybackUrls: ").append(toIndentedString(directPlaybackUrls)).append("\n");
    sb.append("    disableAuthentication: ").append(toIndentedString(disableAuthentication)).append("\n");
    sb.append("    domainName: ").append(toIndentedString(domainName)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    idleTimeout: ").append(toIndentedString(idleTimeout)).append("\n");
    sb.append("    lowLatency: ").append(toIndentedString(lowLatency)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    outputs: ").append(toIndentedString(outputs)).append("\n");
    sb.append("    password: ").append(toIndentedString(password)).append("\n");
    sb.append("    playMaximumConnections: ").append(toIndentedString(playMaximumConnections)).append("\n");
    sb.append("    protocol: ").append(toIndentedString(protocol)).append("\n");
    sb.append("    recording: ").append(toIndentedString(recording)).append("\n");
    sb.append("    sourcePort: ").append(toIndentedString(sourcePort)).append("\n");
    sb.append("    sourceUrl: ").append(toIndentedString(sourceUrl)).append("\n");
    sb.append("    streamExtension: ").append(toIndentedString(streamExtension)).append("\n");
    sb.append("    streamName: ").append(toIndentedString(streamName)).append("\n");
    sb.append("    streamSmoother: ").append(toIndentedString(streamSmoother)).append("\n");
    sb.append("    streamSourceId: ").append(toIndentedString(streamSourceId)).append("\n");
    sb.append("    suppressStreamTargetStart: ").append(toIndentedString(suppressStreamTargetStart)).append("\n");
    sb.append("    transcoderType: ").append(toIndentedString(transcoderType)).append("\n");
    sb.append("    updatedAt: ").append(toIndentedString(updatedAt)).append("\n");
    sb.append("    username: ").append(toIndentedString(username)).append("\n");
    sb.append("    watermark: ").append(toIndentedString(watermark)).append("\n");
    sb.append("    watermarkHeight: ").append(toIndentedString(watermarkHeight)).append("\n");
    sb.append("    watermarkImageUrl: ").append(toIndentedString(watermarkImageUrl)).append("\n");
    sb.append("    watermarkOpacity: ").append(toIndentedString(watermarkOpacity)).append("\n");
    sb.append("    watermarkPosition: ").append(toIndentedString(watermarkPosition)).append("\n");
    sb.append("    watermarkWidth: ").append(toIndentedString(watermarkWidth)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

