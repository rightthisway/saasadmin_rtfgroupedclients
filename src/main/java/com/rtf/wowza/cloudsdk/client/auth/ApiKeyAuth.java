/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.auth;

import com.rtf.wowza.cloudsdk.client.Pair;

import java.util.Map;
import java.util.List;

/**
 * The Class ApiKeyAuth.
 */
public class ApiKeyAuth implements Authentication {
  
  /** The location. */
  private final String location;
  
  /** The param name. */
  private final String paramName;

  /** The api key. */
  private String apiKey;
  
  /** The api key prefix. */
  private String apiKeyPrefix;

  /**
   * Instantiates a new api key auth.
   *
   * @param location the location
   * @param paramName the param name
   */
  public ApiKeyAuth(String location, String paramName) {
    this.location = location;
    this.paramName = paramName;
  }

  /**
   * Gets the location.
   *
   * @return the location
   */
  public String getLocation() {
    return location;
  }

  /**
   * Gets the param name.
   *
   * @return the param name
   */
  public String getParamName() {
    return paramName;
  }

  /**
   * Gets the api key.
   *
   * @return the api key
   */
  public String getApiKey() {
    return apiKey;
  }

  /**
   * Sets the api key.
   *
   * @param apiKey the new api key
   */
  public void setApiKey(String apiKey) {
    this.apiKey = apiKey;
  }

  /**
   * Gets the api key prefix.
   *
   * @return the api key prefix
   */
  public String getApiKeyPrefix() {
    return apiKeyPrefix;
  }

  /**
   * Sets the api key prefix.
   *
   * @param apiKeyPrefix the new api key prefix
   */
  public void setApiKeyPrefix(String apiKeyPrefix) {
    this.apiKeyPrefix = apiKeyPrefix;
  }

  /**
   * Apply to params.
   *
   * @param queryParams the query params
   * @param headerParams the header params
   */
  @Override
  public void applyToParams(List<Pair> queryParams, Map<String, String> headerParams) {
    if (apiKey == null) {
      return;
    }
    String value;
    if (apiKeyPrefix != null) {
      value = apiKeyPrefix + " " + apiKey;
    } else {
      value = apiKey;
    }
    if ("query".equals(location)) {
      queryParams.add(new Pair(paramName, value));
    } else if ("header".equals(location)) {
      headerParams.put(paramName, value);
    }
  }
}
