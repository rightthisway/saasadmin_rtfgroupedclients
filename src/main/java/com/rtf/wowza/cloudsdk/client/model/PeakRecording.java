/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * The Class PeakRecording.
 */
@ApiModel(description = "")
@javax.annotation.Generated(value ="com.wowza.cloudsdk.JavaCreate", date = "2019-03-02T10:45:24.077Z")
public class PeakRecording {
  
  /** The bytes total. */
  @SerializedName("bytes_total")
  private Long bytesTotal = null;

  /**
   * Bytes total.
   *
   * @param bytesTotal the bytes total
   * @return the peak recording
   */
  public PeakRecording bytesTotal(Long bytesTotal) {
    this.bytesTotal = bytesTotal;
    return this;
  }

   /**
    * Gets the bytes total.
    *
    * @return the bytes total
    */
  @ApiModelProperty(example = "", value = "The amount, in bytes, used to store recordings during the selected time frame. If the time frame is in the past, <em>bytes_total</em> is the amount of storage that was used and billed. If the time frame includes the current billing period, <em>bytes_total</em> is the greatest amount of content stored in Wowza Streaming Cloud at any point to date in the billing period.")
  public Long getBytesTotal() {
    return bytesTotal;
  }

  /**
   * Sets the bytes total.
   *
   * @param bytesTotal the new bytes total
   */
  public void setBytesTotal(Long bytesTotal) {
    this.bytesTotal = bytesTotal;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PeakRecording peakRecording = (PeakRecording) o;
    return Objects.equals(this.bytesTotal, peakRecording.bytesTotal);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(bytesTotal);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PeakRecording {\n");
    
    sb.append("    bytesTotal: ").append(toIndentedString(bytesTotal)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

