/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class Meta.
 */
@ApiModel(description = "")
@javax.annotation.Generated(value ="com.wowza.cloudsdk.JavaCreate", date = "2019-03-02T10:45:24.077Z")
public class Meta {
  
  /** The status. */
  @SerializedName("status")
  private Integer status = null;

  /** The code. */
  @SerializedName("code")
  private String code = null;

  /** The title. */
  @SerializedName("title")
  private String title = null;

  /** The message. */
  @SerializedName("message")
  private String message = null;

  /** The description. */
  @SerializedName("description")
  private String description = null;

  /**
   * Status.
   *
   * @param status the status
   * @return the meta
   */
  public Meta status(Integer status) {
    this.status = status;
    return this;
  }

   /**
    * Gets the status.
    *
    * @return the status
    */
  @ApiModelProperty(example = "", value = "")
  public Integer getStatus() {
    return status;
  }

  /**
   * Sets the status.
   *
   * @param status the new status
   */
  public void setStatus(Integer status) {
    this.status = status;
  }

  /**
   * Code.
   *
   * @param code the code
   * @return the meta
   */
  public Meta code(String code) {
    this.code = code;
    return this;
  }

   /**
    * Gets the code.
    *
    * @return the code
    */
  @ApiModelProperty(example = "", value = "")
  public String getCode() {
    return code;
  }

  /**
   * Sets the code.
   *
   * @param code the new code
   */
  public void setCode(String code) {
    this.code = code;
  }

  /**
   * Title.
   *
   * @param title the title
   * @return the meta
   */
  public Meta title(String title) {
    this.title = title;
    return this;
  }

   /**
    * Gets the title.
    *
    * @return the title
    */
  @ApiModelProperty(example = "", value = "")
  public String getTitle() {
    return title;
  }

  /**
   * Sets the title.
   *
   * @param title the new title
   */
  public void setTitle(String title) {
    this.title = title;
  }

  /**
   * Message.
   *
   * @param message the message
   * @return the meta
   */
  public Meta message(String message) {
    this.message = message;
    return this;
  }

   /**
    * Gets the message.
    *
    * @return the message
    */
  @ApiModelProperty(example = "", value = "")
  public String getMessage() {
    return message;
  }

  /**
   * Sets the message.
   *
   * @param message the new message
   */
  public void setMessage(String message) {
    this.message = message;
  }

  /**
   * Description.
   *
   * @param description the description
   * @return the meta
   */
  public Meta description(String description) {
    this.description = description;
    return this;
  }

   /**
    * Gets the description.
    *
    * @return the description
    */
  @ApiModelProperty(example = "", value = "")
  public String getDescription() {
    return description;
  }

  /**
   * Sets the description.
   *
   * @param description the new description
   */
  public void setDescription(String description) {
    this.description = description;
  }

  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Meta meta = (Meta) o;
    return Objects.equals(this.status, meta.status) &&
        Objects.equals(this.code, meta.code) &&
        Objects.equals(this.title, meta.title) &&
        Objects.equals(this.message, meta.message) &&
        Objects.equals(this.description, meta.description);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(status, code, title, message, description);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Meta {\n");
    
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    code: ").append(toIndentedString(code)).append("\n");
    sb.append("    title: ").append(toIndentedString(title)).append("\n");
    sb.append("    message: ").append(toIndentedString(message)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

