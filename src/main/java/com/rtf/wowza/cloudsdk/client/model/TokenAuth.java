/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.time.OffsetDateTime;

/**
 * The Class TokenAuth.
 */
@ApiModel(description = "")
public class TokenAuth {
  
  /** The created at. */
  @SerializedName("created_at")
  private OffsetDateTime createdAt = null;

  /** The enabled. */
  @SerializedName("enabled")
  private Boolean enabled = null;

  /** The stream target id. */
  @SerializedName("stream_target_id")
  private String streamTargetId = null;

  /** The trusted shared secret. */
  @SerializedName("trusted_shared_secret")
  private String trustedSharedSecret = null;

  /** The updated at. */
  @SerializedName("updated_at")
  private OffsetDateTime updatedAt = null;

  /**
   * Created at.
   *
   * @param createdAt the created at
   * @return the token auth
   */
  public TokenAuth createdAt(OffsetDateTime createdAt) {
    this.createdAt = createdAt;
    return this;
  }

   /**
    * Gets the created at.
    *
    * @return the created at
    */
  @ApiModelProperty(example = "", value = "The date and time that the token authorization was created.")
  public OffsetDateTime getCreatedAt() {
    return createdAt;
  }

  /**
   * Sets the created at.
   *
   * @param createdAt the new created at
   */
  public void setCreatedAt(OffsetDateTime createdAt) {
    this.createdAt = createdAt;
  }

  /**
   * Enabled.
   *
   * @param enabled the enabled
   * @return the token auth
   */
  public TokenAuth enabled(Boolean enabled) {
    this.enabled = enabled;
    return this;
  }

   /**
    * Checks if is enabled.
    *
    * @return the boolean
    */
  @ApiModelProperty(value = "Specify <strong>true</strong> to enable token authorization or <strong>false</strong> to disable.")
  public Boolean isEnabled() {
    return enabled;
  }

  /**
   * Sets the enabled.
   *
   * @param enabled the new enabled
   */
  public void setEnabled(Boolean enabled) {
    this.enabled = enabled;
  }

  /**
   * Stream target id.
   *
   * @param streamTargetId the stream target id
   * @return the token auth
   */
  public TokenAuth streamTargetId(String streamTargetId) {
    this.streamTargetId = streamTargetId;
    return this;
  }

   /**
    * Gets the stream target id.
    *
    * @return the stream target id
    */
  @ApiModelProperty(example = "", value = "The unique alphanumeric string that identifies the stream target.")
  public String getStreamTargetId() {
    return streamTargetId;
  }

  /**
   * Sets the stream target id.
   *
   * @param streamTargetId the new stream target id
   */
  public void setStreamTargetId(String streamTargetId) {
    this.streamTargetId = streamTargetId;
  }

  /**
   * Trusted shared secret.
   *
   * @param trustedSharedSecret the trusted shared secret
   * @return the token auth
   */
  public TokenAuth trustedSharedSecret(String trustedSharedSecret) {
    this.trustedSharedSecret = trustedSharedSecret;
    return this;
  }

   /**
    * Gets the trusted shared secret.
    *
    * @return the trusted shared secret
    */
  @ApiModelProperty(example = "", value = "The trusted shared secret of the token authorization. Must contain only hexadecimal characters and be an even number of total characters not exceeding 32.")
  public String getTrustedSharedSecret() {
    return trustedSharedSecret;
  }

  /**
   * Sets the trusted shared secret.
   *
   * @param trustedSharedSecret the new trusted shared secret
   */
  public void setTrustedSharedSecret(String trustedSharedSecret) {
    this.trustedSharedSecret = trustedSharedSecret;
  }

  /**
   * Updated at.
   *
   * @param updatedAt the updated at
   * @return the token auth
   */
  public TokenAuth updatedAt(OffsetDateTime updatedAt) {
    this.updatedAt = updatedAt;
    return this;
  }

   /**
    * Gets the updated at.
    *
    * @return the updated at
    */
  @ApiModelProperty(example = "", value = "The date and time that the token authorization was updated.")
  public OffsetDateTime getUpdatedAt() {
    return updatedAt;
  }

  /**
   * Sets the updated at.
   *
   * @param updatedAt the new updated at
   */
  public void setUpdatedAt(OffsetDateTime updatedAt) {
    this.updatedAt = updatedAt;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TokenAuth tokenAuth = (TokenAuth) o;
    return Objects.equals(this.createdAt, tokenAuth.createdAt) &&
        Objects.equals(this.enabled, tokenAuth.enabled) &&
        Objects.equals(this.streamTargetId, tokenAuth.streamTargetId) &&
        Objects.equals(this.trustedSharedSecret, tokenAuth.trustedSharedSecret) &&
        Objects.equals(this.updatedAt, tokenAuth.updatedAt);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(createdAt, enabled, streamTargetId, trustedSharedSecret, updatedAt);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TokenAuth {\n");
    
    sb.append("    createdAt: ").append(toIndentedString(createdAt)).append("\n");
    sb.append("    enabled: ").append(toIndentedString(enabled)).append("\n");
    sb.append("    streamTargetId: ").append(toIndentedString(streamTargetId)).append("\n");
    sb.append("    trustedSharedSecret: ").append(toIndentedString(trustedSharedSecret)).append("\n");
    sb.append("    updatedAt: ").append(toIndentedString(updatedAt)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

