/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.rtf.wowza.cloudsdk.client.model.StreamTargetConnectioncode;
import java.io.IOException;

/**
 * The Class StreamTargetCreateConnectioncode.
 */
public class StreamTargetCreateConnectioncode {
  
  /** The stream target. */
  @SerializedName("stream_target")
  private StreamTargetConnectioncode streamTarget = null;

  /**
   * Stream target.
   *
   * @param streamTarget the stream target
   * @return the stream target create connectioncode
   */
  public StreamTargetCreateConnectioncode streamTarget(StreamTargetConnectioncode streamTarget) {
    this.streamTarget = streamTarget;
    return this;
  }

   /**
    * Gets the stream target connectioncode.
    *
    * @return the stream target connectioncode
    */
  @ApiModelProperty(required = true, value = "")
  public StreamTargetConnectioncode getStreamTargetConnectioncode() {
    return streamTarget;
  }

  /**
   * Sets the stream target connectioncode.
   *
   * @param streamTarget the new stream target connectioncode
   */
  public void setStreamTargetConnectioncode(StreamTargetConnectioncode streamTarget) {
    this.streamTarget = streamTarget;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    StreamTargetCreateConnectioncode inlineResponse20026 = (StreamTargetCreateConnectioncode) o;
    return Objects.equals(this.streamTarget, inlineResponse20026.streamTarget);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(streamTarget);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class StreamTargetCreateConnectioncode {\n");
    
    sb.append("    streamTarget: ").append(toIndentedString(streamTarget)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

