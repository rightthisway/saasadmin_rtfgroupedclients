/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.rtf.wowza.cloudsdk.client.model.OutputStreamTarget;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.time.OffsetDateTime;

/**
 * The Class Output.
 */
@ApiModel(description = "")
public class Output {
  
  /** The aspect ratio height. */
  @SerializedName("aspect_ratio_height")
  private Integer aspectRatioHeight = null;

  /** The aspect ratio width. */
  @SerializedName("aspect_ratio_width")
  private Integer aspectRatioWidth = null;

  /** The bitrate audio. */
  @SerializedName("bitrate_audio")
  private Integer bitrateAudio = null;

  /** The bitrate video. */
  @SerializedName("bitrate_video")
  private Integer bitrateVideo = null;

  /** The created at. */
  @SerializedName("created_at")
  private OffsetDateTime createdAt = null;

  /**
   * The Enum FramerateReductionEnum.
   */
  @JsonAdapter(FramerateReductionEnum.Adapter.class)
  public enum FramerateReductionEnum {
    
    /** The  0. */
    _0("0"),
    
    /** The  1 2. */
    _1_2("1/2"),
    
    /** The  1 4. */
    _1_4("1/4"),
    
    /** The  1 25. */
    _1_25("1/25"),
    
    /** The  1 30. */
    _1_30("1/30"),
    
    /** The  1 50. */
    _1_50("1/50"),
    
    /** The  1 60. */
    _1_60("1/60");

    /** The value. */
    private String value;

    /**
     * Instantiates a new framerate reduction enum.
     *
     * @param value the value
     */
    FramerateReductionEnum(String value) {
      this.value = value;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
      return value;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString() {
      return String.valueOf(value);
    }

    /**
     * From value.
     *
     * @param text the text
     * @return the framerate reduction enum
     */
    public static FramerateReductionEnum fromValue(String text) {
      for (FramerateReductionEnum b : FramerateReductionEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    /**
     * The Class Adapter.
     */
    public static class Adapter extends TypeAdapter<FramerateReductionEnum> {
      
      /**
       * Write.
       *
       * @param jsonWriter the json writer
       * @param enumeration the enumeration
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public void write(final JsonWriter jsonWriter, final FramerateReductionEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      /**
       * Read.
       *
       * @param jsonReader the json reader
       * @return the framerate reduction enum
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public FramerateReductionEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return FramerateReductionEnum.fromValue(String.valueOf(value));
      }
    }
  }

  /** The framerate reduction. */
  @SerializedName("framerate_reduction")
  private FramerateReductionEnum framerateReduction = null;

  /**
   * The Enum H264ProfileEnum.
   */
  @JsonAdapter(H264ProfileEnum.Adapter.class)
  public enum H264ProfileEnum {
    
    /** The main. */
    MAIN("main"),
    
    /** The baseline. */
    BASELINE("baseline"),
    
    /** The high. */
    HIGH("high");

    /** The value. */
    private String value;

    /**
     * Instantiates a new h 264 profile enum.
     *
     * @param value the value
     */
    H264ProfileEnum(String value) {
      this.value = value;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
      return value;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString() {
      return String.valueOf(value);
    }

    /**
     * From value.
     *
     * @param text the text
     * @return the h 264 profile enum
     */
    public static H264ProfileEnum fromValue(String text) {
      for (H264ProfileEnum b : H264ProfileEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    /**
     * The Class Adapter.
     */
    public static class Adapter extends TypeAdapter<H264ProfileEnum> {
      
      /**
       * Write.
       *
       * @param jsonWriter the json writer
       * @param enumeration the enumeration
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public void write(final JsonWriter jsonWriter, final H264ProfileEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      /**
       * Read.
       *
       * @param jsonReader the json reader
       * @return the h 264 profile enum
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public H264ProfileEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return H264ProfileEnum.fromValue(String.valueOf(value));
      }
    }
  }

  /** The h 264 profile. */
  @SerializedName("h264_profile")
  private H264ProfileEnum h264Profile = null;

  /** The id. */
  @SerializedName("id")
  private String id = null;

  /**
   * The Enum KeyframesEnum.
   */
  @JsonAdapter(KeyframesEnum.Adapter.class)
  public enum KeyframesEnum {
    
    /** The follow source. */
    FOLLOW_SOURCE("follow_source"),
    
    /** The  25. */
    _25("25"),
    
    /** The  30. */
    _30("30"),
    
    /** The  50. */
    _50("50"),
    
    /** The  60. */
    _60("60"),
    
    /** The  100. */
    _100("100"),
    
    /** The  120. */
    _120("120");

    /** The value. */
    private String value;

    /**
     * Instantiates a new keyframes enum.
     *
     * @param value the value
     */
    KeyframesEnum(String value) {
      this.value = value;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
      return value;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString() {
      return String.valueOf(value);
    }

    /**
     * From value.
     *
     * @param text the text
     * @return the keyframes enum
     */
    public static KeyframesEnum fromValue(String text) {
      for (KeyframesEnum b : KeyframesEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    /**
     * The Class Adapter.
     */
    public static class Adapter extends TypeAdapter<KeyframesEnum> {
      
      /**
       * Write.
       *
       * @param jsonWriter the json writer
       * @param enumeration the enumeration
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public void write(final JsonWriter jsonWriter, final KeyframesEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      /**
       * Read.
       *
       * @param jsonReader the json reader
       * @return the keyframes enum
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public KeyframesEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return KeyframesEnum.fromValue(String.valueOf(value));
      }
    }
  }

  /** The keyframes. */
  @SerializedName("keyframes")
  private KeyframesEnum keyframes = null;

  /** The name. */
  @SerializedName("name")
  private String name = null;

  /** The output stream targets. */
  @SerializedName("output_stream_targets")
  private List<OutputStreamTarget> outputStreamTargets = null;

  /** The passthrough audio. */
  @SerializedName("passthrough_audio")
  private Boolean passthroughAudio = null;

  /** The passthrough video. */
  @SerializedName("passthrough_video")
  private Boolean passthroughVideo = null;

  /**
   * The Enum StreamFormatEnum.
   */
  @JsonAdapter(StreamFormatEnum.Adapter.class)
  public enum StreamFormatEnum {
    
    /** The audiovideo. */
    AUDIOVIDEO("audiovideo"),
    
    /** The videoonly. */
    VIDEOONLY("videoonly"),
    
    /** The audioonly. */
    AUDIOONLY("audioonly");

    /** The value. */
    private String value;

    /**
     * Instantiates a new stream format enum.
     *
     * @param value the value
     */
    StreamFormatEnum(String value) {
      this.value = value;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
      return value;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString() {
      return String.valueOf(value);
    }

    /**
     * From value.
     *
     * @param text the text
     * @return the stream format enum
     */
    public static StreamFormatEnum fromValue(String text) {
      for (StreamFormatEnum b : StreamFormatEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    /**
     * The Class Adapter.
     */
    public static class Adapter extends TypeAdapter<StreamFormatEnum> {
      
      /**
       * Write.
       *
       * @param jsonWriter the json writer
       * @param enumeration the enumeration
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public void write(final JsonWriter jsonWriter, final StreamFormatEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      /**
       * Read.
       *
       * @param jsonReader the json reader
       * @return the stream format enum
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public StreamFormatEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return StreamFormatEnum.fromValue(String.valueOf(value));
      }
    }
  }

  /** The stream format. */
  @SerializedName("stream_format")
  private StreamFormatEnum streamFormat = null;

  /** The transcoder id. */
  @SerializedName("transcoder_id")
  private String transcoderId = null;

  /** The updated at. */
  @SerializedName("updated_at")
  private OffsetDateTime updatedAt = null;

  /**
   * Aspect ratio height.
   *
   * @param aspectRatioHeight the aspect ratio height
   * @return the output
   */
  public Output aspectRatioHeight(Integer aspectRatioHeight) {
    this.aspectRatioHeight = aspectRatioHeight;
    return this;
  }

   /**
    * Gets the aspect ratio height.
    *
    * @return the aspect ratio height
    */
  @ApiModelProperty(example = "", value = "The height, in pixels, of the output rendition. Should correspond to a widescreen or standard aspect ratio and be divisible by 8. The default is <strong>1080</strong>.")
  public Integer getAspectRatioHeight() {
    return aspectRatioHeight;
  }

  /**
   * Sets the aspect ratio height.
   *
   * @param aspectRatioHeight the new aspect ratio height
   */
  public void setAspectRatioHeight(Integer aspectRatioHeight) {
    this.aspectRatioHeight = aspectRatioHeight;
  }

  /**
   * Aspect ratio width.
   *
   * @param aspectRatioWidth the aspect ratio width
   * @return the output
   */
  public Output aspectRatioWidth(Integer aspectRatioWidth) {
    this.aspectRatioWidth = aspectRatioWidth;
    return this;
  }

   /**
    * Gets the aspect ratio width.
    *
    * @return the aspect ratio width
    */
  @ApiModelProperty(example = "", value = "The width, in pixels, of the output rendition. Should correspond to a widescreen or standard aspect ratio and be divisible by 8. The default is <strong>1980</strong>.")
  public Integer getAspectRatioWidth() {
    return aspectRatioWidth;
  }

  /**
   * Sets the aspect ratio width.
   *
   * @param aspectRatioWidth the new aspect ratio width
   */
  public void setAspectRatioWidth(Integer aspectRatioWidth) {
    this.aspectRatioWidth = aspectRatioWidth;
  }

  /**
   * Bitrate audio.
   *
   * @param bitrateAudio the bitrate audio
   * @return the output
   */
  public Output bitrateAudio(Integer bitrateAudio) {
    this.bitrateAudio = bitrateAudio;
    return this;
  }

   /**
    * Gets the bitrate audio.
    *
    * @return the bitrate audio
    */
  @ApiModelProperty(example = "", value = "The audio bitrate, in kilobits per second (Kbps). Must be between <strong>0</strong> (for passthrough audio) and <strong>1000</strong>. The default is <strong>128</strong>.")
  public Integer getBitrateAudio() {
    return bitrateAudio;
  }

  /**
   * Sets the bitrate audio.
   *
   * @param bitrateAudio the new bitrate audio
   */
  public void setBitrateAudio(Integer bitrateAudio) {
    this.bitrateAudio = bitrateAudio;
  }

  /**
   * Bitrate video.
   *
   * @param bitrateVideo the bitrate video
   * @return the output
   */
  public Output bitrateVideo(Integer bitrateVideo) {
    this.bitrateVideo = bitrateVideo;
    return this;
  }

   /**
    * Gets the bitrate video.
    *
    * @return the bitrate video
    */
  @ApiModelProperty(example = "", value = "The video bitrate, in kilobits per second (Kbps). Must be between <strong>0</strong> (for passthrough video) and <strong>10240</strong>. The default is <strong>4000</strong>.")
  public Integer getBitrateVideo() {
    return bitrateVideo;
  }

  /**
   * Sets the bitrate video.
   *
   * @param bitrateVideo the new bitrate video
   */
  public void setBitrateVideo(Integer bitrateVideo) {
    this.bitrateVideo = bitrateVideo;
  }

  /**
   * Created at.
   *
   * @param createdAt the created at
   * @return the output
   */
  public Output createdAt(OffsetDateTime createdAt) {
    this.createdAt = createdAt;
    return this;
  }

   /**
    * Gets the created at.
    *
    * @return the created at
    */
  @ApiModelProperty(example = "", value = "The date and time that the output rendition was created.")
  public OffsetDateTime getCreatedAt() {
    return createdAt;
  }

  /**
   * Sets the created at.
   *
   * @param createdAt the new created at
   */
  public void setCreatedAt(OffsetDateTime createdAt) {
    this.createdAt = createdAt;
  }

  /**
   * Framerate reduction.
   *
   * @param framerateReduction the framerate reduction
   * @return the output
   */
  public Output framerateReduction(FramerateReductionEnum framerateReduction) {
    this.framerateReduction = framerateReduction;
    return this;
  }

   /**
    * Gets the framerate reduction.
    *
    * @return the framerate reduction
    */
  @ApiModelProperty(example = "", value = "Reduce the frame rate of the transcoded output rendition. The default, <strong>0</strong>, uses the encoded stream's frame rate without reduction.")
  public FramerateReductionEnum getFramerateReduction() {
    return framerateReduction;
  }

  /**
   * Sets the framerate reduction.
   *
   * @param framerateReduction the new framerate reduction
   */
  public void setFramerateReduction(FramerateReductionEnum framerateReduction) {
    this.framerateReduction = framerateReduction;
  }

  /**
   * H 264 profile.
   *
   * @param h264Profile the h 264 profile
   * @return the output
   */
  public Output h264Profile(H264ProfileEnum h264Profile) {
    this.h264Profile = h264Profile;
    return this;
  }

   /**
    * Gets the h 264 profile.
    *
    * @return the h 264 profile
    */
  @ApiModelProperty(example = "", value = "The encoding method. Specify <strong>main</strong> for desktop streaming, <strong>baseline</strong> for playback on mobile devices, or <strong>high</strong> for HD playback. The default is <strong>high</strong>.")
  public H264ProfileEnum getH264Profile() {
    return h264Profile;
  }

  /**
   * Sets the h 264 profile.
   *
   * @param h264Profile the new h 264 profile
   */
  public void setH264Profile(H264ProfileEnum h264Profile) {
    this.h264Profile = h264Profile;
  }

  /**
   * Id.
   *
   * @param id the id
   * @return the output
   */
  public Output id(String id) {
    this.id = id;
    return this;
  }

   /**
    * Gets the id.
    *
    * @return the id
    */
  @ApiModelProperty(example = "", value = "The unique alphanumeric string that identifies the output rendition.")
  public String getId() {
    return id;
  }

  /**
   * Sets the id.
   *
   * @param id the new id
   */
  public void setId(String id) {
    this.id = id;
  }

  /**
   * Keyframes.
   *
   * @param keyframes the keyframes
   * @return the output
   */
  public Output keyframes(KeyframesEnum keyframes) {
    this.keyframes = keyframes;
    return this;
  }

   /**
    * Gets the keyframes.
    *
    * @return the keyframes
    */
  @ApiModelProperty(example = "", value = "The interval used to define the compression applied to a group of frames. The default, <strong>follow_source</strong>, uses the keyframe interval of the source video.")
  public KeyframesEnum getKeyframes() {
    return keyframes;
  }

  /**
   * Sets the keyframes.
   *
   * @param keyframes the new keyframes
   */
  public void setKeyframes(KeyframesEnum keyframes) {
    this.keyframes = keyframes;
  }

  /**
   * Name.
   *
   * @param name the name
   * @return the output
   */
  public Output name(String name) {
    this.name = name;
    return this;
  }

   /**
    * Gets the name.
    *
    * @return the name
    */
  @ApiModelProperty(example = "", value = "A descriptive name for the output (generated, not writable).")
  public String getName() {
    return name;
  }

  /**
   * Sets the name.
   *
   * @param name the new name
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Output stream targets.
   *
   * @param outputStreamTargets the output stream targets
   * @return the output
   */
  public Output outputStreamTargets(List<OutputStreamTarget> outputStreamTargets) {
    this.outputStreamTargets = outputStreamTargets;
    return this;
  }

  /**
   * Adds the output stream targets item.
   *
   * @param outputStreamTargetsItem the output stream targets item
   * @return the output
   */
  public Output addOutputStreamTargetsItem(OutputStreamTarget outputStreamTargetsItem) {
    if (this.outputStreamTargets == null) {
      this.outputStreamTargets = new ArrayList<OutputStreamTarget>();
    }
    this.outputStreamTargets.add(outputStreamTargetsItem);
    return this;
  }

   /**
    * Gets the output stream targets.
    *
    * @return the output stream targets
    */
  @ApiModelProperty(value = "")
  public List<OutputStreamTarget> getOutputStreamTargets() {
    return outputStreamTargets;
  }

  /**
   * Sets the output stream targets.
   *
   * @param outputStreamTargets the new output stream targets
   */
  public void setOutputStreamTargets(List<OutputStreamTarget> outputStreamTargets) {
    this.outputStreamTargets = outputStreamTargets;
  }

  /**
   * Passthrough audio.
   *
   * @param passthroughAudio the passthrough audio
   * @return the output
   */
  public Output passthroughAudio(Boolean passthroughAudio) {
    this.passthroughAudio = passthroughAudio;
    return this;
  }

   /**
    * Checks if is passthrough audio.
    *
    * @return the boolean
    */
  @ApiModelProperty(value = "If <strong>true</strong>, sends the audio track to the target without transcoding. The default is <strong>false</strong>.")
  public Boolean isPassthroughAudio() {
    return passthroughAudio;
  }

  /**
   * Sets the passthrough audio.
   *
   * @param passthroughAudio the new passthrough audio
   */
  public void setPassthroughAudio(Boolean passthroughAudio) {
    this.passthroughAudio = passthroughAudio;
  }

  /**
   * Passthrough video.
   *
   * @param passthroughVideo the passthrough video
   * @return the output
   */
  public Output passthroughVideo(Boolean passthroughVideo) {
    this.passthroughVideo = passthroughVideo;
    return this;
  }

   /**
    * Checks if is passthrough video.
    *
    * @return the boolean
    */
  @ApiModelProperty(value = "If <strong>true</strong>, sends the video track to the target without transcoding. The default is <strong>false</strong>.")
  public Boolean isPassthroughVideo() {
    return passthroughVideo;
  }

  /**
   * Sets the passthrough video.
   *
   * @param passthroughVideo the new passthrough video
   */
  public void setPassthroughVideo(Boolean passthroughVideo) {
    this.passthroughVideo = passthroughVideo;
  }

  /**
   * Stream format.
   *
   * @param streamFormat the stream format
   * @return the output
   */
  public Output streamFormat(StreamFormatEnum streamFormat) {
    this.streamFormat = streamFormat;
    return this;
  }

   /**
    * Gets the stream format.
    *
    * @return the stream format
    */
  @ApiModelProperty(example = "", value = "The contents of the stream. The default is both audio and video (<strong>audiovideo</strong>).")
  public StreamFormatEnum getStreamFormat() {
    return streamFormat;
  }

  /**
   * Sets the stream format.
   *
   * @param streamFormat the new stream format
   */
  public void setStreamFormat(StreamFormatEnum streamFormat) {
    this.streamFormat = streamFormat;
  }

  /**
   * Transcoder id.
   *
   * @param transcoderId the transcoder id
   * @return the output
   */
  public Output transcoderId(String transcoderId) {
    this.transcoderId = transcoderId;
    return this;
  }

   /**
    * Gets the transcoder id.
    *
    * @return the transcoder id
    */
  @ApiModelProperty(example = "", value = "The unique alphanumeric string that identifies the transcoder.")
  public String getTranscoderId() {
    return transcoderId;
  }

  /**
   * Sets the transcoder id.
   *
   * @param transcoderId the new transcoder id
   */
  public void setTranscoderId(String transcoderId) {
    this.transcoderId = transcoderId;
  }

  /**
   * Updated at.
   *
   * @param updatedAt the updated at
   * @return the output
   */
  public Output updatedAt(OffsetDateTime updatedAt) {
    this.updatedAt = updatedAt;
    return this;
  }

   /**
    * Gets the updated at.
    *
    * @return the updated at
    */
  @ApiModelProperty(example = "", value = "The date and time that the output rendition was updated.")
  public OffsetDateTime getUpdatedAt() {
    return updatedAt;
  }

  /**
   * Sets the updated at.
   *
   * @param updatedAt the new updated at
   */
  public void setUpdatedAt(OffsetDateTime updatedAt) {
    this.updatedAt = updatedAt;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Output output = (Output) o;
    return Objects.equals(this.aspectRatioHeight, output.aspectRatioHeight) &&
        Objects.equals(this.aspectRatioWidth, output.aspectRatioWidth) &&
        Objects.equals(this.bitrateAudio, output.bitrateAudio) &&
        Objects.equals(this.bitrateVideo, output.bitrateVideo) &&
        Objects.equals(this.createdAt, output.createdAt) &&
        Objects.equals(this.framerateReduction, output.framerateReduction) &&
        Objects.equals(this.h264Profile, output.h264Profile) &&
        Objects.equals(this.id, output.id) &&
        Objects.equals(this.keyframes, output.keyframes) &&
        Objects.equals(this.name, output.name) &&
        Objects.equals(this.outputStreamTargets, output.outputStreamTargets) &&
        Objects.equals(this.passthroughAudio, output.passthroughAudio) &&
        Objects.equals(this.passthroughVideo, output.passthroughVideo) &&
        Objects.equals(this.streamFormat, output.streamFormat) &&
        Objects.equals(this.transcoderId, output.transcoderId) &&
        Objects.equals(this.updatedAt, output.updatedAt);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(aspectRatioHeight, aspectRatioWidth, bitrateAudio, bitrateVideo, createdAt, framerateReduction, h264Profile, id, keyframes, name, outputStreamTargets, passthroughAudio, passthroughVideo, streamFormat, transcoderId, updatedAt);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Output {\n");
    
    sb.append("    aspectRatioHeight: ").append(toIndentedString(aspectRatioHeight)).append("\n");
    sb.append("    aspectRatioWidth: ").append(toIndentedString(aspectRatioWidth)).append("\n");
    sb.append("    bitrateAudio: ").append(toIndentedString(bitrateAudio)).append("\n");
    sb.append("    bitrateVideo: ").append(toIndentedString(bitrateVideo)).append("\n");
    sb.append("    createdAt: ").append(toIndentedString(createdAt)).append("\n");
    sb.append("    framerateReduction: ").append(toIndentedString(framerateReduction)).append("\n");
    sb.append("    h264Profile: ").append(toIndentedString(h264Profile)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    keyframes: ").append(toIndentedString(keyframes)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    outputStreamTargets: ").append(toIndentedString(outputStreamTargets)).append("\n");
    sb.append("    passthroughAudio: ").append(toIndentedString(passthroughAudio)).append("\n");
    sb.append("    passthroughVideo: ").append(toIndentedString(passthroughVideo)).append("\n");
    sb.append("    streamFormat: ").append(toIndentedString(streamFormat)).append("\n");
    sb.append("    transcoderId: ").append(toIndentedString(transcoderId)).append("\n");
    sb.append("    updatedAt: ").append(toIndentedString(updatedAt)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

