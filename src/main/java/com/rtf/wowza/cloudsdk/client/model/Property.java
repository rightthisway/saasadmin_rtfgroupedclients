/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * The Class Property.
 */
@ApiModel(description = "")
public class Property {
  
  /**
   * The Enum KeyEnum.
   */
  @JsonAdapter(KeyEnum.Adapter.class)
  public enum KeyEnum {
    
    /** The chunksize. */
    CHUNKSIZE("chunkSize"),
    
    /** The convertamfdata. */
    CONVERTAMFDATA("convertAMFData"),
    
    /** The sendssl. */
    SENDSSL("sendSSL"),
    
    /** The playssl. */
    PLAYSSL("playSSL"),
    
    /** The playlistseconds. */
    PLAYLISTSECONDS("playlistSeconds"),
    
    /** The redundantchunklists. */
    REDUNDANTCHUNKLISTS("redundantChunklists"),
    
    /** The relativeplaylists. */
    RELATIVEPLAYLISTS("relativePlaylists");

    /** The value. */
    private String value;

    /**
     * Instantiates a new key enum.
     *
     * @param value the value
     */
    KeyEnum(String value) {
      this.value = value;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
      return value;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString() {
      return String.valueOf(value);
    }

    /**
     * From value.
     *
     * @param text the text
     * @return the key enum
     */
    public static KeyEnum fromValue(String text) {
      for (KeyEnum b : KeyEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    /**
     * The Class Adapter.
     */
    public static class Adapter extends TypeAdapter<KeyEnum> {
      
      /**
       * Write.
       *
       * @param jsonWriter the json writer
       * @param enumeration the enumeration
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public void write(final JsonWriter jsonWriter, final KeyEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      /**
       * Read.
       *
       * @param jsonReader the json reader
       * @return the key enum
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public KeyEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return KeyEnum.fromValue(String.valueOf(value));
      }
    }
  }

  /** The key. */
  @SerializedName("key")
  private KeyEnum key = null;

  /**
   * The Enum SectionEnum.
   */
  @JsonAdapter(SectionEnum.Adapter.class)
  public enum SectionEnum {
    
    /** The hls. */
    HLS("hls"),
    
    /** The playlist. */
    PLAYLIST("playlist");

    /** The value. */
    private String value;

    /**
     * Instantiates a new section enum.
     *
     * @param value the value
     */
    SectionEnum(String value) {
      this.value = value;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
      return value;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString() {
      return String.valueOf(value);
    }

    /**
     * From value.
     *
     * @param text the text
     * @return the section enum
     */
    public static SectionEnum fromValue(String text) {
      for (SectionEnum b : SectionEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    /**
     * The Class Adapter.
     */
    public static class Adapter extends TypeAdapter<SectionEnum> {
      
      /**
       * Write.
       *
       * @param jsonWriter the json writer
       * @param enumeration the enumeration
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public void write(final JsonWriter jsonWriter, final SectionEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      /**
       * Read.
       *
       * @param jsonReader the json reader
       * @return the section enum
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public SectionEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return SectionEnum.fromValue(String.valueOf(value));
      }
    }
  }

  /** The section. */
  @SerializedName("section")
  private SectionEnum section = null;

  /** The value. */
  @SerializedName("value")
  private String value = null;

  /**
   * Key.
   *
   * @param key the key
   * @return the property
   */
  public Property key(KeyEnum key) {
    this.key = key;
    return this;
  }

   /**
    * Gets the key.
    *
    * @return the key
    */
  @ApiModelProperty(example = "chunkSize", required = true, value = "<strong>chunkSize</strong> defines the duration of the time-based audio and video chunks that Wowza Streaming Cloud delivers to the target. <br /><br /><strong>convertAMFData</strong> determines whether Wowza Streaming Cloud converts incoming AMF data into ID3 tags. <br /><br /><strong>sendSSL</strong> determines whether Wowza Streaming Cloud sends the stream from the transcoder to the target by using SSL (HTTPS). <br /><br /><strong>playSSL</strong> determines whether Wowza Streaming Cloud sends the stream from the target to the player by using SSL (HTTPS). <br /><br /><strong>playlistSeconds</strong> defines the maximum allowable length of the playlist. <br /><br /><strong>redundantChunklists</strong> determines whether Wowza Streaming Cloud creates redundant chunklists within a playlist. If a primary chunklist within a playlist fails, players that support redundancy during playback can switch to the redundant chunklist.<br /><br />**Note:** Enabling **redundantChunklists** increases playback reliability but doubles egress data usage and associated charges.<br /><br /><strong>relativePlaylists</strong> allows the viewer to watch the stream over HTTP and HTTPS, whichever protocol their browser calls.")
  public KeyEnum getKey() {
    return key;
  }

  /**
   * Sets the key.
   *
   * @param key the new key
   */
  public void setKey(KeyEnum key) {
    this.key = key;
  }

  /**
   * Section.
   *
   * @param section the section
   * @return the property
   */
  public Property section(SectionEnum section) {
    this.section = section;
    return this;
  }

   /**
    * Gets the section.
    *
    * @return the section
    */
  @ApiModelProperty(example = "hls", required = true, value = "The section of the stream target configuration table that contains the property. <br /><br />The valid value for <strong>chunkSize</strong>, <strong>convertAMFData</strong>, and <strong>sendSSL</strong> is <strong>hls</strong>. <br /><br />The valid value for <strong>playSSL</strong>, <strong>playlistSeconds</strong>, <strong>redundantChunklists</strong>, and <strong>relativePlaylists</strong> is <strong>playlist</strong>.")
  public SectionEnum getSection() {
    return section;
  }

  /**
   * Sets the section.
   *
   * @param section the new section
   */
  public void setSection(SectionEnum section) {
    this.section = section;
  }

  /**
   * Value.
   *
   * @param value the value
   * @return the property
   */
  public Property value(String value) {
    this.value = value;
    return this;
  }

   /**
    * Gets the value.
    *
    * @return the value
    */
  @ApiModelProperty(example = "6", required = true, value = "For <strong>chunkSize</strong>, use <strong>2</strong>, <strong>4</strong>, <strong>6</strong>, <strong>8</strong>, or <strong>10</strong>, expressed as a string or an integer. <br /><br />For <strong>convertAMFData</strong>, <strong>sendSSL</strong>, <strong>playSSL</strong>, <strong>redundantChunklists</strong>, and <strong>relativePlaylists</strong> use <strong>true</strong> or <strong>false</strong>, expressed as a string or a Boolean. <br /><br />For <strong>playlistSeconds</strong>, use any value between <strong>6</strong> and <strong>200</strong>, expressed as a string or an integer.")
  public String getValue() {
    return value;
  }

  /**
   * Sets the value.
   *
   * @param value the new value
   */
  public void setValue(String value) {
    this.value = value;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Property property = (Property) o;
    return Objects.equals(this.key, property.key) &&
        Objects.equals(this.section, property.section) &&
        Objects.equals(this.value, property.value);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(key, section, value);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Property {\n");
    
    sb.append("    key: ").append(toIndentedString(key)).append("\n");
    sb.append("    section: ").append(toIndentedString(section)).append("\n");
    sb.append("    value: ").append(toIndentedString(value)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

