/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.time.OffsetDateTime;

/**
 * The Class IndexPlayer.
 */
@ApiModel(description = "")
public class IndexPlayer {
  
  /** The created at. */
  @SerializedName("created_at")
  private OffsetDateTime createdAt = null;

  /** The id. */
  @SerializedName("id")
  private String id = null;

  /** The transcoder id. */
  @SerializedName("transcoder_id")
  private String transcoderId = null;

  /** The type. */
  @SerializedName("type")
  private String type = null;

  /** The updated at. */
  @SerializedName("updated_at")
  private OffsetDateTime updatedAt = null;

  /**
   * Created at.
   *
   * @param createdAt the created at
   * @return the index player
   */
  public IndexPlayer createdAt(OffsetDateTime createdAt) {
    this.createdAt = createdAt;
    return this;
  }

   /**
    * Gets the created at.
    *
    * @return the created at
    */
  @ApiModelProperty(example = "", value = "The date and time that the player was created.")
  public OffsetDateTime getCreatedAt() {
    return createdAt;
  }

  /**
   * Sets the created at.
   *
   * @param createdAt the new created at
   */
  public void setCreatedAt(OffsetDateTime createdAt) {
    this.createdAt = createdAt;
  }

  /**
   * Id.
   *
   * @param id the id
   * @return the index player
   */
  public IndexPlayer id(String id) {
    this.id = id;
    return this;
  }

   /**
    * Gets the id.
    *
    * @return the id
    */
  @ApiModelProperty(example = "", value = "The unique alphanumeric string that identifies the player.")
  public String getId() {
    return id;
  }

  /**
   * Sets the id.
   *
   * @param id the new id
   */
  public void setId(String id) {
    this.id = id;
  }

  /**
   * Transcoder id.
   *
   * @param transcoderId the transcoder id
   * @return the index player
   */
  public IndexPlayer transcoderId(String transcoderId) {
    this.transcoderId = transcoderId;
    return this;
  }

   /**
    * Gets the transcoder id.
    *
    * @return the transcoder id
    */
  @ApiModelProperty(example = "", value = "The unique alphanumeric string that identifies the transcoder.")
  public String getTranscoderId() {
    return transcoderId;
  }

  /**
   * Sets the transcoder id.
   *
   * @param transcoderId the new transcoder id
   */
  public void setTranscoderId(String transcoderId) {
    this.transcoderId = transcoderId;
  }

  /**
   * Type.
   *
   * @param type the type
   * @return the index player
   */
  public IndexPlayer type(String type) {
    this.type = type;
    return this;
  }

   /**
    * Gets the type.
    *
    * @return the type
    */
  @ApiModelProperty(example = "", value = "The player you want to use. Valid values are <strong>original_html5</strong>, which provides HTML5 playback and falls back to Flash on older browsers, and <strong>wowza_player</strong>, which provides HTML5 playback over Apple HLS. <strong>wowza_player</strong> requires that <em>target_delivery_protocol</em> be <strong>hls-https</strong> and <em>closed_caption_type</em> be <strong>none</strong>. The default is <strong>original_html5</strong>.")
  public String getType() {
    return type;
  }

  /**
   * Sets the type.
   *
   * @param type the new type
   */
  public void setType(String type) {
    this.type = type;
  }

  /**
   * Updated at.
   *
   * @param updatedAt the updated at
   * @return the index player
   */
  public IndexPlayer updatedAt(OffsetDateTime updatedAt) {
    this.updatedAt = updatedAt;
    return this;
  }

   /**
    * Gets the updated at.
    *
    * @return the updated at
    */
  @ApiModelProperty(example = "", value = "The date and time that the player was updated.")
  public OffsetDateTime getUpdatedAt() {
    return updatedAt;
  }

  /**
   * Sets the updated at.
   *
   * @param updatedAt the new updated at
   */
  public void setUpdatedAt(OffsetDateTime updatedAt) {
    this.updatedAt = updatedAt;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    IndexPlayer indexPlayer = (IndexPlayer) o;
    return Objects.equals(this.createdAt, indexPlayer.createdAt) &&
        Objects.equals(this.id, indexPlayer.id) &&
        Objects.equals(this.transcoderId, indexPlayer.transcoderId) &&
        Objects.equals(this.type, indexPlayer.type) &&
        Objects.equals(this.updatedAt, indexPlayer.updatedAt);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(createdAt, id, transcoderId, type, updatedAt);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class IndexPlayer {\n");
    
    sb.append("    createdAt: ").append(toIndentedString(createdAt)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    transcoderId: ").append(toIndentedString(transcoderId)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    updatedAt: ").append(toIndentedString(updatedAt)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

