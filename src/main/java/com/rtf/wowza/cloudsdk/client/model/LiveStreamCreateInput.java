/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.rtf.wowza.cloudsdk.client.model.LiveStream;
import java.io.IOException;

/**
 * The Class LiveStreamCreateInput.
 */
@ApiModel(description = "")
public class LiveStreamCreateInput {
  
  /** The live stream. */
  @SerializedName("live_stream")
  private LiveStream liveStream = null;

  /**
   * Live stream.
   *
   * @param liveStream the live stream
   * @return the live stream create input
   */
  public LiveStreamCreateInput liveStream(LiveStream liveStream) {
    this.liveStream = liveStream;
    return this;
  }

   /**
    * Gets the live stream.
    *
    * @return the live stream
    */
  @ApiModelProperty(required = true, value = "")
  public LiveStream getLiveStream() {
    return liveStream;
  }

  /**
   * Sets the live stream.
   *
   * @param liveStream the new live stream
   */
  public void setLiveStream(LiveStream liveStream) {
    this.liveStream = liveStream;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    LiveStreamCreateInput liveStreamCreateInput = (LiveStreamCreateInput) o;
    return Objects.equals(this.liveStream, liveStreamCreateInput.liveStream);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(liveStream);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class LiveStreamCreateInput {\n");
    
    sb.append("    liveStream: ").append(toIndentedString(liveStream)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

