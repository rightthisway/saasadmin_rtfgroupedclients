/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.time.OffsetDateTime;

/**
 * The Class StreamSourceAkamai.
 */
@ApiModel(description = "")
public class StreamSourceAkamai {
  
  /** The backup ip address. */
  @SerializedName("backup_ip_address")
  private String backupIpAddress = null;

  /** The backup url. */
  @SerializedName("backup_url")
  private String backupUrl = null;

  /** The created at. */
  @SerializedName("created_at")
  private OffsetDateTime createdAt = null;

  /** The id. */
  @SerializedName("id")
  private String id = null;

  /** The ip address. */
  @SerializedName("ip_address")
  private String ipAddress = null;

  /**
   * The Enum LocationEnum.
   */
  @JsonAdapter(LocationEnum.Adapter.class)
  public enum LocationEnum {
    
    /** The asia pacific australia. */
    ASIA_PACIFIC_AUSTRALIA("asia_pacific_australia"),
    
    /** The asia pacific japan. */
    ASIA_PACIFIC_JAPAN("asia_pacific_japan"),
    
    /** The asia pacific singapore. */
    ASIA_PACIFIC_SINGAPORE("asia_pacific_singapore"),
    
    /** The asia pacific taiwan. */
    ASIA_PACIFIC_TAIWAN("asia_pacific_taiwan"),
    
    /** The eu belgium. */
    EU_BELGIUM("eu_belgium"),
    
    /** The eu germany. */
    EU_GERMANY("eu_germany"),
    
    /** The eu ireland. */
    EU_IRELAND("eu_ireland"),
    
    /** The south america brazil. */
    SOUTH_AMERICA_BRAZIL("south_america_brazil"),
    
    /** The us central iowa. */
    US_CENTRAL_IOWA("us_central_iowa"),
    
    /** The us east virginia. */
    US_EAST_VIRGINIA("us_east_virginia"),
    
    /** The us west california. */
    US_WEST_CALIFORNIA("us_west_california"),
    
    /** The us west oregon. */
    US_WEST_OREGON("us_west_oregon");

    /** The value. */
    private String value;

    /**
     * Instantiates a new location enum.
     *
     * @param value the value
     */
    LocationEnum(String value) {
      this.value = value;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
      return value;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString() {
      return String.valueOf(value);
    }

    /**
     * From value.
     *
     * @param text the text
     * @return the location enum
     */
    public static LocationEnum fromValue(String text) {
      for (LocationEnum b : LocationEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    /**
     * The Class Adapter.
     */
    public static class Adapter extends TypeAdapter<LocationEnum> {
      
      /**
       * Write.
       *
       * @param jsonWriter the json writer
       * @param enumeration the enumeration
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public void write(final JsonWriter jsonWriter, final LocationEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      /**
       * Read.
       *
       * @param jsonReader the json reader
       * @return the location enum
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public LocationEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return LocationEnum.fromValue(String.valueOf(value));
      }
    }
  }

  /** The location. */
  @SerializedName("location")
  private LocationEnum location = null;

  /**
   * The Enum LocationMethodEnum.
   */
  @JsonAdapter(LocationMethodEnum.Adapter.class)
  public enum LocationMethodEnum {
    
    /** The region. */
    REGION("region"),
    
    /** The ip address. */
    IP_ADDRESS("ip_address");

    /** The value. */
    private String value;

    /**
     * Instantiates a new location method enum.
     *
     * @param value the value
     */
    LocationMethodEnum(String value) {
      this.value = value;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
      return value;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString() {
      return String.valueOf(value);
    }

    /**
     * From value.
     *
     * @param text the text
     * @return the location method enum
     */
    public static LocationMethodEnum fromValue(String text) {
      for (LocationMethodEnum b : LocationMethodEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    /**
     * The Class Adapter.
     */
    public static class Adapter extends TypeAdapter<LocationMethodEnum> {
      
      /**
       * Write.
       *
       * @param jsonWriter the json writer
       * @param enumeration the enumeration
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public void write(final JsonWriter jsonWriter, final LocationMethodEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      /**
       * Read.
       *
       * @param jsonReader the json reader
       * @return the location method enum
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public LocationMethodEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return LocationMethodEnum.fromValue(String.valueOf(value));
      }
    }
  }

  /** The location method. */
  @SerializedName("location_method")
  private LocationMethodEnum locationMethod = null;

  /** The name. */
  @SerializedName("name")
  private String name = null;

  /** The password. */
  @SerializedName("password")
  private String password = null;

  /** The playback url. */
  @SerializedName("playback_url")
  private String playbackUrl = null;

  /** The primary url. */
  @SerializedName("primary_url")
  private String primaryUrl = null;

  /** The provider. */
  @SerializedName("provider")
  private String provider = null;

  /** The stream name. */
  @SerializedName("stream_name")
  private String streamName = null;

  /** The updated at. */
  @SerializedName("updated_at")
  private OffsetDateTime updatedAt = null;

  /** The username. */
  @SerializedName("username")
  private String username = null;

  /**
   * Backup ip address.
   *
   * @param backupIpAddress the backup ip address
   * @return the stream source akamai
   */
  public StreamSourceAkamai backupIpAddress(String backupIpAddress) {
    this.backupIpAddress = backupIpAddress;
    return this;
  }

   /**
    * Gets the backup ip address.
    *
    * @return the backup ip address
    */
  @ApiModelProperty(example = "", value = "If <em>location_method</em> is <strong>ip_address</strong>, the backup IP address of the source encoder.")
  public String getBackupIpAddress() {
    return backupIpAddress;
  }

  /**
   * Sets the backup ip address.
   *
   * @param backupIpAddress the new backup ip address
   */
  public void setBackupIpAddress(String backupIpAddress) {
    this.backupIpAddress = backupIpAddress;
  }

  /**
   * Backup url.
   *
   * @param backupUrl the backup url
   * @return the stream source akamai
   */
  public StreamSourceAkamai backupUrl(String backupUrl) {
    this.backupUrl = backupUrl;
    return this;
  }

   /**
    * Gets the backup url.
    *
    * @return the backup url
    */
  @ApiModelProperty(example = "", value = "The backup RTMP playback URL of the transcoded stream.")
  public String getBackupUrl() {
    return backupUrl;
  }

  /**
   * Sets the backup url.
   *
   * @param backupUrl the new backup url
   */
  public void setBackupUrl(String backupUrl) {
    this.backupUrl = backupUrl;
  }

  /**
   * Created at.
   *
   * @param createdAt the created at
   * @return the stream source akamai
   */
  public StreamSourceAkamai createdAt(OffsetDateTime createdAt) {
    this.createdAt = createdAt;
    return this;
  }

   /**
    * Gets the created at.
    *
    * @return the created at
    */
  @ApiModelProperty(example = "", value = "The date and time that the Akamai stream source was created.")
  public OffsetDateTime getCreatedAt() {
    return createdAt;
  }

  /**
   * Sets the created at.
   *
   * @param createdAt the new created at
   */
  public void setCreatedAt(OffsetDateTime createdAt) {
    this.createdAt = createdAt;
  }

  /**
   * Id.
   *
   * @param id the id
   * @return the stream source akamai
   */
  public StreamSourceAkamai id(String id) {
    this.id = id;
    return this;
  }

   /**
    * Gets the id.
    *
    * @return the id
    */
  @ApiModelProperty(example = "", value = "The unique alphanumeric string that identifies the Akamai stream source.")
  public String getId() {
    return id;
  }

  /**
   * Sets the id.
   *
   * @param id the new id
   */
  public void setId(String id) {
    this.id = id;
  }

  /**
   * Ip address.
   *
   * @param ipAddress the ip address
   * @return the stream source akamai
   */
  public StreamSourceAkamai ipAddress(String ipAddress) {
    this.ipAddress = ipAddress;
    return this;
  }

   /**
    * Gets the ip address.
    *
    * @return the ip address
    */
  @ApiModelProperty(example = "", value = "If <em>location_method</em> is <strong>ip_address</strong>, the primary IP address of the source encoder.")
  public String getIpAddress() {
    return ipAddress;
  }

  /**
   * Sets the ip address.
   *
   * @param ipAddress the new ip address
   */
  public void setIpAddress(String ipAddress) {
    this.ipAddress = ipAddress;
  }

  /**
   * Location.
   *
   * @param location the location
   * @return the stream source akamai
   */
  public StreamSourceAkamai location(LocationEnum location) {
    this.location = location;
    return this;
  }

   /**
    * Gets the location.
    *
    * @return the location
    */
  @ApiModelProperty(example = "", value = "If <em>location_method</em> is <strong>region</strong>, specify a location as close as possible to the source encoder.")
  public LocationEnum getLocation() {
    return location;
  }

  /**
   * Sets the location.
   *
   * @param location the new location
   */
  public void setLocation(LocationEnum location) {
    this.location = location;
  }

  /**
   * Location method.
   *
   * @param locationMethod the location method
   * @return the stream source akamai
   */
  public StreamSourceAkamai locationMethod(LocationMethodEnum locationMethod) {
    this.locationMethod = locationMethod;
    return this;
  }

   /**
    * Gets the location method.
    *
    * @return the location method
    */
  @ApiModelProperty(example = "", value = "The method used to determine the location of the Akamai stream source, either by <strong>region</strong> or based on the source encoder's <strong>ip_address</strong>.")
  public LocationMethodEnum getLocationMethod() {
    return locationMethod;
  }

  /**
   * Sets the location method.
   *
   * @param locationMethod the new location method
   */
  public void setLocationMethod(LocationMethodEnum locationMethod) {
    this.locationMethod = locationMethod;
  }

  /**
   * Name.
   *
   * @param name the name
   * @return the stream source akamai
   */
  public StreamSourceAkamai name(String name) {
    this.name = name;
    return this;
  }

   /**
    * Gets the name.
    *
    * @return the name
    */
  @ApiModelProperty(example = "", value = "A descriptive name for the Akamai stream source. Maximum 255 characters.")
  public String getName() {
    return name;
  }

  /**
   * Sets the name.
   *
   * @param name the new name
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Password.
   *
   * @param password the password
   * @return the stream source akamai
   */
  public StreamSourceAkamai password(String password) {
    this.password = password;
    return this;
  }

   /**
    * Gets the password.
    *
    * @return the password
    */
  @ApiModelProperty(example = "", value = "The password that you can use to configure the source encoder to authenticate to the Akamai stream source.")
  public String getPassword() {
    return password;
  }

  /**
   * Sets the password.
   *
   * @param password the new password
   */
  public void setPassword(String password) {
    this.password = password;
  }

  /**
   * Playback url.
   *
   * @param playbackUrl the playback url
   * @return the stream source akamai
   */
  public StreamSourceAkamai playbackUrl(String playbackUrl) {
    this.playbackUrl = playbackUrl;
    return this;
  }

   /**
    * Gets the playback url.
    *
    * @return the playback url
    */
  @ApiModelProperty(example = "", value = "The full RTMP playback URL.")
  public String getPlaybackUrl() {
    return playbackUrl;
  }

  /**
   * Sets the playback url.
   *
   * @param playbackUrl the new playback url
   */
  public void setPlaybackUrl(String playbackUrl) {
    this.playbackUrl = playbackUrl;
  }

  /**
   * Primary url.
   *
   * @param primaryUrl the primary url
   * @return the stream source akamai
   */
  public StreamSourceAkamai primaryUrl(String primaryUrl) {
    this.primaryUrl = primaryUrl;
    return this;
  }

   /**
    * Gets the primary url.
    *
    * @return the primary url
    */
  @ApiModelProperty(example = "", value = "The primary RTMP playback URL of the transcoded stream.")
  public String getPrimaryUrl() {
    return primaryUrl;
  }

  /**
   * Sets the primary url.
   *
   * @param primaryUrl the new primary url
   */
  public void setPrimaryUrl(String primaryUrl) {
    this.primaryUrl = primaryUrl;
  }

  /**
   * Provider.
   *
   * @param provider the provider
   * @return the stream source akamai
   */
  public StreamSourceAkamai provider(String provider) {
    this.provider = provider;
    return this;
  }

   /**
    * Gets the provider.
    *
    * @return the provider
    */
  @ApiModelProperty(example = "", value = "The provider of the Akamai stream source.")
  public String getProvider() {
    return provider;
  }

  /**
   * Sets the provider.
   *
   * @param provider the new provider
   */
  public void setProvider(String provider) {
    this.provider = provider;
  }

  /**
   * Stream name.
   *
   * @param streamName the stream name
   * @return the stream source akamai
   */
  public StreamSourceAkamai streamName(String streamName) {
    this.streamName = streamName;
    return this;
  }

   /**
    * Gets the stream name.
    *
    * @return the stream name
    */
  @ApiModelProperty(example = "", value = "The name of the stream that you can use to configure the source encoder to connect to the Akamai stream source.")
  public String getStreamName() {
    return streamName;
  }

  /**
   * Sets the stream name.
   *
   * @param streamName the new stream name
   */
  public void setStreamName(String streamName) {
    this.streamName = streamName;
  }

  /**
   * Updated at.
   *
   * @param updatedAt the updated at
   * @return the stream source akamai
   */
  public StreamSourceAkamai updatedAt(OffsetDateTime updatedAt) {
    this.updatedAt = updatedAt;
    return this;
  }

   /**
    * Gets the updated at.
    *
    * @return the updated at
    */
  @ApiModelProperty(example = "", value = "The date and time that the Akamai stream source was updated.")
  public OffsetDateTime getUpdatedAt() {
    return updatedAt;
  }

  /**
   * Sets the updated at.
   *
   * @param updatedAt the new updated at
   */
  public void setUpdatedAt(OffsetDateTime updatedAt) {
    this.updatedAt = updatedAt;
  }

  /**
   * Username.
   *
   * @param username the username
   * @return the stream source akamai
   */
  public StreamSourceAkamai username(String username) {
    this.username = username;
    return this;
  }

   /**
    * Gets the username.
    *
    * @return the username
    */
  @ApiModelProperty(example = "", value = "The username that you can use to configure the source encoder to authenticate to the Akamai stream source.")
  public String getUsername() {
    return username;
  }

  /**
   * Sets the username.
   *
   * @param username the new username
   */
  public void setUsername(String username) {
    this.username = username;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    StreamSourceAkamai streamSourceAkamai = (StreamSourceAkamai) o;
    return Objects.equals(this.backupIpAddress, streamSourceAkamai.backupIpAddress) &&
        Objects.equals(this.backupUrl, streamSourceAkamai.backupUrl) &&
        Objects.equals(this.createdAt, streamSourceAkamai.createdAt) &&
        Objects.equals(this.id, streamSourceAkamai.id) &&
        Objects.equals(this.ipAddress, streamSourceAkamai.ipAddress) &&
        Objects.equals(this.location, streamSourceAkamai.location) &&
        Objects.equals(this.locationMethod, streamSourceAkamai.locationMethod) &&
        Objects.equals(this.name, streamSourceAkamai.name) &&
        Objects.equals(this.password, streamSourceAkamai.password) &&
        Objects.equals(this.playbackUrl, streamSourceAkamai.playbackUrl) &&
        Objects.equals(this.primaryUrl, streamSourceAkamai.primaryUrl) &&
        Objects.equals(this.provider, streamSourceAkamai.provider) &&
        Objects.equals(this.streamName, streamSourceAkamai.streamName) &&
        Objects.equals(this.updatedAt, streamSourceAkamai.updatedAt) &&
        Objects.equals(this.username, streamSourceAkamai.username);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(backupIpAddress, backupUrl, createdAt, id, ipAddress, location, locationMethod, name, password, playbackUrl, primaryUrl, provider, streamName, updatedAt, username);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class StreamSourceAkamai {\n");
    
    sb.append("    backupIpAddress: ").append(toIndentedString(backupIpAddress)).append("\n");
    sb.append("    backupUrl: ").append(toIndentedString(backupUrl)).append("\n");
    sb.append("    createdAt: ").append(toIndentedString(createdAt)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    ipAddress: ").append(toIndentedString(ipAddress)).append("\n");
    sb.append("    location: ").append(toIndentedString(location)).append("\n");
    sb.append("    locationMethod: ").append(toIndentedString(locationMethod)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    password: ").append(toIndentedString(password)).append("\n");
    sb.append("    playbackUrl: ").append(toIndentedString(playbackUrl)).append("\n");
    sb.append("    primaryUrl: ").append(toIndentedString(primaryUrl)).append("\n");
    sb.append("    provider: ").append(toIndentedString(provider)).append("\n");
    sb.append("    streamName: ").append(toIndentedString(streamName)).append("\n");
    sb.append("    updatedAt: ").append(toIndentedString(updatedAt)).append("\n");
    sb.append("    username: ").append(toIndentedString(username)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

