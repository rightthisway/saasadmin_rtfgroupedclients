/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.rtf.wowza.cloudsdk.client.model.Uptime;
import java.io.IOException;

/**
 * The Class TranscoderCreateUptime.
 */
public class TranscoderCreateUptime {
  
  /** The uptime. */
  @SerializedName("uptime")
  private Uptime uptime = null;

  /**
   * Uptime.
   *
   * @param uptime the uptime
   * @return the transcoder create uptime
   */
  public TranscoderCreateUptime uptime(Uptime uptime) {
    this.uptime = uptime;
    return this;
  }

   /**
    * Gets the transcoder uptime.
    *
    * @return the transcoder uptime
    */
  @ApiModelProperty(required = true, value = "")
  public Uptime getTranscoderUptime() {
    return uptime;
  }

  /**
   * Sets the transcoder uptime.
   *
   * @param uptime the new transcoder uptime
   */
  public void setTranscoderUptime(Uptime uptime) {
    this.uptime = uptime;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TranscoderCreateUptime inlineResponse20042 = (TranscoderCreateUptime) o;
    return Objects.equals(this.uptime, inlineResponse20042.uptime);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(uptime);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TranscoderCreateUptime {\n");
    
    sb.append("    uptime: ").append(toIndentedString(uptime)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

