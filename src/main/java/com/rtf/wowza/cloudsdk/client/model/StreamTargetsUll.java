/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.rtf.wowza.cloudsdk.client.model.IndexUllStreamTarget;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class StreamTargetsUll.
 */
@ApiModel(description = "")
public class StreamTargetsUll {
  
  /** The stream targets ull. */
  @SerializedName("stream_targets_ull")
  private List<IndexUllStreamTarget> streamTargetsUll = new ArrayList<IndexUllStreamTarget>();

  /**
   * Stream targets ull.
   *
   * @param streamTargetsUll the stream targets ull
   * @return the stream targets ull
   */
  public StreamTargetsUll streamTargetsUll(List<IndexUllStreamTarget> streamTargetsUll) {
    this.streamTargetsUll = streamTargetsUll;
    return this;
  }

  /**
   * Adds the stream targets ull item.
   *
   * @param streamTargetsUllItem the stream targets ull item
   * @return the stream targets ull
   */
  public StreamTargetsUll addStreamTargetsUllItem(IndexUllStreamTarget streamTargetsUllItem) {
    this.streamTargetsUll.add(streamTargetsUllItem);
    return this;
  }

   /**
    * Gets the stream targets ull.
    *
    * @return the stream targets ull
    */
  @ApiModelProperty(required = true, value = "")
  public List<IndexUllStreamTarget> getStreamTargetsUll() {
    return streamTargetsUll;
  }

  /**
   * Sets the stream targets ull.
   *
   * @param streamTargetsUll the new stream targets ull
   */
  public void setStreamTargetsUll(List<IndexUllStreamTarget> streamTargetsUll) {
    this.streamTargetsUll = streamTargetsUll;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    StreamTargetsUll streamTargetsUll = (StreamTargetsUll) o;
    return Objects.equals(this.streamTargetsUll, streamTargetsUll.streamTargetsUll);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(streamTargetsUll);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class StreamTargetsUll {\n");
    
    sb.append("    streamTargetsUll: ").append(toIndentedString(streamTargetsUll)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

