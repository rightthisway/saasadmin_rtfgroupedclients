/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client;

import java.util.List;
import java.util.Map;

/**
 * The Class ApiResponse.
 *
 * @param <T> the generic type
 */
public class ApiResponse<T> {
    
    /** The status code. */
    final private int statusCode;
    
    /** The headers. */
    final private Map<String, List<String>> headers;
    
    /** The data. */
    final private T data;

    /**
     * Instantiates a new api response.
     *
     * @param statusCode the status code
     * @param headers the headers
     */
    public ApiResponse(int statusCode, Map<String, List<String>> headers) {
        this(statusCode, headers, null);
    }

    /**
     * Instantiates a new api response.
     *
     * @param statusCode the status code
     * @param headers the headers
     * @param data the data
     */
    public ApiResponse(int statusCode, Map<String, List<String>> headers, T data) {
        this.statusCode = statusCode;
        this.headers = headers;
        this.data = data;
    }

    /**
     * Gets the status code.
     *
     * @return the status code
     */
    public int getStatusCode() {
        return statusCode;
    }

    /**
     * Gets the headers.
     *
     * @return the headers
     */
    public Map<String, List<String>> getHeaders() {
        return headers;
    }

    /**
     * Gets the data.
     *
     * @return the data
     */
    public T getData() {
        return data;
    }
}
