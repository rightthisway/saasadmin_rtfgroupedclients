/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.rtf.wowza.cloudsdk.client.model.HashOfZones;
import java.io.IOException;

/**
 * The Class HashOfProtocols.
 */
@ApiModel(description = "A hash of protocols that accrued network usage, keyed by the name of the protocol that generated the usage.")
public class HashOfProtocols {
  
  /** The zones. */
  @SerializedName("zones")
  private HashOfZones zones = null;

  /**
   * Zones.
   *
   * @param zones the zones
   * @return the hash of protocols
   */
  public HashOfProtocols zones(HashOfZones zones) {
    this.zones = zones;
    return this;
  }

   /**
    * Gets the zones.
    *
    * @return the zones
    */
  @ApiModelProperty(value = "")
  public HashOfZones getZones() {
    return zones;
  }

  /**
   * Sets the zones.
   *
   * @param zones the new zones
   */
  public void setZones(HashOfZones zones) {
    this.zones = zones;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    HashOfProtocols hashOfProtocols = (HashOfProtocols) o;
    return Objects.equals(this.zones, hashOfProtocols.zones);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(zones);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class HashOfProtocols {\n");
    
    sb.append("    zones: ").append(toIndentedString(zones)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

