/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.rtf.wowza.cloudsdk.client.model.LiveStreamConnectioncode;
import java.io.IOException;

/**
 * The Class LiveStreamCreateConnectioncode.
 */
public class LiveStreamCreateConnectioncode {
  
  /** The live stream connectioncode. */
  @SerializedName("live_stream")
  private LiveStreamConnectioncode liveStreamConnectioncode = null;

  /**
   * Live stream connectioncode.
   *
   * @param liveStreamConnectioncode the live stream connectioncode
   * @return the live stream create connectioncode
   */
  public LiveStreamCreateConnectioncode liveStreamConnectioncode(LiveStreamConnectioncode liveStreamConnectioncode) {
    this.liveStreamConnectioncode = liveStreamConnectioncode;
    return this;
  }

   /**
    * Gets the live stream connectioncode.
    *
    * @return the live stream connectioncode
    */
  @ApiModelProperty(required = true, value = "")
  public LiveStreamConnectioncode getLiveStreamConnectioncode() {
    return liveStreamConnectioncode;
  }

  /**
   * Sets the live stream connectioncode.
   *
   * @param liveStreamConnectioncode the new live stream connectioncode
   */
  public void setLiveStreamConnectioncode(LiveStreamConnectioncode liveStreamConnectioncode) {
    this.liveStreamConnectioncode = liveStreamConnectioncode;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    LiveStreamCreateConnectioncode inlineResponse2004 = (LiveStreamCreateConnectioncode) o;
    return Objects.equals(this.liveStreamConnectioncode, inlineResponse2004.liveStreamConnectioncode);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(liveStreamConnectioncode);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class LiveStreamCreateConnectioncode {\n");
    
    sb.append("    liveStreamConnectioncode: ").append(toIndentedString(liveStreamConnectioncode)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

