/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * The Class TranscoderState.
 */
public class TranscoderState {
  
  /** The ip address. */
  @SerializedName("ip_address")
  private String ipAddress = null;

  /**
   * The Enum StateEnum.
   */
  @JsonAdapter(StateEnum.Adapter.class)
  public enum StateEnum {
    
    /** The starting. */
    STARTING("starting"),
    
    /** The stopping. */
    STOPPING("stopping"),
    
    /** The started. */
    STARTED("started"),
    
    /** The stopped. */
    STOPPED("stopped"),
    
    /** The resetting. */
    RESETTING("resetting");

    /** The value. */
    private String value;

    /**
     * Instantiates a new state enum.
     *
     * @param value the value
     */
    StateEnum(String value) {
      this.value = value;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
      return value;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString() {
      return String.valueOf(value);
    }

    /**
     * From value.
     *
     * @param text the text
     * @return the state enum
     */
    public static StateEnum fromValue(String text) {
      for (StateEnum b : StateEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    /**
     * The Class Adapter.
     */
    public static class Adapter extends TypeAdapter<StateEnum> {
      
      /**
       * Write.
       *
       * @param jsonWriter the json writer
       * @param enumeration the enumeration
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public void write(final JsonWriter jsonWriter, final StateEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      /**
       * Read.
       *
       * @param jsonReader the json reader
       * @return the state enum
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public StateEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return StateEnum.fromValue(String.valueOf(value));
      }
    }
  }

  /** The state. */
  @SerializedName("state")
  private StateEnum state = null;

  /** The uptime id. */
  @SerializedName("uptime_id")
  private String uptimeId = null;

  /**
   * Ip address.
   *
   * @param ipAddress the ip address
   * @return the transcoder state
   */
  public TranscoderState ipAddress(String ipAddress) {
    this.ipAddress = ipAddress;
    return this;
  }

   /**
    * Gets the ip address.
    *
    * @return the ip address
    */
  @ApiModelProperty(example = "1.2.3.4", value = "Available from version 1.1.<br /><br />The IP address of the transcoder instance. If the transcoder <em>state</em> is anything other than <strong>started</strong>, the <em>ip_address</em> is <strong>0.0.0.0</strong>.")
  public String getIpAddress() {
    return ipAddress;
  }

  /**
   * Sets the ip address.
   *
   * @param ipAddress the new ip address
   */
  public void setIpAddress(String ipAddress) {
    this.ipAddress = ipAddress;
  }

  /**
   * State.
   *
   * @param state the state
   * @return the transcoder state
   */
  public TranscoderState state(StateEnum state) {
    this.state = state;
    return this;
  }

   /**
    * Gets the state.
    *
    * @return the state
    */
  @ApiModelProperty(example = "started", value = "The state of the transcoder.")
  public StateEnum getState() {
    return state;
  }

  /**
   * Sets the state.
   *
   * @param state the new state
   */
  public void setState(StateEnum state) {
    this.state = state;
  }

  /**
   * Uptime id.
   *
   * @param uptimeId the uptime id
   * @return the transcoder state
   */
  public TranscoderState uptimeId(String uptimeId) {
    this.uptimeId = uptimeId;
    return this;
  }

   /**
    * Gets the uptime id.
    *
    * @return the uptime id
    */
  @ApiModelProperty(example = "abcd1234", value = "The unique identifier associated with a specific uptime period of a transcoder.")
  public String getUptimeId() {
    return uptimeId;
  }

  /**
   * Sets the uptime id.
   *
   * @param uptimeId the new uptime id
   */
  public void setUptimeId(String uptimeId) {
    this.uptimeId = uptimeId;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TranscoderState transcoder4 = (TranscoderState) o;
    return Objects.equals(this.ipAddress, transcoder4.ipAddress) &&
        Objects.equals(this.state, transcoder4.state) &&
        Objects.equals(this.uptimeId, transcoder4.uptimeId);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(ipAddress, state, uptimeId);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TranscoderState {\n");
    
    sb.append("    ipAddress: ").append(toIndentedString(ipAddress)).append("\n");
    sb.append("    state: ").append(toIndentedString(state)).append("\n");
    sb.append("    uptimeId: ").append(toIndentedString(uptimeId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

