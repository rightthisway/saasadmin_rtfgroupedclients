/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.rtf.wowza.cloudsdk.client.model.Output;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class Outputs.
 */
@ApiModel(description = "")
public class Outputs {
  
  /** The outputs. */
  @SerializedName("outputs")
  private List<Output> outputs = new ArrayList<Output>();

  /**
   * Outputs.
   *
   * @param outputs the outputs
   * @return the outputs
   */
  public Outputs outputs(List<Output> outputs) {
    this.outputs = outputs;
    return this;
  }

  /**
   * Adds the outputs item.
   *
   * @param outputsItem the outputs item
   * @return the outputs
   */
  public Outputs addOutputsItem(Output outputsItem) {
    this.outputs.add(outputsItem);
    return this;
  }

   /**
    * Gets the outputs.
    *
    * @return the outputs
    */
  @ApiModelProperty(required = true, value = "")
  public List<Output> getOutputs() {
    return outputs;
  }

  /**
   * Sets the outputs.
   *
   * @param outputs the new outputs
   */
  public void setOutputs(List<Output> outputs) {
    this.outputs = outputs;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Outputs outputs = (Outputs) o;
    return Objects.equals(this.outputs, outputs.outputs);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(outputs);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Outputs {\n");
    
    sb.append("    outputs: ").append(toIndentedString(outputs)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

