/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.rtf.wowza.cloudsdk.client.model.OutputStreamTarget;
import java.io.IOException;

/**
 * The Class OutputStreamTargetCreateInput.
 */
@ApiModel(description = "")
public class OutputStreamTargetCreateInput {
  
  /** The output stream target. */
  @SerializedName("output_stream_target")
  private OutputStreamTarget outputStreamTarget = null;

  /**
   * Output stream target.
   *
   * @param outputStreamTarget the output stream target
   * @return the output stream target create input
   */
  public OutputStreamTargetCreateInput outputStreamTarget(OutputStreamTarget outputStreamTarget) {
    this.outputStreamTarget = outputStreamTarget;
    return this;
  }

   /**
    * Gets the output stream target.
    *
    * @return the output stream target
    */
  @ApiModelProperty(required = true, value = "")
  public OutputStreamTarget getOutputStreamTarget() {
    return outputStreamTarget;
  }

  /**
   * Sets the output stream target.
   *
   * @param outputStreamTarget the new output stream target
   */
  public void setOutputStreamTarget(OutputStreamTarget outputStreamTarget) {
    this.outputStreamTarget = outputStreamTarget;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OutputStreamTargetCreateInput outputStreamTargetCreateInput = (OutputStreamTargetCreateInput) o;
    return Objects.equals(this.outputStreamTarget, outputStreamTargetCreateInput.outputStreamTarget);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(outputStreamTarget);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class OutputStreamTargetCreateInput {\n");
    
    sb.append("    outputStreamTarget: ").append(toIndentedString(outputStreamTarget)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

