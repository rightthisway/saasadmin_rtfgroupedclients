/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.rtf.wowza.cloudsdk.client.model.IndexCustomStreamTarget;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class StreamTargetsCustom.
 */
@ApiModel(description = "")
public class StreamTargetsCustom {
  
  /** The stream targets custom. */
  @SerializedName("stream_targets_custom")
  private List<IndexCustomStreamTarget> streamTargetsCustom = new ArrayList<IndexCustomStreamTarget>();

  /**
   * Stream targets custom.
   *
   * @param streamTargetsCustom the stream targets custom
   * @return the stream targets custom
   */
  public StreamTargetsCustom streamTargetsCustom(List<IndexCustomStreamTarget> streamTargetsCustom) {
    this.streamTargetsCustom = streamTargetsCustom;
    return this;
  }

  /**
   * Adds the stream targets custom item.
   *
   * @param streamTargetsCustomItem the stream targets custom item
   * @return the stream targets custom
   */
  public StreamTargetsCustom addStreamTargetsCustomItem(IndexCustomStreamTarget streamTargetsCustomItem) {
    this.streamTargetsCustom.add(streamTargetsCustomItem);
    return this;
  }

   /**
    * Gets the stream targets custom.
    *
    * @return the stream targets custom
    */
  @ApiModelProperty(required = true, value = "")
  public List<IndexCustomStreamTarget> getStreamTargetsCustom() {
    return streamTargetsCustom;
  }

  /**
   * Sets the stream targets custom.
   *
   * @param streamTargetsCustom the new stream targets custom
   */
  public void setStreamTargetsCustom(List<IndexCustomStreamTarget> streamTargetsCustom) {
    this.streamTargetsCustom = streamTargetsCustom;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    StreamTargetsCustom streamTargetsCustom = (StreamTargetsCustom) o;
    return Objects.equals(this.streamTargetsCustom, streamTargetsCustom.streamTargetsCustom);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(streamTargetsCustom);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class StreamTargetsCustom {\n");
    
    sb.append("    streamTargetsCustom: ").append(toIndentedString(streamTargetsCustom)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

