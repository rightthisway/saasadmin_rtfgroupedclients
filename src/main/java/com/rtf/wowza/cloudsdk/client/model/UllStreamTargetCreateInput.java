/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.rtf.wowza.cloudsdk.client.model.StreamTargetUll;
import java.io.IOException;

/**
 * The Class UllStreamTargetCreateInput.
 */
@ApiModel(description = "")
public class UllStreamTargetCreateInput {
  
  /** The stream target ull. */
  @SerializedName("stream_target_ull")
  private StreamTargetUll streamTargetUll = null;

  /**
   * Stream target ull.
   *
   * @param streamTargetUll the stream target ull
   * @return the ull stream target create input
   */
  public UllStreamTargetCreateInput streamTargetUll(StreamTargetUll streamTargetUll) {
    this.streamTargetUll = streamTargetUll;
    return this;
  }

   /**
    * Gets the stream target ull.
    *
    * @return the stream target ull
    */
  @ApiModelProperty(required = true, value = "")
  public StreamTargetUll getStreamTargetUll() {
    return streamTargetUll;
  }

  /**
   * Sets the stream target ull.
   *
   * @param streamTargetUll the new stream target ull
   */
  public void setStreamTargetUll(StreamTargetUll streamTargetUll) {
    this.streamTargetUll = streamTargetUll;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UllStreamTargetCreateInput ullStreamTargetCreateInput = (UllStreamTargetCreateInput) o;
    return Objects.equals(this.streamTargetUll, ullStreamTargetCreateInput.streamTargetUll);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(streamTargetUll);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class UllStreamTargetCreateInput {\n");
    
    sb.append("    streamTargetUll: ").append(toIndentedString(streamTargetUll)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

