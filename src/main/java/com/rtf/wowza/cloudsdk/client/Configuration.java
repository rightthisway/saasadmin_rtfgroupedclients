/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client;

/**
 * The Class Configuration.
 */
public class Configuration {
    
    /** The default api client. */
    private static ApiClient defaultApiClient = new ApiClient();

    /**
     * Gets the default api client.
     *
     * @return the default api client
     */
    public static ApiClient getDefaultApiClient() {
        return defaultApiClient;
    }

    /**
     * Sets the default api client.
     *
     * @param apiClient the new default api client
     */
    public static void setDefaultApiClient(ApiClient apiClient) {
        defaultApiClient = apiClient;
    }
}
