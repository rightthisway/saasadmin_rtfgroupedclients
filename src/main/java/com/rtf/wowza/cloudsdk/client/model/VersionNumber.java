/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * The Class VersionNumber.
 */
@ApiModel(description = "")
public class VersionNumber {
  
  /**
   * The Enum StatusEnum.
   */
  @JsonAdapter(StatusEnum.Adapter.class)
  public enum StatusEnum {
    
    /** The current. */
    CURRENT("current"),
    
    /** The supported. */
    SUPPORTED("supported"),
    
    /** The deprecated. */
    DEPRECATED("deprecated");

    /** The value. */
    private String value;

    /**
     * Instantiates a new status enum.
     *
     * @param value the value
     */
    StatusEnum(String value) {
      this.value = value;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
      return value;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString() {
      return String.valueOf(value);
    }

    /**
     * From value.
     *
     * @param text the text
     * @return the status enum
     */
    public static StatusEnum fromValue(String text) {
      for (StatusEnum b : StatusEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    /**
     * The Class Adapter.
     */
    public static class Adapter extends TypeAdapter<StatusEnum> {
      
      /**
       * Write.
       *
       * @param jsonWriter the json writer
       * @param enumeration the enumeration
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public void write(final JsonWriter jsonWriter, final StatusEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      /**
       * Read.
       *
       * @param jsonReader the json reader
       * @return the status enum
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public StatusEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return StatusEnum.fromValue(String.valueOf(value));
      }
    }
  }

  /** The status. */
  @SerializedName("status")
  private StatusEnum status = null;

  /** The base uri. */
  @SerializedName("base_uri")
  private String baseUri = null;

  /**
   * Status.
   *
   * @param status the status
   * @return the version number
   */
  public VersionNumber status(StatusEnum status) {
    this.status = status;
    return this;
  }

   /**
    * Gets the status.
    *
    * @return the status
    */
  @ApiModelProperty(example = "current", value = "The status of the version.")
  public StatusEnum getStatus() {
    return status;
  }

  /**
   * Sets the status.
   *
   * @param status the new status
   */
  public void setStatus(StatusEnum status) {
    this.status = status;
  }

  /**
   * Base uri.
   *
   * @param baseUri the base uri
   * @return the version number
   */
  public VersionNumber baseUri(String baseUri) {
    this.baseUri = baseUri;
    return this;
  }

   /**
    * Gets the base uri.
    *
    * @return the base uri
    */
  @ApiModelProperty(example = "/api/v1.2", value = "The base URI for the version.")
  public String getBaseUri() {
    return baseUri;
  }

  /**
   * Sets the base uri.
   *
   * @param baseUri the new base uri
   */
  public void setBaseUri(String baseUri) {
    this.baseUri = baseUri;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    VersionNumber versionNumber = (VersionNumber) o;
    return Objects.equals(this.status, versionNumber.status) &&
        Objects.equals(this.baseUri, versionNumber.baseUri);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(status, baseUri);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class VersionNumber {\n");
    
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    baseUri: ").append(toIndentedString(baseUri)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

