/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.rtf.wowza.cloudsdk.client.model.Url;
import java.io.IOException;

/**
 * The Class UrlInput.
 */
@ApiModel(description = "")
public class UrlInput {
  
  /** The url. */
  @SerializedName("url")
  private Url url = null;

  /**
   * Url.
   *
   * @param url the url
   * @return the url input
   */
  public UrlInput url(Url url) {
    this.url = url;
    return this;
  }

   /**
    * Gets the url.
    *
    * @return the url
    */
  @ApiModelProperty(required = true, value = "")
  public Url getUrl() {
    return url;
  }

  /**
   * Sets the url.
   *
   * @param url the new url
   */
  public void setUrl(Url url) {
    this.url = url;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UrlInput urlInput = (UrlInput) o;
    return Objects.equals(this.url, urlInput.url);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(url);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class UrlInput {\n");
    
    sb.append("    url: ").append(toIndentedString(url)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

