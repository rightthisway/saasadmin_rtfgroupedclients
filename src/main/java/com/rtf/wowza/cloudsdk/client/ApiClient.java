/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client;

import com.squareup.okhttp.*;
import com.squareup.okhttp.internal.http.HttpMethod;
import com.squareup.okhttp.logging.HttpLoggingInterceptor;
import com.squareup.okhttp.logging.HttpLoggingInterceptor.Level;
import okio.BufferedSink;
import okio.Okio;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;

import javax.net.ssl.*;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.rtf.wowza.cloudsdk.client.auth.Authentication;
import com.rtf.wowza.cloudsdk.client.auth.HttpBasicAuth;
import com.rtf.wowza.cloudsdk.client.auth.ApiKeyAuth;
import com.rtf.wowza.cloudsdk.client.auth.OAuth;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/**
 * The Class ApiClient.
 */
public class ApiClient {


/** The api version. */
private String apiVersion = "1.3.1";

/** The api signature path. */
private String apiSignaturePath = "/api/v1.5";

/** The base path. */
//private String basePath = "https://sandbox.cloud.wowza.com"+apiSignaturePath;
private String basePath = "https://api.cloud.wowza.com"+apiSignaturePath;

/** The java version. */
private String javaVersion = System.getProperty("java.version");

/** The java VM name. */
private String javaVMName = System.getProperty("java.vm.name");

/** The os name. */
private String osName = System.getProperty("os.name");

/** The debugging. */
private boolean debugging = false;

/** The default header map. */
private Map<String, String> defaultHeaderMap = new HashMap<String, String>();

/** The temp folder path. */
private String tempFolderPath = null;

/** The authentications. */
private Map<String, ApiKeyAuth> authentications;

/** The date format. */
private DateFormat dateFormat;

/** The datetime format. */
private DateFormat datetimeFormat;

/** The lenient datetime format. */
private boolean lenientDatetimeFormat;

/** The date length. */
private int dateLength;

/** The ssl ca cert. */
private InputStream sslCaCert;

/** The verifying ssl. */
private boolean verifyingSsl;

/** The key managers. */
private KeyManager[] keyManagers;

/** The http client. */
private OkHttpClient httpClient;

/** The json. */
private JSON json;

/** The logging interceptor. */
private HttpLoggingInterceptor loggingInterceptor;

/*public String wscaccesskey = "OoUCOqWrWPlSIT7XNDuGTFdegA9yAUTk9IJLSuubDElidcDwTXJmDvj32rqX343f";
public String wscapikey = "KlHxyThNN8r95VKLyodMFTmAFHxK0POseDD7HQ8QDv4iRkHDta1ispQtcMgw3360";*/

/** The wscaccesskey. */
public String wscaccesskey = "vOgv8jhIVQpGfdhK46YHmhT7xFQvWi7i8Yyl1fwXMrZxrlHRuhoH3nv7PuQC3628";

/** The wscapikey. */
public String wscapikey = "Rkr2rpt7fKSULC5IpX6WJ0mJm78hQZQBDlDrMPDofmel9EZgTw0d34EA0Xmk3219";
    
    /**
     * Instantiates a new api client.
     */
    /*
     * Constructor for ApiClient
     */
    public ApiClient() {
        httpClient = new OkHttpClient();


        verifyingSsl = true;

        json = new JSON();

	// Set default User-Agent.
        String userAgent = "";
        userAgent =" {";
        userAgent+=" \"name\": \"WowzaCloudSDK-java-client\", ";
        userAgent+=" \"version\": \""+apiVersion+"\", ";
        userAgent+=" \"platform\": \""+osName+"\", ";
        userAgent+=" \"engine\": \""+javaVMName+","+javaVersion+"\" ";
        userAgent+=" }";

	setUserAgent(userAgent);

        // Setup authentications (key: authentication name, value: authentication).
        authentications = new HashMap<String, ApiKeyAuth>();
        authentications.put("wsc-access-key", new ApiKeyAuth("header", "wsc-access-key"));
        authentications.put("wsc-api-key", new ApiKeyAuth("header", "wsc-api-key"));
        authentications.put("wsc-timestamp", new ApiKeyAuth("header", "wsc-timestamp"));
        authentications.put("wsc-signature", new ApiKeyAuth("header", "wsc-signature"));
        // Prevent the authentications from being modified.
        authentications = Collections.unmodifiableMap(authentications);
    }
    
    /**
     * Gets the base path.
     *
     * @return the base path
     */
    public String getBasePath() {
        return basePath;
    }

    /**
     * Sets the base path.
     *
     * @param basePath the base path
     * @return the api client
     */
    public ApiClient setBasePath(String basePath) {
        this.basePath = basePath;
        return this;
    }

    /**
     * Gets the http client.
     *
     * @return the http client
     */
    public OkHttpClient getHttpClient() {
        return httpClient;
    }

    /**
     * Sets the http client.
     *
     * @param httpClient the http client
     * @return the api client
     */
    public ApiClient setHttpClient(OkHttpClient httpClient) {
        this.httpClient = httpClient;
        return this;
    }

    /**
     * Gets the json.
     *
     * @return the json
     */
    public JSON getJSON() {
        return json;
    }

    /**
     * Sets the JSON.
     *
     * @param json the json
     * @return the api client
     */
    public ApiClient setJSON(JSON json) {
        this.json = json;
        return this;
    }

    /**
     * Checks if is verifying ssl.
     *
     * @return true, if is verifying ssl
     */
    public boolean isVerifyingSsl() {
        return verifyingSsl;
    }

    /**
     * Sets the verifying ssl.
     *
     * @param verifyingSsl the verifying ssl
     * @return the api client
     */
    public ApiClient setVerifyingSsl(boolean verifyingSsl) {
        this.verifyingSsl = verifyingSsl;
        applySslSettings();
        return this;
    }

    /**
     * Gets the ssl ca cert.
     *
     * @return the ssl ca cert
     */
    public InputStream getSslCaCert() {
        return sslCaCert;
    }

    /**
     * Sets the ssl ca cert.
     *
     * @param sslCaCert the ssl ca cert
     * @return the api client
     */
    public ApiClient setSslCaCert(InputStream sslCaCert) {
        this.sslCaCert = sslCaCert;
        applySslSettings();
        return this;
    }

    /**
     * Gets the key managers.
     *
     * @return the key managers
     */
    public KeyManager[] getKeyManagers() {
        return keyManagers;
    }

    /**
     * Sets the key managers.
     *
     * @param managers the managers
     * @return the api client
     */
    public ApiClient setKeyManagers(KeyManager[] managers) {
        this.keyManagers = managers;
        applySslSettings();
        return this;
    }

    /**
     * Gets the date format.
     *
     * @return the date format
     */
    public DateFormat getDateFormat() {
        return dateFormat;
    }

    /**
     * Sets the date format.
     *
     * @param dateFormat the date format
     * @return the api client
     */
    public ApiClient setDateFormat(DateFormat dateFormat) {
        this.json.setDateFormat(dateFormat);
        return this;
    }

    /**
     * Sets the sql date format.
     *
     * @param dateFormat the date format
     * @return the api client
     */
    public ApiClient setSqlDateFormat(DateFormat dateFormat) {
        this.json.setSqlDateFormat(dateFormat);
        return this;
    }

    /**
     * Sets the offset date time format.
     *
     * @param dateFormat the date format
     * @return the api client
     */
    public ApiClient setOffsetDateTimeFormat(DateTimeFormatter dateFormat) {
        this.json.setOffsetDateTimeFormat(dateFormat);
        return this;
    }

    /**
     * Sets the local date format.
     *
     * @param dateFormat the date format
     * @return the api client
     */
    public ApiClient setLocalDateFormat(DateTimeFormatter dateFormat) {
        this.json.setLocalDateFormat(dateFormat);
        return this;
    }

    /**
     * Sets the lenient on json.
     *
     * @param lenientOnJson the lenient on json
     * @return the api client
     */
    public ApiClient setLenientOnJson(boolean lenientOnJson) {
        this.json.setLenientOnJson(lenientOnJson);
        return this;
    }

    /**
     * Gets the authentications.
     *
     * @return the authentications
     */
    public Map<String, ApiKeyAuth> getAuthentications() {
        return authentications;
    }

    /**
     * Gets the authentication.
     *
     * @param authName the auth name
     * @return the authentication
     */
    public Authentication getAuthentication(String authName) {
        return authentications.get(authName);
    }

    /**
     * Sets the username.
     *
     * @param username the new username
     */
    public void setUsername(String username) {
        for (Authentication auth : authentications.values()) {
            if (auth instanceof HttpBasicAuth) {
                ((HttpBasicAuth) auth).setUsername(username);
                return;
            }
        }
        throw new RuntimeException("No HTTP basic authentication configured!");
    }

    /**
     * Sets the password.
     *
     * @param password the new password
     */
    public void setPassword(String password) {
        for (Authentication auth : authentications.values()) {
            if (auth instanceof HttpBasicAuth) {
                ((HttpBasicAuth) auth).setPassword(password);
                return;
            }
        }
        throw new RuntimeException("No HTTP basic authentication configured!");
    }

    /**
     * Sets the api key.
     *
     * @param apiKey the new api key
     */
    public void setApiKey(String apiKey) {
        for (Authentication auth : authentications.values()) {
            if (auth instanceof ApiKeyAuth) {
                ((ApiKeyAuth) auth).setApiKey(apiKey);
                return;
            }
        }
        throw new RuntimeException("No API key authentication configured!");
    }

    /**
     * Sets the api key prefix.
     *
     * @param apiKeyPrefix the new api key prefix
     */
    public void setApiKeyPrefix(String apiKeyPrefix) {
        for (Authentication auth : authentications.values()) {
            if (auth instanceof ApiKeyAuth) {
                ((ApiKeyAuth) auth).setApiKeyPrefix(apiKeyPrefix);
                return;
            }
        }
        throw new RuntimeException("No API key authentication configured!");
    }

    /**
     * Sets the access token.
     *
     * @param accessToken the new access token
     */
    public void setAccessToken(String accessToken) {
        for (Authentication auth : authentications.values()) {
            if (auth instanceof OAuth) {
                ((OAuth) auth).setAccessToken(accessToken);
                return;
            }
        }
        throw new RuntimeException("No OAuth2 authentication configured!");
    }

    /**
     * Sets the user agent.
     *
     * @param userAgent the user agent
     * @return the api client
     */
    public ApiClient setUserAgent(String userAgent) {
        addDefaultHeader("User-Agent", userAgent);
        return this;
    }

    /**
     * Adds the default header.
     *
     * @param key the key
     * @param value the value
     * @return the api client
     */
    public ApiClient addDefaultHeader(String key, String value) {
        defaultHeaderMap.put(key, value);
        return this;
    }

    /**
     * Checks if is debugging.
     *
     * @return true, if is debugging
     */
    public boolean isDebugging() {
        return debugging;
    }

    /**
     * Sets the debugging.
     *
     * @param debugging the debugging
     * @return the api client
     */
    public ApiClient setDebugging(boolean debugging) {
        if (debugging != this.debugging) {
            if (debugging) {
                loggingInterceptor = new HttpLoggingInterceptor();
                loggingInterceptor.setLevel(Level.BODY);
                httpClient.interceptors().add(loggingInterceptor);
            } else {
                httpClient.interceptors().remove(loggingInterceptor);
                loggingInterceptor = null;
            }
        }
        this.debugging = debugging;
        return this;
    }

    /**
     * Gets the temp folder path.
     *
     * @return the temp folder path
     */
    public String getTempFolderPath() {
        return tempFolderPath;
    }

    /**
     * Sets the temp folder path.
     *
     * @param tempFolderPath the temp folder path
     * @return the api client
     */
    public ApiClient setTempFolderPath(String tempFolderPath) {
        this.tempFolderPath = tempFolderPath;
        return this;
    }

    /**
     * Gets the connect timeout.
     *
     * @return the connect timeout
     */
    public int getConnectTimeout() {
        return httpClient.getConnectTimeout();
    }

    /**
     * Sets the connect timeout.
     *
     * @param connectionTimeout the connection timeout
     * @return the api client
     */
    public ApiClient setConnectTimeout(int connectionTimeout) {
        httpClient.setConnectTimeout(connectionTimeout, TimeUnit.MILLISECONDS);
        return this;
    }

    /**
     * Gets the read timeout.
     *
     * @return the read timeout
     */
    public int getReadTimeout() {
        return httpClient.getReadTimeout();
    }

    /**
     * Sets the read timeout.
     *
     * @param readTimeout the read timeout
     * @return the api client
     */
    public ApiClient setReadTimeout(int readTimeout) {
        httpClient.setReadTimeout(readTimeout, TimeUnit.MILLISECONDS);
        return this;
    }

    /**
     * Gets the write timeout.
     *
     * @return the write timeout
     */
    public int getWriteTimeout() {
        return httpClient.getWriteTimeout();
    }

    /**
     * Sets the write timeout.
     *
     * @param writeTimeout the write timeout
     * @return the api client
     */
    public ApiClient setWriteTimeout(int writeTimeout) {
        httpClient.setWriteTimeout(writeTimeout, TimeUnit.MILLISECONDS);
        return this;
    }

    /**
     * Parameter to string.
     *
     * @param param the param
     * @return the string
     */
    public String parameterToString(Object param) {
        if (param == null) {
            return "";
        } else if (param instanceof Date || param instanceof OffsetDateTime || param instanceof LocalDate) {
            //Serialize to json string and remove the " enclosing characters
            String jsonStr = json.serialize(param);
            return jsonStr.substring(1, jsonStr.length() - 1);
        } else if (param instanceof Collection) {
            StringBuilder b = new StringBuilder();
            for (Object o : (Collection)param) {
                if (b.length() > 0) {
                    b.append(",");
                }
                b.append(String.valueOf(o));
            }
            return b.toString();
        } else {
            return String.valueOf(param);
        }
    }

    /**
     * Parameter to pair.
     *
     * @param name the name
     * @param value the value
     * @return the list
     */
    public List<Pair> parameterToPair(String name, Object value) {
        List<Pair> params = new ArrayList<Pair>();

        // preconditions
        if (name == null || name.isEmpty() || value == null || value instanceof Collection) return params;

        params.add(new Pair(name, parameterToString(value)));
        return params;
    }

    /**
     * Parameter to pairs.
     *
     * @param collectionFormat the collection format
     * @param name the name
     * @param value the value
     * @return the list
     */
    public List<Pair> parameterToPairs(String collectionFormat, String name, Collection value) {
        List<Pair> params = new ArrayList<Pair>();

        // preconditions
        if (name == null || name.isEmpty() || value == null || value.isEmpty()) {
            return params;
        }

        // create the params based on the collection format
        if ("multi".equals(collectionFormat)) {
            for (Object item : value) {
                params.add(new Pair(name, escapeString(parameterToString(item))));
            }
            return params;
        }

        // collectionFormat is assumed to be "csv" by default
        String delimiter = ",";

        // escape all delimiters except commas, which are URI reserved
        // characters
        if ("ssv".equals(collectionFormat)) {
            delimiter = escapeString(" ");
        } else if ("tsv".equals(collectionFormat)) {
            delimiter = escapeString("\t");
        } else if ("pipes".equals(collectionFormat)) {
            delimiter = escapeString("|");
        }

        StringBuilder sb = new StringBuilder() ;
        for (Object item : value) {
            sb.append(delimiter);
            sb.append(escapeString(parameterToString(item)));
        }

        params.add(new Pair(name, sb.substring(delimiter.length())));

        return params;
    }

    /**
     * Sanitize filename.
     *
     * @param filename the filename
     * @return the string
     */
    public String sanitizeFilename(String filename) {
        return filename.replaceAll(".*[/\\\\]", "");
    }

    /**
     * Checks if is json mime.
     *
     * @param mime the mime
     * @return true, if is json mime
     */
    public boolean isJsonMime(String mime) {
      String jsonMime = "(?i)^(application/json|[^;/ \t]+/[^;/ \t]+[+]json)[ \t]*(;.*)?$";
      return mime != null && (mime.matches(jsonMime) || mime.equals("*/*"));
    }

    /**
     * Select header accept.
     *
     * @param accepts the accepts
     * @return the string
     */
    public String selectHeaderAccept(String[] accepts) {
        if (accepts.length == 0) {
            return null;
        }
        for (String accept : accepts) {
            if (isJsonMime(accept)) {
                return accept;
            }
        }
        return StringUtil.join(accepts, ",");
    }

    /**
     * Select header content type.
     *
     * @param contentTypes the content types
     * @return the string
     */
    public String selectHeaderContentType(String[] contentTypes) {
        if (contentTypes.length == 0 || contentTypes[0].equals("*/*")) {
             return "application/json";
        }
        for (String contentType : contentTypes) {
            if (isJsonMime(contentType)) {
                return contentType;
            }
        }
        return contentTypes[0];
    }

    /**
     * Escape string.
     *
     * @param str the str
     * @return the string
     */
    public String escapeString(String str) {
        try {
            return URLEncoder.encode(str, "utf8").replaceAll("\\+", "%20");
        } catch (UnsupportedEncodingException e) {
            return str;
        }
    }

    /**
     * Deserialize.
     *
     * @param <T> the generic type
     * @param response the response
     * @param returnType the return type
     * @return the t
     * @throws ApiException the api exception
     */
    @SuppressWarnings("unchecked")
    public <T> T deserialize(Response response, Type returnType) throws ApiException {
        if (response == null || returnType == null) {
            return null;
        }

        if ("byte[]".equals(returnType.toString())) {
            // Handle binary response (byte array).
            try {
                return (T) response.body().bytes();
            } catch (IOException e) {
                throw new ApiException(e);
            }
        } else if (returnType.equals(File.class)) {
            // Handle file downloading.
            return (T) downloadFileFromResponse(response);
        }

        String respBody;
        try {
            if (response.body() != null)
                respBody = response.body().string();
            else
                respBody = null;
        } catch (IOException e) {
            throw new ApiException(e);
        }

        if (respBody == null || "".equals(respBody)) {
            return null;
        }

        String contentType = response.headers().get("Content-Type");
        if (contentType == null) {
            // ensuring a default content type
            contentType = "application/json";
        }
        if (isJsonMime(contentType)) {
            return json.deserialize(respBody, returnType);
        } else if (returnType.equals(String.class)) {
            // Expecting string, return the raw response body.
            return (T) respBody;
        } else {
            throw new ApiException(
                    "Content type \"" + contentType + "\" is not supported for type: " + returnType,
                    response.code(),
                    response.headers().toMultimap(),
                    respBody);
        }
    }

    /**
     * Serialize.
     *
     * @param obj the obj
     * @param contentType the content type
     * @return the request body
     * @throws ApiException the api exception
     */
    public RequestBody serialize(Object obj, String contentType) throws ApiException {
        if (obj instanceof byte[]) {
            // Binary (byte array) body parameter support.
            return RequestBody.create(MediaType.parse(contentType), (byte[]) obj);
        } else if (obj instanceof File) {
            // File body parameter support.
            return RequestBody.create(MediaType.parse(contentType), (File) obj);
        } else if (isJsonMime(contentType)) {
            String content;
            if (obj != null) {
                content = json.serialize(obj);
            } else {
                content = null;
            }
            return RequestBody.create(MediaType.parse(contentType), content);
        } else {
            throw new ApiException("Content type \"" + contentType + "\" is not supported");
        }
    }

    /**
     * Download file from response.
     *
     * @param response the response
     * @return the file
     * @throws ApiException the api exception
     */
    public File downloadFileFromResponse(Response response) throws ApiException {
        try {
            File file = prepareDownloadFile(response);
            BufferedSink sink = Okio.buffer(Okio.sink(file));
            sink.writeAll(response.body().source());
            sink.close();
            return file;
        } catch (IOException e) {
            throw new ApiException(e);
        }
    }

    /**
     * Prepare download file.
     *
     * @param response the response
     * @return the file
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public File prepareDownloadFile(Response response) throws IOException {
        String filename = null;
        String contentDisposition = response.header("Content-Disposition");
        if (contentDisposition != null && !"".equals(contentDisposition)) {
            // Get filename from the Content-Disposition header.
            Pattern pattern = Pattern.compile("filename=['\"]?([^'\"\\s]+)['\"]?");
            Matcher matcher = pattern.matcher(contentDisposition);
            if (matcher.find()) {
                filename = sanitizeFilename(matcher.group(1));
            }
        }

        String prefix = null;
        String suffix = null;
        if (filename == null) {
            prefix = "download-";
            suffix = "";
        } else {
            int pos = filename.lastIndexOf(".");
            if (pos == -1) {
                prefix = filename + "-";
            } else {
                prefix = filename.substring(0, pos) + "-";
                suffix = filename.substring(pos);
            }
            // File.createTempFile requires the prefix to be at least three characters long
            if (prefix.length() < 3)
                prefix = "download-";
        }

        if (tempFolderPath == null)
            return File.createTempFile(prefix, suffix);
        else
            return File.createTempFile(prefix, suffix, new File(tempFolderPath));
    }

    /**
     * Execute.
     *
     * @param <T> the generic type
     * @param call the call
     * @return the api response
     * @throws ApiException the api exception
     */
    public <T> ApiResponse<T> execute(Call call) throws ApiException {
        return execute(call, null);
    }

    /**
     * Execute.
     *
     * @param <T> the generic type
     * @param call the call
     * @param returnType the return type
     * @return the api response
     * @throws ApiException the api exception
     */
    public <T> ApiResponse<T> execute(Call call, Type returnType) throws ApiException {
        try {
            Response response = call.execute();
            T data = handleResponse(response, returnType);
            return new ApiResponse<T>(response.code(), response.headers().toMultimap(), data);
        } catch (IOException e) {
            throw new ApiException(e);
        }
    }

    /**
     * Execute async.
     *
     * @param <T> the generic type
     * @param call the call
     * @param callback the callback
     */
    public <T> void executeAsync(Call call, ApiCallback<T> callback) {
        executeAsync(call, null, callback);
    }

    /**
     * Execute async.
     *
     * @param <T> the generic type
     * @param call the call
     * @param returnType the return type
     * @param callback the callback
     */
    @SuppressWarnings("unchecked")
    public <T> void executeAsync(Call call, final Type returnType, final ApiCallback<T> callback) {
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                callback.onFailure(new ApiException(e), 0, null);
            }

            @Override
            public void onResponse(Response response) throws IOException {
                T result;
                try {
                    result = (T) handleResponse(response, returnType);
                } catch (ApiException e) {
                    callback.onFailure(e, response.code(), response.headers().toMultimap());
                    return;
                }
                callback.onSuccess(result, response.code(), response.headers().toMultimap());
            }
        });
    }

    /**
     * Handle response.
     *
     * @param <T> the generic type
     * @param response the response
     * @param returnType the return type
     * @return the t
     * @throws ApiException the api exception
     */
    public <T> T handleResponse(Response response, Type returnType) throws ApiException {
        if (response.isSuccessful()) {
            if (returnType == null || response.code() == 204) {
                // returning null if the returnType is not defined,
                // or the status code is 204 (No Content)
                if (response.body() != null) {
                    try {
                        response.body().close();
                    } catch (IOException e) {
                        throw new ApiException(response.message(), e, response.code(), response.headers().toMultimap());
                    }
                }
                return null;
            } else {
                return deserialize(response, returnType);
            }
        } else {
            String respBody = null;
            if (response.body() != null) {
                try {
                    respBody = response.body().string();
                } catch (IOException e) {
                    throw new ApiException(response.message(), e, response.code(), response.headers().toMultimap());
                }
            }
            throw new ApiException(response.message(), response.code(), response.headers().toMultimap(), respBody);
        }
    }

    /**
     * Builds the call.
     *
     * @param path the path
     * @param method the method
     * @param queryParams the query params
     * @param collectionQueryParams the collection query params
     * @param body the body
     * @param headerParams the header params
     * @param formParams the form params
     * @param authNames the auth names
     * @param progressRequestListener the progress request listener
     * @return the call
     * @throws ApiException the api exception
     */
    public Call buildCall(String path, String method, List<Pair> queryParams, List<Pair> collectionQueryParams, Object body, Map<String, String> headerParams, Map<String, Object> formParams, String[] authNames, ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Request request = buildRequest(path, method, queryParams, collectionQueryParams, body, headerParams, formParams, authNames, progressRequestListener);

        return httpClient.newCall(request);
    }

    /**
     * Builds the request.
     *
     * @param path the path
     * @param method the method
     * @param queryParams the query params
     * @param collectionQueryParams the collection query params
     * @param body the body
     * @param headerParams the header params
     * @param formParams the form params
     * @param authNames the auth names
     * @param progressRequestListener the progress request listener
     * @return the request
     * @throws ApiException the api exception
     */
    public Request buildRequest(String path, String method, List<Pair> queryParams, List<Pair> collectionQueryParams, Object body, Map<String, String> headerParams, Map<String, Object> formParams, String[] authNames, ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        updateParamsForAuth(path,authNames, queryParams, headerParams);

        final String url = buildUrl(path, queryParams, collectionQueryParams);
        final Request.Builder reqBuilder = new Request.Builder().url(url);
        processHeaderParams(headerParams, reqBuilder);

        String contentType = (String) headerParams.get("Content-Type");
        // ensuring a default content type
        if (contentType == null) {
            contentType = "application/json";
        }

        RequestBody reqBody;
        if (!HttpMethod.permitsRequestBody(method)) {
            reqBody = null;
        } else if ("application/x-www-form-urlencoded".equals(contentType)) {
            reqBody = buildRequestBodyFormEncoding(formParams);
        } else if ("multipart/form-data".equals(contentType)) {
            reqBody = buildRequestBodyMultipart(formParams);
        } else if (body == null) {
            if ("DELETE".equals(method)) {
                // allow calling DELETE without sending a request body
                reqBody = null;
            } else {
                // use an empty request body (for POST, PUT and PATCH)
                reqBody = RequestBody.create(MediaType.parse(contentType), "");
            }
        } else {
            reqBody = serialize(body, contentType);
        }

        Request request = null;

        if(progressRequestListener != null && reqBody != null) {
            ProgressRequestBody progressRequestBody = new ProgressRequestBody(reqBody, progressRequestListener);
            request = reqBuilder.method(method, progressRequestBody).build();
        } else {
            request = reqBuilder.method(method, reqBody).build();
        }

        return request;
    }

    /**
     * Builds the url.
     *
     * @param path the path
     * @param queryParams the query params
     * @param collectionQueryParams the collection query params
     * @return the string
     */
    public String buildUrl(String path, List<Pair> queryParams, List<Pair> collectionQueryParams) {
        final StringBuilder url = new StringBuilder();
        url.append(basePath).append(path);

        if (queryParams != null && !queryParams.isEmpty()) {
            // support (constant) query string in `path`, e.g. "/posts?draft=1"
            String prefix = path.contains("?") ? "&" : "?";
            for (Pair param : queryParams) {
                if (param.getValue() != null) {
                    if (prefix != null) {
                        url.append(prefix);
                        prefix = null;
                    } else {
                        url.append("&");
                    }
                    String value = parameterToString(param.getValue());
                    url.append(escapeString(param.getName())).append("=").append(escapeString(value));
                }
            }
        }

        if (collectionQueryParams != null && !collectionQueryParams.isEmpty()) {
            String prefix = url.toString().contains("?") ? "&" : "?";
            for (Pair param : collectionQueryParams) {
                if (param.getValue() != null) {
                    if (prefix != null) {
                        url.append(prefix);
                        prefix = null;
                    } else {
                        url.append("&");
                    }
                    String value = parameterToString(param.getValue());
                    // collection query parameter value already escaped as part of parameterToPairs
                    url.append(escapeString(param.getName())).append("=").append(value);
                }
            }
        }

        return url.toString();
    }

    /**
     * Process header params.
     *
     * @param headerParams the header params
     * @param reqBuilder the req builder
     */
    public void processHeaderParams(Map<String, String> headerParams, Request.Builder reqBuilder) {
        for (Entry<String, String> param : headerParams.entrySet()) {
            reqBuilder.header(param.getKey(), parameterToString(param.getValue()));
        }
        for (Entry<String, String> header : defaultHeaderMap.entrySet()) {
            if (!headerParams.containsKey(header.getKey())) {
                reqBuilder.header(header.getKey(), parameterToString(header.getValue()));
            }
        }
    }

    /**
     * Update params for auth.
     *
     * @param path the path
     * @param authNames the auth names
     * @param queryParams the query params
     * @param headerParams the header params
     */
    public void updateParamsForAuth(String path,String[] authNames, List<Pair> queryParams, Map<String, String> headerParams) {
    	long timeStamp = 0L;
    	String apiKey = "";
        for (String authName : authNames) {
        	ApiKeyAuth auth = authentications.get(authName);
            if (auth == null) throw new RuntimeException("Authentication undefined: " + authName);

            if ( auth.getParamName().equals("wsc-api-key") )
            	apiKey = auth.getApiKey();

            if ( auth.getParamName().equals("wsc-timestamp") )
            {
            	timeStamp = System.currentTimeMillis()/1000;
            	auth.setApiKey(String.valueOf(timeStamp));
            }
            auth.applyToParams(queryParams, headerParams);
        }
      ApiKeyAuth auth = authentications.get("wsc-signature");
      if ( auth != null ) {
      	String signature = generate_request_signature(path,apiKey,timeStamp);
      	auth.setApiKey(signature);
      	auth.applyToParams(queryParams, headerParams);
      }
    }


    /**
     * Generate request signature.
     *
     * @param requestPath the request path
     * @param apiKey the api key
     * @param timeStamp the time stamp
     * @return the string
     */
    private String generate_request_signature(String requestPath, String apiKey, long timeStamp)
    {
    	// Make sure we only have the path.  No query parameters
    	requestPath = requestPath.split("\\?")[0];

    	// Make sure there is a leading slash
    	if (!requestPath.startsWith("/"))
    		requestPath = "/"+ requestPath;

    	// Make sure there is not a trailing slash
    	if (requestPath.endsWith("/"))
    		requestPath = requestPath.substring(0, requestPath.length() - 1);

        requestPath = apiSignaturePath+requestPath;
    	// Make the complete request string
    	String data = timeStamp +":" + requestPath +":" + apiKey;

    	String output = "";
    	try {
    		Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
    		SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes("UTF-8"), "HmacSHA256");
    		sha256_HMAC.init(secret_key);
    		// Return the HMAC-SHA256-Hex string.
    		byte[] byteData = sha256_HMAC.doFinal(data.getBytes("UTF-8"));
    		output = formatBytes(byteData);
    	} catch  ( Exception noAlgo ) {
    		System.out.println("ApiClient: generate_request_signature: failed: "+noAlgo.toString());
    	}
    	return output;
    }

   /**
    * Format bytes.
    *
    * @param data the data
    * @return the string
    */
   private String formatBytes(byte[] data)
   {
       String output="";
       for(int i=0;i<data.length;i++)
		{
			int c = (int)0xff&data[i];
			String pStr = toTwoDigitHex(c);
			output+=pStr;
		}
	return output;
   }

    /**
     * To two digit hex.
     *
     * @param i the i
     * @return the string
     */
    private static String toTwoDigitHex(int i) {
    	String hex = Integer.toHexString(i);
    	if (hex.length() < 2)
    		hex = "0"+hex;
    	return hex;
    }


    /**
     * Builds the request body form encoding.
     *
     * @param formParams the form params
     * @return the request body
     */
    public RequestBody buildRequestBodyFormEncoding(Map<String, Object> formParams) {
        FormEncodingBuilder formBuilder  = new FormEncodingBuilder();
        for (Entry<String, Object> param : formParams.entrySet()) {
            formBuilder.add(param.getKey(), parameterToString(param.getValue()));
        }
        return formBuilder.build();
    }

    /**
     * Builds the request body multipart.
     *
     * @param formParams the form params
     * @return the request body
     */
    public RequestBody buildRequestBodyMultipart(Map<String, Object> formParams) {
        MultipartBuilder mpBuilder = new MultipartBuilder().type(MultipartBuilder.FORM);
        for (Entry<String, Object> param : formParams.entrySet()) {
            if (param.getValue() instanceof File) {
                File file = (File) param.getValue();
                Headers partHeaders = Headers.of("Content-Disposition", "form-data; name=\"" + param.getKey() + "\"; filename=\"" + file.getName() + "\"");
                MediaType mediaType = MediaType.parse(guessContentTypeFromFile(file));
                mpBuilder.addPart(partHeaders, RequestBody.create(mediaType, file));
            } else {
                Headers partHeaders = Headers.of("Content-Disposition", "form-data; name=\"" + param.getKey() + "\"");
                mpBuilder.addPart(partHeaders, RequestBody.create(null, parameterToString(param.getValue())));
            }
        }
        return mpBuilder.build();
    }

    /**
     * Guess content type from file.
     *
     * @param file the file
     * @return the string
     */
    public String guessContentTypeFromFile(File file) {
        String contentType = URLConnection.guessContentTypeFromName(file.getName());
        if (contentType == null) {
            return "application/octet-stream";
        } else {
            return contentType;
        }
    }

    /**
     * Apply ssl settings.
     */
    private void applySslSettings() {
        try {
            TrustManager[] trustManagers = null;
            HostnameVerifier hostnameVerifier = null;
            if (!verifyingSsl) {
                TrustManager trustAll = new X509TrustManager() {
                    @Override
                    public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {}
                    @Override
                    public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {}
                    @Override
                    public X509Certificate[] getAcceptedIssuers() { return null; }
                };
                SSLContext sslContext = SSLContext.getInstance("TLS");
                trustManagers = new TrustManager[]{ trustAll };
                hostnameVerifier = new HostnameVerifier() {
                    @Override
                    public boolean verify(String hostname, SSLSession session) { return true; }
                };
            } else if (sslCaCert != null) {
                char[] password = null; // Any password will work.
                CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
                Collection<? extends Certificate> certificates = certificateFactory.generateCertificates(sslCaCert);
                if (certificates.isEmpty()) {
                    throw new IllegalArgumentException("expected non-empty set of trusted certificates");
                }
                KeyStore caKeyStore = newEmptyKeyStore(password);
                int index = 0;
                for (Certificate certificate : certificates) {
                    String certificateAlias = "ca" + Integer.toString(index++);
                    caKeyStore.setCertificateEntry(certificateAlias, certificate);
                }
                TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
                trustManagerFactory.init(caKeyStore);
                trustManagers = trustManagerFactory.getTrustManagers();
            }

            if (keyManagers != null || trustManagers != null) {
                SSLContext sslContext = SSLContext.getInstance("TLS");
                sslContext.init(keyManagers, trustManagers, new SecureRandom());
                httpClient.setSslSocketFactory(sslContext.getSocketFactory());
            } else {
                httpClient.setSslSocketFactory(null);
            }
            httpClient.setHostnameVerifier(hostnameVerifier);
        } catch (GeneralSecurityException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * New empty key store.
     *
     * @param password the password
     * @return the key store
     * @throws GeneralSecurityException the general security exception
     */
    private KeyStore newEmptyKeyStore(char[] password) throws GeneralSecurityException {
        try {
            KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            keyStore.load(null, password);
            return keyStore;
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }
}
