/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.rtf.wowza.cloudsdk.client.model.ShmMetrics;
import java.io.IOException;

/**
 * The Class LiveStreamCreateMetrics.
 */
public class LiveStreamCreateMetrics {
  
  /** The live stream metrics. */
  @SerializedName("live_stream")
  private ShmMetrics liveStreamMetrics = null;

  /**
   * Live stream metrics.
   *
   * @param liveStreamMetrics the live stream metrics
   * @return the live stream create metrics
   */
  public LiveStreamCreateMetrics liveStreamMetrics(ShmMetrics liveStreamMetrics) {
    this.liveStreamMetrics = liveStreamMetrics;
    return this;
  }

   /**
    * Gets the live stream metrics.
    *
    * @return the live stream metrics
    */
  @ApiModelProperty(required = true, value = "")
  public ShmMetrics getLiveStreamMetrics() {
    return liveStreamMetrics;
  }

  /**
   * Sets the live stream metrics.
   *
   * @param liveStreamMetrics the new live stream metrics
   */
  public void setLiveStreamMetrics(ShmMetrics liveStreamMetrics) {
    this.liveStreamMetrics = liveStreamMetrics;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    LiveStreamCreateMetrics inlineResponse2007 = (LiveStreamCreateMetrics) o;
    return Objects.equals(this.liveStreamMetrics, inlineResponse2007.liveStreamMetrics);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(liveStreamMetrics);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class LiveStreamCreateMetrics {\n");
    
    sb.append("    liveStreamMetrics: ").append(toIndentedString(liveStreamMetrics)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

