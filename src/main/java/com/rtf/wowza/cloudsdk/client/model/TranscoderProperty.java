/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * The Class TranscoderProperty.
 */
@ApiModel(description = "")
public class TranscoderProperty {
  
  /** The key. */
  @SerializedName("key")
  private String key = null;

  /** The section. */
  @SerializedName("section")
  private String section = null;

  /** The value. */
  @SerializedName("value")
  private String value = null;

  /**
   * Key.
   *
   * @param key the key
   * @return the transcoder property
   */
  public TranscoderProperty key(String key) {
    this.key = key;
    return this;
  }

   /**
    * Gets the key.
    *
    * @return the key
    */
  @ApiModelProperty(example = "", value = "The key of the property. <br /><br />For <strong>rtsp</strong>, valid values are <strong>debugRtspSession</strong>, <strong>maxRtcpWaitTime</strong>, <strong>avSyncMethod</strong>, <strong>rtspValidationFrequency</strong>, <strong>rtpTransportMode</strong>, <strong>rtspFilterUnknownTracks</strong>, <strong>rtpIgnoreSpropParameterSets</strong>, and <strong>rtpIgnoreProfileLevelId</strong>. <br /><br />For <strong>cupertino</strong>, valid values are <strong>cupertinoEnableProgramDateTime</strong>, <strong>cupertinoEnableId3ProgramDateTime</strong>, and <strong>cupertinoProgramDateTimeOffset</strong>.")
  public String getKey() {
    return key;
  }

  /**
   * Sets the key.
   *
   * @param key the new key
   */
  public void setKey(String key) {
    this.key = key;
  }

  /**
   * Section.
   *
   * @param section the section
   * @return the transcoder property
   */
  public TranscoderProperty section(String section) {
    this.section = section;
    return this;
  }

   /**
    * Gets the section.
    *
    * @return the section
    */
  @ApiModelProperty(example = "", value = "The section of the transcoder configuration table that contains the property. Valid values are <strong>rtsp</strong> and <strong>cupertino</strong>.")
  public String getSection() {
    return section;
  }

  /**
   * Sets the section.
   *
   * @param section the new section
   */
  public void setSection(String section) {
    this.section = section;
  }

  /**
   * Value.
   *
   * @param value the value
   * @return the transcoder property
   */
  public TranscoderProperty value(String value) {
    this.value = value;
    return this;
  }

   /**
    * Gets the value.
    *
    * @return the value
    */
  @ApiModelProperty(example = "", value = "The value of the property. <br /><br /> For <strong>debugRtspSession</strong>, <strong>avSyncMethod</strong>, <strong>rtspFilterUnknownTracks</strong>, <strong>rtpIgnoreSpropParameterSets</strong>, <strong>rtpIgnoreProfileLevelId</strong>, <strong>cupertinoEnableProgramDateTime</strong>, and <strong>cupertinoEnableId3ProgramDateTime</strong>, valid values are the Booleans <strong>true</strong> and <strong>false</strong>. <br /><br />For <strong>maxRtcpWaitTime</strong>, valid values are <strong>0</strong> (ms) and greater, expressed as an integer. <br /><br />For <strong>rtpTransportMode</strong>, valid values are the strings <strong>udp</strong> and <strong>interleave</strong>. <br /><br />For <strong>rtspValidationFrequency</strong>, valid values are <strong>0</strong> (ms) and greater, expressed as an integer. <br /><br />For <strong>cupertinoProgramDateTimeOffset</strong>, valid values are any integer (ms), expressed as an integer.")
  public String getValue() {
    return value;
  }

  /**
   * Sets the value.
   *
   * @param value the new value
   */
  public void setValue(String value) {
    this.value = value;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TranscoderProperty transcoderProperty = (TranscoderProperty) o;
    return Objects.equals(this.key, transcoderProperty.key) &&
        Objects.equals(this.section, transcoderProperty.section) &&
        Objects.equals(this.value, transcoderProperty.value);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(key, section, value);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TranscoderProperty {\n");
    
    sb.append("    key: ").append(toIndentedString(key)).append("\n");
    sb.append("    section: ").append(toIndentedString(section)).append("\n");
    sb.append("    value: ").append(toIndentedString(value)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

