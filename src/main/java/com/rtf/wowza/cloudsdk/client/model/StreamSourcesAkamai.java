/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.rtf.wowza.cloudsdk.client.model.IndexAkamaiStreamSource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class StreamSourcesAkamai.
 */
@ApiModel(description = "")
public class StreamSourcesAkamai {
  
  /** The stream sources akamai. */
  @SerializedName("stream_sources_akamai")
  private List<IndexAkamaiStreamSource> streamSourcesAkamai = new ArrayList<IndexAkamaiStreamSource>();

  /**
   * Stream sources akamai.
   *
   * @param streamSourcesAkamai the stream sources akamai
   * @return the stream sources akamai
   */
  public StreamSourcesAkamai streamSourcesAkamai(List<IndexAkamaiStreamSource> streamSourcesAkamai) {
    this.streamSourcesAkamai = streamSourcesAkamai;
    return this;
  }

  /**
   * Adds the stream sources akamai item.
   *
   * @param streamSourcesAkamaiItem the stream sources akamai item
   * @return the stream sources akamai
   */
  public StreamSourcesAkamai addStreamSourcesAkamaiItem(IndexAkamaiStreamSource streamSourcesAkamaiItem) {
    this.streamSourcesAkamai.add(streamSourcesAkamaiItem);
    return this;
  }

   /**
    * Gets the stream sources akamai.
    *
    * @return the stream sources akamai
    */
  @ApiModelProperty(required = true, value = "")
  public List<IndexAkamaiStreamSource> getStreamSourcesAkamai() {
    return streamSourcesAkamai;
  }

  /**
   * Sets the stream sources akamai.
   *
   * @param streamSourcesAkamai the new stream sources akamai
   */
  public void setStreamSourcesAkamai(List<IndexAkamaiStreamSource> streamSourcesAkamai) {
    this.streamSourcesAkamai = streamSourcesAkamai;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    StreamSourcesAkamai streamSourcesAkamai = (StreamSourcesAkamai) o;
    return Objects.equals(this.streamSourcesAkamai, streamSourcesAkamai.streamSourcesAkamai);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(streamSourcesAkamai);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class StreamSourcesAkamai {\n");
    
    sb.append("    streamSourcesAkamai: ").append(toIndentedString(streamSourcesAkamai)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

