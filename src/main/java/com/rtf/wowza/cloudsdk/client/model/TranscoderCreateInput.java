/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.rtf.wowza.cloudsdk.client.model.Transcoder;
import java.io.IOException;

/**
 * The Class TranscoderCreateInput.
 */
@ApiModel(description = "")
public class TranscoderCreateInput {
  
  /** The transcoder. */
  @SerializedName("transcoder")
  private Transcoder transcoder = null;

  /**
   * Transcoder.
   *
   * @param transcoder the transcoder
   * @return the transcoder create input
   */
  public TranscoderCreateInput transcoder(Transcoder transcoder) {
    this.transcoder = transcoder;
    return this;
  }

   /**
    * Gets the transcoder.
    *
    * @return the transcoder
    */
  @ApiModelProperty(required = true, value = "")
  public Transcoder getTranscoder() {
    return transcoder;
  }

  /**
   * Sets the transcoder.
   *
   * @param transcoder the new transcoder
   */
  public void setTranscoder(Transcoder transcoder) {
    this.transcoder = transcoder;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TranscoderCreateInput transcoderCreateInput = (TranscoderCreateInput) o;
    return Objects.equals(this.transcoder, transcoderCreateInput.transcoder);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(transcoder);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TranscoderCreateInput {\n");
    
    sb.append("    transcoder: ").append(toIndentedString(transcoder)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

