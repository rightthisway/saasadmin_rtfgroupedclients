/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.rtf.wowza.cloudsdk.client.model.LiveStreamState;
import java.io.IOException;

/**
 * The Class LiveStreamCreateState.
 */
public class LiveStreamCreateState {
  
  /** The live stream state. */
  @SerializedName("live_stream")
  private LiveStreamState liveStreamState = null;

  /**
   * Live stream state.
   *
   * @param liveStreamState the live stream state
   * @return the live stream create state
   */
  public LiveStreamCreateState liveStreamState(LiveStreamState liveStreamState) {
    this.liveStreamState = liveStreamState;
    return this;
  }

   /**
    * Gets the live stream state.
    *
    * @return the live stream state
    */
  @ApiModelProperty(required = true, value = "")
  public LiveStreamState getLiveStreamState() {
    return liveStreamState;
  }

  /**
   * Sets the live stream state.
   *
   * @param liveStreamState the new live stream state
   */
  public void setLiveStreamState(LiveStreamState liveStreamState) {
    this.liveStreamState = liveStreamState;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    LiveStreamCreateState inlineResponse2006 = (LiveStreamCreateState) o;
    return Objects.equals(this.liveStreamState, inlineResponse2006.liveStreamState);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(liveStreamState);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class LiveStreamCreateState {\n");
    
    sb.append("    liveStreamState: ").append(toIndentedString(liveStreamState)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

