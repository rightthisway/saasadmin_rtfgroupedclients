/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.rtf.wowza.cloudsdk.client.model.LiveStreamThumbnail;
import java.io.IOException;

/**
 * The Class LiveStreamCreateThumbnail.
 */
public class LiveStreamCreateThumbnail {
  
  /** The live stream thumbnail. */
  @SerializedName("live_stream")
  private LiveStreamThumbnail liveStreamThumbnail = null;

  /**
   * Live stream thumbnail.
   *
   * @param liveStreamThumbnail the live stream thumbnail
   * @return the live stream create thumbnail
   */
  public LiveStreamCreateThumbnail liveStreamThumbnail(LiveStreamThumbnail liveStreamThumbnail) {
    this.liveStreamThumbnail = liveStreamThumbnail;
    return this;
  }

   /**
    * Gets the live stream thumbnail url.
    *
    * @return the live stream thumbnail url
    */
  @ApiModelProperty(required = true, value = "")
  public LiveStreamThumbnail getLiveStreamThumbnailUrl() {
    return liveStreamThumbnail;
  }

  /**
   * Sets the live stream thumbnail url.
   *
   * @param liveStreamThumbnail the new live stream thumbnail url
   */
  public void setLiveStreamThumbnailUrl(LiveStreamThumbnail liveStreamThumbnail) {
    this.liveStreamThumbnail = liveStreamThumbnail;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    LiveStreamCreateThumbnail inlineResponse2005 = (LiveStreamCreateThumbnail) o;
    return Objects.equals(this.liveStreamThumbnail, inlineResponse2005.liveStreamThumbnail);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(liveStreamThumbnail);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class LiveStreamCreateThumbnail {\n");
    
    sb.append("    liveStreamThumbnail: ").append(toIndentedString(liveStreamThumbnail)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

