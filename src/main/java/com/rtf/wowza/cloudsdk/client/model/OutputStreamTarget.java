/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.rtf.wowza.cloudsdk.client.model.StreamTarget;
import java.io.IOException;
import java.time.OffsetDateTime;

/**
 * The Class OutputStreamTarget.
 */
@ApiModel(description = "")
public class OutputStreamTarget {
  
  /** The created at. */
  @SerializedName("created_at")
  private OffsetDateTime createdAt = null;

  /** The id. */
  @SerializedName("id")
  private String id = null;

  /** The output id. */
  @SerializedName("output_id")
  private String outputId = null;

  /** The stream target. */
  @SerializedName("stream_target")
  private StreamTarget streamTarget = null;

  /** The stream target id. */
  @SerializedName("stream_target_id")
  private String streamTargetId = null;

  /** The updated at. */
  @SerializedName("updated_at")
  private OffsetDateTime updatedAt = null;

  /** The use stream target backup url. */
  @SerializedName("use_stream_target_backup_url")
  private Boolean useStreamTargetBackupUrl = null;

  /**
   * Created at.
   *
   * @param createdAt the created at
   * @return the output stream target
   */
  public OutputStreamTarget createdAt(OffsetDateTime createdAt) {
    this.createdAt = createdAt;
    return this;
  }

   /**
    * Gets the created at.
    *
    * @return the created at
    */
  @ApiModelProperty(example = "", value = "The date and time that the output stream target was created.")
  public OffsetDateTime getCreatedAt() {
    return createdAt;
  }

  /**
   * Sets the created at.
   *
   * @param createdAt the new created at
   */
  public void setCreatedAt(OffsetDateTime createdAt) {
    this.createdAt = createdAt;
  }

  /**
   * Id.
   *
   * @param id the id
   * @return the output stream target
   */
  public OutputStreamTarget id(String id) {
    this.id = id;
    return this;
  }

   /**
    * Gets the id.
    *
    * @return the id
    */
  @ApiModelProperty(example = "", value = "The unique alphanumeric string that identifies the output stream target.")
  public String getId() {
    return id;
  }

  /**
   * Sets the id.
   *
   * @param id the new id
   */
  public void setId(String id) {
    this.id = id;
  }

  /**
   * Output id.
   *
   * @param outputId the output id
   * @return the output stream target
   */
  public OutputStreamTarget outputId(String outputId) {
    this.outputId = outputId;
    return this;
  }

   /**
    * Gets the output id.
    *
    * @return the output id
    */
  @ApiModelProperty(example = "", value = "The unique alphanumeric string that identifies the output rendition.")
  public String getOutputId() {
    return outputId;
  }

  /**
   * Sets the output id.
   *
   * @param outputId the new output id
   */
  public void setOutputId(String outputId) {
    this.outputId = outputId;
  }

  /**
   * Stream target.
   *
   * @param streamTarget the stream target
   * @return the output stream target
   */
  public OutputStreamTarget streamTarget(StreamTarget streamTarget) {
    this.streamTarget = streamTarget;
    return this;
  }

   /**
    * Gets the stream target.
    *
    * @return the stream target
    */
  @ApiModelProperty(value = "")
  public StreamTarget getStreamTarget() {
    return streamTarget;
  }

  /**
   * Sets the stream target.
   *
   * @param streamTarget the new stream target
   */
  public void setStreamTarget(StreamTarget streamTarget) {
    this.streamTarget = streamTarget;
  }

  /**
   * Stream target id.
   *
   * @param streamTargetId the stream target id
   * @return the output stream target
   */
  public OutputStreamTarget streamTargetId(String streamTargetId) {
    this.streamTargetId = streamTargetId;
    return this;
  }

   /**
    * Gets the stream target id.
    *
    * @return the stream target id
    */
  @ApiModelProperty(example = "", value = "The unique alphanumeric string that identifies the stream target.")
  public String getStreamTargetId() {
    return streamTargetId;
  }

  /**
   * Sets the stream target id.
   *
   * @param streamTargetId the new stream target id
   */
  public void setStreamTargetId(String streamTargetId) {
    this.streamTargetId = streamTargetId;
  }

  /**
   * Updated at.
   *
   * @param updatedAt the updated at
   * @return the output stream target
   */
  public OutputStreamTarget updatedAt(OffsetDateTime updatedAt) {
    this.updatedAt = updatedAt;
    return this;
  }

   /**
    * Gets the updated at.
    *
    * @return the updated at
    */
  @ApiModelProperty(example = "", value = "The date and time that the output stream target was updated.")
  public OffsetDateTime getUpdatedAt() {
    return updatedAt;
  }

  /**
   * Sets the updated at.
   *
   * @param updatedAt the new updated at
   */
  public void setUpdatedAt(OffsetDateTime updatedAt) {
    this.updatedAt = updatedAt;
  }

  /**
   * Use stream target backup url.
   *
   * @param useStreamTargetBackupUrl the use stream target backup url
   * @return the output stream target
   */
  public OutputStreamTarget useStreamTargetBackupUrl(Boolean useStreamTargetBackupUrl) {
    this.useStreamTargetBackupUrl = useStreamTargetBackupUrl;
    return this;
  }

   /**
    * Checks if is use stream target backup url.
    *
    * @return the boolean
    */
  @ApiModelProperty(value = "Specifies whether to use the stream target's primary or backup URL.")
  public Boolean isUseStreamTargetBackupUrl() {
    return useStreamTargetBackupUrl;
  }

  /**
   * Sets the use stream target backup url.
   *
   * @param useStreamTargetBackupUrl the new use stream target backup url
   */
  public void setUseStreamTargetBackupUrl(Boolean useStreamTargetBackupUrl) {
    this.useStreamTargetBackupUrl = useStreamTargetBackupUrl;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OutputStreamTarget outputStreamTarget = (OutputStreamTarget) o;
    return Objects.equals(this.createdAt, outputStreamTarget.createdAt) &&
        Objects.equals(this.id, outputStreamTarget.id) &&
        Objects.equals(this.outputId, outputStreamTarget.outputId) &&
        Objects.equals(this.streamTarget, outputStreamTarget.streamTarget) &&
        Objects.equals(this.streamTargetId, outputStreamTarget.streamTargetId) &&
        Objects.equals(this.updatedAt, outputStreamTarget.updatedAt) &&
        Objects.equals(this.useStreamTargetBackupUrl, outputStreamTarget.useStreamTargetBackupUrl);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(createdAt, id, outputId, streamTarget, streamTargetId, updatedAt, useStreamTargetBackupUrl);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class OutputStreamTarget {\n");
    
    sb.append("    createdAt: ").append(toIndentedString(createdAt)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    outputId: ").append(toIndentedString(outputId)).append("\n");
    sb.append("    streamTarget: ").append(toIndentedString(streamTarget)).append("\n");
    sb.append("    streamTargetId: ").append(toIndentedString(streamTargetId)).append("\n");
    sb.append("    updatedAt: ").append(toIndentedString(updatedAt)).append("\n");
    sb.append("    useStreamTargetBackupUrl: ").append(toIndentedString(useStreamTargetBackupUrl)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

