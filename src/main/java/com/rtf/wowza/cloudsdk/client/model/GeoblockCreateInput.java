/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.rtf.wowza.cloudsdk.client.model.Geoblock;
import java.io.IOException;

/**
 * The Class GeoblockCreateInput.
 */
@ApiModel(description = "")
public class GeoblockCreateInput {
  
  /** The geoblock. */
  @SerializedName("geoblock")
  private Geoblock geoblock = null;

  /**
   * Geoblock.
   *
   * @param geoblock the geoblock
   * @return the geoblock create input
   */
  public GeoblockCreateInput geoblock(Geoblock geoblock) {
    this.geoblock = geoblock;
    return this;
  }

   /**
    * Gets the geoblock.
    *
    * @return the geoblock
    */
  @ApiModelProperty(required = true, value = "")
  public Geoblock getGeoblock() {
    return geoblock;
  }

  /**
   * Sets the geoblock.
   *
   * @param geoblock the new geoblock
   */
  public void setGeoblock(Geoblock geoblock) {
    this.geoblock = geoblock;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    GeoblockCreateInput geoblockCreateInput = (GeoblockCreateInput) o;
    return Objects.equals(this.geoblock, geoblockCreateInput.geoblock);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(geoblock);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class GeoblockCreateInput {\n");
    
    sb.append("    geoblock: ").append(toIndentedString(geoblock)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

