/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.rtf.wowza.cloudsdk.client.model.Schedule;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class Schedules.
 */
@ApiModel(description = "")
public class Schedules {
  
  /** The schedules. */
  @SerializedName("schedules")
  private List<Schedule> schedules = new ArrayList<Schedule>();

  /**
   * Schedules.
   *
   * @param schedules the schedules
   * @return the schedules
   */
  public Schedules schedules(List<Schedule> schedules) {
    this.schedules = schedules;
    return this;
  }

  /**
   * Adds the schedules item.
   *
   * @param schedulesItem the schedules item
   * @return the schedules
   */
  public Schedules addSchedulesItem(Schedule schedulesItem) {
    this.schedules.add(schedulesItem);
    return this;
  }

   /**
    * Gets the schedules.
    *
    * @return the schedules
    */
  @ApiModelProperty(required = true, value = "")
  public List<Schedule> getSchedules() {
    return schedules;
  }

  /**
   * Sets the schedules.
   *
   * @param schedules the new schedules
   */
  public void setSchedules(List<Schedule> schedules) {
    this.schedules = schedules;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Schedules schedules = (Schedules) o;
    return Objects.equals(this.schedules, schedules.schedules);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(schedules);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Schedules {\n");
    
    sb.append("    schedules: ").append(toIndentedString(schedules)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

