/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client;

import java.util.Map;
import java.util.List;

/**
 * The Class ApiException.
 */
public class ApiException extends Exception {
    
    /** The code. */
    private int code = 0;
    
    /** The response headers. */
    private Map<String, List<String>> responseHeaders = null;
    
    /** The response body. */
    private String responseBody = null;

    /**
     * Instantiates a new api exception.
     */
    public ApiException() {}

    /**
     * Instantiates a new api exception.
     *
     * @param throwable the throwable
     */
    public ApiException(Throwable throwable) {
        super(throwable);
    }

    /**
     * Instantiates a new api exception.
     *
     * @param message the message
     */
    public ApiException(String message) {
        super(message);
    }

    /**
     * Instantiates a new api exception.
     *
     * @param message the message
     * @param throwable the throwable
     * @param code the code
     * @param responseHeaders the response headers
     * @param responseBody the response body
     */
    public ApiException(String message, Throwable throwable, int code, Map<String, List<String>> responseHeaders, String responseBody) {
        super(message, throwable);
        this.code = code;
        this.responseHeaders = responseHeaders;
        this.responseBody = responseBody;
    }

    /**
     * Instantiates a new api exception.
     *
     * @param message the message
     * @param code the code
     * @param responseHeaders the response headers
     * @param responseBody the response body
     */
    public ApiException(String message, int code, Map<String, List<String>> responseHeaders, String responseBody) {
        this(message, (Throwable) null, code, responseHeaders, responseBody);
    }

    /**
     * Instantiates a new api exception.
     *
     * @param message the message
     * @param throwable the throwable
     * @param code the code
     * @param responseHeaders the response headers
     */
    public ApiException(String message, Throwable throwable, int code, Map<String, List<String>> responseHeaders) {
        this(message, throwable, code, responseHeaders, null);
    }

    /**
     * Instantiates a new api exception.
     *
     * @param code the code
     * @param responseHeaders the response headers
     * @param responseBody the response body
     */
    public ApiException(int code, Map<String, List<String>> responseHeaders, String responseBody) {
        this((String) null, (Throwable) null, code, responseHeaders, responseBody);
    }

    /**
     * Instantiates a new api exception.
     *
     * @param code the code
     * @param message the message
     */
    public ApiException(int code, String message) {
        super(message);
        this.code = code;
    }

    /**
     * Instantiates a new api exception.
     *
     * @param code the code
     * @param message the message
     * @param responseHeaders the response headers
     * @param responseBody the response body
     */
    public ApiException(int code, String message, Map<String, List<String>> responseHeaders, String responseBody) {
        this(code, message);
        this.responseHeaders = responseHeaders;
        this.responseBody = responseBody;
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public int getCode() {
        return code;
    }

    /**
     * Gets the response headers.
     *
     * @return the response headers
     */
    public Map<String, List<String>> getResponseHeaders() {
        return responseHeaders;
    }

    /**
     * Gets the response body.
     *
     * @return the response body
     */
    public String getResponseBody() {
        return responseBody;
    }
}
