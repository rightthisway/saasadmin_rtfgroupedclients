/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * The Class SourceConnectionInformation.
 */
@ApiModel(description = "")
public class SourceConnectionInformation {
  
  /** The primaryserver. */
  @SerializedName("primary_server")
  private String primaryserver = null;

  /** The hostport. */
  @SerializedName("host_port")
  private String hostport = null;

  /** The streamname. */
  @SerializedName("stream_name")
  private String streamname = null;

  /** The disableauthentication. */
  @SerializedName("disable_authentication")
  private Boolean disableauthentication = null;
  
  /** The username. */
  @SerializedName("username")
  private String username = null;
  
  /** The password. */
  @SerializedName("password")
  private String password = null;

  /**
   * Primaryserver.
   *
   * @param primaryserver the primaryserver
   * @return the source connection information
   */
  public SourceConnectionInformation primaryserver(String primaryserver) {
    this.primaryserver = primaryserver;
    return this;
  }
  

   /**
    * Gets the primaryserver.
    *
    * @return the primaryserver
    */
   public String getPrimaryserver() {
	return primaryserver;
}


/**
 * Sets the primaryserver.
 *
 * @param primaryserver the new primaryserver
 */
public void setPrimaryserver(String primaryserver) {
	this.primaryserver = primaryserver;
}


/**
 * Hostport.
 *
 * @param hostport the hostport
 * @return the source connection information
 */
public SourceConnectionInformation hostport(String hostport) {
    this.hostport = hostport;
    return this;
  }

/**
 * Gets the hostport.
 *
 * @return the hostport
 */
public String getHostport() {
	return hostport;
}


/**
 * Sets the hostport.
 *
 * @param hostport the new hostport
 */
public void setHostport(String hostport) {
	this.hostport = hostport;
}

/**
 * Streamname.
 *
 * @param streamname the streamname
 * @return the source connection information
 */
public SourceConnectionInformation streamname(String streamname) {
    this.streamname = streamname;
    return this;
  }

/**
 * Gets the streamname.
 *
 * @return the streamname
 */
public String getStreamname() {
	return streamname;
}


/**
 * Sets the streamname.
 *
 * @param streamname the new streamname
 */
public void setStreamname(String streamname) {
	this.streamname = streamname;
}

/**
 * Disableauthentication.
 *
 * @param disableauthentication the disableauthentication
 * @return the source connection information
 */
public SourceConnectionInformation disableauthentication(Boolean disableauthentication) {
    this.disableauthentication = disableauthentication;
    return this;
  }

/**
 * Gets the disableauthentication.
 *
 * @return the disableauthentication
 */
public Boolean getDisableauthentication() {
	return disableauthentication;
}


/**
 * Sets the disableauthentication.
 *
 * @param disableauthentication the new disableauthentication
 */
public void setDisableauthentication(Boolean disableauthentication) {
	this.disableauthentication = disableauthentication;
}


/**
 * Username.
 *
 * @param username the username
 * @return the source connection information
 */
public SourceConnectionInformation username(String username) {
    this.username = username;
    return this;
  }

/**
 * Gets the username.
 *
 * @return the username
 */
public String getUsername() {
	return username;
}


/**
 * Sets the username.
 *
 * @param username the new username
 */
public void setUsername(String username) {
	this.username = username;
}

/**
 * Password.
 *
 * @param password the password
 * @return the source connection information
 */
public SourceConnectionInformation password(String password) {
    this.password = password;
    return this;
  }

/**
 * Gets the password.
 *
 * @return the password
 */
public String getPassword() {
	return password;
}


/**
 * Sets the password.
 *
 * @param password the new password
 */
public void setPassword(String password) {
	this.password = password;
}



  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    
    SourceConnectionInformation audioCodecMetric = (SourceConnectionInformation) o;
    return Objects.equals(this.primaryserver, audioCodecMetric.primaryserver) &&
        Objects.equals(this.hostport, audioCodecMetric.hostport) &&
        Objects.equals(this.streamname, audioCodecMetric.streamname) &&
        Objects.equals(this.disableauthentication, audioCodecMetric.disableauthentication) &&
        Objects.equals(this.username, audioCodecMetric.username) &&
        Objects.equals(this.password, audioCodecMetric.password) ;
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(primaryserver, hostport, streamname, disableauthentication,username,password);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SourceConnectionInformation {\n");
    
    sb.append("    primaryserver: ").append(toIndentedString(primaryserver)).append("\n");
    sb.append("    hostport: ").append(toIndentedString(hostport)).append("\n");
    sb.append("    streamname: ").append(toIndentedString(streamname)).append("\n");
    sb.append("    disableauthentication: ").append(toIndentedString(disableauthentication)).append("\n");
    sb.append("    username: ").append(toIndentedString(username)).append("\n");
    sb.append("    password: ").append(toIndentedString(password)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

