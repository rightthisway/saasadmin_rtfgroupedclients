/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.rtf.wowza.cloudsdk.client.model.PlaybackUrl;
import com.rtf.wowza.cloudsdk.client.model.StreamTargetsId;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.time.OffsetDateTime;

/**
 * The Class LiveStream.
 */
@ApiModel(description = "")
@javax.annotation.Generated(value ="com.wowza.cloudsdk.JavaCreate", date = "2019-03-02T10:45:24.077Z")
public class LiveStream {
  
  /** The aspect ratio height. */
  @SerializedName("aspect_ratio_height")
  private Integer aspectRatioHeight = null;

  /** The aspect ratio width. */
  @SerializedName("aspect_ratio_width")
  private Integer aspectRatioWidth = null;

  /**
   * The Enum BillingModeEnum.
   */
  @JsonAdapter(BillingModeEnum.Adapter.class)
  public enum BillingModeEnum {
    
    /** The pay as you go. */
    PAY_AS_YOU_GO("pay_as_you_go"),
    
    /** The twentyfour seven. */
    TWENTYFOUR_SEVEN("twentyfour_seven");

    /** The value. */
    private String value;

    /**
     * Instantiates a new billing mode enum.
     *
     * @param value the value
     */
    BillingModeEnum(String value) {
      this.value = value;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
      return value;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString() {
      return String.valueOf(value);
    }

    /**
     * From value.
     *
     * @param text the text
     * @return the billing mode enum
     */
    public static BillingModeEnum fromValue(String text) {
      for (BillingModeEnum b : BillingModeEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    /**
     * The Class Adapter.
     */
    public static class Adapter extends TypeAdapter<BillingModeEnum> {
      
      /**
       * Write.
       *
       * @param jsonWriter the json writer
       * @param enumeration the enumeration
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public void write(final JsonWriter jsonWriter, final BillingModeEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      /**
       * Read.
       *
       * @param jsonReader the json reader
       * @return the billing mode enum
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public BillingModeEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return BillingModeEnum.fromValue(String.valueOf(value));
      }
    }
  }

  /** The billing mode. */
  @SerializedName("billing_mode")
  private BillingModeEnum billingMode = null;

  /**
   * The Enum BroadcastLocationEnum.
   */
  @JsonAdapter(BroadcastLocationEnum.Adapter.class)
  public enum BroadcastLocationEnum {
    
    /** The asia pacific australia. */
    ASIA_PACIFIC_AUSTRALIA("asia_pacific_australia"),
    
    /** The asia pacific japan. */
    ASIA_PACIFIC_JAPAN("asia_pacific_japan"),
    
    /** The asia pacific singapore. */
    ASIA_PACIFIC_SINGAPORE("asia_pacific_singapore"),
    
    /** The asia pacific taiwan. */
    ASIA_PACIFIC_TAIWAN("asia_pacific_taiwan"),
    
    /** The eu belgium. */
    EU_BELGIUM("eu_belgium"),
    
    /** The eu germany. */
    EU_GERMANY("eu_germany"),
    
    /** The eu ireland. */
    EU_IRELAND("eu_ireland"),
    
    /** The south america brazil. */
    SOUTH_AMERICA_BRAZIL("south_america_brazil"),
    
    /** The us central iowa. */
    US_CENTRAL_IOWA("us_central_iowa"),
    
    /** The us east virginia. */
    US_EAST_VIRGINIA("us_east_virginia"),
    
    /** The us east s carolina. */
    US_EAST_S_CAROLINA("us_east_s_carolina"),
    
    /** The us west california. */
    US_WEST_CALIFORNIA("us_west_california"),
    
    /** The us west oregon. */
    US_WEST_OREGON("us_west_oregon");

    /** The value. */
    private String value;

    /**
     * Instantiates a new broadcast location enum.
     *
     * @param value the value
     */
    BroadcastLocationEnum(String value) {
      this.value = value;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
      return value;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString() {
      return String.valueOf(value);
    }

    /**
     * From value.
     *
     * @param text the text
     * @return the broadcast location enum
     */
    public static BroadcastLocationEnum fromValue(String text) {
      for (BroadcastLocationEnum b : BroadcastLocationEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    /**
     * The Class Adapter.
     */
    public static class Adapter extends TypeAdapter<BroadcastLocationEnum> {
      
      /**
       * Write.
       *
       * @param jsonWriter the json writer
       * @param enumeration the enumeration
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public void write(final JsonWriter jsonWriter, final BroadcastLocationEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      /**
       * Read.
       *
       * @param jsonReader the json reader
       * @return the broadcast location enum
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public BroadcastLocationEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return BroadcastLocationEnum.fromValue(String.valueOf(value));
      }
    }
  }

  /** The broadcast location. */
  @SerializedName("broadcast_location")
  private BroadcastLocationEnum broadcastLocation = null;

  /**
   * The Enum ClosedCaptionTypeEnum.
   */
  @JsonAdapter(ClosedCaptionTypeEnum.Adapter.class)
  public enum ClosedCaptionTypeEnum {
    
    /** The none. */
    NONE("none"),
    
    /** The cea. */
    CEA("cea"),
    
    /** The on text. */
    ON_TEXT("on_text"),
    
    /** The both. */
    BOTH("both");

    /** The value. */
    private String value;

    /**
     * Instantiates a new closed caption type enum.
     *
     * @param value the value
     */
    ClosedCaptionTypeEnum(String value) {
      this.value = value;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
      return value;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString() {
      return String.valueOf(value);
    }

    /**
     * From value.
     *
     * @param text the text
     * @return the closed caption type enum
     */
    public static ClosedCaptionTypeEnum fromValue(String text) {
      for (ClosedCaptionTypeEnum b : ClosedCaptionTypeEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    /**
     * The Class Adapter.
     */
    public static class Adapter extends TypeAdapter<ClosedCaptionTypeEnum> {
      
      /**
       * Write.
       *
       * @param jsonWriter the json writer
       * @param enumeration the enumeration
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public void write(final JsonWriter jsonWriter, final ClosedCaptionTypeEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      /**
       * Read.
       *
       * @param jsonReader the json reader
       * @return the closed caption type enum
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public ClosedCaptionTypeEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return ClosedCaptionTypeEnum.fromValue(String.valueOf(value));
      }
    }
  }

  /** The closed caption type. */
  @SerializedName("closed_caption_type")
  private ClosedCaptionTypeEnum closedCaptionType = null;

  /** The connection code. */
  @SerializedName("connection_code")
  private String connectionCode = null;

  /** The password. */
  @SerializedName("password")
  private String password = null;

  /** The username. */
  @SerializedName("username")
  private String username = null;

  /** The disable authentication. */
  @SerializedName("disable_authentication")
  private Boolean disableAuthentication = false;

  /** The connection code expires at. */
  @SerializedName("connection_code_expires_at")
  private OffsetDateTime connectionCodeExpiresAt = null;

  /** The created at. */
  @SerializedName("created_at")
  private OffsetDateTime createdAt = null;

  /**
   * The Enum DeliveryMethodEnum.
   */
  @JsonAdapter(DeliveryMethodEnum.Adapter.class)
  public enum DeliveryMethodEnum {
    
    /** The pull. */
    PULL("pull"),
    
    /** The cdn. */
    CDN("cdn"),
    
    /** The push. */
    PUSH("push");

    /** The value. */
    private String value;

    /**
     * Instantiates a new delivery method enum.
     *
     * @param value the value
     */
    DeliveryMethodEnum(String value) {
      this.value = value;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
      return value;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString() {
      return String.valueOf(value);
    }

    /**
     * From value.
     *
     * @param text the text
     * @return the delivery method enum
     */
    public static DeliveryMethodEnum fromValue(String text) {
      for (DeliveryMethodEnum b : DeliveryMethodEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    /**
     * The Class Adapter.
     */
    public static class Adapter extends TypeAdapter<DeliveryMethodEnum> {
      
      /**
       * Write.
       *
       * @param jsonWriter the json writer
       * @param enumeration the enumeration
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public void write(final JsonWriter jsonWriter, final DeliveryMethodEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      /**
       * Read.
       *
       * @param jsonReader the json reader
       * @return the delivery method enum
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public DeliveryMethodEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return DeliveryMethodEnum.fromValue(String.valueOf(value));
      }
    }
  }

  /** The delivery method. */
  @SerializedName("delivery_method")
  private DeliveryMethodEnum deliveryMethod = null;

  /** The delivery protocols. */
  @SerializedName("delivery_protocols")
  private List<String> deliveryProtocols = null;

  /**
   * The Enum DeliveryTypeEnum.
   */
  @JsonAdapter(DeliveryTypeEnum.Adapter.class)
  public enum DeliveryTypeEnum {
    
    /** The single bitrate. */
    SINGLE_BITRATE("single-bitrate"),
    
    /** The multi bitrate. */
    MULTI_BITRATE("multi-bitrate");

    /** The value. */
    private String value;

    /**
     * Instantiates a new delivery type enum.
     *
     * @param value the value
     */
    DeliveryTypeEnum(String value) {
      this.value = value;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
      return value;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString() {
      return String.valueOf(value);
    }

    /**
     * From value.
     *
     * @param text the text
     * @return the delivery type enum
     */
    public static DeliveryTypeEnum fromValue(String text) {
      for (DeliveryTypeEnum b : DeliveryTypeEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    /**
     * The Class Adapter.
     */
    public static class Adapter extends TypeAdapter<DeliveryTypeEnum> {
      
      /**
       * Write.
       *
       * @param jsonWriter the json writer
       * @param enumeration the enumeration
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public void write(final JsonWriter jsonWriter, final DeliveryTypeEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      /**
       * Read.
       *
       * @param jsonReader the json reader
       * @return the delivery type enum
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public DeliveryTypeEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return DeliveryTypeEnum.fromValue(String.valueOf(value));
      }
    }
  }

  /** The delivery type. */
  @SerializedName("delivery_type")
  private DeliveryTypeEnum deliveryType = null;

  /** The direct playback urls. */
  @SerializedName("direct_playback_urls")
  private HashMap<String,List<PlaybackUrl>> directPlaybackUrls = null;

  /**
   * The Enum EncoderEnum.
   */
  @JsonAdapter(EncoderEnum.Adapter.class)
  public enum EncoderEnum {
    
    /** The wowza streaming engine. */
    WOWZA_STREAMING_ENGINE("wowza_streaming_engine"),
    
    /** The wowza gocoder. */
    WOWZA_GOCODER("wowza_gocoder"),
    
    /** The media ds. */
    MEDIA_DS("media_ds"),
    
    /** The axis. */
    AXIS("axis"),
    
    /** The epiphan. */
    EPIPHAN("epiphan"),
    
    /** The hauppauge. */
    HAUPPAUGE("hauppauge"),
    
    /** The jvc. */
    JVC("jvc"),
    
    /** The live u. */
    LIVE_U("live_u"),
    
    /** The matrox. */
    MATROX("matrox"),
    
    /** The newtek tricaster. */
    NEWTEK_TRICASTER("newtek_tricaster"),
    
    /** The osprey. */
    OSPREY("osprey"),
    
    /** The sony. */
    SONY("sony"),
    
    /** The telestream wirecast. */
    TELESTREAM_WIRECAST("telestream_wirecast"),
    
    /** The teradek cube. */
    TERADEK_CUBE("teradek_cube"),
    
    /** The vmix. */
    VMIX("vmix"),
    
    /** The x split. */
    X_SPLIT("x_split"),
    
    /** The ipcamera. */
    IPCAMERA("ipcamera"),
    
    /** The other rtmp. */
    OTHER_RTMP("other_rtmp"),
    
    /** The other rtsp. */
    OTHER_RTSP("other_rtsp");

    /** The value. */
    private String value;

    /**
     * Instantiates a new encoder enum.
     *
     * @param value the value
     */
    EncoderEnum(String value) {
      this.value = value;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
      return value;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString() {
      return String.valueOf(value);
    }

    /**
     * From value.
     *
     * @param text the text
     * @return the encoder enum
     */
    public static EncoderEnum fromValue(String text) {
      for (EncoderEnum b : EncoderEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    /**
     * The Class Adapter.
     */
    public static class Adapter extends TypeAdapter<EncoderEnum> {
      
      /**
       * Write.
       *
       * @param jsonWriter the json writer
       * @param enumeration the enumeration
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public void write(final JsonWriter jsonWriter, final EncoderEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      /**
       * Read.
       *
       * @param jsonReader the json reader
       * @return the encoder enum
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public EncoderEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return EncoderEnum.fromValue(String.valueOf(value));
      }
    }
  }

  /** The encoder. */
  @SerializedName("encoder")
  private EncoderEnum encoder = null;

  /** The hosted page. */
  @SerializedName("hosted_page")
  private Boolean hostedPage = null;

  /** The hosted page description. */
  @SerializedName("hosted_page_description")
  private String hostedPageDescription = null;

  /** The hosted page logo image url. */
  @SerializedName("hosted_page_logo_image_url")
  private String hostedPageLogoImageUrl = null;

  /** The hosted page sharing icons. */
  @SerializedName("hosted_page_sharing_icons")
  private Boolean hostedPageSharingIcons = null;

  /** The hosted page title. */
  @SerializedName("hosted_page_title")
  private String hostedPageTitle = null;

  /** The hosted page url. */
  @SerializedName("hosted_page_url")
  private String hostedPageUrl = null;

  /** The id. */
  @SerializedName("id")
  private String id = null;

  /** The low latency. */
  @SerializedName("low_latency")
  private Boolean lowLatency = null;

  /** The name. */
  @SerializedName("name")
  private String name = null;

  /** The player countdown. */
  @SerializedName("player_countdown")
  private Boolean playerCountdown = null;

  /** The player countdown at. */
  @SerializedName("player_countdown_at")
  private OffsetDateTime playerCountdownAt = null;

  /** The player embed code. */
  @SerializedName("player_embed_code")
  private String playerEmbedCode = null;

  /** The player hds playback url. */
  @SerializedName("player_hds_playback_url")
  private String playerHdsPlaybackUrl = null;

  /** The player hls playback url. */
  @SerializedName("player_hls_playback_url")
  private String playerHlsPlaybackUrl = null;

  /** The player id. */
  @SerializedName("player_id")
  private String playerId = null;

  /** The player logo image url. */
  @SerializedName("player_logo_image_url")
  private String playerLogoImageUrl = null;

  /**
   * The Enum PlayerLogoPositionEnum.
   */
  @JsonAdapter(PlayerLogoPositionEnum.Adapter.class)
  public enum PlayerLogoPositionEnum {
    
    /** The top left. */
    TOP_LEFT("top-left"),
    
    /** The top right. */
    TOP_RIGHT("top-right"),
    
    /** The bottom left. */
    BOTTOM_LEFT("bottom-left"),
    
    /** The bottom right. */
    BOTTOM_RIGHT("bottom-right");

    /** The value. */
    private String value;

    /**
     * Instantiates a new player logo position enum.
     *
     * @param value the value
     */
    PlayerLogoPositionEnum(String value) {
      this.value = value;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
      return value;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString() {
      return String.valueOf(value);
    }

    /**
     * From value.
     *
     * @param text the text
     * @return the player logo position enum
     */
    public static PlayerLogoPositionEnum fromValue(String text) {
      for (PlayerLogoPositionEnum b : PlayerLogoPositionEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    /**
     * The Class Adapter.
     */
    public static class Adapter extends TypeAdapter<PlayerLogoPositionEnum> {
      
      /**
       * Write.
       *
       * @param jsonWriter the json writer
       * @param enumeration the enumeration
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public void write(final JsonWriter jsonWriter, final PlayerLogoPositionEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      /**
       * Read.
       *
       * @param jsonReader the json reader
       * @return the player logo position enum
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public PlayerLogoPositionEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return PlayerLogoPositionEnum.fromValue(String.valueOf(value));
      }
    }
  }

  /** The player logo position. */
  @SerializedName("player_logo_position")
  private PlayerLogoPositionEnum playerLogoPosition = null;

  /** The player responsive. */
  @SerializedName("player_responsive")
  private Boolean playerResponsive = null;

  /** The player type. */
  @SerializedName("player_type")
  private String playerType = null;

  /** The player video poster image url. */
  @SerializedName("player_video_poster_image_url")
  private String playerVideoPosterImageUrl = null;

  /** The player width. */
  @SerializedName("player_width")
  private Integer playerWidth = null;

  /** The recording. */
  @SerializedName("recording")
  private Boolean recording = null;

  /** The source connection information. */
  @SerializedName("source_connection_information")
  private SourceConnectionInformation sourceConnectionInformation = null;

  /** The stream source id. */
  @SerializedName("stream_source_id")
  private String streamSourceId = null;

  /** The stream targets. */
  @SerializedName("stream_targets")
  private List<StreamTargetsId> streamTargets = null;

  /**
   * The Enum TargetDeliveryProtocolEnum.
   */
  @JsonAdapter(TargetDeliveryProtocolEnum.Adapter.class)
  public enum TargetDeliveryProtocolEnum {
    
    /** The https. */
    HTTPS("hls-https"),
    
    /** The hds. */
    HDS("hls-hds");

    /** The value. */
    private String value;

    /**
     * Instantiates a new target delivery protocol enum.
     *
     * @param value the value
     */
    TargetDeliveryProtocolEnum(String value) {
      this.value = value;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
      return value;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString() {
      return String.valueOf(value);
    }

    /**
     * From value.
     *
     * @param text the text
     * @return the target delivery protocol enum
     */
    public static TargetDeliveryProtocolEnum fromValue(String text) {
      for (TargetDeliveryProtocolEnum b : TargetDeliveryProtocolEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    /**
     * The Class Adapter.
     */
    public static class Adapter extends TypeAdapter<TargetDeliveryProtocolEnum> {
      
      /**
       * Write.
       *
       * @param jsonWriter the json writer
       * @param enumeration the enumeration
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public void write(final JsonWriter jsonWriter, final TargetDeliveryProtocolEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      /**
       * Read.
       *
       * @param jsonReader the json reader
       * @return the target delivery protocol enum
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public TargetDeliveryProtocolEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return TargetDeliveryProtocolEnum.fromValue(String.valueOf(value));
      }
    }
  }

  /** The target delivery protocol. */
  @SerializedName("target_delivery_protocol")
  private TargetDeliveryProtocolEnum targetDeliveryProtocol = null;

  /**
   * The Enum TranscoderTypeEnum.
   */
  @JsonAdapter(TranscoderTypeEnum.Adapter.class)
  public enum TranscoderTypeEnum {
    
    /** The transcoded. */
    TRANSCODED("transcoded"),
    
    /** The passthrough. */
    PASSTHROUGH("passthrough");

    /** The value. */
    private String value;

    /**
     * Instantiates a new transcoder type enum.
     *
     * @param value the value
     */
    TranscoderTypeEnum(String value) {
      this.value = value;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
      return value;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString() {
      return String.valueOf(value);
    }

    /**
     * From value.
     *
     * @param text the text
     * @return the transcoder type enum
     */
    public static TranscoderTypeEnum fromValue(String text) {
      for (TranscoderTypeEnum b : TranscoderTypeEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    /**
     * The Class Adapter.
     */
    public static class Adapter extends TypeAdapter<TranscoderTypeEnum> {
      
      /**
       * Write.
       *
       * @param jsonWriter the json writer
       * @param enumeration the enumeration
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public void write(final JsonWriter jsonWriter, final TranscoderTypeEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      /**
       * Read.
       *
       * @param jsonReader the json reader
       * @return the transcoder type enum
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public TranscoderTypeEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return TranscoderTypeEnum.fromValue(String.valueOf(value));
      }
    }
  }

  /** The transcoder type. */
  @SerializedName("transcoder_type")
  private TranscoderTypeEnum transcoderType = null;

  /** The updated at. */
  @SerializedName("updated_at")
  private OffsetDateTime updatedAt = null;

  /** The use stream source. */
  @SerializedName("use_stream_source")
  private Boolean useStreamSource = null;

  /**
   * Aspect ratio height.
   *
   * @param aspectRatioHeight the aspect ratio height
   * @return the live stream
   */
  public LiveStream aspectRatioHeight(Integer aspectRatioHeight) {
    this.aspectRatioHeight = aspectRatioHeight;
    return this;
  }

   /**
    * Gets the aspect ratio height.
    *
    * @return the aspect ratio height
    */
  @ApiModelProperty(example = "", value = "The height, in pixels, of the video source. Should correspond to a widescreen (16:9) or standard (4:3) aspect ratio and be divisible by 8.")
  public Integer getAspectRatioHeight() {
    return aspectRatioHeight;
  }

  /**
   * Sets the aspect ratio height.
   *
   * @param aspectRatioHeight the new aspect ratio height
   */
  public void setAspectRatioHeight(Integer aspectRatioHeight) {
    this.aspectRatioHeight = aspectRatioHeight;
  }

  /**
   * Aspect ratio width.
   *
   * @param aspectRatioWidth the aspect ratio width
   * @return the live stream
   */
  public LiveStream aspectRatioWidth(Integer aspectRatioWidth) {
    this.aspectRatioWidth = aspectRatioWidth;
    return this;
  }

   /**
    * Gets the aspect ratio width.
    *
    * @return the aspect ratio width
    */
  @ApiModelProperty(example = "", value = "The width, in pixels, of the video source. Should correspond to a widescreen (16:9) or standard (4:3) aspect ratio and be divisible by 8.")
  public Integer getAspectRatioWidth() {
    return aspectRatioWidth;
  }

  /**
   * Sets the aspect ratio width.
   *
   * @param aspectRatioWidth the new aspect ratio width
   */
  public void setAspectRatioWidth(Integer aspectRatioWidth) {
    this.aspectRatioWidth = aspectRatioWidth;
  }

  /**
   * Billing mode.
   *
   * @param billingMode the billing mode
   * @return the live stream
   */
  public LiveStream billingMode(BillingModeEnum billingMode) {
    this.billingMode = billingMode;
    return this;
  }

   /**
    * Gets the billing mode.
    *
    * @return the billing mode
    */
  @ApiModelProperty(example = "", value = "The billing mode for the stream. The default is <strong>pay_as_you_go</strong>.")
  public BillingModeEnum getBillingMode() {
    return billingMode;
  }

  /**
   * Sets the billing mode.
   *
   * @param billingMode the new billing mode
   */
  public void setBillingMode(BillingModeEnum billingMode) {
    this.billingMode = billingMode;
  }

  /**
   * Broadcast location.
   *
   * @param broadcastLocation the broadcast location
   * @return the live stream
   */
  public LiveStream broadcastLocation(BroadcastLocationEnum broadcastLocation) {
    this.broadcastLocation = broadcastLocation;
    return this;
  }

   /**
    * Gets the broadcast location.
    *
    * @return the broadcast location
    */
  @ApiModelProperty(example = "", value = "The location of your stream. Choose a location as close as possible to your video source.")
  public BroadcastLocationEnum getBroadcastLocation() {
    return broadcastLocation;
  }

  /**
   * Sets the broadcast location.
   *
   * @param broadcastLocation the new broadcast location
   */
  public void setBroadcastLocation(BroadcastLocationEnum broadcastLocation) {
    this.broadcastLocation = broadcastLocation;
  }

  /**
   * Closed caption type.
   *
   * @param closedCaptionType the closed caption type
   * @return the live stream
   */
  public LiveStream closedCaptionType(ClosedCaptionTypeEnum closedCaptionType) {
    this.closedCaptionType = closedCaptionType;
    return this;
  }

   /**
    * Gets the closed caption type.
    *
    * @return the closed caption type
    */
  @ApiModelProperty(example = "", value = "The type of closed caption data being passed from the source. The default, <strong>none</strong>, indicates that no data is being provided. <strong>cea</strong> indicates that a CEA closed captioning data stream is being provided. <strong>on_text</strong> indicates that an onTextData closed captioning data stream is being provided. <strong>both</strong> indicates that both CEA and onTextData closed captioning data streams are being provided.")
  public ClosedCaptionTypeEnum getClosedCaptionType() {
    return closedCaptionType;
  }

  /**
   * Sets the closed caption type.
   *
   * @param closedCaptionType the new closed caption type
   */
  public void setClosedCaptionType(ClosedCaptionTypeEnum closedCaptionType) {
    this.closedCaptionType = closedCaptionType;
  }

  /**
   * Connection code.
   *
   * @param connectionCode the connection code
   * @return the live stream
   */
  public LiveStream connectionCode(String connectionCode) {
    this.connectionCode = connectionCode;
    return this;
  }

   /**
    * Gets the connection code.
    *
    * @return the connection code
    */
  @ApiModelProperty(example = "", value = "A six-character, alphanumeric string that allows certain encoders, including Wowza Streaming Engine and the Wowza GoCoder app, to connect with Wowza Streaming Cloud. The code can be used once and expires 24 hours after it's created.")
  public String getConnectionCode() {
    return connectionCode;
  }

  /**
   * Sets the connection code.
   *
   * @param connectionCode the new connection code
   */
  public void setConnectionCode(String connectionCode) {
    this.connectionCode = connectionCode;
  }

  /**
   * Gets the username.
   *
   * @return the username
   */
  public String getUsername() {
    return username;
  }

  /**
   * Sets the username.
   *
   * @param username the new username
   */
  public void setUsername(String username) {
    this.username = username;
  }

  /**
   * Gets the password.
   *
   * @return the password
   */
  public String getPassword() {
    return password;
  }

  /**
   * Sets the disable authentication.
   *
   * @param auth the new disable authentication
   */
  public void setDisableAuthentication(Boolean auth) {
    this.disableAuthentication = auth;
  }

  /**
   * Gets the disable authentication.
   *
   * @return the disable authentication
   */
  public Boolean getDisableAuthentication() {
    return disableAuthentication;
  }

  /**
   * Sets the password.
   *
   * @param password the new password
   */
  public void setPassword(String password) {
    this.password = password;
  }

  /**
   * Connection code expires at.
   *
   * @param connectionCodeExpiresAt the connection code expires at
   * @return the live stream
   */
  public LiveStream connectionCodeExpiresAt(OffsetDateTime connectionCodeExpiresAt) {
    this.connectionCodeExpiresAt = connectionCodeExpiresAt;
    return this;
  }

   /**
    * Gets the connection code expires at.
    *
    * @return the connection code expires at
    */
  @ApiModelProperty(example = "", value = "The date and time that the <em>connection_code</em> expires.")
  public OffsetDateTime getConnectionCodeExpiresAt() {
    return connectionCodeExpiresAt;
  }

  /**
   * Sets the connection code expires at.
   *
   * @param connectionCodeExpiresAt the new connection code expires at
   */
  public void setConnectionCodeExpiresAt(OffsetDateTime connectionCodeExpiresAt) {
    this.connectionCodeExpiresAt = connectionCodeExpiresAt;
  }

  /**
   * Created at.
   *
   * @param createdAt the created at
   * @return the live stream
   */
  public LiveStream createdAt(OffsetDateTime createdAt) {
    this.createdAt = createdAt;
    return this;
  }

   /**
    * Gets the created at.
    *
    * @return the created at
    */
  @ApiModelProperty(example = "", value = "The date and time that the live stream was created.")
  public OffsetDateTime getCreatedAt() {
    return createdAt;
  }

  /**
   * Sets the created at.
   *
   * @param createdAt the new created at
   */
  public void setCreatedAt(OffsetDateTime createdAt) {
    this.createdAt = createdAt;
  }

  /**
   * Delivery method.
   *
   * @param deliveryMethod the delivery method
   * @return the live stream
   */
  public LiveStream deliveryMethod(DeliveryMethodEnum deliveryMethod) {
    this.deliveryMethod = deliveryMethod;
    return this;
  }

   /**
    * Gets the delivery method.
    *
    * @return the delivery method
    */
  @ApiModelProperty(example = "", value = "The type of connection between the video source and the transcoder. The default, <strong>push</strong>, instructs the source to push the stream to the transcoder. <strong>pull</strong> instructs the transcoder to pull the video from the source. <strong>cdn</strong> uses a stream source to deliver the stream to the transcoder.")
  public DeliveryMethodEnum getDeliveryMethod() {
    return deliveryMethod;
  }

  /**
   * Sets the delivery method.
   *
   * @param deliveryMethod the new delivery method
   */
  public void setDeliveryMethod(DeliveryMethodEnum deliveryMethod) {
    this.deliveryMethod = deliveryMethod;
  }

  /**
   * Delivery protocols.
   *
   * @param deliveryProtocols the delivery protocols
   * @return the live stream
   */
  public LiveStream deliveryProtocols(List<String> deliveryProtocols) {
    this.deliveryProtocols = deliveryProtocols;
    return this;
  }

  /**
   * Adds the delivery protocols item.
   *
   * @param deliveryProtocolsItem the delivery protocols item
   * @return the live stream
   */
  public LiveStream addDeliveryProtocolsItem(String deliveryProtocolsItem) {
    if (this.deliveryProtocols == null) {
      this.deliveryProtocols = new ArrayList<String>();
    }
    this.deliveryProtocols.add(deliveryProtocolsItem);
    return this;
  }

   /**
    * Gets the delivery protocols.
    *
    * @return the delivery protocols
    */
  @ApiModelProperty(example = "\"\"", value = "An array of direct delivery protocols enabled for this live stream. By default, <strong>rtmp</strong>, <strong>rtsp</strong>, and <strong>wowz</strong> are enabled.")
  public List<String> getDeliveryProtocols() {
    return deliveryProtocols;
  }

  /**
   * Sets the delivery protocols.
   *
   * @param deliveryProtocols the new delivery protocols
   */
  public void setDeliveryProtocols(List<String> deliveryProtocols) {
    this.deliveryProtocols = deliveryProtocols;
  }

  /**
   * Delivery type.
   *
   * @param deliveryType the delivery type
   * @return the live stream
   */
  public LiveStream deliveryType(DeliveryTypeEnum deliveryType) {
    this.deliveryType = deliveryType;
    return this;
  }

   /**
    * Gets the delivery type.
    *
    * @return the delivery type
    */
  @ApiModelProperty(example = "", value = "For streams whose <em>encoder</em> is <strong>wowza_streaming_engine</strong>. The default is <strong>multi-bitrate</strong>, which means you're sending one or more bitrate renditions from Wowza Streaming Engine directly to a Wowza CDN target without transcoding in Wowza Streaming Cloud. The value <strong>single-bitrate</strong> means you're sending a single source stream to Wowza Streaming Cloud for transcoding and/or to deliver the source stream to multiple stream targets in Wowza Streaming Cloud.")
  public DeliveryTypeEnum getDeliveryType() {
    return deliveryType;
  }

  /**
   * Sets the delivery type.
   *
   * @param deliveryType the new delivery type
   */
  public void setDeliveryType(DeliveryTypeEnum deliveryType) {
    this.deliveryType = deliveryType;
  }

  /**
   * Direct playback urls.
   *
   * @param directPlaybackUrls the direct playback urls
   * @return the live stream
   */
  public LiveStream directPlaybackUrls(HashMap<String,List<PlaybackUrl>> directPlaybackUrls) {
    this.directPlaybackUrls = directPlaybackUrls;
    return this;
  }

  /**
   * Adds the direct playback urls item.
   *
   * @param name the name
   * @param directPlaybackUrlsItem the direct playback urls item
   * @return the live stream
   */
  public LiveStream addDirectPlaybackUrlsItem(String name,PlaybackUrl directPlaybackUrlsItem) {
    if (this.directPlaybackUrls == null) {
      this.directPlaybackUrls = new HashMap<String,List<PlaybackUrl>>();
    }
	if ( !this.directPlaybackUrls.containsKey(name) )
		{
		List<PlaybackUrl> newList = new ArrayList<PlaybackUrl>();
		newList.add(directPlaybackUrlsItem);
    		this.directPlaybackUrls.put(name,newList);
		}
	else
		{
		List<PlaybackUrl> current = this.directPlaybackUrls.get(name);
		current.add(directPlaybackUrlsItem);
		this.directPlaybackUrls.put(name,current);
		}
    return this;
  }

   /**
    * Gets the direct playback urls.
    *
    * @return the direct playback urls
    */
  @ApiModelProperty(example = "\"\"", value = "An array of direct playback URLs for the live stream's delivery protocols. Each protocol has a URL for the source and a URL for each output rendition.")
  public HashMap<String,List<PlaybackUrl>> getDirectPlaybackUrls() {
    return directPlaybackUrls;
  }

  /**
   * Sets the direct playback urls.
   *
   * @param directPlaybackUrls the direct playback urls
   */
  public void setDirectPlaybackUrls(HashMap<String,List<PlaybackUrl>> directPlaybackUrls) {
    this.directPlaybackUrls = directPlaybackUrls;
  }

  /**
   * Encoder.
   *
   * @param encoder the encoder
   * @return the live stream
   */
  public LiveStream encoder(EncoderEnum encoder) {
    this.encoder = encoder;
    return this;
  }

   /**
    * Gets the encoder.
    *
    * @return the encoder
    */
  @ApiModelProperty(example = "", value = "The video source for the live stream. Choose the type of camera or encoder you're using to connect to the Wowza Streaming Cloud transcoder. If your specific device isn't listed, choose <strong>ipcamera</strong>, <strong>other_rtmp</strong>, or <strong>other_rtsp</strong>.")
  public EncoderEnum getEncoder() {
    return encoder;
  }

  /**
   * Sets the encoder.
   *
   * @param encoder the new encoder
   */
  public void setEncoder(EncoderEnum encoder) {
    this.encoder = encoder;
  }

  /**
   * Hosted page.
   *
   * @param hostedPage the hosted page
   * @return the live stream
   */
  public LiveStream hostedPage(Boolean hostedPage) {
    this.hostedPage = hostedPage;
    return this;
  }

   /**
    * Checks if is hosted page.
    *
    * @return the boolean
    */
  @ApiModelProperty(value = "A web page hosted by Wowza Streaming Cloud that includes a player for the live stream. The default, <strong>true</strong>, creates a hosted page. Specify <strong>false</strong> to not create a hosted web page.")
  public Boolean isHostedPage() {
    return hostedPage;
  }

  /**
   * Sets the hosted page.
   *
   * @param hostedPage the new hosted page
   */
  public void setHostedPage(Boolean hostedPage) {
    this.hostedPage = hostedPage;
  }

  /**
   * Hosted page description.
   *
   * @param hostedPageDescription the hosted page description
   * @return the live stream
   */
  public LiveStream hostedPageDescription(String hostedPageDescription) {
    this.hostedPageDescription = hostedPageDescription;
    return this;
  }

   /**
    * Gets the hosted page description.
    *
    * @return the hosted page description
    */
  @ApiModelProperty(example = "", value = "A description that appears on the hosted page below the player. Can't include custom HTML, JavaScript, or other tags.")
  public String getHostedPageDescription() {
    return hostedPageDescription;
  }

  /**
   * Sets the hosted page description.
   *
   * @param hostedPageDescription the new hosted page description
   */
  public void setHostedPageDescription(String hostedPageDescription) {
    this.hostedPageDescription = hostedPageDescription;
  }

  /**
   * Hosted page logo image url.
   *
   * @param hostedPageLogoImageUrl the hosted page logo image url
   * @return the live stream
   */
  public LiveStream hostedPageLogoImageUrl(String hostedPageLogoImageUrl) {
    this.hostedPageLogoImageUrl = hostedPageLogoImageUrl;
    return this;
  }

   /**
    * Gets the hosted page logo image url.
    *
    * @return the hosted page logo image url
    */
  @ApiModelProperty(example = "", value = "The path to a GIF, JPEG, or PNG logo file that appears in the upper-left corner of the hosted page. Logo file must be 2.5 MB or smaller.")
  public String getHostedPageLogoImageUrl() {
    return hostedPageLogoImageUrl;
  }

  /**
   * Sets the hosted page logo image url.
   *
   * @param hostedPageLogoImageUrl the new hosted page logo image url
   */
  public void setHostedPageLogoImageUrl(String hostedPageLogoImageUrl) {
    this.hostedPageLogoImageUrl = hostedPageLogoImageUrl;
  }

  /**
   * Hosted page sharing icons.
   *
   * @param hostedPageSharingIcons the hosted page sharing icons
   * @return the live stream
   */
  public LiveStream hostedPageSharingIcons(Boolean hostedPageSharingIcons) {
    this.hostedPageSharingIcons = hostedPageSharingIcons;
    return this;
  }

   /**
    * Checks if is hosted page sharing icons.
    *
    * @return the boolean
    */
  @ApiModelProperty(value = "Icons that let viewers share the stream on Facebook, Google+, Twitter, and by email. The default, <strong>true</strong>, includes sharing icons on the hosted page. Specify <strong>false</strong> to omit sharing icons.")
  public Boolean isHostedPageSharingIcons() {
    return hostedPageSharingIcons;
  }

  /**
   * Sets the hosted page sharing icons.
   *
   * @param hostedPageSharingIcons the new hosted page sharing icons
   */
  public void setHostedPageSharingIcons(Boolean hostedPageSharingIcons) {
    this.hostedPageSharingIcons = hostedPageSharingIcons;
  }

  /**
   * Hosted page title.
   *
   * @param hostedPageTitle the hosted page title
   * @return the live stream
   */
  public LiveStream hostedPageTitle(String hostedPageTitle) {
    this.hostedPageTitle = hostedPageTitle;
    return this;
  }

   /**
    * Gets the hosted page title.
    *
    * @return the hosted page title
    */
  @ApiModelProperty(example = "", value = "A title for the page that appears above the player. Can't include custom HTML, JavaScript, or other tags.")
  public String getHostedPageTitle() {
    return hostedPageTitle;
  }

  /**
   * Sets the hosted page title.
   *
   * @param hostedPageTitle the new hosted page title
   */
  public void setHostedPageTitle(String hostedPageTitle) {
    this.hostedPageTitle = hostedPageTitle;
  }

  /**
   * Hosted page url.
   *
   * @param hostedPageUrl the hosted page url
   * @return the live stream
   */
  public LiveStream hostedPageUrl(String hostedPageUrl) {
    this.hostedPageUrl = hostedPageUrl;
    return this;
  }

   /**
    * Gets the hosted page url.
    *
    * @return the hosted page url
    */
  @ApiModelProperty(example = "", value = "The URL of the Wowza Streaming Cloud-hosted webpage that viewers can visit to watch the stream.")
  public String getHostedPageUrl() {
    return hostedPageUrl;
  }

  /**
   * Sets the hosted page url.
   *
   * @param hostedPageUrl the new hosted page url
   */
  public void setHostedPageUrl(String hostedPageUrl) {
    this.hostedPageUrl = hostedPageUrl;
  }

  /**
   * Id.
   *
   * @param id the id
   * @return the live stream
   */
  public LiveStream id(String id) {
    this.id = id;
    return this;
  }

   /**
    * Gets the id.
    *
    * @return the id
    */
  @ApiModelProperty(example = "", value = "The unique alphanumeric string that identifies the live stream.")
  public String getId() {
    return id;
  }

  /**
   * Sets the id.
   *
   * @param id the new id
   */
  public void setId(String id) {
    this.id = id;
  }

  /**
   * Low latency.
   *
   * @param lowLatency the low latency
   * @return the live stream
   */
  public LiveStream lowLatency(Boolean lowLatency) {
    this.lowLatency = lowLatency;
    return this;
  }

   /**
    * Checks if is low latency.
    *
    * @return the boolean
    */
  @ApiModelProperty(value = "For streams whose <em>target_delivery_protocol</em> is <strong>hls-https</strong>. If <strong>true</strong>, turns off incoming and sort packet buffers and delivers smaller video packets to the player, which can reduce latency as long as networks can handle the increased overhead. The default is <strong>false</strong>. <br /><br />This parameter only affects streams played over a target whose <em>type</em> is <strong>WowzaStreamTarget</strong> and whose <em>provider</em> is <strong>akamai_cupertino</strong>. It does <em>not</em> reduce latency in streams played over a hosted page and is unrelated to Wowza ultra low latency stream targets.")
  public Boolean isLowLatency() {
    return lowLatency;
  }

  /**
   * Sets the low latency.
   *
   * @param lowLatency the new low latency
   */
  public void setLowLatency(Boolean lowLatency) {
    this.lowLatency = lowLatency;
  }

  /**
   * Name.
   *
   * @param name the name
   * @return the live stream
   */
  public LiveStream name(String name) {
    this.name = name;
    return this;
  }

   /**
    * Gets the name.
    *
    * @return the name
    */
  @ApiModelProperty(example = "", value = "A descriptive name for the live stream. Maximum 200 characters.")
  public String getName() {
    return name;
  }

  /**
   * Sets the name.
   *
   * @param name the new name
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Player countdown.
   *
   * @param playerCountdown the player countdown
   * @return the live stream
   */
  public LiveStream playerCountdown(Boolean playerCountdown) {
    this.playerCountdown = playerCountdown;
    return this;
  }

   /**
    * Checks if is player countdown.
    *
    * @return the boolean
    */
  @ApiModelProperty(value = "A clock that appears in the player before the event and counts down to the start of the stream. Specify <strong>true</strong> to display the countdown clock. The default is <strong>false</strong>.")
  public Boolean isPlayerCountdown() {
    return playerCountdown;
  }

  /**
   * Sets the player countdown.
   *
   * @param playerCountdown the new player countdown
   */
  public void setPlayerCountdown(Boolean playerCountdown) {
    this.playerCountdown = playerCountdown;
  }

  /**
   * Player countdown at.
   *
   * @param playerCountdownAt the player countdown at
   * @return the live stream
   */
  public LiveStream playerCountdownAt(OffsetDateTime playerCountdownAt) {
    this.playerCountdownAt = playerCountdownAt;
    return this;
  }

   /**
    * Gets the player countdown at.
    *
    * @return the player countdown at
    */
  @ApiModelProperty(example = "", value = "The date and time that the event starts, used by the countdown clock. Specify <strong>YYYY-MM-DD HH:MM:SS</strong>, where <strong>HH</strong> is a 24-hour clock in UTC.")
  public OffsetDateTime getPlayerCountdownAt() {
    return playerCountdownAt;
  }

  /**
   * Sets the player countdown at.
   *
   * @param playerCountdownAt the new player countdown at
   */
  public void setPlayerCountdownAt(OffsetDateTime playerCountdownAt) {
    this.playerCountdownAt = playerCountdownAt;
  }

  /**
   * Player embed code.
   *
   * @param playerEmbedCode the player embed code
   * @return the live stream
   */
  public LiveStream playerEmbedCode(String playerEmbedCode) {
    this.playerEmbedCode = playerEmbedCode;
    return this;
  }

   /**
    * Gets the player embed code.
    *
    * @return the player embed code
    */
  @ApiModelProperty(example = "", value = "The HTML code that can be used in an external webpage to host the Wowza Streaming Cloud player.")
  public String getPlayerEmbedCode() {
    return playerEmbedCode;
  }

  /**
   * Sets the player embed code.
   *
   * @param playerEmbedCode the new player embed code
   */
  public void setPlayerEmbedCode(String playerEmbedCode) {
    this.playerEmbedCode = playerEmbedCode;
  }

  /**
   * Player hds playback url.
   *
   * @param playerHdsPlaybackUrl the player hds playback url
   * @return the live stream
   */
  public LiveStream playerHdsPlaybackUrl(String playerHdsPlaybackUrl) {
    this.playerHdsPlaybackUrl = playerHdsPlaybackUrl;
    return this;
  }

   /**
    * Gets the player hds playback url.
    *
    * @return the player hds playback url
    */
  @ApiModelProperty(example = "", value = "The address that can be used to configure playback of the stream using the Adobe HDS protocol.")
  public String getPlayerHdsPlaybackUrl() {
    return playerHdsPlaybackUrl;
  }

  /**
   * Sets the player hds playback url.
   *
   * @param playerHdsPlaybackUrl the new player hds playback url
   */
  public void setPlayerHdsPlaybackUrl(String playerHdsPlaybackUrl) {
    this.playerHdsPlaybackUrl = playerHdsPlaybackUrl;
  }

  /**
   * Player hls playback url.
   *
   * @param playerHlsPlaybackUrl the player hls playback url
   * @return the live stream
   */
  public LiveStream playerHlsPlaybackUrl(String playerHlsPlaybackUrl) {
    this.playerHlsPlaybackUrl = playerHlsPlaybackUrl;
    return this;
  }

   /**
    * Gets the player hls playback url.
    *
    * @return the player hls playback url
    */
  @ApiModelProperty(example = "", value = "The address that can be used to configure playback of the stream using the Apple HLS protocol.")
  public String getPlayerHlsPlaybackUrl() {
    return playerHlsPlaybackUrl;
  }

  /**
   * Sets the player hls playback url.
   *
   * @param playerHlsPlaybackUrl the new player hls playback url
   */
  public void setPlayerHlsPlaybackUrl(String playerHlsPlaybackUrl) {
    this.playerHlsPlaybackUrl = playerHlsPlaybackUrl;
  }

  /**
   * Player id.
   *
   * @param playerId the player id
   * @return the live stream
   */
  public LiveStream playerId(String playerId) {
    this.playerId = playerId;
    return this;
  }

   /**
    * Gets the player id.
    *
    * @return the player id
    */
  @ApiModelProperty(example = "", value = "The unique alphanumeric string that identifies the player.")
  public String getPlayerId() {
    return playerId;
  }

  /**
   * Sets the player id.
   *
   * @param playerId the new player id
   */
  public void setPlayerId(String playerId) {
    this.playerId = playerId;
  }

  /**
   * Player logo image url.
   *
   * @param playerLogoImageUrl the player logo image url
   * @return the live stream
   */
  public LiveStream playerLogoImageUrl(String playerLogoImageUrl) {
    this.playerLogoImageUrl = playerLogoImageUrl;
    return this;
  }

   /**
    * Gets the player logo image url.
    *
    * @return the player logo image url
    */
  @ApiModelProperty(example = "", value = "The path to a GIF, JPEG, or PNG logo file that appears partially transparent in a corner of the player throughout playback. Logo file must be 2.5 MB or smaller.")
  public String getPlayerLogoImageUrl() {
    return playerLogoImageUrl;
  }

  /**
   * Sets the player logo image url.
   *
   * @param playerLogoImageUrl the new player logo image url
   */
  public void setPlayerLogoImageUrl(String playerLogoImageUrl) {
    this.playerLogoImageUrl = playerLogoImageUrl;
  }

  /**
   * Player logo position.
   *
   * @param playerLogoPosition the player logo position
   * @return the live stream
   */
  public LiveStream playerLogoPosition(PlayerLogoPositionEnum playerLogoPosition) {
    this.playerLogoPosition = playerLogoPosition;
    return this;
  }

   /**
    * Gets the player logo position.
    *
    * @return the player logo position
    */
  @ApiModelProperty(example = "", value = "The corner of the player in which you want the player logo to appear. The default is <strong>top-left</strong>.")
  public PlayerLogoPositionEnum getPlayerLogoPosition() {
    return playerLogoPosition;
  }

  /**
   * Sets the player logo position.
   *
   * @param playerLogoPosition the new player logo position
   */
  public void setPlayerLogoPosition(PlayerLogoPositionEnum playerLogoPosition) {
    this.playerLogoPosition = playerLogoPosition;
  }

  /**
   * Player responsive.
   *
   * @param playerResponsive the player responsive
   * @return the live stream
   */
  public LiveStream playerResponsive(Boolean playerResponsive) {
    this.playerResponsive = playerResponsive;
    return this;
  }

   /**
    * Checks if is player responsive.
    *
    * @return the boolean
    */
  @ApiModelProperty(value = "A player whose size adjusts according to the device on which it's being viewed. If <strong>true</strong>, creates a responsive player. If <strong>false</strong>, specify a <em>player_width</em>.")
  public Boolean isPlayerResponsive() {
    return playerResponsive;
  }

  /**
   * Sets the player responsive.
   *
   * @param playerResponsive the new player responsive
   */
  public void setPlayerResponsive(Boolean playerResponsive) {
    this.playerResponsive = playerResponsive;
  }

  /**
   * Player type.
   *
   * @param playerType the player type
   * @return the live stream
   */
  public LiveStream playerType(String playerType) {
    this.playerType = playerType;
    return this;
  }

   /**
    * Gets the player type.
    *
    * @return the player type
    */
  @ApiModelProperty(example = "", value = "The player you want to use. Valid values are <strong>original_html5</strong>, which provides HTML5 playback and falls back to Flash on older browsers, and <strong>wowza_player</strong>, which provides HTML5 playback over Apple HLS. <strong>wowza_player</strong> requires that <strong>target_delivery_protocol</strong> be <strong>hls-https</strong> and <strong>closed_caption_type</strong> be <strong>none</strong>. The default is <strong>original_html5</strong>.")
  public String getPlayerType() {
    return playerType;
  }

  /**
   * Sets the player type.
   *
   * @param playerType the new player type
   */
  public void setPlayerType(String playerType) {
    this.playerType = playerType;
  }

  /**
   * Player video poster image url.
   *
   * @param playerVideoPosterImageUrl the player video poster image url
   * @return the live stream
   */
  public LiveStream playerVideoPosterImageUrl(String playerVideoPosterImageUrl) {
    this.playerVideoPosterImageUrl = playerVideoPosterImageUrl;
    return this;
  }

   /**
    * Gets the player video poster image url.
    *
    * @return the player video poster image url
    */
  @ApiModelProperty(example = "", value = "The path to a GIF, JPEG, or PNG poster image that appears in the player before the stream begins. Poster image files must be 2.5 MB or smaller.")
  public String getPlayerVideoPosterImageUrl() {
    return playerVideoPosterImageUrl;
  }

  /**
   * Sets the player video poster image url.
   *
   * @param playerVideoPosterImageUrl the new player video poster image url
   */
  public void setPlayerVideoPosterImageUrl(String playerVideoPosterImageUrl) {
    this.playerVideoPosterImageUrl = playerVideoPosterImageUrl;
  }

  /**
   * Player width.
   *
   * @param playerWidth the player width
   * @return the live stream
   */
  public LiveStream playerWidth(Integer playerWidth) {
    this.playerWidth = playerWidth;
    return this;
  }

   /**
    * Gets the player width.
    *
    * @return the player width
    */
  @ApiModelProperty(example = "", value = "The width, in pixels, of a fixed-size player. The default is <strong>640</strong>.")
  public Integer getPlayerWidth() {
    return playerWidth;
  }

  /**
   * Sets the player width.
   *
   * @param playerWidth the new player width
   */
  public void setPlayerWidth(Integer playerWidth) {
    this.playerWidth = playerWidth;
  }

  /**
   * Recording.
   *
   * @param recording the recording
   * @return the live stream
   */
  public LiveStream recording(Boolean recording) {
    this.recording = recording;
    return this;
  }

   /**
    * Checks if is recording.
    *
    * @return the boolean
    */
  @ApiModelProperty(value = "If <strong>true</strong>, creates a recording of the live stream. The default is <strong>false</strong>.")
  public Boolean isRecording() {
    return recording;
  }

  /**
   * Sets the recording.
   *
   * @param recording the new recording
   */
  public void setRecording(Boolean recording) {
    this.recording = recording;
  }

  /**
   * Source connection information.
   *
   * @param sourceConnectionInformation the source connection information
   * @return the live stream
   */
  public LiveStream sourceConnectionInformation(SourceConnectionInformation sourceConnectionInformation) {
    this.sourceConnectionInformation = sourceConnectionInformation;
    return this;
  }

   /**
    * Gets the source connection information.
    *
    * @return the source connection information
    */
  @ApiModelProperty(example = "\"\"", value = "Details that you can use to manually configure and connect a video source to the live stream.")
  public SourceConnectionInformation getSourceConnectionInformation() {
    return sourceConnectionInformation;
  }

  /**
   * Sets the source connection information.
   *
   * @param sourceConnectionInformation the new source connection information
   */
  public void setSourceConnectionInformation(SourceConnectionInformation sourceConnectionInformation) {
    this.sourceConnectionInformation = sourceConnectionInformation;
  }

  /**
   * Stream source id.
   *
   * @param streamSourceId the stream source id
   * @return the live stream
   */
  public LiveStream streamSourceId(String streamSourceId) {
    this.streamSourceId = streamSourceId;
    return this;
  }

   /**
    * Gets the stream source id.
    *
    * @return the stream source id
    */
  @ApiModelProperty(example = "", value = "The unique alphanumeric string that identifies the stream source, if a stream source is used.")
  public String getStreamSourceId() {
    return streamSourceId;
  }

  /**
   * Sets the stream source id.
   *
   * @param streamSourceId the new stream source id
   */
  public void setStreamSourceId(String streamSourceId) {
    this.streamSourceId = streamSourceId;
  }

  /**
   * Stream targets.
   *
   * @param streamTargets the stream targets
   * @return the live stream
   */
  public LiveStream streamTargets(List<StreamTargetsId> streamTargets) {
    this.streamTargets = streamTargets;
    return this;
  }

  /**
   * Adds the stream targets item.
   *
   * @param streamTargetsItem the stream targets item
   * @return the live stream
   */
  public LiveStream addStreamTargetsItem(StreamTargetsId streamTargetsItem) {
    if (this.streamTargets == null) {
      this.streamTargets = new ArrayList<StreamTargetsId>();
    }
    this.streamTargets.add(streamTargetsItem);
    return this;
  }

   /**
    * Gets the stream targets.
    *
    * @return the stream targets
    */
  @ApiModelProperty(example = "\"\"", value = "An array of unique alphanumeric strings that identify the stream targets used by the live stream.")
  public List<StreamTargetsId> getStreamTargets() {
    return streamTargets;
  }

  /**
   * Sets the stream targets.
   *
   * @param streamTargets the new stream targets
   */
  public void setStreamTargets(List<StreamTargetsId> streamTargets) {
    this.streamTargets = streamTargets;
  }

  /**
   * Target delivery protocol.
   *
   * @param targetDeliveryProtocol the target delivery protocol
   * @return the live stream
   */
  public LiveStream targetDeliveryProtocol(TargetDeliveryProtocolEnum targetDeliveryProtocol) {
    this.targetDeliveryProtocol = targetDeliveryProtocol;
    return this;
  }

   /**
    * Gets the target delivery protocol.
    *
    * @return the target delivery protocol
    */
  @ApiModelProperty(example = "", value = "The type of stream being delivered from Wowza Streaming Cloud. The default is <strong>hls-https</strong>.")
  public TargetDeliveryProtocolEnum getTargetDeliveryProtocol() {
    return targetDeliveryProtocol;
  }

  /**
   * Sets the target delivery protocol.
   *
   * @param targetDeliveryProtocol the new target delivery protocol
   */
  public void setTargetDeliveryProtocol(TargetDeliveryProtocolEnum targetDeliveryProtocol) {
    this.targetDeliveryProtocol = targetDeliveryProtocol;
  }

  /**
   * Transcoder type.
   *
   * @param transcoderType the transcoder type
   * @return the live stream
   */
  public LiveStream transcoderType(TranscoderTypeEnum transcoderType) {
    this.transcoderType = transcoderType;
    return this;
  }

   /**
    * Gets the transcoder type.
    *
    * @return the transcoder type
    */
  @ApiModelProperty(example = "", value = "The type of transcoder, either <strong>transcoded</strong> for streams that are transcoded into adaptive bitrate renditions or <strong>passthrough</strong> for streams that aren't processed by the transcoder. The default is <strong>transcoded</strong>.")
  public TranscoderTypeEnum getTranscoderType() {
    return transcoderType;
  }

  /**
   * Sets the transcoder type.
   *
   * @param transcoderType the new transcoder type
   */
  public void setTranscoderType(TranscoderTypeEnum transcoderType) {
    this.transcoderType = transcoderType;
  }

  /**
   * Updated at.
   *
   * @param updatedAt the updated at
   * @return the live stream
   */
  public LiveStream updatedAt(OffsetDateTime updatedAt) {
    this.updatedAt = updatedAt;
    return this;
  }

   /**
    * Gets the updated at.
    *
    * @return the updated at
    */
  @ApiModelProperty(example = "", value = "The date and time that the live stream was updated.")
  public OffsetDateTime getUpdatedAt() {
    return updatedAt;
  }

  /**
   * Sets the updated at.
   *
   * @param updatedAt the new updated at
   */
  public void setUpdatedAt(OffsetDateTime updatedAt) {
    this.updatedAt = updatedAt;
  }

  /**
   * Use stream source.
   *
   * @param useStreamSource the use stream source
   * @return the live stream
   */
  public LiveStream useStreamSource(Boolean useStreamSource) {
    this.useStreamSource = useStreamSource;
    return this;
  }

   /**
    * Checks if is use stream source.
    *
    * @return the boolean
    */
  @ApiModelProperty(value = "If <strong>true</strong>, uses a stream source to deliver the stream to Wowza Streaming Cloud. The default, <strong>false</strong>, pushes directly to Wowza Streaming Cloud.")
  public Boolean isUseStreamSource() {
    return useStreamSource;
  }

  /**
   * Sets the use stream source.
   *
   * @param useStreamSource the new use stream source
   */
  public void setUseStreamSource(Boolean useStreamSource) {
    this.useStreamSource = useStreamSource;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    LiveStream liveStream = (LiveStream) o;
    return Objects.equals(this.aspectRatioHeight, liveStream.aspectRatioHeight) &&
        Objects.equals(this.aspectRatioWidth, liveStream.aspectRatioWidth) &&
        Objects.equals(this.billingMode, liveStream.billingMode) &&
        Objects.equals(this.broadcastLocation, liveStream.broadcastLocation) &&
        Objects.equals(this.closedCaptionType, liveStream.closedCaptionType) &&
        Objects.equals(this.connectionCode, liveStream.connectionCode) &&
        Objects.equals(this.connectionCodeExpiresAt, liveStream.connectionCodeExpiresAt) &&
        Objects.equals(this.createdAt, liveStream.createdAt) &&
        Objects.equals(this.deliveryMethod, liveStream.deliveryMethod) &&
        Objects.equals(this.deliveryProtocols, liveStream.deliveryProtocols) &&
        Objects.equals(this.deliveryType, liveStream.deliveryType) &&
        Objects.equals(this.directPlaybackUrls, liveStream.directPlaybackUrls) &&
        Objects.equals(this.encoder, liveStream.encoder) &&
        Objects.equals(this.hostedPage, liveStream.hostedPage) &&
        Objects.equals(this.hostedPageDescription, liveStream.hostedPageDescription) &&
        Objects.equals(this.hostedPageLogoImageUrl, liveStream.hostedPageLogoImageUrl) &&
        Objects.equals(this.hostedPageSharingIcons, liveStream.hostedPageSharingIcons) &&
        Objects.equals(this.hostedPageTitle, liveStream.hostedPageTitle) &&
        Objects.equals(this.hostedPageUrl, liveStream.hostedPageUrl) &&
        Objects.equals(this.id, liveStream.id) &&
        Objects.equals(this.lowLatency, liveStream.lowLatency) &&
        Objects.equals(this.name, liveStream.name) &&
        Objects.equals(this.playerCountdown, liveStream.playerCountdown) &&
        Objects.equals(this.playerCountdownAt, liveStream.playerCountdownAt) &&
        Objects.equals(this.playerEmbedCode, liveStream.playerEmbedCode) &&
        Objects.equals(this.playerHdsPlaybackUrl, liveStream.playerHdsPlaybackUrl) &&
        Objects.equals(this.playerHlsPlaybackUrl, liveStream.playerHlsPlaybackUrl) &&
        Objects.equals(this.playerId, liveStream.playerId) &&
        Objects.equals(this.playerLogoImageUrl, liveStream.playerLogoImageUrl) &&
        Objects.equals(this.playerLogoPosition, liveStream.playerLogoPosition) &&
        Objects.equals(this.playerResponsive, liveStream.playerResponsive) &&
        Objects.equals(this.playerType, liveStream.playerType) &&
        Objects.equals(this.playerVideoPosterImageUrl, liveStream.playerVideoPosterImageUrl) &&
        Objects.equals(this.playerWidth, liveStream.playerWidth) &&
        Objects.equals(this.recording, liveStream.recording) &&
        Objects.equals(this.sourceConnectionInformation, liveStream.sourceConnectionInformation) &&
        Objects.equals(this.streamSourceId, liveStream.streamSourceId) &&
        Objects.equals(this.streamTargets, liveStream.streamTargets) &&
        Objects.equals(this.targetDeliveryProtocol, liveStream.targetDeliveryProtocol) &&
        Objects.equals(this.transcoderType, liveStream.transcoderType) &&
        Objects.equals(this.updatedAt, liveStream.updatedAt) &&
        Objects.equals(this.useStreamSource, liveStream.useStreamSource);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(aspectRatioHeight, aspectRatioWidth, billingMode, broadcastLocation, closedCaptionType, connectionCode, connectionCodeExpiresAt, createdAt, deliveryMethod, deliveryProtocols, deliveryType, directPlaybackUrls, encoder, hostedPage, hostedPageDescription, hostedPageLogoImageUrl, hostedPageSharingIcons, hostedPageTitle, hostedPageUrl, id, lowLatency, name, playerCountdown, playerCountdownAt, playerEmbedCode, playerHdsPlaybackUrl, playerHlsPlaybackUrl, playerId, playerLogoImageUrl, playerLogoPosition, playerResponsive, playerType, playerVideoPosterImageUrl, playerWidth, recording, sourceConnectionInformation, streamSourceId, streamTargets, targetDeliveryProtocol, transcoderType, updatedAt, useStreamSource);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class LiveStream {\n");
    
    sb.append("    aspectRatioHeight: ").append(toIndentedString(aspectRatioHeight)).append("\n");
    sb.append("    aspectRatioWidth: ").append(toIndentedString(aspectRatioWidth)).append("\n");
    sb.append("    billingMode: ").append(toIndentedString(billingMode)).append("\n");
    sb.append("    broadcastLocation: ").append(toIndentedString(broadcastLocation)).append("\n");
    sb.append("    closedCaptionType: ").append(toIndentedString(closedCaptionType)).append("\n");
    sb.append("    connectionCode: ").append(toIndentedString(connectionCode)).append("\n");
    sb.append("    connectionCodeExpiresAt: ").append(toIndentedString(connectionCodeExpiresAt)).append("\n");
    sb.append("    createdAt: ").append(toIndentedString(createdAt)).append("\n");
    sb.append("    deliveryMethod: ").append(toIndentedString(deliveryMethod)).append("\n");
    sb.append("    deliveryProtocols: ").append(toIndentedString(deliveryProtocols)).append("\n");
    sb.append("    deliveryType: ").append(toIndentedString(deliveryType)).append("\n");
    sb.append("    directPlaybackUrls: ").append(toIndentedString(directPlaybackUrls)).append("\n");
    sb.append("    encoder: ").append(toIndentedString(encoder)).append("\n");
    sb.append("    hostedPage: ").append(toIndentedString(hostedPage)).append("\n");
    sb.append("    hostedPageDescription: ").append(toIndentedString(hostedPageDescription)).append("\n");
    sb.append("    hostedPageLogoImageUrl: ").append(toIndentedString(hostedPageLogoImageUrl)).append("\n");
    sb.append("    hostedPageSharingIcons: ").append(toIndentedString(hostedPageSharingIcons)).append("\n");
    sb.append("    hostedPageTitle: ").append(toIndentedString(hostedPageTitle)).append("\n");
    sb.append("    hostedPageUrl: ").append(toIndentedString(hostedPageUrl)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    lowLatency: ").append(toIndentedString(lowLatency)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    playerCountdown: ").append(toIndentedString(playerCountdown)).append("\n");
    sb.append("    playerCountdownAt: ").append(toIndentedString(playerCountdownAt)).append("\n");
    sb.append("    playerEmbedCode: ").append(toIndentedString(playerEmbedCode)).append("\n");
    sb.append("    playerHdsPlaybackUrl: ").append(toIndentedString(playerHdsPlaybackUrl)).append("\n");
    sb.append("    playerHlsPlaybackUrl: ").append(toIndentedString(playerHlsPlaybackUrl)).append("\n");
    sb.append("    playerId: ").append(toIndentedString(playerId)).append("\n");
    sb.append("    playerLogoImageUrl: ").append(toIndentedString(playerLogoImageUrl)).append("\n");
    sb.append("    playerLogoPosition: ").append(toIndentedString(playerLogoPosition)).append("\n");
    sb.append("    playerResponsive: ").append(toIndentedString(playerResponsive)).append("\n");
    sb.append("    playerType: ").append(toIndentedString(playerType)).append("\n");
    sb.append("    playerVideoPosterImageUrl: ").append(toIndentedString(playerVideoPosterImageUrl)).append("\n");
    sb.append("    playerWidth: ").append(toIndentedString(playerWidth)).append("\n");
    sb.append("    recording: ").append(toIndentedString(recording)).append("\n");
    sb.append("    sourceConnectionInformation: ").append(toIndentedString(sourceConnectionInformation)).append("\n");
    sb.append("    streamSourceId: ").append(toIndentedString(streamSourceId)).append("\n");
    sb.append("    streamTargets: ").append(toIndentedString(streamTargets)).append("\n");
    sb.append("    targetDeliveryProtocol: ").append(toIndentedString(targetDeliveryProtocol)).append("\n");
    sb.append("    transcoderType: ").append(toIndentedString(transcoderType)).append("\n");
    sb.append("    updatedAt: ").append(toIndentedString(updatedAt)).append("\n");
    sb.append("    useStreamSource: ").append(toIndentedString(useStreamSource)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

