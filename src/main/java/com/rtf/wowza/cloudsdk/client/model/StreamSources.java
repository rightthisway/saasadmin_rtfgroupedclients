/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.rtf.wowza.cloudsdk.client.model.IndexStreamSource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class StreamSources.
 */
@ApiModel(description = "")
public class StreamSources {
  
  /** The stream sources. */
  @SerializedName("stream_sources")
  private List<IndexStreamSource> streamSources = new ArrayList<IndexStreamSource>();

  /**
   * Stream sources.
   *
   * @param streamSources the stream sources
   * @return the stream sources
   */
  public StreamSources streamSources(List<IndexStreamSource> streamSources) {
    this.streamSources = streamSources;
    return this;
  }

  /**
   * Adds the stream sources item.
   *
   * @param streamSourcesItem the stream sources item
   * @return the stream sources
   */
  public StreamSources addStreamSourcesItem(IndexStreamSource streamSourcesItem) {
    this.streamSources.add(streamSourcesItem);
    return this;
  }

   /**
    * Gets the stream sources.
    *
    * @return the stream sources
    */
  @ApiModelProperty(required = true, value = "")
  public List<IndexStreamSource> getStreamSources() {
    return streamSources;
  }

  /**
   * Sets the stream sources.
   *
   * @param streamSources the new stream sources
   */
  public void setStreamSources(List<IndexStreamSource> streamSources) {
    this.streamSources = streamSources;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    StreamSources streamSources = (StreamSources) o;
    return Objects.equals(this.streamSources, streamSources.streamSources);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(streamSources);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class StreamSources {\n");
    
    sb.append("    streamSources: ").append(toIndentedString(streamSources)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

