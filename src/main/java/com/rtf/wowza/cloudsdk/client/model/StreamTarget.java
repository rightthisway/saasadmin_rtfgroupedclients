/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.time.OffsetDateTime;

/**
 * The Class StreamTarget.
 */
@ApiModel(description = "")
public class StreamTarget {
  
  /** The backup url. */
  @SerializedName("backup_url")
  private String backupUrl = null;

  /** The connection code. */
  @SerializedName("connection_code")
  private String connectionCode = null;

  /** The connection code expires at. */
  @SerializedName("connection_code_expires_at")
  private OffsetDateTime connectionCodeExpiresAt = null;

  /** The created at. */
  @SerializedName("created_at")
  private OffsetDateTime createdAt = null;

  /** The hds playback url. */
  @SerializedName("hds_playback_url")
  private String hdsPlaybackUrl = null;

  /** The hls playback url. */
  @SerializedName("hls_playback_url")
  private String hlsPlaybackUrl = null;

  /** The id. */
  @SerializedName("id")
  private String id = null;

  /**
   * The Enum LocationEnum.
   */
  @JsonAdapter(LocationEnum.Adapter.class)
  public enum LocationEnum {
    
    /** The asia pacific australia. */
    ASIA_PACIFIC_AUSTRALIA("asia_pacific_australia"),
    
    /** The asia pacific japan. */
    ASIA_PACIFIC_JAPAN("asia_pacific_japan"),
    
    /** The asia pacific singapore. */
    ASIA_PACIFIC_SINGAPORE("asia_pacific_singapore"),
    
    /** The asia pacific taiwan. */
    ASIA_PACIFIC_TAIWAN("asia_pacific_taiwan"),
    
    /** The eu belgium. */
    EU_BELGIUM("eu_belgium"),
    
    /** The eu germany. */
    EU_GERMANY("eu_germany"),
    
    /** The eu ireland. */
    EU_IRELAND("eu_ireland"),
    
    /** The south america brazil. */
    SOUTH_AMERICA_BRAZIL("south_america_brazil"),
    
    /** The us central iowa. */
    US_CENTRAL_IOWA("us_central_iowa"),
    
    /** The us east virginia. */
    US_EAST_VIRGINIA("us_east_virginia"),
    
    /** The us west california. */
    US_WEST_CALIFORNIA("us_west_california"),
    
    /** The us west oregon. */
    US_WEST_OREGON("us_west_oregon");

    /** The value. */
    private String value;

    /**
     * Instantiates a new location enum.
     *
     * @param value the value
     */
    LocationEnum(String value) {
      this.value = value;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
      return value;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString() {
      return String.valueOf(value);
    }

    /**
     * From value.
     *
     * @param text the text
     * @return the location enum
     */
    public static LocationEnum fromValue(String text) {
      for (LocationEnum b : LocationEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    /**
     * The Class Adapter.
     */
    public static class Adapter extends TypeAdapter<LocationEnum> {
      
      /**
       * Write.
       *
       * @param jsonWriter the json writer
       * @param enumeration the enumeration
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public void write(final JsonWriter jsonWriter, final LocationEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      /**
       * Read.
       *
       * @param jsonReader the json reader
       * @return the location enum
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public LocationEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return LocationEnum.fromValue(String.valueOf(value));
      }
    }
  }

  /** The location. */
  @SerializedName("location")
  private LocationEnum location = null;

  /** The name. */
  @SerializedName("name")
  private String name = null;

  /** The password. */
  @SerializedName("password")
  private String password = null;

  /** The primary url. */
  @SerializedName("primary_url")
  private String primaryUrl = null;

  /** The provider. */
  @SerializedName("provider")
  private String provider = null;

  /** The rtmp playback url. */
  @SerializedName("rtmp_playback_url")
  private String rtmpPlaybackUrl = null;

  /** The secure ingest query param. */
  @SerializedName("secure_ingest_query_param")
  private String secureIngestQueryParam = null;

  /** The stream name. */
  @SerializedName("stream_name")
  private String streamName = null;

  /**
   * The Enum TypeEnum.
   */
  @JsonAdapter(TypeEnum.Adapter.class)
  public enum TypeEnum {
    
    /** The wowzastreamtarget. */
    WOWZASTREAMTARGET("WowzaStreamTarget"),
    
    /** The customstreamtarget. */
    CUSTOMSTREAMTARGET("CustomStreamTarget");

    /** The value. */
    private String value;

    /**
     * Instantiates a new type enum.
     *
     * @param value the value
     */
    TypeEnum(String value) {
      this.value = value;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
      return value;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString() {
      return String.valueOf(value);
    }

    /**
     * From value.
     *
     * @param text the text
     * @return the type enum
     */
    public static TypeEnum fromValue(String text) {
      for (TypeEnum b : TypeEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    /**
     * The Class Adapter.
     */
    public static class Adapter extends TypeAdapter<TypeEnum> {
      
      /**
       * Write.
       *
       * @param jsonWriter the json writer
       * @param enumeration the enumeration
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public void write(final JsonWriter jsonWriter, final TypeEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      /**
       * Read.
       *
       * @param jsonReader the json reader
       * @return the type enum
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public TypeEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return TypeEnum.fromValue(String.valueOf(value));
      }
    }
  }

  /** The type. */
  @SerializedName("type")
  private TypeEnum type = null;

  /** The updated at. */
  @SerializedName("updated_at")
  private OffsetDateTime updatedAt = null;

  /** The use cors. */
  @SerializedName("use_cors")
  private Boolean useCors = null;

  /** The use secure ingest. */
  @SerializedName("use_secure_ingest")
  private Boolean useSecureIngest = null;

  /** The username. */
  @SerializedName("username")
  private String username = null;

  /**
   * Backup url.
   *
   * @param backupUrl the backup url
   * @return the stream target
   */
  public StreamTarget backupUrl(String backupUrl) {
    this.backupUrl = backupUrl;
    return this;
  }

   /**
    * Gets the backup url.
    *
    * @return the backup url
    */
  @ApiModelProperty(example = "", value = "Only for targets whose whose <em>provider</em> is <em>not</em> <strong>akamai_cupertino</strong>. The backup RTMP ingest URL of the stream target.")
  public String getBackupUrl() {
    return backupUrl;
  }

  /**
   * Sets the backup url.
   *
   * @param backupUrl the new backup url
   */
  public void setBackupUrl(String backupUrl) {
    this.backupUrl = backupUrl;
  }

  /**
   * Connection code.
   *
   * @param connectionCode the connection code
   * @return the stream target
   */
  public StreamTarget connectionCode(String connectionCode) {
    this.connectionCode = connectionCode;
    return this;
  }

   /**
    * Gets the connection code.
    *
    * @return the connection code
    */
  @ApiModelProperty(example = "", value = "A six-character, alphanumeric string that allows select encoders, such as Wowza Streaming Engine or the Wowza GoCoder app, to send an encoded stream to a stream target in Wowza Streaming Cloud. The code can be used once and expires 24 hours after it's created.")
  public String getConnectionCode() {
    return connectionCode;
  }

  /**
   * Sets the connection code.
   *
   * @param connectionCode the new connection code
   */
  public void setConnectionCode(String connectionCode) {
    this.connectionCode = connectionCode;
  }

  /**
   * Connection code expires at.
   *
   * @param connectionCodeExpiresAt the connection code expires at
   * @return the stream target
   */
  public StreamTarget connectionCodeExpiresAt(OffsetDateTime connectionCodeExpiresAt) {
    this.connectionCodeExpiresAt = connectionCodeExpiresAt;
    return this;
  }

   /**
    * Gets the connection code expires at.
    *
    * @return the connection code expires at
    */
  @ApiModelProperty(example = "", value = "The date and time that the <em>connection_code</em> expires.")
  public OffsetDateTime getConnectionCodeExpiresAt() {
    return connectionCodeExpiresAt;
  }

  /**
   * Sets the connection code expires at.
   *
   * @param connectionCodeExpiresAt the new connection code expires at
   */
  public void setConnectionCodeExpiresAt(OffsetDateTime connectionCodeExpiresAt) {
    this.connectionCodeExpiresAt = connectionCodeExpiresAt;
  }

  /**
   * Created at.
   *
   * @param createdAt the created at
   * @return the stream target
   */
  public StreamTarget createdAt(OffsetDateTime createdAt) {
    this.createdAt = createdAt;
    return this;
  }

   /**
    * Gets the created at.
    *
    * @return the created at
    */
  @ApiModelProperty(example = "", value = "The date and time that the stream target was created.")
  public OffsetDateTime getCreatedAt() {
    return createdAt;
  }

  /**
   * Sets the created at.
   *
   * @param createdAt the new created at
   */
  public void setCreatedAt(OffsetDateTime createdAt) {
    this.createdAt = createdAt;
  }

  /**
   * Hds playback url.
   *
   * @param hdsPlaybackUrl the hds playback url
   * @return the stream target
   */
  public StreamTarget hdsPlaybackUrl(String hdsPlaybackUrl) {
    this.hdsPlaybackUrl = hdsPlaybackUrl;
    return this;
  }

   /**
    * Gets the hds playback url.
    *
    * @return the hds playback url
    */
  @ApiModelProperty(example = "", value = "The web address that the target uses to play Adobe HDS streams.")
  public String getHdsPlaybackUrl() {
    return hdsPlaybackUrl;
  }

  /**
   * Sets the hds playback url.
   *
   * @param hdsPlaybackUrl the new hds playback url
   */
  public void setHdsPlaybackUrl(String hdsPlaybackUrl) {
    this.hdsPlaybackUrl = hdsPlaybackUrl;
  }

  /**
   * Hls playback url.
   *
   * @param hlsPlaybackUrl the hls playback url
   * @return the stream target
   */
  public StreamTarget hlsPlaybackUrl(String hlsPlaybackUrl) {
    this.hlsPlaybackUrl = hlsPlaybackUrl;
    return this;
  }

   /**
    * Gets the hls playback url.
    *
    * @return the hls playback url
    */
  @ApiModelProperty(example = "", value = "Only for targets whose <em>provider</em> is <strong>akamai_cupertino</strong>. The web address that the target uses to play Apple HLS streams.")
  public String getHlsPlaybackUrl() {
    return hlsPlaybackUrl;
  }

  /**
   * Sets the hls playback url.
   *
   * @param hlsPlaybackUrl the new hls playback url
   */
  public void setHlsPlaybackUrl(String hlsPlaybackUrl) {
    this.hlsPlaybackUrl = hlsPlaybackUrl;
  }

  /**
   * Id.
   *
   * @param id the id
   * @return the stream target
   */
  public StreamTarget id(String id) {
    this.id = id;
    return this;
  }

   /**
    * Gets the id.
    *
    * @return the id
    */
  @ApiModelProperty(example = "", value = "The unique alphanumeric string that identifies the stream target.")
  public String getId() {
    return id;
  }

  /**
   * Sets the id.
   *
   * @param id the new id
   */
  public void setId(String id) {
    this.id = id;
  }

  /**
   * Location.
   *
   * @param location the location
   * @return the stream target
   */
  public StreamTarget location(LocationEnum location) {
    this.location = location;
    return this;
  }

   /**
    * Gets the location.
    *
    * @return the location
    */
  @ApiModelProperty(example = "", value = "Only for targets whose <em>type</em> is <strong>WowzaStreamTarget</strong> and <em>provider</em> is <em>not</em> <strong>akamai_cupertino</strong>. Choose a location as close as possible to your video source.")
  public LocationEnum getLocation() {
    return location;
  }

  /**
   * Sets the location.
   *
   * @param location the new location
   */
  public void setLocation(LocationEnum location) {
    this.location = location;
  }

  /**
   * Name.
   *
   * @param name the name
   * @return the stream target
   */
  public StreamTarget name(String name) {
    this.name = name;
    return this;
  }

   /**
    * Gets the name.
    *
    * @return the name
    */
  @ApiModelProperty(example = "", value = "A descriptive name for the stream target. Maximum 255 characters.")
  public String getName() {
    return name;
  }

  /**
   * Sets the name.
   *
   * @param name the new name
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Password.
   *
   * @param password the password
   * @return the stream target
   */
  public StreamTarget password(String password) {
    this.password = password;
    return this;
  }

   /**
    * Gets the password.
    *
    * @return the password
    */
  @ApiModelProperty(example = "", value = "Only for targets whose <em>type</em> is <strong>CustomStreamTarget</strong> and <em>provider</em> is <em>not</em> **akamai_cupertino**. A <em>username</em> must also be present. The password associated with the target username for RTMP authentication.")
  public String getPassword() {
    return password;
  }

  /**
   * Sets the password.
   *
   * @param password the new password
   */
  public void setPassword(String password) {
    this.password = password;
  }

  /**
   * Primary url.
   *
   * @param primaryUrl the primary url
   * @return the stream target
   */
  public StreamTarget primaryUrl(String primaryUrl) {
    this.primaryUrl = primaryUrl;
    return this;
  }

   /**
    * Gets the primary url.
    *
    * @return the primary url
    */
  @ApiModelProperty(example = "", value = "The primary ingest URL of the target.")
  public String getPrimaryUrl() {
    return primaryUrl;
  }

  /**
   * Sets the primary url.
   *
   * @param primaryUrl the new primary url
   */
  public void setPrimaryUrl(String primaryUrl) {
    this.primaryUrl = primaryUrl;
  }

  /**
   * Provider.
   *
   * @param provider the provider
   * @return the stream target
   */
  public StreamTarget provider(String provider) {
    this.provider = provider;
    return this;
  }

   /**
    * Gets the provider.
    *
    * @return the provider
    */
  @ApiModelProperty(example = "", value = "The CDN for the target. <br /><br />Required for targets whose <em>type</em> is <strong>CustomStreamTarget</strong>. Valid values for <strong>CustomStreamTarget</strong> are <strong>akamai</strong>, <strong>akamai_cupertino</strong>, <strong>akamai_rtmp</strong>, <strong>limelight</strong>, <strong>rtmp</strong>, and <strong>ustream</strong>. Values can be appended with **_mock** to use in the sandbox environment. <br /><br />Valid values for <strong>WowzaStreamTarget</strong> are <strong>akamai</strong>, <strong>akamai_cupertino</strong> (default), and <strong>akamai_legacy_rtmp</strong>.")
  public String getProvider() {
    return provider;
  }

  /**
   * Sets the provider.
   *
   * @param provider the new provider
   */
  public void setProvider(String provider) {
    this.provider = provider;
  }

  /**
   * Rtmp playback url.
   *
   * @param rtmpPlaybackUrl the rtmp playback url
   * @return the stream target
   */
  public StreamTarget rtmpPlaybackUrl(String rtmpPlaybackUrl) {
    this.rtmpPlaybackUrl = rtmpPlaybackUrl;
    return this;
  }

   /**
    * Gets the rtmp playback url.
    *
    * @return the rtmp playback url
    */
  @ApiModelProperty(example = "", value = "Only for targets whose <em>type</em> is <strong>CustomStreamTarget</strong>. The web address that the target uses to play RTMP streams.")
  public String getRtmpPlaybackUrl() {
    return rtmpPlaybackUrl;
  }

  /**
   * Sets the rtmp playback url.
   *
   * @param rtmpPlaybackUrl the new rtmp playback url
   */
  public void setRtmpPlaybackUrl(String rtmpPlaybackUrl) {
    this.rtmpPlaybackUrl = rtmpPlaybackUrl;
  }

  /**
   * Secure ingest query param.
   *
   * @param secureIngestQueryParam the secure ingest query param
   * @return the stream target
   */
  public StreamTarget secureIngestQueryParam(String secureIngestQueryParam) {
    this.secureIngestQueryParam = secureIngestQueryParam;
    return this;
  }

   /**
    * Gets the secure ingest query param.
    *
    * @return the secure ingest query param
    */
  @ApiModelProperty(example = "", value = "Only for targets whose <em>use_secure_ingest</em> is <strong>true</strong>. The query parameter needed for secure stream delivery between the transcoder and the target.")
  public String getSecureIngestQueryParam() {
    return secureIngestQueryParam;
  }

  /**
   * Sets the secure ingest query param.
   *
   * @param secureIngestQueryParam the new secure ingest query param
   */
  public void setSecureIngestQueryParam(String secureIngestQueryParam) {
    this.secureIngestQueryParam = secureIngestQueryParam;
  }

  /**
   * Stream name.
   *
   * @param streamName the stream name
   * @return the stream target
   */
  public StreamTarget streamName(String streamName) {
    this.streamName = streamName;
    return this;
  }

   /**
    * Gets the stream name.
    *
    * @return the stream name
    */
  @ApiModelProperty(example = "", value = "The name of the stream being ingested into the target.")
  public String getStreamName() {
    return streamName;
  }

  /**
   * Sets the stream name.
   *
   * @param streamName the new stream name
   */
  public void setStreamName(String streamName) {
    this.streamName = streamName;
  }

  /**
   * Type.
   *
   * @param type the type
   * @return the stream target
   */
  public StreamTarget type(TypeEnum type) {
    this.type = type;
    return this;
  }

   /**
    * Gets the type.
    *
    * @return the type
    */
  @ApiModelProperty(example = "", value = "<strong>WowzaStreamTarget</strong> is a Wowza CDN target. <strong>CustomStreamTarget</strong> (the default) is an external, third-party destination, and <strong>UltraLowLatencyStreamTarget</strong> is an ultra low latency stream target. <!--and <strong>FacebookStreamTarget</strong> (a Facebook Live target).-->")
  public TypeEnum getType() {
    return type;
  }

  /**
   * Sets the type.
   *
   * @param type the new type
   */
  public void setType(TypeEnum type) {
    this.type = type;
  }

  /**
   * Updated at.
   *
   * @param updatedAt the updated at
   * @return the stream target
   */
  public StreamTarget updatedAt(OffsetDateTime updatedAt) {
    this.updatedAt = updatedAt;
    return this;
  }

   /**
    * Gets the updated at.
    *
    * @return the updated at
    */
  @ApiModelProperty(example = "", value = "The date and time that the stream target was updated.")
  public OffsetDateTime getUpdatedAt() {
    return updatedAt;
  }

  /**
   * Sets the updated at.
   *
   * @param updatedAt the new updated at
   */
  public void setUpdatedAt(OffsetDateTime updatedAt) {
    this.updatedAt = updatedAt;
  }

  /**
   * Use cors.
   *
   * @param useCors the use cors
   * @return the stream target
   */
  public StreamTarget useCors(Boolean useCors) {
    this.useCors = useCors;
    return this;
  }

   /**
    * Checks if is use cors.
    *
    * @return the boolean
    */
  @ApiModelProperty(value = "Only for targets whose <em>type</em> is <strong>WowzaStreamTarget</strong> and <em>provider</em> is <strong>akamai_cupertino</strong>. CORS, or cross-origin resource sharing, allows streams to be sent to providers such as Peer5, Viblast, and Streamroot, which implement a peer-to-peer grid delivery system.")
  public Boolean isUseCors() {
    return useCors;
  }

  /**
   * Sets the use cors.
   *
   * @param useCors the new use cors
   */
  public void setUseCors(Boolean useCors) {
    this.useCors = useCors;
  }

  /**
   * Use secure ingest.
   *
   * @param useSecureIngest the use secure ingest
   * @return the stream target
   */
  public StreamTarget useSecureIngest(Boolean useSecureIngest) {
    this.useSecureIngest = useSecureIngest;
    return this;
  }

   /**
    * Checks if is use secure ingest.
    *
    * @return the boolean
    */
  @ApiModelProperty(value = "Only for targets whose <em>type</em> is <strong>WowzaStreamTarget</strong> and <em>provider</em> is <strong>akamai_cupertino</strong>. If <strong>true</strong>, generates a <em>secure_ingest_query_param</em> to securely deliver the stream from the transcoder to the provider.")
  public Boolean isUseSecureIngest() {
    return useSecureIngest;
  }

  /**
   * Sets the use secure ingest.
   *
   * @param useSecureIngest the new use secure ingest
   */
  public void setUseSecureIngest(Boolean useSecureIngest) {
    this.useSecureIngest = useSecureIngest;
  }

  /**
   * Username.
   *
   * @param username the username
   * @return the stream target
   */
  public StreamTarget username(String username) {
    this.username = username;
    return this;
  }

   /**
    * Gets the username.
    *
    * @return the username
    */
  @ApiModelProperty(example = "", value = "Only for targets whose <em>type</em> is <strong>CustomStreamTarget</strong> and <em>provider</em> is <em>not</em> **akamai_cupertino**. The username or ID that the target uses for RTMP authentication.")
  public String getUsername() {
    return username;
  }

  /**
   * Sets the username.
   *
   * @param username the new username
   */
  public void setUsername(String username) {
    this.username = username;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    StreamTarget streamTarget = (StreamTarget) o;
    return Objects.equals(this.backupUrl, streamTarget.backupUrl) &&
        Objects.equals(this.connectionCode, streamTarget.connectionCode) &&
        Objects.equals(this.connectionCodeExpiresAt, streamTarget.connectionCodeExpiresAt) &&
        Objects.equals(this.createdAt, streamTarget.createdAt) &&
        Objects.equals(this.hdsPlaybackUrl, streamTarget.hdsPlaybackUrl) &&
        Objects.equals(this.hlsPlaybackUrl, streamTarget.hlsPlaybackUrl) &&
        Objects.equals(this.id, streamTarget.id) &&
        Objects.equals(this.location, streamTarget.location) &&
        Objects.equals(this.name, streamTarget.name) &&
        Objects.equals(this.password, streamTarget.password) &&
        Objects.equals(this.primaryUrl, streamTarget.primaryUrl) &&
        Objects.equals(this.provider, streamTarget.provider) &&
        Objects.equals(this.rtmpPlaybackUrl, streamTarget.rtmpPlaybackUrl) &&
        Objects.equals(this.secureIngestQueryParam, streamTarget.secureIngestQueryParam) &&
        Objects.equals(this.streamName, streamTarget.streamName) &&
        Objects.equals(this.type, streamTarget.type) &&
        Objects.equals(this.updatedAt, streamTarget.updatedAt) &&
        Objects.equals(this.useCors, streamTarget.useCors) &&
        Objects.equals(this.useSecureIngest, streamTarget.useSecureIngest) &&
        Objects.equals(this.username, streamTarget.username);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(backupUrl, connectionCode, connectionCodeExpiresAt, createdAt, hdsPlaybackUrl, hlsPlaybackUrl, id, location, name, password, primaryUrl, provider, rtmpPlaybackUrl, secureIngestQueryParam, streamName, type, updatedAt, useCors, useSecureIngest, username);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class StreamTarget {\n");
    
    sb.append("    backupUrl: ").append(toIndentedString(backupUrl)).append("\n");
    sb.append("    connectionCode: ").append(toIndentedString(connectionCode)).append("\n");
    sb.append("    connectionCodeExpiresAt: ").append(toIndentedString(connectionCodeExpiresAt)).append("\n");
    sb.append("    createdAt: ").append(toIndentedString(createdAt)).append("\n");
    sb.append("    hdsPlaybackUrl: ").append(toIndentedString(hdsPlaybackUrl)).append("\n");
    sb.append("    hlsPlaybackUrl: ").append(toIndentedString(hlsPlaybackUrl)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    location: ").append(toIndentedString(location)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    password: ").append(toIndentedString(password)).append("\n");
    sb.append("    primaryUrl: ").append(toIndentedString(primaryUrl)).append("\n");
    sb.append("    provider: ").append(toIndentedString(provider)).append("\n");
    sb.append("    rtmpPlaybackUrl: ").append(toIndentedString(rtmpPlaybackUrl)).append("\n");
    sb.append("    secureIngestQueryParam: ").append(toIndentedString(secureIngestQueryParam)).append("\n");
    sb.append("    streamName: ").append(toIndentedString(streamName)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    updatedAt: ").append(toIndentedString(updatedAt)).append("\n");
    sb.append("    useCors: ").append(toIndentedString(useCors)).append("\n");
    sb.append("    useSecureIngest: ").append(toIndentedString(useSecureIngest)).append("\n");
    sb.append("    username: ").append(toIndentedString(username)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

