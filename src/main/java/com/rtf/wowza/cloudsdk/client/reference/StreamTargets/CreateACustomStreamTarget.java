 /*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/
package com.rtf.wowza.cloudsdk.client.reference.StreamTargets;

import com.rtf.wowza.cloudsdk.client.*;
import com.rtf.wowza.cloudsdk.client.api.*;
import com.rtf.wowza.cloudsdk.client.model.*;
import com.rtf.wowza.cloudsdk.client.auth.*;

import java.util.List;

/**
 * The Class CreateACustomStreamTarget.
 */
public class CreateACustomStreamTarget {

    /**
     * The main method.
     *
     * @param args the arguments
     */
    public static void main(String[] args) {

        ApiClient defaultClient = Configuration.getDefaultApiClient();

        ApiKeyAuth wscaccesskey = (ApiKeyAuth)defaultClient.getAuthentication("wsc-access-key");
        wscaccesskey.setApiKey("YOUR API KEY");

        ApiKeyAuth wscapikey = (ApiKeyAuth)defaultClient.getAuthentication("wsc-api-key");
        wscapikey.setApiKey("YOUR API KEY");

	StreamTargetsApi apiInstance = new StreamTargetsApi();
        StreamTargetCustom targetCustom = new StreamTargetCustom();
        targetCustom.setName("Java SDK custom stream target");
        targetCustom.setPrimaryUrl("rtmp://mydomainofsomeusetome.net/pathrequired");
        targetCustom.setProvider("rtmp");
        targetCustom.setStreamName("12345678");
	targetCustom.setUsername("javaSDKUsername");
	targetCustom.setPassword("javaSDKPassword");


	try {
		StreamTargetCustom result = apiInstance.createCustomStreamTarget(targetCustom);
		System.out.println(result);
	} catch (ApiException e) {
	    System.err.println("Exception when calling StreamTargetsApi#createCustomStreamTarget");
	    e.printStackTrace();
		}
	}
}
