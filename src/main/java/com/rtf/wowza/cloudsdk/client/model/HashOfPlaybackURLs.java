/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * The Class HashOfPlaybackURLs.
 */
@ApiModel(description = "A hash of **hls**, **wowz**, and **ws** URLs that can be used by the player.")
public class HashOfPlaybackURLs {
  
  /** The hls. */
  @SerializedName("hls")
  private String hls = null;

  /** The wowz. */
  @SerializedName("wowz")
  private String wowz = null;

  /** The ws. */
  @SerializedName("ws")
  private String ws = null;

  /**
   * Hls.
   *
   * @param hls the hls
   * @return the hash of playback UR ls
   */
  public HashOfPlaybackURLs hls(String hls) {
    this.hls = hls;
    return this;
  }

   /**
    * Gets the hls.
    *
    * @return the hls
    */
  @ApiModelProperty(example = "https://wowzasubdomain-i.akamaihd.net/hls/live/268548/0P1q1S0lVTE5MQmZVRWM1UXB0Z3V5e3b/playlist.m3u8", value = "The web address that the ultra low latency target can use to play the Apple HLS stream.")
  public String getHls() {
    return hls;
  }

  /**
   * Sets the hls.
   *
   * @param hls the new hls
   */
  public void setHls(String hls) {
    this.hls = hls;
  }

  /**
   * Wowz.
   *
   * @param wowz the wowz
   * @return the hash of playback UR ls
   */
  public HashOfPlaybackURLs wowz(String wowz) {
    this.wowz = wowz;
    return this;
  }

   /**
    * Gets the wowz.
    *
    * @return the wowz
    */
  @ApiModelProperty(example = "wowz://edge-subdomain.cdn.wowza.com/live/_definst_/0P1q1bGo4YmR1bGNKWW9nWVVodHJ6010", value = "The **wowz** and **wowzs** web addresses that the ultra low latency target can use to play WOWZ streams.")
  public String getWowz() {
    return wowz;
  }

  /**
   * Sets the wowz.
   *
   * @param wowz the new wowz
   */
  public void setWowz(String wowz) {
    this.wowz = wowz;
  }

  /**
   * Ws.
   *
   * @param ws the ws
   * @return the hash of playback UR ls
   */
  public HashOfPlaybackURLs ws(String ws) {
    this.ws = ws;
    return this;
  }

   /**
    * Gets the ws.
    *
    * @return the ws
    */
  @ApiModelProperty(example = "ws://edge-subdomain.cdn.wowza.com/live/_definst_/0P1q1bGo4YmR1bGNKWW9nWVVodHJ6010", value = "The **ws** and **wss** web addresses that the ultra low latency target can use to play the WebSocket stream.")
  public String getWs() {
    return ws;
  }

  /**
   * Sets the ws.
   *
   * @param ws the new ws
   */
  public void setWs(String ws) {
    this.ws = ws;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    HashOfPlaybackURLs hashOfPlaybackURLs = (HashOfPlaybackURLs) o;
    return Objects.equals(this.hls, hashOfPlaybackURLs.hls) &&
        Objects.equals(this.wowz, hashOfPlaybackURLs.wowz) &&
        Objects.equals(this.ws, hashOfPlaybackURLs.ws);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(hls, wowz, ws);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class HashOfPlaybackURLs {\n");
    
    sb.append("    hls: ").append(toIndentedString(hls)).append("\n");
    sb.append("    wowz: ").append(toIndentedString(wowz)).append("\n");
    sb.append("    ws: ").append(toIndentedString(ws)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

