/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * The Class TheNameOfTheProtocol.
 */
@ApiModel(description = "")
public class TheNameOfTheProtocol {
  
  /** The name. */
  @SerializedName("name")
  private String name = null;

  /** The percentage viewers. */
  @SerializedName("percentage_viewers")
  private Integer percentageViewers = null;

  /** The percentage viewing time. */
  @SerializedName("percentage_viewing_time")
  private Integer percentageViewingTime = null;

  /** The seconds avg viewing time. */
  @SerializedName("seconds_avg_viewing_time")
  private Integer secondsAvgViewingTime = null;

  /** The seconds total viewing time. */
  @SerializedName("seconds_total_viewing_time")
  private Integer secondsTotalViewingTime = null;

  /** The total unique viewers. */
  @SerializedName("total_unique_viewers")
  private Integer totalUniqueViewers = null;

  /**
   * Name.
   *
   * @param name the name
   * @return the the name of the protocol
   */
  public TheNameOfTheProtocol name(String name) {
    this.name = name;
    return this;
  }

   /**
    * Gets the name.
    *
    * @return the name
    */
  @ApiModelProperty(example = "", value = "The name of the protocol.")
  public String getName() {
    return name;
  }

  /**
   * Sets the name.
   *
   * @param name the new name
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Percentage viewers.
   *
   * @param percentageViewers the percentage viewers
   * @return the the name of the protocol
   */
  public TheNameOfTheProtocol percentageViewers(Integer percentageViewers) {
    this.percentageViewers = percentageViewers;
    return this;
  }

   /**
    * Gets the percentage viewers.
    *
    * @return the percentage viewers
    */
  @ApiModelProperty(example = "", value = "Total percentage of requests to play content (<strong>100</strong>).")
  public Integer getPercentageViewers() {
    return percentageViewers;
  }

  /**
   * Sets the percentage viewers.
   *
   * @param percentageViewers the new percentage viewers
   */
  public void setPercentageViewers(Integer percentageViewers) {
    this.percentageViewers = percentageViewers;
  }

  /**
   * Percentage viewing time.
   *
   * @param percentageViewingTime the percentage viewing time
   * @return the the name of the protocol
   */
  public TheNameOfTheProtocol percentageViewingTime(Integer percentageViewingTime) {
    this.percentageViewingTime = percentageViewingTime;
    return this;
  }

   /**
    * Gets the percentage viewing time.
    *
    * @return the percentage viewing time
    */
  @ApiModelProperty(example = "", value = "The percentage of total time that the protocol or rendition was played. Always <strong>100</strong> for <em>stream_target</em>.")
  public Integer getPercentageViewingTime() {
    return percentageViewingTime;
  }

  /**
   * Sets the percentage viewing time.
   *
   * @param percentageViewingTime the new percentage viewing time
   */
  public void setPercentageViewingTime(Integer percentageViewingTime) {
    this.percentageViewingTime = percentageViewingTime;
  }

  /**
   * Seconds avg viewing time.
   *
   * @param secondsAvgViewingTime the seconds avg viewing time
   * @return the the name of the protocol
   */
  public TheNameOfTheProtocol secondsAvgViewingTime(Integer secondsAvgViewingTime) {
    this.secondsAvgViewingTime = secondsAvgViewingTime;
    return this;
  }

   /**
    * Gets the seconds avg viewing time.
    *
    * @return the seconds avg viewing time
    */
  @ApiModelProperty(example = "", value = "The average length of time, in seconds, that the stream was played at the target.")
  public Integer getSecondsAvgViewingTime() {
    return secondsAvgViewingTime;
  }

  /**
   * Sets the seconds avg viewing time.
   *
   * @param secondsAvgViewingTime the new seconds avg viewing time
   */
  public void setSecondsAvgViewingTime(Integer secondsAvgViewingTime) {
    this.secondsAvgViewingTime = secondsAvgViewingTime;
  }

  /**
   * Seconds total viewing time.
   *
   * @param secondsTotalViewingTime the seconds total viewing time
   * @return the the name of the protocol
   */
  public TheNameOfTheProtocol secondsTotalViewingTime(Integer secondsTotalViewingTime) {
    this.secondsTotalViewingTime = secondsTotalViewingTime;
    return this;
  }

   /**
    * Gets the seconds total viewing time.
    *
    * @return the seconds total viewing time
    */
  @ApiModelProperty(example = "", value = "The total length of time, in seconds, that the stream was played at the target. May be longer than the duration of the stream.")
  public Integer getSecondsTotalViewingTime() {
    return secondsTotalViewingTime;
  }

  /**
   * Sets the seconds total viewing time.
   *
   * @param secondsTotalViewingTime the new seconds total viewing time
   */
  public void setSecondsTotalViewingTime(Integer secondsTotalViewingTime) {
    this.secondsTotalViewingTime = secondsTotalViewingTime;
  }

  /**
   * Total unique viewers.
   *
   * @param totalUniqueViewers the total unique viewers
   * @return the the name of the protocol
   */
  public TheNameOfTheProtocol totalUniqueViewers(Integer totalUniqueViewers) {
    this.totalUniqueViewers = totalUniqueViewers;
    return this;
  }

   /**
    * Gets the total unique viewers.
    *
    * @return the total unique viewers
    */
  @ApiModelProperty(example = "", value = "The total number of requests to download at least one chunk of the stream at the target (for HTTP streams) or requests to connect to and play the stream (for ultra low latency streams). A unique viewer is a single IP address; multiple users that share the same IP address are counted once.")
  public Integer getTotalUniqueViewers() {
    return totalUniqueViewers;
  }

  /**
   * Sets the total unique viewers.
   *
   * @param totalUniqueViewers the new total unique viewers
   */
  public void setTotalUniqueViewers(Integer totalUniqueViewers) {
    this.totalUniqueViewers = totalUniqueViewers;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TheNameOfTheProtocol theNameOfTheProtocol = (TheNameOfTheProtocol) o;
    return Objects.equals(this.name, theNameOfTheProtocol.name) &&
        Objects.equals(this.percentageViewers, theNameOfTheProtocol.percentageViewers) &&
        Objects.equals(this.percentageViewingTime, theNameOfTheProtocol.percentageViewingTime) &&
        Objects.equals(this.secondsAvgViewingTime, theNameOfTheProtocol.secondsAvgViewingTime) &&
        Objects.equals(this.secondsTotalViewingTime, theNameOfTheProtocol.secondsTotalViewingTime) &&
        Objects.equals(this.totalUniqueViewers, theNameOfTheProtocol.totalUniqueViewers);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(name, percentageViewers, percentageViewingTime, secondsAvgViewingTime, secondsTotalViewingTime, totalUniqueViewers);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TheNameOfTheProtocol {\n");
    
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    percentageViewers: ").append(toIndentedString(percentageViewers)).append("\n");
    sb.append("    percentageViewingTime: ").append(toIndentedString(percentageViewingTime)).append("\n");
    sb.append("    secondsAvgViewingTime: ").append(toIndentedString(secondsAvgViewingTime)).append("\n");
    sb.append("    secondsTotalViewingTime: ").append(toIndentedString(secondsTotalViewingTime)).append("\n");
    sb.append("    totalUniqueViewers: ").append(toIndentedString(totalUniqueViewers)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

