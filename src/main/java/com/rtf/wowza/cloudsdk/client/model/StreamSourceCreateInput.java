/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.rtf.wowza.cloudsdk.client.model.StreamSource;
import java.io.IOException;

/**
 * The Class StreamSourceCreateInput.
 */
@ApiModel(description = "")
public class StreamSourceCreateInput {
  
  /** The stream source. */
  @SerializedName("stream_source")
  private StreamSource streamSource = null;

  /**
   * Stream source.
   *
   * @param streamSource the stream source
   * @return the stream source create input
   */
  public StreamSourceCreateInput streamSource(StreamSource streamSource) {
    this.streamSource = streamSource;
    return this;
  }

   /**
    * Gets the stream source.
    *
    * @return the stream source
    */
  @ApiModelProperty(required = true, value = "")
  public StreamSource getStreamSource() {
    return streamSource;
  }

  /**
   * Sets the stream source.
   *
   * @param streamSource the new stream source
   */
  public void setStreamSource(StreamSource streamSource) {
    this.streamSource = streamSource;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    StreamSourceCreateInput streamSourceCreateInput = (StreamSourceCreateInput) o;
    return Objects.equals(this.streamSource, streamSourceCreateInput.streamSource);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(streamSource);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class StreamSourceCreateInput {\n");
    
    sb.append("    streamSource: ").append(toIndentedString(streamSource)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

