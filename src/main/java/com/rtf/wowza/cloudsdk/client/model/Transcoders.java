/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.rtf.wowza.cloudsdk.client.model.IndexTranscoder;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class Transcoders.
 */
@ApiModel(description = "")
public class Transcoders {
  
  /** The transcoders. */
  @SerializedName("transcoders")
  private List<IndexTranscoder> transcoders = new ArrayList<IndexTranscoder>();

  /**
   * Transcoders.
   *
   * @param transcoders the transcoders
   * @return the transcoders
   */
  public Transcoders transcoders(List<IndexTranscoder> transcoders) {
    this.transcoders = transcoders;
    return this;
  }

  /**
   * Adds the transcoders item.
   *
   * @param transcodersItem the transcoders item
   * @return the transcoders
   */
  public Transcoders addTranscodersItem(IndexTranscoder transcodersItem) {
    this.transcoders.add(transcodersItem);
    return this;
  }

   /**
    * Gets the transcoders.
    *
    * @return the transcoders
    */
  @ApiModelProperty(required = true, value = "")
  public List<IndexTranscoder> getTranscoders() {
    return transcoders;
  }

  /**
   * Sets the transcoders.
   *
   * @param transcoders the new transcoders
   */
  public void setTranscoders(List<IndexTranscoder> transcoders) {
    this.transcoders = transcoders;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Transcoders transcoders = (Transcoders) o;
    return Objects.equals(this.transcoders, transcoders.transcoders);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(transcoders);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Transcoders {\n");
    
    sb.append("    transcoders: ").append(toIndentedString(transcoders)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

