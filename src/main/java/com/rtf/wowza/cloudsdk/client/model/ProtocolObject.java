/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.rtf.wowza.cloudsdk.client.model.TheNameOfTheProtocol;
import java.io.IOException;

/**
 * The Class ProtocolObject.
 */
public class ProtocolObject {
  
  /** The protocol name. */
  @SerializedName("protocol_name")
  private TheNameOfTheProtocol protocolName = null;

  /**
   * Protocol name.
   *
   * @param protocolName the protocol name
   * @return the protocol object
   */
  public ProtocolObject protocolName(TheNameOfTheProtocol protocolName) {
    this.protocolName = protocolName;
    return this;
  }

   /**
    * Gets the protocol name.
    *
    * @return the protocol name
    */
  @ApiModelProperty(value = "")
  public TheNameOfTheProtocol getProtocolName() {
    return protocolName;
  }

  /**
   * Sets the protocol name.
   *
   * @param protocolName the new protocol name
   */
  public void setProtocolName(TheNameOfTheProtocol protocolName) {
    this.protocolName = protocolName;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ProtocolObject protocolObject = (ProtocolObject) o;
    return Objects.equals(this.protocolName, protocolObject.protocolName);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(protocolName);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ProtocolObject {\n");
    
    sb.append("    protocolName: ").append(toIndentedString(protocolName)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

