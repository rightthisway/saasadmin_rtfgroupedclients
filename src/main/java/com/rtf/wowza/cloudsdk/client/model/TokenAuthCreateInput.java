/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.rtf.wowza.cloudsdk.client.model.TokenAuth;
import java.io.IOException;

/**
 * The Class TokenAuthCreateInput.
 */
@ApiModel(description = "")
public class TokenAuthCreateInput {
  
  /** The token auth. */
  @SerializedName("token_auth")
  private TokenAuth tokenAuth = null;

  /**
   * Token auth.
   *
   * @param tokenAuth the token auth
   * @return the token auth create input
   */
  public TokenAuthCreateInput tokenAuth(TokenAuth tokenAuth) {
    this.tokenAuth = tokenAuth;
    return this;
  }

   /**
    * Gets the token auth.
    *
    * @return the token auth
    */
  @ApiModelProperty(required = true, value = "")
  public TokenAuth getTokenAuth() {
    return tokenAuth;
  }

  /**
   * Sets the token auth.
   *
   * @param tokenAuth the new token auth
   */
  public void setTokenAuth(TokenAuth tokenAuth) {
    this.tokenAuth = tokenAuth;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TokenAuthCreateInput tokenAuthCreateInput = (TokenAuthCreateInput) o;
    return Objects.equals(this.tokenAuth, tokenAuthCreateInput.tokenAuth);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(tokenAuth);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TokenAuthCreateInput {\n");
    
    sb.append("    tokenAuth: ").append(toIndentedString(tokenAuth)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

