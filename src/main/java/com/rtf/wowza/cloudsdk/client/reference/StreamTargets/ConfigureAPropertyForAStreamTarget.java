 /*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/
package com.rtf.wowza.cloudsdk.client.reference.StreamTargets;

import com.rtf.wowza.cloudsdk.client.*;
import com.rtf.wowza.cloudsdk.client.api.*;
import com.rtf.wowza.cloudsdk.client.model.*;
import com.rtf.wowza.cloudsdk.client.auth.*;

import java.util.List;

/**
 * The Class ConfigureAPropertyForAStreamTarget.
 */
public class ConfigureAPropertyForAStreamTarget {

    /**
     * The main method.
     *
     * @param args the arguments
     * @throws Exception the exception
     */
    public static void main(String[] args) throws Exception {
    	
    	String streamTargetId = "xxn3133j";
    	
    	/*StreamTargetProperty newProp = new StreamTargetProperty();
            newProp.setKey(StreamTargetProperty.KeyEnum.CHUNKSIZE);
            newProp.setSection(StreamTargetProperty.SectionEnum.HLS);
            newProp.setValue("2");
            configureAPropertyForAStreamTarget(streamTargetId, newProp);
            */
    	StreamTargetProperty newProp = new StreamTargetProperty();
        newProp.setKey(StreamTargetProperty.KeyEnum.PLAYLISTSECONDS);
        newProp.setSection(StreamTargetProperty.SectionEnum.PLAYLIST);
        newProp.setValue("6");
        configureAPropertyForAStreamTarget(streamTargetId, newProp);
        
    }
    
    /**
     * Configure A property for A stream target.
     *
     * @param streamTargetId the stream target id
     * @param newProp the new prop
     * @return the stream target property
     * @throws Exception the exception
     */
    public static StreamTargetProperty configureAPropertyForAStreamTarget(String streamTargetId,StreamTargetProperty newProp) throws Exception {

        ApiClient defaultClient = Configuration.getDefaultApiClient();

        ApiKeyAuth wscaccesskey = (ApiKeyAuth)defaultClient.getAuthentication("wsc-access-key");
        wscaccesskey.setApiKey(defaultClient.wscaccesskey);

        ApiKeyAuth wscapikey = (ApiKeyAuth)defaultClient.getAuthentication("wsc-api-key");
        wscapikey.setApiKey(defaultClient.wscapikey);

	StreamTargetsApi apiInstance = new StreamTargetsApi();
	
 
	try {
	    StreamTargetProperty result = apiInstance.createStreamTargetProperty(streamTargetId, newProp);
	    System.out.println(result);
	    return result;
	} catch (ApiException e) {
	    System.err.println("Exception when calling StreamTargetsApi#createStreamTargetProperty");
            System.err.println("Exception when calling StreamTargetsApi#Code:"+e.getCode());
            System.err.println("Exception when calling StreamTargetsApi#ResponseBody:"+e.getResponseBody());

	    e.printStackTrace();
	    throw e;
		}
	}
}
