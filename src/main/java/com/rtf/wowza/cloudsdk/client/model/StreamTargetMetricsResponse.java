/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.rtf.wowza.cloudsdk.client.model.StreamTargetMetrics;
import java.io.IOException;

/**
 * The Class StreamTargetMetricsResponse.
 */
public class StreamTargetMetricsResponse {
  
  /** The id. */
  @SerializedName("id")
  private String id = null;

  /** The metrics. */
  @SerializedName("metrics")
  private StreamTargetMetrics metrics = null;

  /**
   * Id.
   *
   * @param id the id
   * @return the stream target metrics response
   */
  public StreamTargetMetricsResponse id(String id) {
    this.id = id;
    return this;
  }

   /**
    * Gets the id.
    *
    * @return the id
    */
  @ApiModelProperty(example = "abcd1234", value = "The unique alphanumeric string that identifies the stream target.")
  public String getId() {
    return id;
  }

  /**
   * Sets the id.
   *
   * @param id the new id
   */
  public void setId(String id) {
    this.id = id;
  }

  /**
   * Metrics.
   *
   * @param metrics the metrics
   * @return the stream target metrics response
   */
  public StreamTargetMetricsResponse metrics(StreamTargetMetrics metrics) {
    this.metrics = metrics;
    return this;
  }

   /**
    * Gets the metrics.
    *
    * @return the metrics
    */
  @ApiModelProperty(value = "")
  public StreamTargetMetrics getMetrics() {
    return metrics;
  }

  /**
   * Sets the metrics.
   *
   * @param metrics the new metrics
   */
  public void setMetrics(StreamTargetMetrics metrics) {
    this.metrics = metrics;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    StreamTargetMetricsResponse inlineResponse20030 = (StreamTargetMetricsResponse) o;
    return Objects.equals(this.id, inlineResponse20030.id) &&
        Objects.equals(this.metrics, inlineResponse20030.metrics);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(id, metrics);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class StreamTargetMetricsResponse {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    metrics: ").append(toIndentedString(metrics)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

