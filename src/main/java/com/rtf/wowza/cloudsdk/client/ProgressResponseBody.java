/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.ResponseBody;

import java.io.IOException;

import okio.Buffer;
import okio.BufferedSource;
import okio.ForwardingSource;
import okio.Okio;
import okio.Source;

/**
 * The Class ProgressResponseBody.
 */
public class ProgressResponseBody extends ResponseBody {

    /**
     * The listener interface for receiving progress events.
     * The class that is interested in processing a progress
     * event implements this interface, and the object created
     * with that class is registered with a component using the
     * component's <code>addProgressListener<code> method. When
     * the progress event occurs, that object's appropriate
     * method is invoked.
     *
     * @see ProgressEvent
     */
    public interface ProgressListener {
        
        /**
         * Update.
         *
         * @param bytesRead the bytes read
         * @param contentLength the content length
         * @param done the done
         */
        void update(long bytesRead, long contentLength, boolean done);
    }

    /** The response body. */
    private final ResponseBody responseBody;
    
    /** The progress listener. */
    private final ProgressListener progressListener;
    
    /** The buffered source. */
    private BufferedSource bufferedSource;

    /**
     * Instantiates a new progress response body.
     *
     * @param responseBody the response body
     * @param progressListener the progress listener
     */
    public ProgressResponseBody(ResponseBody responseBody, ProgressListener progressListener) {
        this.responseBody = responseBody;
        this.progressListener = progressListener;
    }

    /**
     * Content type.
     *
     * @return the media type
     */
    @Override
    public MediaType contentType() {
        return responseBody.contentType();
    }

    /**
     * Content length.
     *
     * @return the long
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Override
    public long contentLength() throws IOException {
        return responseBody.contentLength();
    }

    /**
     * Source.
     *
     * @return the buffered source
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Override
    public BufferedSource source() throws IOException {
        if (bufferedSource == null) {
            bufferedSource = Okio.buffer(source(responseBody.source()));
        }
        return bufferedSource;
    }

    /**
     * Source.
     *
     * @param source the source
     * @return the source
     */
    private Source source(Source source) {
        return new ForwardingSource(source) {
            long totalBytesRead = 0L;

            @Override
            public long read(Buffer sink, long byteCount) throws IOException {
                long bytesRead = super.read(sink, byteCount);
                // read() returns the number of bytes read, or -1 if this source is exhausted.
                totalBytesRead += bytesRead != -1 ? bytesRead : 0;
                progressListener.update(totalBytesRead, responseBody.contentLength(), bytesRead == -1);
                return bytesRead;
            }
        };
    }
}


