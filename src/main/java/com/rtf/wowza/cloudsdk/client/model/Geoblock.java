/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.time.OffsetDateTime;

/**
 * The Class Geoblock.
 */
@ApiModel(description = "")
public class Geoblock {
  
  /** The countries. */
  @SerializedName("countries")
  private List<String> countries = null;

  /** The created at. */
  @SerializedName("created_at")
  private OffsetDateTime createdAt = null;

  /**
   * The Enum StateEnum.
   */
  @JsonAdapter(StateEnum.Adapter.class)
  public enum StateEnum {
    
    /** The requested. */
    REQUESTED("requested"),
    
    /** The activated. */
    ACTIVATED("activated"),
    
    /** The update requested. */
    UPDATE_REQUESTED("update_requested"),
    
    /** The delete requested. */
    DELETE_REQUESTED("delete_requested"),
    
    /** The failed. */
    FAILED("failed");

    /** The value. */
    private String value;

    /**
     * Instantiates a new state enum.
     *
     * @param value the value
     */
    StateEnum(String value) {
      this.value = value;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
      return value;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString() {
      return String.valueOf(value);
    }

    /**
     * From value.
     *
     * @param text the text
     * @return the state enum
     */
    public static StateEnum fromValue(String text) {
      for (StateEnum b : StateEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    /**
     * The Class Adapter.
     */
    public static class Adapter extends TypeAdapter<StateEnum> {
      
      /**
       * Write.
       *
       * @param jsonWriter the json writer
       * @param enumeration the enumeration
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public void write(final JsonWriter jsonWriter, final StateEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      /**
       * Read.
       *
       * @param jsonReader the json reader
       * @return the state enum
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public StateEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return StateEnum.fromValue(String.valueOf(value));
      }
    }
  }

  /** The state. */
  @SerializedName("state")
  private StateEnum state = null;

  /** The stream target id. */
  @SerializedName("stream_target_id")
  private String streamTargetId = null;

  /**
   * The Enum TypeEnum.
   */
  @JsonAdapter(TypeEnum.Adapter.class)
  public enum TypeEnum {
    
    /** The disabled. */
    DISABLED("disabled"),
    
    /** The allow. */
    ALLOW("allow"),
    
    /** The deny. */
    DENY("deny");

    /** The value. */
    private String value;

    /**
     * Instantiates a new type enum.
     *
     * @param value the value
     */
    TypeEnum(String value) {
      this.value = value;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
      return value;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString() {
      return String.valueOf(value);
    }

    /**
     * From value.
     *
     * @param text the text
     * @return the type enum
     */
    public static TypeEnum fromValue(String text) {
      for (TypeEnum b : TypeEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    /**
     * The Class Adapter.
     */
    public static class Adapter extends TypeAdapter<TypeEnum> {
      
      /**
       * Write.
       *
       * @param jsonWriter the json writer
       * @param enumeration the enumeration
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public void write(final JsonWriter jsonWriter, final TypeEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      /**
       * Read.
       *
       * @param jsonReader the json reader
       * @return the type enum
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public TypeEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return TypeEnum.fromValue(String.valueOf(value));
      }
    }
  }

  /** The type. */
  @SerializedName("type")
  private TypeEnum type = null;

  /** The updated at. */
  @SerializedName("updated_at")
  private OffsetDateTime updatedAt = null;

  /** The whitelist. */
  @SerializedName("whitelist")
  private List<String> whitelist = null;

  /**
   * Countries.
   *
   * @param countries the countries
   * @return the geoblock
   */
  public Geoblock countries(List<String> countries) {
    this.countries = countries;
    return this;
  }

  /**
   * Adds the countries item.
   *
   * @param countriesItem the countries item
   * @return the geoblock
   */
  public Geoblock addCountriesItem(String countriesItem) {
    if (this.countries == null) {
      this.countries = new ArrayList<String>();
    }
    this.countries.add(countriesItem);
    return this;
  }

   /**
    * Gets the countries.
    *
    * @return the countries
    */
  @ApiModelProperty(example = "\"\"", value = "Required when <em>type</em> is <strong>allow</strong> or <strong>deny</strong>. The locations affected by the geo-blocking. Enter a comma-separated list (an array) of two-letter ISO 3166-1 country codes. For a list, see <a href='https://en.wikipedia.org/wiki/ISO_3166-1' target='_blank'>ISO 3166-1</a> on wikipedia.")
  public List<String> getCountries() {
    return countries;
  }

  /**
   * Sets the countries.
   *
   * @param countries the new countries
   */
  public void setCountries(List<String> countries) {
    this.countries = countries;
  }

  /**
   * Created at.
   *
   * @param createdAt the created at
   * @return the geoblock
   */
  public Geoblock createdAt(OffsetDateTime createdAt) {
    this.createdAt = createdAt;
    return this;
  }

   /**
    * Gets the created at.
    *
    * @return the created at
    */
  @ApiModelProperty(example = "", value = "The date and time that the geo-blocking rendition was created.")
  public OffsetDateTime getCreatedAt() {
    return createdAt;
  }

  /**
   * Sets the created at.
   *
   * @param createdAt the new created at
   */
  public void setCreatedAt(OffsetDateTime createdAt) {
    this.createdAt = createdAt;
  }

  /**
   * State.
   *
   * @param state the state
   * @return the geoblock
   */
  public Geoblock state(StateEnum state) {
    this.state = state;
    return this;
  }

   /**
    * Gets the state.
    *
    * @return the state
    */
  @ApiModelProperty(example = "", value = "The state of the geo-blocking.")
  public StateEnum getState() {
    return state;
  }

  /**
   * Sets the state.
   *
   * @param state the new state
   */
  public void setState(StateEnum state) {
    this.state = state;
  }

  /**
   * Stream target id.
   *
   * @param streamTargetId the stream target id
   * @return the geoblock
   */
  public Geoblock streamTargetId(String streamTargetId) {
    this.streamTargetId = streamTargetId;
    return this;
  }

   /**
    * Gets the stream target id.
    *
    * @return the stream target id
    */
  @ApiModelProperty(example = "", value = "The unique alphanumeric string that identifies the stream target.")
  public String getStreamTargetId() {
    return streamTargetId;
  }

  /**
   * Sets the stream target id.
   *
   * @param streamTargetId the new stream target id
   */
  public void setStreamTargetId(String streamTargetId) {
    this.streamTargetId = streamTargetId;
  }

  /**
   * Type.
   *
   * @param type the type
   * @return the geoblock
   */
  public Geoblock type(TypeEnum type) {
    this.type = type;
    return this;
  }

   /**
    * Gets the type.
    *
    * @return the type
    */
  @ApiModelProperty(example = "", value = "The type of geo-blocking to apply. The value <strong>allow</strong> permits viewing only in the locations specified by the <em>countries</em> parameter. The value <strong>deny</strong> prohibits viewing in the locations specified by the <em>countries</em> parameter. The value <strong>disabled</strong> (the default) permits viewing everywhere.")
  public TypeEnum getType() {
    return type;
  }

  /**
   * Sets the type.
   *
   * @param type the new type
   */
  public void setType(TypeEnum type) {
    this.type = type;
  }

  /**
   * Updated at.
   *
   * @param updatedAt the updated at
   * @return the geoblock
   */
  public Geoblock updatedAt(OffsetDateTime updatedAt) {
    this.updatedAt = updatedAt;
    return this;
  }

   /**
    * Gets the updated at.
    *
    * @return the updated at
    */
  @ApiModelProperty(example = "", value = "The date and time that the geo-blocking rendition was updated.")
  public OffsetDateTime getUpdatedAt() {
    return updatedAt;
  }

  /**
   * Sets the updated at.
   *
   * @param updatedAt the new updated at
   */
  public void setUpdatedAt(OffsetDateTime updatedAt) {
    this.updatedAt = updatedAt;
  }

  /**
   * Whitelist.
   *
   * @param whitelist the whitelist
   * @return the geoblock
   */
  public Geoblock whitelist(List<String> whitelist) {
    this.whitelist = whitelist;
    return this;
  }

  /**
   * Adds the whitelist item.
   *
   * @param whitelistItem the whitelist item
   * @return the geoblock
   */
  public Geoblock addWhitelistItem(String whitelistItem) {
    if (this.whitelist == null) {
      this.whitelist = new ArrayList<String>();
    }
    this.whitelist.add(whitelistItem);
    return this;
  }

   /**
    * Gets the whitelist.
    *
    * @return the whitelist
    */
  @ApiModelProperty(example = "\"\"", value = "Whitelisted addresses can be viewed even if they're within a geo-blocked location. Enter a comma-separated list (an array) of IP addresses that always allow streaming.")
  public List<String> getWhitelist() {
    return whitelist;
  }

  /**
   * Sets the whitelist.
   *
   * @param whitelist the new whitelist
   */
  public void setWhitelist(List<String> whitelist) {
    this.whitelist = whitelist;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Geoblock geoblock = (Geoblock) o;
    return Objects.equals(this.countries, geoblock.countries) &&
        Objects.equals(this.createdAt, geoblock.createdAt) &&
        Objects.equals(this.state, geoblock.state) &&
        Objects.equals(this.streamTargetId, geoblock.streamTargetId) &&
        Objects.equals(this.type, geoblock.type) &&
        Objects.equals(this.updatedAt, geoblock.updatedAt) &&
        Objects.equals(this.whitelist, geoblock.whitelist);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(countries, createdAt, state, streamTargetId, type, updatedAt, whitelist);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Geoblock {\n");
    
    sb.append("    countries: ").append(toIndentedString(countries)).append("\n");
    sb.append("    createdAt: ").append(toIndentedString(createdAt)).append("\n");
    sb.append("    state: ").append(toIndentedString(state)).append("\n");
    sb.append("    streamTargetId: ").append(toIndentedString(streamTargetId)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    updatedAt: ").append(toIndentedString(updatedAt)).append("\n");
    sb.append("    whitelist: ").append(toIndentedString(whitelist)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

