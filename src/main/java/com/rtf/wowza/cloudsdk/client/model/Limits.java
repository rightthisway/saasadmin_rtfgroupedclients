/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import java.util.List;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.time.OffsetDateTime;

/**
 * The Class Limits.
 */
@javax.annotation.Generated(value ="com.wowza.cloudsdk.JavaCreate", date = "2019-03-02T10:45:24.077Z")
public class Limits {
  
  /** The fields. */
  @SerializedName("fields")
  private List<String> fields = null;

  /** The from. */
  @SerializedName("from")
  private OffsetDateTime from = null;

  /** The to. */
  @SerializedName("to")
  private OffsetDateTime to = null;

  /**
   * Fields.
   *
   * @param fields the fields
   * @return the limits
   */
  public Limits fields(List<String> fields) {
    this.fields = fields;
    return this;
  }

   /**
    * Gets the fields.
    *
    * @return the fields
    */
  @ApiModelProperty(required = true, value = "A comma-separated list of fields that were returned in the request.")
  public List<String> getFields() {
    return fields;
  }

  /**
   * Sets the fields.
   *
   * @param fields the new fields
   */
  public void setFields(List<String> fields) {
    this.fields = fields;
  }

  /**
   * From.
   *
   * @param from the from
   * @return the limits
   */
  public Limits from(OffsetDateTime from) {
    this.from = from;
    return this;
  }

   /**
    * Gets the from.
    *
    * @return the from
    */
  @ApiModelProperty(required = true, value = "The start of range of time when the metrics were aggregated for the query.")
  public OffsetDateTime getFrom() {
    return from;
  }

  /**
   * Sets the from.
   *
   * @param from the new from
   */
  public void setFrom(OffsetDateTime from) {
    this.from = from;
  }

  /**
   * To.
   *
   * @param to the to
   * @return the limits
   */
  public Limits to(OffsetDateTime to) {
    this.to = to;
    return this;
  }

   /**
    * Gets the to.
    *
    * @return the to
    */
  @ApiModelProperty(required = true, value = "The end of the range of time when the metrics were aggregated for the query.")
  public OffsetDateTime getTo() {
    return to;
  }

  /**
   * Sets the to.
   *
   * @param to the new to
   */
  public void setTo(OffsetDateTime to) {
    this.to = to;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Limits limits1 = (Limits) o;
    return Objects.equals(this.fields, limits1.fields) &&
        Objects.equals(this.from, limits1.from) &&
        Objects.equals(this.to, limits1.to);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(fields, from, to);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Limits {\n");
    
    sb.append("    fields: ").append(toIndentedString(fields)).append("\n");
    sb.append("    from: ").append(toIndentedString(from)).append("\n");
    sb.append("    to: ").append(toIndentedString(to)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

