/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.time.OffsetDateTime;

/**
 * The Class StreamTargetUll.
 */
@ApiModel(description = "")
@javax.annotation.Generated(value ="com.wowza.cloudsdk.JavaCreate", date = "2019-03-02T10:45:24.077Z")
public class StreamTargetUll {
  
  /** The created at. */
  @SerializedName("created_at")
  private OffsetDateTime createdAt = null;

  /** The connection code. */
  @SerializedName("connection_code")
  private String connectionCode = null;

  /** The connection code expires at. */
  @SerializedName("connection_code_expires_at")
  private OffsetDateTime connectionCodeExpiresAt = null;

  /** The enabled. */
  @SerializedName("enabled")
  private Boolean enabled = null;

  /** The enable hls. */
  @SerializedName("enable_hls")
  private Boolean enableHls = null;

  /** The id. */
  @SerializedName("id")
  private String id = null;

  /** The ingest ip whitelist. */
  @SerializedName("ingest_ip_whitelist")
  private List<String> ingestIpWhitelist = null;

  /** The name. */
  @SerializedName("name")
  private String name = null;

  /** The playback urls. */
  @SerializedName("playback_urls")
  private HashMap<String,List<String>> playbackUrls = null;

  /** The primary url. */
  @SerializedName("primary_url")
  private String primaryUrl = null;

  /**
   * The Enum RegionOverrideEnum.
   */
  @JsonAdapter(RegionOverrideEnum.Adapter.class)
  public enum RegionOverrideEnum {
    
    /** The origin nl central1 cdn wowza com. */
    ORIGIN_NL_CENTRAL1_CDN_WOWZA_COM("origin-nl-central1.cdn.wowza.com"),
    
    /** The origin us central2 cdn wowza com. */
    ORIGIN_US_CENTRAL2_CDN_WOWZA_COM("origin-us-central2.cdn.wowza.com"),
    
    /** The origin in west1 cdn wowza com. */
    ORIGIN_IN_WEST1_CDN_WOWZA_COM("origin-in-west1.cdn.wowza.com"),
    
    /** The origin hk central1 cdn wowza com. */
    ORIGIN_HK_CENTRAL1_CDN_WOWZA_COM("origin-hk-central1.cdn.wowza.com"),
    
    /** The origin jp east1 cdn wowza com. */
    ORIGIN_JP_EAST1_CDN_WOWZA_COM("origin-jp-east1.cdn.wowza.com"),
    
    /** The null. */
    NULL("null");

    /** The value. */
    private String value;

    /**
     * Instantiates a new region override enum.
     *
     * @param value the value
     */
    RegionOverrideEnum(String value) {
      this.value = value;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
      return value;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString() {
      return String.valueOf(value);
    }

    /**
     * From value.
     *
     * @param text the text
     * @return the region override enum
     */
    public static RegionOverrideEnum fromValue(String text) {
      for (RegionOverrideEnum b : RegionOverrideEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    /**
     * The Class Adapter.
     */
    public static class Adapter extends TypeAdapter<RegionOverrideEnum> {
      
      /**
       * Write.
       *
       * @param jsonWriter the json writer
       * @param enumeration the enumeration
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public void write(final JsonWriter jsonWriter, final RegionOverrideEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      /**
       * Read.
       *
       * @param jsonReader the json reader
       * @return the region override enum
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public RegionOverrideEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return RegionOverrideEnum.fromValue(String.valueOf(value));
      }
    }
  }

  /** The region override. */
  @SerializedName("region_override")
  private RegionOverrideEnum regionOverride = null;

  /**
   * The Enum SourceDeliveryMethodEnum.
   */
  @JsonAdapter(SourceDeliveryMethodEnum.Adapter.class)
  public enum SourceDeliveryMethodEnum {
    
    /** The push. */
    PUSH("push"),
    
    /** The pull. */
    PULL("pull");

    /** The value. */
    private String value;

    /**
     * Instantiates a new source delivery method enum.
     *
     * @param value the value
     */
    SourceDeliveryMethodEnum(String value) {
      this.value = value;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
      return value;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString() {
      return String.valueOf(value);
    }

    /**
     * From value.
     *
     * @param text the text
     * @return the source delivery method enum
     */
    public static SourceDeliveryMethodEnum fromValue(String text) {
      for (SourceDeliveryMethodEnum b : SourceDeliveryMethodEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    /**
     * The Class Adapter.
     */
    public static class Adapter extends TypeAdapter<SourceDeliveryMethodEnum> {
      
      /**
       * Write.
       *
       * @param jsonWriter the json writer
       * @param enumeration the enumeration
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public void write(final JsonWriter jsonWriter, final SourceDeliveryMethodEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      /**
       * Read.
       *
       * @param jsonReader the json reader
       * @return the source delivery method enum
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public SourceDeliveryMethodEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return SourceDeliveryMethodEnum.fromValue(String.valueOf(value));
      }
    }
  }

  /** The source delivery method. */
  @SerializedName("source_delivery_method")
  private SourceDeliveryMethodEnum sourceDeliveryMethod = null;

  /** The source url. */
  @SerializedName("source_url")
  private String sourceUrl = null;

  /**
   * The Enum StateEnum.
   */
  @JsonAdapter(StateEnum.Adapter.class)
  public enum StateEnum {
    
    /** The started. */
    STARTED("started"),
    
    /** The stopped. */
    STOPPED("stopped"),
    
    /** The error. */
    ERROR("error");

    /** The value. */
    private String value;

    /**
     * Instantiates a new state enum.
     *
     * @param value the value
     */
    StateEnum(String value) {
      this.value = value;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
      return value;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString() {
      return String.valueOf(value);
    }

    /**
     * From value.
     *
     * @param text the text
     * @return the state enum
     */
    public static StateEnum fromValue(String text) {
      for (StateEnum b : StateEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    /**
     * The Class Adapter.
     */
    public static class Adapter extends TypeAdapter<StateEnum> {
      
      /**
       * Write.
       *
       * @param jsonWriter the json writer
       * @param enumeration the enumeration
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public void write(final JsonWriter jsonWriter, final StateEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      /**
       * Read.
       *
       * @param jsonReader the json reader
       * @return the state enum
       * @throws IOException Signals that an I/O exception has occurred.
       */
      @Override
      public StateEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return StateEnum.fromValue(String.valueOf(value));
      }
    }
  }

  /** The state. */
  @SerializedName("state")
  private StateEnum state = null;

  /** The stream name. */
  @SerializedName("stream_name")
  private String streamName = null;

  /** The updated at. */
  @SerializedName("updated_at")
  private OffsetDateTime updatedAt = null;

  /**
   * Created at.
   *
   * @param createdAt the created at
   * @return the stream target ull
   */
  public StreamTargetUll createdAt(OffsetDateTime createdAt) {
    this.createdAt = createdAt;
    return this;
  }

   /**
    * Gets the created at.
    *
    * @return the created at
    */
  @ApiModelProperty(example = "", value = "The date and time that the ultra low latency stream target was created.")
  public OffsetDateTime getCreatedAt() {
    return createdAt;
  }

  /**
   * Sets the created at.
   *
   * @param createdAt the new created at
   */
  public void setCreatedAt(OffsetDateTime createdAt) {
    this.createdAt = createdAt;
  }

  /**
   * Connection code.
   *
   * @param connectionCode the connection code
   * @return the stream target ull
   */
  public StreamTargetUll connectionCode(String connectionCode) {
    this.connectionCode = connectionCode;
    return this;
  }

   /**
    * Gets the connection code.
    *
    * @return the connection code
    */
  @ApiModelProperty(example = "2bbeaf", value = "A six-character, alphanumeric string that allows the Wowza GoCoder app to send an encoded stream to an ultra low latency stream target. The code can be used once and expires 24 hours after it's created.")
  public String getConnectionCode() {
    return connectionCode;
  }

  /**
   * Sets the connection code.
   *
   * @param connectionCode the new connection code
   */
  public void setConnectionCode(String connectionCode) {
    this.connectionCode = connectionCode;
  }

  /**
   * Connection code expires at.
   *
   * @param connectionCodeExpiresAt the connection code expires at
   * @return the stream target ull
   */
  public StreamTargetUll connectionCodeExpiresAt(OffsetDateTime connectionCodeExpiresAt) {
    this.connectionCodeExpiresAt = connectionCodeExpiresAt;
    return this;
  }

   /**
    * Gets the connection code expires at.
    *
    * @return the connection code expires at
    */
  @ApiModelProperty(example = "", value = "The date and time that the <em>connection_code</em> expires.")
  public OffsetDateTime getConnectionCodeExpiresAt() {
    return connectionCodeExpiresAt;
  }

  /**
   * Sets the connection code expires at.
   *
   * @param connectionCodeExpiresAt the new connection code expires at
   */
  public void setConnectionCodeExpiresAt(OffsetDateTime connectionCodeExpiresAt) {
    this.connectionCodeExpiresAt = connectionCodeExpiresAt;
  }

  /**
   * Enabled.
   *
   * @param enabled the enabled
   * @return the stream target ull
   */
  public StreamTargetUll enabled(Boolean enabled) {
    this.enabled = enabled;
    return this;
  }

   /**
    * Checks if is enabled.
    *
    * @return the boolean
    */
  @ApiModelProperty(example = "true", value = "If <strong>true</strong> (the default), the source stream is ready to be ingested. If **false**, the source stream won't be ingested by the target's origin server.")
  public Boolean isEnabled() {
    return enabled;
  }

  /**
   * Sets the enabled.
   *
   * @param enabled the new enabled
   */
  public void setEnabled(Boolean enabled) {
    this.enabled = enabled;
  }

  /**
   * Enable hls.
   *
   * @param enableHls the enable hls
   * @return the stream target ull
   */
  public StreamTargetUll enableHls(Boolean enableHls) {
    this.enableHls = enableHls;
    return this;
  }

   /**
    * Checks if is enable hls.
    *
    * @return the boolean
    */
  @ApiModelProperty(example = "true", value = "If <strong>true</strong>, creates an Apple HLS URL for playback on iOS devices. The default is <strong>false</strong>. The HLS stream has the <strong>convertAMFData</strong> stream target property enabled by default.")
  public Boolean isEnableHls() {
    return enableHls;
  }

  /**
   * Sets the enable hls.
   *
   * @param enableHls the new enable hls
   */
  public void setEnableHls(Boolean enableHls) {
    this.enableHls = enableHls;
  }

  /**
   * Id.
   *
   * @param id the id
   * @return the stream target ull
   */
  public StreamTargetUll id(String id) {
    this.id = id;
    return this;
  }

   /**
    * Gets the id.
    *
    * @return the id
    */
  @ApiModelProperty(example = "1234abcd", value = "The unique alphanumeric string that identifies the ultra low latency stream target.")
  public String getId() {
    return id;
  }

  /**
   * Sets the id.
   *
   * @param id the new id
   */
  public void setId(String id) {
    this.id = id;
  }

  /**
   * Ingest ip whitelist.
   *
   * @param ingestIpWhitelist the ingest ip whitelist
   * @return the stream target ull
   */
  public StreamTargetUll ingestIpWhitelist(List<String> ingestIpWhitelist) {
    this.ingestIpWhitelist = ingestIpWhitelist;
    return this;
  }

  /**
   * Adds the ingest ip whitelist item.
   *
   * @param ingestIpWhitelistItem the ingest ip whitelist item
   * @return the stream target ull
   */
  public StreamTargetUll addIngestIpWhitelistItem(String ingestIpWhitelistItem) {
    if (this.ingestIpWhitelist == null) {
      this.ingestIpWhitelist = new ArrayList<String>();
    }
    this.ingestIpWhitelist.add(ingestIpWhitelistItem);
    return this;
  }

   /**
    * Gets the ingest ip whitelist.
    *
    * @return the ingest ip whitelist
    */
  @ApiModelProperty(example = "[\"10.11.12.*\",\"13.12.11.10\"]", value = "Only for ultra low latency stream targets whose <em>source_delivery_method</em> is **push**. An array  of IP addresses in dot-decimal notation that can be used to connect to the target's origin server. Wildcards (*) are accepted for the final value in the IP address only.")
  public List<String> getIngestIpWhitelist() {
    return ingestIpWhitelist;
  }

  /**
   * Sets the ingest ip whitelist.
   *
   * @param ingestIpWhitelist the new ingest ip whitelist
   */
  public void setIngestIpWhitelist(List<String> ingestIpWhitelist) {
    this.ingestIpWhitelist = ingestIpWhitelist;
  }

  /**
   * Name.
   *
   * @param name the name
   * @return the stream target ull
   */
  public StreamTargetUll name(String name) {
    this.name = name;
    return this;
  }

   /**
    * Gets the name.
    *
    * @return the name
    */
  @ApiModelProperty(example = "My Ultra Low Latency Stream Target", value = "A descriptive name for the ultra low latency stream target. Maximum 255 characters.")
  public String getName() {
    return name;
  }

  /**
   * Sets the name.
   *
   * @param name the new name
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Playback urls.
   *
   * @param playbackUrls the playback urls
   * @return the stream target ull
   */
  public StreamTargetUll playbackUrls(HashMap<String,List<String>> playbackUrls) {
    this.playbackUrls = playbackUrls;
    return this;
  }

   /**
    * Gets the playback urls.
    *
    * @return the playback urls
    */
  @ApiModelProperty(value = "")
  public HashMap<String,List<String>> getPlaybackUrls() {
    return playbackUrls;
  }

  /**
   * Sets the playback urls.
   *
   * @param playbackUrls the playback urls
   */
  public void setPlaybackUrls(HashMap<String,List<String>> playbackUrls) {
    this.playbackUrls = playbackUrls;
  }
  
  /**
   * Adds the playback urls item.
   *
   * @param name the name
   * @param playbackUrlsItem the playback urls item
   * @return the stream target ull
   */
  public StreamTargetUll addPlaybackUrlsItem(String name,String playbackUrlsItem) {
    if (this.playbackUrls == null) {
      this.playbackUrls = new HashMap<String,List<String>>();
    }
        if ( !this.playbackUrls.containsKey(name) )
                {
                List<String> newList = new ArrayList<String>();
                newList.add(playbackUrlsItem);
                this.playbackUrls.put(name,newList);
                }
        else
                {
                List<String> current = this.playbackUrls.get(name);
                current.add(playbackUrlsItem);
                this.playbackUrls.put(name,current);
                }
    return this;
  }


  /**
   * Primary url.
   *
   * @param primaryUrl the primary url
   * @return the stream target ull
   */
  public StreamTargetUll primaryUrl(String primaryUrl) {
    this.primaryUrl = primaryUrl;
    return this;
  }

   /**
    * Gets the primary url.
    *
    * @return the primary url
    */
  @ApiModelProperty(example = "rtmp://origin-subdomain.cdn.wowza.com:1935/live/0I0q1bjZhRzZtfSdv4TpCnlmwQT16239", value = "Only for ultra low latency stream targets whose <em>source_delivery_method</em> is **push**.The primary ingest URL of the target.")
  public String getPrimaryUrl() {
    return primaryUrl;
  }

  /**
   * Sets the primary url.
   *
   * @param primaryUrl the new primary url
   */
  public void setPrimaryUrl(String primaryUrl) {
    this.primaryUrl = primaryUrl;
  }

  /**
   * Region override.
   *
   * @param regionOverride the region override
   * @return the stream target ull
   */
  public StreamTargetUll regionOverride(RegionOverrideEnum regionOverride) {
    this.regionOverride = regionOverride;
    return this;
  }

   /**
    * Gets the region override.
    *
    * @return the region override
    */
  @ApiModelProperty(example = "origin-nl-central1.cdn.wowza.com", value = "Only for ultra low latency stream targets whose <em>source_delivery_method</em> is **pull**. The location of the ultra low latency stream target's origin server. If unspecified, Wowza Streaming Cloud determines the optimal region for the origin server.")
  public RegionOverrideEnum getRegionOverride() {
    return regionOverride;
  }

  /**
   * Sets the region override.
   *
   * @param regionOverride the new region override
   */
  public void setRegionOverride(RegionOverrideEnum regionOverride) {
    this.regionOverride = regionOverride;
  }

  /**
   * Source delivery method.
   *
   * @param sourceDeliveryMethod the source delivery method
   * @return the stream target ull
   */
  public StreamTargetUll sourceDeliveryMethod(SourceDeliveryMethodEnum sourceDeliveryMethod) {
    this.sourceDeliveryMethod = sourceDeliveryMethod;
    return this;
  }

   /**
    * Gets the source delivery method.
    *
    * @return the source delivery method
    */
  @ApiModelProperty(example = "pull", value = "The type of connection between the stream source and the ultra low latency stream target. **push** instructs the source to push the stream to the stream target. **pull** instructs the stream target to pull the stream from the source.")
  public SourceDeliveryMethodEnum getSourceDeliveryMethod() {
    return sourceDeliveryMethod;
  }

  /**
   * Sets the source delivery method.
   *
   * @param sourceDeliveryMethod the new source delivery method
   */
  public void setSourceDeliveryMethod(SourceDeliveryMethodEnum sourceDeliveryMethod) {
    this.sourceDeliveryMethod = sourceDeliveryMethod;
  }

  /**
   * Source url.
   *
   * @param sourceUrl the source url
   * @return the stream target ull
   */
  public StreamTargetUll sourceUrl(String sourceUrl) {
    this.sourceUrl = sourceUrl;
    return this;
  }

   /**
    * Gets the source url.
    *
    * @return the source url
    */
  @ApiModelProperty(example = "rtsp://example.com/video", value = "Only for ultra low latency stream targets whose <em>source_delivery_method</em> is **pull**. The URL of a source IP camera or encoder connecting to the stream target.")
  public String getSourceUrl() {
    return sourceUrl;
  }

  /**
   * Sets the source url.
   *
   * @param sourceUrl the new source url
   */
  public void setSourceUrl(String sourceUrl) {
    this.sourceUrl = sourceUrl;
  }

  /**
   * State.
   *
   * @param state the state
   * @return the stream target ull
   */
  public StreamTargetUll state(StateEnum state) {
    this.state = state;
    return this;
  }

   /**
    * Gets the state.
    *
    * @return the state
    */
  @ApiModelProperty(example = "stopped", value = "The state of the ultra low latency stream target.")
  public StateEnum getState() {
    return state;
  }

  /**
   * Sets the state.
   *
   * @param state the new state
   */
  public void setState(StateEnum state) {
    this.state = state;
  }

  /**
   * Stream name.
   *
   * @param streamName the stream name
   * @return the stream target ull
   */
  public StreamTargetUll streamName(String streamName) {
    this.streamName = streamName;
    return this;
  }

   /**
    * Gets the stream name.
    *
    * @return the stream name
    */
  @ApiModelProperty(example = "0I0q1bjZhRzZtfSdv4TpCnlmwQT16239", value = "The name of the stream being ingested into the target. Returned only for ultra low latency stream targets whose <em>source_delivery_method</em> is **push**.")
  public String getStreamName() {
    return streamName;
  }

  /**
   * Sets the stream name.
   *
   * @param streamName the new stream name
   */
  public void setStreamName(String streamName) {
    this.streamName = streamName;
  }

  /**
   * Updated at.
   *
   * @param updatedAt the updated at
   * @return the stream target ull
   */
  public StreamTargetUll updatedAt(OffsetDateTime updatedAt) {
    this.updatedAt = updatedAt;
    return this;
  }

   /**
    * Gets the updated at.
    *
    * @return the updated at
    */
  @ApiModelProperty(example = "", value = "The date and time that the ultra low latency stream target was updated.")
  public OffsetDateTime getUpdatedAt() {
    return updatedAt;
  }

  /**
   * Sets the updated at.
   *
   * @param updatedAt the new updated at
   */
  public void setUpdatedAt(OffsetDateTime updatedAt) {
    this.updatedAt = updatedAt;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    StreamTargetUll streamTargetUll = (StreamTargetUll) o;
    return Objects.equals(this.createdAt, streamTargetUll.createdAt) &&
        Objects.equals(this.connectionCode, streamTargetUll.connectionCode) &&
        Objects.equals(this.connectionCodeExpiresAt, streamTargetUll.connectionCodeExpiresAt) &&
        Objects.equals(this.enabled, streamTargetUll.enabled) &&
        Objects.equals(this.enableHls, streamTargetUll.enableHls) &&
        Objects.equals(this.id, streamTargetUll.id) &&
        Objects.equals(this.ingestIpWhitelist, streamTargetUll.ingestIpWhitelist) &&
        Objects.equals(this.name, streamTargetUll.name) &&
        Objects.equals(this.playbackUrls, streamTargetUll.playbackUrls) &&
        Objects.equals(this.primaryUrl, streamTargetUll.primaryUrl) &&
        Objects.equals(this.regionOverride, streamTargetUll.regionOverride) &&
        Objects.equals(this.sourceDeliveryMethod, streamTargetUll.sourceDeliveryMethod) &&
        Objects.equals(this.sourceUrl, streamTargetUll.sourceUrl) &&
        Objects.equals(this.state, streamTargetUll.state) &&
        Objects.equals(this.streamName, streamTargetUll.streamName) &&
        Objects.equals(this.updatedAt, streamTargetUll.updatedAt);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(createdAt, connectionCode, connectionCodeExpiresAt, enabled, enableHls, id, ingestIpWhitelist, name, playbackUrls, primaryUrl, regionOverride, sourceDeliveryMethod, sourceUrl, state, streamName, updatedAt);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class StreamTargetUll {\n");
    
    sb.append("    createdAt: ").append(toIndentedString(createdAt)).append("\n");
    sb.append("    connectionCode: ").append(toIndentedString(connectionCode)).append("\n");
    sb.append("    connectionCodeExpiresAt: ").append(toIndentedString(connectionCodeExpiresAt)).append("\n");
    sb.append("    enabled: ").append(toIndentedString(enabled)).append("\n");
    sb.append("    enableHls: ").append(toIndentedString(enableHls)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    ingestIpWhitelist: ").append(toIndentedString(ingestIpWhitelist)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    playbackUrls: ").append(toIndentedString(playbackUrls)).append("\n");
    sb.append("    primaryUrl: ").append(toIndentedString(primaryUrl)).append("\n");
    sb.append("    regionOverride: ").append(toIndentedString(regionOverride)).append("\n");
    sb.append("    sourceDeliveryMethod: ").append(toIndentedString(sourceDeliveryMethod)).append("\n");
    sb.append("    sourceUrl: ").append(toIndentedString(sourceUrl)).append("\n");
    sb.append("    state: ").append(toIndentedString(state)).append("\n");
    sb.append("    streamName: ").append(toIndentedString(streamName)).append("\n");
    sb.append("    updatedAt: ").append(toIndentedString(updatedAt)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

