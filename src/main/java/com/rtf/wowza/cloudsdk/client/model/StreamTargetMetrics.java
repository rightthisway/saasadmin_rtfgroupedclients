/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.time.OffsetDateTime;

/**
 * The Class StreamTargetMetrics.
 */
@ApiModel(description = "")
public class StreamTargetMetrics {
  
  /** The average bytes in. */
  @SerializedName("average_bytes_in")
  private Float averageBytesIn = null;

  /** The average total connections. */
  @SerializedName("average_total_connections")
  private Float averageTotalConnections = null;

  /** The created at. */
  @SerializedName("created_at")
  private OffsetDateTime createdAt = null;

  /** The dropped connections. */
  @SerializedName("dropped_connections")
  private Integer droppedConnections = null;

  /** The maximum total connections. */
  @SerializedName("maximum_total_connections")
  private Integer maximumTotalConnections = null;

  /** The minimum total connections. */
  @SerializedName("minimum_total_connections")
  private Integer minimumTotalConnections = null;

  /** The new connections. */
  @SerializedName("new_connections")
  private Integer newConnections = null;

  /**
   * Average bytes in.
   *
   * @param averageBytesIn the average bytes in
   * @return the stream target metrics
   */
  public StreamTargetMetrics averageBytesIn(Float averageBytesIn) {
    this.averageBytesIn = averageBytesIn;
    return this;
  }

   /**
    * Gets the average bytes in.
    *
    * @return the average bytes in
    */
  @ApiModelProperty(example = "456789.12", value = "The average number of bytes transfered to the origin server by the source.")
  public Float getAverageBytesIn() {
    return averageBytesIn;
  }

  /**
   * Sets the average bytes in.
   *
   * @param averageBytesIn the new average bytes in
   */
  public void setAverageBytesIn(Float averageBytesIn) {
    this.averageBytesIn = averageBytesIn;
  }

  /**
   * Average total connections.
   *
   * @param averageTotalConnections the average total connections
   * @return the stream target metrics
   */
  public StreamTargetMetrics averageTotalConnections(Float averageTotalConnections) {
    this.averageTotalConnections = averageTotalConnections;
    return this;
  }

   /**
    * Gets the average total connections.
    *
    * @return the average total connections
    */
  @ApiModelProperty(example = "10.1", value = "The total number of current connections.")
  public Float getAverageTotalConnections() {
    return averageTotalConnections;
  }

  /**
   * Sets the average total connections.
   *
   * @param averageTotalConnections the new average total connections
   */
  public void setAverageTotalConnections(Float averageTotalConnections) {
    this.averageTotalConnections = averageTotalConnections;
  }

  /**
   * Created at.
   *
   * @param createdAt the created at
   * @return the stream target metrics
   */
  public StreamTargetMetrics createdAt(OffsetDateTime createdAt) {
    this.createdAt = createdAt;
    return this;
  }

   /**
    * Gets the created at.
    *
    * @return the created at
    */
  @ApiModelProperty(example = "", value = "The date and time, in UTC, that the metrics were recorded.")
  public OffsetDateTime getCreatedAt() {
    return createdAt;
  }

  /**
   * Sets the created at.
   *
   * @param createdAt the new created at
   */
  public void setCreatedAt(OffsetDateTime createdAt) {
    this.createdAt = createdAt;
  }

  /**
   * Dropped connections.
   *
   * @param droppedConnections the dropped connections
   * @return the stream target metrics
   */
  public StreamTargetMetrics droppedConnections(Integer droppedConnections) {
    this.droppedConnections = droppedConnections;
    return this;
  }

   /**
    * Gets the dropped connections.
    *
    * @return the dropped connections
    */
  @ApiModelProperty(example = "0", value = "The total number of dropped connections since the last interval.")
  public Integer getDroppedConnections() {
    return droppedConnections;
  }

  /**
   * Sets the dropped connections.
   *
   * @param droppedConnections the new dropped connections
   */
  public void setDroppedConnections(Integer droppedConnections) {
    this.droppedConnections = droppedConnections;
  }

  /**
   * Maximum total connections.
   *
   * @param maximumTotalConnections the maximum total connections
   * @return the stream target metrics
   */
  public StreamTargetMetrics maximumTotalConnections(Integer maximumTotalConnections) {
    this.maximumTotalConnections = maximumTotalConnections;
    return this;
  }

   /**
    * Gets the maximum total connections.
    *
    * @return the maximum total connections
    */
  @ApiModelProperty(example = "20", value = "The maximum number of connections during the interval.")
  public Integer getMaximumTotalConnections() {
    return maximumTotalConnections;
  }

  /**
   * Sets the maximum total connections.
   *
   * @param maximumTotalConnections the new maximum total connections
   */
  public void setMaximumTotalConnections(Integer maximumTotalConnections) {
    this.maximumTotalConnections = maximumTotalConnections;
  }

  /**
   * Minimum total connections.
   *
   * @param minimumTotalConnections the minimum total connections
   * @return the stream target metrics
   */
  public StreamTargetMetrics minimumTotalConnections(Integer minimumTotalConnections) {
    this.minimumTotalConnections = minimumTotalConnections;
    return this;
  }

   /**
    * Gets the minimum total connections.
    *
    * @return the minimum total connections
    */
  @ApiModelProperty(example = "0", value = "The minimum number of connections during the interval.")
  public Integer getMinimumTotalConnections() {
    return minimumTotalConnections;
  }

  /**
   * Sets the minimum total connections.
   *
   * @param minimumTotalConnections the new minimum total connections
   */
  public void setMinimumTotalConnections(Integer minimumTotalConnections) {
    this.minimumTotalConnections = minimumTotalConnections;
  }

  /**
   * New connections.
   *
   * @param newConnections the new connections
   * @return the stream target metrics
   */
  public StreamTargetMetrics newConnections(Integer newConnections) {
    this.newConnections = newConnections;
    return this;
  }

   /**
    * Gets the new connections.
    *
    * @return the new connections
    */
  @ApiModelProperty(example = "0", value = "The total number of new connections since the last interval.")
  public Integer getNewConnections() {
    return newConnections;
  }

  /**
   * Sets the new connections.
   *
   * @param newConnections the new new connections
   */
  public void setNewConnections(Integer newConnections) {
    this.newConnections = newConnections;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    StreamTargetMetrics streamTargetMetrics = (StreamTargetMetrics) o;
    return Objects.equals(this.averageBytesIn, streamTargetMetrics.averageBytesIn) &&
        Objects.equals(this.averageTotalConnections, streamTargetMetrics.averageTotalConnections) &&
        Objects.equals(this.createdAt, streamTargetMetrics.createdAt) &&
        Objects.equals(this.droppedConnections, streamTargetMetrics.droppedConnections) &&
        Objects.equals(this.maximumTotalConnections, streamTargetMetrics.maximumTotalConnections) &&
        Objects.equals(this.minimumTotalConnections, streamTargetMetrics.minimumTotalConnections) &&
        Objects.equals(this.newConnections, streamTargetMetrics.newConnections);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(averageBytesIn, averageTotalConnections, createdAt, droppedConnections, maximumTotalConnections, minimumTotalConnections, newConnections);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class StreamTargetMetrics {\n");
    
    sb.append("    averageBytesIn: ").append(toIndentedString(averageBytesIn)).append("\n");
    sb.append("    averageTotalConnections: ").append(toIndentedString(averageTotalConnections)).append("\n");
    sb.append("    createdAt: ").append(toIndentedString(createdAt)).append("\n");
    sb.append("    droppedConnections: ").append(toIndentedString(droppedConnections)).append("\n");
    sb.append("    maximumTotalConnections: ").append(toIndentedString(maximumTotalConnections)).append("\n");
    sb.append("    minimumTotalConnections: ").append(toIndentedString(minimumTotalConnections)).append("\n");
    sb.append("    newConnections: ").append(toIndentedString(newConnections)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

