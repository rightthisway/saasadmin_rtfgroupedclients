/*
* ***************************************************************************************************
* Copyright (c) 2021 RSG. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RSG Dev
* ***************************************************************************************************
*/


package com.rtf.wowza.cloudsdk.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.rtf.wowza.cloudsdk.client.model.AudioCodecMetric;
import com.rtf.wowza.cloudsdk.client.model.BitsInRateMetric;
import com.rtf.wowza.cloudsdk.client.model.BitsOutRateMetric;
import com.rtf.wowza.cloudsdk.client.model.BytesInRateMetric;
import com.rtf.wowza.cloudsdk.client.model.BytesOutRateMetric;
import com.rtf.wowza.cloudsdk.client.model.ConfiguredBytesOutRateMetric;
import com.rtf.wowza.cloudsdk.client.model.ConnectedMetric;
import com.rtf.wowza.cloudsdk.client.model.CpuMetric;
import com.rtf.wowza.cloudsdk.client.model.FrameRateMetric;
import com.rtf.wowza.cloudsdk.client.model.FrameSizeMetric;
import com.rtf.wowza.cloudsdk.client.model.GpuDecoderUsageMetric;
import com.rtf.wowza.cloudsdk.client.model.GpuDriverVersionMetric;
import com.rtf.wowza.cloudsdk.client.model.GpuEncoderUsageMetric;
import com.rtf.wowza.cloudsdk.client.model.GpuMemoryUsageMetric;
import com.rtf.wowza.cloudsdk.client.model.GpuUsageMetric;
import com.rtf.wowza.cloudsdk.client.model.HeightMetric;
import com.rtf.wowza.cloudsdk.client.model.KeyframeIntervalMetric;
import com.rtf.wowza.cloudsdk.client.model.StreamTargetStatusOUTPUTIDXSTREAMTARGETIDXMetric;
import com.rtf.wowza.cloudsdk.client.model.UniqueViewsMetric;
import com.rtf.wowza.cloudsdk.client.model.VideoCodecMetric;
import com.rtf.wowza.cloudsdk.client.model.WidthMetric;
import java.io.IOException;

/**
 * The Class ShmMetrics.
 */
@ApiModel(description = "")
public class ShmMetrics {
  
  /** The audio codec. */
  @SerializedName("audio_codec")
  private AudioCodecMetric audioCodec = null;

  /** The bits in rate. */
  @SerializedName("bits_in_rate")
  private BitsInRateMetric bitsInRate = null;

  /** The bits out rate. */
  @SerializedName("bits_out_rate")
  private BitsOutRateMetric bitsOutRate = null;

  /** The bytes in rate. */
  @SerializedName("bytes_in_rate")
  private BytesInRateMetric bytesInRate = null;

  /** The bytes out rate. */
  @SerializedName("bytes_out_rate")
  private BytesOutRateMetric bytesOutRate = null;

  /** The configured bytes out rate. */
  @SerializedName("configured_bytes_out_rate")
  private ConfiguredBytesOutRateMetric configuredBytesOutRate = null;

  /** The connected. */
  @SerializedName("connected")
  private ConnectedMetric connected = null;

  /** The cpu. */
  @SerializedName("cpu")
  private CpuMetric cpu = null;

  /** The frame size. */
  @SerializedName("frame_size")
  private FrameSizeMetric frameSize = null;

  /** The frame rate. */
  @SerializedName("frame_rate")
  private FrameRateMetric frameRate = null;

  /** The gpu decoder usage. */
  @SerializedName("gpu_decoder_usage")
  private GpuDecoderUsageMetric gpuDecoderUsage = null;

  /** The gpu driver version. */
  @SerializedName("gpu_driver_version")
  private GpuDriverVersionMetric gpuDriverVersion = null;

  /** The gpu encoder usage. */
  @SerializedName("gpu_encoder_usage")
  private GpuEncoderUsageMetric gpuEncoderUsage = null;

  /** The gpu memory usage. */
  @SerializedName("gpu_memory_usage")
  private GpuMemoryUsageMetric gpuMemoryUsage = null;

  /** The gpu usage. */
  @SerializedName("gpu_usage")
  private GpuUsageMetric gpuUsage = null;

  /** The height. */
  @SerializedName("height")
  private HeightMetric height = null;

  /** The keyframe interval. */
  @SerializedName("keyframe_interval")
  private KeyframeIntervalMetric keyframeInterval = null;

  /** The stream target status OUTPUTIDXSTREAMTARGETIDX. */
  @SerializedName("stream_target_status_OUTPUTIDX_STREAMTARGETIDX")
  private StreamTargetStatusOUTPUTIDXSTREAMTARGETIDXMetric streamTargetStatusOUTPUTIDXSTREAMTARGETIDX = null;

  /** The unique views. */
  @SerializedName("unique_views")
  private UniqueViewsMetric uniqueViews = null;

  /** The video codec. */
  @SerializedName("video_codec")
  private VideoCodecMetric videoCodec = null;

  /** The width. */
  @SerializedName("width")
  private WidthMetric width = null;

  /**
   * Audio codec.
   *
   * @param audioCodec the audio codec
   * @return the shm metrics
   */
  public ShmMetrics audioCodec(AudioCodecMetric audioCodec) {
    this.audioCodec = audioCodec;
    return this;
  }

   /**
    * Gets the audio codec.
    *
    * @return the audio codec
    */
  @ApiModelProperty(value = "The audio codec of the video source.")
  public AudioCodecMetric getAudioCodec() {
    return audioCodec;
  }

  /**
   * Sets the audio codec.
   *
   * @param audioCodec the new audio codec
   */
  public void setAudioCodec(AudioCodecMetric audioCodec) {
    this.audioCodec = audioCodec;
  }

  /**
   * Bits in rate.
   *
   * @param bitsInRate the bits in rate
   * @return the shm metrics
   */
  public ShmMetrics bitsInRate(BitsInRateMetric bitsInRate) {
    this.bitsInRate = bitsInRate;
    return this;
  }

   /**
    * Gets the bits in rate.
    *
    * @return the bits in rate
    */
  @ApiModelProperty(value = "The rate of the stream traveling from the source encoder to Wowza Streaming Cloud, in kilobits per second.")
  public BitsInRateMetric getBitsInRate() {
    return bitsInRate;
  }

  /**
   * Sets the bits in rate.
   *
   * @param bitsInRate the new bits in rate
   */
  public void setBitsInRate(BitsInRateMetric bitsInRate) {
    this.bitsInRate = bitsInRate;
  }

  /**
   * Bits out rate.
   *
   * @param bitsOutRate the bits out rate
   * @return the shm metrics
   */
  public ShmMetrics bitsOutRate(BitsOutRateMetric bitsOutRate) {
    this.bitsOutRate = bitsOutRate;
    return this;
  }

   /**
    * Gets the bits out rate.
    *
    * @return the bits out rate
    */
  @ApiModelProperty(value = "The total actual bitrate of all outbound video streams, in kilobits per second.")
  public BitsOutRateMetric getBitsOutRate() {
    return bitsOutRate;
  }

  /**
   * Sets the bits out rate.
   *
   * @param bitsOutRate the new bits out rate
   */
  public void setBitsOutRate(BitsOutRateMetric bitsOutRate) {
    this.bitsOutRate = bitsOutRate;
  }

  /**
   * Bytes in rate.
   *
   * @param bytesInRate the bytes in rate
   * @return the shm metrics
   */
  public ShmMetrics bytesInRate(BytesInRateMetric bytesInRate) {
    this.bytesInRate = bytesInRate;
    return this;
  }

   /**
    * Gets the bytes in rate.
    *
    * @return the bytes in rate
    */
  @ApiModelProperty(value = "The rate of the stream traveling from the source encoder to Wowza Streaming Cloud, in kilobytes per second.")
  public BytesInRateMetric getBytesInRate() {
    return bytesInRate;
  }

  /**
   * Sets the bytes in rate.
   *
   * @param bytesInRate the new bytes in rate
   */
  public void setBytesInRate(BytesInRateMetric bytesInRate) {
    this.bytesInRate = bytesInRate;
  }

  /**
   * Bytes out rate.
   *
   * @param bytesOutRate the bytes out rate
   * @return the shm metrics
   */
  public ShmMetrics bytesOutRate(BytesOutRateMetric bytesOutRate) {
    this.bytesOutRate = bytesOutRate;
    return this;
  }

   /**
    * Gets the bytes out rate.
    *
    * @return the bytes out rate
    */
  @ApiModelProperty(value = "The total actual bitrate of all outbound video streams, in kilobytes per second.")
  public BytesOutRateMetric getBytesOutRate() {
    return bytesOutRate;
  }

  /**
   * Sets the bytes out rate.
   *
   * @param bytesOutRate the new bytes out rate
   */
  public void setBytesOutRate(BytesOutRateMetric bytesOutRate) {
    this.bytesOutRate = bytesOutRate;
  }

  /**
   * Configured bytes out rate.
   *
   * @param configuredBytesOutRate the configured bytes out rate
   * @return the shm metrics
   */
  public ShmMetrics configuredBytesOutRate(ConfiguredBytesOutRateMetric configuredBytesOutRate) {
    this.configuredBytesOutRate = configuredBytesOutRate;
    return this;
  }

   /**
    * Gets the configured bytes out rate.
    *
    * @return the configured bytes out rate
    */
  @ApiModelProperty(value = "The total configured bitrate of all outbound video streams, in kilobits per second.")
  public ConfiguredBytesOutRateMetric getConfiguredBytesOutRate() {
    return configuredBytesOutRate;
  }

  /**
   * Sets the configured bytes out rate.
   *
   * @param configuredBytesOutRate the new configured bytes out rate
   */
  public void setConfiguredBytesOutRate(ConfiguredBytesOutRateMetric configuredBytesOutRate) {
    this.configuredBytesOutRate = configuredBytesOutRate;
  }

  /**
   * Connected.
   *
   * @param connected the connected
   * @return the shm metrics
   */
  public ShmMetrics connected(ConnectedMetric connected) {
    this.connected = connected;
    return this;
  }

   /**
    * Gets the connected.
    *
    * @return the connected
    */
  @ApiModelProperty(value = "The connection status of the video source. <strong>Yes</strong> means the source is connected. <strong>No</strong> means the source is not connected.")
  public ConnectedMetric getConnected() {
    return connected;
  }

  /**
   * Sets the connected.
   *
   * @param connected the new connected
   */
  public void setConnected(ConnectedMetric connected) {
    this.connected = connected;
  }

  /**
   * Cpu.
   *
   * @param cpu the cpu
   * @return the shm metrics
   */
  public ShmMetrics cpu(CpuMetric cpu) {
    this.cpu = cpu;
    return this;
  }

   /**
    * Gets the cpu.
    *
    * @return the cpu
    */
  @ApiModelProperty(value = "The percentage of available CPU power on the virtual host being used by the transcoder.")
  public CpuMetric getCpu() {
    return cpu;
  }

  /**
   * Sets the cpu.
   *
   * @param cpu the new cpu
   */
  public void setCpu(CpuMetric cpu) {
    this.cpu = cpu;
  }

  /**
   * Frame size.
   *
   * @param frameSize the frame size
   * @return the shm metrics
   */
  public ShmMetrics frameSize(FrameSizeMetric frameSize) {
    this.frameSize = frameSize;
    return this;
  }

   /**
    * Gets the frame size.
    *
    * @return the frame size
    */
  @ApiModelProperty(value = "The frame size of the video source, in pixels.")
  public FrameSizeMetric getFrameSize() {
    return frameSize;
  }

  /**
   * Sets the frame size.
   *
   * @param frameSize the new frame size
   */
  public void setFrameSize(FrameSizeMetric frameSize) {
    this.frameSize = frameSize;
  }

  /**
   * Frame rate.
   *
   * @param frameRate the frame rate
   * @return the shm metrics
   */
  public ShmMetrics frameRate(FrameRateMetric frameRate) {
    this.frameRate = frameRate;
    return this;
  }

   /**
    * Gets the frame rate.
    *
    * @return the frame rate
    */
  @ApiModelProperty(value = "The frame rate of the video source, in frames per second.")
  public FrameRateMetric getFrameRate() {
    return frameRate;
  }

  /**
   * Sets the frame rate.
   *
   * @param frameRate the new frame rate
   */
  public void setFrameRate(FrameRateMetric frameRate) {
    this.frameRate = frameRate;
  }

  /**
   * Gpu decoder usage.
   *
   * @param gpuDecoderUsage the gpu decoder usage
   * @return the shm metrics
   */
  public ShmMetrics gpuDecoderUsage(GpuDecoderUsageMetric gpuDecoderUsage) {
    this.gpuDecoderUsage = gpuDecoderUsage;
    return this;
  }

   /**
    * Gets the gpu decoder usage.
    *
    * @return the gpu decoder usage
    */
  @ApiModelProperty(value = "The percentage of the GPU decoding power on the virtual host being used by the transcoder.")
  public GpuDecoderUsageMetric getGpuDecoderUsage() {
    return gpuDecoderUsage;
  }

  /**
   * Sets the gpu decoder usage.
   *
   * @param gpuDecoderUsage the new gpu decoder usage
   */
  public void setGpuDecoderUsage(GpuDecoderUsageMetric gpuDecoderUsage) {
    this.gpuDecoderUsage = gpuDecoderUsage;
  }

  /**
   * Gpu driver version.
   *
   * @param gpuDriverVersion the gpu driver version
   * @return the shm metrics
   */
  public ShmMetrics gpuDriverVersion(GpuDriverVersionMetric gpuDriverVersion) {
    this.gpuDriverVersion = gpuDriverVersion;
    return this;
  }

   /**
    * Gets the gpu driver version.
    *
    * @return the gpu driver version
    */
  @ApiModelProperty(value = "The version of the GPU driver on the virtual host.")
  public GpuDriverVersionMetric getGpuDriverVersion() {
    return gpuDriverVersion;
  }

  /**
   * Sets the gpu driver version.
   *
   * @param gpuDriverVersion the new gpu driver version
   */
  public void setGpuDriverVersion(GpuDriverVersionMetric gpuDriverVersion) {
    this.gpuDriverVersion = gpuDriverVersion;
  }

  /**
   * Gpu encoder usage.
   *
   * @param gpuEncoderUsage the gpu encoder usage
   * @return the shm metrics
   */
  public ShmMetrics gpuEncoderUsage(GpuEncoderUsageMetric gpuEncoderUsage) {
    this.gpuEncoderUsage = gpuEncoderUsage;
    return this;
  }

   /**
    * Gets the gpu encoder usage.
    *
    * @return the gpu encoder usage
    */
  @ApiModelProperty(value = "The percentage of available GPU encoding power on the virtual host being used by the transcoder.")
  public GpuEncoderUsageMetric getGpuEncoderUsage() {
    return gpuEncoderUsage;
  }

  /**
   * Sets the gpu encoder usage.
   *
   * @param gpuEncoderUsage the new gpu encoder usage
   */
  public void setGpuEncoderUsage(GpuEncoderUsageMetric gpuEncoderUsage) {
    this.gpuEncoderUsage = gpuEncoderUsage;
  }

  /**
   * Gpu memory usage.
   *
   * @param gpuMemoryUsage the gpu memory usage
   * @return the shm metrics
   */
  public ShmMetrics gpuMemoryUsage(GpuMemoryUsageMetric gpuMemoryUsage) {
    this.gpuMemoryUsage = gpuMemoryUsage;
    return this;
  }

   /**
    * Gets the gpu memory usage.
    *
    * @return the gpu memory usage
    */
  @ApiModelProperty(value = "The percentage of the GPU memory usage on the virtual host being used by the transcoder.")
  public GpuMemoryUsageMetric getGpuMemoryUsage() {
    return gpuMemoryUsage;
  }

  /**
   * Sets the gpu memory usage.
   *
   * @param gpuMemoryUsage the new gpu memory usage
   */
  public void setGpuMemoryUsage(GpuMemoryUsageMetric gpuMemoryUsage) {
    this.gpuMemoryUsage = gpuMemoryUsage;
  }

  /**
   * Gpu usage.
   *
   * @param gpuUsage the gpu usage
   * @return the shm metrics
   */
  public ShmMetrics gpuUsage(GpuUsageMetric gpuUsage) {
    this.gpuUsage = gpuUsage;
    return this;
  }

   /**
    * Gets the gpu usage.
    *
    * @return the gpu usage
    */
  @ApiModelProperty(value = "The percentage of the total GPU usage on the virtual host being used by the transcoder.")
  public GpuUsageMetric getGpuUsage() {
    return gpuUsage;
  }

  /**
   * Sets the gpu usage.
   *
   * @param gpuUsage the new gpu usage
   */
  public void setGpuUsage(GpuUsageMetric gpuUsage) {
    this.gpuUsage = gpuUsage;
  }

  /**
   * Height.
   *
   * @param height the height
   * @return the shm metrics
   */
  public ShmMetrics height(HeightMetric height) {
    this.height = height;
    return this;
  }

   /**
    * Gets the height.
    *
    * @return the height
    */
  @ApiModelProperty(value = "The height of the frame of the video source frame, in pixels.")
  public HeightMetric getHeight() {
    return height;
  }

  /**
   * Sets the height.
   *
   * @param height the new height
   */
  public void setHeight(HeightMetric height) {
    this.height = height;
  }

  /**
   * Keyframe interval.
   *
   * @param keyframeInterval the keyframe interval
   * @return the shm metrics
   */
  public ShmMetrics keyframeInterval(KeyframeIntervalMetric keyframeInterval) {
    this.keyframeInterval = keyframeInterval;
    return this;
  }

   /**
    * Gets the keyframe interval.
    *
    * @return the keyframe interval
    */
  @ApiModelProperty(value = "The number of video frames compressed in a group of pictures (GOP) between keyframes.")
  public KeyframeIntervalMetric getKeyframeInterval() {
    return keyframeInterval;
  }

  /**
   * Sets the keyframe interval.
   *
   * @param keyframeInterval the new keyframe interval
   */
  public void setKeyframeInterval(KeyframeIntervalMetric keyframeInterval) {
    this.keyframeInterval = keyframeInterval;
  }

  /**
   * Stream target status OUTPUTIDXSTREAMTARGETIDX.
   *
   * @param streamTargetStatusOUTPUTIDXSTREAMTARGETIDX the stream target status OUTPUTIDXSTREAMTARGETIDX
   * @return the shm metrics
   */
  public ShmMetrics streamTargetStatusOUTPUTIDXSTREAMTARGETIDX(StreamTargetStatusOUTPUTIDXSTREAMTARGETIDXMetric streamTargetStatusOUTPUTIDXSTREAMTARGETIDX) {
    this.streamTargetStatusOUTPUTIDXSTREAMTARGETIDX = streamTargetStatusOUTPUTIDXSTREAMTARGETIDX;
    return this;
  }

   /**
    * Gets the stream target status OUTPUTIDXSTREAMTARGETIDX.
    *
    * @return the stream target status OUTPUTIDXSTREAMTARGETIDX
    */
  @ApiModelProperty(value = "The status of the identified stream target assigned to the identified output. OUTPUTIDX identifies the output and STREAMTARGETIDX identifies the stream target. A status is returned for every stream target used by the transcoder.")
  public StreamTargetStatusOUTPUTIDXSTREAMTARGETIDXMetric getStreamTargetStatusOUTPUTIDXSTREAMTARGETIDX() {
    return streamTargetStatusOUTPUTIDXSTREAMTARGETIDX;
  }

  /**
   * Sets the stream target status OUTPUTIDXSTREAMTARGETIDX.
   *
   * @param streamTargetStatusOUTPUTIDXSTREAMTARGETIDX the new stream target status OUTPUTIDXSTREAMTARGETIDX
   */
  public void setStreamTargetStatusOUTPUTIDXSTREAMTARGETIDX(StreamTargetStatusOUTPUTIDXSTREAMTARGETIDXMetric streamTargetStatusOUTPUTIDXSTREAMTARGETIDX) {
    this.streamTargetStatusOUTPUTIDXSTREAMTARGETIDX = streamTargetStatusOUTPUTIDXSTREAMTARGETIDX;
  }

  /**
   * Unique views.
   *
   * @param uniqueViews the unique views
   * @return the shm metrics
   */
  public ShmMetrics uniqueViews(UniqueViewsMetric uniqueViews) {
    this.uniqueViews = uniqueViews;
    return this;
  }

   /**
    * Gets the unique views.
    *
    * @return the unique views
    */
  @ApiModelProperty(value = "The number of IP addresses that received at least one chunk of the stream on any player or device in a 24-hour period.")
  public UniqueViewsMetric getUniqueViews() {
    return uniqueViews;
  }

  /**
   * Sets the unique views.
   *
   * @param uniqueViews the new unique views
   */
  public void setUniqueViews(UniqueViewsMetric uniqueViews) {
    this.uniqueViews = uniqueViews;
  }

  /**
   * Video codec.
   *
   * @param videoCodec the video codec
   * @return the shm metrics
   */
  public ShmMetrics videoCodec(VideoCodecMetric videoCodec) {
    this.videoCodec = videoCodec;
    return this;
  }

   /**
    * Gets the video codec.
    *
    * @return the video codec
    */
  @ApiModelProperty(value = "The video codec of the video source.")
  public VideoCodecMetric getVideoCodec() {
    return videoCodec;
  }

  /**
   * Sets the video codec.
   *
   * @param videoCodec the new video codec
   */
  public void setVideoCodec(VideoCodecMetric videoCodec) {
    this.videoCodec = videoCodec;
  }

  /**
   * Width.
   *
   * @param width the width
   * @return the shm metrics
   */
  public ShmMetrics width(WidthMetric width) {
    this.width = width;
    return this;
  }

   /**
    * Gets the width.
    *
    * @return the width
    */
  @ApiModelProperty(value = "The width of the frame of the video source, in pixels.")
  public WidthMetric getWidth() {
    return width;
  }

  /**
   * Sets the width.
   *
   * @param width the new width
   */
  public void setWidth(WidthMetric width) {
    this.width = width;
  }


  /**
   * Equals.
   *
   * @param o the o
   * @return true, if successful
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ShmMetrics shmMetrics = (ShmMetrics) o;
    return Objects.equals(this.audioCodec, shmMetrics.audioCodec) &&
        Objects.equals(this.bitsInRate, shmMetrics.bitsInRate) &&
        Objects.equals(this.bitsOutRate, shmMetrics.bitsOutRate) &&
        Objects.equals(this.bytesInRate, shmMetrics.bytesInRate) &&
        Objects.equals(this.bytesOutRate, shmMetrics.bytesOutRate) &&
        Objects.equals(this.configuredBytesOutRate, shmMetrics.configuredBytesOutRate) &&
        Objects.equals(this.connected, shmMetrics.connected) &&
        Objects.equals(this.cpu, shmMetrics.cpu) &&
        Objects.equals(this.frameSize, shmMetrics.frameSize) &&
        Objects.equals(this.frameRate, shmMetrics.frameRate) &&
        Objects.equals(this.gpuDecoderUsage, shmMetrics.gpuDecoderUsage) &&
        Objects.equals(this.gpuDriverVersion, shmMetrics.gpuDriverVersion) &&
        Objects.equals(this.gpuEncoderUsage, shmMetrics.gpuEncoderUsage) &&
        Objects.equals(this.gpuMemoryUsage, shmMetrics.gpuMemoryUsage) &&
        Objects.equals(this.gpuUsage, shmMetrics.gpuUsage) &&
        Objects.equals(this.height, shmMetrics.height) &&
        Objects.equals(this.keyframeInterval, shmMetrics.keyframeInterval) &&
        Objects.equals(this.streamTargetStatusOUTPUTIDXSTREAMTARGETIDX, shmMetrics.streamTargetStatusOUTPUTIDXSTREAMTARGETIDX) &&
        Objects.equals(this.uniqueViews, shmMetrics.uniqueViews) &&
        Objects.equals(this.videoCodec, shmMetrics.videoCodec) &&
        Objects.equals(this.width, shmMetrics.width);
  }

  /**
   * Hash code.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    return Objects.hash(audioCodec, bitsInRate, bitsOutRate, bytesInRate, bytesOutRate, configuredBytesOutRate, connected, cpu, frameSize, frameRate, gpuDecoderUsage, gpuDriverVersion, gpuEncoderUsage, gpuMemoryUsage, gpuUsage, height, keyframeInterval, streamTargetStatusOUTPUTIDXSTREAMTARGETIDX, uniqueViews, videoCodec, width);
  }


  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ShmMetrics {\n");
    
    sb.append("    audioCodec: ").append(toIndentedString(audioCodec)).append("\n");
    sb.append("    bitsInRate: ").append(toIndentedString(bitsInRate)).append("\n");
    sb.append("    bitsOutRate: ").append(toIndentedString(bitsOutRate)).append("\n");
    sb.append("    bytesInRate: ").append(toIndentedString(bytesInRate)).append("\n");
    sb.append("    bytesOutRate: ").append(toIndentedString(bytesOutRate)).append("\n");
    sb.append("    configuredBytesOutRate: ").append(toIndentedString(configuredBytesOutRate)).append("\n");
    sb.append("    connected: ").append(toIndentedString(connected)).append("\n");
    sb.append("    cpu: ").append(toIndentedString(cpu)).append("\n");
    sb.append("    frameSize: ").append(toIndentedString(frameSize)).append("\n");
    sb.append("    frameRate: ").append(toIndentedString(frameRate)).append("\n");
    sb.append("    gpuDecoderUsage: ").append(toIndentedString(gpuDecoderUsage)).append("\n");
    sb.append("    gpuDriverVersion: ").append(toIndentedString(gpuDriverVersion)).append("\n");
    sb.append("    gpuEncoderUsage: ").append(toIndentedString(gpuEncoderUsage)).append("\n");
    sb.append("    gpuMemoryUsage: ").append(toIndentedString(gpuMemoryUsage)).append("\n");
    sb.append("    gpuUsage: ").append(toIndentedString(gpuUsage)).append("\n");
    sb.append("    height: ").append(toIndentedString(height)).append("\n");
    sb.append("    keyframeInterval: ").append(toIndentedString(keyframeInterval)).append("\n");
    sb.append("    streamTargetStatusOUTPUTIDXSTREAMTARGETIDX: ").append(toIndentedString(streamTargetStatusOUTPUTIDXSTREAMTARGETIDX)).append("\n");
    sb.append("    uniqueViews: ").append(toIndentedString(uniqueViews)).append("\n");
    sb.append("    videoCodec: ").append(toIndentedString(videoCodec)).append("\n");
    sb.append("    width: ").append(toIndentedString(width)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * To indented string.
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

