/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.cass.dvo;

import java.io.Serializable;

/**
 * The Class CustomerAnswerDVO.
 */

public class CustomerAnswerDVO implements Serializable {

	private static final long serialVersionUID = 6274949128569802856L;

	/** The conid. */
	private String conid;

	/** The custid. */
	private String custid;

	/** The custplayid. */
	private String custplayid;

	/** The custans. */
	private String custans;

	/** The conname. */
	private String conname;

	/** The qsnbnkans. */
	private String qsnbnkans;

	/** The rwdtype. */
	private String rwdtype;

	/** The qsnbnkid. */
	private Integer qsnbnkid;

	/** The qsnseqno. */
	private Integer qsnseqno;

	/** The consrtdate. */
	private long consrtdate;

	/** The credate. */
	private long credate;

	/** The upddate. */
	private long upddate;

	/** The iscrtans. */
	private boolean iscrtans;

	/** The rwdvalue. */
	private Double rwdvalue;

	/**
	 * Gets the conid.
	 *
	 * @return the conid
	 */
	public String getConid() {
		return conid;
	}

	/**
	 * Sets the conid.
	 *
	 * @param conid
	 *            the new conid
	 */
	public void setConid(String conid) {
		this.conid = conid;
	}

	/**
	 * Gets the custid.
	 *
	 * @return the custid
	 */
	public String getCustid() {
		return custid;
	}

	/**
	 * Sets the custid.
	 *
	 * @param custid
	 *            the new custid
	 */
	public void setCustid(String custid) {
		this.custid = custid;
	}

	/**
	 * Gets the custplayid.
	 *
	 * @return the custplayid
	 */
	public String getCustplayid() {
		return custplayid;
	}

	/**
	 * Sets the custplayid.
	 *
	 * @param custplayid
	 *            the new custplayid
	 */
	public void setCustplayid(String custplayid) {
		this.custplayid = custplayid;
	}

	/**
	 * Gets the custans.
	 *
	 * @return the custans
	 */
	public String getCustans() {
		return custans;
	}

	/**
	 * Sets the custans.
	 *
	 * @param custans
	 *            the new custans
	 */
	public void setCustans(String custans) {
		this.custans = custans;
	}

	/**
	 * Gets the conname.
	 *
	 * @return the conname
	 */
	public String getConname() {
		return conname;
	}

	/**
	 * Sets the conname.
	 *
	 * @param conname
	 *            the new conname
	 */
	public void setConname(String conname) {
		this.conname = conname;
	}

	/**
	 * Gets the qsnbnkans.
	 *
	 * @return the qsnbnkans
	 */
	public String getQsnbnkans() {
		return qsnbnkans;
	}

	/**
	 * Sets the qsnbnkans.
	 *
	 * @param qsnbnkans
	 *            the new qsnbnkans
	 */
	public void setQsnbnkans(String qsnbnkans) {
		this.qsnbnkans = qsnbnkans;
	}

	/**
	 * Gets the rwdtype.
	 *
	 * @return the rwdtype
	 */
	public String getRwdtype() {
		return rwdtype;
	}

	/**
	 * Sets the rwdtype.
	 *
	 * @param rwdtype
	 *            the new rwdtype
	 */
	public void setRwdtype(String rwdtype) {
		this.rwdtype = rwdtype;
	}

	/**
	 * Gets the qsnbnkid.
	 *
	 * @return the qsnbnkid
	 */
	public Integer getQsnbnkid() {
		return qsnbnkid;
	}

	/**
	 * Sets the qsnbnkid.
	 *
	 * @param qsnbnkid
	 *            the new qsnbnkid
	 */
	public void setQsnbnkid(Integer qsnbnkid) {
		this.qsnbnkid = qsnbnkid;
	}

	/**
	 * Gets the qsnseqno.
	 *
	 * @return the qsnseqno
	 */
	public Integer getQsnseqno() {
		return qsnseqno;
	}

	/**
	 * Sets the qsnseqno.
	 *
	 * @param qsnseqno
	 *            the new qsnseqno
	 */
	public void setQsnseqno(Integer qsnseqno) {
		this.qsnseqno = qsnseqno;
	}

	/**
	 * Gets the consrtdate.
	 *
	 * @return the consrtdate
	 */
	public long getConsrtdate() {
		return consrtdate;
	}

	/**
	 * Sets the consrtdate.
	 *
	 * @param consrtdate
	 *            the new consrtdate
	 */
	public void setConsrtdate(long consrtdate) {
		this.consrtdate = consrtdate;
	}

	/**
	 * Gets the credate.
	 *
	 * @return the credate
	 */
	public long getCredate() {
		return credate;
	}

	/**
	 * Sets the credate.
	 *
	 * @param credate
	 *            the new credate
	 */
	public void setCredate(long credate) {
		this.credate = credate;
	}

	/**
	 * Gets the upddate.
	 *
	 * @return the upddate
	 */
	public long getUpddate() {
		return upddate;
	}

	/**
	 * Sets the upddate.
	 *
	 * @param upddate
	 *            the new upddate
	 */
	public void setUpddate(long upddate) {
		this.upddate = upddate;
	}

	/**
	 * Checks if is iscrtans.
	 *
	 * @return true, if is iscrtans
	 */
	public boolean isIscrtans() {
		return iscrtans;
	}

	/**
	 * Sets the iscrtans.
	 *
	 * @param iscrtans
	 *            the new iscrtans
	 */
	public void setIscrtans(boolean iscrtans) {
		this.iscrtans = iscrtans;
	}

	/**
	 * Gets the rwdvalue.
	 *
	 * @return the rwdvalue
	 */
	public Double getRwdvalue() {
		if (rwdvalue == null) {
			rwdvalue = 0.0;
		}
		return rwdvalue;
	}

	/**
	 * Sets the rwdvalue.
	 *
	 * @param rwdvalue
	 *            the new rwdvalue
	 */
	public void setRwdvalue(Double rwdvalue) {
		this.rwdvalue = rwdvalue;
	}

}
