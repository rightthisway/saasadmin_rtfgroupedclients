/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.cass.dvo;

import java.io.Serializable;

/**
 * The Class QuestionBankDVO.
 */
public class QuestionBankDVO implements Serializable {

	private static final long serialVersionUID = 2251405121563380812L;

	/** The isact. */
	private Boolean isact;

	/** The ctyp. */
	private String ctyp;

	/** The sctyp. */
	private String sctyp;

	/** The qsnmode. */
	private String qsnmode;

	/** The qtx. */
	private String qtx;

	/** The opa. */
	private String opa;

	/** The opb. */
	private String opb;

	/** The opc. */
	private String opc;

	/** The opd. */
	private String opd;

	/** The anstype. */
	// FEEDBACK OR REGULAR
	private String anstype;

	/** The cans. */
	private String cans;

	/** The qbid. */
	private Integer qbid;

	/** The q orientn. */
	// DISPLAY ANSWER OPTIONS HORIZONTAL OR VERTICAL
	private String qOrientn;

	/** The cl id. */
	private String clId;

	/**
	 * Gets the qsnmode.
	 *
	 * @return the qsnmode
	 */
	public String getQsnmode() {
		return qsnmode;
	}

	/**
	 * Sets the qsnmode.
	 *
	 * @param qsnmode
	 *            the new qsnmode
	 */
	public void setQsnmode(String qsnmode) {
		this.qsnmode = qsnmode;
	}

	/**
	 * Gets the anstype.
	 *
	 * @return the anstype
	 */
	public String getAnstype() {
		return anstype;
	}

	/**
	 * Sets the anstype.
	 *
	 * @param anstype
	 *            the new anstype
	 */
	public void setAnstype(String anstype) {
		this.anstype = anstype;
	}

	/**
	 * Gets the q orientn.
	 *
	 * @return the q orientn
	 */
	public String getqOrientn() {
		return qOrientn;
	}

	/**
	 * Sets the q orientn.
	 *
	 * @param qOrientn
	 *            the new q orientn
	 */
	public void setqOrientn(String qOrientn) {
		this.qOrientn = qOrientn;
	}

	/**
	 * Gets the isact.
	 *
	 * @return the isact
	 */
	public Boolean getIsact() {
		return isact;
	}

	/**
	 * Sets the isact.
	 *
	 * @param isact
	 *            the new isact
	 */
	public void setIsact(Boolean isact) {
		this.isact = isact;
	}

	/**
	 * Gets the qtx.
	 *
	 * @return the qtx
	 */
	public String getQtx() {
		return qtx;
	}

	/**
	 * Sets the qtx.
	 *
	 * @param qtx
	 *            the new qtx
	 */
	public void setQtx(String qtx) {
		this.qtx = qtx;
	}

	/**
	 * Gets the qbid.
	 *
	 * @return the qbid
	 */
	public Integer getQbid() {
		return qbid;
	}

	/**
	 * Sets the qbid.
	 *
	 * @param qbid
	 *            the new qbid
	 */
	public void setQbid(Integer qbid) {
		this.qbid = qbid;
	}

	/**
	 * Gets the opa.
	 *
	 * @return the opa
	 */
	public String getOpa() {
		return opa;
	}

	/**
	 * Sets the opa.
	 *
	 * @param opa
	 *            the new opa
	 */
	public void setOpa(String opa) {
		this.opa = opa;
	}

	/**
	 * Gets the opb.
	 *
	 * @return the opb
	 */
	public String getOpb() {
		return opb;
	}

	/**
	 * Sets the opb.
	 *
	 * @param opb
	 *            the new opb
	 */
	public void setOpb(String opb) {
		this.opb = opb;
	}

	/**
	 * Gets the opc.
	 *
	 * @return the opc
	 */
	public String getOpc() {
		return opc;
	}

	/**
	 * Sets the opc.
	 *
	 * @param opc
	 *            the new opc
	 */
	public void setOpc(String opc) {
		this.opc = opc;
	}

	/**
	 * Gets the opd.
	 *
	 * @return the opd
	 */
	public String getOpd() {
		return opd;
	}

	/**
	 * Sets the opd.
	 *
	 * @param opd
	 *            the new opd
	 */
	public void setOpd(String opd) {
		this.opd = opd;
	}

	/**
	 * Gets the cans.
	 *
	 * @return the cans
	 */
	public String getCans() {
		return cans;
	}

	/**
	 * Sets the cans.
	 *
	 * @param cans
	 *            the new cans
	 */
	public void setCans(String cans) {
		this.cans = cans;
	}

	/**
	 * Gets the ctyp.
	 *
	 * @return the ctyp
	 */
	public String getCtyp() {
		return ctyp;
	}

	/**
	 * Sets the ctyp.
	 *
	 * @param ctyp
	 *            the new ctyp
	 */
	public void setCtyp(String ctyp) {
		this.ctyp = ctyp;
	}

	/**
	 * Gets the sctyp.
	 *
	 * @return the sctyp
	 */
	public String getSctyp() {
		return sctyp;
	}

	/**
	 * Sets the sctyp.
	 *
	 * @param sctyp
	 *            the new sctyp
	 */
	public void setSctyp(String sctyp) {
		this.sctyp = sctyp;
	}

	/**
	 * Gets the cl id.
	 *
	 * @return the cl id
	 */
	public String getClId() {
		return clId;
	}

	/**
	 * Sets the cl id.
	 *
	 * @param clId
	 *            the new cl id
	 */
	public void setClId(String clId) {
		this.clId = clId;
	}

}
