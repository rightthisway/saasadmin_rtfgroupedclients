/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.cass.dvo;

import java.io.Serializable;

/**
 * The Class ContestPlayerTrackingDVO.
 */
public class ContestPlayerTrackingDVO implements Serializable {

	private static final long serialVersionUID = -5309380803653537250L;

	/** The co id. */
	private String coId;

	/** The cu id. */
	private String cuId;

	/** The player id. */
	private String playerId;

	/** The play time. */
	private Long playTime;

	/** The sts. */
	private String sts;

	/** The q cnt. */
	private Integer qCnt;

	/**
	 * Gets the co id.
	 *
	 * @return the co id
	 */
	public String getCoId() {
		return coId;
	}

	/**
	 * Sets the co id.
	 *
	 * @param coId
	 *            the new co id
	 */
	public void setCoId(String coId) {
		this.coId = coId;
	}

	/**
	 * Gets the cu id.
	 *
	 * @return the cu id
	 */
	public String getCuId() {
		return cuId;
	}

	/**
	 * Sets the cu id.
	 *
	 * @param cuId
	 *            the new cu id
	 */
	public void setCuId(String cuId) {
		this.cuId = cuId;
	}

	/**
	 * Gets the player id.
	 *
	 * @return the player id
	 */
	public String getPlayerId() {
		return playerId;
	}

	/**
	 * Sets the player id.
	 *
	 * @param playerId
	 *            the new player id
	 */
	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}

	/**
	 * Gets the play time.
	 *
	 * @return the play time
	 */
	public Long getPlayTime() {
		return playTime;
	}

	/**
	 * Sets the play time.
	 *
	 * @param playTime
	 *            the new play time
	 */
	public void setPlayTime(Long playTime) {
		this.playTime = playTime;
	}

	/**
	 * Gets the sts.
	 *
	 * @return the sts
	 */
	public String getSts() {
		return sts;
	}

	/**
	 * Sets the sts.
	 *
	 * @param sts
	 *            the new sts
	 */
	public void setSts(String sts) {
		this.sts = sts;
	}

	/**
	 * Gets the q cnt.
	 *
	 * @return the q cnt
	 */
	public Integer getqCnt() {
		return qCnt;
	}

	/**
	 * Sets the q cnt.
	 *
	 * @param qCnt
	 *            the new q cnt
	 */
	public void setqCnt(Integer qCnt) {
		this.qCnt = qCnt;
	}
}
