/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.cass.dvo;

import java.io.Serializable;

/**
 * The Class ContestQuestionDVO.
 */
public class ContestQuestionDVO implements Serializable {

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "ContestQuestionDVO [isactive=" + isactive + ", conid=" + conid + ", ctyp=" + ctyp + ", sctyp=" + sctyp
				+ ", qtx=" + qtx + ", qbid=" + qbid + ", opa=" + opa + ", opb=" + opb + ", opc=" + opc + ", opd=" + opd
				+ ", anstype=" + anstype + ", conqsnid=" + conqsnid + ", cans=" + cans + ", qsnmode=" + qsnmode
				+ ", qsnorient=" + qsnorient + ", rwdtype=" + rwdtype + ", rwdvalue=" + rwdvalue + ", qsnseqno="
				+ qsnseqno + ", getQsnseqno()=" + getQsnseqno() + ", getIsactive()=" + getIsactive() + ", getConid()="
				+ getConid() + ", getCtyp()=" + getCtyp() + ", getSctyp()=" + getSctyp() + ", getQtx()=" + getQtx()
				+ ", getQbid()=" + getQbid() + ", getOpa()=" + getOpa() + ", getOpb()=" + getOpb() + ", getOpc()="
				+ getOpc() + ", getOpd()=" + getOpd() + ", getAnstype()=" + getAnstype() + ", getConqsnid()="
				+ getConqsnid() + ", getCans()=" + getCans() + ", getQsnmode()=" + getQsnmode() + ", getQsnorient()="
				+ getQsnorient() + ", getRwdtype()=" + getRwdtype() + ", getRwdvalue()=" + getRwdvalue()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString()
				+ "]";
	}

	/** The isactive. */
	private Boolean isactive;

	/** The conid. */
	private String conid;

	/** The ctyp. */
	private String ctyp;

	/** The sctyp. */
	private String sctyp;

	/** The qtx. */
	private String qtx;

	/** The qbid. */
	private Integer qbid;

	/** The opa. */
	private String opa;

	/** The opb. */
	private String opb;

	/** The opc. */
	private String opc;

	/** The opd. */
	private String opd;

	/** The anstype. */
	private String anstype;

	/** The conqsnid. */
	private Integer conqsnid;

	/** The cans. */
	private String cans;

	/** The qsnmode. */
	private String qsnmode;

	/** The qsnorient. */
	private String qsnorient;

	/** The rwdtype. */
	private String rwdtype;

	/** The rwdvalue. */
	private Double rwdvalue;

	/** The qsnseqno. */
	private Integer qsnseqno;

	/**
	 * Gets the qsnseqno.
	 *
	 * @return the qsnseqno
	 */
	public Integer getQsnseqno() {
		return qsnseqno;
	}

	/**
	 * Sets the qsnseqno.
	 *
	 * @param qsnseqno
	 *            the new qsnseqno
	 */
	public void setQsnseqno(Integer qsnseqno) {
		this.qsnseqno = qsnseqno;
	}

	/**
	 * Gets the isactive.
	 *
	 * @return the isactive
	 */
	public Boolean getIsactive() {
		return isactive;
	}

	/**
	 * Sets the isactive.
	 *
	 * @param isactive
	 *            the new isactive
	 */
	public void setIsactive(Boolean isactive) {
		this.isactive = isactive;
	}

	/**
	 * Gets the conid.
	 *
	 * @return the conid
	 */
	public String getConid() {
		return conid;
	}

	/**
	 * Sets the conid.
	 *
	 * @param conid
	 *            the new conid
	 */
	public void setConid(String conid) {
		this.conid = conid;
	}

	/**
	 * Gets the ctyp.
	 *
	 * @return the ctyp
	 */
	public String getCtyp() {
		return ctyp;
	}

	/**
	 * Sets the ctyp.
	 *
	 * @param ctyp
	 *            the new ctyp
	 */
	public void setCtyp(String ctyp) {
		this.ctyp = ctyp;
	}

	/**
	 * Gets the sctyp.
	 *
	 * @return the sctyp
	 */
	public String getSctyp() {
		return sctyp;
	}

	/**
	 * Sets the sctyp.
	 *
	 * @param sctyp
	 *            the new sctyp
	 */
	public void setSctyp(String sctyp) {
		this.sctyp = sctyp;
	}

	/**
	 * Gets the qtx.
	 *
	 * @return the qtx
	 */
	public String getQtx() {
		return qtx;
	}

	/**
	 * Sets the qtx.
	 *
	 * @param qtx
	 *            the new qtx
	 */
	public void setQtx(String qtx) {
		this.qtx = qtx;
	}

	/**
	 * Gets the qbid.
	 *
	 * @return the qbid
	 */
	public Integer getQbid() {
		return qbid;
	}

	/**
	 * Sets the qbid.
	 *
	 * @param qbid
	 *            the new qbid
	 */
	public void setQbid(Integer qbid) {
		this.qbid = qbid;
	}

	/**
	 * Gets the opa.
	 *
	 * @return the opa
	 */
	public String getOpa() {
		return opa;
	}

	/**
	 * Sets the opa.
	 *
	 * @param opa
	 *            the new opa
	 */
	public void setOpa(String opa) {
		this.opa = opa;
	}

	/**
	 * Gets the opb.
	 *
	 * @return the opb
	 */
	public String getOpb() {
		return opb;
	}

	/**
	 * Sets the opb.
	 *
	 * @param opb
	 *            the new opb
	 */
	public void setOpb(String opb) {
		this.opb = opb;
	}

	/**
	 * Gets the opc.
	 *
	 * @return the opc
	 */
	public String getOpc() {
		return opc;
	}

	/**
	 * Sets the opc.
	 *
	 * @param opc
	 *            the new opc
	 */
	public void setOpc(String opc) {
		this.opc = opc;
	}

	/**
	 * Gets the opd.
	 *
	 * @return the opd
	 */
	public String getOpd() {
		return opd;
	}

	/**
	 * Sets the opd.
	 *
	 * @param opd
	 *            the new opd
	 */
	public void setOpd(String opd) {
		this.opd = opd;
	}

	/**
	 * Gets the anstype.
	 *
	 * @return the anstype
	 */
	public String getAnstype() {
		return anstype;
	}

	/**
	 * Sets the anstype.
	 *
	 * @param anstype
	 *            the new anstype
	 */
	public void setAnstype(String anstype) {
		this.anstype = anstype;
	}

	/**
	 * Gets the conqsnid.
	 *
	 * @return the conqsnid
	 */
	public Integer getConqsnid() {
		return conqsnid;
	}

	/**
	 * Sets the conqsnid.
	 *
	 * @param conqsnid
	 *            the new conqsnid
	 */
	public void setConqsnid(Integer conqsnid) {
		this.conqsnid = conqsnid;
	}

	/**
	 * Gets the cans.
	 *
	 * @return the cans
	 */
	public String getCans() {
		return cans;
	}

	/**
	 * Sets the cans.
	 *
	 * @param cans
	 *            the new cans
	 */
	public void setCans(String cans) {
		this.cans = cans;
	}

	/**
	 * Gets the qsnmode.
	 *
	 * @return the qsnmode
	 */
	public String getQsnmode() {
		return qsnmode;
	}

	/**
	 * Sets the qsnmode.
	 *
	 * @param qsnmode
	 *            the new qsnmode
	 */
	public void setQsnmode(String qsnmode) {
		this.qsnmode = qsnmode;
	}

	/**
	 * Gets the qsnorient.
	 *
	 * @return the qsnorient
	 */
	public String getQsnorient() {
		return qsnorient;
	}

	/**
	 * Sets the qsnorient.
	 *
	 * @param qsnorient
	 *            the new qsnorient
	 */
	public void setQsnorient(String qsnorient) {
		this.qsnorient = qsnorient;
	}

	/**
	 * Gets the rwdtype.
	 *
	 * @return the rwdtype
	 */
	public String getRwdtype() {
		return rwdtype;
	}

	/**
	 * Sets the rwdtype.
	 *
	 * @param rwdtype
	 *            the new rwdtype
	 */
	public void setRwdtype(String rwdtype) {
		this.rwdtype = rwdtype;
	}

	/**
	 * Gets the rwdvalue.
	 *
	 * @return the rwdvalue
	 */
	public Double getRwdvalue() {
		return rwdvalue;
	}

	/**
	 * Sets the rwdvalue.
	 *
	 * @param rwdvalue
	 *            the new rwdvalue
	 */
	public void setRwdvalue(Double rwdvalue) {
		this.rwdvalue = rwdvalue;
	}

}
