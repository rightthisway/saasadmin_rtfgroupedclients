/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.cass.dvo;

import java.io.Serializable;

/**
 * The Class ContestWinnerDVO.
 */
public class ContestWinnerDVO implements Serializable {

	private static final long serialVersionUID = 4931201177352237112L;

	/** The co id. */
	private String coId;

	/** The cu id. */
	private String cuId;

	/** The co name. */
	private String coName;

	/** The co date. */
	private Long coDate;

	/** The rwd type. */
	private String rwdType;

	/** The rwd val. */
	private Double rwdVal;

	/** The cr date. */
	private Long crDate;

	/** The cr by. */
	private String crBy;

	/** The up date. */
	private Long upDate;

	/** The up by. */
	private String upBy;

	/** The sts. */
	private String sts;

	/**
	 * Gets the co id.
	 *
	 * @return the co id
	 */
	public String getCoId() {
		return coId;
	}

	/**
	 * Sets the co id.
	 *
	 * @param coId
	 *            the new co id
	 */
	public void setCoId(String coId) {
		this.coId = coId;
	}

	/**
	 * Gets the cu id.
	 *
	 * @return the cu id
	 */
	public String getCuId() {
		return cuId;
	}

	/**
	 * Sets the cu id.
	 *
	 * @param cuId
	 *            the new cu id
	 */
	public void setCuId(String cuId) {
		this.cuId = cuId;
	}

	/**
	 * Gets the rwd type.
	 *
	 * @return the rwd type
	 */
	public String getRwdType() {
		return rwdType;
	}

	/**
	 * Sets the rwd type.
	 *
	 * @param rwdType
	 *            the new rwd type
	 */
	public void setRwdType(String rwdType) {
		this.rwdType = rwdType;
	}

	/**
	 * Gets the rwd val.
	 *
	 * @return the rwd val
	 */
	public Double getRwdVal() {
		return rwdVal;
	}

	/**
	 * Sets the rwd val.
	 *
	 * @param rwdVal
	 *            the new rwd val
	 */
	public void setRwdVal(Double rwdVal) {
		this.rwdVal = rwdVal;
	}

	/**
	 * Gets the sts.
	 *
	 * @return the sts
	 */
	public String getSts() {
		return sts;
	}

	/**
	 * Sets the sts.
	 *
	 * @param sts
	 *            the new sts
	 */
	public void setSts(String sts) {
		this.sts = sts;
	}

	/**
	 * Gets the co name.
	 *
	 * @return the co name
	 */
	public String getCoName() {
		return coName;
	}

	/**
	 * Sets the co name.
	 *
	 * @param coName
	 *            the new co name
	 */
	public void setCoName(String coName) {
		this.coName = coName;
	}

	/**
	 * Gets the co date.
	 *
	 * @return the co date
	 */
	public Long getCoDate() {
		return coDate;
	}

	/**
	 * Sets the co date.
	 *
	 * @param coDate
	 *            the new co date
	 */
	public void setCoDate(Long coDate) {
		this.coDate = coDate;
	}

	/**
	 * Gets the cr date.
	 *
	 * @return the cr date
	 */
	public Long getCrDate() {
		return crDate;
	}

	/**
	 * Sets the cr date.
	 *
	 * @param crDate
	 *            the new cr date
	 */
	public void setCrDate(Long crDate) {
		this.crDate = crDate;
	}

	/**
	 * Gets the cr by.
	 *
	 * @return the cr by
	 */
	public String getCrBy() {
		return crBy;
	}

	/**
	 * Sets the cr by.
	 *
	 * @param crBy
	 *            the new cr by
	 */
	public void setCrBy(String crBy) {
		this.crBy = crBy;
	}

	/**
	 * Gets the up date.
	 *
	 * @return the up date
	 */
	public Long getUpDate() {
		return upDate;
	}

	/**
	 * Sets the up date.
	 *
	 * @param upDate
	 *            the new up date
	 */
	public void setUpDate(Long upDate) {
		this.upDate = upDate;
	}

	/**
	 * Gets the up by.
	 *
	 * @return the up by
	 */
	public String getUpBy() {
		return upBy;
	}

	/**
	 * Sets the up by.
	 *
	 * @param upBy
	 *            the new up by
	 */
	public void setUpBy(String upBy) {
		this.upBy = upBy;
	}

}
