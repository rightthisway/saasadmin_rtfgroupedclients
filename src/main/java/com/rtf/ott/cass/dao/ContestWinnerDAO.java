/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.cass.dao;

import com.datastax.driver.core.exceptions.DriverException;
import com.rtf.ott.cass.dvo.ContestWinnerDVO;
import com.rtf.saas.cass.db.CassandraConnector;
import com.rtf.saas.exception.RTFDataAccessException;
import com.rtf.saas.util.GenUtil;

/**
 * The Class ContestWinnerDAO.
 */
public class ContestWinnerDAO {

	/**
	 * Save contest winner.
	 *
	 * @param w
	 *            the w
	 * @return the boolean
	 * @throws Exception
	 *             the exception
	 */
	public static Boolean saveContestWinner(ContestWinnerDVO w) throws Exception {
		try {
			CassandraConnector.getSession().execute(
					"INSERT INTO pt_ofltx_contest_winner (conid,custid,rwdtype,conname,consrtdate,creby,credate,rwdval,status,updby,upddate)  VALUES (?,?, ?,?,?,?,toTimestamp(now()), ?,?,?,?)",
					w.getCoId(), w.getCuId(), w.getRwdType(), w.getCoName(), w.getCoDate(), w.getCrBy(), w.getRwdVal(),
					w.getSts(), w.getUpBy(), w.getUpDate());
			return true;
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
	}
}
