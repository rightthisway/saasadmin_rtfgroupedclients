/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.cass.dao;

import java.util.ArrayList;
import java.util.List;

import com.datastax.driver.core.BatchStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.exceptions.DriverException;
import com.rtf.ott.cass.dvo.CustomerAnswerDVO;
import com.rtf.saas.cass.db.CassandraConnector;
import com.rtf.saas.exception.RTFDataAccessException;
import com.rtf.saas.util.GenUtil;

/**
 * The Class ContestQuestionDAO.
 */
public class ContestQuestionDAO {

	/**
	 * Save contestquestions.
	 *
	 * @param obj
	 *            the obj
	 * @return the customer answer DVO
	 * @throws Exception
	 *             the exception
	 */
	public static CustomerAnswerDVO saveContestquestions(com.rtf.ott.sql.dvo.ContestQuestionDVO obj) throws Exception {

		try {
			String keySpace = CassandraConnector.getRtfSassCassKeySpace();
			StringBuffer sql = new StringBuffer();
			sql.append(" insert into ").append(keySpace)
					.append("pa_ofltx_contest_question_only_active  (clintid,conid,conqsnid,qsntext,ansopta,ansoptb,")
					.append("    ansoptc,ansoptd,corans,isactive,qsnbnkid,qsnmode,qsnorient,qsnseqno,rwdtype,rwdval)")
					.append(" values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)  ");

			CassandraConnector.getSession().execute(sql.toString(), obj.getClintid(), obj.getConid(), obj.getConqsnid(),
					obj.getQsntext(), obj.getOpa(), obj.getOpb(), obj.getOpc(), obj.getOpd(), obj.getCorans(),
					obj.getIsactive(), obj.getQbid(), obj.getQsnmode(), obj.getQsnorient(), obj.getQsnseqno(),
					obj.getRwdtype(), obj.getRwdvalue());

		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(ex);
		}
		return null;
	}

	/**
	 * Update contestquestions.
	 *
	 * @param obj
	 *            the obj
	 * @return the customer answer DVO
	 * @throws Exception
	 *             the exception
	 */
	public static CustomerAnswerDVO updateContestquestions(com.rtf.ott.sql.dvo.ContestQuestionDVO obj)
			throws Exception {

		try {
			String keySpace = CassandraConnector.getRtfSassCassKeySpace();
			StringBuffer sql = new StringBuffer();
			sql.append(" update ").append(keySpace)
					.append("pa_ofltx_contest_question_only_active set qsntext=?,ansopta=?,ansoptb=?,")
					.append("    ansoptc=?,ansoptd=?,corans=?,isactive=?,qsnbnkid=?,qsnmode=?,qsnorient=?,qsnseqno=?,rwdtype=?,rwdval=? ")
					.append(" where clintid=? and conid=? and conqsnid=?  ");
			CassandraConnector.getSession().execute(sql.toString(), obj.getQsntext(), obj.getOpa(), obj.getOpb(),
					obj.getOpc(), obj.getOpd(), obj.getCorans(), obj.getIsactive(), obj.getQbid(), obj.getQsnmode(),
					obj.getQsnorient(), obj.getQsnseqno(), obj.getRwdtype(), obj.getRwdvalue(), obj.getClintid(),
					obj.getConid(), obj.getConqsnid());

		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(ex);
		}
		return null;
	}

	/**
	 * Update contest questions seq no.
	 *
	 * @param objList
	 *            the obj list
	 * @throws Exception
	 *             the exception
	 */
	public static void updateContestQuestionsSeqNo(List<com.rtf.ott.sql.dvo.ContestQuestionDVO> objList)
			throws Exception {

		try {
			String keySpace = CassandraConnector.getRtfSassCassKeySpace();
			Session session = CassandraConnector.getSession();
			BatchStatement batchStatement = new BatchStatement();
			StringBuffer sql = new StringBuffer();
			sql.append("  update ").append(keySpace).append("pa_ofltx_contest_question_only_active set qsnseqno=? where clintid=? and conid=? and conqsnid=? ");
			PreparedStatement preparedStatement = session.prepare(sql.toString());
			for (com.rtf.ott.sql.dvo.ContestQuestionDVO obj : objList) {
				batchStatement.add(
						preparedStatement.bind(obj.getQsnseqno(), obj.getClintid(), obj.getConid(), obj.getConqsnid()));
			}

			session.execute(batchStatement);
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(ex);
		}
	}

	/**
	 * Delete contestquestions.
	 *
	 * @param obj
	 *            the obj
	 * @return the customer answer DVO
	 * @throws Exception
	 *             the exception
	 */
	public static CustomerAnswerDVO deleteContestquestions(com.rtf.ott.sql.dvo.ContestQuestionDVO obj)
			throws Exception {

		try {
			String keySpace = CassandraConnector.getRtfSassCassKeySpace();
			StringBuffer sql = new StringBuffer();
			sql.append(" delete from  ").append(keySpace).append("pa_ofltx_contest_question_only_active where clintid=? and conid=? and conqsnid=? ");
			CassandraConnector.getSession().execute(sql.toString(),
					obj.getClintid(), obj.getConid(), obj.getConqsnid());

		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(ex);
		}
		return null;
	}
}
