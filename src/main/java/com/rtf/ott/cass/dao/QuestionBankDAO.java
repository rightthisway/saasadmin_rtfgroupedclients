/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.cass.dao;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.exceptions.DriverException;
import com.rtf.ott.sql.dvo.QuestionBankDVO;
import com.rtf.saas.cass.db.CassandraConnector;

/**
 * The Class QuestionBankDAO.
 */
public class QuestionBankDAO {

	/**
	 * Save question bank.
	 *
	 * @param dvo
	 *            the dvo
	 * @return the boolean
	 * @throws Exception
	 *             the exception
	 */
	public static Boolean saveQuestionBank(QuestionBankDVO dvo) throws Exception {
		boolean isInserted = false;
		Session session = CassandraConnector.getSession();
		StringBuffer sql = new StringBuffer();
		sql.append(" insert into pa_ofltx_question_bank_only_active ")
		.append(" (clintid,cattype,subcattype,qsntext  ")
		.append(",ansopta,ansoptb,ansoptc,ansoptd , anstype ")
		.append(" ,corans ,qsnbnkid,qsnorient) ")
		.append(" values (?,?,?,?,?,?,?,?,?,?,?,?)");
		try {
			PreparedStatement statement = session.prepare(sql.toString());
			BoundStatement boundStatement = statement.bind(dvo.getClintid(), dvo.getCtyp(), dvo.getSctyp(),
					dvo.getQtx(), dvo.getOpa(), dvo.getOpb(), dvo.getOpc(), dvo.getOpd(), dvo.getAnstype(),
					dvo.getCans(), dvo.getQsnbnkid(), dvo.getqOrientn());
			session.execute(boundStatement);
			isInserted = true;
		} catch (final DriverException de) {
			de.printStackTrace();
			isInserted = false;
		} catch (Exception ex) {
			ex.printStackTrace();
			isInserted = false;
		}
		return isInserted;
	}

	/**
	 * Update question bank.
	 *
	 * @param dvo
	 *            the dvo
	 * @return the boolean
	 * @throws Exception
	 *             the exception
	 */
	public static Boolean updateQuestionBank(QuestionBankDVO dvo) throws Exception {
		boolean isInserted = false;
		Session session = CassandraConnector.getSession();
		StringBuffer sql = new StringBuffer();
		sql.append(" update  pa_ofltx_question_bank_only_active ")
		.append(" set ansopta=? ,ansoptb=?,ansoptc=?,ansoptd=? , anstype=? , qsntext = ? ")
		.append(" , corans=? ,qsnorient = ? where  ")
		.append(" clintid = ? and cattype=? and subcattype= ? and qsnbnkid = ? ");

		try {
			PreparedStatement statement = session.prepare(sql.toString());
			BoundStatement boundStatement = statement.bind(

					dvo.getOpa(), dvo.getOpb(), dvo.getOpc(), dvo.getOpd(), dvo.getAnstype(), dvo.getQtx(),

					dvo.getCans(), dvo.getqOrientn(),

					dvo.getClintid(), dvo.getCtyp(), dvo.getSctyp(), dvo.getQsnbnkid()

			);
			session.execute(boundStatement);
			isInserted = true;
		} catch (final DriverException de) {
			de.printStackTrace();
			isInserted = false;
		} catch (Exception ex) {
			ex.printStackTrace();
			isInserted = false;
		}
		return isInserted;
	}

	/**
	 * Delete question bank.
	 *
	 * @param dvo
	 *            the dvo
	 * @return the boolean
	 * @throws Exception
	 *             the exception
	 */
	public static Boolean deleteQuestionBank(QuestionBankDVO dvo) throws Exception {
		boolean isInserted = false;
		Session session = CassandraConnector.getSession();
		StringBuffer sql = new StringBuffer();
		sql.append(" delete from  pa_ofltx_question_bank_only_active ")
		.append("  where  ")
		.append(" clintid = ? and cattype=? and subcattype= ? and qsnbnkid = ? ");

		try {
			PreparedStatement statement = session.prepare(sql.toString());
			BoundStatement boundStatement = statement.bind(dvo.getClintid(), dvo.getCtyp(), dvo.getSctyp(),
					dvo.getQsnbnkid());
			session.execute(boundStatement);
			isInserted = true;
		} catch (final DriverException de) {
			de.printStackTrace();
			isInserted = false;
		} catch (Exception ex) {
			ex.printStackTrace();
			isInserted = false;
		}
		return isInserted;
	}
}
