/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.ott.cass.dvo.ContestDVO;
import com.rtf.ott.dto.OTTContestQuestionsDTO;
import com.rtf.ott.service.OTTContestQuestionsService;
import com.rtf.ott.service.OTTContestService;
import com.rtf.ott.sql.dvo.ContestQuestionDVO;
import com.rtf.ott.util.OTTMessageConstant;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;
import com.rtf.user.util.UserMsgConstants;

/**
 * The Class OTTUpdateContestQuestionsSeqNoServlet.
 */
@WebServlet("/ottUpdateContQuestSeqNo.json")
public class OTTUpdateContestQuestionsSeqNoServlet extends RtfSaasBaseServlet {

	private static final long serialVersionUID = 3510075737751418317L;

	/**
	 * Generate Http Response for OTT contest Question Response data is sent in JSON
	 * format.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}

	/**
	 * Update contest question sequence no
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String clId = request.getParameter("clId");
		String contId = request.getParameter("coId");
		String qsnIdStr = request.getParameter("qId");
		String moveType = request.getParameter("mtype");

		OTTContestQuestionsDTO respDTO = new OTTContestQuestionsDTO();
		respDTO.setSts(0);
		respDTO.setClId(clId);

		try {
			String userName = "";
			if (clId == null || clId.isEmpty()) {
				setClientMessage(respDTO, UserMsgConstants.INVALID_CLIENT, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (contId == null || contId.isEmpty()) {
				setClientMessage(respDTO, OTTMessageConstant.INVALID_CONTEST_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (moveType == null || moveType.isEmpty()) {
				setClientMessage(respDTO, OTTMessageConstant.INVALID_QUEST_MOVE_TYPE, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (!moveType.equals("UP") && !moveType.equals("DOWN")) {
				setClientMessage(respDTO, OTTMessageConstant.INVALID_QUEST_MOVE_TYPE, null);
				generateResponse(request, response, respDTO);
				return;
			}
			Integer qId = null;
			try {
				qId = Integer.parseInt(qsnIdStr);

			} catch (Exception e) {
				setClientMessage(respDTO, OTTMessageConstant.INVALID_QUEST_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}
			ContestQuestionDVO contQuestion = OTTContestQuestionsService.getcontestquestionsByQuestionId(clId, contId,
					qId);
			if (contQuestion == null) {
				setClientMessage(respDTO, OTTMessageConstant.QUESTION_ID_NOT_EXIT, null);
				generateResponse(request, response, respDTO);
				return;
			}
			ContestDVO contest = OTTContestService.getSQLContestByContestId(clId, contId);
			if (contest == null) {
				setClientMessage(respDTO, OTTMessageConstant.CONTEST_ID_NOT_EXISTS, null);
				generateResponse(request, response, respDTO);
				return;
			}
			List<ContestQuestionDVO> updatedList = new ArrayList<ContestQuestionDVO>();
			if (moveType.equals(OTTMessageConstant.QUEST_MOVE_UP)) {
				if (contQuestion.getQsnseqno().equals(1)) {
					setClientMessage(respDTO, OTTMessageConstant.NOT_MOVE_FIRST_QUEST_TO_UP, null);
					generateResponse(request, response, respDTO);
					return;
				}
				Integer newSeqno = contQuestion.getQsnseqno() - 1;
				contQuestion.setQsnseqno(newSeqno);
				contQuestion.setUpdby(userName);
				contQuestion.setUpddate(new Date());
				updatedList.add(contQuestion);

				ContestQuestionDVO newseqNocontestQuestion = OTTContestQuestionsService
						.getcontestquestionsByContestIdandQuestionSeqNo(clId, contId, newSeqno);
				if (newseqNocontestQuestion == null) {
					setClientMessage(respDTO, OTTMessageConstant.NOT_MOVE_FIRST_QUEST_TO_UP, null);
					generateResponse(request, response, respDTO);
					return;
				}
				newseqNocontestQuestion.setQsnseqno(newSeqno + 1);
				newseqNocontestQuestion.setUpdby(userName);
				newseqNocontestQuestion.setUpddate(new Date());
				updatedList.add(newseqNocontestQuestion);

			} else if (moveType.equals(OTTMessageConstant.QUEST_MOVE_DOWN)) {
				Integer newSeqno = contQuestion.getQsnseqno() + 1;
				contQuestion.setQsnseqno(newSeqno);
				contQuestion.setUpdby(userName);
				contQuestion.setUpddate(new Date());
				updatedList.add(contQuestion);

				ContestQuestionDVO newseqNocontestQuestion = OTTContestQuestionsService
						.getcontestquestionsByContestIdandQuestionSeqNo(clId, contId, newSeqno);
				if (newseqNocontestQuestion == null) {
					setClientMessage(respDTO, OTTMessageConstant.NOT_MOVE_LAST_QUEST_TO_DOWN, null);
					generateResponse(request, response, respDTO);
					return;
				}
				newseqNocontestQuestion.setQsnseqno(newSeqno - 1);
				newseqNocontestQuestion.setUpdby(userName);
				newseqNocontestQuestion.setUpddate(new Date());
				updatedList.add(newseqNocontestQuestion);
			}
			if (!updatedList.isEmpty()) {
				OTTContestQuestionsService.updateContestQuestionsSeqno(clId, updatedList);
			}

			respDTO.setSts(1);
			setClientMessage(respDTO, null, OTTMessageConstant.QUEST_MOVE_SUCCESS_MSG);
			generateResponse(request, response, respDTO);

		} catch (Exception e) {
			e.printStackTrace();

			setClientMessage(respDTO, null, null);
			generateResponse(request, response, respDTO);
			return;
		}

	}

}
