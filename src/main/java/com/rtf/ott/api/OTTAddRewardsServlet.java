/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.api;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.rtf.aws.AWSFileService;
import com.rtf.aws.AwsS3Response;
import com.rtf.common.util.SaaSAdminAppCache;
import com.rtf.ott.dto.OTTRerwardTypeDTO;
import com.rtf.ott.service.OTTManageRewardService;
import com.rtf.ott.sql.dvo.RewardTypeDVO;
import com.rtf.ott.util.OTTMessageConstant;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.FileUtil;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;
import com.rtf.saas.util.SAASMessages;
import com.rtf.user.util.UserMsgConstants;

/**
 * The Class OTTAddRewardsServlet.
 */
@WebServlet("/ottaddrewards.json")
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 10, // 10 MB
		maxFileSize = 1024 * 1024 * 20, // 20 MB
		maxRequestSize = 1024 * 1024 * 20) // 20 MB
public class OTTAddRewardsServlet extends RtfSaasBaseServlet {

	private static final long serialVersionUID = -8286504388031218568L;

	/**
	 * Generate Http Response for OTT contest Reward Response data is sent in JSON
	 * format..
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * Add Contest Rewards
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String clId = request.getParameter("clId");
		String rwdtype = request.getParameter("rwdtype");
		String rwddesc = request.getParameter("rwddesc");
		String rwdunimesr = request.getParameter("rwdunimesr");
		String userId = request.getParameter("cau");

		OTTRerwardTypeDTO respDTO = new OTTRerwardTypeDTO();
		respDTO.setSts(0);
		respDTO.setClId(clId);

		try {
			if (GenUtil.isNullOrEmpty(clId)) {
				setClientMessage(respDTO, UserMsgConstants.INVALID_CLIENT, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (GenUtil.isNullOrEmpty(rwdtype)) {
				setClientMessage(respDTO, OTTMessageConstant.REWARD_TYPE_IS_MANDATORY, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (GenUtil.isNullOrEmpty(rwdunimesr)) {
				setClientMessage(respDTO, OTTMessageConstant.REWARD_UNIT_MEASUREMENT_IS_MANDATORY, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (rwdtype.length() > 20) {
				setClientMessage(respDTO, OTTMessageConstant.REWARD_NAME_LENGTH_EXCEEDS_LIMIT, null);
				generateResponse(request, response, respDTO);
				return;
			}

			RewardTypeDVO dvo = new RewardTypeDVO();
			dvo.setRwdtype(rwdtype);
			dvo.setRwdunimesr(rwdunimesr);
			dvo.setRwddesc(rwddesc);
			String tmpDIr = SaaSAdminAppCache.TMP_FOLDER;
			tmpDIr = tmpDIr + File.separator + clId + File.separator;
			File fileSaveDir = new File(tmpDIr);
			if (!fileSaveDir.exists()) {
				fileSaveDir.mkdirs();
			}
			String origFileName = null;
			try {
				for (Part part : request.getParts()) {
					origFileName = FileUtil.getFileName(part);
				}
			} catch (Exception ex) {
				respDTO.setSts(0);
				setClientMessage(respDTO, SAASMessages.RWD_ICON_FILE_UPLOAD_MANDATORY, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (GenUtil.isNullOrEmpty(origFileName)) {
				respDTO.setSts(0);
				setClientMessage(respDTO, SAASMessages.RWD_ICON_FILE_UPLOAD_MANDATORY, null);
				generateResponse(request, response, respDTO);
				return;
			}

			String ext = FileUtil.getFileExtension(origFileName);
			String fileName = clId + "_" + System.currentTimeMillis() + "." + ext;
			try {
				for (Part part : request.getParts()) {
					part.write(tmpDIr + File.separator + fileName);
				}
			} catch (Exception ex) {
				respDTO.setSts(0);
				setClientMessage(respDTO, null, null);
				generateResponse(request, response, respDTO);
				return;
			}

			String s3bucket = SaaSAdminAppCache.s3staticbucket;
			String rewardsfolder = SaaSAdminAppCache.envfoldertype + "/" + SaaSAdminAppCache.s3rewardsfolder + "/"
					+ clId;
			File tmpfile = new File(tmpDIr + File.separator + fileName);
			AwsS3Response awsRsponse = AWSFileService.upLoadFileToS3(s3bucket, fileName, rewardsfolder, tmpfile);
			if (null != awsRsponse && awsRsponse.getStatus() != 1) {
				setClientMessage(respDTO, null, null);
				generateResponse(request, response, respDTO);
				return;
			}
			String serverLinkPath = SaaSAdminAppCache.cfstaticurl + "/" + rewardsfolder + "/" + fileName;
			dvo.setRwdimgurl(serverLinkPath);
			respDTO.setReward(dvo);
			respDTO = OTTManageRewardService.createRewardType(clId, respDTO, userId);
			if (respDTO.getSts() == 0) {
				setClientMessage(respDTO, null, null);
				generateResponse(request, response, respDTO);
				return;
			}

			generateResponse(request, response, respDTO);
			return;
		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(respDTO, null, null);
			generateResponse(request, response, respDTO);
		}
		return;
	}

}
