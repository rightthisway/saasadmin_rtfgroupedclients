/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.ott.cass.dvo.ContestDVO;
import com.rtf.ott.dto.OTTContestQuestionsDTO;
import com.rtf.ott.service.OTTContestQuestionsService;
import com.rtf.ott.service.OTTContestService;
import com.rtf.ott.sql.dvo.ContestQuestionDVO;
import com.rtf.ott.util.OTTMessageConstant;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;
import com.rtf.user.util.UserMsgConstants;

/**
 * The Class OTTUpdateContestQuestionsServlet.
 */
@WebServlet("/ottUpdateContQuest.json")
public class OTTUpdateContestQuestionsServlet extends RtfSaasBaseServlet {

	private static final long serialVersionUID = 6106918898352489651L;

	/**
	 * Generate Http Response for OTT contest Question Response data is sent in JSON
	 * format.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}

	/**
	 * Update contest question details
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String clId = request.getParameter("clId");
		String contId = request.getParameter("coId");
		String qsttext = request.getParameter("qstText");
		String optA = request.getParameter("optA");
		String optB = request.getParameter("optB");
		String optC = request.getParameter("optC");
		String optD = request.getParameter("optD");
		String answer = request.getParameter("ans");
		String qsnOrient = request.getParameter("orient");
		String qsnIdStr = request.getParameter("qId");

		OTTContestQuestionsDTO respDTO = new OTTContestQuestionsDTO();
		respDTO.setSts(0);
		respDTO.setClId(clId);

		try {
			String userName = "";
			if (clId == null || clId.isEmpty()) {
				setClientMessage(respDTO, UserMsgConstants.INVALID_CLIENT, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (contId == null || contId.isEmpty()) {
				setClientMessage(respDTO, OTTMessageConstant.INVALID_CONTEST_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (qsttext == null || qsttext.isEmpty()) {
				setClientMessage(respDTO, OTTMessageConstant.INVALID_QUEST_TEXT, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (optA == null || optA.isEmpty()) {
				setClientMessage(respDTO, OTTMessageConstant.INVALID_QUEST_OPT_A, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (optB == null || optB.isEmpty()) {
				setClientMessage(respDTO, OTTMessageConstant.INVALID_QUEST_OPT_B, null);
				generateResponse(request, response, respDTO);
				return;
			}

			if (qsnOrient == null || qsnOrient.isEmpty()) {
				setClientMessage(respDTO, OTTMessageConstant.INVALID_QUEST_ORIENT, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (qsnIdStr == null || qsnIdStr.isEmpty()) {
				setClientMessage(respDTO, OTTMessageConstant.INVALID_QUEST_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}
			Integer questionId = null;
			try {
				questionId = Integer.parseInt(qsnIdStr);

			} catch (Exception e) {
				setClientMessage(respDTO, OTTMessageConstant.INVALID_QUEST_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}
			ContestDVO contest = OTTContestService.getSQLContestByContestId(clId, contId);
			if (contest == null) {
				setClientMessage(respDTO, OTTMessageConstant.CONTEST_ID_NOT_EXISTS, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (contest.getAnsType().equals(OTTMessageConstant.CONTEST_TYPE_FEEDBACK)) {
				if (answer != null) {
					setClientMessage(respDTO, OTTMessageConstant.FEEDBACK_QUEST_ANSWER_INVALID, null);
					generateResponse(request, response, respDTO);
					return;
				}
			} else {
				if (answer == null || answer.isEmpty()) {
					setClientMessage(respDTO, OTTMessageConstant.INVALID_ANSWER, null);
					generateResponse(request, response, respDTO);
					return;
				}
				if (answer.equals(OTTMessageConstant.CONTEST_QUEST_OPT_C) || answer.equals(OTTMessageConstant.CONTEST_QUEST_OPT_D)) {
					if (optC == null || optC.isEmpty()) {
						setClientMessage(respDTO, OTTMessageConstant.INVALID_QUEST_OPT_C, null);
						generateResponse(request, response, respDTO);
						return;
					}
				}
				if (answer.equals(OTTMessageConstant.CONTEST_QUEST_OPT_D)) {
					if (optD == null || optD.isEmpty()) {
						setClientMessage(respDTO, OTTMessageConstant.CONTEST_QUEST_OPT_D, null);
						generateResponse(request, response, respDTO);
						return;
					}
				}
			}
			ContestQuestionDVO contQuestion = OTTContestQuestionsService.getcontestquestionsByQuestionId(clId, contId,
					questionId);
			if (contQuestion == null) {
				setClientMessage(respDTO, OTTMessageConstant.QUESTION_ID_NOT_EXIT, null);
				generateResponse(request, response, respDTO);
				return;
			}

			contQuestion.setQsntext(qsttext);
			contQuestion.setOpa(optA);
			contQuestion.setOpb(optB);
			contQuestion.setOpc(optC);
			contQuestion.setOpd(optD);
			contQuestion.setCorans(answer);
			contQuestion.setQsnorient(qsnOrient);
			contQuestion.setUpdby(userName);
			contQuestion.setUpddate(new Date());

			Integer id = OTTContestQuestionsService.updateContestQuestions(clId, contQuestion);
			if (id == null) {
				setClientMessage(respDTO, OTTMessageConstant.CONTEST_QUEST_NOT_UPDATED, null);
				generateResponse(request, response, respDTO);
				return;
			}

			respDTO.setSts(1);
			setClientMessage(respDTO, null,  OTTMessageConstant.CONT_QUEST_UPDATE_SUCCESS_MSG);
			generateResponse(request, response, respDTO);

		} catch (Exception e) {
			e.printStackTrace();

			setClientMessage(respDTO, null, null);
			generateResponse(request, response, respDTO);
			return;
		}

	}

}
