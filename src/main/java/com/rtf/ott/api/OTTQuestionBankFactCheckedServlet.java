package com.rtf.ott.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.ott.dto.OTTQuestionDTO;
import com.rtf.ott.service.OTTQuestionBankService;
import com.rtf.ott.util.OTTMessageConstant;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class OTTQuestionBankFactCheckedServlet.
 */
@WebServlet("/updatequefact.json")
public class OTTQuestionBankFactCheckedServlet extends RtfSaasBaseServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6871836134748421321L;

	/**
	 * Generate Http Response for OTT contest Question Response data is sent in JSON
	 * format..
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	
	/**
	 * Marked question bank question fact checked/unchecked.
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
		String clId = request.getParameter("clId");
		String qbIdsStr = request.getParameter("qbIds");
		String isChckd = request.getParameter("isChckd");
		String cau = request.getParameter("cau");
		
		OTTQuestionDTO respDTO = new OTTQuestionDTO();
		respDTO.setSts(0);
		respDTO.setClId(clId);
		
		try {
			if (GenUtil.isNullOrEmpty(clId)) {
				setClientMessage(respDTO, OTTMessageConstant.INVALID_CLIENT_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}
			
			if (GenUtil.isNullOrEmpty(cau)) {
				setClientMessage(respDTO, OTTMessageConstant.INVALID_LOGGEDIN_USER, null);
				generateResponse(request, response, respDTO);
				return;
			}

			if (GenUtil.isNullOrEmpty(qbIdsStr)) {
				setClientMessage(respDTO, OTTMessageConstant.INVALID_QBANK_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}

			if (GenUtil.isNullOrEmpty(isChckd)) {
				setClientMessage(respDTO, OTTMessageConstant.INVALID_FACT_CHECK, null);
				generateResponse(request, response, respDTO);
				return;
			}
			
			Boolean isChecked = false;
			try {
				isChecked = Boolean.parseBoolean(isChckd);
			} catch (Exception e) {
				e.printStackTrace();
				setClientMessage(respDTO, OTTMessageConstant.INVALID_FACT_CHECK, null);
				generateResponse(request, response, respDTO);
				return;
			}
			Boolean isUpd = false;
			String[] qbIds = qbIdsStr.split(",");
			if (qbIds.length == 0) {
				setClientMessage(respDTO, OTTMessageConstant.INVALID_QBANK_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}
			Integer qbId = 0;
			for (String idStr : qbIds) {
				if (idStr.trim().isEmpty()) {
					continue;
				}
				
				try {
					qbId = Integer.parseInt(idStr);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				isUpd = OTTQuestionBankService.updateQuestionFact(clId, qbId, isChecked);
			}
			
			if(!isUpd){
				setClientMessage(respDTO, OTTMessageConstant.ERROR_FACT_CHECK, null);
				generateResponse(request, response, respDTO);
				return;
			}
			respDTO.setSts(1);
			setClientMessage(respDTO, null, OTTMessageConstant.FACT_CHECK_UPDATED);
			generateResponse(request, response, respDTO);
			return;
			
		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(respDTO, null, null);
			generateResponse(request, response, respDTO);
		}
		
	}
}
