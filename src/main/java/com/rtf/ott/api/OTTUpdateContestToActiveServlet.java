/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.ott.cass.dvo.ContestDVO;
import com.rtf.ott.dto.OTTContestDTO;
import com.rtf.ott.service.OTTContestService;
import com.rtf.ott.util.DateFormatUtil;
import com.rtf.ott.util.OTTMessageConstant;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;
import com.rtf.user.util.UserMsgConstants;

/**
 * The Class OTTUpdateContestToActiveServlet.
 */
@WebServlet("/ottupdatecontesttoactive.json")
public class OTTUpdateContestToActiveServlet extends RtfSaasBaseServlet {

	private static final long serialVersionUID = 8701498815185000319L;

	/**
	 * Generate Http Response for OTT contest Response data is sent in JSON format.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * Process request.
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String clId = request.getParameter("clId");
		String cau = request.getParameter("cau");
		String coId = request.getParameter("coId");
		String stDate = request.getParameter("stDate");
		String enDate = request.getParameter("enDate");

		OTTContestDTO respDTO = new OTTContestDTO();
		respDTO.setSts(0);
		respDTO.setClId(clId);

		try {
			if (clId == null || clId.isEmpty()) {
				setClientMessage(respDTO, UserMsgConstants.INVALID_CLIENT, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (coId == null || coId.isEmpty()) {
				setClientMessage(respDTO, OTTMessageConstant.INVALID_CONTEST_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (stDate == null || stDate.isEmpty()) {
				setClientMessage(respDTO, OTTMessageConstant.INVALID_START_DATE, null);
				generateResponse(request, response, respDTO);
				return;
			}

			Date sDate = null;
			Date eDate = null;

			String stDate1 = stDate + " 00:00:00";
			String enDate1 = enDate + " 23:59:00";

			sDate = DateFormatUtil.getDateWithTwentyFourHourFormat(stDate1);
			eDate = DateFormatUtil.getDateWithTwentyFourHourFormat(enDate1);

			ContestDVO contest = OTTContestService.getSQLContestByContestId(clId, coId);
			if (contest == null) {
				setClientMessage(respDTO, OTTMessageConstant.CONTEST_ID_NOT_EXISTS, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (contest.getIsAct() == 2 || contest.getIsAct().equals(2)) {
				setClientMessage(respDTO, OTTMessageConstant.CANT_UPDATE_LIVE_CONTEST, null);
				generateResponse(request, response, respDTO);
				return;
			}

			Date now = new Date();
			if (eDate.before(now)) {
				setClientMessage(respDTO, OTTMessageConstant.CONT_END_DATE_TODAY_OR_FUTURE, null);
				generateResponse(request, response, respDTO);
				return;
			}

			contest.setEnDate(eDate.getTime());
			contest.setIsAct(1);
			contest.setStDate(sDate.getTime());
			contest.setUpBy(cau);
			contest.setUpDate(now.getTime());

			boolean isUpdate = OTTContestService.updateContest(contest);
			if (isUpdate) {
				respDTO.setSts(1);
				respDTO.setMsg(OTTMessageConstant.CONTEST_UPDATE_TO_ACTIVE_SUCCESS_MSG);
				respDTO.setContestMstrDVO(contest);
				generateResponse(request, response, respDTO);
				return;
			} else {
				setClientMessage(respDTO, null, null);
				generateResponse(request, response, respDTO);
				return;
			}
		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(respDTO, null, null);
			generateResponse(request, response, respDTO);
		}
		return;

	}

}
