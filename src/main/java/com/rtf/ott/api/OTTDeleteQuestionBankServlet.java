/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.ott.dto.OTTQuestionDTO;
import com.rtf.ott.service.OTTQuestionBankService;
import com.rtf.ott.sql.dvo.QuestionBankDVO;
import com.rtf.ott.util.OTTMessageConstant;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;
import com.rtf.saas.util.SAASMessages;
import com.rtf.user.util.UserMsgConstants;

/**
 * The Class OTTDeleteQuestionBankServlet.
 */
@WebServlet("/ottdeletequebank.json")
public class OTTDeleteQuestionBankServlet extends RtfSaasBaseServlet {

	private static final long serialVersionUID = 5046882511996320919L;

	/**
	 * Generate Http Response for OTT contest Question Response data is sent in JSON
	 * format..
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * Delete question from question bank
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String clId = request.getParameter("clId");
		String qbId = request.getParameter("qbId");

		Integer qId = null;

		OTTQuestionDTO respDTO = new OTTQuestionDTO();
		respDTO.setSts(0);
		respDTO.setClId(clId);

		try {
			if (GenUtil.isNullOrEmpty(clId)) {
				setClientMessage(respDTO, UserMsgConstants.INVALID_CLIENT, null);
				generateResponse(request, response, respDTO);
				return;
			}

			if (GenUtil.isNullOrEmpty(qbId)) {
				setClientMessage(respDTO, OTTMessageConstant.INVALID_QBANK_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}

			try {
				qId = Integer.parseInt(qbId);
			} catch (Exception ex) {
				setClientMessage(respDTO, OTTMessageConstant.INVALID_QBANK_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}

			QuestionBankDVO dvo = new QuestionBankDVO();
			dvo.setQsnbnkid(qId);

			dvo.setIsActive(Boolean.FALSE);
			dvo.setCreBy(OTTMessageConstant.TEST_USER);

			respDTO.setOttqbdo(dvo);
			respDTO = OTTQuestionBankService.deleteQuestion(clId, respDTO);
			if (respDTO.getSts() == 0) {
				if (respDTO.getMsg() == null || respDTO.getMsg().isEmpty()) {
					setClientMessage(respDTO, null, null);
				}
				setClientMessage(respDTO, null, null);
				generateResponse(request, response, respDTO);
				return;
			}
			setClientMessage(respDTO, null, SAASMessages.GEN_DELETE_SUCCESS_MSG);
			generateResponse(request, response, respDTO);
			return;
		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(respDTO, null, null);
			generateResponse(request, response, respDTO);
		}
		return;
	}

}
