/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.ott.cass.dvo.ContestDVO;
import com.rtf.ott.dto.OTTContestQuestionsDTO;
import com.rtf.ott.service.OTTContestQuestionsService;
import com.rtf.ott.service.OTTContestService;
import com.rtf.ott.sql.dao.QuestionBankSQLDAO;
import com.rtf.ott.sql.dvo.ContestQuestionDVO;
import com.rtf.ott.sql.dvo.QuestionBankDVO;
import com.rtf.ott.util.OTTMessageConstant;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;
import com.rtf.user.util.UserMsgConstants;

/**
 * The Class OTTAddContestQuestionsfromBankServlet.
 */
@WebServlet("/ottAddContQuestFromBank.json")
public class OTTAddContestQuestionsfromBankServlet extends RtfSaasBaseServlet {

	private static final long serialVersionUID = 7484406570375916628L;

	/**
	 * Generate Http Response for OTT contest Question Response data is sent in JSON
	 * format..
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}

	/**
	 * Add contest question from question bank
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String clId = request.getParameter("clId");
		String contId = request.getParameter("coId");
		String questionbnkIdStr = request.getParameter("qbId");
		String userName = request.getParameter("uname");

		OTTContestQuestionsDTO respDTO = new OTTContestQuestionsDTO();
		respDTO.setSts(0);
		respDTO.setClId(clId);

		try {
			if (clId == null || clId.isEmpty()) {
				setClientMessage(respDTO, UserMsgConstants.INVALID_CLIENT, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (contId == null || contId.isEmpty()) {
				setClientMessage(respDTO, OTTMessageConstant.INVALID_CONTEST_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (questionbnkIdStr == null || questionbnkIdStr.isEmpty()) {
				setClientMessage(respDTO, OTTMessageConstant.INVALID_QBANK_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}
			Integer qbnkId = null;
			try {
				qbnkId = Integer.parseInt(questionbnkIdStr);
			} catch (Exception e) {

			}
			ContestDVO contest = OTTContestService.getSQLContestByContestId(clId, contId);
			if (contest == null) {
				setClientMessage(respDTO, OTTMessageConstant.CONTEST_ID_NOT_EXISTS, null);
				generateResponse(request, response, respDTO);
				return;
			}

			QuestionBankDVO questionbank = QuestionBankSQLDAO.getOLTXQuestionsFromQBankByquestionId(clId, qbnkId);
			if (questionbank == null) {
				setClientMessage(respDTO, OTTMessageConstant.QBANK_ID_NOT_EXISTS, null);
				generateResponse(request, response, respDTO);
				return;
			}
			ContestQuestionDVO contQuestionDb = OTTContestQuestionsService
					.getcontestquestionsByContestIdandQuestionBankId(clId, contId, qbnkId);
			if (contQuestionDb != null) {
				setClientMessage(respDTO, OTTMessageConstant.QBANK_ID_ALREADY_EXISTS_FOR_CONTEST, null);
				generateResponse(request, response, respDTO);
				return;
			}

			if (!contest.getAnsType().equals(questionbank.getAnstype())) {
				setClientMessage(respDTO, OTTMessageConstant.QTYPE_MISMATCH_CONT_QTYPE, null);
				generateResponse(request, response, respDTO);
				return;
			}

			int qsize = contest.getqSize();
			Integer qestionsCount = OTTContestQuestionsService.getContestQuestionsCount(clId, contId);
			if (qestionsCount != null && qestionsCount >= qsize) {
				setClientMessage(respDTO, "Already " + qestionsCount + " questions created for this contest", null);
				generateResponse(request, response, respDTO);
				return;
			}

			ContestQuestionDVO contQuestion = new ContestQuestionDVO();
			contQuestion.setClintid(clId);
			contQuestion.setConid(contId);
			contQuestion.setQsntext(questionbank.getQtx());
			contQuestion.setOpa(questionbank.getOpa());
			contQuestion.setOpb(questionbank.getOpb());
			contQuestion.setOpc(questionbank.getOpc());
			contQuestion.setOpd(questionbank.getOpd());
			contQuestion.setCorans(questionbank.getCans());
			contQuestion.setQsnorient(questionbank.getqOrientn());
			contQuestion.setQbid(qbnkId);
			contQuestion.setCreby(userName);
			contQuestion.setCredate(new Date());
			contQuestion.setIsactive(Boolean.TRUE);
			contQuestion.setQsnseqno((qestionsCount + 1));

			Integer id = OTTContestQuestionsService.saveContestQuestions(clId, contQuestion);
			if (id == null) {
				setClientMessage(respDTO, OTTMessageConstant.CONTEST_QUEST_NOT_CREATED, null);
				generateResponse(request, response, respDTO);
				return;
			}

			respDTO.setSts(1);
			setClientMessage(respDTO, null, OTTMessageConstant.CONT_QUEST_CREATED_SUCCESS_MSG);
			generateResponse(request, response, respDTO);

		} catch (Exception e) {
			e.printStackTrace();

			setClientMessage(respDTO, null, null);
			generateResponse(request, response, respDTO);
			return;
		}

	}

}
