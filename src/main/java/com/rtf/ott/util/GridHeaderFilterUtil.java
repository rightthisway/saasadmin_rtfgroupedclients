/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.util;

/**
 * The Class GridHeaderFilterUtil.
 */
public class GridHeaderFilterUtil {

	
	/**
	 * Gets the contest filter query.
	 *
	 * @param filter the filter
	 * @return the contest filter query
	 */
	public static String getContestFilterQuery(String filter){
		StringBuffer sql = new StringBuffer();
		try {
			if(filter!=null && !filter.isEmpty() && filter.contains(":") && filter.contains(",")){
				String params[] = filter.split(",");
				for (String param : params) {
					String nameValue[] = param.split(":");
					if(nameValue.length ==2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("name")){
							sql.append(" AND c.conname like '%"+value+"%' ");
						}else if(name.equalsIgnoreCase("enDateTimeStr")){
							getDatePartQuery(sql, "c.consrtdate", value);
						}else if(name.equalsIgnoreCase("enDateTimeStr")){
							getDatePartQuery(sql, "c.conenddate", value);
						}else if(name.equalsIgnoreCase("cat")){
							sql.append(" AND c.cattype like '%"+value+"%' ");
						}else if(name.equalsIgnoreCase("subCat")){
							sql.append(" AND c.subcattype like '%"+value+"%' ");
						}else if(name.equalsIgnoreCase("qMode")){
							sql.append("AND c.qsnmode like '%"+value+"%' ");
						}else if(name.equalsIgnoreCase("ansType")){
							sql.append(" AND c.anstype like '%"+value+"%' ");
						}else if(name.equalsIgnoreCase("isElimination")){
							if(value.equalsIgnoreCase("YES")){
								sql.append(" AND c.iselimtype=1 ");
							}else if(value.equalsIgnoreCase("NO")){
								sql.append(" AND c.iselimtype=0 ");
							}
						}else if(name.equalsIgnoreCase("qSize")){
							sql.append(" AND c.noofqns ="+value);
						}else if(name.equalsIgnoreCase("seqNo")){
							sql.append(" AND c.cardseqno ="+value);
						}else if(name.equalsIgnoreCase("crBy")){
							sql.append(" AND c.creby like '%"+value+"%' ");
						}else if(name.equalsIgnoreCase("crDateTimeStr")){
							getDatePartQuery(sql, "c.credate", value);
						}else if(name.equalsIgnoreCase("upBy")){
							sql.append(" AND c.updby like '%"+value+"%' ");
						}else if(name.equalsIgnoreCase("upDateTimeStr")){
							getDatePartQuery(sql, "c.upddate", value);
						}else if(name.equalsIgnoreCase("status")){
							sql.append(" AND s.isconactivetext  like '%"+value+"%' ");
						}else if(name.equalsIgnoreCase("playinterval")){
							sql.append(" AND c.playinterval="+value+" ");
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sql.toString();
	}
	
	
	
	/**
	 * Gets the contest config filter query.
	 *
	 * @param filter the filter
	 * @return the contest config filter query
	 */
	public static String getContestConfigFilterQuery(String filter){
		StringBuffer sql = new StringBuffer();
		try {
			if(filter!=null && !filter.isEmpty() && filter.contains(":") && filter.contains(",")){
				String params[] = filter.split(",");
				for(String param : params){
					String nameValue[] = param.split(":");
					if(nameValue.length ==2){
						String name = nameValue[0];
						String value = nameValue[1];
						if(!validateParamValues(name,value)){
							continue;
						}
						if(name.equalsIgnoreCase("name")){
							sql.append(" AND keyid like '%"+value+"%' ");
						}else if(name.equalsIgnoreCase("desc")){
							sql.append(" AND descr like '%"+value+"%' ");
						}else if(name.equalsIgnoreCase("value")){
							sql.append(" AND keyvalue ="+value+" ");
						}else if(name.equalsIgnoreCase("upBy")){
							sql.append(" AND updby like '%"+value+"%' ");
						}else if(name.equalsIgnoreCase("upDate")){
							getDatePartQuery(sql, "upddate", value);
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sql.toString();
	}
	
	/**
	 * Gets the contest questions filter query.
	 *
	 * @param filter the filter
	 * @return the contest questions filter query
	 * @throws Exception the exception
	 */
	public static String getContestQuestionsFilterQuery(String filter) throws Exception{
		StringBuffer sql = new StringBuffer();
		try {
			if(filter!=null && !filter.isEmpty() && filter.contains(":") && filter.contains(",")){
				String params[] = filter.split(",");
				for(String param : params){
					try {
						String nameValue[] = param.split(":");
						if(nameValue.length ==2){
							String name = nameValue[0];
							String value = nameValue[1];
							if(!validateParamValues(name,value)){
								continue;
							}
							if(name.equalsIgnoreCase("qsntext")){
								sql.append(" AND qsntext like '%"+value+"%' ");
							}else if(name.equalsIgnoreCase("opa")){
								sql.append(" AND ansopta like '%"+value+"%' ");
							}else if(name.equalsIgnoreCase("opb")){
								sql.append(" AND ansoptb like '%"+value+"%' ");
							}else if(name.equalsIgnoreCase("opc")){
								sql.append(" AND ansoptc like '%"+value+"%' ");
							}else if(name.equalsIgnoreCase("opd")){
								sql.append(" AND ansoptd like '%"+value+"%' ");
							}else if(name.equalsIgnoreCase("corans")){
								sql.append("AND corans like '%"+value+"%' ");
							} else if(name.equalsIgnoreCase("crBy")){
								sql.append(" AND creby like '%"+value+"%' ");
							}else if(name.equalsIgnoreCase("credateStr")){
								getDatePartQuery(sql, "credate", value);
							}else if(name.equalsIgnoreCase("upBy")){
								sql.append(" AND updby like '%"+value+"%' ");
							}else if(name.equalsIgnoreCase("upddateStr")){
								getDatePartQuery(sql, "upddate", value);
							}
						}
					} catch(Exception e) {
						
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sql.toString();
	}
	
	/**
	 * Gets the all question bank questions for contest with filter query.
	 *
	 * @param filter the filter
	 * @return the all question bank questions for contest with filter query
	 * @throws Exception the exception
	 */
	public static String getAllQuestionBankQuestionsForContestWithFilterQuery(String filter) throws Exception{
		StringBuffer sql = new StringBuffer();
		try {
			if(filter!=null && !filter.isEmpty() && filter.contains(":") && filter.contains(",")){
				String params[] = filter.split(",");
				for(String param : params){
					try {
						String nameValue[] = param.split(":");
						if(nameValue.length ==2){
							String name = nameValue[0];
							String value = nameValue[1];
							if(!validateParamValues(name,value)){
								continue;
							}
							if(name.equalsIgnoreCase("qtx")){
								sql.append(" AND qsntext like '%"+value+"%' ");
							}else if(name.equalsIgnoreCase("opa")){
								sql.append(" AND ansopta like '%"+value+"%' ");
							}else if(name.equalsIgnoreCase("opb")){
								sql.append(" AND ansoptb like '%"+value+"%' ");
							}else if(name.equalsIgnoreCase("opc")){
								sql.append(" AND ansoptc like '%"+value+"%' ");
							}else if(name.equalsIgnoreCase("opd")){
								sql.append(" AND ansoptd like '%"+value+"%' ");
							}else if(name.equalsIgnoreCase("cans")){
								sql.append("AND corans like '%"+value+"%' ");
							} else if(name.equalsIgnoreCase("anstype")){
								sql.append("AND anstype like '%"+value+"%' ");
							}else if(name.equalsIgnoreCase("qOrientn")){
								sql.append("AND qsnorient like '%"+value+"%' ");
							}else if(name.equalsIgnoreCase("ctyp")){
								sql.append("AND cattype like '%"+value+"%' ");
							}else if(name.equalsIgnoreCase("sctyp")){
								sql.append("AND subcattype like '%"+value+"%' ");
							} else if(name.equalsIgnoreCase("creBy")){
								sql.append(" AND creby like '%"+value+"%' ");
							}else if(name.equalsIgnoreCase("crDateTimeStr")){
								getDatePartQuery(sql, "credate", value);
							}else if(name.equalsIgnoreCase("updBy")){
								sql.append(" AND updby like '%"+value+"%' ");
							}else if(name.equalsIgnoreCase("upDateTimeStr")){
								getDatePartQuery(sql, "upddate", value);
							}
						}
					} catch(Exception e) {
						
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sql.toString();
	}
	
	/**
	 * Gets the all categories filter query.
	 *
	 * @param filter the filter
	 * @return the all categories filter query
	 * @throws Exception the exception
	 */
	public static String getAllCategoriesFilterQuery(String filter) throws Exception{
		StringBuffer sql = new StringBuffer();
		try {
			if(filter!=null && !filter.isEmpty() && filter.contains(":") && filter.contains(",")){
				String params[] = filter.split(",");
				for(String param : params){
					try {
						String nameValue[] = param.split(":");
						if(nameValue.length ==2){
							String name = nameValue[0];
							String value = nameValue[1];
							if(!validateParamValues(name,value)){
								continue;
							}
							if(name.equalsIgnoreCase("cattype")){
								sql.append(" AND cattype like '%"+value+"%' ");
							}else if(name.equalsIgnoreCase("catdesc")){
								sql.append(" AND catdesc like '%"+value+"%' ");
							}else if(name.equalsIgnoreCase("creby")){
								sql.append(" AND creby like '%"+value+"%' ");
							}else if(name.equalsIgnoreCase("crDateTimeStr")){
								getDatePartQuery(sql, "credate", value);
							}else if(name.equalsIgnoreCase("updby")){
								sql.append(" AND updby like '%"+value+"%' ");
							}else if(name.equalsIgnoreCase("upDateTimeStr")){
								getDatePartQuery(sql, "upddate", value);
							}
						}
					} catch(Exception e) {
						
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sql.toString();
	}
	
	/**
	 * Gets the all sub categories filter query.
	 *
	 * @param filter the filter
	 * @return the all sub categories filter query
	 * @throws Exception the exception
	 */
	public static String getAllSubCategoriesFilterQuery(String filter) throws Exception{
		StringBuffer sql = new StringBuffer();
		try {
			if(filter!=null && !filter.isEmpty() && filter.contains(":") && filter.contains(",")){
				String params[] = filter.split(",");
				for(String param : params){
					try {
						String nameValue[] = param.split(":");
						if(nameValue.length ==2){
							String name = nameValue[0];
							String value = nameValue[1];
							if(!validateParamValues(name,value)){
								continue;
							}
							if(name.equalsIgnoreCase("cattype")){
								sql.append(" AND cattype like '%"+value+"%' ");
							}else if(name.equalsIgnoreCase("subcattype")){
								sql.append(" AND subcattype like '%"+value+"%' ");
							}else if(name.equalsIgnoreCase("subcatdesc")){
								sql.append(" AND subcatdesc like '%"+value+"%' ");
							}else if(name.equalsIgnoreCase("creby")){
								sql.append(" AND creby like '%"+value+"%' ");
							}else if(name.equalsIgnoreCase("crDateTimeStr")){
								getDatePartQuery(sql, "credate", value);
							}else if(name.equalsIgnoreCase("updby")){
								sql.append(" AND updby like '%"+value+"%' ");
							}else if(name.equalsIgnoreCase("upDateTimeStr")){
								getDatePartQuery(sql, "upddate", value);
							}
						}
					} catch(Exception e) {
						
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sql.toString();
	}
	
	/**
	 * Gets the all reward types filter query.
	 *
	 * @param filter the filter
	 * @return the all reward types filter query
	 * @throws Exception the exception
	 */
	public static String getAllRewardTypesFilterQuery(String filter) throws Exception{
		StringBuffer sql = new StringBuffer();
		try {
			if(filter!=null && !filter.isEmpty() && filter.contains(":") && filter.contains(",")){
				String params[] = filter.split(",");
				for(String param : params){
					try {
						String nameValue[] = param.split(":");
						if(nameValue.length ==2){
							String name = nameValue[0];
							String value = nameValue[1];
							if(!validateParamValues(name,value)){
								continue;
							}
							if(name.equalsIgnoreCase("rwdtype")){
								sql.append(" AND rwdtype like '%"+value+"%' ");
							}else if(name.equalsIgnoreCase("rwddesc")){
								sql.append(" AND rwddesc like '%"+value+"%' ");
							}else if(name.equalsIgnoreCase("rwdunimesr")){
								sql.append(" AND rwdunimesr like '%"+value+"%' ");
							}else if(name.equalsIgnoreCase("creby")){
								sql.append(" AND creby like '%"+value+"%' ");
							}else if(name.equalsIgnoreCase("crDateTimeStr")){
								getDatePartQuery(sql, "credate", value);
							}else if(name.equalsIgnoreCase("updby")){
								sql.append(" AND updby like '%"+value+"%' ");
							}else if(name.equalsIgnoreCase("upDateTimeStr")){
								getDatePartQuery(sql, "upddate", value);
							}
						}
					} catch(Exception e) {
						
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sql.toString();
	}
	
	/**
	 * Gets the date part query.
	 *
	 * @param sql the sql
	 * @param columnName the column name
	 * @param value the value
	 * @return the date part query
	 */
	private static void getDatePartQuery(StringBuffer sql,String columnName,String value){
		try {
			if(DateFormatUtil.extractDateElement(value,"DAY") > 0){
				sql.append(" AND DATEPART(day, "+columnName+") = "+DateFormatUtil.extractDateElement(value,"DAY")+" ");
			}
			if(DateFormatUtil.extractDateElement(value,"MONTH") > 0){
				sql.append(" AND DATEPART(month, "+columnName+") = "+DateFormatUtil.extractDateElement(value,"MONTH")+" ");
			}
			if(DateFormatUtil.extractDateElement(value,"YEAR") > 0){
				sql.append(" AND DATEPART(year, "+columnName+") = "+DateFormatUtil.extractDateElement(value,"YEAR")+" ");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Validate param values.
	 *
	 * @param name the name
	 * @param value the value
	 * @return true, if successful
	 */
	private static boolean validateParamValues(String name,String value){
		if(name==null || name.isEmpty() || name.equalsIgnoreCase("undefined") ||
				value==null || value.isEmpty() || value.equalsIgnoreCase("undefined")){
			return false;
		}
		return true;	
	}
}
