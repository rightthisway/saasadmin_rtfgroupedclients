/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.util;

/**
 * The Enum ContestAnswerTypeEnums.
 */
public enum ContestAnswerTypeEnums {

	/** The feedback. */
	FEEDBACK,
	/** The regular. */
	REGULAR
}
