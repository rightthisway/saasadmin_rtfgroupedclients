package com.rtf.ott.util;

public class OTTMessageConstant {
	
	
	public static final String STRING_NULL = "null";
	
	public static final String INVALID_CONTEST_ID = "Invalid Contest id.";
	
	public static final String INVALID_CLIENT_ID = "Invalid client ID.";
	
	public static final String INVALID_QBANK_ID = "Invalid Question Bank Id.";
	
	public static final String INVALID_FACT_CHECK = "Invalid fact checked/unchecked status.";
	
	public static final String ERROR_FACT_CHECK = "Error occured while updating fact checked/unchecked status.";
	
	public static final String FACT_CHECK_UPDATED = "Question fact checked/unhecked status updated.";
	
	public static final String INVALID_LOGGEDIN_USER = "Invalid logged in user details.";
	
	public static final String CONTEST_ID_NOT_EXISTS = "Contest id not exists";
	
	public static final String QBANK_ID_NOT_EXISTS = "Question Bank Id not exists";
	
	public static final String QBANK_ID_ALREADY_EXISTS_FOR_CONTEST = "Question Bank question already exist for this contest";
	
	public static final String QTYPE_MISMATCH_CONT_QTYPE = "Question Type is mismatch with contest question type.";
	
	public static final String CATEGORY_NAME_MANDATORY = "Category Name  is mandatory";
	
	public static final String SUB_CATEGORY_NAME_MANDATORY = "Sub Category Name  is mandatory";
	
	public static final String TEST_USER = "testuser";
	
	public static final String CONTEST_QUEST_NOT_CREATED = "Contest questions not created.";
	
	public static final String CONTEST_QUEST_NOT_UPDATED = "Contest questions not updated.";
	
	public static final String CONT_QUEST_CREATED_SUCCESS_MSG = "Question Created successfully.";
	
	public static final String CONT_QUEST_UPDATE_SUCCESS_MSG = "Question Updated successfully.";
	
	
	
	public static final String INVALID_QUEST_TEXT = "Invalid Question text.";
	
	public static final String INVALID_QUEST_OPT_A = "Invalid Option A";
	
	public static final String INVALID_QUEST_OPT_B = "Invalid Option B";
	
	public static final String INVALID_QUEST_OPT_C = "Invalid Option C";
	
	public static final String INVALID_QUEST_OPT_D = "Invalid Option D";
	
	public static final String INVALID_QUEST_ORIENT = "Invalid Question Orient";
	
	public static final String FEEDBACK_QUEST_ANSWER_INVALID = "Do not select answer for Feedback contest questions.";
	
	public static final String INVALID_ANSWER = "Invalid Answer";
	
	public static final String CONTEST_TYPE_FEEDBACK = "FEEDBACK";
	
	public static final String CONTEST_QUEST_OPT_A = "A";
	
	public static final String CONTEST_QUEST_OPT_B = "B";
	
	public static final String CONTEST_QUEST_OPT_C = "C";
	
	public static final String CONTEST_QUEST_OPT_D = "D";


	public static final String INVALID_START_DATE = "Invalid start date.";
	
	public static final String INVALID_CATEGORY = "Invalid Category.";
	
	public static final String INVALID_QUEST_MODE = "Invalid question mode.";
	
	public static final String INVALID_NATURE_OF_QUEST = "Invalid nature of question.";
	
	public static final String INVALID_PLAY_INTERVAL = "Invalid play interval minutes.";
	
	public static final String CONTEST_CREATED_SUCCESS_MSG = "Contest data saved successfully.";
	
	public static final String CONTEST_QUEST_DEETE_SUCCESS_MSG = "Questions Deleted successfully.";
	
	public static final String CONTEST_DEETE_SUCCESS_MSG = "Contest Deleted successfully.";
	
	public static final String CANT_END_NON_ACTIVE_CONTEST = "Cannot end contest, Only contest with Live status can be ended.";
	
	public static final String CONTEST_END_SUCCESS_MSG = "Contest Ended successfully.";
	
	public static final String CONTEST_UPDATE_SUCCESS_MSG = "Contest data updated successfully.";
	
	public static final String CONTEST_UPDATE_TO_ACTIVE_SUCCESS_MSG = "Contest updated to active successfully.";
	
	public static final String INVALID_CONT_STATUS = "Invalid contest status.";
	
	public static final String INVALID_QBANK_STATUS = "Invalid Qbank Fetch status.";
	
	public static final String INVALID_FETCH_STATUS = "Invalid  Fetch status.";
	
	public static final String ONE_CONTEST_ONLY_IN_LIVE = "There is already live contest, Only one scratch and win contest can be live at a time.";
	
	public static final String SELECTED_CONTEST_IS_ALREADY_LIVE = "Selected contest is already Live.";
	
	public static final String ACTIVE_CONTEST_ONLY_CAN_START = "Cannot start contest, Only active contest can be started.";
	
	public static final String CANT_START_FUTURE_DATE_CONTEST = "Cannot start contest, Contest start date is future date.";
	
	public static final String CANT_START_PAST_END_DATE_CONTEST = "Cannot start contest, Contest end date is past date.";
	
	public static final String FIXED_CONTEST_WITH_NO_QUESTIONS = "Cannot start contest, Question mode is fixed and questions are not added in contest.";
	
	public static final String CONT_QUEST_MODE_FIXED = "FIXED";
	
	public static final String CONT_QUEST_MODE_RANDOM = "RANDOM";
	
	public static final String RANDOM_CONTEST_WITH_NO_QUESTIONS_IN_BANK = "Cannot start contest, Question bank does not have question with same category and subcategory mentiond at contest level.";
	
	public static final String CONTEST_START_SUCCESS_MSG = "Contest updated to live.";

	public static final String CANT_UPDATE_LIVE_CONTEST = "Not Allowed to update Live contest, Please end it first.";
	
	public static final String CONTEST_REWARD_UPDATE_SUCCESS_MSG = "Contest reward prizes updated successfully.";

	public static final String CONTEST_DATE_SHOULD_BE_TODAY_OR_FUTURE = "Contest end date should be today or future date.";
	

	public static final String INVALID_ELIMINATION_TYPE = "Invalid elimination type.";
	
	public static final String NO_OF_QUESTION_MANDATORY = "No. of question is mendatory.";
	
	public static final String REWARD_TYPE_0 = "0";
	
	public static final String WINNER_PRIZE_VALUE_MANDATORY = "Winner prize value is mendatory when winner prize type is selected.";
	
	public static final String WINNER_PRIZE_MANDATORY = "winner prize is mendatory when winner prize value is provided.";

	public static final String WINNER_PRIZE_VALUE_VALID_NUMBER = "Winner prize value should be greater than 0.";
	
	public static final String INVALID_WINNER_PRIZE_VALUE = "Invalid winner prize value.";
	
	public static final String INVALID_NO_OF_QUEST = "Invalid No. of question value.";
	
	public static final String INVALID_QUEST_TYPE_SELECT = "Please Specify Question is of: FEEDBACK OR REGUAR type";
	
	public static final String REWARD_TYPE_IS_MANDATORY = "Reward Type is mandatory";
	
	public static final String REWARD_UNIT_MEASUREMENT_IS_MANDATORY = "Reward Unit of Measurement is mandatory";
	
	public static final String REWARD_NAME_LENGTH_EXCEEDS_LIMIT = "Reward Name should be Less than 20 characters";
	
	public static final String CATEGORY_IS_MANDATORY = "Category is Mandatory ";
	
	public static final String INVALID_QUEST_ID = "Invalid Question ids.";
	
	public static final String QUESTION_ID_NOT_EXIT = "Question id not exists";
	
	public static final String SUB_CATEGORY_IS_MANDATORY = "Sub Category is Mandatory ";
	
	public static final String CANT_CHANGE_LIVE_CONTEST_REWARDS = "Not Allowed to configure reward prizes for  Live contest, Please end it first.";
	
	public static final String NO_THEMES_EXIST = "No Themes found in system.";
	
	public static final String ONLY_ACTIVE_CONTEST_CAN_BE_STARTED = "Cannot start contest, Only active contest can be started.";
	
	public static final String ADD_QUESTION_TO_CONTEST_TO_START = "Please add question to contest first to start contest.";
	
	public static final String CONT_NO_OF_QUESTION_MISMATCHES = "No. of questions are mismatching(actual questions and no. of question mentioned at contest level)";
	
	public static final String QUEST_BANK_NOT_HAVE_CONT_NO_OF_QUESTIONS = "Question bank does not have number of question mentioned at contest level with same category and subcategory.";
	
	public static final String INVALID_QUEST_MOVE_TYPE = "Invalid question move Type.";
	
	public static final String NOT_MOVE_FIRST_QUEST_TO_UP = "You can't move first question to Upside.";
	
	public static final String QUEST_MOVE_UP = "UP";
	
	public static final String QUEST_MOVE_DOWN = "DOWN";
	
	public static final String NOT_MOVE_LAST_QUEST_TO_DOWN = "You can't move last question to Downside.";
	
	public static final String QUEST_MOVE_SUCCESS_MSG = "Questions seqNo updated successfully.";
	
	public static final String CONT_REWRD_PROZE_UPDATE_SUCCESS_MSG = "Contest reward prizes updated successfully.";
	
	public static final String CONT_END_DATE_TODAY_OR_FUTURE = "Contest end date should be today or future date.";
}
