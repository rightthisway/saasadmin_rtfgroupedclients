/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.sql.dvo;

import java.io.Serializable;
import java.util.Date;

import com.rtf.saas.util.DateFormatUtil;

/**
 * The Class SubCategoryDVO.
 */
public class SubCategoryDVO implements Serializable {

	private static final long serialVersionUID = -1616147244088274386L;

	/** The cattype. */
	private String cattype;

	/** The subcattype. */
	private String subcattype;

	/** The isactive. */
	private Boolean isactive;

	/** The subcatdesc. */
	private String subcatdesc;

	/** The creby. */
	private String creby;

	/** The updby. */
	private String updby;

	/** The credate. */
	private Date credate;

	/** The upddate. */
	private Date upddate;

	/** The cr date time str. */
	private String crDateTimeStr;

	/** The up date time str. */
	private String upDateTimeStr;

	/**
	 * Gets the cattype.
	 *
	 * @return the cattype
	 */
	public String getCattype() {
		return cattype;
	}

	/**
	 * Sets the cattype.
	 *
	 * @param cattype
	 *            the new cattype
	 */
	public void setCattype(String cattype) {
		this.cattype = cattype;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "SubCategoryDVO [cattype=" + cattype + ", subcattype=" + subcattype + ", isactive=" + isactive
				+ ", subcatdesc=" + subcatdesc + ", creby=" + creby + ", updby=" + updby + ", credate=" + credate
				+ ", upddate=" + upddate + ", crDateTimeStr=" + crDateTimeStr + ", upDateTimeStr=" + upDateTimeStr
				+ "]";
	}

	/**
	 * Gets the subcattype.
	 *
	 * @return the subcattype
	 */
	public String getSubcattype() {
		return subcattype;
	}

	/**
	 * Sets the subcattype.
	 *
	 * @param subcattype
	 *            the new subcattype
	 */
	public void setSubcattype(String subcattype) {
		this.subcattype = subcattype;
	}

	/**
	 * Gets the isactive.
	 *
	 * @return the isactive
	 */
	public Boolean getIsactive() {
		return isactive;
	}

	/**
	 * Sets the isactive.
	 *
	 * @param isactive
	 *            the new isactive
	 */
	public void setIsactive(Boolean isactive) {
		this.isactive = isactive;
	}

	/**
	 * Gets the subcatdesc.
	 *
	 * @return the subcatdesc
	 */
	public String getSubcatdesc() {
		return subcatdesc;
	}

	/**
	 * Sets the subcatdesc.
	 *
	 * @param subcatdesc
	 *            the new subcatdesc
	 */
	public void setSubcatdesc(String subcatdesc) {
		this.subcatdesc = subcatdesc;
	}

	/**
	 * Gets the creby.
	 *
	 * @return the creby
	 */
	public String getCreby() {
		return creby;
	}

	/**
	 * Sets the creby.
	 *
	 * @param creby
	 *            the new creby
	 */
	public void setCreby(String creby) {
		this.creby = creby;
	}

	/**
	 * Gets the updby.
	 *
	 * @return the updby
	 */
	public String getUpdby() {
		return updby;
	}

	/**
	 * Sets the updby.
	 *
	 * @param updby
	 *            the new updby
	 */
	public void setUpdby(String updby) {
		this.updby = updby;
	}

	/**
	 * Gets the credate.
	 *
	 * @return the credate
	 */
	public Date getCredate() {
		return credate;
	}

	/**
	 * Sets the credate.
	 *
	 * @param credate
	 *            the new credate
	 */
	public void setCredate(Date credate) {
		this.credate = credate;
	}

	/**
	 * Gets the upddate.
	 *
	 * @return the upddate
	 */
	public Date getUpddate() {
		return upddate;
	}

	/**
	 * Sets the upddate.
	 *
	 * @param upddate
	 *            the new upddate
	 */
	public void setUpddate(Date upddate) {
		this.upddate = upddate;
	}

	/**
	 * Gets the cr date time str.
	 *
	 * @return the cr date time str
	 */
	public String getCrDateTimeStr() {
		crDateTimeStr = DateFormatUtil.getDateMMDDYYYYYHHmmss(credate);
		return crDateTimeStr;
	}

	/**
	 * Sets the cr date time str.
	 *
	 * @param crDateTimeStr
	 *            the new cr date time str
	 */
	public void setCrDateTimeStr(String crDateTimeStr) {
		this.crDateTimeStr = crDateTimeStr;
	}

	/**
	 * Gets the up date time str.
	 *
	 * @return the up date time str
	 */
	public String getUpDateTimeStr() {
		upDateTimeStr = DateFormatUtil.getDateMMDDYYYYYHHmmss(upddate);
		return upDateTimeStr;
	}

	/**
	 * Sets the up date time str.
	 *
	 * @param upDateTimeStr
	 *            the new up date time str
	 */
	public void setUpDateTimeStr(String upDateTimeStr) {
		this.upDateTimeStr = upDateTimeStr;
	}

}
