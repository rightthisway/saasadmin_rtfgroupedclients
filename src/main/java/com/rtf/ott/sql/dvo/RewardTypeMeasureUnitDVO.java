/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.sql.dvo;

import java.io.Serializable;
import java.util.Date;

/**
 * The Class RewardTypeMeasureUnitDVO.
 */
public class RewardTypeMeasureUnitDVO implements Serializable{
	
	private static final long serialVersionUID = 3405987005085502029L;

	/** The clintid. */
	private String clintid;
	
	/** The rwdunimesr. */
	private String rwdunimesr;
	
	/** The creby. */
	private String creby;
	
	/** The unimesrdesc. */
	private String unimesrdesc;
	
	/** The updby. */
	private String updby;
	
	/** The credate. */
	private Date credate;
	
	/** The upddate. */
	private Date upddate;
	
	/** The isactive. */
	private Boolean  isactive;
	
	/**
	 * Gets the clintid.
	 *
	 * @return the clintid
	 */
	public String getClintid() {
		return clintid;
	}
	
	/**
	 * Sets the clintid.
	 *
	 * @param clintid the new clintid
	 */
	public void setClintid(String clintid) {
		this.clintid = clintid;
	}
	
	/**
	 * Gets the rwdunimesr.
	 *
	 * @return the rwdunimesr
	 */
	public String getRwdunimesr() {
		return rwdunimesr;
	}
	
	/**
	 * Sets the rwdunimesr.
	 *
	 * @param rwdunimesr the new rwdunimesr
	 */
	public void setRwdunimesr(String rwdunimesr) {
		this.rwdunimesr = rwdunimesr;
	}
	
	/**
	 * Gets the creby.
	 *
	 * @return the creby
	 */
	public String getCreby() {
		return creby;
	}
	
	/**
	 * Sets the creby.
	 *
	 * @param creby the new creby
	 */
	public void setCreby(String creby) {
		this.creby = creby;
	}
	
	/**
	 * Gets the unimesrdesc.
	 *
	 * @return the unimesrdesc
	 */
	public String getUnimesrdesc() {
		return unimesrdesc;
	}
	
	/**
	 * Sets the unimesrdesc.
	 *
	 * @param unimesrdesc the new unimesrdesc
	 */
	public void setUnimesrdesc(String unimesrdesc) {
		this.unimesrdesc = unimesrdesc;
	}
	
	/**
	 * Gets the updby.
	 *
	 * @return the updby
	 */
	public String getUpdby() {
		return updby;
	}
	
	/**
	 * Sets the updby.
	 *
	 * @param updby the new updby
	 */
	public void setUpdby(String updby) {
		this.updby = updby;
	}
	
	/**
	 * Gets the credate.
	 *
	 * @return the credate
	 */
	public Date getCredate() {
		return credate;
	}
	
	/**
	 * Sets the credate.
	 *
	 * @param credate the new credate
	 */
	public void setCredate(Date credate) {
		this.credate = credate;
	}
	
	/**
	 * Gets the upddate.
	 *
	 * @return the upddate
	 */
	public Date getUpddate() {
		return upddate;
	}
	
	/**
	 * Sets the upddate.
	 *
	 * @param upddate the new upddate
	 */
	public void setUpddate(Date upddate) {
		this.upddate = upddate;
	}
	
	/**
	 * Gets the isactive.
	 *
	 * @return the isactive
	 */
	public Boolean getIsactive() {
		return isactive;
	}
	
	/**
	 * Sets the isactive.
	 *
	 * @param isactive the new isactive
	 */
	public void setIsactive(Boolean isactive) {
		this.isactive = isactive;
	}
	
	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "RewardTypeMeasureUnitDVO [clintid=" + clintid + ", rwdunimesr=" + rwdunimesr + ", creby=" + creby
				+ ", unimesrdesc=" + unimesrdesc + ", updby=" + updby + ", credate=" + credate + ", upddate=" + upddate
				+ ", isactive=" + isactive + "]";
	}
	


}
