/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.sql.dvo;

import com.rtf.saas.sql.dvo.SaasBaseDVO;

/**
 * The Class QuestionBankDVO.
 */
public class QuestionBankDVO extends SaasBaseDVO {

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "QuestionBankDVO [clintid=" + clintid + ", qsnbnkid=" + qsnbnkid + ", opa=" + opa + ", opb=" + opb
				+ ", opc=" + opc + ", opd=" + opd + ", anstype=" + anstype + ", ctyp=" + ctyp + ", cans=" + cans
				+ ", isActive=" + isActive + ", qOrientn=" + qOrientn + ", qtx=" + qtx + ", sctyp=" + sctyp
				+ ", getUpdDt()=" + getUpdDt() + ", getCreDate()=" + getCreDate() + ", getCrDateTimeStr()="
				+ getCrDateTimeStr() + ", getUpDateTimeStr()=" + getUpDateTimeStr() + ", getUpdBy()=" + getUpdBy()
				+ ", getCreBy()=" + getCreBy() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}

	/** The clintid. */
	private String clintid;

	/** The qsnbnkid. */
	private Integer qsnbnkid;

	/** The opa. */
	private String opa;

	/** The opb. */
	private String opb;

	/** The opc. */
	private String opc;

	/** The opd. */
	private String opd;

	/** The anstype. */
	private String anstype;

	/** The ctyp. */
	private String ctyp;

	/** The cans. */
	private String cans;

	/** The is active. */
	private Boolean isActive;

	/** The q orientn. */
	private String qOrientn;

	/** The qtx. */
	private String qtx;

	/** The sctyp. */
	private String sctyp;
	
	/** The is question validated. */
	private Boolean isVldtd;
	
	
	/** The is question validated. */
	private String factChkd;

	/**
	 * Gets the clintid.
	 *
	 * @return the clintid
	 */
	public String getClintid() {
		return clintid;
	}

	/**
	 * Sets the clintid.
	 *
	 * @param clintid
	 *            the new clintid
	 */
	public void setClintid(String clintid) {
		this.clintid = clintid;
	}

	/**
	 * Gets the qsnbnkid.
	 *
	 * @return the qsnbnkid
	 */
	public Integer getQsnbnkid() {
		return qsnbnkid;
	}

	/**
	 * Sets the qsnbnkid.
	 *
	 * @param qsnbnkid
	 *            the new qsnbnkid
	 */
	public void setQsnbnkid(Integer qsnbnkid) {
		this.qsnbnkid = qsnbnkid;
	}

	/**
	 * Gets the opa.
	 *
	 * @return the opa
	 */
	public String getOpa() {
		return opa;
	}

	/**
	 * Sets the opa.
	 *
	 * @param opa
	 *            the new opa
	 */
	public void setOpa(String opa) {
		this.opa = opa;
	}

	/**
	 * Gets the opb.
	 *
	 * @return the opb
	 */
	public String getOpb() {
		return opb;
	}

	/**
	 * Sets the opb.
	 *
	 * @param opb
	 *            the new opb
	 */
	public void setOpb(String opb) {
		this.opb = opb;
	}

	/**
	 * Gets the opc.
	 *
	 * @return the opc
	 */
	public String getOpc() {
		return opc;
	}

	/**
	 * Sets the opc.
	 *
	 * @param opc
	 *            the new opc
	 */
	public void setOpc(String opc) {
		this.opc = opc;
	}

	/**
	 * Gets the opd.
	 *
	 * @return the opd
	 */
	public String getOpd() {
		return opd;
	}

	/**
	 * Sets the opd.
	 *
	 * @param opd
	 *            the new opd
	 */
	public void setOpd(String opd) {
		this.opd = opd;
	}

	/**
	 * Gets the anstype.
	 *
	 * @return the anstype
	 */
	public String getAnstype() {
		return anstype;
	}

	/**
	 * Sets the anstype.
	 *
	 * @param anstype
	 *            the new anstype
	 */
	public void setAnstype(String anstype) {
		this.anstype = anstype;
	}

	/**
	 * Gets the ctyp.
	 *
	 * @return the ctyp
	 */
	public String getCtyp() {
		return ctyp;
	}

	/**
	 * Sets the ctyp.
	 *
	 * @param ctyp
	 *            the new ctyp
	 */
	public void setCtyp(String ctyp) {
		this.ctyp = ctyp;
	}

	/**
	 * Gets the cans.
	 *
	 * @return the cans
	 */
	public String getCans() {
		return cans;
	}

	/**
	 * Sets the cans.
	 *
	 * @param cans
	 *            the new cans
	 */
	public void setCans(String cans) {
		this.cans = cans;
	}

	/**
	 * Gets the checks if is active.
	 *
	 * @return the checks if is active
	 */
	public Boolean getIsActive() {
		return isActive;
	}

	/**
	 * Sets the checks if is active.
	 *
	 * @param isActive
	 *            the new checks if is active
	 */
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	/**
	 * Gets the q orientn.
	 *
	 * @return the q orientn
	 */
	public String getqOrientn() {
		return qOrientn;
	}

	/**
	 * Sets the q orientn.
	 *
	 * @param qOrientn
	 *            the new q orientn
	 */
	public void setqOrientn(String qOrientn) {
		this.qOrientn = qOrientn;
	}

	/**
	 * Gets the qtx.
	 *
	 * @return the qtx
	 */
	public String getQtx() {
		return qtx;
	}

	/**
	 * Sets the qtx.
	 *
	 * @param qtx
	 *            the new qtx
	 */
	public void setQtx(String qtx) {
		this.qtx = qtx;
	}

	/**
	 * Gets the sctyp.
	 *
	 * @return the sctyp
	 */
	public String getSctyp() {
		return sctyp;
	}

	/**
	 * Sets the sctyp.
	 *
	 * @param sctyp
	 *            the new sctyp
	 */
	public void setSctyp(String sctyp) {
		this.sctyp = sctyp;
	}

	/**
	 * Gets the question validated.
	 *
	 * @return the isVldtd
	 */
	public Boolean getIsVldtd() {
		return isVldtd;
	}

	/**
	 * Sets the question validated.
	 *
	 * @param isVldtd
	 */
	public void setIsVldtd(Boolean isVldtd) {
		this.isVldtd = isVldtd;
	}

	/**
	 * Gets the question fact is checked.
	 *
	 * @return the factChkd
	 */
	public String getFactChkd() {
		factChkd = "NO";
		if(isVldtd!=null && isVldtd){
			factChkd = "YES";
		}
		return factChkd;
	}

	/**
	 * Sets the question fact is checked.
	 *
	 * @param factChkd
	 */
	public void setFactChkd(String factChkd) {
		this.factChkd = factChkd;
	}
	
	
	
}
