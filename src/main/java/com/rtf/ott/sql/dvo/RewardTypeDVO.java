/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.sql.dvo;

import java.io.Serializable;
import java.util.Date;

import com.rtf.saas.util.DateFormatUtil;

/**
 * The Class RewardTypeDVO.
 */
public class RewardTypeDVO implements Serializable{

	private static final long serialVersionUID = -7102581180083311896L;

	/** The cl id. */
	private String clId;

	/** The rwdtype. */
	private String rwdtype;

	/** The rwddesc. */
	private String rwddesc;

	/** The rwdunimesr. */
	private String rwdunimesr;

	/** The rwdimgurl. */
	private String rwdimgurl;

	/** The isactive. */
	private Boolean isactive;

	/** The creby. */
	private String creby;

	/** The updby. */
	private String updby;

	/** The credate. */
	private Date credate;

	/** The upddate. */
	private Date upddate;

	/** The cr date time str. */
	private String crDateTimeStr;

	/** The up date time str. */
	private String upDateTimeStr;

	/**
	 * Gets the cl id.
	 *
	 * @return the cl id
	 */
	public String getClId() {
		return clId;
	}

	/**
	 * Sets the cl id.
	 *
	 * @param clId
	 *            the new cl id
	 */
	public void setClId(String clId) {
		this.clId = clId;
	}

	/**
	 * Gets the rwdtype.
	 *
	 * @return the rwdtype
	 */
	public String getRwdtype() {
		return rwdtype;
	}

	/**
	 * Sets the rwdtype.
	 *
	 * @param rwdtype
	 *            the new rwdtype
	 */
	public void setRwdtype(String rwdtype) {
		this.rwdtype = rwdtype;
	}

	/**
	 * Gets the rwddesc.
	 *
	 * @return the rwddesc
	 */
	public String getRwddesc() {
		return rwddesc;
	}

	/**
	 * Sets the rwddesc.
	 *
	 * @param rwddesc
	 *            the new rwddesc
	 */
	public void setRwddesc(String rwddesc) {
		this.rwddesc = rwddesc;
	}

	/**
	 * Gets the rwdunimesr.
	 *
	 * @return the rwdunimesr
	 */
	public String getRwdunimesr() {
		return rwdunimesr;
	}

	/**
	 * Sets the rwdunimesr.
	 *
	 * @param rwdunimesr
	 *            the new rwdunimesr
	 */
	public void setRwdunimesr(String rwdunimesr) {
		this.rwdunimesr = rwdunimesr;
	}

	/**
	 * Gets the rwdimgurl.
	 *
	 * @return the rwdimgurl
	 */
	public String getRwdimgurl() {
		return rwdimgurl;
	}

	/**
	 * Sets the rwdimgurl.
	 *
	 * @param rwdimgurl
	 *            the new rwdimgurl
	 */
	public void setRwdimgurl(String rwdimgurl) {
		this.rwdimgurl = rwdimgurl;
	}

	/**
	 * Gets the isactive.
	 *
	 * @return the isactive
	 */
	public Boolean getIsactive() {
		return isactive;
	}

	/**
	 * Sets the isactive.
	 *
	 * @param isactive
	 *            the new isactive
	 */
	public void setIsactive(Boolean isactive) {
		this.isactive = isactive;
	}

	/**
	 * Gets the creby.
	 *
	 * @return the creby
	 */
	public String getCreby() {
		return creby;
	}

	/**
	 * Sets the creby.
	 *
	 * @param creby
	 *            the new creby
	 */
	public void setCreby(String creby) {
		this.creby = creby;
	}

	/**
	 * Gets the updby.
	 *
	 * @return the updby
	 */
	public String getUpdby() {
		return updby;
	}

	/**
	 * Sets the updby.
	 *
	 * @param updby
	 *            the new updby
	 */
	public void setUpdby(String updby) {
		this.updby = updby;
	}

	/**
	 * Gets the credate.
	 *
	 * @return the credate
	 */
	public Date getCredate() {
		return credate;
	}

	/**
	 * Sets the credate.
	 *
	 * @param credate
	 *            the new credate
	 */
	public void setCredate(Date credate) {
		this.credate = credate;
	}

	/**
	 * Gets the upddate.
	 *
	 * @return the upddate
	 */
	public Date getUpddate() {
		return upddate;
	}

	/**
	 * Sets the upddate.
	 *
	 * @param upddate
	 *            the new upddate
	 */
	public void setUpddate(Date upddate) {
		this.upddate = upddate;
	}

	/**
	 * Gets the cr date time str.
	 *
	 * @return the cr date time str
	 */
	public String getCrDateTimeStr() {
		crDateTimeStr = DateFormatUtil.getDateMMDDYYYYYHHmmss(credate);
		return crDateTimeStr;
	}

	/**
	 * Sets the cr date time str.
	 *
	 * @param crDateTimeStr
	 *            the new cr date time str
	 */
	public void setCrDateTimeStr(String crDateTimeStr) {
		this.crDateTimeStr = crDateTimeStr;
	}

	/**
	 * Gets the up date time str.
	 *
	 * @return the up date time str
	 */
	public String getUpDateTimeStr() {
		upDateTimeStr = DateFormatUtil.getDateMMDDYYYYYHHmmss(upddate);
		return upDateTimeStr;
	}

	/**
	 * Sets the up date time str.
	 *
	 * @param upDateTimeStr
	 *            the new up date time str
	 */
	public void setUpDateTimeStr(String upDateTimeStr) {
		this.upDateTimeStr = upDateTimeStr;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "RewardTypeDVO [clId=" + clId + ", rwdtype=" + rwdtype + ", rwddesc=" + rwddesc + ", rwdunimesr="
				+ rwdunimesr + ", rwdimgurl=" + rwdimgurl + ", isactive=" + isactive + ", creby=" + creby + ", updby="
				+ updby + ", credate=" + credate + ", upddate=" + upddate + ", crDateTimeStr=" + crDateTimeStr
				+ ", upDateTimeStr=" + upDateTimeStr + "]";
	}

}
