/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.sql.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.rtf.common.util.GridSortingUtil;
import com.rtf.ott.sql.dvo.SubCategoryDVO;
import com.rtf.ott.util.StatusEnums;
import com.rtf.saas.sql.db.DatabaseConnections;
import com.rtf.saas.util.Constant;
import com.rtf.saas.util.PaginationUtil;

/**
 * The Class SubCategorySQLDAO.
 */
public class SubCategorySQLDAO {

	/*
	 * @param Clientid
	 * 
	 * @return
	 * 
	 * @throws Exception
	 */

	/**
	 * Gets the all sub categories for cat.
	 *
	 * @param clientId
	 *            the client id
	 * @param category
	 *            the category
	 * @return the all sub categories for cat
	 * @throws Exception
	 *             the exception
	 */
	public static List<SubCategoryDVO> getAllSubCategoriesForCat(String clientId, String category) throws Exception {

		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		List<SubCategoryDVO> scatList = null;
		StringBuffer sql = new StringBuffer(" SELECT * from  sd_sub_category_type with(nolock) where clintid = ? and isactive = ? and cattype = ?");
		try {
			conn = DatabaseConnections.getSaasAdminConnection();

			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			ps.setString(2, StatusEnums.TRUE.name());
			ps.setString(3, category);
			rs = ps.executeQuery();
			scatList = new ArrayList<SubCategoryDVO>();
			while (rs.next()) {
				SubCategoryDVO vo = new SubCategoryDVO();
				vo.setCattype(rs.getString("cattype"));
				vo.setSubcattype(rs.getString("subcattype"));
				vo.setSubcatdesc(rs.getString("subcatdesc"));
				vo.setIsactive(rs.getBoolean("isactive"));
				vo.setCreby(rs.getString("creby"));
				vo.setUpdby(rs.getString("updby"));
				vo.setUpddate(rs.getDate("upddate"));
				vo.setCredate(rs.getDate("credate"));
				scatList.add(vo);
			}
			return scatList;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;

	}

	/**
	 * Gets the all sub categorieswith filter.
	 *
	 * @param clientId
	 *            the client id
	 * @param filterQuery
	 *            the filter query
	 * @param pgNo
	 *            the pg no
	 * @return the all sub categorieswith filter
	 * @throws Exception
	 *             the exception
	 */
	public static List<SubCategoryDVO> getAllSubCategorieswithFilter(String clientId, String filterQuery, String pgNo,String filter)
			throws Exception {

		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		List<SubCategoryDVO> scatList = null;
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT * from  sd_sub_category_type  " + "with(nolock) where clintid = ? and isactive = ?");

		String paginationQuery = PaginationUtil.getPaginationQuery(pgNo);
		if (filterQuery != null && !filterQuery.isEmpty()) {
			sql.append(filterQuery);
		}
		String sortingSql = GridSortingUtil.getSubCategoriesSortingQuery(filter);
		if(sortingSql!=null && !sortingSql.trim().isEmpty()){
			sql.append(sortingSql);
		}else{
			sql.append("  order by subcattype ");
		}
		
		sql.append(paginationQuery);

		try {
			conn = DatabaseConnections.getSaasAdminConnection();

			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			ps.setString(2, StatusEnums.TRUE.name());
			rs = ps.executeQuery();
			scatList = new ArrayList<SubCategoryDVO>();
			while (rs.next()) {
				SubCategoryDVO vo = new SubCategoryDVO();
				Timestamp timestamp = null;
				vo.setCattype(rs.getString("cattype"));
				vo.setSubcattype(rs.getString("subcattype"));
				vo.setSubcatdesc(rs.getString("subcatdesc"));
				vo.setIsactive(rs.getBoolean("isactive"));
				vo.setCreby(rs.getString("creby"));
				vo.setUpdby(rs.getString("updby"));
				timestamp = rs.getTimestamp("credate");
				vo.setCredate(new java.util.Date(timestamp != null ? timestamp.getTime() : -10));
				timestamp = rs.getTimestamp("upddate");
				vo.setUpddate(new java.util.Date(timestamp != null ? timestamp.getTime() : -10));
				scatList.add(vo);
			}
			return scatList;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;

	}

	/**
	 * Gets the all sub categories data to export.
	 *
	 * @param clientId
	 *            the client id
	 * @param filterQuery
	 *            the filter query
	 * @return the all sub categories data to export
	 * @throws Exception
	 *             the exception
	 */
	public static List<SubCategoryDVO> getAllSubCategoriesDataToExport(String clientId, String filterQuery)
			throws Exception {

		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		List<SubCategoryDVO> scatList = null;
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT * from  sd_sub_category_type  " + "with(nolock) where clintid = ? and isactive = ?");

		if (filterQuery != null && !filterQuery.isEmpty()) {
			sql.append(filterQuery);
		}
		sql.append("  order by subcattype ");

		try {
			conn = DatabaseConnections.getSaasAdminConnection();

			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			ps.setString(2, StatusEnums.TRUE.name());
			rs = ps.executeQuery();
			scatList = new ArrayList<SubCategoryDVO>();
			while (rs.next()) {
				SubCategoryDVO vo = new SubCategoryDVO();
				Timestamp timestamp = null;
				vo.setCattype(rs.getString("cattype"));
				vo.setSubcattype(rs.getString("subcattype"));
				vo.setSubcatdesc(rs.getString("subcatdesc"));
				vo.setIsactive(rs.getBoolean("isactive"));
				vo.setCreby(rs.getString("creby"));
				vo.setUpdby(rs.getString("updby"));
				timestamp = rs.getTimestamp("credate");
				vo.setCredate(new java.util.Date(timestamp != null ? timestamp.getTime() : -10));
				timestamp = rs.getTimestamp("upddate");
				vo.setUpddate(new java.util.Date(timestamp != null ? timestamp.getTime() : -10));
				scatList.add(vo);
			}
			return scatList;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;

	}

	/**
	 * Gets the all active sub categories countwith filter.
	 *
	 * @param clientId
	 *            the client id
	 * @param filterQuery
	 *            the filter query
	 * @return the all active sub categories countwith filter
	 * @throws Exception
	 *             the exception
	 */
	public static Integer getAllActiveSubCategoriesCountwithFilter(String clientId, String filterQuery)
			throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Integer count = 0;
		try {

			StringBuffer sql = new StringBuffer(" SELECT count(*) from  sd_sub_category_type  with(nolock) where clintid = ? and isactive = ? ");

			if (filterQuery != null && !filterQuery.isEmpty()) {
				sql.append(filterQuery);
			}
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			ps.setString(2, StatusEnums.TRUE.name());

			rs = ps.executeQuery();
			while (rs.next()) {
				count = rs.getInt(1);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return count;
	}

	/**
	 * Gets the all sub categories.
	 *
	 * @param clientId
	 *            the client id
	 * @param pNo
	 *            the no
	 * @return the all sub categories
	 * @throws Exception
	 *             the exception
	 */
	public static List<SubCategoryDVO> getAllSubCategories(String clientId, String pNo) throws Exception {

		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		List<SubCategoryDVO> scatList = null;
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT * from  sd_sub_category_type  ")
				.append(" with(nolock) where clintid = ? and isactive = ? order by cattype " + " OFFSET (").append(pNo)
				.append("-1)*").append(Constant.NoOfRecordsPerScreen + " ROWS FETCH NEXT ")
				.append(Constant.NoOfRecordsPerScreen).append("  ROWS ONLY");
		try {
			conn = DatabaseConnections.getSaasAdminConnection();

			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			ps.setString(2, StatusEnums.TRUE.name());
			rs = ps.executeQuery();
			scatList = new ArrayList<SubCategoryDVO>();
			while (rs.next()) {
				SubCategoryDVO vo = new SubCategoryDVO();
				vo.setCattype(rs.getString("cattype"));
				vo.setSubcattype(rs.getString("subcattype"));
				vo.setSubcatdesc(rs.getString("subcatdesc"));
				vo.setIsactive(rs.getBoolean("isactive"));
				vo.setCreby(rs.getString("creby"));
				vo.setUpdby(rs.getString("updby"));
				vo.setUpddate(rs.getDate("upddate"));
				vo.setCredate(rs.getDate("credate"));
				scatList.add(vo);
			}
			return scatList;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;

	}

	/**
	 * Creates the sub category.
	 *
	 * @param userId
	 *            the user id
	 * @param clientid
	 *            the clientid
	 * @param dvo
	 *            the dvo
	 * @return the integer
	 * @throws Exception
	 *             the exception
	 */
	public static Integer createSubCategory(String userId, String clientid, SubCategoryDVO dvo) throws Exception {

		Connection conn = null;
		PreparedStatement ps = null;
		Integer affectedRows = 0;
		StringBuffer sql = new StringBuffer();
		sql.append(" insert into  sd_sub_category_type ( clintid , cattype, subcattype ")
				.append("   , subcatdesc  , isactive   , creby ,credate,updby,upddate ) ")
				.append("	values(?,?,?,?,?,?,getdate(),?,getDate()) ");
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientid);
			ps.setString(2, dvo.getCattype());
			ps.setString(3, dvo.getSubcattype());
			ps.setString(4, dvo.getSubcatdesc());
			ps.setBoolean(5, Boolean.TRUE);
			ps.setString(6, userId);
			ps.setString(7, userId);
			affectedRows = ps.executeUpdate();
			return affectedRows;

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return affectedRows;
	}

	/**
	 * Delete sub category.
	 *
	 * @param userId
	 *            the user id
	 * @param clientid
	 *            the clientid
	 * @param dvo
	 *            the dvo
	 * @return the integer
	 * @throws Exception
	 *             the exception
	 */
	public static Integer deleteSubCategory(String userId, String clientid, SubCategoryDVO dvo) throws Exception {

		Connection conn = null;
		Integer affectedRows = 0;
		PreparedStatement ps = null;
		StringBuffer sql = new StringBuffer(" update  sd_sub_category_type  set isactive = ?  , updby = ? , upddate= getDate() where clintid =  ? and cattype = ? and subcattype = ? ");

		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setBoolean(1, Boolean.FALSE);
			ps.setString(2, userId);
			ps.setString(3, clientid);
			ps.setString(4, dvo.getCattype());
			ps.setString(5, dvo.getSubcattype());

			affectedRows = ps.executeUpdate();
			return affectedRows;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return affectedRows;
	}

	/**
	 * Gets the default sub categories for cat.
	 *
	 * @param clientId
	 *            the client id
	 * @param category
	 *            the category
	 * @return the default sub categories for cat
	 * @throws Exception
	 *             the exception
	 */
	public static List<SubCategoryDVO> getDefaultSubCategoriesForCat(String clientId, String category)
			throws Exception {

		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		List<SubCategoryDVO> scatList = null;
		StringBuffer sql = new StringBuffer(" SELECT * from  sd_sub_category_type with(nolock) where clintid = ? and isactive = ? and cattype = ? and subcattype = ?");
		try {
			conn = DatabaseConnections.getSaasAdminConnection();

			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			ps.setString(2, StatusEnums.TRUE.name());
			ps.setString(3, category);
			ps.setString(4, Constant.ALL_SUB_CATEGORY);
			rs = ps.executeQuery();
			scatList = new ArrayList<SubCategoryDVO>();
			while (rs.next()) {
				SubCategoryDVO vo = new SubCategoryDVO();
				vo.setCattype(rs.getString("cattype"));
				vo.setSubcattype(rs.getString("subcattype"));
				vo.setSubcatdesc(rs.getString("subcatdesc"));
				vo.setIsactive(rs.getBoolean("isactive"));
				vo.setCreby(rs.getString("creby"));
				vo.setUpdby(rs.getString("updby"));
				vo.setUpddate(rs.getDate("upddate"));
				vo.setCredate(rs.getDate("credate"));
				scatList.add(vo);
			}
			return scatList;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;

	}
}
