/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.sql.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.rtf.common.util.GridSortingUtil;
import com.rtf.ott.sql.dvo.ContestQuestionDVO;
import com.rtf.saas.sql.db.DatabaseConnections;
import com.rtf.saas.util.PaginationUtil;

/**
 * The Class ContestQuestionsDAO.
 */
public class ContestQuestionsDAO {
	/**
	 * Gets the contestquestions by question id.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 * @param questionId
	 *            the question id
	 * @return the contestquestions by question id
	 */
	public static ContestQuestionDVO getcontestquestionsByQuestionId(String clientId, String contestId,
			Integer questionId) {

		String saasAdminLinkedServer = DatabaseConnections.getSaasAdminLinkedServer(clientId) + ".dbo.";
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT * FROM ").append(saasAdminLinkedServer).append("pa_ofltx_contest_question where  clintid='")
				.append(clientId).append("' and  conid='").append(contestId).append("' and conqsnid = '")
				.append(questionId + "'");

		try {
			Connection conn = DatabaseConnections.getSaasAdminConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql.toString());

			ContestQuestionDVO obj = null;
			while (rs.next()) {
				obj = new ContestQuestionDVO();
				obj.setClintid(rs.getString("clintid"));
				obj.setConqsnid(rs.getInt("conqsnid"));
				obj.setConid(rs.getString("conid"));
				obj.setQsnseqno(rs.getInt("qsnseqno"));
				obj.setIsactive(rs.getBoolean("isactive"));
				obj.setOpa(rs.getString("ansopta"));
				obj.setOpb(rs.getString("ansoptb"));
				obj.setOpc(rs.getString("ansoptc"));
				obj.setOpd(rs.getString("ansoptd"));
				obj.setCorans(rs.getString("corans"));
				obj.setCreby(rs.getString("creby"));
				obj.setCredate(new Date(rs.getTimestamp("credate").getTime()));
				obj.setQbid(rs.getInt("qsnbnkid"));
				obj.setQsnorient(rs.getString("qsnorient"));
				obj.setQsntext(rs.getString("qsntext"));
				obj.setRwdtype(rs.getString("rwdtype"));
				obj.setRwdvalue(rs.getDouble("rwdval"));
				obj.setUpdby(rs.getString("updby"));
				if (rs.getTimestamp("upddate") != null) {
					obj.setUpddate(new Date(rs.getTimestamp("upddate").getTime()));
				}
			}
			rs.close();
			stmt.close();
			DatabaseConnections.closeConnection(conn);
			return obj;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Gets the contestquestions by contest idand question seq no.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 * @param seqno
	 *            the seqno
	 * @return the contestquestions by contest idand question seq no
	 */
	public static ContestQuestionDVO getcontestquestionsByContestIdandQuestionSeqNo(String clientId, String contestId,
			Integer seqno) {

		String saasAdminLinkedServer = DatabaseConnections.getSaasAdminLinkedServer(clientId) + ".dbo.";
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT * FROM ").append(saasAdminLinkedServer).append("pa_ofltx_contest_question where  clintid='")
				.append(clientId).append("' and conid='").append(contestId).append("' and qsnseqno = ").append(seqno);

		try {
			Connection conn = DatabaseConnections.getSaasAdminConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql.toString());

			ContestQuestionDVO obj = null;
			while (rs.next()) {
				obj = new ContestQuestionDVO();
				obj.setClintid(rs.getString("clintid"));
				obj.setConqsnid(rs.getInt("conqsnid"));
				obj.setConid(rs.getString("conid"));
				obj.setQsnseqno(rs.getInt("qsnseqno"));
				obj.setIsactive(rs.getBoolean("isactive"));
				obj.setOpa(rs.getString("ansopta"));
				obj.setOpb(rs.getString("ansoptb"));
				obj.setOpc(rs.getString("ansoptc"));
				obj.setOpd(rs.getString("ansoptd"));
				obj.setCorans(rs.getString("corans"));
				obj.setCreby(rs.getString("creby"));
				obj.setCredate(new Date(rs.getTimestamp("credate").getTime()));
				obj.setQbid(rs.getInt("qsnbnkid"));
				obj.setQsnorient(rs.getString("qsnorient"));
				obj.setQsntext(rs.getString("qsntext"));
				obj.setRwdtype(rs.getString("rwdtype"));
				obj.setRwdvalue(rs.getDouble("rwdval"));
				obj.setUpdby(rs.getString("updby"));
				if (rs.getTimestamp("upddate") != null) {
					obj.setUpddate(new Date(rs.getTimestamp("upddate").getTime()));
				}
			}
			rs.close();
			stmt.close();
			DatabaseConnections.closeConnection(conn);
			return obj;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Gets the contestquestions by contest idand question bank id.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 * @param questionBankId
	 *            the question bank id
	 * @return the contestquestions by contest idand question bank id
	 */
	public static ContestQuestionDVO getcontestquestionsByContestIdandQuestionBankId(String clientId, String contestId,
			Integer questionBankId) {

		String saasAdminLinkedServer = DatabaseConnections.getSaasAdminLinkedServer(clientId) + ".dbo.";
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT * FROM ").append(saasAdminLinkedServer).append("pa_ofltx_contest_question where  clintid='")
				.append(clientId).append("' and conid='").append(contestId).append("' and qsnbnkid = ")
				.append(questionBankId);

		try {
			Connection conn = DatabaseConnections.getSaasAdminConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql.toString());

			ContestQuestionDVO obj = null;
			while (rs.next()) {
				obj = new ContestQuestionDVO();
				obj.setClintid(rs.getString("clintid"));
				obj.setConqsnid(rs.getInt("conqsnid"));
				obj.setConid(rs.getString("conid"));
				obj.setQsnseqno(rs.getInt("qsnseqno"));
				obj.setIsactive(rs.getBoolean("isactive"));
				obj.setOpa(rs.getString("ansopta"));
				obj.setOpb(rs.getString("ansoptb"));
				obj.setOpc(rs.getString("ansoptc"));
				obj.setOpd(rs.getString("ansoptd"));
				obj.setCorans(rs.getString("corans"));
				obj.setCreby(rs.getString("creby"));
				obj.setCredate(new Date(rs.getTimestamp("credate").getTime()));
				obj.setQbid(rs.getInt("qsnbnkid"));
				obj.setQsnorient(rs.getString("qsnorient"));
				obj.setQsntext(rs.getString("qsntext"));
				obj.setRwdtype(rs.getString("rwdtype"));
				obj.setRwdvalue(rs.getDouble("rwdval"));
				obj.setUpdby(rs.getString("updby"));
				if (rs.getTimestamp("upddate") != null) {
					obj.setUpddate(new Date(rs.getTimestamp("upddate").getTime()));
				}
			}
			rs.close();
			stmt.close();
			DatabaseConnections.closeConnection(conn);
			return obj;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Gets the contestquestions bycontest id.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 * @return the contestquestions bycontest id
	 */
	public static List<ContestQuestionDVO> getcontestquestionsBycontestId(String clientId, String contestId) {

		String saasAdminLinkedServer = DatabaseConnections.getSaasAdminLinkedServer(clientId) + ".dbo.";
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT * FROM ").append(saasAdminLinkedServer).append("pa_ofltx_contest_question where  clintid='")
				.append(clientId).append("' and conid = '").append(contestId).append("' order by qsnseqno");
		List<ContestQuestionDVO> list = new ArrayList<ContestQuestionDVO>();
		try {
			Connection conn = DatabaseConnections.getSaasAdminConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql.toString());

			ContestQuestionDVO obj = null;
			while (rs.next()) {
				obj = new ContestQuestionDVO();
				obj.setClintid(rs.getString("clintid"));
				obj.setConqsnid(rs.getInt("conqsnid"));
				obj.setConid(rs.getString("conid"));
				obj.setQsnseqno(rs.getInt("qsnseqno"));
				obj.setIsactive(rs.getBoolean("isactive"));
				obj.setOpa(rs.getString("ansopta"));
				obj.setOpb(rs.getString("ansoptb"));
				obj.setOpc(rs.getString("ansoptc"));
				obj.setOpd(rs.getString("ansoptd"));
				obj.setCorans(rs.getString("corans"));
				obj.setCreby(rs.getString("creby"));
				obj.setCredate(new Date(rs.getTimestamp("credate").getTime()));
				obj.setQbid(rs.getInt("qsnbnkid"));
				obj.setQsnorient(rs.getString("qsnorient"));
				obj.setQsntext(rs.getString("qsntext"));
				obj.setRwdtype(rs.getString("rwdtype"));
				obj.setRwdvalue(rs.getDouble("rwdval"));
				obj.setUpdby(rs.getString("updby"));
				if (rs.getTimestamp("upddate") != null) {
					obj.setUpddate(new Date(rs.getTimestamp("upddate").getTime()));
				}
				list.add(obj);

			}
			rs.close();
			stmt.close();
			DatabaseConnections.closeConnection(conn);
			return list;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Gets the contestquestions by contest id and filter.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 * @param filterQuery
	 *            the filter query
	 * @param pgNo
	 *            the pg no
	 * @return the contestquestions by contest id and filter
	 */
	public static List<ContestQuestionDVO> getcontestquestionsBycontestIdAndFilter(String clientId, String contestId,
			String filterQuery, String pgNo,String filter) {

		String saasAdminLinkedServer = DatabaseConnections.getSaasAdminLinkedServer(clientId) + ".dbo.";
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT * FROM ").append(saasAdminLinkedServer).append("pa_ofltx_contest_question where  clintid='")
				.append(clientId).append("' " + " and conid = '").append(contestId).append("' ");

		String paginationQuery = PaginationUtil.getPaginationQuery(pgNo);
		if (filterQuery != null && !filterQuery.isEmpty()) {
			sql.append(filterQuery);
		}
		String sortingSql = GridSortingUtil.getOTTContestQuestionsSortingQuery(filter);
		if(sortingSql != null && !sortingSql.trim().isEmpty()){
			sql.append(sortingSql);
		}else{
			sql.append(" order by conqsnid desc ");
		}
		
		sql.append(paginationQuery);

		List<ContestQuestionDVO> list = new ArrayList<ContestQuestionDVO>();
		try {
			Connection conn = DatabaseConnections.getSaasAdminConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql.toString());

			ContestQuestionDVO obj = null;
			while (rs.next()) {
				obj = new ContestQuestionDVO();
				obj.setClintid(rs.getString("clintid"));
				obj.setConqsnid(rs.getInt("conqsnid"));
				obj.setConid(rs.getString("conid"));
				obj.setQsnseqno(rs.getInt("qsnseqno"));
				obj.setIsactive(rs.getBoolean("isactive"));
				obj.setOpa(rs.getString("ansopta"));
				obj.setOpb(rs.getString("ansoptb"));
				obj.setOpc(rs.getString("ansoptc"));
				obj.setOpd(rs.getString("ansoptd"));
				obj.setCorans(rs.getString("corans"));
				obj.setCreby(rs.getString("creby"));
				obj.setCredate(new Date(rs.getTimestamp("credate").getTime()));
				obj.setQbid(rs.getInt("qsnbnkid"));
				obj.setQsnorient(rs.getString("qsnorient"));
				obj.setQsntext(rs.getString("qsntext"));
				obj.setRwdtype(rs.getString("rwdtype"));
				obj.setRwdvalue(rs.getDouble("rwdval"));
				obj.setUpdby(rs.getString("updby"));
				if (rs.getTimestamp("upddate") != null) {
					obj.setUpddate(new Date(rs.getTimestamp("upddate").getTime()));
				}
				list.add(obj);

			}
			rs.close();
			stmt.close();
			DatabaseConnections.closeConnection(conn);
			return list;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Gets the all contest questions data to export.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 * @param filterQuery
	 *            the filter query
	 * @return the all contest questions data to export
	 */
	public static List<ContestQuestionDVO> getAllContestQuestionsDataToExport(String clientId, String contestId,
			String filterQuery) {

		String saasAdminLinkedServer = DatabaseConnections.getSaasAdminLinkedServer(clientId) + ".dbo.";
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT * FROM ").append(saasAdminLinkedServer).append("pa_ofltx_contest_question where  clintid='")
				.append(clientId).append("'  and conid = '").append(contestId).append("' ");

		if (filterQuery != null && !filterQuery.isEmpty()) {
			sql.append(filterQuery);
		}
		sql.append("  order by conqsnid desc ");

		List<ContestQuestionDVO> list = new ArrayList<ContestQuestionDVO>();
		try {
			Connection conn = DatabaseConnections.getSaasAdminConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql.toString());

			ContestQuestionDVO obj = null;
			while (rs.next()) {
				obj = new ContestQuestionDVO();
				obj.setClintid(rs.getString("clintid"));
				obj.setConqsnid(rs.getInt("conqsnid"));
				obj.setConid(rs.getString("conid"));
				obj.setQsnseqno(rs.getInt("qsnseqno"));
				obj.setIsactive(rs.getBoolean("isactive"));
				obj.setOpa(rs.getString("ansopta"));
				obj.setOpb(rs.getString("ansoptb"));
				obj.setOpc(rs.getString("ansoptc"));
				obj.setOpd(rs.getString("ansoptd"));
				obj.setCorans(rs.getString("corans"));
				obj.setCreby(rs.getString("creby"));
				obj.setCredate(new Date(rs.getTimestamp("credate").getTime()));
				obj.setQbid(rs.getInt("qsnbnkid"));
				obj.setQsnorient(rs.getString("qsnorient"));
				obj.setQsntext(rs.getString("qsntext"));
				obj.setRwdtype(rs.getString("rwdtype"));
				obj.setRwdvalue(rs.getDouble("rwdval"));
				obj.setUpdby(rs.getString("updby"));
				if (rs.getTimestamp("upddate") != null) {
					obj.setUpddate(new Date(rs.getTimestamp("upddate").getTime()));
				}
				list.add(obj);

			}
			rs.close();
			stmt.close();
			DatabaseConnections.closeConnection(conn);
			return list;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Gets the all contest questions count by contest idand filter.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 * @param filterQuery
	 *            the filter query
	 * @return the all contest questions count by contest idand filter
	 * @throws Exception
	 *             the exception
	 */
	public static Integer getAllContestQuestionsCountByContestIdandFilter(String clientId, String contestId,
			String filterQuery) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Integer count = 0;
		try {

			String saasAdminLinkedServer = DatabaseConnections.getSaasAdminLinkedServer(clientId) + ".dbo.";
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT count(*) FROM ").append(saasAdminLinkedServer)
					.append("pa_ofltx_contest_question where  clintid=? ").append(" and conid = ? ");

			if (filterQuery != null && !filterQuery.isEmpty()) {
				sql.append(filterQuery);
			}
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			ps.setString(2, contestId);

			rs = ps.executeQuery();
			while (rs.next()) {
				count = rs.getInt(1);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return count;
	}

	/**
	 * Save contest questions.
	 *
	 * @param clientId
	 *            the client id
	 * @param obj
	 *            the obj
	 * @return the integer
	 */
	public static Integer saveContestQuestions(String clientId, ContestQuestionDVO obj) {
		Integer id = null;
		String saasAdminLinkedServer = DatabaseConnections.getSaasAdminLinkedServer(clientId) + ".dbo.";
		StringBuffer sql = new StringBuffer();
		sql.append("insert into ").append(saasAdminLinkedServer)
				.append("pa_ofltx_contest_question(clintid, conid, qsnseqno, isactive, ansopta, ansoptb, ansoptc, ansoptd, ")
				.append(" corans, creby, credate, qsnbnkid,  qsnorient, qsntext, rwdtype, rwdval, updby, upddate) ")
				.append("values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,getDate())");

		try {
			Connection conn = DatabaseConnections.getSaasAdminConnection();
			PreparedStatement statement = conn.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, obj.getClintid());
			statement.setString(2, obj.getConid());
			statement.setInt(3, obj.getQsnseqno());
			statement.setBoolean(4, obj.getIsactive());
			statement.setString(5, obj.getOpa());
			statement.setString(6, obj.getOpb());
			statement.setString(7, obj.getOpc());
			statement.setString(8, obj.getOpd());
			statement.setString(9, obj.getCorans());
			statement.setString(10, obj.getCreby());

			statement.setTimestamp(11, new Timestamp(obj.getCredate().getTime()));
			if (obj.getQbid() != null) {
				statement.setInt(12, obj.getQbid());
			} else {
				statement.setString(12, null);
			}
			statement.setString(13, obj.getQsnorient());
			statement.setString(14, obj.getQsntext());
			statement.setString(15, obj.getRwdtype());
			statement.setDouble(16, obj.getRwdvalue());
			statement.setString(17, obj.getCreby());

			int affectedRows = statement.executeUpdate();
			if (affectedRows == 0) {
				throw new SQLException("Creating contest question record failed, no rows affected.");
			}
			ResultSet generatedKeys;
			try {
				generatedKeys = statement.getGeneratedKeys();
				if (generatedKeys.next()) {
					id = generatedKeys.getInt(1);

				}
				generatedKeys.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			statement.close();
			DatabaseConnections.closeConnection(conn);
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return id;

	}

	/**
	 * Update contest questions.
	 *
	 * @param clientId
	 *            the client id
	 * @param obj
	 *            the obj
	 * @return the integer
	 */
	public static Integer updateContestQuestions(String clientId, ContestQuestionDVO obj) {
		Integer affectedRows = null;
		String saasAdminLinkedServer = DatabaseConnections.getSaasAdminLinkedServer(clientId) + ".dbo.";
		StringBuffer sql = new StringBuffer();
		sql.append("update ").append(saasAdminLinkedServer)
				.append("pa_ofltx_contest_question set qsntext =?, ansopta=?, ansoptb=?, ansoptc=?, ansoptd=?, ")
				.append(" corans=?, isactive=?, qsnbnkid=?, qsnorient=?, updby=?, upddate=? where  ")
				.append(" clintid=? and conqsnid = ?");

		try {
			Connection conn = DatabaseConnections.getSaasAdminConnection();
			PreparedStatement statement = conn.prepareStatement(sql.toString());
			statement.setString(1, obj.getQsntext());
			statement.setString(2, obj.getOpa());
			statement.setString(3, obj.getOpb());
			statement.setString(4, obj.getOpc());
			statement.setString(5, obj.getOpd());
			statement.setString(6, obj.getCorans());
			statement.setBoolean(7, obj.getIsactive());
			statement.setInt(8, obj.getQbid());
			statement.setString(9, obj.getQsnorient());
			statement.setString(10, obj.getUpdby());
			statement.setTimestamp(11, new Timestamp(obj.getUpddate().getTime()));
			statement.setString(12, obj.getClintid());
			statement.setInt(13, obj.getConqsnid());

			affectedRows = statement.executeUpdate();
			if (affectedRows == 0) {
				throw new SQLException("updating contest question record failed, no rows affected.");
			}
			statement.close();
			DatabaseConnections.closeConnection(conn);
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return affectedRows;

	}

	/**
	 * Update contest questions seqno.
	 *
	 * @param clientId
	 *            the client id
	 * @param list
	 *            the list
	 * @return the integer
	 */
	public static Integer updateContestQuestionsSeqno(String clientId, List<ContestQuestionDVO> list) {
		Integer affectedRows = null;
		String saasAdminLinkedServer = DatabaseConnections.getSaasAdminLinkedServer(clientId) + ".dbo.";
		StringBuffer sql = new StringBuffer();
		sql.append("update ").append(saasAdminLinkedServer)
				.append("pa_ofltx_contest_question set qsnseqno =?,  updby=?, upddate=? where  ")
				.append(" clintid=? and conqsnid = ?");

		try {
			Connection conn = DatabaseConnections.getSaasAdminConnection();
			PreparedStatement statement = conn.prepareStatement(sql.toString());

			for (ContestQuestionDVO obj : list) {
				statement.setInt(1, obj.getQsnseqno());
				statement.setString(2, obj.getUpdby());
				statement.setTimestamp(3, new Timestamp(obj.getUpddate().getTime()));
				statement.setString(4, obj.getClintid());
				statement.setInt(5, obj.getConqsnid());

				statement.addBatch();
			}
			int[] rws = statement.executeBatch();
			if (rws == null || rws.length == 0) {
				throw new SQLException("updating contest question seqno record failed, no rows affected.");
			}
			affectedRows = rws.length;

			statement.close();
			DatabaseConnections.closeConnection(conn);
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return affectedRows;

	}

	/**
	 * Delete contest questions.
	 *
	 * @param clientId
	 *            the client id
	 * @param obj
	 *            the obj
	 * @return the integer
	 */
	public static Integer deleteContestQuestions(String clientId, ContestQuestionDVO obj) {
		Integer affectedRows = null;
		String saasAdminLinkedServer = DatabaseConnections.getSaasAdminLinkedServer(clientId) + ".dbo.";
		StringBuffer sql = new StringBuffer();
		sql.append("delete from ").append(saasAdminLinkedServer).append("pa_ofltx_contest_question where clintid=? and conqsnid = ?");

		try {
			Connection conn = DatabaseConnections.getSaasAdminConnection();
			PreparedStatement statement = conn.prepareStatement(sql.toString());
			statement.setString(1, obj.getClintid());
			statement.setInt(2, obj.getConqsnid());

			affectedRows = statement.executeUpdate();
			if (affectedRows == 0) {
				throw new SQLException("delete contest question record failed, no rows affected.");
			}
			statement.close();
			DatabaseConnections.closeConnection(conn);
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return affectedRows;

	}

	/**
	 * Gets the contest questions count.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 * @return the contest questions count
	 */
	public static Integer getContestQuestionsCount(String clientId, String contestId) {

		String saasAdminLinkedServer = DatabaseConnections.getSaasAdminLinkedServer(clientId) + ".dbo.";
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT  count(*) as qCount FROM ").append(saasAdminLinkedServer).append("pa_ofltx_contest_question where  clintid='")
		.append(clientId).append("' and conid = '").append(contestId + "'");

		Integer qsize = null;
		try {

			Connection conn = DatabaseConnections.getSaasAdminConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql.toString());
			while (rs.next()) {
				qsize = rs.getInt("qCount");
			}
			rs.close();
			stmt.close();
			DatabaseConnections.closeConnection(conn);
			return qsize;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return qsize;
	}

}
