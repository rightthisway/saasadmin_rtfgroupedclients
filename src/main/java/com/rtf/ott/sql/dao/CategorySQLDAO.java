/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.sql.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.rtf.common.util.GridSortingUtil;
import com.rtf.ott.sql.dvo.CategoryDVO;
import com.rtf.ott.util.StatusEnums;
import com.rtf.saas.sql.db.DatabaseConnections;
import com.rtf.saas.util.PaginationUtil;

/**
 * The Class CategorySQLDAO.
 */
public class CategorySQLDAO {

	/**
	 * Gets the all active categories.
	 *
	 * @param clientId
	 *            the client id
	 * @return the all active categories
	 * @throws Exception
	 *             the exception
	 */
	/*
	 * @param Clientid
	 * 
	 * @return
	 * 
	 * @throws Exception
	 */
	public static List<CategoryDVO> getAllActiveCategories(String clientId) throws Exception {

		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		List<CategoryDVO> catList = null;
		StringBuffer sql = new StringBuffer(" SELECT * from  sd_category_type  with(nolock) where clintid = ? and isactive = ? ");

		try {
			conn = DatabaseConnections.getSaasAdminConnection();

			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			ps.setString(2, StatusEnums.TRUE.name());
			rs = ps.executeQuery();
			catList = new ArrayList<CategoryDVO>();
			while (rs.next()) {
				CategoryDVO vo = new CategoryDVO();
				vo.setCattype(rs.getString("cattype"));
				vo.setCatdesc(rs.getString("catdesc"));
				vo.setIsactive(rs.getBoolean("isactive"));
				vo.setCreby(rs.getString("creby"));
				vo.setUpdby(rs.getString("updby"));
				vo.setUpddate(rs.getDate("upddate"));
				vo.setCredate(rs.getDate("credate"));
				catList.add(vo);
			}
			return catList;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;

	}

	/**
	 * Gets the all active categorieswith filter.
	 *
	 * @param clientId
	 *            the client id
	 * @param filterQuery
	 *            the filter query
	 * @param pgNo
	 *            the pg no
	 * @return the all active categorieswith filter
	 * @throws Exception
	 *             the exception
	 */
	public static List<CategoryDVO> getAllActiveCategorieswithFilter(String clientId, String filterQuery, String pgNo,String filter)
			throws Exception {

		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		List<CategoryDVO> catList = null;
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT * from  sd_category_type  with(nolock) where clintid = ? and isactive = ? ");

		String paginationQuery = PaginationUtil.getPaginationQuery(pgNo);
		if (filterQuery != null && !filterQuery.isEmpty()) {
			sql.append(filterQuery);
		}
		String sortingSql = GridSortingUtil.getCategoriesSortingQuery(filter);
		if(sortingSql!=null && !sortingSql.trim().isEmpty()){
			sql.append(sortingSql);
		}else{
			sql.append("  order by cattype ");
		}
		sql.append(paginationQuery);

		try {
			conn = DatabaseConnections.getSaasAdminConnection();

			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			ps.setString(2, StatusEnums.TRUE.name());
			rs = ps.executeQuery();
			catList = new ArrayList<CategoryDVO>();
			while (rs.next()) {
				CategoryDVO vo = new CategoryDVO();
				Timestamp timestamp = null;
				vo.setCattype(rs.getString("cattype"));
				vo.setCatdesc(rs.getString("catdesc"));
				vo.setIsactive(rs.getBoolean("isactive"));
				vo.setCreby(rs.getString("creby"));
				vo.setUpdby(rs.getString("updby"));

				timestamp = rs.getTimestamp("credate");
				vo.setCredate(new java.util.Date(timestamp != null ? timestamp.getTime() : -10));
				timestamp = rs.getTimestamp("upddate");
				vo.setUpddate(new java.util.Date(timestamp != null ? timestamp.getTime() : -10));
				catList.add(vo);
			}
			return catList;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * Gets the all categories data to export.
	 *
	 * @param clientId
	 *            the client id
	 * @param filterQuery
	 *            the filter query
	 * @return the all categories data to export
	 * @throws Exception
	 *             the exception
	 */
	public static List<CategoryDVO> getAllCategoriesDataToExport(String clientId, String filterQuery) throws Exception {

		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		List<CategoryDVO> catList = null;
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT * from  sd_category_type  with(nolock) where clintid = ? and isactive = ? ");

		if (filterQuery != null && !filterQuery.isEmpty()) {
			sql.append(filterQuery);
		}
		sql.append("  order by cattype ");

		try {
			conn = DatabaseConnections.getSaasAdminConnection();

			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			ps.setString(2, StatusEnums.TRUE.name());
			rs = ps.executeQuery();
			catList = new ArrayList<CategoryDVO>();
			while (rs.next()) {
				CategoryDVO vo = new CategoryDVO();
				Timestamp timestamp = null;
				vo.setCattype(rs.getString("cattype"));
				vo.setCatdesc(rs.getString("catdesc"));
				vo.setIsactive(rs.getBoolean("isactive"));
				vo.setCreby(rs.getString("creby"));
				vo.setUpdby(rs.getString("updby"));
				timestamp = rs.getTimestamp("credate");
				vo.setCredate(new java.util.Date(timestamp != null ? timestamp.getTime() : -10));
				timestamp = rs.getTimestamp("upddate");
				vo.setUpddate(new java.util.Date(timestamp != null ? timestamp.getTime() : -10));
				catList.add(vo);
			}
			return catList;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;

	}

	/**
	 * Gets the all active categories countwith filter.
	 *
	 * @param clientId
	 *            the client id
	 * @param filterQuery
	 *            the filter query
	 * @return the all active categories countwith filter
	 * @throws Exception
	 *             the exception
	 */
	public static Integer getAllActiveCategoriesCountwithFilter(String clientId, String filterQuery) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Integer count = 0;
		try {
			StringBuffer sql = new StringBuffer();
			sql.append(" SELECT count(*) from  sd_category_type  with(nolock) where clintid = ? and isactive = ? ");

			if (filterQuery != null && !filterQuery.isEmpty()) {
				sql.append(filterQuery);
			}
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			ps.setString(2, StatusEnums.TRUE.name());

			rs = ps.executeQuery();
			while (rs.next()) {
				count = rs.getInt(1);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return count;
	}

	/**
	 * Creates the category.
	 *
	 * @param userId
	 *            the user id
	 * @param clientid
	 *            the clientid
	 * @param dvo
	 *            the dvo
	 * @return the integer
	 * @throws Exception
	 *             the exception
	 */
	public static Integer createCategory(String userId, String clientid, CategoryDVO dvo) throws Exception {

		Connection conn = null;
		PreparedStatement ps = null;
		Integer affectedRows = 0;
		StringBuffer sql = new StringBuffer();
		sql.append(" insert into  sd_category_type ( 		 clintid" + "           ,cattype")
				.append("           ,catdesc         ,creby           ,isactive")
				.append("			,credate,updby,upddate ) ").append("values(?,?,?,?,?,getdate(),?,getDate()) ");

		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientid);
			ps.setString(2, dvo.getCattype());
			ps.setString(3, dvo.getCatdesc());
			ps.setString(4, userId);
			ps.setBoolean(5, Boolean.TRUE);
			ps.setString(6, userId);
			affectedRows = ps.executeUpdate();
			return affectedRows;

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return affectedRows;
	}

	/**
	 * Delete category.
	 *
	 * @param userId
	 *            the user id
	 * @param clientid
	 *            the clientid
	 * @param dvo
	 *            the dvo
	 * @return the integer
	 * @throws Exception
	 *             the exception
	 */
	public static Integer deleteCategory(String userId, String clientid, CategoryDVO dvo) throws Exception {

		Connection conn = null;
		Integer affectedRows = 0;
		PreparedStatement ps = null;
		StringBuffer sql = new StringBuffer(" update  sd_category_type set isactive = ? , updby = ? ,upddate=getDate()  where clintid =  ? and cattype = ? ");
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setBoolean(1, Boolean.FALSE);
			ps.setString(2, userId);
			ps.setString(3, clientid);
			ps.setString(4, dvo.getCattype());
			affectedRows = ps.executeUpdate();
			return affectedRows;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return affectedRows;
	}

}
