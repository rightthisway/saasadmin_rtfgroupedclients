/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.sql.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.rtf.common.util.GridSortingUtil;
import com.rtf.ott.sql.dvo.RewardTypeDVO;
import com.rtf.ott.util.StatusEnums;
import com.rtf.saas.sql.db.DatabaseConnections;
import com.rtf.saas.util.PaginationUtil;

/**
 * The Class SDRewardTypeSQLDAO.
 */
public class SDRewardTypeSQLDAO {

	/*
	 * @param Clientid
	 * 
	 * @return
	 * 
	 * @throws Exception
	 */

	/**
	 * Gets the all reward types with filter.
	 *
	 * @param clientId
	 *            the client id
	 * @param pgNo
	 *            the pg no
	 * @param filterQuery
	 *            the filter query
	 * @return the all reward types with filter
	 * @throws Exception
	 *             the exception
	 */
	public static List<RewardTypeDVO> getAllRewardTypesWithFilter(String clientId, String pgNo, String filterQuery,String filter)
			throws Exception {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		List<RewardTypeDVO> rwdList = null;
		StringBuffer sql = new StringBuffer(
				" SELECT * from sd_reward_type  with(nolock) where clintid = ? and isactive = ?   ");
		String paginationQuery = PaginationUtil.getPaginationQuery(pgNo);
		if (filterQuery != null && !filterQuery.isEmpty()) {
			sql.append(filterQuery);
		}
		
		String sortingSql = GridSortingUtil.getRewardTypesSortingQuery(filter);
		if(sortingSql!=null && !sortingSql.trim().isEmpty()){
			sql.append(sortingSql);
		}else{
			sql.append("  order by rwdtype ");
		}
		
		
		sql.append(paginationQuery);
		try {
			conn = DatabaseConnections.getSaasAdminConnection();

			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			ps.setBoolean(2, Boolean.TRUE);
			rs = ps.executeQuery();

			rwdList = new ArrayList<RewardTypeDVO>();
			while (rs.next()) {
				Timestamp timestamp = null;
				RewardTypeDVO vo = new RewardTypeDVO();
				vo.setRwdtype(rs.getString("rwdtype"));
				vo.setIsactive(rs.getBoolean("isactive"));
				vo.setRwddesc(rs.getString("rwddesc"));
				vo.setRwdunimesr(rs.getString("rwdunimesr"));
				vo.setRwdimgurl(rs.getString("rwdimgurl"));
				vo.setCreby(rs.getString("creby"));
				vo.setUpdby(rs.getString("updby"));

				timestamp = rs.getTimestamp("credate");
				vo.setCredate(new java.util.Date(timestamp != null ? timestamp.getTime() : -10));
				timestamp = rs.getTimestamp("upddate");
				vo.setUpddate(new java.util.Date(timestamp != null ? timestamp.getTime() : -10));
				rwdList.add(vo);
			}

			return rwdList;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * Gets the reward type data to export.
	 *
	 * @param clientId
	 *            the client id
	 * @param filterQuery
	 *            the filter query
	 * @return the reward type data to export
	 * @throws Exception
	 *             the exception
	 */
	public static List<RewardTypeDVO> getRewardTypeDataToExport(String clientId, String filterQuery) throws Exception {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		List<RewardTypeDVO> rwdList = null;

		StringBuffer sql = new StringBuffer(
				"SELECT * from sd_reward_type  with(nolock) where clintid = ? and isactive = ?   ");
		if (filterQuery != null && !filterQuery.isEmpty()) {
			sql.append(filterQuery);
		}
		sql.append("  order by rwdtype ");
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			ps.setBoolean(2, Boolean.TRUE);
			rs = ps.executeQuery();

			rwdList = new ArrayList<RewardTypeDVO>();
			while (rs.next()) {
				Timestamp timestamp = null;
				RewardTypeDVO vo = new RewardTypeDVO();
				vo.setClId(rs.getString("clintid"));
				vo.setRwdtype(rs.getString("rwdtype"));
				vo.setIsactive(rs.getBoolean("isactive"));
				vo.setRwddesc(rs.getString("rwddesc"));
				vo.setRwdunimesr(rs.getString("rwdunimesr"));
				vo.setRwdimgurl(rs.getString("rwdimgurl"));
				vo.setCreby(rs.getString("creby"));
				vo.setUpdby(rs.getString("updby"));
				timestamp = rs.getTimestamp("credate");
				vo.setCredate(new java.util.Date(timestamp != null ? timestamp.getTime() : -10));
				timestamp = rs.getTimestamp("upddate");
				vo.setUpddate(new java.util.Date(timestamp != null ? timestamp.getTime() : -10));
				rwdList.add(vo);
			}

			return rwdList;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * Gets the all reward types countwith filter.
	 *
	 * @param clientId
	 *            the client id
	 * @param filterQuery
	 *            the filter query
	 * @return the all reward types countwith filter
	 * @throws Exception
	 *             the exception
	 */
	public static Integer getAllRewardTypesCountwithFilter(String clientId, String filterQuery) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Integer count = 0;
		try {

			StringBuffer sql = new StringBuffer(" SELECT count(*) from sd_reward_type  with(nolock) where clintid = ? and isactive = ?   ");

			if (filterQuery != null && !filterQuery.isEmpty()) {
				sql.append(filterQuery);
			}
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			ps.setString(2, StatusEnums.TRUE.name());

			rs = ps.executeQuery();
			while (rs.next()) {
				count = rs.getInt(1);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return count;
	}

	/**
	 * Delete reward type.
	 *
	 * @param clientid
	 *            the clientid
	 * @param rwdtype
	 *            the rwdtype
	 * @param userId
	 *            the user id
	 * @return the integer
	 * @throws Exception
	 *             the exception
	 */
	public static Integer deleteRewardType(String clientid, String rwdtype, String userId) throws Exception {

		Connection conn = null;
		PreparedStatement ps = null;
		Integer affectedRows = 0;
		StringBuffer sql = new StringBuffer(" update  sd_reward_type	set    isactive = ?   ,updby =  ? 	,upddate  = getDate()  where clintid =  ? and rwdtype = ? ");

		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setBoolean(1, Boolean.FALSE);
			ps.setString(2, userId);
			ps.setString(3, clientid);
			ps.setString(4, rwdtype);
			affectedRows = ps.executeUpdate();
			return affectedRows;

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return affectedRows;
	}

	/**
	 * Creates the reward type.
	 *
	 * @param clientid
	 *            the clientid
	 * @param dvo
	 *            the dvo
	 * @param userId
	 *            the user id
	 * @return the integer
	 * @throws Exception
	 *             the exception
	 */
	public static Integer createRewardType(String clientid, RewardTypeDVO dvo, String userId) throws Exception {
		int affectedRows = 0;
		Connection conn = null;
		PreparedStatement ps = null;
		StringBuffer sql = new StringBuffer();
		sql.append(" insert into  sd_reward_type  (  clintid   ,rwdtype  ,isactive  ")
				.append("    ,rwddesc ,  rwdunimesr  , rwdimgurl              ,creby")
				.append("			,credate,updby,upddate ) ").append("values(?,?,?,?,?,?,?,getdate(),?,getDate()) ");

		try {
			conn = DatabaseConnections.getSaasAdminConnection();

			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientid);
			ps.setString(2, dvo.getRwdtype());
			ps.setBoolean(3, Boolean.TRUE);
			ps.setString(4, dvo.getRwddesc());
			ps.setString(5, dvo.getRwdunimesr());
			ps.setString(6, dvo.getRwdimgurl());
			ps.setString(7, userId);
			ps.setString(8, userId);
			affectedRows = ps.executeUpdate();

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return affectedRows;
	}

	/**
	 * Update reward type.
	 *
	 * @param clientid
	 *            the clientid
	 * @param dvo
	 *            the dvo
	 * @param userId
	 *            the user id
	 * @return the integer
	 */
	public static Integer updateRewardType(String clientid, RewardTypeDVO dvo, String userId) {
		Connection conn = null;
		PreparedStatement ps = null;
		Integer affectedRows = 0;
		StringBuffer sql = new StringBuffer(" update  sd_reward_type	set rwddesc = ? ,  rwdunimesr = ?  , rwdimgurl = ?   ,updby =  ? 	,upddate  = getDate()  where clintid =  ? and rwdtype = ? ");
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, dvo.getRwddesc());
			ps.setString(2, dvo.getRwdunimesr());
			ps.setString(3, dvo.getRwdimgurl());
			ps.setString(4, userId);
			ps.setString(5, clientid);
			ps.setString(6, dvo.getRwdtype());
			affectedRows = ps.executeUpdate();
			return affectedRows;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return affectedRows;
	}

	/**
	 * Update reward typewith no image.
	 *
	 * @param clientid
	 *            the clientid
	 * @param dvo
	 *            the dvo
	 * @param userId
	 *            the user id
	 * @return the integer
	 */
	public static Integer updateRewardTypewithNoImage(String clientid, RewardTypeDVO dvo, String userId) {
		Connection conn = null;
		PreparedStatement ps = null;
		Integer affectedRows = 0;
		StringBuffer sql = new StringBuffer(" update  sd_reward_type	set rwddesc = ? ,  rwdunimesr = ?   ,updby =  ? 	,upddate  = getDate()  where clintid =  ? and rwdtype = ? ");
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, dvo.getRwddesc());
			ps.setString(2, dvo.getRwdunimesr());
			ps.setString(3, userId);
			ps.setString(4, clientid);
			ps.setString(5, dvo.getRwdtype());
			affectedRows = ps.executeUpdate();
			return affectedRows;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return affectedRows;
	}
}
