/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.service;

import java.util.List;

import com.rtf.ott.dto.OTTSubCategoryDTO;
import com.rtf.ott.sql.dao.SubCategorySQLDAO;
import com.rtf.ott.sql.dvo.SubCategoryDVO;
import com.rtf.ott.util.GridHeaderFilterUtil;
import com.rtf.saas.util.PaginationUtil;

/**
 * The Class OTTSubCategoryService.
 */
public class OTTSubCategoryService {

	/**
	 * Fetch OTT sub category list for cat.
	 *
	 * @param clientId
	 *            the client id
	 * @param dto
	 *            the dto
	 * @param category
	 *            the category
	 * @return the OTT sub category DTO
	 */
	public static OTTSubCategoryDTO fetchOTTSubCategoryListForCat(String clientId, OTTSubCategoryDTO dto,
			String category) {
		List<SubCategoryDVO> sCatList = null;
		try {
			sCatList = SubCategorySQLDAO.getAllSubCategoriesForCat(clientId, category);
			dto.setsCatlist(sCatList);
			dto.setSts(1);
		} catch (Exception ex) {
			dto.setSts(0);
			ex.printStackTrace();
		}
		return dto;
	}

	/**
	 * Fetch OTT sub category list withfilter.
	 *
	 * @param clientId
	 *            the client id
	 * @param dto
	 *            the dto
	 * @param filter
	 *            the filter
	 * @param pgNo
	 *            the pg no
	 * @return the OTT sub category DTO
	 */
	public static OTTSubCategoryDTO fetchOTTSubCategoryListWithfilter(String clientId, OTTSubCategoryDTO dto,
			String filter, String pgNo) {
		List<SubCategoryDVO> sCatList = null;
		try {
			String filterQuery = GridHeaderFilterUtil.getAllSubCategoriesFilterQuery(filter);

			sCatList = SubCategorySQLDAO.getAllSubCategorieswithFilter(clientId, filterQuery, pgNo,filter);
			Integer count = SubCategorySQLDAO.getAllActiveSubCategoriesCountwithFilter(clientId, filterQuery);
			dto.setPagination(PaginationUtil.calculatePaginationParameter(count, pgNo));
			dto.setsCatlist(sCatList);
			dto.setSts(1);
		} catch (Exception ex) {
			dto.setSts(0);
			ex.printStackTrace();
		}
		return dto;
	}

	/**
	 * Gets the all sub categories data to export.
	 *
	 * @param clientId
	 *            the client id
	 * @param filter
	 *            the filter
	 * @return the all sub categories data to export
	 */
	public static List<SubCategoryDVO> getAllSubCategoriesDataToExport(String clientId, String filter) {
		List<SubCategoryDVO> sCatList = null;
		try {
			String filterQuery = GridHeaderFilterUtil.getAllSubCategoriesFilterQuery(filter);
			sCatList = SubCategorySQLDAO.getAllSubCategoriesDataToExport(clientId, filterQuery);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return sCatList;
	}

	/**
	 * Creates the sub category.
	 *
	 * @param userId
	 *            the user id
	 * @param clientid
	 *            the clientid
	 * @param dto
	 *            the dto
	 * @return the OTT sub category DTO
	 * @throws Exception
	 *             the exception
	 */
	public static OTTSubCategoryDTO createSubCategory(String userId, String clientid, OTTSubCategoryDTO dto)
			throws Exception {
		try {
			dto.setSts(0);
			SubCategoryDVO dvo = dto.getDvo();
			Integer updCnt = SubCategorySQLDAO.createSubCategory(userId, clientid, dvo);
			dto.setSts(updCnt);
		} catch (Exception ex) {
			dto.setSts(0);
			ex.printStackTrace();
		}
		return dto;
	}

	/**
	 * Delete sub category.
	 *
	 * @param userId
	 *            the user id
	 * @param clientid
	 *            the clientid
	 * @param dto
	 *            the dto
	 * @return the OTT sub category DTO
	 * @throws Exception
	 *             the exception
	 */
	public static OTTSubCategoryDTO deleteSubCategory(String userId, String clientid, OTTSubCategoryDTO dto)
			throws Exception {
		try {
			dto.setSts(0);
			SubCategoryDVO dvo = dto.getDvo();
			Integer updCnt = SubCategorySQLDAO.deleteSubCategory(userId, clientid, dvo);
			dto.setSts(updCnt);
		} catch (Exception ex) {
			dto.setSts(0);
			ex.printStackTrace();
		}
		return dto;
	}

}
