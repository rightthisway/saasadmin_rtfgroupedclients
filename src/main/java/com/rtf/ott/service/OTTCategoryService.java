/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.service;

import java.util.List;

import com.rtf.ott.dto.OTTCategoryDTO;
import com.rtf.ott.sql.dao.CategorySQLDAO;
import com.rtf.ott.sql.dao.SubCategorySQLDAO;
import com.rtf.ott.sql.dvo.CategoryDVO;
import com.rtf.ott.sql.dvo.SubCategoryDVO;
import com.rtf.ott.util.GridHeaderFilterUtil;
import com.rtf.saas.util.Constant;
import com.rtf.saas.util.PaginationUtil;

/**
 * The Class OTTCategoryService.
 */
public class OTTCategoryService {

	/**
	 * Fetch OTT categor list.
	 *
	 * @param clientId
	 *            the client id
	 * @param dto
	 *            the dto
	 * @param pNo
	 *            the no
	 * @return the OTT category DTO
	 */
	public static OTTCategoryDTO fetchOTTCategorList(String clientId, OTTCategoryDTO dto, String pNo) {
		List<CategoryDVO> catList = null;
		try {
			catList = CategorySQLDAO.getAllActiveCategories(clientId);
			dto.setCatlist(catList);
			dto.setSts(1);
		} catch (Exception ex) {
			dto.setSts(0);
			ex.printStackTrace();
		}
		return dto;
	}

	/**
	 * Fetch OTT categor list with filter.
	 *
	 * @param clientId
	 *            the client id
	 * @param dto
	 *            the dto
	 * @param pgNo
	 *            the pg no
	 * @param filter
	 *            the filter
	 * @return the OTT category DTO
	 */
	public static OTTCategoryDTO fetchOTTCategorListWithFilter(String clientId, OTTCategoryDTO dto, String pgNo,
			String filter) {
		List<CategoryDVO> catList = null;
		try {

			String filterQuery = GridHeaderFilterUtil.getAllCategoriesFilterQuery(filter);

			catList = CategorySQLDAO.getAllActiveCategorieswithFilter(clientId, filterQuery, pgNo,filter);
			Integer count = CategorySQLDAO.getAllActiveCategoriesCountwithFilter(clientId, filterQuery);
			dto.setPagination(PaginationUtil.calculatePaginationParameter(count, pgNo));
			dto.setCatlist(catList);
			dto.setSts(1);

		} catch (Exception ex) {
			dto.setSts(0);
			ex.printStackTrace();
		}
		return dto;
	}

	/**
	 * Gets the all categories data to export.
	 *
	 * @param clientId
	 *            the client id
	 * @param filter
	 *            the filter
	 * @return the all categories data to export
	 */
	public static List<CategoryDVO> getAllCategoriesDataToExport(String clientId, String filter) {
		List<CategoryDVO> catList = null;
		try {
			String filterQuery = GridHeaderFilterUtil.getAllCategoriesFilterQuery(filter);
			catList = CategorySQLDAO.getAllCategoriesDataToExport(clientId, filterQuery);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return catList;
	}

	/**
	 * Creates the category.
	 *
	 * @param userId
	 *            the user id
	 * @param clientid
	 *            the clientid
	 * @param dto
	 *            the dto
	 * @return the OTT category DTO
	 * @throws Exception
	 *             the exception
	 */
	public static OTTCategoryDTO createCategory(String userId, String clientid, OTTCategoryDTO dto) throws Exception {
		try {
			dto.setSts(0);
			CategoryDVO dvo = dto.getDvo();
			Integer updCnt = CategorySQLDAO.createCategory(userId, clientid, dvo);
			if (updCnt == 1) {
				List<SubCategoryDVO> subcatList = SubCategorySQLDAO.getDefaultSubCategoriesForCat(clientid,
						dvo.getCattype());
				if (subcatList == null || subcatList.size() == 0) {
					SubCategoryDVO sdvo = new SubCategoryDVO();
					sdvo.setCattype(dvo.getCattype());
					sdvo.setSubcattype(Constant.ALL_SUB_CATEGORY);
					sdvo.setSubcatdesc(Constant.ALL_SUB_CATEGORY_DESC + dvo.getCattype());
					updCnt = SubCategorySQLDAO.createSubCategory(userId, clientid, sdvo);
				}
			}
			dto.setSts(updCnt);
		} catch (Exception ex) {
			dto.setSts(0);
			ex.printStackTrace();
		}

		return dto;
	}

	/**
	 * Delete category.
	 *
	 * @param userId
	 *            the user id
	 * @param clientid
	 *            the clientid
	 * @param dto
	 *            the dto
	 * @return the OTT category DTO
	 * @throws Exception
	 *             the exception
	 */
	public static OTTCategoryDTO deleteCategory(String userId, String clientid, OTTCategoryDTO dto) throws Exception {
		try {
			dto.setSts(0);
			CategoryDVO dvo = dto.getDvo();
			Integer updCnt = CategorySQLDAO.deleteCategory(userId, clientid, dvo);
			dto.setSts(updCnt);
		} catch (Exception ex) {
			dto.setSts(0);
			ex.printStackTrace();
		}
		return dto;
	}

}
