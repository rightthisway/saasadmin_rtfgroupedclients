/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.service;

import java.util.List;

import com.rtf.ott.cass.dao.ContestQuestionDAO;
import com.rtf.ott.dto.OTTContestQuestionsDTO;
import com.rtf.ott.sql.dao.ContestQuestionsDAO;
import com.rtf.ott.sql.dvo.ContestQuestionDVO;
import com.rtf.ott.util.GridHeaderFilterUtil;
import com.rtf.saas.util.PaginationUtil;

/**
 * The Class OTTContestQuestionsService.
 */
public class OTTContestQuestionsService {

	/**
	 * Gets the contestquestions by question id.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 * @param questionId
	 *            the question id
	 * @return the contestquestions by question id
	 * @throws Exception
	 *             the exception
	 */
	public static ContestQuestionDVO getcontestquestionsByQuestionId(String clientId, String contestId,
			Integer questionId) throws Exception {
		ContestQuestionDVO contQuestion = null;
		try {
			contQuestion = ContestQuestionsDAO.getcontestquestionsByQuestionId(clientId, contestId, questionId);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return contQuestion;
	}

	/**
	 * Gets the contestquestions bycontest id.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 * @return the contestquestions bycontest id
	 * @throws Exception
	 *             the exception
	 */
	public static List<ContestQuestionDVO> getcontestquestionsBycontestId(String clientId, String contestId)
			throws Exception {
		List<ContestQuestionDVO> list = null;
		try {
			list = ContestQuestionsDAO.getcontestquestionsBycontestId(clientId, contestId);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return list;
	}

	/**
	 * Gets the contestquestions by contest idand question seq no.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 * @param seqno
	 *            the seqno
	 * @return the contestquestions by contest idand question seq no
	 * @throws Exception
	 *             the exception
	 */
	public static ContestQuestionDVO getcontestquestionsByContestIdandQuestionSeqNo(String clientId, String contestId,
			Integer seqno) throws Exception {
		ContestQuestionDVO newseqNocontestQuestion = null;
		try {
			newseqNocontestQuestion = ContestQuestionsDAO.getcontestquestionsByContestIdandQuestionSeqNo(clientId,
					contestId, seqno);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return newseqNocontestQuestion;
	}

	/**
	 * Gets the contest questions count.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 * @return the contest questions count
	 * @throws Exception
	 *             the exception
	 */
	public static Integer getContestQuestionsCount(String clientId, String contestId) throws Exception {
		Integer questionsCount = null;
		try {
			questionsCount = ContestQuestionsDAO.getContestQuestionsCount(clientId, contestId);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return questionsCount;
	}

	/**
	 * Gets the contestquestions by contest idand question bank id.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 * @param questionBankId
	 *            the question bank id
	 * @return the contestquestions by contest idand question bank id
	 * @throws Exception
	 *             the exception
	 */
	public static ContestQuestionDVO getcontestquestionsByContestIdandQuestionBankId(String clientId, String contestId,
			Integer questionBankId) throws Exception {
		ContestQuestionDVO contQuestion = null;
		try {
			contQuestion = ContestQuestionsDAO.getcontestquestionsByContestIdandQuestionBankId(clientId, contestId,
					questionBankId);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return contQuestion;
	}

	/**
	 * Gets the all contest questions by contest idand filter.
	 *
	 * @param respDTO
	 *            the resp DTO
	 * @param filter
	 *            the filter
	 * @param pgNo
	 *            the pg no
	 * @return the all contest questions by contest idand filter
	 * @throws Exception
	 *             the exception
	 */
	public static OTTContestQuestionsDTO getAllContestQuestionsByContestIdandFilter(OTTContestQuestionsDTO respDTO,
			String filter, String pgNo) throws Exception {
		List<ContestQuestionDVO> contQuestions = null;
		try {

			String filterQuery = GridHeaderFilterUtil.getContestQuestionsFilterQuery(filter);
			contQuestions = ContestQuestionsDAO.getcontestquestionsBycontestIdAndFilter(respDTO.getClId(),
					respDTO.getCoId(), filterQuery, pgNo,filter);
			Integer count = ContestQuestionsDAO.getAllContestQuestionsCountByContestIdandFilter(respDTO.getClId(),
					respDTO.getCoId(), filterQuery);

			respDTO.setPagination(PaginationUtil.calculatePaginationParameter(count, pgNo));
			respDTO.setList(contQuestions);
			respDTO.setSts(1);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return respDTO;
	}

	/**
	 * Gets the all contest questions data to export.
	 *
	 * @param clId
	 *            the cl id
	 * @param coId
	 *            the co id
	 * @param filter
	 *            the filter
	 * @return the all contest questions data to export
	 * @throws Exception
	 *             the exception
	 */
	public static List<ContestQuestionDVO> getAllContestQuestionsDataToExport(String clId, String coId, String filter)
			throws Exception {
		List<ContestQuestionDVO> contQuestions = null;
		try {
			String filterQuery = GridHeaderFilterUtil.getContestQuestionsFilterQuery(filter);
			contQuestions = ContestQuestionsDAO.getAllContestQuestionsDataToExport(clId, coId, filterQuery);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return contQuestions;
	}

	/**
	 * Save contest questions.
	 *
	 * @param clientId
	 *            the client id
	 * @param contQuestion
	 *            the cont question
	 * @return the integer
	 * @throws Exception
	 *             the exception
	 */
	public static Integer saveContestQuestions(String clientId, ContestQuestionDVO contQuestion) throws Exception {
		Integer id = null;
		try {
			id = ContestQuestionsDAO.saveContestQuestions(clientId, contQuestion);
			if (id != null) {
				contQuestion.setConqsnid(id);
				ContestQuestionDAO.saveContestquestions(contQuestion);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return id;
	}

	/**
	 * Update contest questions.
	 *
	 * @param clientId
	 *            the client id
	 * @param contQuestion
	 *            the cont question
	 * @return the integer
	 * @throws Exception
	 *             the exception
	 */
	public static Integer updateContestQuestions(String clientId, ContestQuestionDVO contQuestion) throws Exception {
		Integer updatecount = null;
		try {
			updatecount = ContestQuestionsDAO.updateContestQuestions(clientId, contQuestion);
			if (updatecount != null) {
				ContestQuestionDAO.updateContestquestions(contQuestion);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return updatecount;
	}

	/**
	 * Delete contest questions.
	 *
	 * @param clientId
	 *            the client id
	 * @param contQuestion
	 *            the cont question
	 * @return the integer
	 * @throws Exception
	 *             the exception
	 */
	public static Integer deleteContestQuestions(String clientId, ContestQuestionDVO contQuestion) throws Exception {
		Integer updateCount = null;
		try {
			updateCount = ContestQuestionsDAO.deleteContestQuestions(clientId, contQuestion);
			if (updateCount != null && updateCount > 0) {
				ContestQuestionDAO.deleteContestquestions(contQuestion);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return updateCount;
	}

	/**
	 * Update contest questions seqno.
	 *
	 * @param clientId
	 *            the client id
	 * @param contQuestionList
	 *            the cont question list
	 * @return the integer
	 * @throws Exception
	 *             the exception
	 */
	public static Integer updateContestQuestionsSeqno(String clientId, List<ContestQuestionDVO> contQuestionList)
			throws Exception {
		Integer updateCount = null;
		try {
			updateCount = ContestQuestionsDAO.updateContestQuestionsSeqno(clientId, contQuestionList);
			if (updateCount != null && updateCount > 0) {
				ContestQuestionDAO.updateContestQuestionsSeqNo(contQuestionList);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return updateCount;
	}

}
