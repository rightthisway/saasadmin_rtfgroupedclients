/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.dto;

import java.util.List;

import com.rtf.ott.sql.dvo.CategoryDVO;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.dto.PaginationDTO;

/**
 * The Class OTTCategoryDTO.
 */
public class OTTCategoryDTO extends RtfSaasBaseDTO {

	/** The dvo. */
	private CategoryDVO dvo;

	/** The pagination. */
	private PaginationDTO pagination;

	/**
	 * Gets the dvo.
	 *
	 * @return the dvo
	 */
	public CategoryDVO getDvo() {
		return dvo;
	}

	/**
	 * Sets the dvo.
	 *
	 * @param dvo
	 *            the new dvo
	 */
	public void setDvo(CategoryDVO dvo) {
		this.dvo = dvo;
	}

	/**
	 * Gets the catlist.
	 *
	 * @return the catlist
	 */
	public List<CategoryDVO> getCatlist() {
		return catlist;
	}

	/**
	 * Sets the catlist.
	 *
	 * @param catlist
	 *            the new catlist
	 */
	public void setCatlist(List<CategoryDVO> catlist) {
		this.catlist = catlist;
	}

	/** The catlist. */
	private List<CategoryDVO> catlist;

	/**
	 * Gets the pagination.
	 *
	 * @return the pagination
	 */
	public PaginationDTO getPagination() {
		return pagination;
	}

	/**
	 * Sets the pagination.
	 *
	 * @param pagination
	 *            the new pagination
	 */
	public void setPagination(PaginationDTO pagination) {
		this.pagination = pagination;
	}

}
