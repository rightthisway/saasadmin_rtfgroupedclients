/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.dto;

import java.util.List;

import com.rtf.ott.cass.dvo.ContestRewardDVO;
import com.rtf.saas.arch.RtfSaasBaseDTO;

/**
 * The Class OTTContestRewardDTO.
 */
public class OTTContestRewardDTO extends RtfSaasBaseDTO {

	/** The reward. */
	private ContestRewardDVO reward;

	/** The rewards. */
	private List<ContestRewardDVO> rewards;

	/**
	 * Gets the reward.
	 *
	 * @return the reward
	 */
	public ContestRewardDVO getReward() {
		return reward;
	}

	/**
	 * Sets the reward.
	 *
	 * @param reward
	 *            the new reward
	 */
	public void setReward(ContestRewardDVO reward) {
		this.reward = reward;
	}

	/**
	 * Gets the rewards.
	 *
	 * @return the rewards
	 */
	public List<ContestRewardDVO> getRewards() {
		return rewards;
	}

	/**
	 * Sets the rewards.
	 *
	 * @param rewards
	 *            the new rewards
	 */
	public void setRewards(List<ContestRewardDVO> rewards) {
		this.rewards = rewards;
	}

}
