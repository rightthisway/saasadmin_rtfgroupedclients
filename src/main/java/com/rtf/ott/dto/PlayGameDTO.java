/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.dto;

import com.rtf.saas.arch.RtfSaasBaseDTO;

/**
 * The Class PlayGameDTO.
 */
public class PlayGameDTO extends RtfSaasBaseDTO {

	/** The cl id. */
	private String clId;

	/** The cu id. */
	private String cuId;

	/** The co id. */
	private String coId;

	/** The player id. */
	private String playerId;

	/** The q size. */
	private Integer qSize;

	/** The is elm. */
	private Boolean isElm;

	/** The elm msg. */
	private String elmMsg;

	/** The elm img. */
	private String elmImg;

	/** The wait timer. */
	private Integer waitTimer;

	/** The que timer. */
	private Integer queTimer;

	/** The ans timer. */
	private Integer ansTimer;

	/** The pop timer. */
	private Integer popTimer;

	/**
	 * Gets the cl id.
	 *
	 * @return the cl id
	 */
	public String getClId() {
		return clId;
	}

	/**
	 * Sets the cl id.
	 *
	 * @param clId
	 *            the new cl id
	 */
	public void setClId(String clId) {
		this.clId = clId;
	}

	/**
	 * Gets the cu id.
	 *
	 * @return the cu id
	 */
	public String getCuId() {
		return cuId;
	}

	/**
	 * Sets the cu id.
	 *
	 * @param cuId
	 *            the new cu id
	 */
	public void setCuId(String cuId) {
		this.cuId = cuId;
	}

	/**
	 * Gets the co id.
	 *
	 * @return the co id
	 */
	public String getCoId() {
		return coId;
	}

	/**
	 * Sets the co id.
	 *
	 * @param coId
	 *            the new co id
	 */
	public void setCoId(String coId) {
		this.coId = coId;
	}

	/**
	 * Gets the player id.
	 *
	 * @return the player id
	 */
	public String getPlayerId() {
		return playerId;
	}

	/**
	 * Sets the player id.
	 *
	 * @param playerId
	 *            the new player id
	 */
	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}

	/**
	 * Gets the wait timer.
	 *
	 * @return the wait timer
	 */
	public Integer getWaitTimer() {
		return waitTimer;
	}

	/**
	 * Sets the wait timer.
	 *
	 * @param waitTimer
	 *            the new wait timer
	 */
	public void setWaitTimer(Integer waitTimer) {
		this.waitTimer = waitTimer;
	}

	/**
	 * Gets the que timer.
	 *
	 * @return the que timer
	 */
	public Integer getQueTimer() {
		return queTimer;
	}

	/**
	 * Sets the que timer.
	 *
	 * @param queTimer
	 *            the new que timer
	 */
	public void setQueTimer(Integer queTimer) {
		this.queTimer = queTimer;
	}

	/**
	 * Gets the ans timer.
	 *
	 * @return the ans timer
	 */
	public Integer getAnsTimer() {
		return ansTimer;
	}

	/**
	 * Sets the ans timer.
	 *
	 * @param ansTimer
	 *            the new ans timer
	 */
	public void setAnsTimer(Integer ansTimer) {
		this.ansTimer = ansTimer;
	}

	/**
	 * Gets the pop timer.
	 *
	 * @return the pop timer
	 */
	public Integer getPopTimer() {
		return popTimer;
	}

	/**
	 * Sets the pop timer.
	 *
	 * @param popTimer
	 *            the new pop timer
	 */
	public void setPopTimer(Integer popTimer) {
		this.popTimer = popTimer;
	}

	/**
	 * Gets the q size.
	 *
	 * @return the q size
	 */
	public Integer getqSize() {
		return qSize;
	}

	/**
	 * Sets the q size.
	 *
	 * @param qSize
	 *            the new q size
	 */
	public void setqSize(Integer qSize) {
		this.qSize = qSize;
	}

	/**
	 * Gets the checks if is elm.
	 *
	 * @return the checks if is elm
	 */
	public Boolean getIsElm() {
		return isElm;
	}

	/**
	 * Sets the checks if is elm.
	 *
	 * @param isElm
	 *            the new checks if is elm
	 */
	public void setIsElm(Boolean isElm) {
		this.isElm = isElm;
	}

	/**
	 * Gets the elm msg.
	 *
	 * @return the elm msg
	 */
	public String getElmMsg() {
		return elmMsg;
	}

	/**
	 * Sets the elm msg.
	 *
	 * @param elmMsg
	 *            the new elm msg
	 */
	public void setElmMsg(String elmMsg) {
		this.elmMsg = elmMsg;
	}

	/**
	 * Gets the elm img.
	 *
	 * @return the elm img
	 */
	public String getElmImg() {
		return elmImg;
	}

	/**
	 * Sets the elm img.
	 *
	 * @param elmImg
	 *            the new elm img
	 */
	public void setElmImg(String elmImg) {
		this.elmImg = elmImg;
	}

}
