/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.dto;

import com.rtf.saas.arch.RtfSaasBaseDTO;

/**
 * The Class ContestSummaryDTO.
 */
public class ContestSummaryDTO extends RtfSaasBaseDTO {

	/** The cl id. */
	private String clId;
	
	/** The cu id. */
	private String cuId;
	
	/** The co id. */
	private String coId;
	
	/** The rwd type. */
	private String rwdType;
	
	/** The rwd val. */
	private Double rwdVal;
	
	/** The player id. */
	private String playerId;
	
	/** The rwd img. */
	private String rwdImg;
	
	/**
	 * Gets the cl id.
	 *
	 * @return the cl id
	 */
	public String getClId() {
		return clId;
	}
	
	/**
	 * Sets the cl id.
	 *
	 * @param clId the new cl id
	 */
	public void setClId(String clId) {
		this.clId = clId;
	}
	
	/**
	 * Gets the cu id.
	 *
	 * @return the cu id
	 */
	public String getCuId() {
		return cuId;
	}
	
	/**
	 * Sets the cu id.
	 *
	 * @param cuId the new cu id
	 */
	public void setCuId(String cuId) {
		this.cuId = cuId;
	}
	
	/**
	 * Gets the co id.
	 *
	 * @return the co id
	 */
	public String getCoId() {
		return coId;
	}
	
	/**
	 * Sets the co id.
	 *
	 * @param coId the new co id
	 */
	public void setCoId(String coId) {
		this.coId = coId;
	}
	
	/**
	 * Gets the rwd type.
	 *
	 * @return the rwd type
	 */
	public String getRwdType() {
		return rwdType;
	}
	
	/**
	 * Sets the rwd type.
	 *
	 * @param rwdType the new rwd type
	 */
	public void setRwdType(String rwdType) {
		this.rwdType = rwdType;
	}
	
	/**
	 * Gets the rwd val.
	 *
	 * @return the rwd val
	 */
	public Double getRwdVal() {
		return rwdVal;
	}
	
	/**
	 * Sets the rwd val.
	 *
	 * @param rwdVal the new rwd val
	 */
	public void setRwdVal(Double rwdVal) {
		this.rwdVal = rwdVal;
	}
	
	/**
	 * Gets the player id.
	 *
	 * @return the player id
	 */
	public String getPlayerId() {
		return playerId;
	}
	
	/**
	 * Sets the player id.
	 *
	 * @param playerId the new player id
	 */
	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}
	
	/**
	 * Gets the rwd img.
	 *
	 * @return the rwd img
	 */
	public String getRwdImg() {
		return rwdImg;
	}
	
	/**
	 * Sets the rwd img.
	 *
	 * @param rwdImg the new rwd img
	 */
	public void setRwdImg(String rwdImg) {
		this.rwdImg = rwdImg;
	}
	
}
