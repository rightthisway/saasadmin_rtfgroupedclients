/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.dto;

import java.util.List;

import com.rtf.ott.sql.dvo.QuestionBankDVO;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.dto.PaginationDTO;

/**
 * The Class OTTQuestionDTO.
 */
public class OTTQuestionDTO extends RtfSaasBaseDTO {

	/** The ottqbdo. */
	private QuestionBankDVO ottqbdo;

	/** The qb list. */
	private List<QuestionBankDVO> qbList;

	/** The pagination. */
	private PaginationDTO pagination;

	/**
	 * Gets the ottqbdo.
	 *
	 * @return the ottqbdo
	 */
	public QuestionBankDVO getOttqbdo() {
		return ottqbdo;
	}

	/**
	 * Sets the ottqbdo.
	 *
	 * @param ottqbdo
	 *            the new ottqbdo
	 */
	public void setOttqbdo(QuestionBankDVO ottqbdo) {
		this.ottqbdo = ottqbdo;
	}

	/**
	 * Gets the qb list.
	 *
	 * @return the qb list
	 */
	public List<QuestionBankDVO> getQbList() {
		return qbList;
	}

	/**
	 * Sets the qb list.
	 *
	 * @param qbList
	 *            the new qb list
	 */
	public void setQbList(List<QuestionBankDVO> qbList) {
		this.qbList = qbList;
	}

	/** The hme. */
	private Boolean hme;

	/**
	 * Gets the hme.
	 *
	 * @return the hme
	 */
	public Boolean getHme() {
		if (hme == null) {
			hme = false;
		}
		return hme;

	}

	/**
	 * Sets the hme.
	 *
	 * @param hme
	 *            the new hme
	 */
	public void setHme(Boolean hme) {
		this.hme = hme;
	}

	/**
	 * Gets the pagination.
	 *
	 * @return the pagination
	 */
	public PaginationDTO getPagination() {
		return pagination;
	}

	/**
	 * Sets the pagination.
	 *
	 * @param pagination
	 *            the new pagination
	 */
	public void setPagination(PaginationDTO pagination) {
		this.pagination = pagination;
	}

}
