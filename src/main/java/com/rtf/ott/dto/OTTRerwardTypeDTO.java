/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.dto;

import java.util.List;

import com.rtf.ott.sql.dvo.RewardTypeDVO;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.dto.PaginationDTO;

/**
 * The Class OTTRerwardTypeDTO.
 */
public class OTTRerwardTypeDTO extends RtfSaasBaseDTO {

	/** The reward. */
	private RewardTypeDVO reward;

	/** The reward list. */
	private List<RewardTypeDVO> rewardList;

	/** The pagination. */
	private PaginationDTO pagination;

	/**
	 * Gets the reward.
	 *
	 * @return the reward
	 */
	public RewardTypeDVO getReward() {
		return reward;
	}

	/**
	 * Sets the reward.
	 *
	 * @param reward
	 *            the new reward
	 */
	public void setReward(RewardTypeDVO reward) {
		this.reward = reward;
	}

	/**
	 * Gets the reward list.
	 *
	 * @return the reward list
	 */
	public List<RewardTypeDVO> getRewardList() {
		return rewardList;
	}

	/**
	 * Sets the reward list.
	 *
	 * @param rewardList
	 *            the new reward list
	 */
	public void setRewardList(List<RewardTypeDVO> rewardList) {
		this.rewardList = rewardList;
	}

	/**
	 * Gets the pagination.
	 *
	 * @return the pagination
	 */
	public PaginationDTO getPagination() {
		return pagination;
	}

	/**
	 * Sets the pagination.
	 *
	 * @param pagination
	 *            the new pagination
	 */
	public void setPagination(PaginationDTO pagination) {
		this.pagination = pagination;
	}

}
