/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.user.dto;

import java.util.List;

import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.user.dvo.UserRoleDVO;

/**
 * The Class UserRoleDTO.
 */
public class UserRoleDTO extends RtfSaasBaseDTO {

	/** The list. */
	private List<UserRoleDVO> list;

	/**
	 * Gets the list.
	 *
	 * @return the list
	 */
	public List<UserRoleDVO> getList() {
		return list;
	}

	/**
	 * Sets the list.
	 *
	 * @param list the new list
	 */
	public void setList(List<UserRoleDVO> list) {
		this.list = list;
	}

}