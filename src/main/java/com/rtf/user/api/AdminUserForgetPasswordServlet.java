/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.user.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;
import com.rtf.user.dto.UserDTO;
import com.rtf.user.dvo.ClientUserDVO;
import com.rtf.user.service.AdminUserService;
import com.rtf.user.sql.dao.ClientUserDAO;
import com.rtf.user.util.UserMsgConstants;

/**
 * The Class AdminUserForgetPasswordServlet.
 */
@WebServlet("/aufgtp.json")
public class AdminUserForgetPasswordServlet extends RtfSaasBaseServlet {

	/**
	 * Serial version id
	 */
	private static final long serialVersionUID = 3576603663844041697L;

	/**
	 * Generate Http Response for Client User Response data is sent in JSON format.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}

	/**
	 * validate user based on user id and sent password reset link to registered
	 * email id .
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String userIdStr = request.getParameter("userId");
		UserDTO dto = new UserDTO();
		dto.setSts(0);
		try {

			if (userIdStr == null || userIdStr.isEmpty()) {
				setClientMessage(dto, UserMsgConstants.INVALID_USER_ID, null);
				generateResponse(request, response, dto);
				return;
			}
			userIdStr = userIdStr.toLowerCase();

			String regex = UserMsgConstants.EMAIL_VALIDATE_REG_EXP;
			Pattern pattern = Pattern.compile(regex);
			Matcher match = pattern.matcher(userIdStr);
			if (!match.matches()) {
				setClientMessage(dto, UserMsgConstants.VALID_EMAIL_ADDRES_MSG, null);
				generateResponse(request, response, dto);
				return;
			}
			ClientUserDVO user = ClientUserDAO.getClientUserByUserId(userIdStr);
			if (user == null || user.getUserid() == null) {
				setClientMessage(dto, null, null);
				generateResponse(request, response, dto);
				return;
			}
			String cliId = user.getClintid();
			ClientUserDVO dvo = new ClientUserDVO();
			dvo.setClintid(cliId);
			dvo.setUserid(userIdStr);
			dto.setDvo(dvo);
			dto = AdminUserService.adminUserForgotPassword(dto, cliId);
			if (dto.getSts() == 0) {
				setClientMessage(dto, null, null);
				generateResponse(request, response, dto);
				return;
			}
			dto.setMsg(UserMsgConstants.USER_PWD_RESET_INSTRUCTION);
			generateResponse(request, response, dto);
			return;
		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(dto, null, null);
			generateResponse(request, response, dto);
		}

	}

}
