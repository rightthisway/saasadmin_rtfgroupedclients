/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.user.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import com.rtf.common.util.SecurityUtil;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;
import com.rtf.user.dto.UserDTO;
import com.rtf.user.dvo.ClientUserDVO;
import com.rtf.user.service.AdminUserService;
import com.rtf.user.sql.dao.ClientUserDAO;
import com.rtf.user.util.UserMsgConstants;

/**
 * The Class AdminUserValidateEmailResetPasswordServlet.
 */
@WebServlet("/auvrstp.json")
public class AdminUserValidateEmailResetPasswordServlet extends RtfSaasBaseServlet {

	/**
	 * Serial version id
	 */
	private static final long serialVersionUID = 1403894164936371220L;

	/**
	 * Generate Http Response for Client User Response data is sent in JSON format.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * validate password reset token sent to client user and return client user
	 * details.
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		UserDTO dto = new UserDTO();
		dto.setSts(0);
		String actMsg = StringUtils.EMPTY;
		String clId = null;
		String token = request.getParameter("t");
		try {

			if (token == null || token.isEmpty() || !token.contains("~~")) {
				actMsg = UserMsgConstants.INVALID_RESET_PWD_TOKEN;
				setClientMessage(dto, actMsg, null);
				generateResponse(request, response, dto);
				return;
			}

			String tok[] = token.split("~~");
			if (tok.length < 3) {
				actMsg = UserMsgConstants.INVALID_RESET_PWD_TOKEN;
				setClientMessage(dto, actMsg, null);
				generateResponse(request, response, dto);
				return;
			}
			clId = SecurityUtil.decryptString(tok[0]);
			String custId = SecurityUtil.decryptString(tok[1]);

			ClientUserDVO custvo = ClientUserDAO.getClientUserByClientIdAndUserId(clId, custId);
			if (custvo == null || GenUtil.isNullOrEmpty(custvo.getClintid())) {
				actMsg = UserMsgConstants.USER_NOT_REGISTERED;
				setClientMessage(dto, actMsg, null);
				generateResponse(request, response, dto);
				return;
			}
			String uuid = tok[2];
			dto = AdminUserService.validateEmailResetTokens(dto, clId, custId, uuid);
			if (dto.getSts() == 0) {
				setClientMessage(dto, null, null);
				generateResponse(request, response, dto);
				return;
			}
			ClientUserDVO user = new ClientUserDVO();
			user.setClintid(clId);
			user.setUserid(custId);
			dto.setDvo(user);
			dto.setMsg(UserMsgConstants.RESET_VALIDATION_IS_SUCCESS);
			generateResponse(request, response, dto);
			return;
		} catch (Exception e) {
			actMsg = UserMsgConstants.COMMON_ERR_MSG;
			e.printStackTrace();
			setClientMessage(dto, actMsg, null);
			generateResponse(request, response, dto);
			return;
		}

	}

}
