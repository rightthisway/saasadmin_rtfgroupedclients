/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.user.dvo;

import java.io.Serializable;
import java.util.Date;

/**
 * The Class ClientUserPasswordDVO.
 */
public class ClientUserPasswordDVO implements Serializable {

	/** The clintid. */
	private String clintid;

	/** The userid. */
	private String userid;

	/** The useremail. */
	private String useremail;

	/** The userpwd. */
	private String userpwd;

	/** The notisentdate. */
	private Date notisentdate;

	/** The notiurl. */
	private String notiurl;

	/** The notiveridate. */
	private Date notiveridate;

	/** The ispwdactivated. */
	private String ispwdactivated;

	/** The prevcredate. */
	private Date prevcredate;

	/** The creby. */
	private String creby;

	/** The credate. */
	private Date credate;

	/** The updby. */
	private String updby;

	/** The upddate. */
	private Date upddate;

	/** The resettoken. */
	private String resettoken;

	/** The tokenvaliditydate. */
	private Date tokenvaliditydate;

	/**
	 * Gets the clintid.
	 *
	 * @return the clintid
	 */
	public String getClintid() {
		return clintid;
	}

	/**
	 * Sets the clintid.
	 *
	 * @param clintid the new clintid
	 */
	public void setClintid(String clintid) {
		this.clintid = clintid;
	}

	/**
	 * Gets the userid.
	 *
	 * @return the userid
	 */
	public String getUserid() {
		return userid;
	}

	/**
	 * Sets the userid.
	 *
	 * @param userid the new userid
	 */
	public void setUserid(String userid) {
		this.userid = userid;
	}

	/**
	 * Gets the useremail.
	 *
	 * @return the useremail
	 */
	public String getUseremail() {
		return useremail;
	}

	/**
	 * Sets the useremail.
	 *
	 * @param useremail the new useremail
	 */
	public void setUseremail(String useremail) {
		this.useremail = useremail;
	}

	/**
	 * Gets the userpwd.
	 *
	 * @return the userpwd
	 */
	public String getUserpwd() {
		return userpwd;
	}

	/**
	 * Sets the userpwd.
	 *
	 * @param userpwd the new userpwd
	 */
	public void setUserpwd(String userpwd) {
		this.userpwd = userpwd;
	}

	/**
	 * Gets the notisentdate.
	 *
	 * @return the notisentdate
	 */
	public Date getNotisentdate() {
		return notisentdate;
	}

	/**
	 * Sets the notisentdate.
	 *
	 * @param notisentdate the new notisentdate
	 */
	public void setNotisentdate(Date notisentdate) {
		this.notisentdate = notisentdate;
	}

	/**
	 * Gets the notiurl.
	 *
	 * @return the notiurl
	 */
	public String getNotiurl() {
		return notiurl;
	}

	/**
	 * Sets the notiurl.
	 *
	 * @param notiurl the new notiurl
	 */
	public void setNotiurl(String notiurl) {
		this.notiurl = notiurl;
	}

	/**
	 * Gets the notiveridate.
	 *
	 * @return the notiveridate
	 */
	public Date getNotiveridate() {
		return notiveridate;
	}

	/**
	 * Sets the notiveridate.
	 *
	 * @param notiveridate the new notiveridate
	 */
	public void setNotiveridate(Date notiveridate) {
		this.notiveridate = notiveridate;
	}

	/**
	 * Gets the ispwdactivated.
	 *
	 * @return the ispwdactivated
	 */
	public String getIspwdactivated() {
		return ispwdactivated;
	}

	/**
	 * Sets the ispwdactivated.
	 *
	 * @param ispwdactivated the new ispwdactivated
	 */
	public void setIspwdactivated(String ispwdactivated) {
		this.ispwdactivated = ispwdactivated;
	}

	/**
	 * Gets the prevcredate.
	 *
	 * @return the prevcredate
	 */
	public Date getPrevcredate() {
		return prevcredate;
	}

	/**
	 * Sets the prevcredate.
	 *
	 * @param prevcredate the new prevcredate
	 */
	public void setPrevcredate(Date prevcredate) {
		this.prevcredate = prevcredate;
	}

	/**
	 * Gets the creby.
	 *
	 * @return the creby
	 */
	public String getCreby() {
		return creby;
	}

	/**
	 * Sets the creby.
	 *
	 * @param creby the new creby
	 */
	public void setCreby(String creby) {
		this.creby = creby;
	}

	/**
	 * Gets the credate.
	 *
	 * @return the credate
	 */
	public Date getCredate() {
		return credate;
	}

	/**
	 * Sets the credate.
	 *
	 * @param credate the new credate
	 */
	public void setCredate(Date credate) {
		this.credate = credate;
	}

	/**
	 * Gets the updby.
	 *
	 * @return the updby
	 */
	public String getUpdby() {
		return updby;
	}

	/**
	 * Sets the updby.
	 *
	 * @param updby the new updby
	 */
	public void setUpdby(String updby) {
		this.updby = updby;
	}

	/**
	 * Gets the upddate.
	 *
	 * @return the upddate
	 */
	public Date getUpddate() {
		return upddate;
	}

	/**
	 * Sets the upddate.
	 *
	 * @param upddate the new upddate
	 */
	public void setUpddate(Date upddate) {
		this.upddate = upddate;
	}

	/**
	 * Gets the resettoken.
	 *
	 * @return the resettoken
	 */
	public String getResettoken() {
		return resettoken;
	}

	/**
	 * Sets the resettoken.
	 *
	 * @param resettoken the new resettoken
	 */
	public void setResettoken(String resettoken) {
		this.resettoken = resettoken;
	}

	/**
	 * Gets the tokenvaliditydate.
	 *
	 * @return the tokenvaliditydate
	 */
	public Date getTokenvaliditydate() {
		return tokenvaliditydate;
	}

	/**
	 * Sets the tokenvaliditydate.
	 *
	 * @param tokenvaliditydate the new tokenvaliditydate
	 */
	public void setTokenvaliditydate(Date tokenvaliditydate) {
		this.tokenvaliditydate = tokenvaliditydate;
	}

}