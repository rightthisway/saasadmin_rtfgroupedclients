/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.user.sql.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.rtf.saas.sql.db.DatabaseConnections;
import com.rtf.user.dvo.ClientUserPasswordDVO;
import com.rtf.user.util.UserMsgConstants;

/**
 * The Class ClientUserPasswordDAO.
 */
public class ClientUserPasswordDAO {

	/**
	 * Gets the client user password by user id.
	 *
	 * @param userId the user id
	 * @return the client user password by user id
	 * @throws Exception the exception
	 */
	/*
	 * @param Clientid
	 * 
	 * @return
	 * 
	 * @throws Exception
	 */
	public static ClientUserPasswordDVO getClientUserPasswordByUserId(String userId) throws Exception {

		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		ClientUserPasswordDVO clientUserPwd = null;
		StringBuffer sql = new StringBuffer("SELECT clintid,userid,useremail,userpwd,notisentdate,notiurl,notiveridate,ispwdactivated,creby,credate,prevcredate,updby,upddate");
				sql.append("  FROM client_user_pwd where userid=?");
		try {
			conn = DatabaseConnections.getSaasClientDetailsConnection();

			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, userId);
			rs = ps.executeQuery();
			while (rs.next()) {
				clientUserPwd = new ClientUserPasswordDVO();
				clientUserPwd.setClintid(rs.getString("clintid"));
				clientUserPwd.setUserid(rs.getString("userid"));
				clientUserPwd.setUseremail(rs.getString("useremail"));
				clientUserPwd.setUserpwd(rs.getString("userpwd"));
				clientUserPwd.setNotiurl(rs.getString("notiurl"));
				clientUserPwd.setIspwdactivated(rs.getString("ispwdactivated"));
				clientUserPwd.setCreby(rs.getString("creby"));
				clientUserPwd.setUpdby(rs.getString("updby"));

				if (rs.getTimestamp("credate") != null) {
					clientUserPwd.setCredate(new Date(rs.getTimestamp("credate").getTime()));
				}
				if (rs.getTimestamp("notisentdate") != null) {
					clientUserPwd.setNotisentdate(new Date(rs.getTimestamp("notisentdate").getTime()));
				}
				if (rs.getTimestamp("notiveridate") != null) {
					clientUserPwd.setNotiveridate(new Date(rs.getTimestamp("notiveridate").getTime()));
				}
				if (rs.getTimestamp("prevcredate") != null) {
					clientUserPwd.setPrevcredate(new Date(rs.getTimestamp("prevcredate").getTime()));
				}
				if (rs.getTimestamp("upddate") != null) {
					clientUserPwd.setUpddate(new Date(rs.getTimestamp("upddate").getTime()));
				}
			}
			return clientUserPwd;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;

	}

	/**
	 * Gets the all client user passwords.
	 *
	 * @return the all client user passwords
	 * @throws Exception the exception
	 */
	public static List<ClientUserPasswordDVO> getAllClientUserPasswords() throws Exception {

		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		List<ClientUserPasswordDVO> list = new ArrayList<ClientUserPasswordDVO>();
		ClientUserPasswordDVO clientUserPwd = null;
		StringBuffer sql = new StringBuffer("SELECT clintid,userid,useremail,userpwd,notisentdate,notiurl,notiveridate,ispwdactivated,creby,credate,prevcredate,updby,upddate");
				sql.append("  FROM client_user_pwd ");
		try {
			conn = DatabaseConnections.getSaasClientDetailsConnection();

			ps = conn.prepareStatement(sql.toString());
			rs = ps.executeQuery();
			while (rs.next()) {
				clientUserPwd = new ClientUserPasswordDVO();
				clientUserPwd.setClintid(rs.getString("clintid"));
				clientUserPwd.setUserid(rs.getString("userid"));
				clientUserPwd.setUseremail(rs.getString("useremail"));
				clientUserPwd.setUserpwd(rs.getString("userpwd"));
				clientUserPwd.setNotiurl(rs.getString("notiurl"));
				clientUserPwd.setIspwdactivated(rs.getString("ispwdactivated"));
				clientUserPwd.setCreby(rs.getString("creby"));
				clientUserPwd.setUpdby(rs.getString("updby"));

				if (rs.getTimestamp("credate") != null) {
					clientUserPwd.setCredate(new Date(rs.getTimestamp("credate").getTime()));
				}
				if (rs.getTimestamp("notisentdate") != null) {
					clientUserPwd.setNotisentdate(new Date(rs.getTimestamp("notisentdate").getTime()));
				}
				if (rs.getTimestamp("notiveridate") != null) {
					clientUserPwd.setNotiveridate(new Date(rs.getTimestamp("notiveridate").getTime()));
				}
				if (rs.getTimestamp("prevcredate") != null) {
					clientUserPwd.setPrevcredate(new Date(rs.getTimestamp("prevcredate").getTime()));
				}
				if (rs.getTimestamp("upddate") != null) {
					clientUserPwd.setUpddate(new Date(rs.getTimestamp("upddate").getTime()));
				}
				list.add(clientUserPwd);
			}
			return list;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;

	}

	/**
	 * Save client U ser password.
	 *
	 * @param clientId the client id
	 * @param obj      the obj
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer saveClientUSerPassword(String clientId, ClientUserPasswordDVO obj) throws Exception {
		Integer id = null;
		String linkServer = DatabaseConnections.getSaasClientRegLinkedServer() + ".dbo.";

		StringBuffer sql = new StringBuffer("insert into " + linkServer + "client_user_pwd(clintid, userid, useremail,userpwd,creby, credate) ");
				sql.append("values(?,?,?,?,?,?)");

		try {
			Connection conn = DatabaseConnections.getSaasClientDetailsConnection();
			PreparedStatement statement = conn.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, obj.getClintid());
			statement.setString(2, obj.getUserid());
			statement.setString(3, obj.getUseremail());
			statement.setString(4, obj.getUserpwd());
			statement.setString(5, obj.getCreby());
			statement.setTimestamp(6, new Timestamp(obj.getCredate().getTime()));

			int affectedRows = statement.executeUpdate();
			if (affectedRows == 0) {
				throw new SQLException("Creating client user pwd record failed, no rows affected.");
			}
			ResultSet generatedKeys;
			try {
				generatedKeys = statement.getGeneratedKeys();
				if (generatedKeys.next()) {
					id = generatedKeys.getInt(1);

				}
				generatedKeys.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			statement.close();
			DatabaseConnections.closeConnection(conn);
		} catch (SQLException se) {
			se.printStackTrace();
			throw se;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return id;

	}

	/**
	 * Admin reset user password.
	 *
	 * @param dvo the dvo
	 * @return the boolean
	 */
	public static Boolean adminResetUserPassword(ClientUserPasswordDVO dvo) {
		Connection conn = null;
		PreparedStatement ps = null;
		Boolean isUpdated = false;
		String linkServer = DatabaseConnections.getSaasClientRegLinkedServer() + ".dbo.";
		try {
			conn = DatabaseConnections.getSaasClientDetailsConnection();
			StringBuffer sql = new StringBuffer("UPDATE " + linkServer + "client_user_pwd set userpwd = ? , ");
			sql.append(" updby=?, upddate=getDate() where clintid=? and userid=?");
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, dvo.getUserpwd());
			ps.setString(2, dvo.getUpdby());
			ps.setString(3, dvo.getClintid());
			ps.setString(4, dvo.getUserid());
			int affectedRows = ps.executeUpdate();
			if (affectedRows > 0) {
				isUpdated = true;
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return isUpdated;
	}

	/**
	 * Gets the client user password by clint id by user id.
	 *
	 * @param clId   the cl id
	 * @param userId the user id
	 * @return the client user password by clint id by user id
	 * @throws Exception the exception
	 */
	public static ClientUserPasswordDVO getClientUserPasswordByClintIdByUserId(String clId, String userId)
			throws Exception {

		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		ClientUserPasswordDVO clientUserPwd = null;
		StringBuffer sql = new StringBuffer("SELECT clintid,userid,useremail" + "  FROM client_user_pwd where userid=? and clintid=? ");
		try {
			conn = DatabaseConnections.getSaasClientDetailsConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, userId);
			ps.setString(2, clId);
			rs = ps.executeQuery();
			while (rs.next()) {
				clientUserPwd = new ClientUserPasswordDVO();
				clientUserPwd.setClintid(rs.getString("clintid"));
				clientUserPwd.setUserid(rs.getString("userid"));
				clientUserPwd.setUseremail(rs.getString("useremail"));
			}
			return clientUserPwd;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;

	}

	/**
	 * User update pasword token.
	 *
	 * @param dvo the dvo
	 * @return the boolean
	 */
	public static Boolean userUpdatePaswordToken(ClientUserPasswordDVO dvo) {
		Connection conn = null;
		PreparedStatement ps = null;
		Boolean isUpdated = false;
		String linkServer = DatabaseConnections.getSaasClientRegLinkedServer() + ".dbo.";
		try {
			conn = DatabaseConnections.getSaasClientDetailsConnection();
			StringBuffer sql = new StringBuffer("UPDATE " + linkServer + "client_user_pwd set userpwd = ? , ");
					sql.append(" updby=?, upddate=getDate() , resettoken=? , ispwdactivated=? where clintid=? and userid=?");
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, dvo.getUserpwd());
			ps.setString(2, dvo.getUpdby());
			ps.setString(3, dvo.getResettoken());
			ps.setString(4, dvo.getIspwdactivated());
			ps.setString(5, dvo.getClintid());
			ps.setString(6, dvo.getUserid());
			int affectedRows = ps.executeUpdate();
			if (affectedRows > 0) {
				isUpdated = true;
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return isUpdated;
	}

	/**
	 * User update email recd reset token.
	 *
	 * @param cliId      the cli id
	 * @param cuId       the cu id
	 * @param resetToken the reset token
	 * @return the boolean
	 */
	public static Boolean userUpdateEmailRecdResetToken(String cliId, String cuId, String resetToken) {
		Connection conn = null;
		PreparedStatement ps = null;
		Boolean isUpdated = false;
		String linkServer = DatabaseConnections.getSaasClientRegLinkedServer() + ".dbo.";
		try {
			conn = DatabaseConnections.getSaasClientDetailsConnection();
			StringBuffer sql = new StringBuffer("UPDATE " + linkServer + "client_user_pwd set  ");
			sql.append(" updby=?, upddate=getDate() , notiveridate=getDate(), resettoken=? ,");
			sql.append("  ispwdactivated=? where clintid=? and userid=? and resettoken = ?");
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, cuId);
			ps.setString(2, null);
			ps.setString(3, UserMsgConstants.ACTIVE_USER_STATUS);
			ps.setString(4, cliId);
			ps.setString(5, cuId);
			ps.setString(6, resetToken);
			int affectedRows = ps.executeUpdate();
			if (affectedRows > 0) {
				isUpdated = true;
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return isUpdated;
	}

	/**
	 * Delete user password.
	 *
	 * @param cliId the cli id
	 * @param uId   the u id
	 * @return true, if successful
	 * @throws Exception the exception
	 */
	public static boolean deleteUserPassword(String cliId, String uId) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		Boolean isUpdated = false;
		String linkServer = DatabaseConnections.getSaasClientRegLinkedServer() + ".dbo.";
		try {
			conn = DatabaseConnections.getSaasClientDetailsConnection();
			StringBuffer sql = new StringBuffer("DELETE FROM  " + linkServer + "client_user_pwd WHERE clintid=? and userid=?");
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, cliId);
			ps.setString(2, uId);
			int affectedRows = ps.executeUpdate();
			if (affectedRows > 0) {
				isUpdated = true;
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return isUpdated;
	}

}
