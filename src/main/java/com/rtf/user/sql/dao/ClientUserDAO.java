/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.user.sql.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.rtf.common.util.GridSortingUtil;
import com.rtf.saas.sql.db.DatabaseConnections;
import com.rtf.saas.util.PaginationUtil;
import com.rtf.user.dvo.ClientUserDVO;

/**
 * The Class ClientUserDAO.
 */
public class ClientUserDAO {

	/**
	 * Gets the client user by user id.
	 *
	 * @param userId the user id
	 * @return the client user by user id
	 * @throws Exception the exception
	 */
	/*
	 * @param Clientid
	 * 
	 * @return
	 * 
	 * @throws Exception
	 */
	public static ClientUserDVO getClientUserByUserId(String userId) throws Exception {

		String linkServer = DatabaseConnections.getSaasClientRegLinkedServer() + ".dbo.";
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		ClientUserDVO clientUser = null;
		
		StringBuffer sql = new StringBuffer("SELECT clintid, userid, useremail, creby, credate, isactive, istestuser, updby, upddate, ");
		sql.append(" userfname, userlname, userphone,userrole from " + linkServer + "client_user where userid=?");
		try {
			conn = DatabaseConnections.getSaasClientDetailsConnection();

			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, userId);
			rs = ps.executeQuery();
			while (rs.next()) {
				clientUser = new ClientUserDVO();
				clientUser.setClintid(rs.getString("clintid"));
				clientUser.setUserid(rs.getString("userid"));
				clientUser.setUseremail(rs.getString("useremail"));
				clientUser.setUserfname(rs.getString("userfname"));
				clientUser.setUserlname(rs.getString("userlname"));
				clientUser.setUserphone(rs.getString("userphone"));
				clientUser.setUserrole(rs.getString("userrole"));
				clientUser.setCredate(rs.getDate("credate"));
				clientUser.setCreby(rs.getString("creby"));
				clientUser.setUpddate(rs.getDate("upddate"));
				clientUser.setUpdby(rs.getString("updby"));
				clientUser.setIsactive(rs.getString("isactive"));
				clientUser.setIstestuser(rs.getBoolean("istestuser"));
			}
			return clientUser;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;

	}

	/**
	 * Gets the client user by client id and user id.
	 *
	 * @param clientId the client id
	 * @param userId   the user id
	 * @return the client user by client id and user id
	 * @throws Exception the exception
	 */
	public static ClientUserDVO getClientUserByClientIdAndUserId(String clientId, String userId) throws Exception {

		String linkServer = DatabaseConnections.getSaasClientRegLinkedServer() + ".dbo.";
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		ClientUserDVO clientUser = null;
		StringBuffer sql = new StringBuffer("SELECT clintid, userid, useremail, creby, credate, isactive, istestuser, updby, upddate, ");
		sql.append( " userfname, userlname, userphone,userrole from " + linkServer);
		sql.append("client_user where clintid=? and userid=?");
		try {
			conn = DatabaseConnections.getSaasClientDetailsConnection();

			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			ps.setString(2, userId);
			rs = ps.executeQuery();
			while (rs.next()) {
				clientUser = new ClientUserDVO();
				clientUser.setClintid(rs.getString("clintid"));
				clientUser.setUserid(rs.getString("userid"));
				clientUser.setUseremail(rs.getString("useremail"));
				clientUser.setUserfname(rs.getString("userfname"));
				clientUser.setUserlname(rs.getString("userlname"));
				clientUser.setUserphone(rs.getString("userphone"));
				clientUser.setUserrole(rs.getString("userrole"));
				clientUser.setCredate(rs.getDate("credate"));
				clientUser.setCreby(rs.getString("creby"));
				clientUser.setUpddate(rs.getDate("upddate"));
				clientUser.setUpdby(rs.getString("updby"));
				clientUser.setIsactive(rs.getString("isactive"));
				clientUser.setIstestuser(rs.getBoolean("istestuser"));
			}
			return clientUser;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;

	}

	/**
	 * Gets the active users count by client id.
	 *
	 * @param clientId the client id
	 * @return the active users count by client id
	 * @throws Exception the exception
	 */
	public static Integer getActiveUsersCountByClientId(String clientId) throws Exception {

		String linkServer = DatabaseConnections.getSaasClientRegLinkedServer() + ".dbo.";
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		Integer userCount = null;
		StringBuffer sql = new StringBuffer("SELECT count(*) as userCount from " + linkServer + "client_user where clintid=?");
		try {
			conn = DatabaseConnections.getSaasClientDetailsConnection();

			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			rs = ps.executeQuery();
			if (rs.next()) {
				userCount = rs.getInt("userCount");
			}
			return userCount;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;

	}

	/**
	 * Gets the client user with password by user id.
	 *
	 * @param userId the user id
	 * @return the client user with password by user id
	 * @throws Exception the exception
	 */
	public static ClientUserDVO getClientUserWithPasswordByUserId(String userId) throws Exception {

		String linkServer = DatabaseConnections.getSaasClientRegLinkedServer() + ".dbo.";

		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		ClientUserDVO clientUser = null;
		StringBuffer sql = new StringBuffer("SELECT c.clintid, c.userid, c.useremail, c.creby, c.credate, c.isactive, c.istestuser, c.updby, c.upddate, c.userfname, ");
		sql.append("	c.userlname, c.userphone,c.userrole,cp.userpwd" + "  from " + linkServer + "client_user c ");
		sql.append("  inner join " + linkServer);
		sql.append("client_user_pwd cp on cp.userid=c.userid and cp.clintid=c.clintid where c.userid=?");
		try {
			conn = DatabaseConnections.getSaasClientDetailsConnection();

			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, userId);
			rs = ps.executeQuery();
			while (rs.next()) {
				clientUser = new ClientUserDVO();
				clientUser.setClintid(rs.getString("clintid"));
				clientUser.setUserid(rs.getString("userid"));
				clientUser.setUseremail(rs.getString("useremail"));
				clientUser.setUserfname(rs.getString("userfname"));
				clientUser.setUserlname(rs.getString("userlname"));
				clientUser.setUserphone(rs.getString("userphone"));
				clientUser.setUserrole(rs.getString("userrole"));
				clientUser.setCreby(rs.getString("creby"));
				clientUser.setUpdby(rs.getString("updby"));
				clientUser.setIsactive(rs.getString("isactive"));
				clientUser.setIstestuser(rs.getBoolean("istestuser"));

				clientUser.setPassword(rs.getString("userpwd"));
				if (rs.getTimestamp("credate") != null) {
					clientUser.setCredate(new Date(rs.getTimestamp("credate").getTime()));
				}
				if (rs.getTimestamp("upddate") != null) {
					clientUser.setUpddate(new Date(rs.getTimestamp("upddate").getTime()));
				}
			}
			return clientUser;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;

	}

	/**
	 * Save client U ser.
	 *
	 * @param clientId the client id
	 * @param obj      the obj
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer saveClientUSer(String clientId, ClientUserDVO obj) throws Exception {
		Integer id = null;
		String linkServer = DatabaseConnections.getSaasClientRegLinkedServer() + ".dbo.";

		StringBuffer sql = new StringBuffer("insert into " + linkServer);
		sql.append("client_user(clintid, userid, useremail,userfname,  userlname, userphone, userrole, isactive, ");
		sql.append(" creby, credate, istestuser) " + "values(?,?,?,?,?,?,?,?,?,?,?)");

		try {
			Connection conn = DatabaseConnections.getSaasClientDetailsConnection();
			PreparedStatement statement = conn.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, obj.getClintid());
			statement.setString(2, obj.getUserid());
			statement.setString(3, obj.getUseremail());
			statement.setString(4, obj.getUserfname());
			statement.setString(5, obj.getUserlname());
			statement.setString(6, obj.getUserphone());
			statement.setString(7, obj.getUserrole());
			statement.setString(8, obj.getIsactive());
			statement.setString(9, obj.getCreby());
			statement.setTimestamp(10, new Timestamp(obj.getCredate().getTime()));
			statement.setBoolean(11, obj.getIstestuser());

			int affectedRows = statement.executeUpdate();
			if (affectedRows == 0) {
				throw new SQLException("Creating client user record failed, no rows affected.");
			}
			ResultSet generatedKeys;
			try {
				generatedKeys = statement.getGeneratedKeys();
				if (generatedKeys.next()) {
					id = generatedKeys.getInt(1);

				}
				generatedKeys.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			statement.close();
			DatabaseConnections.closeConnection(conn);
		} catch (SQLException se) {
			se.printStackTrace();
			throw se;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return id;

	}

	/**
	 * Update client user data.
	 *
	 * @param clientId the client id
	 * @param obj      the obj
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer updateClientUserData(String clientId, ClientUserDVO obj) throws Exception {
		Integer affectedRows = null;
		String linkServer = DatabaseConnections.getSaasClientRegLinkedServer() + ".dbo.";

		StringBuffer sql = new StringBuffer("update " + linkServer);
		sql.append("client_user set userfname =?, userlname=?, userphone=?, userrole=?, isactive=?, ");
		sql.append(" istestuser=?, updby=?, upddate=? where clintid=? and userid=?");

		try {
			Connection conn = DatabaseConnections.getSaasClientDetailsConnection();
			PreparedStatement statement = conn.prepareStatement(sql.toString());
			statement.setString(1, obj.getUserfname());
			statement.setString(2, obj.getUserlname());
			statement.setString(3, obj.getUserphone());
			statement.setString(4, obj.getUserrole());
			statement.setString(5, obj.getIsactive());
			statement.setBoolean(6, obj.getIstestuser());
			statement.setString(7, obj.getUpdby());
			statement.setTimestamp(8, new Timestamp(obj.getUpddate().getTime()));
			statement.setString(9, obj.getClintid());
			statement.setString(10, obj.getUserid());

			affectedRows = statement.executeUpdate();
			if (affectedRows == 0) {
				throw new SQLException("updating client user record failed, no rows affected.");
			}
			statement.close();
			DatabaseConnections.closeConnection(conn);
		} catch (SQLException se) {
			se.printStackTrace();
			throw se;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return affectedRows;

	}

	/**
	 * Gets the all users by client idand filter.
	 *
	 * @param clientId    the client id
	 * @param filterQuery the filter query
	 * @param pgNo        the pg no
	 * @return the all users by client idand filter
	 */
	public static List<ClientUserDVO> getAllUsersByClientIdandFilter(String clientId, String filterQuery, String pgNo,String filter) {

		String linkServer = DatabaseConnections.getSaasClientRegLinkedServer() + ".dbo.";
		StringBuffer sql = new StringBuffer("SELECT * from " + linkServer + "client_user c where  c.clintid='" + clientId + "' ");

		String paginationQuery = PaginationUtil.getPaginationQuery(pgNo);
		if (filterQuery != null && !filterQuery.isEmpty()) {
			sql.append(filterQuery);
		}
		String sortingSql = GridSortingUtil.getUserSortingQuery(filter);
		if(sortingSql!=null && !sortingSql.trim().isEmpty()){
			sql.append(sortingSql);
		}else{
			sql.append("  order by userid ");
		}
		
		sql.append(paginationQuery);

		List<ClientUserDVO> list = new ArrayList<ClientUserDVO>();
		try {
			Connection conn = DatabaseConnections.getSaasClientDetailsConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql.toString());

			ClientUserDVO clientUser = null;
			while (rs.next()) {
				clientUser = new ClientUserDVO();
				clientUser.setClintid(rs.getString("clintid"));
				clientUser.setUserid(rs.getString("userid"));
				clientUser.setUseremail(rs.getString("useremail"));
				clientUser.setUserfname(rs.getString("userfname"));
				clientUser.setUserlname(rs.getString("userlname"));
				clientUser.setUserphone(rs.getString("userphone"));
				clientUser.setUserrole(rs.getString("userrole"));
				clientUser.setCreby(rs.getString("creby"));
				clientUser.setUpdby(rs.getString("updby"));
				clientUser.setIsactive(rs.getString("isactive"));
				clientUser.setIstestuser(rs.getBoolean("istestuser"));

				if (rs.getTimestamp("credate") != null) {
					clientUser.setCredate(new Date(rs.getTimestamp("credate").getTime()));
				}
				if (rs.getTimestamp("upddate") != null) {
					clientUser.setUpddate(new Date(rs.getTimestamp("upddate").getTime()));
				}

				list.add(clientUser);
			}
			rs.close();
			stmt.close();
			DatabaseConnections.closeConnection(conn);
			return list;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Gets the all users data to export.
	 *
	 * @param clientId    the client id
	 * @param filterQuery the filter query
	 * @return the all users data to export
	 */
	public static List<ClientUserDVO> getAllUsersDataToExport(String clientId, String filterQuery) {

		String linkServer = DatabaseConnections.getSaasClientRegLinkedServer() + ".dbo.";
		StringBuffer sql = new StringBuffer("SELECT * from " + linkServer + "client_user c where  c.clintid='" + clientId + "' ");

		if (filterQuery != null && !filterQuery.isEmpty()) {
			sql.append(filterQuery);
		}
		sql.append("  order by userid ");

		List<ClientUserDVO> list = new ArrayList<ClientUserDVO>();
		try {
			Connection conn = DatabaseConnections.getSaasClientDetailsConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql.toString());

			ClientUserDVO clientUser = null;
			while (rs.next()) {
				clientUser = new ClientUserDVO();
				clientUser.setClintid(rs.getString("clintid"));
				clientUser.setUserid(rs.getString("userid"));
				clientUser.setUseremail(rs.getString("useremail"));
				clientUser.setUserfname(rs.getString("userfname"));
				clientUser.setUserlname(rs.getString("userlname"));
				clientUser.setUserphone(rs.getString("userphone"));
				clientUser.setUserrole(rs.getString("userrole"));
				clientUser.setCreby(rs.getString("creby"));
				clientUser.setUpdby(rs.getString("updby"));
				clientUser.setIsactive(rs.getString("isactive"));
				clientUser.setIstestuser(rs.getBoolean("istestuser"));

				if (rs.getTimestamp("credate") != null) {
					clientUser.setCredate(new Date(rs.getTimestamp("credate").getTime()));
				}
				if (rs.getTimestamp("upddate") != null) {
					clientUser.setUpddate(new Date(rs.getTimestamp("upddate").getTime()));
				}

				list.add(clientUser);
			}
			rs.close();
			stmt.close();
			DatabaseConnections.closeConnection(conn);
			return list;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Gets the all users count by client idand filter.
	 *
	 * @param clientId    the client id
	 * @param filterQuery the filter query
	 * @return the all users count by client idand filter
	 * @throws Exception the exception
	 */
	public static Integer getAllUsersCountByClientIdandFilter(String clientId, String filterQuery) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Integer count = 0;
		try {

			String linkServer = DatabaseConnections.getSaasClientRegLinkedServer() + ".dbo.";
			StringBuffer sql = new StringBuffer("SELECT count(*) from " + linkServer + "client_user c where  clintid='" + clientId + "' ");

			if (filterQuery != null && !filterQuery.isEmpty()) {
				sql.append(filterQuery);
			}
			conn = DatabaseConnections.getSaasClientDetailsConnection();
			ps = conn.prepareStatement(sql.toString());

			rs = ps.executeQuery();
			while (rs.next()) {
				count = rs.getInt(1);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return count;
	}

	/**
	 * Delete user.
	 *
	 * @param cliId the cli id
	 * @param uId   the u id
	 * @return true, if successful
	 * @throws Exception the exception
	 */
	public static boolean deleteUser(String cliId, String uId) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		Boolean isUpdated = false;
		String linkServer = DatabaseConnections.getSaasClientRegLinkedServer() + ".dbo.";
		try {
			conn = DatabaseConnections.getSaasClientDetailsConnection();
			StringBuffer sql = new StringBuffer("DELETE FROM  " + linkServer + "client_user WHERE clintid=? and userid=?");
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, cliId);
			ps.setString(2, uId);
			int affectedRows = ps.executeUpdate();
			if (affectedRows > 0) {
				isUpdated = true;
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return isUpdated;
	}

	/**
	 * Activate de activate user.
	 *
	 * @param cliId  the cli id
	 * @param uId    the u id
	 * @param status the status
	 * @return the boolean
	 */
	public static Boolean activateDeActivateUser(String cliId, String uId, String status) {
		Connection conn = null;
		PreparedStatement ps = null;
		Boolean isUpdated = false;
		String linkServer = DatabaseConnections.getSaasClientRegLinkedServer() + ".dbo.";
		try {
			conn = DatabaseConnections.getSaasClientDetailsConnection();
			StringBuffer sql = new StringBuffer("UPDATE " + linkServer + "client_user set isactive = ? where clintid=? and userid=?");
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, status);
			ps.setString(2, cliId);
			ps.setString(3, uId);
			int affectedRows = ps.executeUpdate();
			if (affectedRows > 0) {
				isUpdated = true;
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return isUpdated;
	}

}
