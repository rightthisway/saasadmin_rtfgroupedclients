/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.user.sql.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.rtf.saas.sql.db.DatabaseConnections;
import com.rtf.user.dvo.UserRoleDVO;

/**
 * The Class UserRoleDAO.
 */
public class UserRoleDAO {

	/**
	 * Gets the all user roles.
	 *
	 * @return the all user roles
	 * @throws Exception the exception
	 */
	/*
	 * @param Clientid
	 * 
	 * @return
	 * 
	 * @throws Exception
	 */
	public static List<UserRoleDVO> getAllUserRoles() throws Exception {

		String linkServer = DatabaseConnections.getSaasClientRegLinkedServer() + ".dbo.";
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		List<UserRoleDVO> list = new ArrayList<UserRoleDVO>();
		UserRoleDVO userRole = null;
		StringBuffer sql = new StringBuffer("select rolename from " + linkServer + "sd_user_role where isactive=1");
		try {
			conn = DatabaseConnections.getSaasClientDetailsConnection();

			ps = conn.prepareStatement(sql.toString());
			rs = ps.executeQuery();
			while (rs.next()) {
				userRole = new UserRoleDVO();
				userRole.setName(rs.getString("rolename"));
				userRole.setValue(rs.getString("rolename"));
				list.add(userRole);
			}
			return list;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;

	}
}
