/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.exception;

/**
 * The Class RTFDataAccessException.
 */
public class RTFDataAccessException extends Exception {
	
	private static final long serialVersionUID = -978951182807261233L;

	/**
	 * Instantiates a new RTF data access exception.
	 */
	public RTFDataAccessException() {		
	}
	
	/**
	 * Instantiates a new RTF data access exception.
	 *
	 * @param msg the msg
	 * @param ex the ex
	 */
	public RTFDataAccessException(String msg,Exception ex) 	{	
		 // Do something with Exception
	}
	
	public RTFDataAccessException(String msg) {		
	}
	
	public RTFDataAccessException(Exception ex) {		
	}

}
