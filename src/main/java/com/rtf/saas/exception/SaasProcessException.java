/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.exception;

/**
 * The Class ContestConfigException.
 */
public class SaasProcessException extends Exception {

	/**
	 *
	 */
	private static final long serialVersionUID = 5012642588925719222L;

	/**
	 * Instantiates a new contest config exception.
	 */
	public SaasProcessException() {
	}

	/**
	 * Instantiates a new contest config exception.
	 *
	 * @param msg the msg
	 */
	public SaasProcessException(String msg) {
		super(msg);
		// TODO Write to DB Tracker

	}

	/**
	 * Instantiates a new contest config exception.
	 *
	 * @param msg the msg
	 * @param ex  the ex
	 */
	public SaasProcessException(String msg, Exception ex) {
		super(msg);
		// TODO Write to DB Tracker
		ex.printStackTrace();
	}
}
