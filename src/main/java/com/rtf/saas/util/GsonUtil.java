/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.util;

import org.codehaus.jackson.map.ObjectMapper;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * The Class GsonUtil.
 */
public class GsonUtil {

	/** The gson. */
	static Gson gson = null;

	/**
	 * Gets the gson instance.
	 *
	 * @return the gson instance
	 */
	public static Gson getGsonInstance() {

		if (gson == null) {
			// gson = new GsonBuilder().setPrettyPrinting().create();
			gson = new GsonBuilder().serializeNulls().create();

		}
		return gson;

	}

	/** The mapper. */
	static ObjectMapper mapper = new ObjectMapper();

	/**
	 * Gets the jaskson obj mapper.
	 *
	 * @return the jaskson obj mapper
	 */
	public static ObjectMapper getJasksonObjMapper() {
		if (mapper == null) {
			mapper = new ObjectMapper();
		}
		return mapper;
	}

}
