/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.util;

import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * The Class NumberFormatUtil.
 */
public class NumberFormatUtil {

	/** The number suffix. */
	private static String NUMBER_SUFFIX = " KMBT";

	/**
	 * Format number to words.
	 *
	 * @param number the number
	 * @return the string
	 */
	public static String formatNumberToWords(Integer number) {
		String numStr = "0";
		try {
			if (number == null || number == 0) {
				return numStr;
			}
			Double num = Double.parseDouble(String.valueOf(number));
			NumberFormat formatter = new DecimalFormat("####.#");
			int power = (int) StrictMath.log10(num);
			num = num / (Math.pow(10, (power / 3) * 3));

			numStr = formatter.format(num);
			numStr = numStr + NUMBER_SUFFIX.charAt(power / 3);
			numStr = numStr.length() > 4 ? numStr.replaceAll("\\.[0-9]+", "") : numStr;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return numStr;
	}

}
