/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ResourceBundle;

/**
 * The Class GenUtil.
 */
public class GenUtil {

	/**
	 * Gets the exception as string.
	 *
	 * @param ex the ex
	 * @return the exception as string
	 */
	public static String getExceptionAsString(Exception ex) {
		StringWriter sw = new StringWriter();
		ex.printStackTrace(new PrintWriter(sw));
		return sw.toString();
	}

	/**
	 * Current time millis.
	 *
	 * @return the long
	 */
	public static long currentTimeMillis() {
		return System.currentTimeMillis();
	}

	/**
	 * Checks if is empty string.
	 *
	 * @param string the string
	 * @return true, if is empty string
	 */
	public static boolean isEmptyString(String string) {
		return string == null || string.length() == 0;
	}

	/**
	 * Checks if is null or empty.
	 *
	 * @param s the s
	 * @return true, if is null or empty
	 */
	public static boolean isNullOrEmpty(String s) {
		if (s == null || s.isEmpty() || isNullOrWhiteSpace(s)) {
			return true;
		}
		return false;
	}

	/**
	 * Checks if is null or white space.
	 *
	 * @param s the s
	 * @return true, if is null or white space
	 */
	public static boolean isNullOrWhiteSpace(String s) {
		if (s == null || s.trim().isEmpty()) {
			return true;
		}
		return false;
	}

	/**
	 * Gets the resource key.
	 *
	 * @param key the key
	 * @return the resource key
	 */
	public static String getResourceKey(String key) {
		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		return resourceBundle.getString(key);
	}

	/**
	 * Gets the active bit flag.
	 *
	 * @param activeStr the active str
	 * @return the active bit flag
	 */
	public static Boolean getActiveBitFlag(String activeStr) {
		Boolean activeFlag = Boolean.FALSE;
		if (isNullOrEmpty(activeStr)) {
			return activeFlag;
		}
		if ("TRUE".equalsIgnoreCase(activeStr)) {
			return Boolean.TRUE;
		}
		if ("ACTIVE".equalsIgnoreCase(activeStr)) {
			return Boolean.TRUE;
		}
		if ("1".equals(activeStr)) {
			return Boolean.TRUE;
		}
		return activeFlag;
	}

	/**
	 * Gets the current time stamp.
	 *
	 * @return the current time stamp
	 */
	public static java.sql.Timestamp getCurrentTimeStamp() {
		java.util.Date today = new java.util.Date();
		return new java.sql.Timestamp(today.getTime());
	}

	/**
	 * Gets the empty string if null.
	 *
	 * @param string the string
	 * @return the empty string if null
	 */
	public static String getEmptyStringIfNull(String string) {
		if (isNullOrEmpty(string) || "null".equalsIgnoreCase(string)) {
			return "";
		}
		return string;
	}

}
