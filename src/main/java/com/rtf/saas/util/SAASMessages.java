/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.util;

/**
 * The Class SAASMessages.
 */
public class SAASMessages {

	/** The Constant GEN_ERR_MSG. */
	public static final String GEN_ERR_MSG = "Something went wrong. Please try again";

	/** The Constant GEN_SUCCESS_MSG. */
	public static final String GEN_SUCCESS_MSG = "Success";

	/** The Constant ADMIN_QB_NOQUESTIONS. */
	public static final String ADMIN_QB_NOQUESTIONS = "No Questions configured";

	/** The Constant ADMIN_QB_NOCATEGORY. */
	public static final String ADMIN_QB_NOCATEGORY = "No Categories configured";

	/** The Constant ADMIN_QB_NOREWARDS. */
	public static final String ADMIN_QB_NOREWARDS = "No Rewards configured";

	/** The Constant ADMIN_QB_NOSUBCATEGORY. */
	public static final String ADMIN_QB_NOSUBCATEGORY = "No Sub Categories configured";

	/** The Constant GEN_UPDATE_SUCCESS_MSG. */
	public static final String GEN_UPDATE_SUCCESS_MSG = "Record Updated Successfully  ";

	/** The Constant GEN_DELETE_SUCCESS_MSG. */
	public static final String GEN_DELETE_SUCCESS_MSG = "Record Deleted Successfully  ";

	/** The Constant RWD_ICON_FILE_UPLOAD_MANDATORY. */
	public static final String RWD_ICON_FILE_UPLOAD_MANDATORY = "Please Upload the Reward Icon file ";

	/** The Constant QBANK_NAME_EXIST. */
	public static final String QBANK_NAME_EXIST = "Question Bank Name exists.Please choose a different name";

	/** The Constant QBANK_NO_RECORDS. */
	public static final String QBANK_NO_RECORDS = "No Question Bank Records";

	/** The Constant SHOPPRODUCT_IMAGE_FILE_UPLOAD_MANDATORY. */
	public static final String SHOPPRODUCT_IMAGE_FILE_UPLOAD_MANDATORY = "Please Upload the Product Image ";
	
	public static String INVALID_USER_NAME = "Invalid User Name.";

	public static String INVALID_PASSWORD = "Invalid Password.";

	public static String INVALID_IP_ADDRESS = "Invalid Ip address.";

	public static String USER_ID_NOT_EXISTS = "UserID not exists";

	public static String USER_ID_OR_PASSWORD_MISMATCH = "Userid or Password is mismatched.";

	public static String INVALID_PRODUCT_ID = "Invalid product id.";

	public static String INVALID_KEY = "Invalid key.";

	public static String INVALID_KEY_VALUE = "Invalid keyvalue.";

	public static String FIRESTORE_CONFIG_NOT_FOUND = "Firestore configuration settings not found in db.";
	
	public static String INVALID_VALUE_FOR_QUESTION_TIMER = "Invalid value for live trivia question timer.";
	
	public static String INVALID_VALUE_FOR_ANSWER_TIMER = "Invalid value for live trivia answer timer.";

	public static String CHAT_ON_VALUE = "ON";

	public static String SELECT_KEY_VALUE_UPDATE_SUCCESS_MSG = "Selected key-value updated successfully";

}
