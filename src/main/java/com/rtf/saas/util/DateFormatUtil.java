/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;

/**
 * The Class DateFormatUtil.
 */
public class DateFormatUtil {

	/** The dt. */
	private static SimpleDateFormat dt = new SimpleDateFormat("MM/dd/yy");

	/** The time ft. */
	private static SimpleDateFormat timeFt = new SimpleDateFormat("hh:mmaa z");

	/**
	 * Gets the date MMDDYYYYYH hmmss.
	 *
	 * @param date the date
	 * @return the date MMDDYYYYYH hmmss
	 */
	public static String getDateMMDDYYYYYHHmmss(Date date) {
		String formatedDate = " ";

		if (date == null)
			return formatedDate;
		if (date.getTime() == -10)
			return formatedDate;

		try {

			SimpleDateFormat simpDate = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			formatedDate = simpDate.format(date);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return formatedDate;

	}

	/**
	 * Format date to month date year.
	 *
	 * @param date the date
	 * @return the string
	 */
	public static String formatDateToMonthDateYear(Date date) {
		String result = "";
		DateFormat formatDate = new SimpleDateFormat("MM/dd/yyyy");
		try {
			if (date != null) {
				result = formatDate.format(date);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * Gets the date with twenty four hour format.
	 *
	 * @param datetime the datetime
	 * @return the date with twenty four hour format
	 */
	public static Date getDateWithTwentyFourHourFormat(String datetime) {
		Date result = null;
		DateFormat formatDateTime = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		try {
			if (datetime != null) {
				result = formatDateTime.parse(datetime);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * Extract date element.
	 *
	 * @param input  the input
	 * @param format the format
	 * @return the integer
	 */
	public static Integer extractDateElement(String input, String format) {
		try {
			if (input == null || input.isEmpty() || format == null || format.isEmpty()) {
				return -1;
			}
			if (format.equals("DAY")) {
				if (input.contains("/")) {
					String arr[] = input.split("/");
					if (arr.length > 1) {
						return Integer.parseInt(arr[1].isEmpty() ? "-1" : arr[1]);
					}
				}
				return -1;
			} else if (format.equals("MONTH")) {
				if (input.contains("/")) {
					String arr[] = input.split("/");
					if (arr.length > 0) {
						return Integer.parseInt(arr[0].isEmpty() ? "-1" : arr[0]);
					}
				} else {
					return Integer.parseInt(input);
				}
				return -1;
			} else if (format.equals("YEAR")) {
				if (input.contains("/")) {
					String arr[] = input.split("/");
					if (arr.length > 2) {
						return Integer.parseInt(arr[2].isEmpty() ? "-1" : arr[2]);
					}
				}
				return -1;
			} else if (format.equals("HOUR")) {
				String amPm = "";
				if (input.contains(":")) {
					String arr[];
					if (input.contains(" ")) {
						arr = input.split(" ");
						amPm = arr[1];
					}
					arr = input.split(":");
					if (arr.length > 0) {
						if (amPm.equalsIgnoreCase("pm")) {
							return Integer.parseInt(arr[0].isEmpty() ? "-1" : arr[0]) + 12;
						}
						return Integer.parseInt(arr[0].isEmpty() ? "-1" : arr[0]);
					}
				} else {
					return Integer.parseInt(input);
				}
				return -1;
			} else if (format.equals("MINUTE")) {
				if (input.contains(":")) {
					if (input.contains(" ")) {
						input = input.substring(0, input.indexOf(" "));
					}
					String[] arr = input.split(":");
					if (arr.length > 1) {
						return Integer.parseInt(arr[1].isEmpty() ? "-1" : arr[1]);
					}
				}
				return -1;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return -1;
	}

	/**
	 * Gets the next contest start time msg.
	 *
	 * @param startDate the start date
	 * @return the next contest start time msg
	 */
	public static String getNextContestStartTimeMsg(Date startDate) {
		String contestStartTime = "";
		try {
			Date today = dt.parse(dt.format(new Date()));
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DAY_OF_MONTH, 1);
			Date tomorrow = dt.parse(dt.format(new Date(cal.getTimeInMillis())));
			Date contestDate = dt.parse(dt.format(startDate));
			if (contestDate.compareTo(today) == 0) {
				contestStartTime = "Today " + timeFt.format(startDate);
				return contestStartTime;
			} else if (contestDate.compareTo(tomorrow) == 0) {
				contestStartTime = "Tomorrow " + timeFt.format(startDate);
				return contestStartTime;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		contestStartTime = dt.format(startDate) + " " + timeFt.format(startDate);
		return contestStartTime;
	}
	
	
	
	
	public static String getISOTimeStamp(){
		String nowAsISO = "";
		try {
			nowAsISO = Instant.now().toString() ;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return nowAsISO;
		
	}
	
	
	public static void main(String[] args) {
		System.out.println(getISOTimeStamp());
	}

}
