/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.util;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.Part;

/**
 * The Class FileUtil.
 */
public class FileUtil {

	/**
	 * Gets the file extension.
	 *
	 * @param f the f
	 * @return the file extension
	 */
	public static String getFileExtension(String f) {
		String ext = "";
		int i = f.lastIndexOf('.');
		if (i > 0 && i < f.length() - 1) {
			ext = f.substring(i + 1);
		}
		return ext;
	}

	/**
	 * Gets the opt value file name map.
	 *
	 * @param part the part
	 * @return the opt value file name map
	 */
	public static Map<String, String> getOptValueFileNameMap(Part part) {
		String contentDisp = part.getHeader("content-disposition");
		Map<String, String> fileNameOptionvalueMap = new HashMap<String, String>();
		String[] tokens = contentDisp.split(";");
		String fileName = null;
		String optvalue = null;
		for (String token : tokens) {
			if (token.trim().startsWith("filename")) {
				fileName = token.substring(token.indexOf("=") + 2, token.length() - 1);
			}
			if (token.trim().startsWith("name")) {
				optvalue = token.substring(token.indexOf("=") + 2, token.length() - 1);
			}
		}
		if (!GenUtil.isEmptyString(fileName)) {
			fileNameOptionvalueMap.put(optvalue, fileName);
		} else {
			fileNameOptionvalueMap = null;
		}
		return fileNameOptionvalueMap;
	}

	/**
	 * Gets the file name.
	 *
	 * @param part the part
	 * @return the file name
	 */
	public static String getFileName(Part part) {
		String contentDisp = part.getHeader("content-disposition");
		String[] tokens = contentDisp.split(";");
		for (String token : tokens) {
			if (token.trim().startsWith("filename")) {
				return token.substring(token.indexOf("=") + 2, token.length() - 1);
			}
		}
		return "";
	}

}
