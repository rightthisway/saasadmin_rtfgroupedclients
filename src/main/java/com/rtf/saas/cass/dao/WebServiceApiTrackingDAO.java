/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.cass.dao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.exceptions.DriverException;
import com.rtf.saas.cass.db.CassandraConnector;
import com.rtf.saas.exception.RTFDataAccessException;
import com.rtf.saas.sql.dvo.WebServiceApiTrackingDVO;
import com.rtf.saas.util.GenUtil;

/**
 * The Class WebServiceApiTrackingDAO.
 */
public class WebServiceApiTrackingDAO {

	/**
	 * Gets the all web service api tracking records.
	 *
	 * @return the all web service api tracking records
	 * @throws Exception the exception
	 */
	public static List<WebServiceApiTrackingDVO> getAllWebServiceApiTrackingRecords() throws Exception {

		ResultSet resultSet = null;
		WebServiceApiTrackingDVO track = null;
		List<WebServiceApiTrackingDVO> trackings = new ArrayList<WebServiceApiTrackingDVO>();
		try {
			resultSet = CassandraConnector.getSession().execute("SELECT * from  saas_prod_web_service_cust_track_cass");
			if (resultSet != null) {
				for (Row rs : resultSet) {
					track = new WebServiceApiTrackingDVO();
					Date date = null;
					track.setActresult(rs.getString("actresult"));
					date = rs.getTimestamp("apiendtime");
					track.setApiendtime(date != null ? new Timestamp(date.getTime()) : null);
					track.setApiname(rs.getString("apiname"));
					date = rs.getTimestamp("apistarttime");
					track.setApistarttime(date != null ? new Timestamp(date.getTime()) : null);
					track.setAppver(rs.getString("appver"));
					track.setClintid(rs.getString("clintid"));
					track.setConid(rs.getString("conid"));
					track.setCustid(rs.getString("custid"));
					track.setCustipaddr(rs.getString("custipaddr"));
					track.setDescription(rs.getString("description"));
					date = rs.getTimestamp("deviceapitime");
					track.setDeviceapitime(date != null ? new Timestamp(date.getTime()) : null);
					track.setDeviceinfo(rs.getString("deviceinfo"));
					track.setFbcallbacktime(rs.getTimestamp("fbcallbacktime"));
					date = rs.getTimestamp("hitdate");
					track.setHitdate(date != null ? new Timestamp(date.getTime()) : null);
					track.setNodeid(rs.getInt("nodeid"));
					track.setPlatform(rs.getString("platform"));
					track.setProdcode(rs.getString("prodcode"));
					track.setResstatus(rs.getInt("resstatus"));
					track.setSessionid(rs.getString("sessionid"));
					trackings.add(track);
				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		} finally {
		}
		return trackings;
	}

	/**
	 * Delete web service api tracking record.
	 *
	 * @param dvo the dvo
	 * @return true, if successful
	 * @throws Exception the exception
	 */
	public static boolean deleteWebServiceApiTrackingRecord(WebServiceApiTrackingDVO dvo) throws Exception {
		boolean isUpdated = false;
		StringBuffer sql = new StringBuffer("delete from  saas_prod_web_service_cust_track_cass   WHERE clintid=? AND custid=? AND apiname=? AND hitdate=?");
		try {
			CassandraConnector.getSession().execute(sql.toString(),
					new Object[] { dvo.getClintid(), dvo.getCustid(), dvo.getApiname(), dvo.getHitdate() });
			isUpdated = true;
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		} 
		return isUpdated;
	}

}
