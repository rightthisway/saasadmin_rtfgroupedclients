/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.cass.dao;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.exceptions.DriverException;
import com.rtf.saas.cass.db.CassandraConnector;
import com.rtf.saas.exception.RTFDataAccessException;
import com.rtf.saas.sql.dvo.ClientConfigDVO;
import com.rtf.saas.util.GenUtil;

/**
 * The Class ClientConfigCassDAO.
 */
public class ClientConfigCassDAO {

	/**
	 * Gets the contest config by key.
	 *
	 * @param clId   the client id
	 * @param prodId the prod id
	 * @param key    the key
	 * @return the contest config by key
	 * @throws Exception the exception
	 */
	public static ClientConfigDVO getContestConfigByKey(String clId, String prodId, String key) throws Exception {
		ResultSet resultSet = null;
		ClientConfigDVO config = null;
		try {
			StringBuffer sql = new StringBuffer("SELECT * from  pa_commn_client_config  WHERE clintid=? AND prodrkeytype=? AND keyid = ?");
			resultSet = CassandraConnector.getSession().execute(sql.toString(), clId,prodId, key);
			if (resultSet != null) {
				for (Row row : resultSet) {
					config = new ClientConfigDVO();
					config.setKey(row.getString("keyid"));
					config.setValue(row.getString("keyvalue"));
				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
		return config;
	}

	/**
	 * Update contest config.
	 *
	 * @param config the config
	 * @return true, if successful
	 * @throws Exception the exception
	 */
	public static boolean updateContestConfig(ClientConfigDVO config) throws Exception {
		Boolean isInserted = false;
		try {
			StringBuffer sql = new StringBuffer("update pa_commn_client_config  set keyvalue=?,updby=?,upddate=toTimestamp(now())  WHERE clintid = ? AND prodrkeytype=? AND keyid =?");
			CassandraConnector.getSession().execute(sql.toString(), new Object[] { config.getValue(), config.getuBy(),
					config.getClId(), config.getProdId(), config.getKey() });
			isInserted = true;
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
		return isInserted;
	}
}
