/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.cass.db;


import java.util.ResourceBundle;

import com.datastax.driver.core.AuthProvider;
import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ConsistencyLevel;
import com.datastax.driver.core.HostDistance;
import com.datastax.driver.core.PlainTextAuthProvider;
import com.datastax.driver.core.PoolingOptions;
import com.datastax.driver.core.QueryOptions;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.policies.LoadBalancingPolicy;

 
/**
 * The Class CassandraConnector.
 */
public class CassandraConnector  {
	
	/** The cluster. */
	private static Cluster cluster;
	
	/** The session. */
	private static Session session;
	
	private static  String node;
	private static  int port;
	private static  String keySpace;
	private static  String userName;
	private static  String password;
	
	private static  String saasPlayKeySpace;
	
	
/** The is session created. */
private static Boolean isSessionCreated= false;
	
	/**
	 * Gets the rtf sass cass key space.
	 *
	 * @return the rtf sass cass key space
	 */
	public static String getRtfSassCassKeySpace() {
		return saasPlayKeySpace;
	}
	 
	/**
	 * Load application values.
	 *
	 * @throws Exception the exception
	 */
	public static void loadApplicationValues() throws Exception {
		try {
			ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
			node =resourceBundle.getString("saas.play.cassandra.db.host.ip");
			port = Integer.parseInt(resourceBundle.getString("saas.play.cassandra.db.port").trim());
			keySpace =resourceBundle.getString("saas.play.cassandra.db.keySpace");
			userName = resourceBundle.getString("saas.play.cassandra.db.user.name");
			password = resourceBundle.getString("saas.play.cassandra.db.password");
			
			saasPlayKeySpace = resourceBundle.getString("saas.play.cassandra.keyspace");
			connect();
			
		}catch(Exception e) {
			e.printStackTrace();
			throw e;
		}
		
	}
	
	/** The load balancing policy. */
	static LoadBalancingPolicy loadBalancingPolicy;
	
	/** The pooling options. */
	static  PoolingOptions poolingOptions;
	
	/** The is with pooling. */
	static Boolean isWithPooling = false;
			
	/**
	 * Connect.
	 *
	 * @throws Exception the exception
	 */
	public  static void connect() throws Exception {
		
		
		PoolingOptions tempPoolingOptions = new PoolingOptions();
		tempPoolingOptions
		  .setMaxRequestsPerConnection(HostDistance.LOCAL, 32768)
		  .setMaxRequestsPerConnection(HostDistance.REMOTE, 32000);
		
		tempPoolingOptions
	    .setConnectionsPerHost(HostDistance.LOCAL,  4, 10)
	    .setConnectionsPerHost(HostDistance.REMOTE, 2, 4);
		 
		
		cluster = Cluster.builder().addContactPoint(node).withPort(port)
				.withCredentials(userName, password).withPoolingOptions(tempPoolingOptions)
				.build();
		session = cluster.connect(keySpace);
		
		loadBalancingPolicy =
			    cluster.getConfiguration().getPolicies().getLoadBalancingPolicy();
		poolingOptions =
				cluster.getConfiguration().getPoolingOptions();
		isWithPooling = true;
	}
 
	/**
	 * Connect with clusters.
	 *
	 * @throws Exception the exception
	 */
	public  static void connectWithClusters() throws Exception {
		
		PoolingOptions tempPoolingOptions = new PoolingOptions();
		tempPoolingOptions
						.setMaxRequestsPerConnection(HostDistance.LOCAL, 32768)
						.setMaxRequestsPerConnection(HostDistance.REMOTE, 32000);
		
		tempPoolingOptions
	    .setConnectionsPerHost(HostDistance.LOCAL,  4, 10)
	    .setConnectionsPerHost(HostDistance.REMOTE, 2, 4);
		
		QueryOptions qo = new QueryOptions().setConsistencyLevel(ConsistencyLevel.ALL);

		AuthProvider authProvider = new PlainTextAuthProvider(userName, password);
		cluster = Cluster.builder()
			       .addContactPoints(node)
			       .withPort(port)
			       .withAuthProvider(authProvider)
			       .withPoolingOptions(poolingOptions)
			       .withQueryOptions(qo)
			       .build();
		
		session = cluster.connect(keySpace);
		
		loadBalancingPolicy =
			    cluster.getConfiguration().getPolicies().getLoadBalancingPolicy();
		poolingOptions =
				cluster.getConfiguration().getPoolingOptions();
		isWithPooling = true;
	}
		
	/**
	 * Gets the session.
	 *
	 * @return the session
	 */
	public static Session getSession() {
		try {
			if(!isSessionCreated) {
				connect();
				isSessionCreated = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return session;
	}
	
	/**
 	 * Close.
 	 */
 	public void close() {
		cluster.close();
	}

	/**
	 * Gets the key space.
	 *
	 * @return the key space
	 */
	public static String getKeySpace() {
		return keySpace;
	}
	
}