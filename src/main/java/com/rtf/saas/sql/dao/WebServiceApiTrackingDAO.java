/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.sql.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.rtf.saas.sql.db.DatabaseConnections;
import com.rtf.saas.sql.dvo.LiveContestPlayTrackingDVO;
import com.rtf.saas.sql.dvo.WebServiceApiTrackingDVO;

/**
 * The Class WebServiceApiTrackingDAO.
 */
public class WebServiceApiTrackingDAO {

	/**
	 * Save contest tracking.
	 *
	 * @param dvo
	 *            the dvo
	 * @throws Exception
	 *             the exception
	 */
	public static void saveContestTracking(LiveContestPlayTrackingDVO dvo) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		StringBuffer sql = new StringBuffer();
		sql.append("INSERT INTO saas_prod_admin_livvx_play_contest_track (clintid,custid,conid,apiname,")
				.append("action,ipaddress,platform,qsnno,description,respstatus) VALUES")
				.append("(?,?,?,?,?,?,?,?,?,?)");

		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, dvo.getClintid());
			ps.setString(2, dvo.getUserName());
			ps.setString(3, dvo.getConid());
			ps.setString(4, dvo.getApiname());
			ps.setString(5, dvo.getAction());
			ps.setString(6, dvo.getIpAddress());
			ps.setString(7, dvo.getPlatform());
			ps.setInt(8, dvo.getqNo());
			ps.setString(9, dvo.getDescription());
			ps.setInt(10, dvo.getResstatus());
			ps.executeUpdate();
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	/**
	 * Save web service api tracking.
	 *
	 * @param dvo
	 *            the dvo
	 * @throws Exception
	 *             the exception
	 */
	public static void saveWebServiceApiTracking(WebServiceApiTrackingDVO dvo) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		StringBuffer sql = new StringBuffer();
		sql.append(" insert into saas_prod_web_service_cust_track_cass ").append(" (clintid,custid, apiname ,   ")
				.append(" hitdate , apistarttime , apiendtime , ")
				.append(" actresult , custipaddr , conid , platform ,").append(" prodcode , description , sessionid  ")
				.append(" ,resstatus ,nodeid,deviceinfo ) ").append(" values (?,?,?,?,?,?, ?,?,?,?, ?,?,?, ?,?,? )");

		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, dvo.getClintid());
			ps.setString(2, dvo.getCustid());
			ps.setString(3, dvo.getApiname());
			ps.setTimestamp(4, dvo.getHitdate());
			ps.setTimestamp(5, dvo.getApistarttime());
			ps.setTimestamp(6, dvo.getApiendtime());
			ps.setString(7, dvo.getActresult());
			ps.setString(8, dvo.getCustipaddr());
			ps.setString(9, dvo.getConid());
			ps.setString(10, dvo.getPlatform());
			ps.setString(11, dvo.getProdcode());
			ps.setString(12, dvo.getDescription());
			ps.setString(13, dvo.getSessionid());
			ps.setInt(14, dvo.getResstatus());
			ps.setInt(15, dvo.getNodeid());
			ps.setString(16, dvo.getDeviceinfo());
			ps.executeUpdate();
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

}
