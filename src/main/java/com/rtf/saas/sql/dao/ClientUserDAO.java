/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.sql.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.rtf.ott.sql.dvo.CategoryDVO;
import com.rtf.saas.sql.db.DatabaseConnections;
import com.rtf.saas.sql.dvo.ClientUserDVO;

/**
 * The Class ClientUserDAO.
 */
public class ClientUserDAO {

	/**
	 * Gets the client user by user id.
	 *
	 * @param userId the user id
	 * @return the client user by user id
	 * @throws Exception the exception
	 */
	/*
	 * @param Clientid
	 * 
	 * @return
	 * 
	 * @throws Exception
	 */
	public static ClientUserDVO getClientUserByUserId(String userId) throws Exception {

		String linkServer = DatabaseConnections.getSaasClientRegLinkedServer() + ".dbo.";
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		ClientUserDVO clientUser = null;
		StringBuffer sql = new StringBuffer("SELECT clintid, userid, useremail, creby, credate, isactive, istestuser, updby, upddate, ");
		sql.append(" userfname, userlname, userphone,usertype from " + linkServer + "client_user where userid=?");
		try {
			conn = DatabaseConnections.getSaasAdminConnection();

			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, userId);
			rs = ps.executeQuery();
			while (rs.next()) {
				clientUser = new ClientUserDVO();
				clientUser.setClintid(rs.getString("clintid"));
				clientUser.setUserid(rs.getString("userid"));
				clientUser.setUseremail(rs.getString("useremail"));
				clientUser.setUserfname(rs.getString("userfname"));
				clientUser.setUserlname(rs.getString("userlname"));
				clientUser.setUserphone(rs.getString("userphone"));
				clientUser.setUsertype(rs.getString("usertype"));
				clientUser.setCredate(rs.getDate("credate"));
				clientUser.setCreby(rs.getString("creby"));
				clientUser.setUpddate(rs.getDate("upddate"));
				clientUser.setUpdby(rs.getString("updby"));
				clientUser.setIsactive(rs.getString("isactive"));
				clientUser.setIstestuser(rs.getBoolean("istestuser"));
			}
			return clientUser;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;

	}

	/**
	 * Gets the client user with password by user id.
	 *
	 * @param userId the user id
	 * @return the client user with password by user id
	 * @throws Exception the exception
	 */
	public static ClientUserDVO getClientUserWithPasswordByUserId(String userId) throws Exception {

		String linkServer = DatabaseConnections.getSaasClientRegLinkedServer() + ".dbo.";

		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		ClientUserDVO clientUser = null;
		StringBuffer sql = new StringBuffer("SELECT c.clintid, c.userid, c.useremail, c.creby, c.credate, c.isactive, c.istestuser, c.updby, c.upddate, c.userfname, ");
		sql.append("c.userlname, c.userphone,c.usertype,cp.userpwd  from " + linkServer + "client_user c ");
		sql.append("  inner join " + linkServer);
		sql.append("client_user_pwd cp on cp.userid=c.userid and cp.clintid=c.clintid where c.userid=?");
		try {
			conn = DatabaseConnections.getSaasAdminConnection();

			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, userId);
			rs = ps.executeQuery();
			while (rs.next()) {
				clientUser = new ClientUserDVO();
				clientUser.setClintid(rs.getString("clintid"));
				clientUser.setUserid(rs.getString("userid"));
				clientUser.setUseremail(rs.getString("useremail"));
				clientUser.setUserfname(rs.getString("userfname"));
				clientUser.setUserlname(rs.getString("userlname"));
				clientUser.setUserphone(rs.getString("userphone"));
				clientUser.setUsertype(rs.getString("usertype"));
				clientUser.setCreby(rs.getString("creby"));
				clientUser.setUpdby(rs.getString("updby"));
				clientUser.setIsactive(rs.getString("isactive"));
				clientUser.setIstestuser(rs.getBoolean("istestuser"));

				clientUser.setPassword(rs.getString("userpwd"));
				if (rs.getTimestamp("credate") != null) {
					clientUser.setCredate(new Date(rs.getTimestamp("credate").getTime()));
				}
				if (rs.getTimestamp("upddate") != null) {
					clientUser.setUpddate(new Date(rs.getTimestamp("upddate").getTime()));
				}
			}
			return clientUser;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;

	}

	/**
	 * Creates the category.
	 *
	 * @param userId   the user id
	 * @param clientid the clientid
	 * @param dvo      the dvo
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer createCategory(String userId, String clientid, CategoryDVO dvo) throws Exception {

		Connection conn = null;
		PreparedStatement ps = null;
		Integer affectedRows = 0;
		StringBuffer sql = new StringBuffer(" insert into  sd_category_type (clintid,cattype");
		sql.append(",catdesc,creby,isactive,credate,updby,upddate ) values(?,?,?,?,?,getdate(),?,getDate()) ");

		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientid);
			ps.setString(2, dvo.getCattype());
			ps.setString(3, dvo.getCatdesc());
			ps.setString(4, userId);
			ps.setBoolean(5, Boolean.TRUE);
			ps.setString(6, userId);
			affectedRows = ps.executeUpdate();
			return affectedRows;

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return affectedRows;
	}

}
