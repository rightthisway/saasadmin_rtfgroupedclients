/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.sql.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.rtf.saas.sql.db.DatabaseConnections;
import com.rtf.saas.sql.dvo.RewardTypeDVO;

/**
 * The Class RewardTypeDAO.
 */
public class RewardTypeDAO {

	/**
	 * Get all reward types.
	 *
	 * @param clId the cl id
	 * @return the list
	 * @throws Exception the exception
	 */
	public static List<RewardTypeDVO> geAllRewardTypes(String clId) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<RewardTypeDVO> rewards = new ArrayList<RewardTypeDVO>();
		RewardTypeDVO reward = null;
		StringBuffer sql = new StringBuffer("SELECT * from  sd_reward_type WHERE clintid=? AND isactive=? ORDER BY rwdtype");
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clId);
			ps.setBoolean(2, Boolean.TRUE);
			rs = ps.executeQuery();

			while (rs.next()) {
				Date date = null;
				reward = new RewardTypeDVO();
				reward.setClId(rs.getString("clintid"));
				reward.setType(rs.getString("rwdtype"));
				reward.setIsAct(rs.getBoolean("isactive"));
				date = rs.getTimestamp("credate");
				reward.setcDate(date != null ? date.getTime() : null);
				reward.setcBy(rs.getString("creby"));
				date = rs.getTimestamp("upddate");
				reward.setuDate(date != null ? date.getTime() : null);
				reward.setuBy(rs.getString("updby"));
				reward.setDesc(rs.getString("rwddesc"));
				reward.setRwdImgUrl(rs.getString("rwdimgurl"));
				reward.setUnit(rs.getString("rwdunimesr"));
				rewards.add(reward);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return rewards;
	}

	/**
	 * Gets the reward type.
	 *
	 * @param clId    the cl id
	 * @param rwdType the rwd type
	 * @return the reward type
	 * @throws Exception the exception
	 */
	public static RewardTypeDVO getRewardType(String clId, String rwdType) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		RewardTypeDVO reward = null;
		StringBuffer sql = new StringBuffer("SELECT * from  sd_reward_type WHERE clintid=? AND rwdtype=?");
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clId);
			ps.setString(2, rwdType);
			rs = ps.executeQuery();

			while (rs.next()) {
				Date date = null;
				reward = new RewardTypeDVO();
				reward.setClId(rs.getString("clintid"));
				reward.setType(rs.getString("rwdtype"));
				reward.setIsAct(rs.getBoolean("isactive"));
				date = rs.getTimestamp("credate");
				reward.setcDate(date != null ? date.getTime() : null);
				reward.setcBy(rs.getString("creby"));
				date = rs.getTimestamp("upddate");
				reward.setuDate(date != null ? date.getTime() : null);
				reward.setuBy(rs.getString("updby"));
				reward.setDesc(rs.getString("rwddesc"));
				reward.setRwdImgUrl(rs.getString("rwdimgurl"));
				reward.setUnit(rs.getString("rwdunimesr"));
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return reward;
	}

}
