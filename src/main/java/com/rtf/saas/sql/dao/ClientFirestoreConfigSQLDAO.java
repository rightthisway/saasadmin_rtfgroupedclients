/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.sql.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.rtf.saas.sql.db.DatabaseConnections;
import com.rtf.saas.sql.dvo.ClientFirestoreConfigDVO;

/**
 * The Class ClientFirestoreConfigSQLDAO.
 */
public class ClientFirestoreConfigSQLDAO {

	/**
	 * Gets the client Config.
	 *
	 * @param clientId the client id
	 * @return the client config
	 * @throws Exception the exception
	 */
	public static List<ClientFirestoreConfigDVO> getClientConfig(String clientId) throws Exception {
		List<ClientFirestoreConfigDVO> configs = new ArrayList<ClientFirestoreConfigDVO>();
		ClientFirestoreConfigDVO config = null;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer sql = new StringBuffer("SELECT * from client_tech_firestore_config  WHERE clintid = ? ");
		try {
			conn = DatabaseConnections.getSaasClientDetailsConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			rs = ps.executeQuery();
			while (rs.next()) {
				config = new ClientFirestoreConfigDVO();
				config.setClId(rs.getString("clintid"));
				config.setFirestoreClientConfig(rs.getString("firestore_client_config"));
				config.setFirestoreAdminConfig(rs.getString("firestore_admin_config"));
				config.setFirestoreDBAlias(rs.getString("firestore_db_alias"));
				configs.add(config);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
				throw ex;
			}
		}
		return configs;

	}
}
