/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.sql.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.rtf.ott.sql.dvo.CategoryDVO;
import com.rtf.saas.sql.db.DatabaseConnections;
import com.rtf.saas.sql.dvo.ClientUserPasswordDVO;

/**
 * The Class ClientUserPasswordDAO.
 */
public class ClientUserPasswordDAO {

	/**
	 * Gets the client user password by user id.
	 *
	 * @param userId the user id
	 * @return the client user password by user id
	 * @throws Exception the exception
	 */
	/*
	 * @param Clientid
	 * 
	 * @return
	 * 
	 * @throws Exception
	 */
	public static ClientUserPasswordDVO getClientUserPasswordByUserId(String userId) throws Exception {

		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		ClientUserPasswordDVO clientUserPwd = null;
		StringBuffer sql = new StringBuffer("SELECT clintid,userid,useremail,userpwd,notisentdate,notiurl,notiveridate,ispwdactivated,creby,credate,prevcredate,updby,upddate"); 
		sql.append("  FROM client_user_pwd where userid=?");
		try {
			conn = DatabaseConnections.getSaasAdminConnection();

			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, userId);
			rs = ps.executeQuery();
			while (rs.next()) {
				clientUserPwd = new ClientUserPasswordDVO();
				clientUserPwd.setClintid(rs.getString("clintid"));
				clientUserPwd.setUserid(rs.getString("userid"));
				clientUserPwd.setUseremail(rs.getString("useremail"));
				clientUserPwd.setUserpwd(rs.getString("userpwd"));
				clientUserPwd.setNotiurl(rs.getString("notiurl"));
				clientUserPwd.setIspwdactivated(rs.getString("ispwdactivated"));
				clientUserPwd.setCreby(rs.getString("creby"));
				clientUserPwd.setUpdby(rs.getString("updby"));
				
				if(rs.getTimestamp("credate") != null) {
					clientUserPwd.setCredate(new Date(rs.getTimestamp("credate").getTime()));	
				}
				if(rs.getTimestamp("notisentdate") != null) {
					clientUserPwd.setNotisentdate(new Date(rs.getTimestamp("notisentdate").getTime()));	
				}
				if(rs.getTimestamp("notiveridate") != null) {
					clientUserPwd.setNotiveridate(new Date(rs.getTimestamp("notiveridate").getTime()));	
				}
				if(rs.getTimestamp("prevcredate") != null) {
					clientUserPwd.setPrevcredate(new Date(rs.getTimestamp("prevcredate").getTime()));	
				}
				if(rs.getTimestamp("upddate") != null) {
					clientUserPwd.setUpddate(new Date(rs.getTimestamp("upddate").getTime()));	
				}
			}
			return clientUserPwd;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;

	}

	/**
	 * Creates the category.
	 *
	 * @param userId the user id
	 * @param clientid the clientid
	 * @param dvo the dvo
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer createCategory(String userId, String clientid, CategoryDVO dvo) throws Exception {

		Connection conn = null;		
		PreparedStatement ps = null;
		Integer affectedRows = 0;
		StringBuffer sql = new StringBuffer(" insert into  sd_category_type (clintid,cattype");
		sql.append(",catdesc,creby,isactive,credate ) ");
		sql.append("values(?,?,?,?,?,getdate()) ");

		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientid);
			ps.setString(2, dvo.getCattype());
			ps.setString(3, dvo.getCatdesc());
			ps.setString(4, userId);
			ps.setBoolean(5, Boolean.TRUE);
			affectedRows = ps.executeUpdate();		
			return affectedRows;

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return affectedRows;
	}

	

}
