/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.sql.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.rtf.saas.sql.db.DatabaseConnections;
import com.rtf.saas.sql.dvo.ClientProductsDVO;

/**
 * The Class ClientProductsDAO.
 */
public class ClientProductsDAO {

	/**
	 * Gets the client products.
	 *
	 * @param clId the cl id
	 * @return the client products
	 * @throws Exception the exception
	 */
	public static List<ClientProductsDVO> getClientProducts(String clId) throws Exception {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		ClientProductsDVO prod = null;
		List<ClientProductsDVO> products = new ArrayList<ClientProductsDVO>();
		StringBuffer sql = new StringBuffer("select * from client_product_registered WHERE clintid=? and isactive=?");
		try {
			conn = DatabaseConnections.getSaasClientDetailsConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clId);
			ps.setString(2, "ACTIVE");
			rs = ps.executeQuery();
			while (rs.next()) {
				prod = new ClientProductsDVO();
				prod.setClId(rs.getString("clintid"));
				prod.setCreBy(rs.getString("creby"));
				prod.setCreDate(rs.getTimestamp("credate"));
				prod.setIsActive(rs.getString("isactive"));
				prod.setIsRenewed(rs.getString("isrenewed"));
				prod.setIsUpgraded(rs.getBoolean("isupgraded"));
				prod.setProdCode(rs.getString("prodcode"));
				prod.setProdRegEndDate(rs.getTimestamp("prodregsenddate"));
				prod.setProdRegStartDate(rs.getTimestamp("prodregsdate"));			
				prod.setRenewDate(rs.getTimestamp("reneweddate"));
				prod.setUpdBy(rs.getString("updby"));
				prod.setUpdDate(rs.getTimestamp("upddate"));
				prod.setUpgradedDate(rs.getTimestamp("upgradeddate"));
				products.add(prod);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return products;
	}

}
