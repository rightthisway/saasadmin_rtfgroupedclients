/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.sql.dvo;

import java.io.Serializable;
import java.util.Date;

/**
 * The Class ClientUserDVO.
 */
public class ClientUserDVO implements Serializable {

	private static final long serialVersionUID = -3391963219805945726L;

	/** The client id. */
	private String clintid;

	/** The userid. */
	private String userid;

	/** The useremail. */
	private String useremail;

	/** The userfname. */
	private String userfname;

	/** The userlname. */
	private String userlname;

	/** The userphone. */
	private String userphone;

	/** The usertype. */
	private String usertype;

	/** The isactive. */
	private String isactive;

	/** The istestuser. */
	private Boolean istestuser;

	/** The creby. */
	private String creby;

	/** The credate. */
	private Date credate;

	/** The updby. */
	private String updby;

	/** The upddate. */
	private Date upddate;

	/** The password. */
	private String password;

	/**
	 * Gets the clintid.
	 *
	 * @return the clintid
	 */
	public String getClintid() {
		return clintid;
	}

	/**
	 * Sets the clintid.
	 *
	 * @param clintid the new clintid
	 */
	public void setClintid(String clintid) {
		this.clintid = clintid;
	}

	/**
	 * Gets the userid.
	 *
	 * @return the userid
	 */
	public String getUserid() {
		return userid;
	}

	/**
	 * Sets the userid.
	 *
	 * @param userid the new userid
	 */
	public void setUserid(String userid) {
		this.userid = userid;
	}

	/**
	 * Gets the useremail.
	 *
	 * @return the useremail
	 */
	public String getUseremail() {
		return useremail;
	}

	/**
	 * Sets the useremail.
	 *
	 * @param useremail the new useremail
	 */
	public void setUseremail(String useremail) {
		this.useremail = useremail;
	}

	/**
	 * Gets the userfname.
	 *
	 * @return the userfname
	 */
	public String getUserfname() {
		return userfname;
	}

	/**
	 * Sets the userfname.
	 *
	 * @param userfname the new userfname
	 */
	public void setUserfname(String userfname) {
		this.userfname = userfname;
	}

	/**
	 * Gets the userlname.
	 *
	 * @return the userlname
	 */
	public String getUserlname() {
		return userlname;
	}

	/**
	 * Sets the userlname.
	 *
	 * @param userlname the new userlname
	 */
	public void setUserlname(String userlname) {
		this.userlname = userlname;
	}

	/**
	 * Gets the userphone.
	 *
	 * @return the userphone
	 */
	public String getUserphone() {
		return userphone;
	}

	/**
	 * Sets the userphone.
	 *
	 * @param userphone the new userphone
	 */
	public void setUserphone(String userphone) {
		this.userphone = userphone;
	}

	/**
	 * Gets the usertype.
	 *
	 * @return the usertype
	 */
	public String getUsertype() {
		return usertype;
	}

	/**
	 * Sets the usertype.
	 *
	 * @param usertype the new usertype
	 */
	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}

	/**
	 * Gets the isactive.
	 *
	 * @return the isactive
	 */
	public String getIsactive() {
		return isactive;
	}

	/**
	 * Sets the isactive.
	 *
	 * @param isactive the new isactive
	 */
	public void setIsactive(String isactive) {
		this.isactive = isactive;
	}

	/**
	 * Gets the istestuser.
	 *
	 * @return the istestuser
	 */
	public Boolean getIstestuser() {
		return istestuser;
	}

	/**
	 * Sets the istestuser.
	 *
	 * @param istestuser the new istestuser
	 */
	public void setIstestuser(Boolean istestuser) {
		this.istestuser = istestuser;
	}

	/**
	 * Gets the creby.
	 *
	 * @return the creby
	 */
	public String getCreby() {
		return creby;
	}

	/**
	 * Sets the creby.
	 *
	 * @param creby the new creby
	 */
	public void setCreby(String creby) {
		this.creby = creby;
	}

	/**
	 * Gets the credate.
	 *
	 * @return the credate
	 */
	public Date getCredate() {
		return credate;
	}

	/**
	 * Sets the credate.
	 *
	 * @param credate the new credate
	 */
	public void setCredate(Date credate) {
		this.credate = credate;
	}

	/**
	 * Gets the updby.
	 *
	 * @return the updby
	 */
	public String getUpdby() {
		return updby;
	}

	/**
	 * Sets the updby.
	 *
	 * @param updby the new updby
	 */
	public void setUpdby(String updby) {
		this.updby = updby;
	}

	/**
	 * Gets the upddate.
	 *
	 * @return the upddate
	 */
	public Date getUpddate() {
		return upddate;
	}

	/**
	 * Sets the upddate.
	 *
	 * @param upddate the new upddate
	 */
	public void setUpddate(Date upddate) {
		this.upddate = upddate;
	}

	/**
	 * Gets the password.
	 *
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the password.
	 *
	 * @param password the new password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

}
