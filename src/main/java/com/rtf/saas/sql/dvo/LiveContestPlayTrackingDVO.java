/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.sql.dvo;

import java.util.Date;

/**
 * The Class LiveContestPlayTrackingDVO.
 */
public class LiveContestPlayTrackingDVO {

	/** The client id. */
	private String clintid;

	/** The conid. */
	private String conid;

	/** The apiname. */
	private String apiname;

	/** The hitdate. */
	private Date hitdate;

	/** The ip address. */
	private String ipAddress;

	/** The platform. */
	private String platform;

	/** The resstatus. */
	private Integer resstatus;

	/** The action. */
	private String action;

	/** The description. */
	private String description;

	/** The q no. */
	private Integer qNo;

	/** The user name. */
	private String userName;

	/**
	 * Gets the clintid.
	 *
	 * @return the clintid
	 */
	public String getClintid() {
		return clintid;
	}

	/**
	 * Sets the clintid.
	 *
	 * @param clintid the new clintid
	 */
	public void setClintid(String clintid) {
		this.clintid = clintid;
	}

	/**
	 * Gets the conid.
	 *
	 * @return the conid
	 */
	public String getConid() {
		return conid;
	}

	/**
	 * Sets the conid.
	 *
	 * @param conid the new conid
	 */
	public void setConid(String conid) {
		this.conid = conid;
	}

	/**
	 * Gets the apiname.
	 *
	 * @return the apiname
	 */
	public String getApiname() {
		return apiname;
	}

	/**
	 * Sets the apiname.
	 *
	 * @param apiname the new apiname
	 */
	public void setApiname(String apiname) {
		this.apiname = apiname;
	}

	/**
	 * Gets the hitdate.
	 *
	 * @return the hitdate
	 */
	public Date getHitdate() {
		return hitdate;
	}

	/**
	 * Sets the hitdate.
	 *
	 * @param hitdate the new hitdate
	 */
	public void setHitdate(Date hitdate) {
		this.hitdate = hitdate;
	}

	/**
	 * Gets the ip address.
	 *
	 * @return the ip address
	 */
	public String getIpAddress() {
		return ipAddress;
	}

	/**
	 * Sets the ip address.
	 *
	 * @param ipAddress the new ip address
	 */
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	/**
	 * Gets the platform.
	 *
	 * @return the platform
	 */
	public String getPlatform() {
		return platform;
	}

	/**
	 * Sets the platform.
	 *
	 * @param platform the new platform
	 */
	public void setPlatform(String platform) {
		this.platform = platform;
	}

	/**
	 * Gets the resstatus.
	 *
	 * @return the resstatus
	 */
	public Integer getResstatus() {
		return resstatus;
	}

	/**
	 * Sets the resstatus.
	 *
	 * @param resstatus the new resstatus
	 */
	public void setResstatus(Integer resstatus) {
		this.resstatus = resstatus;
	}

	/**
	 * Gets the action.
	 *
	 * @return the action
	 */
	public String getAction() {
		return action;
	}

	/**
	 * Sets the action.
	 *
	 * @param action the new action
	 */
	public void setAction(String action) {
		this.action = action;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the q no.
	 *
	 * @return the q no
	 */
	public Integer getqNo() {
		return qNo;
	}

	/**
	 * Sets the q no.
	 *
	 * @param qNo the new q no
	 */
	public void setqNo(Integer qNo) {
		this.qNo = qNo;
	}

	/**
	 * Gets the user name.
	 *
	 * @return the user name
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * Sets the user name.
	 *
	 * @param userName the new user name
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
}
