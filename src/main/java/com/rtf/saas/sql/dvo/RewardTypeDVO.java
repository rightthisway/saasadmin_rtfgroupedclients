/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.sql.dvo;

import java.io.Serializable;

import com.rtf.ott.util.DateFormatUtil;

/**
 * The Class RewardTypeDVO.
 */
public class RewardTypeDVO implements Serializable {

	private static final long serialVersionUID = -8311128489918148577L;

	/** The cl id. */
	private String clId;

	/** The type. */
	private String type;

	/** The desc. */
	private String desc;

	/** The unit. */
	private String unit;

	/** The is act. */
	private Boolean isAct;

	/** The c by. */
	private String cBy;

	/** The u by. */
	private String uBy;

	/** The c date. */
	private Long cDate;

	/** The u date. */
	private Long uDate;

	/** The rwd img url. */
	private String rwdImgUrl;

	/** The cr date time str. */
	private String crDateTimeStr;

	/** The up date time str. */
	private String upDateTimeStr;

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the type.
	 *
	 * @param type the new type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Gets the desc.
	 *
	 * @return the desc
	 */
	public String getDesc() {
		return desc;
	}

	/**
	 * Sets the desc.
	 *
	 * @param desc the new desc
	 */
	public void setDesc(String desc) {
		this.desc = desc;
	}

	/**
	 * Gets the unit.
	 *
	 * @return the unit
	 */
	public String getUnit() {
		return unit;
	}

	/**
	 * Sets the unit.
	 *
	 * @param unit the new unit
	 */
	public void setUnit(String unit) {
		this.unit = unit;
	}

	/**
	 * Gets the checks if is act.
	 *
	 * @return the checks if is act
	 */
	public Boolean getIsAct() {
		return isAct;
	}

	/**
	 * Sets the checks if is act.
	 *
	 * @param isAct the new checks if is act
	 */
	public void setIsAct(Boolean isAct) {
		this.isAct = isAct;
	}

	/**
	 * Gets the c by.
	 *
	 * @return the c by
	 */
	public String getcBy() {
		return cBy;
	}

	/**
	 * Sets the c by.
	 *
	 * @param cBy the new c by
	 */
	public void setcBy(String cBy) {
		this.cBy = cBy;
	}

	/**
	 * Gets the u by.
	 *
	 * @return the u by
	 */
	public String getuBy() {
		return uBy;
	}

	/**
	 * Sets the u by.
	 *
	 * @param uBy the new u by
	 */
	public void setuBy(String uBy) {
		this.uBy = uBy;
	}

	/**
	 * Gets the c date.
	 *
	 * @return the c date
	 */
	public Long getcDate() {
		return cDate;
	}

	/**
	 * Sets the c date.
	 *
	 * @param cDate the new c date
	 */
	public void setcDate(Long cDate) {
		this.cDate = cDate;
	}

	/**
	 * Gets the u date.
	 *
	 * @return the u date
	 */
	public Long getuDate() {
		return uDate;
	}

	/**
	 * Sets the u date.
	 *
	 * @param uDate the new u date
	 */
	public void setuDate(Long uDate) {
		this.uDate = uDate;
	}

	/**
	 * Gets the cl id.
	 *
	 * @return the cl id
	 */
	public String getClId() {
		return clId;
	}

	/**
	 * Sets the cl id.
	 *
	 * @param clId the new cl id
	 */
	public void setClId(String clId) {
		this.clId = clId;
	}

	/**
	 * Gets the rwd img url.
	 *
	 * @return the rwd img url
	 */
	public String getRwdImgUrl() {
		return rwdImgUrl;
	}

	/**
	 * Sets the rwd img url.
	 *
	 * @param rwdImgUrl the new rwd img url
	 */
	public void setRwdImgUrl(String rwdImgUrl) {
		this.rwdImgUrl = rwdImgUrl;
	}

	/**
	 * Gets the cr date time str.
	 *
	 * @return the cr date time str
	 */
	public String getCrDateTimeStr() {
		crDateTimeStr = DateFormatUtil.getMMDDYYYYHHMMSSString(cDate);
		return crDateTimeStr;
	}

	/**
	 * Sets the cr date time str.
	 *
	 * @param crDateTimeStr the new cr date time str
	 */
	public void setCrDateTimeStr(String crDateTimeStr) {
		this.crDateTimeStr = crDateTimeStr;
	}

	/**
	 * Gets the up date time str.
	 *
	 * @return the up date time str
	 */
	public String getUpDateTimeStr() {
		upDateTimeStr = DateFormatUtil.getMMDDYYYYHHMMSSString(uDate);
		return upDateTimeStr;
	}

	/**
	 * Sets the up date time str.
	 *
	 * @param upDateTimeStr the new up date time str
	 */
	public void setUpDateTimeStr(String upDateTimeStr) {
		this.upDateTimeStr = upDateTimeStr;
	}
}
