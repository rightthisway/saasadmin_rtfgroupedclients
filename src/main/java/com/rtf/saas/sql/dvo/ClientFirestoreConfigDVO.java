/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.sql.dvo;

/**
 * The Class ClientFirestoreConfigDVO.
 */
public class ClientFirestoreConfigDVO {

	/** The client id. */
	private String clId;

	/** The firestore admin config. */
	private String firestoreAdminConfig;

	/** The firestore client config. */
	private String firestoreClientConfig;

	/** The firestore DB alias. */
	private String firestoreDBAlias;

	/**
	 * Gets the cl id.
	 *
	 * @return the cl id
	 */
	public String getClId() {
		return clId;
	}

	/**
	 * Sets the cl id.
	 *
	 * @param clId the new cl id
	 */
	public void setClId(String clId) {
		this.clId = clId;
	}

	/**
	 * Gets the firestore admin config.
	 *
	 * @return the firestore admin config
	 */
	public String getFirestoreAdminConfig() {
		return firestoreAdminConfig;
	}

	/**
	 * Sets the firestore admin config.
	 *
	 * @param firestoreAdminConfig the new firestore admin config
	 */
	public void setFirestoreAdminConfig(String firestoreAdminConfig) {
		this.firestoreAdminConfig = firestoreAdminConfig;
	}

	/**
	 * Gets the firestore client config.
	 *
	 * @return the firestore client config
	 */
	public String getFirestoreClientConfig() {
		return firestoreClientConfig;
	}

	/**
	 * Sets the firestore client config.
	 *
	 * @param firestoreClientConfig the new firestore client config
	 */
	public void setFirestoreClientConfig(String firestoreClientConfig) {
		this.firestoreClientConfig = firestoreClientConfig;
	}

	/**
	 * Gets the firestore DB alias.
	 *
	 * @return the firestore DB alias
	 */
	public String getFirestoreDBAlias() {
		return firestoreDBAlias;
	}

	/**
	 * Sets the firestore DB alias.
	 *
	 * @param firestoreDBAlias the new firestore DB alias
	 */
	public void setFirestoreDBAlias(String firestoreDBAlias) {
		this.firestoreDBAlias = firestoreDBAlias;
	}

}
