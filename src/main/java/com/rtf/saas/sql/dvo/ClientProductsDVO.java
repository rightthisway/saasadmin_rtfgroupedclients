/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.sql.dvo;

import java.io.Serializable;
import java.util.Date;

/**
 * The Class ClientProductsDVO.
 */
public class ClientProductsDVO implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -9048863779814070488L;

	/** The client id. */
	private String clId;
	
	/** The prod code. */
	private String prodCode;
	
	/** The prord version. */
	private String prordVersion;
	
	/** The prod reg start date. */
	private Date prodRegStartDate;
	
	/** The prod reg end date. */
	private Date prodRegEndDate;
	
	/** The is active. */
	private String isActive;
	
	/** The is renewed. */
	private String isRenewed;
	
	/** The renew date. */
	private Date renewDate;
	
	/** The is upgraded. */
	private Boolean isUpgraded;
	
	/** The upgraded date. */
	private Date upgradedDate;
	
	/** The cre by. */
	private String creBy;
	
	/** The upd by. */
	private String updBy;
	
	/** The cre date. */
	private Date creDate;
	
	/** The upd date. */
	private Date updDate;
	
	
	/**
	 * Gets the cl id.
	 *
	 * @return the cl id
	 */
	public String getClId() {
		return clId;
	}
	
	/**
	 * Sets the cl id.
	 *
	 * @param clId the new cl id
	 */
	public void setClId(String clId) {
		this.clId = clId;
	}
	
	/**
	 * Gets the prod code.
	 *
	 * @return the prod code
	 */
	public String getProdCode() {
		return prodCode;
	}
	
	/**
	 * Sets the prod code.
	 *
	 * @param prodCode the new prod code
	 */
	public void setProdCode(String prodCode) {
		this.prodCode = prodCode;
	}
	
	/**
	 * Gets the prord version.
	 *
	 * @return the prord version
	 */
	public String getPrordVersion() {
		return prordVersion;
	}
	
	/**
	 * Sets the prord version.
	 *
	 * @param prordVersion the new prord version
	 */
	public void setPrordVersion(String prordVersion) {
		this.prordVersion = prordVersion;
	}
	
	/**
	 * Gets the prod reg start date.
	 *
	 * @return the prod reg start date
	 */
	public Date getProdRegStartDate() {
		return prodRegStartDate;
	}
	
	/**
	 * Sets the prod reg start date.
	 *
	 * @param prodRegStartDate the new prod reg start date
	 */
	public void setProdRegStartDate(Date prodRegStartDate) {
		this.prodRegStartDate = prodRegStartDate;
	}
	
	/**
	 * Gets the prod reg end date.
	 *
	 * @return the prod reg end date
	 */
	public Date getProdRegEndDate() {
		return prodRegEndDate;
	}
	
	/**
	 * Sets the prod reg end date.
	 *
	 * @param prodRegEndDate the new prod reg end date
	 */
	public void setProdRegEndDate(Date prodRegEndDate) {
		this.prodRegEndDate = prodRegEndDate;
	}
	
	/**
	 * Gets the checks if is active.
	 *
	 * @return the checks if is active
	 */
	public String getIsActive() {
		return isActive;
	}
	
	/**
	 * Sets the checks if is active.
	 *
	 * @param isActive the new checks if is active
	 */
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	
	/**
	 * Gets the checks if is renewed.
	 *
	 * @return the checks if is renewed
	 */
	public String getIsRenewed() {
		return isRenewed;
	}
	
	/**
	 * Sets the checks if is renewed.
	 *
	 * @param isRenewed the new checks if is renewed
	 */
	public void setIsRenewed(String isRenewed) {
		this.isRenewed = isRenewed;
	}
	
	/**
	 * Gets the renew date.
	 *
	 * @return the renew date
	 */
	public Date getRenewDate() {
		return renewDate;
	}
	
	/**
	 * Sets the renew date.
	 *
	 * @param renewDate the new renew date
	 */
	public void setRenewDate(Date renewDate) {
		this.renewDate = renewDate;
	}
	
	/**
	 * Gets the checks if is upgraded.
	 *
	 * @return the checks if is upgraded
	 */
	public Boolean getIsUpgraded() {
		return isUpgraded;
	}
	
	/**
	 * Sets the checks if is upgraded.
	 *
	 * @param isUpgraded the new checks if is upgraded
	 */
	public void setIsUpgraded(Boolean isUpgraded) {
		this.isUpgraded = isUpgraded;
	}
	
	/**
	 * Gets the upgraded date.
	 *
	 * @return the upgraded date
	 */
	public Date getUpgradedDate() {
		return upgradedDate;
	}
	
	/**
	 * Sets the upgraded date.
	 *
	 * @param upgradedDate the new upgraded date
	 */
	public void setUpgradedDate(Date upgradedDate) {
		this.upgradedDate = upgradedDate;
	}
	
	/**
	 * Gets the cre by.
	 *
	 * @return the cre by
	 */
	public String getCreBy() {
		return creBy;
	}
	
	/**
	 * Sets the cre by.
	 *
	 * @param creBy the new cre by
	 */
	public void setCreBy(String creBy) {
		this.creBy = creBy;
	}
	
	/**
	 * Gets the upd by.
	 *
	 * @return the upd by
	 */
	public String getUpdBy() {
		return updBy;
	}
	
	/**
	 * Sets the upd by.
	 *
	 * @param updBy the new upd by
	 */
	public void setUpdBy(String updBy) {
		this.updBy = updBy;
	}
	
	/**
	 * Gets the cre date.
	 *
	 * @return the cre date
	 */
	public Date getCreDate() {
		return creDate;
	}
	
	/**
	 * Sets the cre date.
	 *
	 * @param creDate the new cre date
	 */
	public void setCreDate(Date creDate) {
		this.creDate = creDate;
	}
	
	/**
	 * Gets the upd date.
	 *
	 * @return the upd date
	 */
	public Date getUpdDate() {
		return updDate;
	}
	
	/**
	 * Sets the upd date.
	 *
	 * @param updDate the new upd date
	 */
	public void setUpdDate(Date updDate) {
		this.updDate = updDate;
	}
	
}
