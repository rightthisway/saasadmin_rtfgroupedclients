/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.service;

import java.util.List;

import com.rtf.saas.sql.dao.ThemeDAO;
import com.rtf.saas.sql.dvo.ThemeDVO;

/**
 * The Class ThemeService.
 */
public class ThemeService {

	/**
	 * Gets the all themes.
	 *
	 * @param clId the client id
	 * @return the all themes
	 */
	public static List<ThemeDVO> getAllThemes(String clId) {
		try {
			List<ThemeDVO> themes = ThemeDAO.getAllThemes(clId);
			return themes;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Gets the theme by id.
	 *
	 * @param clId the client id
	 * @param id   the id
	 * @return the theme by id
	 */
	public static ThemeDVO getThemeById(String clId, Integer id) {
		try {
			ThemeDVO theme = ThemeDAO.getThemeById(clId, id);
			return theme;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
