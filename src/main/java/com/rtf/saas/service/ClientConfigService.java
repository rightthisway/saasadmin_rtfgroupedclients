/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.rtf.saas.cass.dao.ClientConfigCassDAO;
import com.rtf.saas.sql.dao.ClientConfigSQLDAO;
import com.rtf.saas.sql.dvo.ClientConfigDVO;

/**
 * The Class ClientConfigService.
 */
public class ClientConfigService {

	/**
	 * Gets the all contest connfigs.
	 *
	 * @param clId   the cl id
	 * @param filter the filter
	 * @param pgNo   the pg no
	 * @return the all contest connfigs
	 */
	public static Map<String, List<ClientConfigDVO>> getAllContestConnfigs(String clId, String filter, String pgNo) {
		Map<String, List<ClientConfigDVO>> map = new HashMap<String, List<ClientConfigDVO>>();
		List<ClientConfigDVO> list = null;
		try {
			List<ClientConfigDVO> configs = ClientConfigSQLDAO.getAllContestConnfigs(clId, filter, pgNo);

			for (ClientConfigDVO conf : configs) {
				if (map.get(conf.getProdId()) != null) {
					list = map.get(conf.getProdId());
					if (list == null) {
						list = new ArrayList<ClientConfigDVO>();
					}
					list.add(conf);
					map.put(conf.getProdId(), list);
				} else {
					list = new ArrayList<ClientConfigDVO>();
					list.add(conf);
					map.put(conf.getProdId(), list);
				}

			}
			return map;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Gets the all contest connfigs count.
	 *
	 * @param clId   the cl id
	 * @param filter the filter
	 * @return the all contest connfigs count
	 */
	public static Integer getAllContestConnfigsCount(String clId, String filter) {
		Integer count = 0;
		try {
			count = ClientConfigSQLDAO.getAllContestConnfigsCount(clId, filter);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return count;
	}

	/**
	 * Gets the contest connfig by key.
	 *
	 * @param clId   the cl id
	 * @param prodId the prod id
	 * @param key    the key
	 * @return the contest connfig by key
	 */
	public static ClientConfigDVO getContestConnfigByKey(String clId, String prodId, String key) {
		ClientConfigDVO config = null;
		try {
			config = ClientConfigSQLDAO.getContestConnfigByKey(clId, prodId, key);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return config;
	}

	/**
	 * Update contest config.
	 *
	 * @param config the config
	 * @return the boolean
	 */
	public static Boolean updateContestConfig(ClientConfigDVO config) {
		Boolean isUpdate = false;
		try {
			isUpdate = ClientConfigSQLDAO.updateContestConfig(config);
			if (isUpdate) {
				isUpdate = ClientConfigCassDAO.updateContestConfig(config);
				if (!isUpdate) {
					// revert transaction.
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isUpdate;
	}
}
