/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.dto;

/**
 * The Class PaginationDTO.
 */
public class PaginationDTO {

	/** The page num. */
	private String pageNum;

	/** The total rows. */
	private Integer totalRows;

	/** The page size. */
	private Integer pageSize;

	/** The total pages. */
	private Double totalPages;

	/**
	 * Gets the page num.
	 *
	 * @return the page num
	 */
	public String getPageNum() {
		return pageNum;
	}

	/**
	 * Sets the page num.
	 *
	 * @param pageNum the new page num
	 */
	public void setPageNum(String pageNum) {
		this.pageNum = pageNum;
	}

	/**
	 * Gets the total rows.
	 *
	 * @return the total rows
	 */
	public Integer getTotalRows() {
		return totalRows;
	}

	/**
	 * Sets the total rows.
	 *
	 * @param totalRows the new total rows
	 */
	public void setTotalRows(Integer totalRows) {
		this.totalRows = totalRows;
	}

	/**
	 * Gets the page size.
	 *
	 * @return the page size
	 */
	public Integer getPageSize() {
		return pageSize;
	}

	/**
	 * Sets the page size.
	 *
	 * @param pageSize the new page size
	 */
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * Gets the total pages.
	 *
	 * @return the total pages
	 */
	public Double getTotalPages() {
		return totalPages;
	}

	/**
	 * Sets the total pages.
	 *
	 * @param totalPages the new total pages
	 */
	public void setTotalPages(Double totalPages) {
		this.totalPages = totalPages;
	}

}
