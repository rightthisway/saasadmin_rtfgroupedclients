/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.dto;

import java.util.List;

import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.sql.dvo.ClientConfigDVO;

/**
 * The Class ClientConfigDTO.
 */
public class ClientConfigDTO extends RtfSaasBaseDTO {

	/** The configs. */
	List<ClientConfigDVO> configs;

	/** The ott configs. */
	private List<ClientConfigDVO> ottConfigs;

	/** The live configs. */
	private List<ClientConfigDVO> liveConfigs;

	/** The snw configs. */
	private List<ClientConfigDVO> snwConfigs;

	/** The shop configs. */
	private List<ClientConfigDVO> shopConfigs;

	/** The comn configs. */
	private List<ClientConfigDVO> comnConfigs;

	/** The pagination. */
	PaginationDTO pagination;

	/**
	 * Gets the configs.
	 *
	 * @return the configs
	 */
	public List<ClientConfigDVO> getConfigs() {
		return configs;
	}

	/**
	 * Sets the configs.
	 *
	 * @param configs the new configs
	 */
	public void setConfigs(List<ClientConfigDVO> configs) {
		this.configs = configs;
	}

	/**
	 * Gets the pagination.
	 *
	 * @return the pagination
	 */
	public PaginationDTO getPagination() {
		return pagination;
	}

	/**
	 * Sets the pagination.
	 *
	 * @param pagination the new pagination
	 */
	public void setPagination(PaginationDTO pagination) {
		this.pagination = pagination;
	}

	/**
	 * Gets the ott configs.
	 *
	 * @return the ott configs
	 */
	public List<ClientConfigDVO> getOttConfigs() {
		return ottConfigs;
	}

	/**
	 * Sets the ott configs.
	 *
	 * @param ottConfigs the new ott configs
	 */
	public void setOttConfigs(List<ClientConfigDVO> ottConfigs) {
		this.ottConfigs = ottConfigs;
	}

	/**
	 * Gets the live configs.
	 *
	 * @return the live configs
	 */
	public List<ClientConfigDVO> getLiveConfigs() {
		return liveConfigs;
	}

	/**
	 * Sets the live configs.
	 *
	 * @param liveConfigs the new live configs
	 */
	public void setLiveConfigs(List<ClientConfigDVO> liveConfigs) {
		this.liveConfigs = liveConfigs;
	}

	/**
	 * Gets the snw configs.
	 *
	 * @return the snw configs
	 */
	public List<ClientConfigDVO> getSnwConfigs() {
		return snwConfigs;
	}

	/**
	 * Sets the snw configs.
	 *
	 * @param snwConfigs the new snw configs
	 */
	public void setSnwConfigs(List<ClientConfigDVO> snwConfigs) {
		this.snwConfigs = snwConfigs;
	}

	/**
	 * Gets the shop configs.
	 *
	 * @return the shop configs
	 */
	public List<ClientConfigDVO> getShopConfigs() {
		return shopConfigs;
	}

	/**
	 * Sets the shop configs.
	 *
	 * @param shopConfigs the new shop configs
	 */
	public void setShopConfigs(List<ClientConfigDVO> shopConfigs) {
		this.shopConfigs = shopConfigs;
	}

	/**
	 * Gets the comn configs.
	 *
	 * @return the comn configs
	 */
	public List<ClientConfigDVO> getComnConfigs() {
		return comnConfigs;
	}

	/**
	 * Sets the comn configs.
	 *
	 * @param comnConfigs the new comn configs
	 */
	public void setComnConfigs(List<ClientConfigDVO> comnConfigs) {
		this.comnConfigs = comnConfigs;
	}

}
