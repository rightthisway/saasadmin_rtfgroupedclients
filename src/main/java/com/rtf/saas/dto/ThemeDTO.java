/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.dto;

import java.util.List;

import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.sql.dvo.ThemeDVO;

/**
 * The Class ThemeDTO.
 */
public class ThemeDTO extends RtfSaasBaseDTO {

	/** The themes. */
	private List<ThemeDVO> themes;

	/**
	 * Gets the themes.
	 *
	 * @return the themes
	 */
	public List<ThemeDVO> getThemes() {
		return themes;
	}

	/**
	 * Sets the themes.
	 *
	 * @param themes the new themes
	 */
	public void setThemes(List<ThemeDVO> themes) {
		this.themes = themes;
	}
}
