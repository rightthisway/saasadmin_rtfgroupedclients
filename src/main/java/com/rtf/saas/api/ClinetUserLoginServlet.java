/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.dto.ClientUserDTO;
import com.rtf.saas.service.ClientService;
import com.rtf.saas.sql.dao.ClientUserDAO;
import com.rtf.saas.sql.dvo.ClientProductsDVO;
import com.rtf.saas.sql.dvo.ClientUserDVO;
import com.rtf.saas.util.GsonUtil;
import com.rtf.saas.util.SAASMessages;

/**
 * The Class ClinetUserLoginServlet.
 */
@WebServlet("/clientuserlogin1.json")
public class ClinetUserLoginServlet extends RtfSaasBaseServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6278726166340571693L;

	/**
	 * Generate Http Response for Client User Response data is sent in JSON format.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * Validate client user login based on email and password
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String userIdStr = request.getParameter("userId");
		String pwd = request.getParameter("pwd");
		//String lipAddr = request.getParameter("lipa");
		ClientUserDTO respDTO = new ClientUserDTO();
		respDTO.setSts(0);

		try {
			if (userIdStr == null || userIdStr.isEmpty()) {
				setClientMessage(respDTO, SAASMessages.INVALID_USER_NAME, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (pwd == null || pwd.isEmpty()) {
				setClientMessage(respDTO, SAASMessages.INVALID_PASSWORD, null);
				generateResponse(request, response, respDTO);
				return;
			}
			/*
			 * if (lipAddr == null || lipAddr.isEmpty()) { setClientMessage(respDTO,
			 * SAASMessages.INVALID_IP_ADDRESS, null); generateResponse(request, response,
			 * respDTO); return; }
			 */
			ClientUserDVO clientUser = ClientUserDAO.getClientUserWithPasswordByUserId(userIdStr);

			if (clientUser == null) {
				setClientMessage(respDTO, SAASMessages.USER_ID_NOT_EXISTS, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (!clientUser.getPassword().equals(pwd)) {
				setClientMessage(respDTO, SAASMessages.USER_ID_OR_PASSWORD_MISMATCH, null);
				generateResponse(request, response, respDTO);
				return;
			}
			List<ClientProductsDVO> products = ClientService.getClientProducts(clientUser.getClintid());
			for (ClientProductsDVO prod : products) {
				if (prod.getProdCode().equalsIgnoreCase("LVT/200525/010001")) {
					respDTO.setIsLIVE(true);
				} else if (prod.getProdCode().equalsIgnoreCase("SVT/200525/010001")) {
					respDTO.setIsSHOP(true);
				} else if (prod.getProdCode().equalsIgnoreCase("CCT/200525/010001")) {
					respDTO.setIsCHALLANGE(true);
				} else if (prod.getProdCode().equalsIgnoreCase("OTT/200525/010001")) {
					respDTO.setIsOTT(true);
				} else if (prod.getProdCode().equalsIgnoreCase("SNW/200525/010001")) {
					respDTO.setIsSNW(true);
				} else if (prod.getProdCode().equalsIgnoreCase("OVT/200525/010001")) {
					respDTO.setIsVIDEO(true);
				}
			}

			processRoleBasedMenuItems(clientUser, respDTO);
			respDTO.setClId(clientUser.getClintid());
			respDTO.setUserid(clientUser.getUserid());
			respDTO.setUseremail(clientUser.getUseremail());
			respDTO.setUserfname(clientUser.getUserfname());
			respDTO.setUserlname(clientUser.getUserlname());

			respDTO.setSts(1);
			generateResponse(request, response, respDTO);
			return;

		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(respDTO, null, null);
			generateResponse(request, response, respDTO);
		}
		return;
	}

	/**
	 * get role based menue items
	 *
	 * @param dvo     the dvo
	 * @param respDTO the resp DTO
	 * @return the client user DTO
	 */
	private static ClientUserDTO processRoleBasedMenuItems(ClientUserDVO dvo, ClientUserDTO respDTO) {
		List<String> enabledProdMenus = ClientService.getProductListByRoleMapping(respDTO.getUserrole());
		respDTO = enableMenusBasedOnRole(respDTO, enabledProdMenus);
		return respDTO;
	}

	/**
	 * Enable menus based on role.
	 *
	 * @param respDTO          the resp DTO
	 * @param enabledProdMenus the enabled prod menus
	 * @return the client user DTO
	 */
	private static ClientUserDTO enableMenusBasedOnRole(ClientUserDTO respDTO, List<String> enabledProdMenus) {
		if ("ADMIN".equals(respDTO.getUserrole())) {
			if (respDTO.getIsLIVE() == true || respDTO.getIsSHOP() == true) {
				respDTO.setIsHOST(true);
			}
			respDTO.setIsSETTINGS(true);
			respDTO.setIsREPORTS(true);
			respDTO.setIsUSERS(true);
			respDTO.setIsQUICKTASK(true);
		} else if ("HOST".equals(respDTO.getUserrole())) {
			if (respDTO.getIsLIVE() == true || respDTO.getIsSHOP() == true) {
				respDTO.setIsHOST(true);
			}
			respDTO.setIsLIVE(false);
			respDTO.setIsSHOP(false);
			respDTO.setIsCHALLANGE(false);
			respDTO.setIsOTT(false);
			respDTO.setIsSNW(false);
			respDTO.setIsVIDEO(false);
			respDTO.setIsSETTINGS(false);
			respDTO.setIsREPORTS(false);
			respDTO.setIsUSERS(false);
			respDTO.setIsQUICKTASK(false);
		} else if ("MODERATOR".equals(respDTO.getUserrole())) {
			respDTO.setIsSETTINGS(true);
			respDTO.setIsQUICKTASK(true);
			respDTO.setIsREPORTS(false);
			respDTO.setIsUSERS(false);

		} 
		return respDTO;
	}

}
