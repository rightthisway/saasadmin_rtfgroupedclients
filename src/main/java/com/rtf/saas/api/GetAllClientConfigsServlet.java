/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.dto.ClientConfigDTO;
import com.rtf.saas.service.ClientConfigService;
import com.rtf.saas.sql.dvo.ClientConfigDVO;
import com.rtf.saas.util.Constant;
import com.rtf.saas.util.GsonUtil;
import com.rtf.saas.util.PaginationUtil;
import com.rtf.user.util.UserMsgConstants;

/**
 * The Class GetAllClientConfigsServlet.
 */
@WebServlet("/getallclientconfigs.json")
public class GetAllClientConfigsServlet extends RtfSaasBaseServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 478979805046561830L;

	/**
	 * Generate Http Response for Client User Response data is sent in JSON format.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}

	/**
	 * get all client config details based on client id and filters
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String clId = request.getParameter("clId");
		String filter = request.getParameter("hfilter");
		String pgNo = request.getParameter("pgNo");
		ClientConfigDTO respDTO = new ClientConfigDTO();
		respDTO.setSts(0);
		respDTO.setClId(clId);

		try {
			if (clId == null || clId.isEmpty()) {
				setClientMessage(respDTO, UserMsgConstants.INVALID_CLIENT, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (pgNo == null || pgNo.isEmpty()) {
				pgNo = "1";
			}
			Map<String, List<ClientConfigDVO>> configMap = ClientConfigService.getAllContestConnfigs(clId, filter,
					pgNo);

			if (configMap == null || configMap.isEmpty()) {
				setClientMessage(respDTO, UserMsgConstants.NO_RECORD_FOUND, null);
				generateResponse(request, response, respDTO);
				return;
			}
			Integer count = ClientConfigService.getAllContestConnfigsCount(clId, filter);
			respDTO.setPagination(PaginationUtil.calculatePaginationParameter(count, pgNo));
			respDTO.setOttConfigs(configMap.get(Constant.OTT));
			respDTO.setLiveConfigs(configMap.get(Constant.LIVETRIVIA));
			respDTO.setSnwConfigs(configMap.get(Constant.SNW));
			respDTO.setShopConfigs(configMap.get(Constant.SHOP));
			respDTO.setComnConfigs(configMap.get(Constant.COMN));
			respDTO.setSts(1);
			generateResponse(request, response, respDTO);
			return;
		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(respDTO, null, null);
			generateResponse(request, response, respDTO);
		}
		return;

	}

}
