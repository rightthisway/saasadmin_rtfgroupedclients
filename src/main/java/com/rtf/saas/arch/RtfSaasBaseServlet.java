/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.arch;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.saas.util.GsonUtil;
import com.rtf.saas.util.SAASMessages;
import com.rtf.secure.JWTUtil;

/**
 * The Class RtfSaasBaseServlet.
 */
abstract public class RtfSaasBaseServlet extends HttpServlet {

	/**
	 * serial version id
	 */
	private static final long serialVersionUID = -4545336272538219123L;

	/**
	 * Generate response.
	 *
	 * @param request        the HttpServletRequest
	 * @param response       the HttpServletResponse
	 * @param RtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	public abstract void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO RtfSaasBaseDTO) throws ServletException, IOException;

	/**
	 * Process request.
	 *
	 * @param request  the HttpServletRequest
	 * @param response the HttpServletResponse
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	public abstract void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException;

	/**
	 * Do get.
	 *
	 * @param request  the HttpServletRequest
	 * @param response the HttpServletResponse
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		setEncoding(request, response);
		Integer sts = performAuths(request, response);
		if (sts == 1) {
			processRequest(request, response);
		} else {
			generateAutFailedResponse(request, response);
			return;
		}
	}

	/**
	 * Do post.
	 *
	 * @param request  the HttpServletRequest
	 * @param response the HttpServletResponse
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		setEncoding(request, response);
		Integer sts = performAuths(request, response);
		if (sts == 1) {
			processRequest(request, response);
		} else {
			generateAutFailedResponse(request, response);
			return;
		}
	}

	/**
	 * Sets the client message.
	 *
	 * @param rtfSaasBaseDTO     the rtf saas base DTO
	 * @param customErrMsg       the custom err msg
	 * @param customsucesMessage the customsuces message
	 */
	protected void setClientMessage(RtfSaasBaseDTO rtfSaasBaseDTO, String customErrMsg, String customsucesMessage) {
		if (rtfSaasBaseDTO == null) {
			rtfSaasBaseDTO = new RtfSaasBaseDTO();
		}
		if (rtfSaasBaseDTO.getSts() == null) {
			rtfSaasBaseDTO.setSts(0);
		}
		if (rtfSaasBaseDTO.getSts() == 1) {
			if (customsucesMessage == null)
				customsucesMessage = SAASMessages.GEN_SUCCESS_MSG;
			rtfSaasBaseDTO.setMsg(customsucesMessage);
		} else {
			if (customErrMsg == null)
				customErrMsg = SAASMessages.GEN_ERR_MSG;
			rtfSaasBaseDTO.setMsg(customErrMsg);
		}
	}

	/** The unauthorized URL list. */
	// TODO Add email reset click link ..
	List<String> unauthURLList = new ArrayList<String>(Arrays.asList("/aufgtp.json", "/clientuserlogin.json",
			"/avgquestansbycustreport.json","/congrndwinreport.json","/contparticipantsreport.json","/contquestwiseuseransreport.json",
			"/consummwinreport.json","/Contwisenewcustreport.json","/custcontstatsbsnsreport.json","/custengagementgrp.json",
			"/custengagemonthgrp.json","/custavgtimepercontreport.json","/custavgtimepercontstatsreport.json",
			"/custcontstatsforbusinessreport.json","/custtimepercontreport.json","/livtgetcontestlistreport.json",
			"/mnthcustpartcpbrkdwnreport.json","/monthlyparticipantscountreport.json","/monthwiseregcustcount.json","/noofgamesplayedbycustomerreport.json",
			"/noofshoppgamegrp.json","/nooftriviagamegrp.json","/prtpntsshoppengagegrp.json","/prtpntstrivengagegrp.json","/shoppgamegrp.json",
			"/triviagamegrp.json","/dwnldliveconscript.json","/livegetconexcel.json","/ottgetconexcel.json","/getusersexcel.json","/comngetcategoryexcel.json","/comngetsubcategoryexcel.json",
			"/comngetquebankexcel.json","/comngetrwdtypesexcel.json","/shopgetmerchantexcel.json","/snwgetconexcel.json","/livegetsumwinexcel.json","/livegetgrandwinexcel.json","/shopgetmerchprodexcel.json",
			"/consansreport.json","/conscartreport.json","/consdiscreport.json","/gstoreaccs.json","/gshpfyprds.json","/gstoreaccs.json","/gdprdatarequest.json","/gdprshopredact.json","/gdprcustredact.json"
			));

	/**
	 * Perform auths.
	 *
	 * @param request  the http request
	 * @param response the http response
	 * @return the integer
	 * @throws IOException      Signals that an I/O exception has occurred.
	 * @throws ServletException the servlet exception
	 */
	private Integer performAuths(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		String apipath = request.getServletPath();
		String token = request.getHeader(JWTUtil.JWT_HEADER_STRING);
		if (unauthURLList.contains(apipath)) {
			return 1;
		}
		String cau = JWTUtil.getClientPayloadInToken(token);
		if (cau == null) {
			return 0;
		}
		if (!request.getParameter("cau").equals(cau)) {
			return 0;
		}
		return 1;
	}

	/**
	 * Sets the encoding.
	 *
	 * @param request  the request
	 * @param response the response
	 */
	protected void setEncoding(HttpServletRequest request, HttpServletResponse response) {
		try {
			request.setCharacterEncoding("UTF-8");
			response.setContentType("application/json;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * Generate authorization failed response.
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	public void generateAutFailedResponse(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RtfSaasBaseDTO rtfSaasBaseDTO = new RtfSaasBaseDTO();
		rtfSaasBaseDTO.setSts(-1);
		rtfSaasBaseDTO.setMsg("UNAUTHORIZED ACCESS.Please Login & try Again");
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

}