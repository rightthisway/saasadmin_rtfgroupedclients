/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.inventory.common.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.amazonaws.util.StringUtils;
import com.rtf.common.util.MessageConstant;
import com.rtf.common.util.SaaSAdminAppCache;
import com.rtf.inventory.common.dto.FileUploadGenericDTO;
import com.rtf.inventory.common.dto.RtfSellerTemplateUploadDTO;
import com.rtf.inventory.common.service.RtfSellerProdTemplateService;
import com.rtf.inventory.common.util.ASWS3ImageUploaderUtil;
import com.rtf.inventory.dvo.RtfSlrProdUpldTemplateDVO;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class EditRTFSellerProductTemplate.
 */
@WebServlet("/editRTFSellerProductTemplate.json")
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 10, // 10 MB
		maxFileSize = 1024 * 1024 * 20, // 20 MB
		maxRequestSize = 1024 * 1024 * 20) // 20 MB
public class EditRTFSellerProductTemplate extends RtfSaasBaseServlet {

	private static final long serialVersionUID = -8286504388031218568L;

	/**
	 * Generate Http Response RTFSellerProductTemplate Response data is sent in JSON
	 * format..
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * Add Contest Rewards
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		/**
		*private Integer sputID;		 
		private Integer slrID;
		private String pName;
		private String pBrand;
		private String pDesc;
		private String pLongDesc;
		private Double regPrc;
		private Double selPrc;
		private Double discOff;
		private Integer avilQty;
		private String varNameA;
		private String varValueA;
		private String pImg;		
		*/
		RtfSellerTemplateUploadDTO respDTO = new RtfSellerTemplateUploadDTO();
		respDTO.setSts(0);
		String clId = request.getParameter("clId");
		String sputIDstr = request.getParameter("sputID");
		String slrIDstr = request.getParameter("slrID");
		String pName = request.getParameter("pName");
		String pBrand = request.getParameter("pBrand");
		String pDesc = request.getParameter("pDesc");
		String pLongDesc = request.getParameter("pLongDesc");
		
		String regPrcstr = request.getParameter("regPrc");
		String selPrcstr = request.getParameter("selPrc");
		String discOffstr = request.getParameter("discOff");
		
		String avilQtystr = request.getParameter("avilQty");
		String varNameA = request.getParameter("varNameA");
		String varValueA = request.getParameter("varValueA");
		String pImg = request.getParameter("pImg");
		
		String cau = request.getParameter("cau");
		if(StringUtils.isNullOrEmpty(slrIDstr) || 
		   StringUtils.isNullOrEmpty(sputIDstr) || 	
		   StringUtils.isNullOrEmpty(pName) || 
		   StringUtils.isNullOrEmpty(pBrand) || 	
		   StringUtils.isNullOrEmpty(pDesc) || 	
		   StringUtils.isNullOrEmpty(pLongDesc) || 	
		   StringUtils.isNullOrEmpty(regPrcstr) || 	
		   StringUtils.isNullOrEmpty(selPrcstr) || 	
		   StringUtils.isNullOrEmpty(discOffstr) || 	
		   StringUtils.isNullOrEmpty(avilQtystr) || 	
		  // StringUtils.isNullOrEmpty(varNameA) || 	
		  // StringUtils.isNullOrEmpty(varValueA) || 	
		   StringUtils.isNullOrEmpty(cau) 
		) {
			setClientMessage(respDTO, MessageConstant.MANDATORY_PARAM_MISING, null);
			generateResponse(request, response, respDTO);
			return;
		}
	
		respDTO.setClId(clId);

		try {			
			RtfSlrProdUpldTemplateDVO dvo = new RtfSlrProdUpldTemplateDVO();
			dvo.setSputID(Integer.parseInt(sputIDstr));
			dvo.setSlrID(Integer.parseInt(slrIDstr));
			dvo.setpName(pName);
			dvo.setpBrand(pBrand);
			dvo.setpDesc(pDesc);
			dvo.setpLongDesc(pLongDesc);
			dvo.setRegPrc(Double.parseDouble(regPrcstr));
			dvo.setSelPrc(Double.parseDouble(selPrcstr));
			dvo.setDiscOff(Double.parseDouble(discOffstr));
			dvo.setAvilQty(Integer.parseInt(avilQtystr));
			dvo.setVarNameA(varNameA);
			dvo.setVarValueA(varValueA);
			dvo.setUpdBy(cau);
			String resMsg = "";
			
			System.out.println("****************[pImg] --"  + pImg);
			if(StringUtils.isNullOrEmpty(pImg) ) {
				FileUploadGenericDTO upldto = new FileUploadGenericDTO();
				try {				
					String s3bucket = SaaSAdminAppCache.s3staticbucket;
					String s3moduleFolder = SaaSAdminAppCache.s3inventoryoductsfolder;
					upldto = ASWS3ImageUploaderUtil.uploadFileToS3(clId, request, upldto, s3bucket, s3moduleFolder);
					System.out.println("[3]" + upldto);
					if (upldto.getSts() != 1) {
						resMsg = " Error Uploading Product Image";
						setClientMessage(respDTO, resMsg, null);
						generateResponse(request, response, respDTO);
						return;
					}
					if (GenUtil.isNullOrEmpty(upldto.getAwsfinalUrl())) {
						resMsg = " Error Uploading Product Image";
						setClientMessage(respDTO, resMsg, null);
						generateResponse(request, response, respDTO);
						return;
					}				
					dvo.setpImg(upldto.getAwsfinalUrl());				
				}catch(Exception ex) {
					ex.printStackTrace();
					resMsg = " Error Uploading Product Image";
					setClientMessage(respDTO, resMsg, null);
					generateResponse(request, response, respDTO);
					return;
				}
				
			}else {
				dvo.setpImg(pImg);
			}
		
			respDTO = RtfSellerProdTemplateService.updateRtfSellerProdTemplate(dvo, respDTO);
			respDTO.setDvo(dvo);
			if (respDTO.getSts() == 0) {
				setClientMessage(respDTO, null, null);
				generateResponse(request, response, respDTO);
				return;
			}
			generateResponse(request, response, respDTO);
			return;
		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(respDTO, null, null);
			generateResponse(request, response, respDTO);
		}
		return;
	}

}
