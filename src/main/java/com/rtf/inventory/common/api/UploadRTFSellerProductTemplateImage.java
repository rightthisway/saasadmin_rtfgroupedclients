/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.inventory.common.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.common.util.SaaSAdminAppCache;
import com.rtf.inventory.common.dto.FileUploadGenericDTO;
import com.rtf.inventory.common.dto.RtfSellerTemplateUploadDTO;
import com.rtf.inventory.common.service.RtfSellerProdTemplateService;
import com.rtf.inventory.common.util.ASWS3ImageUploaderUtil;
import com.rtf.inventory.common.util.MessageConstant;
import com.rtf.inventory.dvo.RtfSlrProdUpldTemplateDVO;
import com.rtf.inventory.seller.dvo.SellerDVO;
import com.rtf.inventory.seller.service.SellerService;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class UploadRTFSellerProductTemplateImage.
 */

@WebServlet("/upldRtfSlrProdTemplateImg.json")
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 10, // 10 MB
		maxFileSize = 1024 * 1024 * 20, // 20 MB
		maxRequestSize = 1024 * 1024 * 20) // 20 MB
public class UploadRTFSellerProductTemplateImage extends RtfSaasBaseServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Import product inventory from excel file and store it.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		RtfSellerTemplateUploadDTO dto = new RtfSellerTemplateUploadDTO();
		dto.setSts(0);
		try {
			String resMsg = "";		
			String clientIdStr = request.getParameter("clId");
			String cau = request.getParameter("cau");
			String prodTemplaTeId = request.getParameter("sputID");

			
			if (GenUtil.isNullOrEmpty(prodTemplaTeId)) {
				resMsg = MessageConstant.INVALID_PROD_ID;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}
			
			if (GenUtil.isNullOrEmpty(cau)) {
				resMsg = MessageConstant.INVALID_USER;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}

			if (GenUtil.isNullOrEmpty(clientIdStr)) {
				resMsg = MessageConstant.INVALID_CLIENT;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}		
			RtfSlrProdUpldTemplateDVO tdvo = new RtfSlrProdUpldTemplateDVO();
			FileUploadGenericDTO upldto = new FileUploadGenericDTO();						
			tdvo.setSputID(Integer.parseInt(prodTemplaTeId));
			try {				
				String s3bucket = SaaSAdminAppCache.s3staticbucket;
				String s3moduleFolder = SaaSAdminAppCache.s3inventoryoductsfolder;
				upldto = ASWS3ImageUploaderUtil.uploadFileToS3(clientIdStr, request, upldto, s3bucket, s3moduleFolder);
				System.out.println("[3]" + upldto);
				if (upldto.getSts() != 1) {
					resMsg = MessageConstant.PRODUCT_IMAGE_ERROR+ upldto.getMsg();
					setClientMessage(dto, resMsg, null);
					generateResponse(request, response, dto);
					return;
				}
				if (GenUtil.isNullOrEmpty(upldto.getAwsfinalUrl())) {
					resMsg = MessageConstant.PRODUCT_IMAGE_ERROR + upldto.getMsg();
					setClientMessage(dto, resMsg, null);
					generateResponse(request, response, dto);
					return;
				}				
				tdvo.setpImg(upldto.getAwsfinalUrl());				
			}catch(Exception ex) {
				ex.printStackTrace();
				resMsg = MessageConstant.PRODUCT_IMAGE_ERROR + upldto.getMsg();
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}
			SellerDVO dvo = new SellerDVO();
			dvo.setEmail(cau);
			dvo = SellerService.fetchSellerDetsByEmail( dvo);
			tdvo.setSlrID(dvo.getSlerId());
			dto.setClId(clientIdStr);
			dto.setCau(cau);
			dto.setSts(0);
			dto.setSlrID(dvo.getSlerId());	
			
			System.out.println(dvo);
			System.out.println(tdvo);
			dto = RtfSellerProdTemplateService.updateTemplateImages( dto, tdvo);
			System.out.println("[4]" + dto);
			if (dto.getSts() == 0) {
				setClientMessage(dto, MessageConstant.PRODUCT_IMAGE_ERROR + dto.getMsg(), null);
				generateResponse(request, response, dto);
				return;
			}
			dto.setTmplateLst(null);
			setClientMessage(dto, null, null);
			generateResponse(request, response, dto);
			return;

		} catch (Exception e) {
			setClientMessage(dto, MessageConstant.INV_UPLOAD_ERROR + dto.getMsg(), null);
			generateResponse(request, response, dto);
			return;

		} finally {
		}

	}

	/**
	 * Generate Http Response for Client.Response, data is sent in JSON format.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param rtfSaasBaseDTO
	 *            the rtf saas base DTO
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}
	



}
