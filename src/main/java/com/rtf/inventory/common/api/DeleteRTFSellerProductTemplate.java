/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.inventory.common.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.amazonaws.util.StringUtils;
import com.rtf.common.util.MessageConstant;
import com.rtf.common.util.SaaSAdminAppCache;
import com.rtf.inventory.common.dto.FileUploadGenericDTO;
import com.rtf.inventory.common.dto.RtfSellerTemplateUploadDTO;
import com.rtf.inventory.common.service.RtfSellerProdTemplateService;
import com.rtf.inventory.common.util.ASWS3ImageUploaderUtil;
import com.rtf.inventory.dvo.RtfSlrProdUpldTemplateDVO;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class DeleteRTFSellerProductTemplate.
 */
@WebServlet("/deleteRTFSellerProductTemplate.json")

public class DeleteRTFSellerProductTemplate extends RtfSaasBaseServlet {

	private static final long serialVersionUID = -7286504388031218568L;

	/**
	 * Generate Http Response RTFSellerProductTemplate Response data is sent in JSON
	 * format..
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * Add Contest Rewards
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RtfSellerTemplateUploadDTO respDTO = new RtfSellerTemplateUploadDTO();
		respDTO.setSts(0);
		String clId = request.getParameter("clId");
		String sputIDstr = request.getParameter("sputID");
		String cau = request.getParameter("cau");
		if(
		   StringUtils.isNullOrEmpty(sputIDstr) ||	
		   StringUtils.isNullOrEmpty(cau) 
		) {
			setClientMessage(respDTO, MessageConstant.MANDATORY_PARAM_MISING, null);
			generateResponse(request, response, respDTO);
			return;
		}	
		respDTO.setClId(clId);

		try {			
			RtfSlrProdUpldTemplateDVO dvo = new RtfSlrProdUpldTemplateDVO();
			dvo.setSputID(Integer.parseInt(sputIDstr));			
			respDTO = RtfSellerProdTemplateService.deleteRtfSellerProdTemplate(dvo, respDTO);
			if (respDTO.getSts() == 0) {
				setClientMessage(respDTO, null, null);
				generateResponse(request, response, respDTO);
				return;
			}
			generateResponse(request, response, respDTO);
			return;
		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(respDTO, null, null);
			generateResponse(request, response, respDTO);
		}
		return;
	}

}
