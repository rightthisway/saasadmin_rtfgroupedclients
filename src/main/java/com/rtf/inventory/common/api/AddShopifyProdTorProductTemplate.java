/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.inventory.common.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.amazonaws.util.StringUtils;
import com.rtf.common.util.MessageConstant;
import com.rtf.inventory.common.dto.RtfSellerTemplateUploadDTO;
import com.rtf.inventory.common.service.RtfSellerProdTemplateService;
import com.rtf.inventory.dvo.RtfSlrProdUpldTemplateDVO;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class EditRTFSellerProductTemplate.
 */
@WebServlet("/addshpytoprodtmplte.json")
public class AddShopifyProdTorProductTemplate extends RtfSaasBaseServlet {

	private static final long serialVersionUID = -8286504388031218568L;

	/**
	 * Generate Http Response RTFSellerProductTemplate Response data is sent in JSON
	 * format..
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * Add Contest Rewards
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RtfSellerTemplateUploadDTO respDTO = new RtfSellerTemplateUploadDTO();
		respDTO.setSts(0);
		String clId = request.getParameter("clId");
		String slrIDstr = request.getParameter("slrID");
		String pName = request.getParameter("pName");
		String pBrand = request.getParameter("pBrand");
		String pDesc = request.getParameter("pDesc");
		String pLongDesc = request.getParameter("pLongDesc");

		String regPrcstr = request.getParameter("regPrc");
		String selPrcstr = request.getParameter("selPrc");
		String discOffstr = request.getParameter("discOff");

		String avilQtystr = request.getParameter("avilQty");
		String varNameA = request.getParameter("varNameA");
		String varValueA = request.getParameter("varValueA");
		String pImg = request.getParameter("pImg");
		String cau = request.getParameter("cau");
		if (StringUtils.isNullOrEmpty(slrIDstr) || StringUtils.isNullOrEmpty(pName) || StringUtils.isNullOrEmpty(pDesc)
				|| StringUtils.isNullOrEmpty(pLongDesc) || StringUtils.isNullOrEmpty(regPrcstr)
				|| StringUtils.isNullOrEmpty(selPrcstr) || StringUtils.isNullOrEmpty(discOffstr)
				|| StringUtils.isNullOrEmpty(avilQtystr) || StringUtils.isNullOrEmpty(pImg)
				|| StringUtils.isNullOrEmpty(cau)) {
			setClientMessage(respDTO, MessageConstant.MANDATORY_PARAM_MISING, null);
			generateResponse(request, response, respDTO);
			return;
		}
		respDTO.setClId(clId);
		try {
			RtfSlrProdUpldTemplateDVO dvo = new RtfSlrProdUpldTemplateDVO();
			dvo.setSlrID(Integer.parseInt(slrIDstr));
			dvo.setpName(pName);
			dvo.setpBrand(pBrand);
			dvo.setpDesc(pDesc);
			dvo.setpLongDesc(pLongDesc);
			dvo.setRegPrc(Double.parseDouble(regPrcstr));
			dvo.setSelPrc(Double.parseDouble(selPrcstr));
			dvo.setDiscOff(Double.parseDouble(discOffstr));
			dvo.setAvilQty(Integer.parseInt(avilQtystr));
			dvo.setVarNameA(varNameA);
			dvo.setVarValueA(varValueA);
			dvo.setUpdBy(cau);
			dvo.setpImg(pImg);
			String resMsg = "";
			Boolean isSaved = RtfSellerProdTemplateService.saveProductTemplateData(dvo);
			respDTO.setDvo(dvo);
			if (!isSaved) {
				setClientMessage(respDTO, null, null);
				generateResponse(request, response, respDTO);
				return;
			} else {
				respDTO.setSts(1);
			}
			generateResponse(request, response, respDTO);
			return;
		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(respDTO, null, null);
			generateResponse(request, response, respDTO);
		}
		return;
	}

}
