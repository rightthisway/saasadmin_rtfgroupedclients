package com.rtf.inventory.common.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.rtf.inventory.dvo.RtfSlrProdUpldTemplateDVO;
import com.rtf.saas.sql.db.DatabaseConnections;
import com.rtf.saas.util.PaginationUtil20;

public class RtfSlrProdUpldTemplateDAO {

	/**
	 * Creates the new Product Template Records.
	 *
	 * @param dvo  the dvo
	 * @param conn the conn
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer createSellerProductTemplateRecords(RtfSlrProdUpldTemplateDVO dvo, Connection conn) throws Exception {

		PreparedStatement ps = null;
		StringBuffer sql = new StringBuffer();
		Integer pkId = null;
		sql.append("insert into  rtf_seller_product_upload_template")
		.append(" ( slrID ,pName,pBrand ,pDesc, pLongDesc " ) 
		.append(" ,regPrc,selPrc  ,discOff ,avilQty ")
		.append(" ,varNameA ,varValueA ")
		.append(" ,pImg ,updFileName ")
		.append(" ,updBy,updDate ) VALUES  ")        
		.append(" (?,?,?,? ,?,")
		.append(" ?,?,?,?,")
		.append(" ?,?,")
		.append(" ?,?,")
		.append(" ?,getdate() ) "); 
		try {			
			ps = conn.prepareStatement(sql.toString());
			ps.setInt(1, dvo.getSlrID());			
			ps.setString(2, dvo.getpName());
			ps.setString(3, dvo.getpBrand());
			ps.setString(4, dvo.getpDesc());
			ps.setString(5, dvo.getpLongDesc());
			
			ps.setDouble(6, dvo.getRegPrc());
			ps.setDouble(7, dvo.getSelPrc());
			ps.setDouble(8, dvo.getDiscOff());
			ps.setInt(9, dvo.getAvilQty());
			
			ps.setString(10, dvo.getVarNameA());		
			ps.setString(11, dvo.getVarValueA());
		
			ps.setString(12, dvo.getpImg());
			ps.setString(13, dvo.getUpdFileName());
			ps.setString(14, dvo.getUpdBy());

			int affectedRows = ps.executeUpdate();
			if (affectedRows == 0) {
				throw new SQLException("Creating Seller Product template record failed for Seller Id " + dvo.getSlrID());
			}
		 return affectedRows;
		} catch (SQLException se) {
			se.printStackTrace();
			throw se;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			try {
				ps.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}
	
	
	public static List<RtfSlrProdUpldTemplateDVO> getAllSellerProductTemplateRecordsByFilter(Integer sellerId, String filter,
			String pgNo, Connection conn ) throws Exception {		
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<RtfSlrProdUpldTemplateDVO> list = new ArrayList<RtfSlrProdUpldTemplateDVO>();
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT * from rtf_seller_product_upload_template with (nolock) ")				
				.append(" WHERE slrID = ? ");
		try {		
			String filterQuery = null;//GridHeaderFilterUtil.getContestFilterQuery(filter);
			String paginationQuery = PaginationUtil20.getPaginationQuery(pgNo);
			if (filterQuery != null && !filterQuery.isEmpty()) {
				sql.append(filterQuery);
			}		
			sql.append(" ORDER BY updDate desc");
			sql.append(paginationQuery);
			//conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setInt(1, sellerId);
			rs = ps.executeQuery();

			while (rs.next()) {
				RtfSlrProdUpldTemplateDVO dvo = new RtfSlrProdUpldTemplateDVO();
				Date date = null;	
				dvo.setSputID(rs.getInt("sputID"));
				dvo.setSlrID(rs.getInt("slrID"));			
				dvo.setpName(rs.getString("pName"));
				dvo.setpBrand(rs.getString("pBrand"));
				dvo.setpDesc(rs.getString("pDesc"));
				dvo.setpLongDesc(rs.getString("pLongDesc"));
				
				dvo.setRegPrc(rs.getDouble("regPrc"));
				dvo.setSelPrc(rs.getDouble("selPrc"));
				dvo.setDiscOff(rs.getDouble("discOff"));
				dvo.setAvilQty(rs.getInt("avilQty"));
				
				dvo.setVarNameA(rs.getString("varNameA"));
				dvo.setVarValueA(rs.getString("varValueA"));
			
				dvo.setpImg(rs.getString("pImg"));
				dvo.setUpdFileName(rs.getString("updFileName"));
				dvo.setUpdBy(rs.getString("updBy"));	
				date = rs.getTimestamp("updDate");
				dvo.setUpdDate(date != null ? date.getTime() : null);
				list.add(dvo);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
			//	DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return list;
	}
	
	
	
	/**
	 * Gets the all SellerProductTemplateRecords by statusand filter.
	 *
	 * @param sellerId
	 *            the SellerId id
	
	 * @param filter
	 *            the filter
	 * @return the all SellerProductTemplateRecords count by filter
	 * @throws Exception
	 *             the exception
	 */
	public static Integer getAllSellerProductTemplateRecordsCountByFilter(Integer sellerId, String filter, Connection conn)
			throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Integer count = 0;
		try {
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT count(*) from rtf_seller_product_upload_template with (nolock) WHERE slrID = ? ");
		
			String filterQuery = null;// GridHeaderFilterUtil.getContestFilterQuery(filter);
			if (filterQuery != null && !filterQuery.isEmpty()) {
				sql.append(filterQuery);
			}		
			ps = conn.prepareStatement(sql.toString());
			ps.setInt(1, sellerId);
			rs = ps.executeQuery();
			while (rs.next()) {
				count = rs.getInt(1);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return count;
	}
	
	/**
	 * Update product item Image .
	 *
	 * @param rdvo the RtfSlrProdUpldTemplateDVO entity
	 * @param conn the conn
	 * @return the integer
	 * @throws Exception the exception
	 */
	
	public static Integer updateProductImage(RtfSlrProdUpldTemplateDVO rdvo, Connection conn) throws Exception {
		String sql = " update rtf_seller_product_upload_template set  pimg=? where sputID=?  ";
		Integer updCnt = 0;
		PreparedStatement ps = null;
		System.out.println("[DAO]" + " IMG URL = " +  rdvo.getpImg()   + " SPUTID="  + rdvo.getSputID());
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, rdvo.getpImg());
			ps.setInt(2, rdvo.getSputID());			
			updCnt = ps.executeUpdate();
			return updCnt;
		} catch (SQLException se) {
			se.printStackTrace();
			throw se;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			try {
				ps.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}

}


	public static Integer updateRtfSellerProdTemplate(RtfSlrProdUpldTemplateDVO dvo, Connection conn) throws Exception {
		String sql = " update rtf_seller_product_upload_template set "
				+ " slrID=? , "
				+ " pName = ? , pBrand=?, pDesc=? , pLongDesc = ?, "
				+ " regPrc = ? , selPrc = ? , discOff = ? ,avilQty = ?, "
				+ " varNameA = ? , varValueA = ? ,  "
				+ " pImg = ? , updBy = ? , updDate = getdate() "
				+ "where sputID=?  ";
		Integer updCnt = 0;
		PreparedStatement ps = null;
		System.out.println("[DAO]" + " IMG URL = " +  dvo.getpImg()   + " SPUTID="  + dvo.getSputID());
		try {
			ps = conn.prepareStatement(sql);
			ps.setInt(1, dvo.getSlrID());			
			ps.setString(2, dvo.getpName());
			ps.setString(3, dvo.getpBrand());
			ps.setString(4, dvo.getpDesc());
			ps.setString(5, dvo.getpLongDesc());
			
			ps.setDouble(6, dvo.getRegPrc());
			ps.setDouble(7, dvo.getSelPrc());
			ps.setDouble(8, dvo.getDiscOff());
			ps.setInt(9, dvo.getAvilQty());
			
			ps.setString(10, dvo.getVarNameA());		
			ps.setString(11, dvo.getVarValueA());
		
			ps.setString(12, dvo.getpImg());			
			ps.setString(13, dvo.getUpdBy());
			ps.setInt(14, dvo.getSputID());		
			updCnt = ps.executeUpdate();
			return updCnt;
		} catch (SQLException se) {
			se.printStackTrace();
			throw se;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			try {
				ps.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}


	public static Integer deleteRtfSellerProdTemplate(RtfSlrProdUpldTemplateDVO dvo, Connection conn) throws Exception{
		String sql = " Delete from  rtf_seller_product_upload_template where "
				+ " sputID=?  ";
		Integer updCnt = 0;
		PreparedStatement ps = null;
		System.out.println("[DAO-DELETE]"  + " SPUTID="  + dvo.getSputID());
		try {
			ps = conn.prepareStatement(sql);		
			ps.setInt(1, dvo.getSputID());		
			updCnt = ps.executeUpdate();
			return updCnt;
		} catch (SQLException se) {
			se.printStackTrace();
			throw se;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			try {
				ps.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

}
