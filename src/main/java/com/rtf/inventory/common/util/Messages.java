/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.inventory.common.util;

/**
 * The Class Messages.
 */
public class Messages {
	
	/** The Constant PRODUCT_IMAGE_FILE_UPLOAD_MANDATORY. */
	public static final String PRODUCT_IMAGE_FILE_UPLOAD_MANDATORY=" Product Image File is Mandatory";
	
	/** The Constant SELLER_NAME_ALREADY_EXISTS. */
	public static final String SELLER_NAME_ALREADY_EXISTS="Seller is Existing in our Records";
	

	public static final String DOWNLOAD_FILE_EXC = "Something went wrong while downloading file.";
	
	
	public static final String UPLOAD_FILE_EXC = "Something went wrong while uploading file.";
	
}
