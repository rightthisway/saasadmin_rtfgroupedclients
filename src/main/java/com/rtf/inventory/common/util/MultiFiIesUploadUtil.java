/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.inventory.common.util;

import java.io.File;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

import com.rtf.aws.AWSFileService;
import com.rtf.aws.AwsS3Response;
import com.rtf.common.util.SaaSAdminAppCache;
import com.rtf.saas.exception.RTFServiceAccessException;
import com.rtf.saas.util.FileUtil;

/**
 * The Class MultiFiIesUploadUtil.
 */
public class MultiFiIesUploadUtil {

	/**
	 * Upload multiple files to given bucket and folder on S3.
	 *
	 * @param request the request
	 * @param clId the cl id
	 * @param varoptmap the varoptmap
	 * @return the map
	 * @throws Exception the exception
	 */
	public static Map<String, Map<String, String>> uploadMultipleFiles(HttpServletRequest request, String clId,
			Map<String, Map<String, String>> varoptmap) throws Exception {

		String tmpDir = SaaSAdminAppCache.TMP_FOLDER;
		tmpDir = tmpDir + File.separator + clId + File.separator;
		File fileSaveDir = new File(tmpDir);
		if (!fileSaveDir.exists()) {
			fileSaveDir.mkdirs();
		}
		String origFileName = null;
		String optNameVal = null;
		try {
			for (Part part : request.getParts()) {
				Map<String, String> optnameFileMap = FileUtil.getOptValueFileNameMap(part);
				if (optnameFileMap != null) {
					Map.Entry<String, String> entry = optnameFileMap.entrySet().iterator().next();
					optNameVal = entry.getKey();
					origFileName = entry.getValue();
					String[] optVal = optNameVal.split("_");
					String variantName = optVal[0];
					String optionvalue = optVal[1];

					Map<String, String> initMap = varoptmap.get(variantName);
					if (initMap.containsKey(optionvalue)) {
						String ext = FileUtil.getFileExtension(origFileName);
						String fileName = clId + "_" + System.currentTimeMillis() + "." + ext;
						part.write(tmpDir + File.separator + fileName);
						String s3bucket = SaaSAdminAppCache.s3staticbucket;
						String s3moduleFolder = SaaSAdminAppCache.s3inventoryoductsfolder;	
						String awsfolder= SaaSAdminAppCache.envfoldertype + "/" + s3moduleFolder +"/" + clId ;
						File tmpfile= new File(tmpDir + File.separator + fileName);
						AwsS3Response awsRsponse = AWSFileService.upLoadFileToS3(s3bucket,fileName, awsfolder, tmpfile );
						String serverLinkPath = SaaSAdminAppCache.cfstaticurl + "/" + awsfolder + "/"  + fileName;
						initMap.put(optionvalue, serverLinkPath);
						if(awsRsponse.getStatus() != 1) {
							throw new RTFServiceAccessException("Error Uploading file " + origFileName + " to S3 . Error is " +awsRsponse.getDesc());
						}
						varoptmap.put(variantName, initMap);
					}
				} 			
			}
			return varoptmap;
		} catch (Exception ex) {
				ex.printStackTrace();
		}
		return null;
	}

}
