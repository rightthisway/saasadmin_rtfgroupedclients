package com.rtf.inventory.common.util;

public class MessageConstant {
	
	
	public static final String INVALID_EMAIL = "Enter Valid Email.";
	public static final String INVALID_CLIENT = "Invalid Client Id";
	public static final String INVALID_USER = "User Id does Not Exist";
	
	
	
	
	
	public static final String INV_SAVE_ERROR = "[ Error Saving Inventory Data ]";
	public static final String INV_FILE_PARSE_ERROR = "[ERROR PARSING PRODUCT MASTER SHEET ]";
	public static final String VARIANT_SHEET_PARSE_ERROR = "[ERROR PARSING VARIANT NAME SHEET]"; 
	public static final String OPTVAL_SHEET_PARSE_ERROR = "[ERROR PARSING OPTION VALUE SHEET]";
	public static final String INV_UPLOAD_ERROR = "[Error Occured while Uploading Inventory DATA ]";
	public static final String INV_DATA_FILE_MENDATORY = "Inventory Data File is Mandatory";
	public static final String INV_DATA_UPLOADED = "Inventory DATA Uploaded Successfully";
	
	public static final String INVALID_SELLER = "Invalid seller ID";
	public static final String INVALID_PRODUCT_STATUS = "Invalid product status";
	public static final String INVALID_PRODUCT = "Invalid Seller Product Id";
	public static final String INVALID_PRODUCT_DESC = "Invalid Product Description";
	public static final String INVALID_PRODUCT_NAME = "Invalid Product Name";
	public static final String INVALID_SELL_PRICE = "Invalid Selling Price";
	public static final String INVALID_REG_PRICE= "Invalid Regular Selling Price";
	public static final String INVALID_SKU= "Invalid Product sku";
	public static final String INVALID_EXP_DATE = "Invalid Product Expiry Date";
	public static final String INVALID_VARIANT_FLAG = "Invalid Product Variant enabled  Flag";
	public static final String INVALID_MAX_QTY = "Invalid Max Cust Quantity";
	public static final String INVALID_DISP_DATE = "Invalid Display Start Date";
	public static final String INVALID_PROD_ID= "Invalid Product ID";
	public static final String PRODUCT_IMAGE_ERROR = "Error Uploading Product Image";
	public static final String PRODUCT_ERROR = "Error Occured while Creating the Product Item";
	public static final String PRODUCT_CREATED = "Product Item Created Successfully";
	public static final String PRODUCT_GET_ERROR = "Error Occured while Fetching the Product Items";
	public static final String PRODUCT_FETCHED_MSG="Product Item fetched Successfully";
	public static final String PRODUCT_UPDATE_ERROR="Error Occured while Updating the Product Item Status";
	public static final String PRODUCT_UPDATED="Product Item Status Updated Successfully";
	public static final String PRODUCT_EXIST = " Product With Given Product Ref. Id already Exists for Seller. Please Delete / Activate the existing Product ";
	public static final String PRODUCT_ITEM_UPDATED = "Product Item Updated Successfully";
	
	
	
	

}
