/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.inventory.common.util;

import java.io.File;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

import com.rtf.aws.AWSFileService;
import com.rtf.aws.AwsS3Response;
import com.rtf.common.util.SaaSAdminAppCache;
import com.rtf.inventory.common.dto.FileUploadGenericDTO;
import com.rtf.saas.util.FileUtil;
import com.rtf.saas.util.GenUtil;

/**
 * The Class ASWS3ImageUploaderUtil.
 */
public class ASWS3ImageUploaderUtil {
	
	/**
	 * Upload file to amazon S3 server in given bucket and folder.
	 *
	 * @param clId the cl id
	 * @param request the request
	 * @param dto the dto
	 * @param s3bucket the s 3 bucket
	 * @param s3modulefolder the s 3 modulefolder
	 * @return the file upload generic DTO
	 */
	public static FileUploadGenericDTO uploadFileToS3(String clId , HttpServletRequest request,FileUploadGenericDTO dto , String s3bucket,String s3modulefolder  ) {
		dto.setSts(0);
		String tmpDir = SaaSAdminAppCache.TMP_FOLDER;
		tmpDir = tmpDir + File.separator + clId + File.separator;
		File fileSaveDir = new File(tmpDir);
		if (!fileSaveDir.exists()) {
			fileSaveDir.mkdirs();
		}
		String origFileName = null;
		try {
			for (Part part : request.getParts()) {
				origFileName = FileUtil.getFileName(part);			
			}
		}catch(Exception ex) {
			dto.setSts(0);
			dto.setMsg(Messages.PRODUCT_IMAGE_FILE_UPLOAD_MANDATORY);			
			return dto;
		}
		if(GenUtil.isNullOrEmpty(origFileName)) {
			dto.setSts(0);
			dto.setMsg(Messages.PRODUCT_IMAGE_FILE_UPLOAD_MANDATORY);
			return dto;
		}
		
		String ext = FileUtil.getFileExtension(origFileName);			
		String fileName = clId + "_" + System.currentTimeMillis() + "." + ext;
		try {
			for (Part part : request.getParts()) {				
				part.write(tmpDir + File.separator + fileName);
			}
		}catch(Exception ex) {
			dto.setSts(0);
			dto.setMsg(ex.getLocalizedMessage());
			return dto;
		}
		
		String awsfolder= SaaSAdminAppCache.envfoldertype + "/" + s3modulefolder +"/" + clId ;
		
		File tmpfile= new File(tmpDir + File.separator + fileName);
		AwsS3Response awsRsponse = AWSFileService.upLoadFileToS3(s3bucket,fileName, awsfolder, tmpfile );
		
		if(awsRsponse == null) {
			dto.setSts(0);
			dto.setMsg(" Error Uploading to S3 ");
			return dto;
		}
		
		if(null != awsRsponse && awsRsponse.getStatus() != 1){
			dto.setSts(0);
			dto.setMsg(" Error Uploading to S3 " + awsRsponse.getDesc());
			return dto;			
		}						
		String serverLinkPath = SaaSAdminAppCache.cfstaticurl + "/" + awsfolder + "/"  + fileName;
		dto.setSts(1);
		dto.setAwsfinalUrl(serverLinkPath);
		return dto;
		
		
		
		
		
		
		
		
		
		
	}
	
	
	
	

}
