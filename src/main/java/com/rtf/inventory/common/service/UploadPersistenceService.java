/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.inventory.common.service;

import java.sql.Connection;
import java.util.List;

import com.rtf.inventory.common.dto.InventoryUploadDTO;
import com.rtf.inventory.common.util.MessageConstant;
import com.rtf.inventory.product.dao.ProductItemMasterCassDAO;
import com.rtf.inventory.product.dao.ProductItemMasterDAO;
import com.rtf.inventory.product.dvo.ProductItemMasterDVO;
import com.rtf.inventory.product.service.ProductItemMasterService;
import com.rtf.inventory.variants.dao.ProductVariantNameDAO;
import com.rtf.inventory.variants.dao.VariantInventoryDAO;
import com.rtf.inventory.variants.dvo.ProductInventoryDVO;
import com.rtf.inventory.variants.dvo.ProductItemVariantNameDVO;
import com.rtf.saas.sql.db.DatabaseConnections;

/**
 * The Class UploadPersistenceService.
 */
public class UploadPersistenceService {

	/**
	 * Save all product, variants and inventory data.
	 *
	 * @param dto the dto
	 * @return the inventory upload DTO
	 * @throws Exception the exception
	 */
	public static InventoryUploadDTO saveAllData(InventoryUploadDTO dto) throws Exception {

		List<ProductItemMasterDVO> prodItemList = dto.getPmasterdvolst();
		Connection conn = DatabaseConnections.getSaasAdminConnection();
		
		try {

			if (prodItemList != null && prodItemList.size() > 0) {
				saveProductItemMasterListData(prodItemList, conn );
			}
			/*
			 * List<ProductItemVariantNameDVO> vnlist = dto.getVariantdvolst(); if (vnlist
			 * != null && vnlist.size() > 0) { saveVariantNameData(vnlist, conn); }
			 * List<ProductItemVariantNameDVO> voptvallist = dto.getOptvaluedvolst(); if
			 * (voptvallist != null && voptvallist.size() > 0) {
			 * saveVariantOptValueData(voptvallist, conn); } List<ProductInventoryDVO>
			 * invlist = dto.getInvetorylst(); if (invlist != null && invlist.size() > 0) {
			 * saveInventoryValueData(invlist, conn); }
			 */

		} catch (Exception ex) {
			ex.printStackTrace();
			dto.setMsg(MessageConstant.INV_SAVE_ERROR+ ex.getLocalizedMessage());
			throw ex;
		} finally {
			DatabaseConnections.closeConnection(conn);
		}
		return dto;
	}

	/**
	 * Save inventory value data.
	 *
	 * @param invlist the invlist
	 * @param conn    the conn
	 * @throws Exception the exception
	 */
	private static void saveInventoryValueData(List<ProductInventoryDVO> invlist, Connection conn) throws Exception {
		try {
			for (ProductInventoryDVO dvo : invlist) {
				VariantInventoryDAO.saveInventoryRecordwithouAutoId(dvo, conn);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Save variant opt value data.
	 *
	 * @param voptvallist the voptvallist
	 * @param conn        the conn
	 * @throws Exception the exception
	 */
	private static void saveVariantOptValueData(List<ProductItemVariantNameDVO> voptvallist, Connection conn)
			throws Exception {
		try {
			for (ProductItemVariantNameDVO dvo : voptvallist) {
				ProductVariantNameDAO.createPrductVariantNameOptionValuewithoutAutoId(dvo, conn);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}

	}

	/**
	 * Save variant name data.
	 *
	 * @param vnlist the vnlist
	 * @param conn   the conn
	 * @throws Exception the exception
	 */
	private static void saveVariantNameData(List<ProductItemVariantNameDVO> vnlist, Connection conn) throws Exception {
		try {

			for (ProductItemVariantNameDVO dvo : vnlist) {
				ProductVariantNameDAO.createPrductVariantNamewithoutAutoId(dvo, conn);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}

	}

	/**
	 * Save product item master list data.
	 *
	 * @param prodItemList the prod item list
	 * @param conn         the conn
	 * @throws Exception the exception
	 */
	private static void saveProductItemMasterListData(List<ProductItemMasterDVO> prodItemList, Connection conn)
			throws Exception {

		try {
			for (ProductItemMasterDVO dvo : prodItemList) {
				
				Boolean itemExists = ProductItemMasterService.checkIfProdItemIdExistsForSellerProdId(dvo.getClintId(),dvo.getSlrpitmsId(),dvo.getSlerId());
				if(itemExists) {
					throw new Exception( MessageConstant.PRODUCT_EXIST + "[ " + dvo.getSlrpitmsId() +"]");
				}
				Integer pkId = 	ProductItemMasterDAO.createPrductItem(dvo, conn);				
				System.out.println("Product Cerated with ID " + pkId);
				if (pkId == null) {
					throw new Exception(" ERROR Creating Product Master Record");
				}
				dvo.setItemsId(pkId);
				ProductItemMasterCassDAO.createPrductItem(dvo);				
				System.out.println("Product Saved with ID in CASS " + pkId);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

}
