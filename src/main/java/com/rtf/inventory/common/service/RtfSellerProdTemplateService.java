/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.inventory.common.service;

import java.io.FileInputStream;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.rtf.inventory.common.dao.RtfSlrProdUpldTemplateDAO;
import com.rtf.inventory.common.dto.RtfSellerTemplateUploadDTO;
import com.rtf.inventory.common.util.MessageConstant;
import com.rtf.inventory.dvo.RtfSlrProdUpldTemplateDVO;
import com.rtf.saas.exception.RTFServiceAccessException;
import com.rtf.saas.sql.db.DatabaseConnections;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.PaginationUtil20;

/**
 * The Class RtfSellerProdTemplateService.
 */
public class RtfSellerProdTemplateService {

	/**
	 * Process inventory Template upload data excel file.
	 *
	 * @param dto      the dto
	 * @param sellerId the seller id
	 * @return the inventory upload DTO
	 * @throws Exception the exception
	 */
	public static RtfSellerTemplateUploadDTO processInventoryUploadDataFile(RtfSellerTemplateUploadDTO dto,
			Integer sellerId) throws Exception {
		try {
			dto = parseExcelFile(dto);
			saveProductTemplateListData(dto.getTmplateLst(), sellerId, dto.getCau());
			dto.setSts(1);
		} catch (Exception ex) {
			dto.setSts(0);
			dto.setMsg(ex.getLocalizedMessage());
		}
		return dto;
	}

	/**
	 * Parses the inventory excel file.
	 *
	 * @param dto the dto
	 * @return the inventory upload DTO
	 * @throws Exception the exception
	 */
	public static RtfSellerTemplateUploadDTO parseExcelFile(RtfSellerTemplateUploadDTO dto) throws Exception {
		try {
			FileInputStream fis = new FileInputStream(dto.getUploadedFile());
			XSSFWorkbook wb = new XSSFWorkbook(fis);
			prepareInventoryMasterVO(wb.getSheetAt(0), dto);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return dto;
	}

	/**
	 * Prepare inventory master data object list from file.
	 *
	 * @param sheet the sheet
	 * @param dto   the dto
	 * @throws Exception the exception
	 */
	private static void prepareInventoryMasterVO(XSSFSheet sheet, RtfSellerTemplateUploadDTO dto) throws Exception {
		try {
			Iterator<Row> itr = sheet.iterator();

			List<RtfSlrProdUpldTemplateDVO> pilist = new ArrayList<RtfSlrProdUpldTemplateDVO>();
			while (itr.hasNext()) {
				Row row = itr.next();
				if (row.getRowNum() == 0)
					continue;
				RtfSlrProdUpldTemplateDVO dvo = populateProductTemplateVO(row, dto);
				pilist.add(dvo);
			}
			dto.setTmplateLst(pilist);
		} catch (Exception ex) {
			dto.setMsg(MessageConstant.INV_FILE_PARSE_ERROR + dto.getMsg());
			throw ex;
		}

	}

	/**
	 * Populate product items data object
	 *
	 * @param row the row
	 * @param dto the dto
	 * @return the product item master DVO
	 * @throws Exception the exception
	 */
	private static RtfSlrProdUpldTemplateDVO populateProductTemplateVO(Row row, RtfSellerTemplateUploadDTO dto)
			throws Exception {

		RtfSlrProdUpldTemplateDVO dvo = new RtfSlrProdUpldTemplateDVO();
		Integer integerval = null;
		Double douibleVal = null;
		String stringVal = null;

		/**
		 * Product Name Product Brand Product Short Description Product Full Description
		 * Regular Price Selling Price "% Off" On Selling Price Available Quantity
		 * Variant Name Variant Value
		 **/

		stringVal = fetchMandatoryStringVallue(row, String.valueOf((Object) row.getCell(0)), dto, 0);
		dvo.setpName(stringVal);

		stringVal = fetchMandatoryStringVallue(row, String.valueOf((Object) row.getCell(1)), dto, 1);
		dvo.setpBrand(stringVal);
		stringVal = fetchMandatoryStringVallue(row, String.valueOf((Object) row.getCell(2)), dto, 2);
		dvo.setpDesc(stringVal);
		stringVal = fetchMandatoryStringVallue(row, String.valueOf((Object) row.getCell(3)), dto, 3);
		dvo.setpLongDesc(stringVal);

		douibleVal = fetchDoubleValue(row, String.valueOf((Object) row.getCell(4)), dto, 4);
		dvo.setRegPrc(douibleVal);
		douibleVal = fetchDoubleValue(row, String.valueOf((Object) row.getCell(5)), dto, 5);
		dvo.setSelPrc(douibleVal);

		douibleVal = fetchDoubleValue(row, String.valueOf((Object) row.getCell(6)), dto, 6);
		dvo.setDiscOff(douibleVal);

		Cell c = row.getCell(7);
		c.setCellType(CellType.STRING);
		integerval = fetchIntegerValue(row, String.valueOf((Object) c), dto, 7);
		dvo.setAvilQty(integerval);

		stringVal = fetchMandatoryStringVallue(row, String.valueOf((Object) row.getCell(8)), dto, 8);
		dvo.setVarNameA(stringVal);
		stringVal = fetchMandatoryStringVallue(row, String.valueOf((Object) row.getCell(9)), dto, 9);
		dvo.setVarValueA(stringVal);
		return dvo;

	}

	/**
	 * Save product item master list data.
	 *
	 * @param prodItemList the prod item list
	 * @param sellerId     the seller id
	 * @throws Exception the exception
	 */
	private static void saveProductTemplateListData(List<RtfSlrProdUpldTemplateDVO> prodItemList, Integer sellerId,
			String userId) throws Exception {
		Connection conn = null;
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			for (RtfSlrProdUpldTemplateDVO dvo : prodItemList) {
				dvo.setSlrID(sellerId);
				dvo.setUpdBy(userId);
				RtfSlrProdUpldTemplateDAO.createSellerProductTemplateRecords(dvo, conn);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		} finally {

			if (conn != null) {
				DatabaseConnections.closeConnection(conn);
			}
		}
	}

	/**
	 * Fetch mandatory string value.
	 *
	 * @param row     the row
	 * @param cellval the cellval
	 * @param dto     the dto
	 * @param cellNo  the cell no
	 * @return the string
	 * @throws Exception the exception
	 */
	private static String fetchMandatoryStringVallue(Row row, String cellval, RtfSellerTemplateUploadDTO dto,
			Integer cellNo) throws Exception {

		try {
			if (GenUtil.isEmptyString(cellval) || "null".equalsIgnoreCase(cellval)) {
				throw new RTFServiceAccessException("Invalid data received.");
			}
			return cellval;
		} catch (Exception ex) {
			dto.setSts(0);
			dto.setMsg(" Empty / Invalid  String/Text Value [ " + cellval + " ]  AT ROW NUMBER [ " + row.getRowNum()
					+ " ] CellNo [ " + cellNo + " ]");
			throw new RTFServiceAccessException("Invalid data received.", ex);
		}
	}

	/**
	 * Fetch integer value.
	 *
	 * @param row     the row
	 * @param cellval the cellval
	 * @param dto     the dto
	 * @param cellNo  the cell no
	 * @return the integer
	 * @throws Exception the exception
	 */
	private static Integer fetchIntegerValue(Row row, String cellval, RtfSellerTemplateUploadDTO dto, Integer cellNo)
			throws Exception {
		Integer integerval;
		try {
			System.out.println("cellval " + cellval);
			integerval = Integer.valueOf(cellval);
		} catch (Exception ex) {
			dto.setSts(0);
			dto.setMsg(" Invalid Integer Value [ " + cellval + " ]  AT ROW NUMBER [ " + row.getRowNum() + " ] CellNo [ "
					+ cellNo + " ]");
			throw ex;
		}
		return integerval;
	}

	/**
	 * Fetch integer value if not null.
	 *
	 * @param row     the row
	 * @param cellval the cellval
	 * @param dto     the dto
	 * @param cellNo  the cell no
	 * @return the integer
	 * @throws Exception the exception
	 */
	private static Integer fetchIntegerValueIfNotNull(Row row, String cellval, RtfSellerTemplateUploadDTO dto,
			Integer cellNo) throws Exception {
		Integer integerval;
		if (GenUtil.isEmptyString(cellval) || "null".equalsIgnoreCase(cellval)) {
			return null;
		}
		try {
			integerval = Integer.valueOf(cellval);
		} catch (Exception ex) {
			dto.setSts(0);
			dto.setMsg(" Invalid Integer Value [ " + cellval + " ]  AT ROW NUMBER [ " + row.getRowNum() + " ] CellNo [ "
					+ cellNo + " ]");
			throw ex;
		}
		return integerval;
	}

	/**
	 * Fetch double value.
	 *
	 * @param row     the row
	 * @param cellval the cellval
	 * @param dto     the dto
	 * @param cellNo  the cell no
	 * @return the double
	 * @throws Exception the exception
	 */
	private static Double fetchDoubleValue(Row row, String cellval, RtfSellerTemplateUploadDTO dto, Integer cellNo)
			throws Exception {
		Double doubleval;
		try {
			System.out.println("cellval " + cellval);
			doubleval = Double.valueOf(cellval);
		} catch (Exception ex) {
			dto.setSts(0);
			dto.setMsg(" Invalid Double Value [ " + cellval + " ]  AT ROW NUMBER [ " + row.getRowNum() + " ] CellNo [ "
					+ cellNo + " ]");
			throw ex;
		}
		return doubleval;
	}

	

	public static RtfSellerTemplateUploadDTO fetchRtfSlrProdUpldTemplateDVOs(Integer sellerId, String filter, String pgNo,
			String cau, RtfSellerTemplateUploadDTO dto) throws Exception {
		Connection conn = null;
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			List<RtfSlrProdUpldTemplateDVO> list = RtfSlrProdUpldTemplateDAO
					.getAllSellerProductTemplateRecordsByFilter(sellerId, filter, pgNo, conn);
			Integer count = RtfSlrProdUpldTemplateDAO.getAllSellerProductTemplateRecordsCountByFilter(sellerId, filter,
					conn);
			dto.setPagination(PaginationUtil20.calculatePaginationParameter(count, pgNo));
			dto.setTmplateLst(list);
			dto.setSts(1);
			return dto;
		}  catch (Exception e) {
			e.printStackTrace();
			dto.setSts(0);
		} finally {
			try {
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return dto;
	}
	
	
	/**
	 * Update seller status for PK.
	 *
	 * @param dto the dto
	 * @return the seller DTO
	 * @throws Exception the exception
	 */
	
	public static RtfSellerTemplateUploadDTO updateTemplateImages(RtfSellerTemplateUploadDTO dto,RtfSlrProdUpldTemplateDVO dvo) throws Exception {
		Connection conn = null;	
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			Integer updCnt = RtfSlrProdUpldTemplateDAO.updateProductImage(dvo, conn);
			dto.setSts(updCnt);
			
			System.out.println(" update record for " + dvo.getSputID());
		} catch (Exception ex) {
			dto.setSts(0);
			ex.printStackTrace();			
		} finally {
			DatabaseConnections.closeConnection(conn);
		}
		return dto;
	}

	public static RtfSellerTemplateUploadDTO updateRtfSellerProdTemplate(RtfSlrProdUpldTemplateDVO dvo,
			RtfSellerTemplateUploadDTO dto)  throws Exception{
		Connection conn = null;
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			Integer updCnt = RtfSlrProdUpldTemplateDAO.updateRtfSellerProdTemplate(dvo, conn);
			dto.setSts(updCnt);
			System.out.println(" update record for " + dvo.getSputID());
		} catch (Exception ex) {
			dto.setSts(0);
			ex.printStackTrace();
		} finally {
			DatabaseConnections.closeConnection(conn);
		}
		return dto;
	}

	public static RtfSellerTemplateUploadDTO deleteRtfSellerProdTemplate(RtfSlrProdUpldTemplateDVO dvo,
			RtfSellerTemplateUploadDTO dto) {
		Connection conn = null;
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			Integer updCnt = RtfSlrProdUpldTemplateDAO.deleteRtfSellerProdTemplate(dvo, conn);
			dto.setSts(updCnt);
			System.out.println(" update record for " + dvo.getSputID());
		} catch (Exception ex) {
			dto.setSts(0);
			ex.printStackTrace();
		} finally {
			DatabaseConnections.closeConnection(conn);
		}
		return dto;
	}
	
	
	/**
	 * Save product item master list data.
	 *
	 * @param prodItemList the prod item list
	 * @param sellerId     the seller id
	 * @throws Exception the exception
	 */
	
	public static boolean saveProductTemplateData(RtfSlrProdUpldTemplateDVO dvo) throws Exception {
		Connection conn = null;
		try {
			    conn = DatabaseConnections.getSaasAdminConnection();
				RtfSlrProdUpldTemplateDAO.createSellerProductTemplateRecords(dvo, conn);
				return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		} finally {
			if (conn != null) {
				DatabaseConnections.closeConnection(conn);
			}
		}
		
	}
}