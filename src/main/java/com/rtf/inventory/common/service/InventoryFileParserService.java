/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.inventory.common.service;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.rtf.inventory.common.dto.InventoryUploadDTO;
import com.rtf.inventory.common.util.MessageConstant;
import com.rtf.inventory.product.dvo.ProductItemMasterDVO;
import com.rtf.inventory.variants.dvo.ProductInventoryDVO;
import com.rtf.inventory.variants.dvo.ProductItemVariantNameDVO;
import com.rtf.saas.exception.RTFServiceAccessException;
import com.rtf.saas.util.GenUtil;

/**
 * The Class InventoryFileParserService.
 */
public class InventoryFileParserService {

	/**
	 * Process inventory data file and save it.
	 *
	 * @param dto the dto
	 * @return the inventory upload DTO
	 * @throws Exception the exception
	 */
	public static InventoryUploadDTO processInventoryUploadDataFile(InventoryUploadDTO dto) throws Exception {
		try {
			dto = parseExcelFile(dto);
			dto = UploadPersistenceService.saveAllData(dto);
			dto.setSts(1);
		} catch (Exception ex) {
			dto.setSts(0);
		}
		return dto;
	}

	/**
	 * Parse and process inventory data excel file
	 *
	 * @param dto the dto
	 * @return the inventory upload DTO
	 * @throws Exception the exception
	 */
	public static InventoryUploadDTO parseExcelFile(InventoryUploadDTO dto) throws Exception {
		try {

			FileInputStream fis = new FileInputStream(dto.getUploadedFile());
			XSSFWorkbook wb = new XSSFWorkbook(fis);
			prepareInventoryMasterVO(wb.getSheetAt(0), dto);
			//prepareVariantNameVO(wb.getSheetAt(1), dto);
			//prepareOptionValueVO(wb.getSheetAt(2), dto);
			//prepareInventoryVO(wb.getSheetAt(3), dto);

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return dto;
	}

	/**
	 * Create the inventory data objects list.
	 *
	 * @param sheet the sheet
	 * @param dto   the dto
	 * @throws Exception the exception
	 */
	private static void prepareInventoryMasterVO(XSSFSheet sheet, InventoryUploadDTO dto) throws Exception {
		try {
			Iterator<Row> itr = sheet.iterator();
			List<ProductItemMasterDVO> pilist = new ArrayList<ProductItemMasterDVO>();
			while (itr.hasNext()) {
				Row row = itr.next();
				if (row.getRowNum() == 0)
					continue; 
				ProductItemMasterDVO dvo = populateProductItemsMasterVO(row, dto);
				
				System.out.println(" Parse DVO from excel is " + dvo);
				pilist.add(dvo);
			}
			dto.setPmasterdvolst(pilist);
		} catch (Exception ex) {
			dto.setMsg(MessageConstant.INV_FILE_PARSE_ERROR + dto.getMsg());
			throw ex;
		}

	}

	/**
	 * Create the product variant data object list.
	 *
	 * @param sheet the sheet
	 * @param dto   the dto
	 * @throws Exception the exception
	 */
	private static void prepareVariantNameVO(XSSFSheet sheet, InventoryUploadDTO dto) throws Exception {
		try {
			Iterator<Row> itr = sheet.iterator();
			List<ProductItemVariantNameDVO> lst = new ArrayList<ProductItemVariantNameDVO>();
			while (itr.hasNext()) {
				Row row = itr.next();
				if (row.getRowNum() == 0)
					continue; // Header
				ProductItemVariantNameDVO dvo = populateProductVariantNames(row, dto);
				lst.add(dvo);
			}
			dto.setVariantdvolst(lst);
		} catch (Exception ex) {
			dto.setMsg(MessageConstant.VARIANT_SHEET_PARSE_ERROR + dto.getMsg());
			throw ex;
		}

	}

	/**
	 * Create the product variant data object list.
	 *
	 * @param sheet the sheet
	 * @param dto   the dto
	 * @throws Exception the exception
	 */
	private static void prepareOptionValueVO(XSSFSheet sheet, InventoryUploadDTO dto) throws Exception {
		try {
			Iterator<Row> itr = sheet.iterator();
			List<ProductItemVariantNameDVO> lst = new ArrayList<ProductItemVariantNameDVO>();
			while (itr.hasNext()) {
				Row row = itr.next();
				if (row.getRowNum() == 0)
					continue; // Header
				ProductItemVariantNameDVO dvo = populateVariantOptions(row, dto);
				lst.add(dvo);
			}
			dto.setOptvaluedvolst(lst);
		} catch (Exception ex) {
			dto.setMsg(MessageConstant.OPTVAL_SHEET_PARSE_ERROR + dto.getMsg());
			throw ex;
		}

	}

	/**
	 * Create inventory data object list.
	 *
	 * @param sheet the sheet
	 * @param dto   the dto
	 * @throws Exception the exception
	 */
	private static void prepareInventoryVO(XSSFSheet sheet, InventoryUploadDTO dto) throws Exception {
		try {
			Iterator<Row> itr = sheet.iterator();
			List<ProductInventoryDVO> lst = new ArrayList<ProductInventoryDVO>();
			while (itr.hasNext()) {
				Row row = itr.next();
				if (row.getRowNum() == 0)
					continue; 
				ProductInventoryDVO dvo = populateInventory(row, dto);
				lst.add(dvo);
			}
			dto.setInvetorylst(lst);
		} catch (Exception ex) {
			dto.setMsg(MessageConstant.INV_FILE_PARSE_ERROR+ dto.getMsg());
			throw ex;
		}

	}

	/**
	 * Create product item data object list.
	 *
	 * @param row the row
	 * @param dto the dto
	 * @return the product item master DVO
	 * @throws Exception the exception
	 */
	private static ProductItemMasterDVO populateProductItemsMasterVO(Row row, InventoryUploadDTO dto) throws Exception {
		ProductItemMasterDVO dvo = new ProductItemMasterDVO();
		Integer integerval = null;
		Double douibleVal = null;
		String stringVal = null;
	
	
		Cell intcell =  row.getCell(0);
		intcell.setCellType(Cell.CELL_TYPE_STRING);	
	
		integerval = fetchIntegerValue(row, String.valueOf((Object) intcell), dto, 1);
		dvo.setSlerId(integerval);
		
		Cell refProdIdCell =  row.getCell(1);
		refProdIdCell.setCellType(Cell.CELL_TYPE_STRING);	
		
		stringVal = GenUtil.getEmptyStringIfNull(String.valueOf((Object) refProdIdCell));
		dvo.setSlrpitmsId(stringVal);
		
		stringVal = fetchMandatoryStringVallue(row, String.valueOf((Object) row.getCell(2)), dto, 3);
		dvo.setPname(stringVal);
		
		dvo.setPbrand(String.valueOf((Object) row.getCell(3)));
		dvo.setPdesc(String.valueOf((Object) row.getCell(4)));
		dvo.setPlongDesc(String.valueOf((Object) row.getCell(5)));
		
		douibleVal = fetchDoubleValueIfValid(row, String.valueOf((Object) row.getCell(6)), dto, 7);
		dvo.setPregMinPrc(douibleVal);
		
		douibleVal = fetchDoubleValueIfValid(row, String.valueOf((Object) row.getCell(7)), dto, 8);
		dvo.setPselMinPrc(douibleVal);		
		
		stringVal = GenUtil.getEmptyStringIfNull(String.valueOf((Object) row.getCell(8)));
		dvo.setProdPriceDtl(stringVal);
		
		stringVal = GenUtil.getEmptyStringIfNull(String.valueOf((Object) row.getCell(9)));
		dvo.setPriceType(stringVal);	
		
	
		stringVal = fetchMandatoryStringVallue(row, String.valueOf((Object) row.getCell(10)), dto, 11);
		dvo.setPimg(stringVal);
		
		
		dvo.setPstatus("ACTIVE");
		dvo.setLupdBy(dto.getCau());		
		dvo.setClintId(dto.getClId());
		return dvo;
	}

	/**
	 * Populate product variant names.
	 *
	 * @param row the row
	 * @param dto the dto
	 * @return the product item variant name DVO
	 * @throws Exception the exception
	 */
	private static ProductItemVariantNameDVO populateProductVariantNames(Row row, InventoryUploadDTO dto)
			throws Exception {
		ProductItemVariantNameDVO dvo = new ProductItemVariantNameDVO();
		Integer integerval = null;
		String stringVal = null;
		integerval = fetchIntegerValue(row, String.valueOf((Object) row.getCell(0)), dto, 0);
		dvo.setSlrpivarId(integerval);
		stringVal = fetchMandatoryStringVallue(row, String.valueOf((Object) row.getCell(1)), dto, 1);
		dvo.setSlrpitmsId(stringVal);
		stringVal = fetchMandatoryStringVallue(row, String.valueOf((Object) row.getCell(2)), dto, 2);
		dvo.setSlerId(stringVal);
		stringVal = fetchMandatoryStringVallue(row, String.valueOf((Object) row.getCell(3)), dto, 3);
		dvo.setVoptname(stringVal);
		dvo.setLupdBy(dto.getCau());
		dvo.setClintId(dto.getClId());
		return dvo;
	}

	/**
	 * Populate product variant options.
	 *
	 * @param row the row
	 * @param dto the dto
	 * @return the product item variant name DVO
	 * @throws Exception the exception
	 */
	private static ProductItemVariantNameDVO populateVariantOptions(Row row, InventoryUploadDTO dto) throws Exception {

		ProductItemVariantNameDVO dvo = new ProductItemVariantNameDVO();
		Integer integerval = null;
		String stringVal = null;

		integerval = fetchIntegerValue(row, String.valueOf((Object) row.getCell(0)), dto, 0);
		dvo.setVarovalId(integerval);
		integerval = fetchIntegerValue(row, String.valueOf((Object) row.getCell(1)), dto, 1);
		dvo.setSlrpivarId(integerval);
		stringVal = fetchMandatoryStringVallue(row, String.valueOf((Object) row.getCell(2)), dto, 2);
		dvo.setVoptvalue(stringVal);
		stringVal = GenUtil.getEmptyStringIfNull(String.valueOf((Object) row.getCell(2)));
		dvo.setVoptvalueIimgurl(stringVal);
		dvo.setLupdBy(dto.getCau());
		dvo.setClintId(dto.getClId());
		return dvo;
	}

	/**
	 * Populate product inventory.
	 *
	 * @param row the row
	 * @param dto the dto
	 * @return the product inventory DVO
	 * @throws Exception the exception
	 */
	private static ProductInventoryDVO populateInventory(Row row, InventoryUploadDTO dto) throws Exception {
		Integer integerval = null;
		Double douibleVal = null;
		String stringVal = null;

		ProductInventoryDVO dvo = new ProductInventoryDVO();

		integerval = fetchIntegerValue(row, String.valueOf((Object) row.getCell(0)), dto, 0);
		dvo.setSlrinvId(integerval);

		integerval = fetchIntegerValue(row, String.valueOf((Object) row.getCell(1)), dto, 1);
		dvo.setSlrpivarcomId(integerval);

		integerval = fetchIntegerValue(row, String.valueOf((Object) row.getCell(2)), dto, 2);
		dvo.setSetuniqId(integerval);

		stringVal = fetchMandatoryStringVallue(row, String.valueOf((Object) row.getCell(3)), dto, 3);

		dvo.setSlrpitmsId(stringVal);

		integerval = fetchIntegerValue(row, String.valueOf((Object) row.getCell(4)), dto, 4);
		dvo.setSlerId(integerval);

		stringVal = GenUtil.getEmptyStringIfNull(String.valueOf((Object) row.getCell(5)));
		dvo.setPbrand(stringVal);

		stringVal = GenUtil.getEmptyStringIfNull(String.valueOf((Object) row.getCell(6)));
		dvo.setVoptnamevalcom(stringVal);

		stringVal = fetchMandatoryStringVallue(row, String.valueOf((Object) row.getCell(7)), dto, 7);

		dvo.setPname(stringVal);

		douibleVal = fetchDoubleValue(row, String.valueOf((Object) row.getCell(8)), dto, 8);
		dvo.setPregprc(douibleVal);

		douibleVal = fetchDoubleValue(row, String.valueOf((Object) row.getCell(9)), dto, 9);
		dvo.setPselprc(douibleVal);

		integerval = fetchIntegerValue(row, String.valueOf((Object) row.getCell(10)), dto, 10);
		dvo.setReqltypnts(integerval);

		douibleVal = fetchDoubleValue(row, String.valueOf((Object) row.getCell(11)), dto, 11);
		dvo.setPprcaftltypnts(douibleVal);

		integerval = fetchIntegerValue(row, String.valueOf((Object) row.getCell(12)), dto, 12);
		dvo.setAvlqty(integerval);
		integerval = fetchIntegerValue(row, String.valueOf((Object) row.getCell(13)), dto, 13);
		dvo.setSldqty(integerval);
		stringVal = fetchMandatoryStringVallue(row, String.valueOf((Object) row.getCell(24)), dto, 24);
		dvo.setPrdstatus(stringVal);
		stringVal = fetchMandatoryStringVallue(row, String.valueOf((Object) row.getCell(25)), dto, 25);
		dvo.setPimg(stringVal);

		stringVal = GenUtil.getEmptyStringIfNull(String.valueOf((Object) row.getCell(29)));
		dvo.setVonameone(stringVal);
		stringVal = GenUtil.getEmptyStringIfNull(String.valueOf((Object) row.getCell(30)));
		dvo.setVovalueone(stringVal);

		stringVal = GenUtil.getEmptyStringIfNull(String.valueOf((Object) row.getCell(31)));
		dvo.setVonametwo(stringVal);
		stringVal = GenUtil.getEmptyStringIfNull(String.valueOf((Object) row.getCell(32)));
		dvo.setVovaluetwo(stringVal);

		stringVal = GenUtil.getEmptyStringIfNull(String.valueOf((Object) row.getCell(33)));
		dvo.setVonamethree(stringVal);
		stringVal = GenUtil.getEmptyStringIfNull(String.valueOf((Object) row.getCell(34)));
		dvo.setVovaluethree(stringVal);

		stringVal = GenUtil.getEmptyStringIfNull(String.valueOf((Object) row.getCell(35)));
		dvo.setVonamefour(stringVal);
		stringVal = GenUtil.getEmptyStringIfNull(String.valueOf((Object) row.getCell(36)));
		dvo.setVovaluefour(stringVal);

		integerval = fetchIntegerValueIfNotNull(row, String.valueOf((Object) row.getCell(37)), dto, 37);
		dvo.setSlrpivarIdone(integerval);
		integerval = fetchIntegerValueIfNotNull(row, String.valueOf((Object) row.getCell(38)), dto, 38);
		dvo.setVarovalIdone(integerval);

		integerval = fetchIntegerValueIfNotNull(row, String.valueOf((Object) row.getCell(39)), dto, 39);
		dvo.setSlrpivarIdtwo(integerval);
		integerval = fetchIntegerValueIfNotNull(row, String.valueOf((Object) row.getCell(40)), dto, 40);
		dvo.setVarovalIdtwo(integerval);

		integerval = fetchIntegerValueIfNotNull(row, String.valueOf((Object) row.getCell(41)), dto, 41);
		dvo.setSlrpivarIdthree(integerval);
		integerval = fetchIntegerValueIfNotNull(row, String.valueOf((Object) row.getCell(42)), dto, 42);
		dvo.setVarovalIdthree(integerval);

		integerval = fetchIntegerValueIfNotNull(row, String.valueOf((Object) row.getCell(43)), dto, 43);
		dvo.setSlrpivarIdfour(integerval);
		integerval = fetchIntegerValueIfNotNull(row, String.valueOf((Object) row.getCell(44)), dto, 44);
		dvo.setVarovalIdfour(integerval);

		dvo.setLupdBy(dto.getCau());
		dvo.setClintId(dto.getClId());
		return dvo;
	}

	/**
	 * Fetch mandatory string vallue.
	 *
	 * @param row     the row
	 * @param cellval the cellval
	 * @param dto     the dto
	 * @param cellNo  the cell no
	 * @return the string
	 * @throws Exception the exception
	 */
	private static String fetchMandatoryStringVallue(Row row, String cellval, InventoryUploadDTO dto, Integer cellNo)
			throws Exception {

		try {
			if (GenUtil.isEmptyString(cellval) || "null".equalsIgnoreCase(cellval)) {
				throw new RTFServiceAccessException("Invalid data received.");
			}
			return cellval;
		} catch (Exception ex) {
			dto.setSts(0);
			dto.setMsg(" Empty / Invalid  String/Text Value [ " + cellval + " ]  AT ROW NUMBER [ " + row.getRowNum()
					+ " ] CellNo [ " + cellNo + " ]");
			throw new RTFServiceAccessException("Invalid data received.", ex);
		}
	}

	/**
	 * Fetch integer value.
	 *
	 * @param row     the row
	 * @param cellval the cellval
	 * @param dto     the dto
	 * @param cellNo  the cell no
	 * @return the integer
	 * @throws Exception the exception
	 */
	private static Integer fetchIntegerValue(Row row, String cellval, InventoryUploadDTO dto, Integer cellNo)
			throws Exception {
		Integer integerval;
		try {
			integerval = Integer.valueOf(cellval);
		} catch (Exception ex) {
			dto.setSts(0);
			dto.setMsg(" Invalid Integer Value [ " + cellval + " ]  AT ROW NUMBER [ " + row.getRowNum() + " ] CellNo [ "
					+ cellNo + " ]");
			throw ex;
		}
		return integerval;
	}

	/**
	 * Fetch integer value if not null.
	 *
	 * @param row     the row
	 * @param cellval the cellval
	 * @param dto     the dto
	 * @param cellNo  the cell no
	 * @return the integer
	 * @throws Exception the exception
	 */
	private static Integer fetchIntegerValueIfNotNull(Row row, String cellval, InventoryUploadDTO dto, Integer cellNo)
			throws Exception {
		Integer integerval;
		if (GenUtil.isEmptyString(cellval) || "null".equalsIgnoreCase(cellval)) {
			return null;
		}
		try {
			integerval = Integer.valueOf(cellval);
		} catch (Exception ex) {
			dto.setSts(0);
			dto.setMsg(" Invalid Integer Value [ " + cellval + " ]  AT ROW NUMBER [ " + row.getRowNum() + " ] CellNo [ "
					+ cellNo + " ]");
			throw ex;
		}
		return integerval;
	}

	/**
	 * Fetch double value.
	 *
	 * @param row     the row
	 * @param cellval the cellval
	 * @param dto     the dto
	 * @param cellNo  the cell no
	 * @return the double
	 * @throws Exception the exception
	 */
	private static Double fetchDoubleValue(Row row, String cellval, InventoryUploadDTO dto, Integer cellNo)
			throws Exception {
		Double doubleval;
		try {
			doubleval = Double.valueOf(cellval);
		} catch (Exception ex) {
			dto.setSts(0);
			dto.setMsg(" Invalid Double Value [ " + cellval + " ]  AT ROW NUMBER [ " + row.getRowNum() + " ] CellNo [ "
					+ cellNo + " ]");
			throw ex;
		}
		return doubleval;
	}
	/**
	 * Fetch double value.
	 *
	 * @param row     the row
	 * @param cellval the cellval
	 * @param dto     the dto
	 * @param cellNo  the cell no
	 * @return the double
	 * @throws Exception the exception
	 */
	private static Double fetchDoubleValueIfValid(Row row, String cellval, InventoryUploadDTO dto, Integer cellNo)
			throws Exception {
	
		Double doubleval;
		try {
			doubleval = Double.valueOf(cellval);
		} catch (Exception ex) {
			dto.setSts(0);
			dto.setMsg(" Invalid Double Value [ " + cellval + " ]  AT ROW NUMBER [ " + row.getRowNum() + " ] CellNo [ "
					+ cellNo + " ] Set this to 0 if you are using the free text field- (Price details )  for Price");
			throw ex;
		}
		return doubleval;
	}
	
	

	/**
	 * Fetch boolean value.
	 *
	 * @param row     the row
	 * @param cellval the cellval
	 * @param dto     the dto
	 * @param cellNo  the cell no
	 * @return the boolean
	 * @throws Exception the exception
	 */
	private static Boolean fetchBooleanValue(Row row, String cellval, InventoryUploadDTO dto, Integer cellNo)
			throws Exception {
		Boolean boolval = false;
		try {
			if (GenUtil.isEmptyString(cellval)) {
				throw new RTFServiceAccessException("Invalid data received.");
			}
			if (cellval.equals("1"))
				return true;
		} catch (Exception ex) {
			dto.setSts(0);
			dto.setMsg(" Invalid Boolean Value [ " + cellval + " ]  AT ROW NUMBER [ " + row.getRowNum() + " ] CellNo [ "
					+ cellNo + " ]");
			throw new RTFServiceAccessException("Invalid data received.",ex);
		}
		return boolval;
	}
	
	
	public static void main(String[] args) throws Exception  {
		
		File uploadedFile = new File("C:\\Users\\91903\\Desktop\\RTF-CONTEST\\XLS_UPLOADS\\DAILY_PRODUCTS\\0425\\APR_25_SAAS.xlsx");
		InventoryUploadDTO dto = new InventoryUploadDTO();
		dto.setUploadedFile(uploadedFile);
		dto.setClId("Lloyd cLIENT");
		dto.setCau("lloyd");
		dto.setSts(0);
		
		dto = InventoryFileParserService.processInventoryUploadDataFile(dto);
		
		System.out.println(" Completed Parsing " + dto.getMsg());
		
	}

}