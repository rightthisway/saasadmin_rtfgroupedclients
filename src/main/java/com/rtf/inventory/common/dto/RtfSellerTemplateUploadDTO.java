/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.inventory.common.dto;

import java.io.File;
import java.util.List;

import com.rtf.inventory.dvo.RtfSlrProdUpldTemplateDVO;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.dto.PaginationDTO;

/**
 * The Class RtfSellerTemplateUploadDTO.
 */

public class RtfSellerTemplateUploadDTO extends RtfSaasBaseDTO{

	/** The sts. */
	private Integer sts;

	/** The msg. */
	private String msg;

	/** The aws final url. */
	private String awsfinalUrl;
	
	private List<RtfSlrProdUpldTemplateDVO> tmplateLst;
	
	/** The uploaded file. */
	private File uploadedFile;

	/** The cau. */
	private String cau;
	
	private Integer slrID;
	
	/** The pagination. */
	private PaginationDTO pagination;
	/**
	 * Gets the sts.
	 *
	 * @return the sts
	 */
	private RtfSlrProdUpldTemplateDVO dvo;
	
	public Integer getSts() {
		return sts;
	}

	/**
	 * Sets the sts.
	 *
	 * @param sts the new sts
	 */
	public void setSts(Integer sts) {
		this.sts = sts;
	}

	/**
	 * Gets the msg.
	 *
	 * @return the msg
	 */
	public String getMsg() {
		return msg;
	}

	/**
	 * Sets the msg.
	 *
	 * @param msg the new msg
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}

	/**
	 * Gets the aws final url.
	 *
	 * @return the awsfinal url
	 */
	public String getAwsfinalUrl() {
		return awsfinalUrl;
	}

	/**
	 * Sets the awsfinal url.
	 *
	 * @param awsfinalUrl the new awsfinal url
	 */
	public void setAwsfinalUrl(String awsfinalUrl) {
		this.awsfinalUrl = awsfinalUrl;
	}

	public List<RtfSlrProdUpldTemplateDVO> getTmplateLst() {
		return tmplateLst;
	}

	public void setTmplateLst(List<RtfSlrProdUpldTemplateDVO> tmplateLst) {
		this.tmplateLst = tmplateLst;
	}

	public File getUploadedFile() {
		return uploadedFile;
	}

	public void setUploadedFile(File uploadedFile) {
		this.uploadedFile = uploadedFile;
	}

	public String getCau() {
		return cau;
	}

	public void setCau(String cau) {
		this.cau = cau;
	}

	public Integer getSlrID() {
		return slrID;
	}

	public void setSlrID(Integer slrID) {
		this.slrID = slrID;
	}

	public PaginationDTO getPagination() {
		return pagination;
	}

	public void setPagination(PaginationDTO pagination) {
		this.pagination = pagination;
	}

	public RtfSlrProdUpldTemplateDVO getDvo() {
		return dvo;
	}

	public void setDvo(RtfSlrProdUpldTemplateDVO dvo) {
		this.dvo = dvo;
	}

}
