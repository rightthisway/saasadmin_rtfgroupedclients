/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.inventory.common.dto;

import java.io.File;
import java.util.List;

import com.rtf.inventory.product.dvo.ProductItemMasterDVO;
import com.rtf.inventory.variants.dvo.ProductInventoryDVO;
import com.rtf.inventory.variants.dvo.ProductItemVariantNameDVO;
import com.rtf.saas.arch.RtfSaasBaseDTO;

/**
 * The Class InventoryUploadDTO.
 */
public class InventoryUploadDTO extends RtfSaasBaseDTO {

	/** The uploaded file. */
	private File uploadedFile;

	/** The ProductItemMasterDVO. */
	private List<ProductItemMasterDVO> pmasterdvolst;

	/** The ProductItemVariantNameDVO list. */
	private List<ProductItemVariantNameDVO> variantdvolst;

	/** The ProductItemVariantNameDVO list. */
	private List<ProductItemVariantNameDVO> optvaluedvolst;

	/** The ProductInventoryDVO list. */
	private List<ProductInventoryDVO> invetorylst;

	/** The temp integervals. */
	private Integer tempIntegervals;

	/** The temp double vals. */
	private Integer tempDoubleVals;

	/** The mandatory string. */
	private String mandatoryString;

	/** The mandatory boolean. */
	private String mandatoryBoolean;

	/** The cau. */
	private String cau;

	/**
	 * Gets the ProductItemMasterDVO.
	 *
	 * @return the pmasterdvolst
	 */
	public List<ProductItemMasterDVO> getPmasterdvolst() {
		return pmasterdvolst;
	}

	/**
	 * Sets the ProductItemMasterDVO.
	 *
	 * @param pmasterdvolst the new pmasterdvolst
	 */
	public void setPmasterdvolst(List<ProductItemMasterDVO> pmasterdvolst) {
		this.pmasterdvolst = pmasterdvolst;
	}

	/**
	 * Gets the ProductItemVariantNameDVO list.
	 *
	 * @return the variantdvolst
	 */
	public List<ProductItemVariantNameDVO> getVariantdvolst() {
		return variantdvolst;
	}

	/**
	 * Sets the ProductItemVariantNameDVO list.
	 *
	 * @param variantdvolst the new variantdvolst
	 */
	public void setVariantdvolst(List<ProductItemVariantNameDVO> variantdvolst) {
		this.variantdvolst = variantdvolst;
	}

	/**
	 * Gets the ProductInventoryDVO list.
	 *
	 * @return the invetorylst
	 */
	public List<ProductInventoryDVO> getInvetorylst() {
		return invetorylst;
	}

	/**
	 * Sets the ProductInventoryDVO list.
	 *
	 * @param invetorylst the new invetorylst
	 */
	public void setInvetorylst(List<ProductInventoryDVO> invetorylst) {
		this.invetorylst = invetorylst;
	}

	/**
	 * Genearate string of all properties.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "InventoryUploadDTO [pmasterdvolst=" + pmasterdvolst + ", variantdvolst=" + variantdvolst
				+ ", invetorylst=" + invetorylst + "]";
	}

	/**
	 * Gets the uploaded file.
	 *
	 * @return the uploaded file
	 */
	public File getUploadedFile() {
		return uploadedFile;
	}

	/**
	 * Sets the uploaded file.
	 *
	 * @param uploadedFile the new uploaded file
	 */
	public void setUploadedFile(File uploadedFile) {
		this.uploadedFile = uploadedFile;
	}

	/**
	 * Gets the temp integervals.
	 *
	 * @return the temp integervals
	 */
	public Integer getTempIntegervals() {
		return tempIntegervals;
	}

	/**
	 * Sets the temp integervals.
	 *
	 * @param tempIntegervals the new temp integervals
	 */
	public void setTempIntegervals(Integer tempIntegervals) {
		this.tempIntegervals = tempIntegervals;
	}

	/**
	 * Gets the temp double vals.
	 *
	 * @return the temp double vals
	 */
	public Integer getTempDoubleVals() {
		return tempDoubleVals;
	}

	/**
	 * Sets the temp double vals.
	 *
	 * @param tempDoubleVals the new temp double vals
	 */
	public void setTempDoubleVals(Integer tempDoubleVals) {
		this.tempDoubleVals = tempDoubleVals;
	}

	/**
	 * Gets the mandatory string.
	 *
	 * @return the mandatory string
	 */
	public String getMandatoryString() {
		return mandatoryString;
	}

	/**
	 * Sets the mandatory string.
	 *
	 * @param mandatoryString the new mandatory string
	 */
	public void setMandatoryString(String mandatoryString) {
		this.mandatoryString = mandatoryString;
	}

	/**
	 * Gets the cau.
	 *
	 * @return the cau
	 */
	public String getCau() {
		return cau;
	}

	/**
	 * Sets the cau.
	 *
	 * @param cau the new cau
	 */
	public void setCau(String cau) {
		this.cau = cau;
	}

	/**
	 * Gets the mandatory boolean.
	 *
	 * @return the mandatory boolean
	 */
	public String getMandatoryBoolean() {
		return mandatoryBoolean;
	}

	/**
	 * Sets the mandatory boolean.
	 *
	 * @param mandatoryBoolean the new mandatory boolean
	 */
	public void setMandatoryBoolean(String mandatoryBoolean) {
		this.mandatoryBoolean = mandatoryBoolean;
	}

	/**
	 * Gets the ProductItemVariantNameDVO list.
	 *
	 * @return the optvaluedvolst
	 */
	public List<ProductItemVariantNameDVO> getOptvaluedvolst() {
		return optvaluedvolst;
	}

	/**
	 * Sets the ProductItemVariantNameDVO list.
	 *
	 * @param optvaluedvolst the new optvaluedvolst
	 */
	public void setOptvaluedvolst(List<ProductItemVariantNameDVO> optvaluedvolst) {
		this.optvaluedvolst = optvaluedvolst;
	}

}
