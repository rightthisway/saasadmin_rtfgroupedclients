/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.inventory.common.dto;

/**
 * The Class FileUploadGenericDTO.
 */
public class FileUploadGenericDTO {

	/** The sts. */
	private Integer sts;

	/** The msg. */
	private String msg;

	/** The aws final url. */
	private String awsfinalUrl;

	/**
	 * Gets the sts.
	 *
	 * @return the sts
	 */
	public Integer getSts() {
		return sts;
	}

	/**
	 * Sets the sts.
	 *
	 * @param sts the new sts
	 */
	public void setSts(Integer sts) {
		this.sts = sts;
	}

	/**
	 * Gets the msg.
	 *
	 * @return the msg
	 */
	public String getMsg() {
		return msg;
	}

	/**
	 * Sets the msg.
	 *
	 * @param msg the new msg
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}

	/**
	 * Gets the aws final url.
	 *
	 * @return the awsfinal url
	 */
	public String getAwsfinalUrl() {
		return awsfinalUrl;
	}

	/**
	 * Sets the awsfinal url.
	 *
	 * @param awsfinalUrl the new awsfinal url
	 */
	public void setAwsfinalUrl(String awsfinalUrl) {
		this.awsfinalUrl = awsfinalUrl;
	}

}
