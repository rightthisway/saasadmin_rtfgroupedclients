/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.inventory.product.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.rtf.common.util.GridSortingUtil;
import com.rtf.inventory.product.dvo.ProductItemMasterDVO;
import com.rtf.saas.util.GenUtil;

/**
 * The Class ProductItemMasterDAO.
 */
public class ProductItemMasterDAO {

	/**
	 * Creates the new product item.
	 *
	 * @param dvo  the dvo
	 * @param conn the conn
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer createPrductItem(ProductItemMasterDVO dvo, Connection conn) throws Exception {

		PreparedStatement ps = null;
		StringBuffer sql = new StringBuffer();
		Integer pkId = null;
		sql.append("insert into  rtf_seller_product_items_mstr")
		.append(" ( clintid ,slrpitms_id ,sler_id,pbrand ,pname ,pdesc , plong_desc, " )
		.append(" psel_min_prc , preg_min_prc,pimg,pstatus,")		
		.append(" lupd_date,lupd_by,")
		.append("price_type, pprc_dtl)  ")
		.append(" values(?, ?, ?, ?, ?, ?, ?," )		
		.append("   ? , ? , ? , ? , " )		
		.append("getdate(), ?, ")
		.append("?,? ) ");
		try {
			
			ps = conn.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, dvo.getClintId());
			ps.setString(2, dvo.getSlrpitmsId());
			ps.setInt(3, dvo.getSlerId());
			ps.setString(4, dvo.getPbrand());
			ps.setString(5, dvo.getPname());
			ps.setString(6, dvo.getPdesc());
			ps.setString(7, dvo.getPlongDesc());

			ps.setDouble(8, dvo.getPselMinPrc());
			ps.setDouble(9, dvo.getPregMinPrc());
			ps.setString(10, dvo.getPimg());
			//ps.setString(11, dvo.getPsku());
			ps.setString(11, dvo.getPstatus());
		
			ps.setString(12, dvo.getLupdBy());
			ps.setString(13, dvo.getPriceType());
			ps.setString(14, dvo.getProdPriceDtl());

			int affectedRows = ps.executeUpdate();
			if (affectedRows == 0) {
				throw new SQLException("Creating Product Item Master record failed for Client Id " + dvo.getClintId());
			}
			ResultSet generatedKeys;
			try {
				generatedKeys = ps.getGeneratedKeys();
				if (generatedKeys.next()) {
					pkId = generatedKeys.getInt(1);

				}
				generatedKeys.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			
			System.out.println(" Generated KEY FOR PROD ITEM MASTER IS " + pkId);
			
			return pkId;
		} catch (SQLException se) {
			se.printStackTrace();
			throw se;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			try {
				ps.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}
	
	
	
	
	//-----------------
	
		/**
		 * Fetch all client product items.
		 *
		 * @param rdvo the rdvo
		 * @param conn the conn
		 * @return the list
		 * @throws Exception the exception
		 */
		public static List<ProductItemMasterDVO> fetchAllClientProductItems(ProductItemMasterDVO rdvo, Connection conn,String filter)
				throws Exception {
			PreparedStatement ps = null;
			ResultSet rs = null;
			StringBuffer sql = new StringBuffer();
			sql.append(" select itmsid, clintid ,slrpitms_id ,sler_id ")
			.append(" ,pbrand ,pname ,pdesc ,plong_desc ,psel_min_prc ,preg_min_prc, pprc_dtl ")
			.append(" ,pimg ,pstatus ,lupd_date  ,lupd_by")
			.append(" ,price_type from ")
			.append(" rtf_seller_product_items_mstr where  clintid = ? and pstatus=? and sler_id=?");
			
				String sortingSql = GridSortingUtil.getProductSortingQuery(filter);
					if(sortingSql!=null && !sortingSql.trim().isEmpty()){
						sql.append(sortingSql);
					}else{
						sql.append(" ORDER BY slrpitms_id ");
		}
			
			List<ProductItemMasterDVO> dvolist = new ArrayList<ProductItemMasterDVO>();
			try {
				ps = conn.prepareStatement(sql.toString());
				ps.setString(1, rdvo.getClintId());
				ps.setString(2, rdvo.getPstatus());
				ps.setInt(3, rdvo.getSlerId());
				rs = ps.executeQuery();
				while (rs.next()) {
					Date date = null;
					ProductItemMasterDVO dvo = new ProductItemMasterDVO();
					dvo.setItemsId(rs.getInt("itmsid"));
					dvo.setClintId(rs.getString("clintid"));
					dvo.setSlrpitmsId(rs.getString("slrpitms_id"));
					dvo.setSlerId(rs.getInt("sler_id"));
					dvo.setPbrand(rs.getString("pbrand"));
					dvo.setPname(rs.getString("pname"));
					dvo.setPdesc(rs.getString("pdesc"));
					dvo.setPlongDesc(rs.getString("plong_desc"));
					dvo.setPselMinPrc(rs.getDouble("psel_min_prc"));
					dvo.setPregMinPrc(rs.getDouble("preg_min_prc"));
					dvo.setPimg(rs.getString("pimg"));				
					dvo.setPstatus(rs.getString("pstatus"));
					dvo.setPriceType(rs.getString("price_type"));			
					date = rs.getTimestamp("lupd_date");
					dvo.setLupdDate(date != null ? date.getTime() : null);
					dvo.setLupdBy(rs.getString("lupd_by"));				
					dvo.setProdPriceDtl(rs.getString("pprc_dtl"));				
					dvolist.add(dvo);
				}
				return dvolist;
			} catch (SQLException se) {
				se.printStackTrace();
				throw se;
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			} finally {
				try {
					rs.close();
					ps.close();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
	}
	
	//-----------------
	
	
	
	//---------------------------
	
		/**
		 * Fetch all client product items by client seller ID.
		 *
		 * @param rdvo the rdvo
		 * @param conn the conn
		 * @return the list
		 * @throws Exception the exception
		 */
		public static List<ProductItemMasterDVO> fetchAllClientProductItemsByClientSeller(ProductItemMasterDVO rdvo,
				Connection conn,String filter) throws Exception {
			PreparedStatement ps = null;
			ResultSet rs = null;
			StringBuffer sql = new StringBuffer();
			sql.append("select itmsid, clintid ,slrpitms_id ,sler_id ")
			.append("      ,pbrand ,pname ,pdesc ,plong_desc ,psel_min_prc ,preg_min_prc, pprc_dtl ")
			.append("      ,pimg  ,pstatus ,lupd_date  ,lupd_by")
			.append( "       ,price_type from ")
			.append("      rtf_seller_product_items_mstr where  clintid = ? and pstatus=? and sler_id=? ");
			List<ProductItemMasterDVO> dvolist = new ArrayList<ProductItemMasterDVO>();
			try {
			
			String sortingSql = GridSortingUtil.getProductSortingQuery(filter);
						if(sortingSql!=null && !sortingSql.trim().isEmpty()){
							sql.append(sortingSql);
			}
			
			
				ps = conn.prepareStatement(sql.toString());
				ps.setString(1, rdvo.getClintId());
				ps.setString(2, rdvo.getPstatus());
				ps.setInt(3, rdvo.getSlerId());
				rs = ps.executeQuery();
				while (rs.next()) {
					Date date = null;
					ProductItemMasterDVO dvo = new ProductItemMasterDVO();
					dvo.setItemsId(rs.getInt("itmsid"));
					dvo.setClintId(rs.getString("clintid"));
					dvo.setSlrpitmsId(rs.getString("slrpitms_id"));
					dvo.setSlerId(rs.getInt("sler_id"));
					dvo.setPbrand(rs.getString("pbrand"));
					dvo.setPname(rs.getString("pname"));
					dvo.setPdesc(rs.getString("pdesc"));
					dvo.setPriceType(rs.getString("price_type"));
					dvo.setPlongDesc(rs.getString("plong_desc"));
					dvo.setPselMinPrc(rs.getDouble("psel_min_prc"));
					dvo.setPregMinPrc(rs.getDouble("preg_min_prc"));
					dvo.setPimg(rs.getString("pimg"));
					//dvo.setPsku(rs.getString("psku"));
					dvo.setPstatus(rs.getString("pstatus"));
					//date = rs.getTimestamp("pexp_date");
					//dvo.setPexpDate(date != null ? date.getTime() : null);
					date = rs.getTimestamp("lupd_date");
					dvo.setLupdDate(date != null ? date.getTime() : null);
					dvo.setLupdBy(rs.getString("lupd_by"));
					//dvo.setIsVariant(rs.getBoolean("is_variant"));
					//dvo.setMaxCustQty(rs.getInt("max_cust_qty"));
					dvo.setProdPriceDtl(rs.getString("pprc_dtl"));
					//date = rs.getTimestamp("disp_start_date");
					//dvo.setDispStartdate(date != null ? date.getTime() : null);
					dvolist.add(dvo);
				}
				return dvolist;
			} catch (SQLException se) {
				se.printStackTrace();
				throw se;
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			} finally {
				try {
					rs.close();
					ps.close();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
	}
	
	
	//----------------------------
	
	
	
	
		/**
		 * Fetch product item for By Product ID.
		 *
		 * @param rdvo the rdvo
		 * @param conn the conn
		 * @return the product item master DVO
		 * @throws Exception the exception
		 */
		public static ProductItemMasterDVO fetchProductItemForPK(ProductItemMasterDVO rdvo, Connection conn)
				throws Exception {
			PreparedStatement ps = null;
			ResultSet rs = null;
			ProductItemMasterDVO dvo = null;
			StringBuffer sql = new StringBuffer();
			sql.append("select itmsid, clintid ,slrpitms_id ,sler_id ")
			.append("      ,pbrand ,pname ,pdesc ,plong_desc ,psel_min_prc ,preg_min_prc, pprc_dtl ")
			.append("      ,pimg ,pstatus  ,lupd_date  ,lupd_by")
			.append("      ,price_type from ")
			.append("      rtf_seller_product_items_mstr where   clintid = ? and itmsid=? ");
			try {
				ps = conn.prepareStatement(sql.toString());
				ps.setString(1, rdvo.getClintId());		
				ps.setInt(2, rdvo.getItemsId());
				rs = ps.executeQuery();
				dvo = new ProductItemMasterDVO();
				while (rs.next()) {
					Date date = null;
					dvo.setItemsId(rs.getInt("itmsid"));
					dvo.setClintId(rs.getString("clintid"));
					dvo.setSlrpitmsId(rs.getString("slrpitms_id"));
					dvo.setSlerId(rs.getInt("sler_id"));
					dvo.setPbrand(rs.getString("pbrand"));
					dvo.setPname(rs.getString("pname"));
					dvo.setPdesc(rs.getString("pdesc"));
					dvo.setPlongDesc(rs.getString("plong_desc"));
					dvo.setPselMinPrc(rs.getDouble("psel_min_prc"));
					dvo.setPregMinPrc(rs.getDouble("preg_min_prc"));
					dvo.setPimg(rs.getString("pimg"));
					//dvo.setPsku(rs.getString("psku"));
					dvo.setPriceType(rs.getString("price_type"));
					dvo.setPstatus(rs.getString("pstatus"));
					//date = rs.getTimestamp("pexp_date");
					//dvo.setPexpDate(date != null ? date.getTime() : null);
					date = rs.getTimestamp("lupd_date");
					dvo.setLupdDate(date != null ? date.getTime() : null);
					dvo.setLupdBy(rs.getString("lupd_by"));
					//dvo.setIsVariant(rs.getBoolean("is_variant"));
					//dvo.setMaxCustQty(rs.getInt("max_cust_qty"));
					dvo.setProdPriceDtl(rs.getString("pprc_dtl"));
					//date = rs.getTimestamp("disp_start_date");
					//dvo.setDispStartdate(date != null ? date.getTime() : null);
				}
				return dvo;
			} catch (SQLException se) {
				se.printStackTrace();
				throw se;
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			} finally {
				try {
					rs.close();
					ps.close();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
	}
	
	
	
	/**
		 * Update product item master.
		 *
		 * @param rdvo the rdvo
		 * @param conn the DB connection
		 * @return the affected db row count integer
		 * @throws Exception the exception
		 */
		public static Integer updateProductItemMaster(ProductItemMasterDVO rdvo, Connection conn) throws Exception {
			StringBuffer sql = new StringBuffer();
			sql.append(" update rtf_seller_product_items_mstr set ")
			.append("  pbrand=? ,pname=? ,pdesc=? ,plong_desc=? ,pstatus=?,")
			.append(" pprc_dtl=? ,psel_min_prc=? ,preg_min_prc=?  , price_type=?, ")
			.append(" lupd_by = ?, lupd_date = getDate() , slrpitms_id = ?  ");
			if (!GenUtil.isEmptyString(rdvo.getPimg())) {
				sql.append(" , pimg=? where itmsid=?  ");
			} else {
				sql.append(" where itmsid=? ");
			}
	
			Integer updCnt = 0;
			PreparedStatement ps = null;
			try {
				ps = conn.prepareStatement(sql.toString());
				
				ps.setString(1, rdvo.getPbrand());
				ps.setString(2, rdvo.getPname());
				ps.setString(3, rdvo.getPdesc());
				ps.setString(4, rdvo.getPlongDesc());
				ps.setString(5, rdvo.getPstatus());
				
				ps.setString(6, rdvo.getProdPriceDtl());
				ps.setDouble(7, rdvo.getPselMinPrc());
				ps.setDouble(8, rdvo.getPregMinPrc());
				ps.setString(9, rdvo.getPriceType());
				//ps.setInt(10, rdvo.getMaxCustQty());
				ps.setString(10, rdvo.getLupdBy());	
				ps.setString(11, rdvo.getSlrpitmsId());		
				if(!GenUtil.isEmptyString(rdvo.getPimg())) {
					ps.setString(12, rdvo.getPimg());
					ps.setInt(13, rdvo.getItemsId());				
				} else {				
					ps.setInt(12, rdvo.getItemsId());
				}
	
				updCnt = ps.executeUpdate();
				return updCnt;
	
			} catch (SQLException se) {
				se.printStackTrace();
				throw se;
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			} finally {
				try {
					ps.close();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
	
	}
	
	
	
	
	
		/**
		 * Update product item master status.
		 *
		 * @param rdvo the rdvo
		 * @param conn the conn
		 * @return the integer
		 * @throws Exception the exception
		 */
		public static Integer updateProductItemMasterStatus(ProductItemMasterDVO rdvo, Connection conn) throws Exception {
			String sql = " update rtf_seller_product_items_mstr set " + " pstatus=?, lupd_by = ?,lupd_date = getDate() where itmsid=?  ";
			Integer updCnt = 0;
			PreparedStatement ps = null;
			try {
				ps = conn.prepareStatement(sql);
				ps.setString(1, rdvo.getPstatus());
				ps.setString(2, rdvo.getLupdBy());
				ps.setInt(3, rdvo.getItemsId());
				updCnt = ps.executeUpdate();
				return updCnt;
			} catch (SQLException se) {
				se.printStackTrace();
				throw se;
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			} finally {
				try {
					ps.close();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
	
	}
	

	public static Boolean checkIfItemIdExistsForSellerProdId(String clientIdStr, Integer sellerId, String sellerprodid,
			Connection conn) throws Exception {		
		PreparedStatement ps = null;
		ResultSet rs = null;
		ProductItemMasterDVO dvo = null;
		Boolean isExists = false;
		StringBuffer sql = new StringBuffer();
		sql.append(" select slrpitms_id from ")		
		.append(" rtf_seller_product_items_mstr with (nolock) where  clintid = ? and slrpitms_id=? and sler_id=? ");
		try {
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientIdStr);
			ps.setString(2, sellerprodid);
			ps.setInt(3, sellerId);
			rs = ps.executeQuery();
			dvo = new ProductItemMasterDVO();
			while (rs.next()) {
				isExists = true;
			}
			}catch(Exception ex) {
				ex.printStackTrace();
				throw ex;
			}
		return isExists;
	}
	
	public static Boolean checkIfItemIdExistsForOtherClientSellerProdId(String clientIdStr, Integer sellerId, String sellerprodid,Integer itemId,
			Connection conn) throws Exception {		
		PreparedStatement ps = null;
		ResultSet rs = null;
		ProductItemMasterDVO dvo = null;
		Boolean isExists = false;
		StringBuffer sql = new StringBuffer();
		sql.append(" select slrpitms_id from ")		
		.append(" rtf_seller_product_items_mstr with (nolock) where  clintid = ? and slrpitms_id=? and sler_id=? and itmsid !=?");
		try {
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientIdStr);
			ps.setString(2, sellerprodid);
			ps.setInt(3, sellerId);
			ps.setInt(4, itemId);
			rs = ps.executeQuery();
			dvo = new ProductItemMasterDVO();
			while (rs.next()) {
				isExists = true;
			}
			}catch(Exception ex) {
				ex.printStackTrace();
				throw ex;
			}
		return isExists;
	}
}
