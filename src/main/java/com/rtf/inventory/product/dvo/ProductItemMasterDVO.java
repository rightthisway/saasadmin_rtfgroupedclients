/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.inventory.product.dvo;

/**
 * The Class ProductItemMasterDVO.
 */
public class ProductItemMasterDVO {
	//private Integer prodId;
	private Integer itemsId;

	/** The client id. */
	private String clintId;

	/** The slrpitms id. */
	private String slrpitmsId;

	/** The sler id. */
	private Integer slerId;

	/** The pbrand. */
	private String pbrand;

	/** The pname. */
	private String pname;

	/** The pdesc. */
	private String pdesc;

	/** The plong desc. */
	private String plongDesc;

	/** The price type. */
	private String priceType;

	/** The psel min prc. */
	private Double pselMinPrc;

	/** The preg min prc. */
	private Double pregMinPrc;

	/** The pimg. */
	private String pimg;

	/** The psku. */
	private String psku;

	/** The pstatus. */
	private String pstatus;

	/** The pexp date. */
	private Long pexpDate;

	/** The lupd date. */
	private Long lupdDate;

	/** The lupd by. */
	private String lupdBy;

	/** The is variant. */
	private Boolean isVariant;

	/** The max cust qty. */
	private Integer maxCustQty;

	/** The disp startdate. */
	private Long dispStartdate;
	
	/** The product price details. */
	private String prodPriceDtl;

	/**
	 * Gets the clint id.
	 *
	 * @return the clint id
	 */
	public String getClintId() {
		return clintId;
	}

	/**
	 * Sets the clint id.
	 *
	 * @param clintId the new clint id
	 */
	public void setClintId(String clintId) {
		this.clintId = clintId;
	}

	/**
	 * Gets the slrpitms id.
	 *
	 * @return the slrpitms id
	 */
	public String getSlrpitmsId() {
		return slrpitmsId;
	}

	/**
	 * Sets the slrpitms id.
	 *
	 * @param slrpitmsId the new slrpitms id
	 */
	public void setSlrpitmsId(String slrpitmsId) {
		this.slrpitmsId = slrpitmsId;
	}

	/**
	 * Gets the sler id.
	 *
	 * @return the sler id
	 */
	public Integer getSlerId() {
		return slerId;
	}

	/**
	 * Sets the sler id.
	 *
	 * @param slerId the new sler id
	 */
	public void setSlerId(Integer slerId) {
		this.slerId = slerId;
	}

	/**
	 * Gets the pbrand.
	 *
	 * @return the pbrand
	 */
	public String getPbrand() {
		return pbrand;
	}

	/**
	 * Sets the pbrand.
	 *
	 * @param pbrand the new pbrand
	 */
	public void setPbrand(String pbrand) {
		this.pbrand = pbrand;
	}

	/**
	 * Gets the pname.
	 *
	 * @return the pname
	 */
	public String getPname() {
		return pname;
	}

	/**
	 * Sets the pname.
	 *
	 * @param pname the new pname
	 */
	public void setPname(String pname) {
		this.pname = pname;
	}

	/**
	 * Gets the pdesc.
	 *
	 * @return the pdesc
	 */
	public String getPdesc() {
		return pdesc;
	}

	/**
	 * Sets the pdesc.
	 *
	 * @param pdesc the new pdesc
	 */
	public void setPdesc(String pdesc) {
		this.pdesc = pdesc;
	}

	/**
	 * Gets the plong desc.
	 *
	 * @return the plong desc
	 */
	public String getPlongDesc() {
		return plongDesc;
	}

	/**
	 * Sets the plong desc.
	 *
	 * @param plongDesc the new plong desc
	 */
	public void setPlongDesc(String plongDesc) {
		this.plongDesc = plongDesc;
	}

	/**
	 * Gets the psel min prc.
	 *
	 * @return the psel min prc
	 */
	public Double getPselMinPrc() {
		return pselMinPrc;
	}

	/**
	 * Sets the psel min prc.
	 *
	 * @param pselMinPrc the new psel min prc
	 */
	public void setPselMinPrc(Double pselMinPrc) {
		this.pselMinPrc = pselMinPrc;
	}

	/**
	 * Gets the preg min prc.
	 *
	 * @return the preg min prc
	 */
	public Double getPregMinPrc() {
		return pregMinPrc;
	}

	/**
	 * Sets the preg min prc.
	 *
	 * @param pregMinPrc the new preg min prc
	 */
	public void setPregMinPrc(Double pregMinPrc) {
		this.pregMinPrc = pregMinPrc;
	}

	/**
	 * Gets the pimg.
	 *
	 * @return the pimg
	 */
	public String getPimg() {
		return pimg;
	}

	/**
	 * Sets the pimg.
	 *
	 * @param pimg the new pimg
	 */
	public void setPimg(String pimg) {
		this.pimg = pimg;
	}

	/**
	 * Gets the psku.
	 *
	 * @return the psku
	 */
	public String getPsku() {
		return psku;
	}

	/**
	 * Sets the psku.
	 *
	 * @param psku the new psku
	 */
	public void setPsku(String psku) {
		this.psku = psku;
	}

	/**
	 * Gets the pstatus.
	 *
	 * @return the pstatus
	 */
	public String getPstatus() {
		return pstatus;
	}

	/**
	 * Sets the pstatus.
	 *
	 * @param pstatus the new pstatus
	 */
	public void setPstatus(String pstatus) {
		this.pstatus = pstatus;
	}

	/**
	 * Gets the pexp date.
	 *
	 * @return the pexp date
	 */
	public Long getPexpDate() {
		return pexpDate;
	}

	/**
	 * Sets the pexp date.
	 *
	 * @param pexpDate the new pexp date
	 */
	public void setPexpDate(Long pexpDate) {
		this.pexpDate = pexpDate;
	}

	/**
	 * Gets the lupd date.
	 *
	 * @return the lupd date
	 */
	public Long getLupdDate() {
		return lupdDate;
	}

	/**
	 * Sets the lupd date.
	 *
	 * @param lupdDate the new lupd date
	 */
	public void setLupdDate(Long lupdDate) {
		this.lupdDate = lupdDate;
	}

	/**
	 * Gets the lupd by.
	 *
	 * @return the lupd by
	 */
	public String getLupdBy() {
		return lupdBy;
	}

	/**
	 * Sets the lupd by.
	 *
	 * @param lupdBy the new lupd by
	 */
	public void setLupdBy(String lupdBy) {
		this.lupdBy = lupdBy;
	}

	/**
	 * Gets the checks if is variant.
	 *
	 * @return the checks if is variant
	 */
	public Boolean getIsVariant() {
		return isVariant;
	}

	/**
	 * Sets the checks if is variant.
	 *
	 * @param isVariant the new checks if is variant
	 */
	public void setIsVariant(Boolean isVariant) {
		this.isVariant = isVariant;
	}

	/**
	 * Gets the max cust qty.
	 *
	 * @return the max cust qty
	 */
	public Integer getMaxCustQty() {
		return maxCustQty;
	}

	/**
	 * Sets the max cust qty.
	 *
	 * @param maxCustQty the new max cust qty
	 */
	public void setMaxCustQty(Integer maxCustQty) {
		this.maxCustQty = maxCustQty;
	}

	/**
	 * Gets the price type.
	 *
	 * @return the price type
	 */
	public String getPriceType() {
		return priceType;
	}

	/**
	 * Sets the price type.
	 *
	 * @param priceType the new price type
	 */
	public void setPriceType(String priceType) {
		this.priceType = priceType;
	}

	/**
	 * Gets the disp startdate.
	 *
	 * @return the disp startdate
	 */
	public Long getDispStartdate() {
		return dispStartdate;
	}

	/**
	 * Sets the disp startdate.
	 *
	 * @param dispStartdate the new disp startdate
	 */
	public void setDispStartdate(Long dispStartdate) {
		this.dispStartdate = dispStartdate;
	}
	
	
	/**
	 * Gets product price details.
	 *
	 * @return prodPriceDtl
	 */
	public String getProdPriceDtl() {
		return prodPriceDtl;
	}

	/**
	 * Sets product price details.
	 *
	 * @param prodPriceDtl
	 */
	public void setProdPriceDtl(String prodPriceDtl) {
		this.prodPriceDtl = prodPriceDtl;
	}	

	public Integer getItemsId() {
		return itemsId;
	}

	/**
	 * Sets the items id.
	 *
	 * @param itemsId the new items id
	 */
	public void setItemsId(Integer itemsId) {
		this.itemsId = itemsId;
	}

	@Override
	public String toString() {
		return "ProductItemMasterDVO [itemsId=" + itemsId + ", clintId=" + clintId + ", slrpitmsId=" + slrpitmsId
				+ ", slerId=" + slerId + ", pbrand=" + pbrand + ", pname=" + pname + ", pdesc=" + pdesc + ", plongDesc="
				+ plongDesc + ", priceType=" + priceType + ", pselMinPrc=" + pselMinPrc + ", pregMinPrc=" + pregMinPrc
				+ ", pimg=" + pimg + ", psku=" + psku + ", pstatus=" + pstatus + ", pexpDate=" + pexpDate
				+ ", lupdDate=" + lupdDate + ", lupdBy=" + lupdBy + ", isVariant=" + isVariant + ", maxCustQty="
				+ maxCustQty + ", dispStartdate=" + dispStartdate + ", prodPriceDtl=" + prodPriceDtl + "]";
	}

}
