/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.inventory.product.service;

import java.sql.Connection;
import java.util.List;

import com.rtf.inventory.product.dao.ProductItemMasterCassDAO;
import com.rtf.inventory.product.dao.ProductItemMasterDAO;
import com.rtf.inventory.product.dto.ProductItemMasterDTO;
import com.rtf.inventory.product.dvo.ProductItemMasterDVO;
import com.rtf.saas.sql.db.DatabaseConnections;

/**
 * The Class ProductItemMasterService.
 */
public class ProductItemMasterService {

	/**
	 * Creates the new product item.
	 *
	 * @param dto the dto
	 * @return the product item master DTO
	 * @throws Exception the exception
	 */
	public static ProductItemMasterDTO createProductItem(ProductItemMasterDTO dto) throws Exception {
		Connection conn = null;
		ProductItemMasterDVO dvo = dto.getDvo();
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			Integer pkId = ProductItemMasterDAO.createPrductItem(dvo, conn);
			
			System.out.println("Product Cerated with ID " + pkId);
			if (pkId == null) {
				throw new Exception(" ERROR Creating Product Master Record");
			}
			dvo.setItemsId(pkId);
			Integer sts = ProductItemMasterCassDAO.createPrductItem(dvo);
			dto.setSts(sts);
		} catch (Exception ex) {
			dto.setSts(0);
			ex.printStackTrace();
			throw ex;
		} finally {
			DatabaseConnections.closeConnection(conn);
		}
		return dto;
	}

	/**
	 * Fetch all product items for client.
	 *
	 * @param dto the dto
	 * @return the product item master DTO
	 * @throws Exception the exception
	 */
	public static ProductItemMasterDTO fetchAllProductItemsForClient(ProductItemMasterDTO dto,String filter) throws Exception {
		Connection conn = null;
		ProductItemMasterDVO dvo = dto.getDvo();
		List<ProductItemMasterDVO> lst = null;
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			lst = ProductItemMasterDAO.fetchAllClientProductItems(dvo, conn,filter);
			dto.setLst(lst);
			dto.setSts(1);
		} catch (Exception ex) {
			dto.setSts(0);
			ex.printStackTrace();
			throw ex;
		} finally {
			DatabaseConnections.closeConnection(conn);
		}
		return dto;
	}

	/**
	 * Fetch all client product items by client and seller.
	 *
	 * @param dto the dto
	 * @return the product item master DTO
	 * @throws Exception the exception
	 */
	public static ProductItemMasterDTO fetchAllClientProductItemsByClientSeller(ProductItemMasterDTO dto,String filter)
			throws Exception {
		Connection conn = null;
		ProductItemMasterDVO dvo = dto.getDvo();
		List<ProductItemMasterDVO> lst = null;
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			lst = ProductItemMasterDAO.fetchAllClientProductItemsByClientSeller(dvo, conn,filter);
			dto.setLst(lst);
			dto.setSts(1);
		} catch (Exception ex) {
			dto.setSts(0);
			ex.printStackTrace();
			throw ex;
		} finally {
			DatabaseConnections.closeConnection(conn);
		}
		return dto;
	}

	/**
	 * Update product item master.
	 *
	 * @param dto the dto
	 * @return the product item master DTO
	 * @throws Exception the exception
	 */
	public static ProductItemMasterDTO updateProductItemMasterForPK(ProductItemMasterDTO dto) throws Exception {
		Connection conn = null;
		ProductItemMasterDVO dvo = dto.getDvo();
		dto.setSts(0);
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			Integer updCnt = ProductItemMasterDAO.updateProductItemMaster(dvo, conn);
			System.out.println(" UPDATED SQL  FOR PRODUCT WITH ID " + dvo.getItemsId());
			if(updCnt > 0 ) {
				Integer sts = ProductItemMasterCassDAO.updatePrductItem(dvo);
				System.out.println("  UPDATED CASS  FOR PRODUCT WITH ID  " + dvo.getItemsId() + " [ UPD STS ] " + sts );
				dto.setSts(sts);
				dto.setDvo(dvo);
			}
		} catch (Exception ex) {
			dto.setSts(0);
			ex.printStackTrace();
			throw ex;
		} finally {
			DatabaseConnections.closeConnection(conn);
		}
		return dto;
	}

	/**
	 * Update product item master status.
	 *
	 * @param dto the dto
	 * @return the product item master DTO
	 * @throws Exception the exception
	 */
	public static ProductItemMasterDTO updateProductItemMasterStatusForPK(ProductItemMasterDTO dto) throws Exception {
		Connection conn = null;
		ProductItemMasterDVO dvo = dto.getDvo();
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			Integer updCnt = ProductItemMasterDAO.updateProductItemMasterStatus(dvo, conn);
			dto.setDvo(dvo);
			dto.setSts(updCnt);
		} catch (Exception ex) {
			dto.setSts(0);
			ex.printStackTrace();
			throw ex;
		} finally {
			DatabaseConnections.closeConnection(conn);
		}
		return dto;
	}

	public static Boolean checkIfProdItemIdExistsForSellerProdId(String clientIdStr, String sellerprodid,
			Integer sellerId) throws Exception {
		Connection conn = null;
		Boolean itemExists = false;
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			itemExists = ProductItemMasterDAO.checkIfItemIdExistsForSellerProdId(clientIdStr, sellerId, sellerprodid,
					conn);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		} finally {
			DatabaseConnections.closeConnection(conn);
		}
		return itemExists;
	}
	
	
	public static Boolean checkIfItemIdExistsForOtherClientSellerProdId(String clientIdStr, String sellerprodid,
			Integer sellerId,Integer itemId) throws Exception {
		Connection conn = null;
		Boolean itemExists = false;
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			itemExists = ProductItemMasterDAO.checkIfItemIdExistsForOtherClientSellerProdId(clientIdStr, sellerId, sellerprodid,itemId,
					conn);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		} finally {
			DatabaseConnections.closeConnection(conn);
		}
		return itemExists;
	}	

	private static ProductItemMasterDVO getDVO() throws Exception {
		ProductItemMasterDVO dvo = new ProductItemMasterDVO();
		dvo.setClintId("AMIT202002CID");
		dvo.setSlrpitmsId("99993");
		dvo.setSlerId(10007);
		dvo.setPbrand("LLOYDS");
		dvo.setPname("LLOYDSTEST-PROD-03");
		dvo.setPdesc("LLOYDSTEST-PROD- DESCRIPTION GOES HERE");
		dvo.setPriceType(null);
		dvo.setPlongDesc("LLOYDSTEST-PROD- LONG DESCRIPTION GOES HERE");
		dvo.setPimg(
				"http://d9kn0fqut3lki.cloudfront.net/sa/${s3.inventoryproducts.folder}/RC200601HVYGJE115132/RC200601HVYGJE115132_1615227710904.jpeg");
		dvo.setPstatus("ACTIVE");
		dvo.setLupdBy("LLOYDTEST");
		dvo.setIsVariant(false);
		dvo.setPselMinPrc(10.09);
		dvo.setPregMinPrc(11.99);
		dvo.setProdPriceDtl("Get 10% off on 3 days 4 nights @KASTLES");
		dvo.setMaxCustQty(1);
		return dvo;

	}

	private static Integer testInsert(ProductItemMasterDVO dvo) throws Exception {
		Boolean isExists = ProductItemMasterService.checkIfProdItemIdExistsForSellerProdId(dvo.getClintId(),
				dvo.getSlrpitmsId(), dvo.getSlerId());
		System.out.println(" Is product exists for " + dvo.getSlrpitmsId()   + " [] "  + isExists);		
		ProductItemMasterDTO dto = new ProductItemMasterDTO();
		dto.setDvo(dvo);
		if(!isExists) {
			createProductItem(dto);
		}	
		dto.getDvo().setPdesc("UPDATED DESC FOR PRODUCT ");
		//dto.getDvo().setSlrpitmsId("998877");
		
		//updateProductItemMasterForPK(dto);		
		
		dto  = fetchAllProductItemsForClient( dto ,"");
		List dvoLst = dto.getLst();
		System.out.println("[NO OF PRODUCTS FOR SELLER ] " + dvoLst.size());
		return 1;
	}

	public static void main(String[] args) throws Exception {
		ProductItemMasterDVO dvo = getDVO();
		testInsert(dvo);
		
		
		
		
	}

}
