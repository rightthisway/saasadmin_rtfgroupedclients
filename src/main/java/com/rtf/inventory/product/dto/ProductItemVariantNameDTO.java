/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.inventory.product.dto;

import java.util.List;

import com.rtf.inventory.variants.dvo.ProductItemVariantNameDVO;
import com.rtf.saas.arch.RtfSaasBaseDTO;

/**
 * The Class ProductItemVariantNameDTO.
 */
public class ProductItemVariantNameDTO extends RtfSaasBaseDTO {

	/** The ProductItemVariantNameDVO. */
	private ProductItemVariantNameDVO dvo;

	/** The ProductItemVariantNameDVO list. */
	private List<ProductItemVariantNameDVO> lst;

	/**
	 * Gets the ProductItemVariantNameDVO.
	 *
	 * @return the dvo
	 */
	public ProductItemVariantNameDVO getDvo() {
		return dvo;
	}

	/**
	 * Sets the ProductItemVariantNameDVO.
	 *
	 * @param dvo the new dvo
	 */
	public void setDvo(ProductItemVariantNameDVO dvo) {
		this.dvo = dvo;
	}

	/**
	 * Gets the ProductItemVariantNameDVO list.
	 *
	 * @return the lst
	 */
	public List<ProductItemVariantNameDVO> getLst() {
		return lst;
	}

	/**
	 * Sets the ProductItemVariantNameDVO list.
	 *
	 * @param lst the new lst
	 */
	public void setLst(List<ProductItemVariantNameDVO> lst) {
		this.lst = lst;
	}

}
