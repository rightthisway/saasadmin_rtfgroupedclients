package com.rtf.inventory.seller.service;

import java.sql.Connection;

import com.rtf.inventory.seller.dao.PrivateAppDetailsDAO;
import com.rtf.inventory.seller.dvo.PvtSellerDTO;
import com.rtf.saas.sql.db.DatabaseConnections;
import com.rtf.saas.util.GenUtil;

public class ShopifyService {
	
	public static PvtSellerDTO fetchPrivateSellerDet(PvtSellerDTO dto) throws Exception{
		
		Connection conn = null;	
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			dto =  PrivateAppDetailsDAO.fetchPrivateAppDetails(dto, conn);
			dto.setSts(1);
			if(GenUtil.isEmptyString(dto.getApisky())  || GenUtil.isEmptyString(dto.getApiky())) {
				dto.setSts(0);
				dto.setMsg(" NO Private/Custom Apps configured with 'AVAILABLE' status. ");
			}			
		} catch (Exception ex) {
			dto.setSts(0);
			ex.printStackTrace();
			throw ex;
		} finally {
			DatabaseConnections.closeConnection(conn);
		}
		return dto;
	}

}
