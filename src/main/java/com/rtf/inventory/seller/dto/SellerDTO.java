/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.inventory.seller.dto;

import java.util.List;

import com.rtf.inventory.seller.dvo.SellerDVO;
import com.rtf.saas.arch.RtfSaasBaseDTO;

/**
 * The Class SellerDTO.
 */
public class SellerDTO extends RtfSaasBaseDTO{
	
	/** The dvo. */
	private SellerDVO dvo;
	
	/**
	 * Gets the dvo.
	 *
	 * @return the dvo
	 */
	public SellerDVO getDvo() {
		return dvo;
	}
	
	/**
	 * Sets the dvo.
	 *
	 * @param dvo the new dvo
	 */
	public void setDvo(SellerDVO dvo) {
		this.dvo = dvo;
	}
	
	/**
	 * Gets the lst.
	 *
	 * @return the lst
	 */
	public List<SellerDVO> getLst() {
		return lst;
	}
	
	/**
	 * Sets the lst.
	 *
	 * @param lst the new lst
	 */
	public void setLst(List<SellerDVO> lst) {
		this.lst = lst;
	}
	
	/** The lst. */
	private List<SellerDVO> lst;
	
	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "SellerDTO [dvo=" + dvo + ", lst=" + lst + "]";
	}

}
