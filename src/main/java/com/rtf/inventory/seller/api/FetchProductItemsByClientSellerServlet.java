/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.inventory.seller.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.inventory.product.dto.ProductItemMasterDTO;
import com.rtf.inventory.product.dvo.ProductItemMasterDVO;
import com.rtf.inventory.product.service.ProductItemMasterService;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class FetchProductItemsByClientSellerServlet.
 */

@WebServlet("/livtftchproditemsbysler.json")
public class FetchProductItemsByClientSellerServlet extends RtfSaasBaseServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Process request.
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		ProductItemMasterDTO dto = new ProductItemMasterDTO();
		dto.setSts(0);
		String resMsg = "";
		String clientIdStr = request.getParameter("clId");
		String sellerIdstr = request.getParameter("slerId");
		String pstatus = request.getParameter("pstatus");
		String cau = request.getParameter("cau");
		String filter = request.getParameter("hfilter");
		Integer sellerId = null;
		try {

			if (GenUtil.isNullOrEmpty(cau)) {
				resMsg = "Invalid Login Id:";
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}

			if (GenUtil.isNullOrEmpty(clientIdStr)) {
				resMsg = "Invalid Client Id:" + clientIdStr;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(sellerIdstr)) {
				resMsg = "Invalid Seller Id:" + sellerId;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}
			try {
				sellerId = Integer.parseInt(sellerIdstr);
			} catch (Exception ex) {
				resMsg = "Invalid Seller Id:" + sellerId;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(pstatus)) {
				resMsg = "Invalid Status " + pstatus;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}

			ProductItemMasterDVO dvo = new ProductItemMasterDVO();
			dvo.setClintId(clientIdStr);
			dvo.setSlerId(sellerId);
			dvo.setPstatus(pstatus);
			dvo.setLupdBy(cau);
			dto.setDvo(dvo);
			dto.setSts(0);
			dto = ProductItemMasterService.fetchAllClientProductItemsByClientSeller(dto,filter);
			if (dto.getSts() == 0) {
				setClientMessage(dto, "Error Occured while Creating the Product Item", null);
				generateResponse(request, response, dto);
				return;
			}
			setClientMessage(dto, null, " Product Item fetched Successfully");
			generateResponse(request, response, dto);
			return;

		} catch (Exception e) {
			resMsg = "Error Occured while Fetching the Product Items :" + e.getLocalizedMessage();
			setClientMessage(dto, resMsg, null);
			generateResponse(request, response, dto);
			return;

		} finally {

		}

	}

	/**
	 * Generate response.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}

}
