/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.inventory.seller.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.inventory.seller.dto.SellerDTO;
import com.rtf.inventory.seller.dvo.SellerDVO;
import com.rtf.inventory.seller.service.SellerService;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class AddSellerServlet.
 */

@WebServlet("/addslr.json")

public class AddSellerServlet extends RtfSaasBaseServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Process request.
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		SellerDTO dto = new SellerDTO();
		dto.setSts(0);
		String resMsg = "";

		String clientIdStr = request.getParameter("clId");
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		String compName = request.getParameter("compName");
		String email = request.getParameter("email");
		String phone = request.getParameter("phone");
		String addrLineone = request.getParameter("addrLineone");
		String addrLinetwo = request.getParameter("addrLinetwo");

		String city = request.getParameter("city");
		String state = request.getParameter("state");
		String country = request.getParameter("country");
		String pincode = request.getParameter("pincode");
		String cau = request.getParameter("cau");
		// SELLER TYPE ..
		String sellerType = request.getParameter("slrtype");
		String sellerbaseurl = request.getParameter("slrweburl");
		String apiKey = request.getParameter("aky");
		String apipassword = request.getParameter("apiss");

		try {

			if (GenUtil.isNullOrEmpty(cau)) {
				resMsg = "Invalid Login Id:";
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}

			if (GenUtil.isNullOrEmpty(clientIdStr)) {
				resMsg = "Invalid Client Id:" + clientIdStr;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}

			if (GenUtil.isNullOrEmpty(firstName)) {
				resMsg = "Invalid Seller Name:" + firstName;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(lastName)) {
				resMsg = "Invalid Seller Last Name:" + lastName;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(compName)) {
				resMsg = "Invalid Company Name :" + compName;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(email)) {
				resMsg = "Invalid Email :" + email;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(phone)) {
				resMsg = "Invalid Phone :" + phone;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(sellerType)) {
				resMsg = "Invalid SellerType :" + sellerType;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}
			
			
			if(sellerType.equalsIgnoreCase("SHOPIFY")) {
				
				if (GenUtil.isNullOrEmpty(sellerType)) {
					resMsg = "Invalid SellerType :" + sellerType;
					setClientMessage(dto, resMsg, null);
					generateResponse(request, response, dto);
					return;
				}
				if (GenUtil.isNullOrEmpty(apiKey)) {
					resMsg = "Invalid api  key :" + apiKey;
					setClientMessage(dto, resMsg, null);
					generateResponse(request, response, dto);
					return;
				}
				/*
				 * if (GenUtil.isNullOrEmpty(apipassword)) { resMsg = "Invalid api password :" +
				 * apipassword; setClientMessage(dto, resMsg, null); generateResponse(request,
				 * response, dto); return; }
				 */
				
				
				
				
			}
			
			
			
			
			SellerDVO dvo = new SellerDVO();

			dvo.setClintId(clientIdStr);
			dvo.setFirstName(firstName);
			dvo.setLastName(lastName);
			dvo.setCompName(compName);
			dvo.setEmail(email);
			dvo.setPhone(phone);
			dvo.setAddrLineone(addrLineone);
			dvo.setAddrLinetwo(addrLinetwo);
			dvo.setCity(city);
			dvo.setState(state);
			dvo.setCountry(country);
			dvo.setPincode(pincode);
			dvo.setSlerStatus("ACTIVE");
			dvo.setLupdBy(cau);
			dvo.setApibaseurl(sellerbaseurl);
			dvo.setApiKey(apiKey);
			dvo.setApiPass(apipassword);
			dvo.setSellerType(sellerType);
			dto.setDvo(dvo);
			dto.setSts(0);
			dto = SellerService.createSeller(dto);
			if (dto.getSts() == 0) {
				String msg = "Error Occured while Creating the Seller";
				if (!GenUtil.isEmptyString(dto.getMsg())) {
					msg = dto.getMsg();
					if (msg.contains("UK_rtf_seller_dtls_phone")) {
						msg = "Phone Number is already existing/registered in our records";
					}
				}
				setClientMessage(dto, msg, null);
				generateResponse(request, response, dto);
				return;
			}
			setClientMessage(dto, null, " Seller Created Successfully");
			generateResponse(request, response, dto);
			return;

		} catch (Exception e) {
			resMsg = "Error Occured while Creating the Product Item :" + e.getLocalizedMessage();
			setClientMessage(dto, resMsg, null);
			generateResponse(request, response, dto);
			return;

		} 

	}

	/**
	 * Generate Http Response for Client.Response, data is sent in JSON format.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}

}
