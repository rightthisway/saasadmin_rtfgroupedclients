package com.rtf.inventory.dvo;

public class RtfSlrProdUpldTemplateDVO {	
	private Integer sputID;
	private Integer slrID;
	private String pName;
	private String pBrand;
	private String pDesc;
	private String pLongDesc;
	private Double regPrc;
	private Double selPrc;
	private Double discOff;
	private Integer avilQty;
	private String varNameA;
	private String varValueA;
	private String pImg;
	private String updFileName;
	private String updBy;
	private Long updDate;
	
	
	
	public Integer getSputID() {
		return sputID;
	}
	public void setSputID(Integer sputID) {
		this.sputID = sputID;
	}
	public Integer getSlrID() {
		return slrID;
	}
	public void setSlrID(Integer slrID) {
		this.slrID = slrID;
	}
	public String getpName() {
		return pName;
	}
	public void setpName(String pName) {
		this.pName = pName;
	}
	public String getpBrand() {
		return pBrand;
	}
	public void setpBrand(String pBrand) {
		this.pBrand = pBrand;
	}
	public String getpDesc() {
		return pDesc;
	}
	public void setpDesc(String pDesc) {
		this.pDesc = pDesc;
	}
	public String getpLongDesc() {
		return pLongDesc;
	}
	public void setpLongDesc(String pLongDesc) {
		this.pLongDesc = pLongDesc;
	}
	public Double getRegPrc() {
		return regPrc;
	}
	public void setRegPrc(Double regPrc) {
		this.regPrc = regPrc;
	}
	public Double getSelPrc() {
		return selPrc;
	}
	public void setSelPrc(Double selPrc) {
		this.selPrc = selPrc;
	}
	public Double getDiscOff() {
		return discOff;
	}
	public void setDiscOff(Double discOff) {
		this.discOff = discOff;
	}
	public Integer getAvilQty() {
		return avilQty;
	}
	public void setAvilQty(Integer avilQty) {
		this.avilQty = avilQty;
	}
	public String getVarNameA() {
		return varNameA;
	}
	public void setVarNameA(String varNameA) {
		this.varNameA = varNameA;
	}
	public String getVarValueA() {
		return varValueA;
	}
	public void setVarValueA(String varValueA) {
		this.varValueA = varValueA;
	}
	public String getpImg() {
		return pImg;
	}
	public void setpImg(String pImg) {
		this.pImg = pImg;
	}
	public String getUpdFileName() {
		return updFileName;
	}
	public void setUpdFileName(String updFileName) {
		this.updFileName = updFileName;
	}
	public String getUpdBy() {
		return updBy;
	}
	public void setUpdBy(String updBy) {
		this.updBy = updBy;
	}
	public Long getUpdDate() {
		return updDate;
	}
	public void setUpdDate(Long updDate) {
		this.updDate = updDate;
	}
	@Override
	public String toString() {
		return "RtfSlrProdUpldTemplateDVO [sputID=" + sputID + ", slrID=" + slrID + ", pName=" + pName + ", pBrand="
				+ pBrand + ", pDesc=" + pDesc + ", pLongDesc=" + pLongDesc + ", regPrc=" + regPrc + ", selPrc=" + selPrc
				+ ", discOff=" + discOff + ", avilQty=" + avilQty + ", varNameA=" + varNameA + ", varValueA="
				+ varValueA + ", pImg=" + pImg + ", updFileName=" + updFileName + ", updBy=" + updBy + ", updDate="
				+ updDate + "]";
	}
}