/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.inventory.variants.dvo;

/**
 * The Class ProductInventoryDVO.
 */
public class ProductInventoryDVO {

	/** The clint id. */
	private String clintId;

	/** The slrinv id. */
	private Integer slrinvId;

	/** The slrpivarcom id. */
	private Integer slrpivarcomId;

	/** The setuniq id. */
	private Integer setuniqId;

	/** The slrpitms id. */
	private String slrpitmsId;

	/** The sler id. */
	private Integer slerId;

	/** The pbrand. */
	private String pbrand;

	/** The voptnamevalcom. */
	private String voptnamevalcom;

	/** The pname. */
	private String pname;

	/** The pregprc. */
	private Double pregprc;

	/** The pselprc. */
	private Double pselprc;

	/** The reqltypnts. */
	private Integer reqltypnts;

	/** The pprcaftltypnts. */
	private Double pprcaftltypnts;

	/** The avlqty. */
	private Integer avlqty;

	/** The sldqty. */
	private Integer sldqty;

	/** The prdstatus. */
	private String prdstatus;

	/** The pimg. */
	private String pimg;

	/** The cat id. */
	private Integer catId;

	/** The subcat id. */
	private Integer subcatId;

	/** The exp date. */
	private Long expDate;

	/** The vonameone. */
	private String vonameone;

	/** The vovalueone. */
	private String vovalueone;

	/** The vonametwo. */
	private String vonametwo;

	/** The vovaluetwo. */
	private String vovaluetwo;

	/** The vonamethree. */
	private String vonamethree;

	/** The vovaluethree. */
	private String vovaluethree;

	/** The vonamefour. */
	private String vonamefour;

	/** The vovaluefour. */
	private String vovaluefour;

	/** The slrpivar idone. */
	private Integer slrpivarIdone;

	/** The varoval idone. */
	private Integer varovalIdone;

	/** The slrpivar idtwo. */
	private Integer slrpivarIdtwo;

	/** The varoval idtwo. */
	private Integer varovalIdtwo;

	/** The slrpivar idthree. */
	private Integer slrpivarIdthree;

	/** The varoval idthree. */
	private Integer varovalIdthree;

	/** The slrpivar idfour. */
	private Integer slrpivarIdfour;

	/** The varoval idfour. */
	private Integer varovalIdfour;

	/** The lupd date. */
	private Long lupdDate;

	/** The lupd by. */
	private String lupdBy;

	/** The voptnameval idcom. */
	// For Matrix
	private String voptnamevalIdcom;

	/**
	 * Gets the clint id.
	 *
	 * @return the clint id
	 */
	public String getClintId() {
		return clintId;
	}

	/**
	 * Sets the clint id.
	 *
	 * @param clintId the new clint id
	 */
	public void setClintId(String clintId) {
		this.clintId = clintId;
	}

	/**
	 * Gets the slrinv id.
	 *
	 * @return the slrinv id
	 */
	public Integer getSlrinvId() {
		return slrinvId;
	}

	/**
	 * Sets the slrinv id.
	 *
	 * @param slrinvId the new slrinv id
	 */
	public void setSlrinvId(Integer slrinvId) {
		this.slrinvId = slrinvId;
	}

	/**
	 * Gets the slrpivarcom id.
	 *
	 * @return the slrpivarcom id
	 */
	public Integer getSlrpivarcomId() {
		return slrpivarcomId;
	}

	/**
	 * Sets the slrpivarcom id.
	 *
	 * @param slrpivarcomId the new slrpivarcom id
	 */
	public void setSlrpivarcomId(Integer slrpivarcomId) {
		this.slrpivarcomId = slrpivarcomId;
	}

	/**
	 * Gets the setuniq id.
	 *
	 * @return the setuniq id
	 */
	public Integer getSetuniqId() {
		return setuniqId;
	}

	/**
	 * Sets the setuniq id.
	 *
	 * @param setuniqId the new setuniq id
	 */
	public void setSetuniqId(Integer setuniqId) {
		this.setuniqId = setuniqId;
	}

	/**
	 * Gets the slrpitms id.
	 *
	 * @return the slrpitms id
	 */
	public String getSlrpitmsId() {
		return slrpitmsId;
	}

	/**
	 * Sets the slrpitms id.
	 *
	 * @param slrpitmsId the new slrpitms id
	 */
	public void setSlrpitmsId(String slrpitmsId) {
		this.slrpitmsId = slrpitmsId;
	}

	/**
	 * Gets the sler id.
	 *
	 * @return the sler id
	 */
	public Integer getSlerId() {
		return slerId;
	}

	/**
	 * Sets the sler id.
	 *
	 * @param slerId the new sler id
	 */
	public void setSlerId(Integer slerId) {
		this.slerId = slerId;
	}

	/**
	 * Gets the pbrand.
	 *
	 * @return the pbrand
	 */
	public String getPbrand() {
		return pbrand;
	}

	/**
	 * Sets the pbrand.
	 *
	 * @param pbrand the new pbrand
	 */
	public void setPbrand(String pbrand) {
		this.pbrand = pbrand;
	}

	/**
	 * Gets the voptnamevalcom.
	 *
	 * @return the voptnamevalcom
	 */
	public String getVoptnamevalcom() {
		return voptnamevalcom;
	}

	/**
	 * Sets the voptnamevalcom.
	 *
	 * @param voptnamevalcom the new voptnamevalcom
	 */
	public void setVoptnamevalcom(String voptnamevalcom) {
		this.voptnamevalcom = voptnamevalcom;
	}

	/**
	 * Gets the pname.
	 *
	 * @return the pname
	 */
	public String getPname() {
		return pname;
	}

	/**
	 * Sets the pname.
	 *
	 * @param pname the new pname
	 */
	public void setPname(String pname) {
		this.pname = pname;
	}

	/**
	 * Gets the pregprc.
	 *
	 * @return the pregprc
	 */
	public Double getPregprc() {
		return pregprc;
	}

	/**
	 * Sets the pregprc.
	 *
	 * @param pregprc the new pregprc
	 */
	public void setPregprc(Double pregprc) {
		this.pregprc = pregprc;
	}

	/**
	 * Gets the pselprc.
	 *
	 * @return the pselprc
	 */
	public Double getPselprc() {
		return pselprc;
	}

	/**
	 * Sets the pselprc.
	 *
	 * @param pselprc the new pselprc
	 */
	public void setPselprc(Double pselprc) {
		this.pselprc = pselprc;
	}

	/**
	 * Gets the reqltypnts.
	 *
	 * @return the reqltypnts
	 */
	public Integer getReqltypnts() {
		return reqltypnts;
	}

	/**
	 * Sets the reqltypnts.
	 *
	 * @param reqltypnts the new reqltypnts
	 */
	public void setReqltypnts(Integer reqltypnts) {
		this.reqltypnts = reqltypnts;
	}

	/**
	 * Gets the pprcaftltypnts.
	 *
	 * @return the pprcaftltypnts
	 */
	public Double getPprcaftltypnts() {
		return pprcaftltypnts;
	}

	/**
	 * Sets the pprcaftltypnts.
	 *
	 * @param pprcaftltypnts the new pprcaftltypnts
	 */
	public void setPprcaftltypnts(Double pprcaftltypnts) {
		this.pprcaftltypnts = pprcaftltypnts;
	}

	/**
	 * Gets the avlqty.
	 *
	 * @return the avlqty
	 */
	public Integer getAvlqty() {
		return avlqty;
	}

	/**
	 * Sets the avlqty.
	 *
	 * @param avlqty the new avlqty
	 */
	public void setAvlqty(Integer avlqty) {
		this.avlqty = avlqty;
	}

	/**
	 * Gets the sldqty.
	 *
	 * @return the sldqty
	 */
	public Integer getSldqty() {
		return sldqty;
	}

	/**
	 * Sets the sldqty.
	 *
	 * @param sldqty the new sldqty
	 */
	public void setSldqty(Integer sldqty) {
		this.sldqty = sldqty;
	}

	/**
	 * Gets the prdstatus.
	 *
	 * @return the prdstatus
	 */
	public String getPrdstatus() {
		return prdstatus;
	}

	/**
	 * Sets the prdstatus.
	 *
	 * @param prdstatus the new prdstatus
	 */
	public void setPrdstatus(String prdstatus) {
		this.prdstatus = prdstatus;
	}

	/**
	 * Gets the pimg.
	 *
	 * @return the pimg
	 */
	public String getPimg() {
		return pimg;
	}

	/**
	 * Sets the pimg.
	 *
	 * @param pimg the new pimg
	 */
	public void setPimg(String pimg) {
		this.pimg = pimg;
	}

	/**
	 * Gets the cat id.
	 *
	 * @return the cat id
	 */
	public Integer getCatId() {
		return catId;
	}

	/**
	 * Sets the cat id.
	 *
	 * @param catId the new cat id
	 */
	public void setCatId(Integer catId) {
		this.catId = catId;
	}

	/**
	 * Gets the subcat id.
	 *
	 * @return the subcat id
	 */
	public Integer getSubcatId() {
		return subcatId;
	}

	/**
	 * Sets the subcat id.
	 *
	 * @param subcatId the new subcat id
	 */
	public void setSubcatId(Integer subcatId) {
		this.subcatId = subcatId;
	}

	/**
	 * Gets the exp date.
	 *
	 * @return the exp date
	 */
	public Long getExpDate() {
		return expDate;
	}

	/**
	 * Sets the exp date.
	 *
	 * @param expDate the new exp date
	 */
	public void setExpDate(Long expDate) {
		this.expDate = expDate;
	}

	/**
	 * Gets the vonameone.
	 *
	 * @return the vonameone
	 */
	public String getVonameone() {
		return vonameone;
	}

	/**
	 * Sets the vonameone.
	 *
	 * @param vonameone the new vonameone
	 */
	public void setVonameone(String vonameone) {
		this.vonameone = vonameone;
	}

	/**
	 * Gets the vovalueone.
	 *
	 * @return the vovalueone
	 */
	public String getVovalueone() {
		return vovalueone;
	}

	/**
	 * Sets the vovalueone.
	 *
	 * @param vovalueone the new vovalueone
	 */
	public void setVovalueone(String vovalueone) {
		this.vovalueone = vovalueone;
	}

	/**
	 * Gets the vonametwo.
	 *
	 * @return the vonametwo
	 */
	public String getVonametwo() {
		return vonametwo;
	}

	/**
	 * Sets the vonametwo.
	 *
	 * @param vonametwo the new vonametwo
	 */
	public void setVonametwo(String vonametwo) {
		this.vonametwo = vonametwo;
	}

	/**
	 * Gets the vovaluetwo.
	 *
	 * @return the vovaluetwo
	 */
	public String getVovaluetwo() {
		return vovaluetwo;
	}

	/**
	 * Sets the vovaluetwo.
	 *
	 * @param vovaluetwo the new vovaluetwo
	 */
	public void setVovaluetwo(String vovaluetwo) {
		this.vovaluetwo = vovaluetwo;
	}

	/**
	 * Gets the vonamethree.
	 *
	 * @return the vonamethree
	 */
	public String getVonamethree() {
		return vonamethree;
	}

	/**
	 * Sets the vonamethree.
	 *
	 * @param vonamethree the new vonamethree
	 */
	public void setVonamethree(String vonamethree) {
		this.vonamethree = vonamethree;
	}

	/**
	 * Gets the vovaluethree.
	 *
	 * @return the vovaluethree
	 */
	public String getVovaluethree() {
		return vovaluethree;
	}

	/**
	 * Sets the vovaluethree.
	 *
	 * @param vovaluethree the new vovaluethree
	 */
	public void setVovaluethree(String vovaluethree) {
		this.vovaluethree = vovaluethree;
	}

	/**
	 * Gets the vonamefour.
	 *
	 * @return the vonamefour
	 */
	public String getVonamefour() {
		return vonamefour;
	}

	/**
	 * Sets the vonamefour.
	 *
	 * @param vonamefour the new vonamefour
	 */
	public void setVonamefour(String vonamefour) {
		this.vonamefour = vonamefour;
	}

	/**
	 * Gets the vovaluefour.
	 *
	 * @return the vovaluefour
	 */
	public String getVovaluefour() {
		return vovaluefour;
	}

	/**
	 * Sets the vovaluefour.
	 *
	 * @param vovaluefour the new vovaluefour
	 */
	public void setVovaluefour(String vovaluefour) {
		this.vovaluefour = vovaluefour;
	}

	/**
	 * Gets the slrpivar idone.
	 *
	 * @return the slrpivar idone
	 */
	public Integer getSlrpivarIdone() {
		return slrpivarIdone;
	}

	/**
	 * Sets the slrpivar idone.
	 *
	 * @param slrpivarIdone the new slrpivar idone
	 */
	public void setSlrpivarIdone(Integer slrpivarIdone) {
		this.slrpivarIdone = slrpivarIdone;
	}

	/**
	 * Gets the varoval idone.
	 *
	 * @return the varoval idone
	 */
	public Integer getVarovalIdone() {
		return varovalIdone;
	}

	/**
	 * Sets the varoval idone.
	 *
	 * @param varovalIdone the new varoval idone
	 */
	public void setVarovalIdone(Integer varovalIdone) {
		this.varovalIdone = varovalIdone;
	}

	/**
	 * Gets the slrpivar idtwo.
	 *
	 * @return the slrpivar idtwo
	 */
	public Integer getSlrpivarIdtwo() {
		return slrpivarIdtwo;
	}

	/**
	 * Sets the slrpivar idtwo.
	 *
	 * @param slrpivarIdtwo the new slrpivar idtwo
	 */
	public void setSlrpivarIdtwo(Integer slrpivarIdtwo) {
		this.slrpivarIdtwo = slrpivarIdtwo;
	}

	/**
	 * Gets the varoval idtwo.
	 *
	 * @return the varoval idtwo
	 */
	public Integer getVarovalIdtwo() {
		return varovalIdtwo;
	}

	/**
	 * Sets the varoval idtwo.
	 *
	 * @param varovalIdtwo the new varoval idtwo
	 */
	public void setVarovalIdtwo(Integer varovalIdtwo) {
		this.varovalIdtwo = varovalIdtwo;
	}

	/**
	 * Gets the slrpivar idthree.
	 *
	 * @return the slrpivar idthree
	 */
	public Integer getSlrpivarIdthree() {
		return slrpivarIdthree;
	}

	/**
	 * Sets the slrpivar idthree.
	 *
	 * @param slrpivarIdthree the new slrpivar idthree
	 */
	public void setSlrpivarIdthree(Integer slrpivarIdthree) {
		this.slrpivarIdthree = slrpivarIdthree;
	}

	/**
	 * Gets the varoval idthree.
	 *
	 * @return the varoval idthree
	 */
	public Integer getVarovalIdthree() {
		return varovalIdthree;
	}

	/**
	 * Sets the varoval idthree.
	 *
	 * @param varovalIdthree the new varoval idthree
	 */
	public void setVarovalIdthree(Integer varovalIdthree) {
		this.varovalIdthree = varovalIdthree;
	}

	/**
	 * Gets the slrpivar idfour.
	 *
	 * @return the slrpivar idfour
	 */
	public Integer getSlrpivarIdfour() {
		return slrpivarIdfour;
	}

	/**
	 * Sets the slrpivar idfour.
	 *
	 * @param slrpivarIdfour the new slrpivar idfour
	 */
	public void setSlrpivarIdfour(Integer slrpivarIdfour) {
		this.slrpivarIdfour = slrpivarIdfour;
	}

	/**
	 * Gets the varoval idfour.
	 *
	 * @return the varoval idfour
	 */
	public Integer getVarovalIdfour() {
		return varovalIdfour;
	}

	/**
	 * Sets the varoval idfour.
	 *
	 * @param varovalIdfour the new varoval idfour
	 */
	public void setVarovalIdfour(Integer varovalIdfour) {
		this.varovalIdfour = varovalIdfour;
	}

	/**
	 * Gets the lupd date.
	 *
	 * @return the lupd date
	 */
	public Long getLupdDate() {
		return lupdDate;
	}

	/**
	 * Sets the lupd date.
	 *
	 * @param lupdDate the new lupd date
	 */
	public void setLupdDate(Long lupdDate) {
		this.lupdDate = lupdDate;
	}

	/**
	 * Gets the lupd by.
	 *
	 * @return the lupd by
	 */
	public String getLupdBy() {
		return lupdBy;
	}

	/**
	 * Sets the lupd by.
	 *
	 * @param lupdBy the new lupd by
	 */
	public void setLupdBy(String lupdBy) {
		this.lupdBy = lupdBy;
	}

	/**
	 * Gets the voptnameval idcom.
	 *
	 * @return the voptnameval idcom
	 */
	public String getVoptnamevalIdcom() {
		return voptnamevalIdcom;
	}

	/**
	 * Sets the voptnameval idcom.
	 *
	 * @param voptnamevalIdcom the new voptnameval idcom
	 */
	public void setVoptnamevalIdcom(String voptnamevalIdcom) {
		this.voptnamevalIdcom = voptnamevalIdcom;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "ProductInventoryDVO [clintId=" + clintId + ", slrinvId=" + slrinvId + ", slrpivarcomId=" + slrpivarcomId
				+ ", setuniqId=" + setuniqId + ", slrpitmsId=" + slrpitmsId + ", slerId=" + slerId + ", pbrand="
				+ pbrand + ", voptnamevalcom=" + voptnamevalcom + ", pname=" + pname + ", pregprc=" + pregprc
				+ ", pselprc=" + pselprc + ", reqltypnts=" + reqltypnts + ", pprcaftltypnts=" + pprcaftltypnts
				+ ", avlqty=" + avlqty + ", sldqty=" + sldqty + ", prdstatus=" + prdstatus + ", pimg=" + pimg
				+ ", catId=" + catId + ", subcatId=" + subcatId + ", expDate=" + expDate + ", vonameone=" + vonameone
				+ ", vovalueone=" + vovalueone + ", vonametwo=" + vonametwo + ", vovaluetwo=" + vovaluetwo
				+ ", vonamethree=" + vonamethree + ", vovaluethree=" + vovaluethree + ", vonamefour=" + vonamefour
				+ ", vovaluefour=" + vovaluefour + ", slrpivarIdone=" + slrpivarIdone + ", varovalIdone=" + varovalIdone
				+ ", slrpivarIdtwo=" + slrpivarIdtwo + ", varovalIdtwo=" + varovalIdtwo + ", slrpivarIdthree="
				+ slrpivarIdthree + ", varovalIdthree=" + varovalIdthree + ", slrpivarIdfour=" + slrpivarIdfour
				+ ", varovalIdfour=" + varovalIdfour + ", lupdDate=" + lupdDate + ", lupdBy=" + lupdBy
				+ ", voptnamevalIdcom=" + voptnamevalIdcom + "]";
	}
}
