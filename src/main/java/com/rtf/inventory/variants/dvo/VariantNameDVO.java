/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.inventory.variants.dvo;

/**
 * The Class VariantNameDVO.
 */
public class VariantNameDVO {

	/** The clint id. */
	private String clintId;

	/** The slrpivar id. */
	private Integer slrpivarId;

	/** The slrpitm id. */
	private String slrpitmId;

	/** The sler id. */
	private Integer slerId;

	/** The variant name. */
	private String variantName;

	/**
	 * Gets the clint id.
	 *
	 * @return the clint id
	 */
	public String getClintId() {
		return clintId;
	}

	/**
	 * Sets the clint id.
	 *
	 * @param clintId the new clint id
	 */
	public void setClintId(String clintId) {
		this.clintId = clintId;
	}

	/**
	 * Gets the slrpivar id.
	 *
	 * @return the slrpivar id
	 */
	public Integer getSlrpivarId() {
		return slrpivarId;
	}

	/**
	 * Sets the slrpivar id.
	 *
	 * @param slrpivarId the new slrpivar id
	 */
	public void setSlrpivarId(Integer slrpivarId) {
		this.slrpivarId = slrpivarId;
	}

	/**
	 * Gets the slrpitm id.
	 *
	 * @return the slrpitm id
	 */
	public String getSlrpitmId() {
		return slrpitmId;
	}

	/**
	 * Sets the slrpitm id.
	 *
	 * @param slrpitmId the new slrpitm id
	 */
	public void setSlrpitmId(String slrpitmId) {
		this.slrpitmId = slrpitmId;
	}

	/**
	 * Gets the sler id.
	 *
	 * @return the sler id
	 */
	public Integer getSlerId() {
		return slerId;
	}

	/**
	 * Sets the sler id.
	 *
	 * @param slerId the new sler id
	 */
	public void setSlerId(Integer slerId) {
		this.slerId = slerId;
	}

	/**
	 * Gets the variant name.
	 *
	 * @return the variant name
	 */
	public String getVariantName() {
		return variantName;
	}

	/**
	 * Sets the variant name.
	 *
	 * @param variantName the new variant name
	 */
	public void setVariantName(String variantName) {
		this.variantName = variantName;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "VariantNameDVO [clintId=" + clintId + ", slrpivarId=" + slrpivarId + ", slrpitmId=" + slrpitmId
				+ ", slerId=" + slerId + ", variantName=" + variantName + "]";
	}

}
