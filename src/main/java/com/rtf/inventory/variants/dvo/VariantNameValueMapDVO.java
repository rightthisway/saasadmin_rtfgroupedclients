/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.inventory.variants.dvo;

import java.util.List;

/**
 * The Class VariantNameValueMapDVO.
 */
public class VariantNameValueMapDVO {

	/** The voptname. */
	private String voptname;

	/** The voptvalue. */
	private List<String> voptvalue;

	/** The voptvalue iimgurl. */
	private List<String> voptvalueIimgurl;

	/**
	 * Gets the voptname.
	 *
	 * @return the voptname
	 */
	public String getVoptname() {
		return voptname;
	}

	/**
	 * Sets the voptname.
	 *
	 * @param voptname the new voptname
	 */
	public void setVoptname(String voptname) {
		this.voptname = voptname;
	}

	/**
	 * Gets the voptvalue.
	 *
	 * @return the voptvalue
	 */
	public List<String> getVoptvalue() {
		return voptvalue;
	}

	/**
	 * Sets the voptvalue.
	 *
	 * @param voptvalue the new voptvalue
	 */
	public void setVoptvalue(List<String> voptvalue) {
		this.voptvalue = voptvalue;
	}

	/**
	 * Gets the voptvalue iimgurl.
	 *
	 * @return the voptvalue iimgurl
	 */
	public List<String> getVoptvalueIimgurl() {
		return voptvalueIimgurl;
	}

	/**
	 * Sets the voptvalue iimgurl.
	 *
	 * @param voptvalueIimgurl the new voptvalue iimgurl
	 */
	public void setVoptvalueIimgurl(List<String> voptvalueIimgurl) {
		this.voptvalueIimgurl = voptvalueIimgurl;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "VariantNameValueMapDVO [voptname=" + voptname + ", voptvalue=" + voptvalue + ", voptvalueIimgurl="
				+ voptvalueIimgurl + "]";
	}

}
