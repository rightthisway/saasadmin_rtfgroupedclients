/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.inventory.variants.dto;

import com.rtf.inventory.variants.dvo.ProductItemVariantNameDVO;
import com.rtf.saas.arch.RtfSaasBaseDTO;

/**
 * The Class VariantOptionValueDTO.
 */
public class VariantOptionValueDTO extends RtfSaasBaseDTO {

	/** The clint id. */
	private String clintId;

	/** The slrpitms id. */
	private String slrpitmsId;

	/** The sler id. */
	private String slerId;

	/** The variant id. */
	private String variantId;

	/** The dvo. */
	private ProductItemVariantNameDVO dvo;

	/**
	 * Gets the clint id.
	 *
	 * @return the clint id
	 */
	public String getClintId() {
		return clintId;
	}

	/**
	 * Sets the clint id.
	 *
	 * @param clintId the new clint id
	 */
	public void setClintId(String clintId) {
		this.clintId = clintId;
	}

	/**
	 * Gets the slrpitms id.
	 *
	 * @return the slrpitms id
	 */
	public String getSlrpitmsId() {
		return slrpitmsId;
	}

	/**
	 * Sets the slrpitms id.
	 *
	 * @param slrpitmsId the new slrpitms id
	 */
	public void setSlrpitmsId(String slrpitmsId) {
		this.slrpitmsId = slrpitmsId;
	}

	/**
	 * Gets the sler id.
	 *
	 * @return the sler id
	 */
	public String getSlerId() {
		return slerId;
	}

	/**
	 * Sets the sler id.
	 *
	 * @param slerId the new sler id
	 */
	public void setSlerId(String slerId) {
		this.slerId = slerId;
	}

	/**
	 * Gets the variant id.
	 *
	 * @return the variant id
	 */
	public String getVariantId() {
		return variantId;
	}

	/**
	 * Sets the variant id.
	 *
	 * @param variantId the new variant id
	 */
	public void setVariantId(String variantId) {
		this.variantId = variantId;
	}

	/**
	 * Gets the dvo.
	 *
	 * @return the dvo
	 */
	public ProductItemVariantNameDVO getDvo() {
		return dvo;
	}

	/**
	 * Sets the dvo.
	 *
	 * @param dvo the new dvo
	 */
	public void setDvo(ProductItemVariantNameDVO dvo) {
		this.dvo = dvo;
	}

}
