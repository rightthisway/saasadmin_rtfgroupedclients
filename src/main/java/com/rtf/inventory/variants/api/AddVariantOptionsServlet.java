/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.inventory.variants.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.inventory.common.util.MultiFiIesUploadUtil;
import com.rtf.inventory.variants.dto.VariantOptionValueDTO;
import com.rtf.inventory.variants.dvo.ProductItemVariantNameDVO;
import com.rtf.inventory.variants.service.VariantOptionService;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class AddVariantOptionsServlet.
 */

@WebServlet("/livtaddvaroptns.json")
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 10, // 10 MB
		maxFileSize = 1024 * 1024 * 20, // 20 MB
		maxRequestSize = 1024 * 1024 * 20) // 20 MB
public class AddVariantOptionsServlet extends RtfSaasBaseServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Process request.
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		VariantOptionValueDTO dto = new VariantOptionValueDTO();
		dto.setSts(0);
		String resMsg = "";

		String clientIdStr = request.getParameter("clId");
		String sellerprodid = request.getParameter("slrpitmsId");
		String sellerId = request.getParameter("slerId");
		String varname = request.getParameter("varname");
		String optvalue = request.getParameter("optvalue");
		String cau = request.getParameter("cau");

		try {

			if (GenUtil.isNullOrEmpty(cau)) {
				resMsg = "Invalid Login Id:";
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}

			if (GenUtil.isNullOrEmpty(clientIdStr)) {
				resMsg = "Invalid Client Id:" + clientIdStr;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}

			if (GenUtil.isNullOrEmpty(sellerprodid)) {
				resMsg = "Invalid Seller Product Id:" + sellerprodid;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(sellerId)) {
				resMsg = "Invalid Seller Id:" + sellerId;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(optvalue)) {
				resMsg = "Invalid option value  :" + optvalue;
				setClientMessage(dto, resMsg, null);
			}
			if (GenUtil.isNullOrEmpty(varname)) {
				resMsg = "Invalid Variant Name Description :" + varname;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}

			dto.setClId(clientIdStr);
			dto.setSlerId(sellerId);
			dto.setSlrpitmsId(sellerprodid);

			Map<String, Map<String, String>> varOptValFileNameMap = new HashMap<String, Map<String, String>>();
			String[] variantNamesArray = request.getParameterValues("varname");
			for (String variantname : variantNamesArray) {
				Map<String, String> optvalFileNameMap = new HashMap<String, String>();
				varOptValFileNameMap.put(variantname, optvalFileNameMap);
			}

			for (String variantname : varOptValFileNameMap.keySet()) {

				String[] variantoptvaluesArray = request.getParameterValues("opt" + variantname);
				for (String otpval : variantoptvaluesArray) {
					varOptValFileNameMap.get(variantname).put(otpval, null);
				}

			}

			try {
				varOptValFileNameMap = MultiFiIesUploadUtil.uploadMultipleFiles(request, clientIdStr,
						varOptValFileNameMap);
			} catch (Exception ex) {
				resMsg = "Invalid option value  :" + optvalue;
				setClientMessage(dto, ex.getMessage(), null);
				generateResponse(request, response, dto);
				return;
			}

			ProductItemVariantNameDVO dvo = new ProductItemVariantNameDVO();
			dvo.setClintId(clientIdStr);
			dvo.setSlerId(sellerId);
			dvo.setSlrpitmsId(sellerprodid);
			dvo.setLupdBy(cau);
			dvo.setVariantNameOptValFileNameMap(varOptValFileNameMap);
			dto.setDvo(dvo);
			dto = VariantOptionService.saveVaiantOptionValues(dto);
			if (dto.getSts() == 0) {
				setClientMessage(dto, "Error Occured while Creating the Option Name & Value Variants ", null);
				generateResponse(request, response, dto);
				return;
			}
			Integer matrixcnt = VariantOptionService.saveVaiantProductMatrixValues(dto);
			if (matrixcnt <= 0) {
				setClientMessage(dto, "Error Occured while Creating the Option Name & Value Variants Matrix ", null);
				generateResponse(request, response, dto);
				return;
			}
			setClientMessage(dto, null, " Option Variants/Values & Product Matrix Created Successfully");
			generateResponse(request, response, dto);
			return;
		} catch (Exception e) {
			resMsg = "Error Occured while Creating the Option Name & Value Variants :" + e.getLocalizedMessage();
			if (e.getLocalizedMessage().contains("UK_ids_vopt_name")) {
				resMsg = "Error Occured while Creating the Option Name & Value Variants. Duplicate Variant Name";
			}
			setClientMessage(dto, resMsg, null);
			generateResponse(request, response, dto);
			return;
		}

	}

	/**
	 * Generate response.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}

}
