/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.inventory.variants.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.inventory.variants.dto.ProductInventoryDTO;
import com.rtf.inventory.variants.service.InventoryService;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class FetchInventoryForProductIdServlet.
 */
@WebServlet("/fetchinventoryforprodid.json")
public class FetchInventoryForProductIdServlet extends RtfSaasBaseServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Process request.
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		ProductInventoryDTO dto = new ProductInventoryDTO();
		dto.setSts(0);
		String resMsg = "";

		String clientIdStr = request.getParameter("clId");
		String sellerprodid = request.getParameter("slrpitmsId");
		String sellerIdstr = request.getParameter("slerId");
		String cau = request.getParameter("cau");
		String reqstatus = request.getParameter("status");
		Integer sellerId = null;

		try {

			if (GenUtil.isNullOrEmpty(cau)) {
				resMsg = "Invalid Login Id:";
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}

			if (GenUtil.isNullOrEmpty(clientIdStr)) {
				resMsg = "Invalid Client Id:" + clientIdStr;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}

			if (GenUtil.isNullOrEmpty(sellerIdstr)) {
				resMsg = "Invalid Seller Id:" + sellerId;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}

			if (GenUtil.isNullOrEmpty(reqstatus)) {
				resMsg = "Invalid Product Inventory Status:" + reqstatus;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}
			try {
				sellerId = Integer.parseInt(sellerIdstr);
			} catch (Exception ex) {
				resMsg = "Invalid Seller Id:" + sellerId;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}
			dto.setClId(clientIdStr);
			dto.setSlerId(sellerId);
			dto.setSlrpitmsId(sellerprodid);
			dto.setPrdstatus(reqstatus);
			dto = InventoryService.fetchProductInventoryForProdId(dto);
			setClientMessage(dto, null, " Product Inventory Matrix listed Successfully");
			generateResponse(request, response, dto);
			return;

		} catch (Exception e) {
			resMsg = "Error Occured while Fetching Product Inventory :" + e.getLocalizedMessage();
			setClientMessage(dto, resMsg, null);
			generateResponse(request, response, dto);
			return;

		} finally {

		}

	}

	/**
	 * Generate response.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}

}
