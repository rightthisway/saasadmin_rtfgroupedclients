/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.inventory.variants.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.inventory.variants.dto.ProductInventoryDTO;
import com.rtf.inventory.variants.service.InventoryService;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class AddProductVariantInventoryServlet.
 */
@WebServlet("/addvariantinventory.json")
public class AddProductVariantInventoryServlet extends RtfSaasBaseServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Process request.
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		ProductInventoryDTO dto = new ProductInventoryDTO();
		dto.setSts(0);
		String resMsg = "";

		String clientIdStr = request.getParameter("clId");
		String sellerprodid = request.getParameter("slrpitmsId");
		String sellerIdstr = request.getParameter("slerId");
		String cau = request.getParameter("cau");

		String[] slrpivarcomIdarry = request.getParameterValues("slrpivarcomId");
		String[] setuniqIdarry = request.getParameterValues("setuniqId");
		String[] pregprcarry = request.getParameterValues("pregprc");
		String[] pselprcarry = request.getParameterValues("pselprc");	
		String[] avlqtyarry = request.getParameterValues("avlqty");
		Integer sellerId = null;

		try {

			if (slrpivarcomIdarry == null || slrpivarcomIdarry.length == 0) {
				resMsg = "Invalid Variant Ids";
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}
			if (setuniqIdarry == null || setuniqIdarry.length == 0) {
				resMsg = "Invalid Variant  unique set Ids";
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}
			if (pregprcarry == null || pregprcarry.length == 0) {
				resMsg = "Invalid Variant Regular Selling Price";
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}
			if (pselprcarry == null || pselprcarry.length == 0) {
				resMsg = "Invalid Variant Discounted Price ";
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}
			if (avlqtyarry == null || avlqtyarry.length == 0) {
				resMsg = "Invalid Variant Available quantities ";
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}

			if (GenUtil.isNullOrEmpty(cau)) {
				resMsg = "Invalid Login Id:";
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}

			if (GenUtil.isNullOrEmpty(clientIdStr)) {
				resMsg = "Invalid Client Id:" + clientIdStr;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}

			if (GenUtil.isNullOrEmpty(sellerprodid)) {
				resMsg = "Invalid Seller Product Id:" + sellerprodid;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(sellerIdstr)) {
				resMsg = "Invalid Seller Id:" + sellerId;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}
			try {
				sellerId = Integer.parseInt(sellerIdstr);
			} catch (Exception ex) {
				resMsg = "Invalid Seller Id:" + sellerId;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}
			dto.setClId(clientIdStr);
			dto.setSlerId(sellerId);
			dto.setSlrpitmsId(sellerprodid);
			dto.setLupdBy(cau);
			dto = InventoryService.createProductVariantInventory(dto, slrpivarcomIdarry, setuniqIdarry, pregprcarry,
					pselprcarry, avlqtyarry);
			setClientMessage(dto, null, " Product Variant Inventory created Successfully");
			generateResponse(request, response, dto);
			return;

		} catch (Exception e) {
			resMsg = "Error Occured while creating  Product Variant Inventory :" + e.getLocalizedMessage();
			if (e.getLocalizedMessage().contains("UK_ids_vopt_name")) {
				resMsg = "Error Occured while creating  Product Variant Inventory :";
			}
			setClientMessage(dto, resMsg, null);
			generateResponse(request, response, dto);
			return;
		} 
	}

	/**
	 * Generate response.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}

}
