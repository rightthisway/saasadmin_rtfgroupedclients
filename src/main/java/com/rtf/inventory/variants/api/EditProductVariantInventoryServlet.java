/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.inventory.variants.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.inventory.variants.dto.ProductInventoryDTO;
import com.rtf.inventory.variants.dvo.ProductInventoryDVO;
import com.rtf.inventory.variants.service.InventoryService;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.exception.RTFServiceAccessException;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class EditProductVariantInventoryServlet.
 */
@WebServlet("/editvariantinventory.json")
public class EditProductVariantInventoryServlet extends RtfSaasBaseServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Process request.
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		ProductInventoryDTO dto = new ProductInventoryDTO();
		dto.setSts(0);
		String resMsg = "";

		String clientIdStr = request.getParameter("clId");
		String sellerprodid = request.getParameter("slrpitmsId");
		String sellerIdstr = request.getParameter("slerId");
		String cau = request.getParameter("cau");

		String[] invIdIdarry = request.getParameterValues("invId");
		String[] pregprcarry = request.getParameterValues("pregprc");
		String[] pselprcarry = request.getParameterValues("pselprc");
		String[] reqltypntsarry = request.getParameterValues("reqltypnts");
		String[] pprcaftltypntsarry = request.getParameterValues("pprcaftltypnts");
		String[] avlqtyarry = request.getParameterValues("avlqty");
		String[] soldqtyarry = request.getParameterValues("soldqty");

		Integer sellerId = null;

		try {

			if (invIdIdarry == null || invIdIdarry.length == 0) {
				resMsg = "Invalid Inventory Ids";
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}
			if (pregprcarry == null || pregprcarry.length == 0) {
				resMsg = "Invalid Variant Regular Selling Price";
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}
			if (pselprcarry == null || pselprcarry.length == 0) {
				resMsg = "Invalid Variant Discounted Price ";
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}
			if (avlqtyarry == null || avlqtyarry.length == 0) {
				resMsg = "Invalid Variant Available quantities ";
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}

			if (GenUtil.isNullOrEmpty(cau)) {
				resMsg = "Invalid Login Id:";
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}

			if (GenUtil.isNullOrEmpty(clientIdStr)) {
				resMsg = "Invalid Client Id:" + clientIdStr;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}

			if (GenUtil.isNullOrEmpty(sellerprodid)) {
				resMsg = "Invalid Seller Product Id:" + sellerprodid;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(sellerIdstr)) {
				resMsg = "Invalid Seller Id:" + sellerId;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}
			try {
				sellerId = Integer.parseInt(sellerIdstr);
			} catch (Exception ex) {
				resMsg = "Invalid Seller Id:" + sellerId;
				setClientMessage(dto, resMsg, null);
				generateResponse(request, response, dto);
				return;
			}
			dto.setClId(clientIdStr);
			dto.setSlerId(sellerId);
			dto.setSlrpitmsId(sellerprodid);
			dto.setLupdBy(cau);
			dto = prepareInventoryDTO(dto, invIdIdarry, pregprcarry, pselprcarry, reqltypntsarry, pprcaftltypntsarry,
					avlqtyarry, soldqtyarry);
			dto = InventoryService.updateProductInventoryForProdId(dto);
			if (dto.getSts() == 0) {
				setClientMessage(dto, null, " Failed to Edit Product Variant Inventory [ " + dto.getMsg() + "]");
				generateResponse(request, response, dto);
				return;
			}
			setClientMessage(dto, null, " Product Variant Inventory Edited Successfully");
			generateResponse(request, response, dto);
			return;

		} catch (Exception e) {
			resMsg = "Error Occured while creating  Product Variant Inventory :" + e.getLocalizedMessage();
			if (e.getLocalizedMessage().contains("UK_ids_vopt_name")) {
				resMsg = "Error Occured while creating  Product Variant Inventory :";
			}
			setClientMessage(dto, resMsg, null);
			generateResponse(request, response, dto);
			return;

		} finally {

		}

	}

	/**
	 * Prepare inventory DTO.
	 *
	 * @param dto                the dto
	 * @param invIdIdarry        the inv id idarry
	 * @param pregprcarry        the pregprcarry
	 * @param pselprcarry        the pselprcarry
	 * @param reqltypntsarry     the reqltypntsarry
	 * @param pprcaftltypntsarry the pprcaftltypntsarry
	 * @param avlqtyarry         the avlqtyarry
	 * @param soldqtyarry        the soldqtyarry
	 * @return the product inventory DTO
	 * @throws Exception the exception
	 */
	private ProductInventoryDTO prepareInventoryDTO(ProductInventoryDTO dto, String[] invIdIdarry, String[] pregprcarry,
			String[] pselprcarry, String[] reqltypntsarry, String[] pprcaftltypntsarry, String[] avlqtyarry,
			String[] soldqtyarry) throws Exception {
		int invsize = invIdIdarry.length;
		Integer invId, avaqty, soldqty, reqlyltypts = null;
		Double regprc, selprc, prcaftlyltypts = null;
		List<ProductInventoryDVO> lst = new ArrayList<ProductInventoryDVO>();
		for (int i = 0; i < invsize; i++) {
			try {
				String invIdstr = invIdIdarry[i];
				invId = Integer.parseInt(invIdstr);
			} catch (Exception ex) {
				throw new RTFServiceAccessException(" Invalid Value for Inventory Id ");
			}
			try {
				String regprcstr = pregprcarry[i];
				regprc = Double.parseDouble(regprcstr);
			} catch (Exception ex) {
				throw new RTFServiceAccessException(" Invalid Regular  Price ");
			}
			try {
				String sellprcstr = pselprcarry[i];
				selprc = Double.parseDouble(sellprcstr);
			} catch (Exception ex) {
				throw new RTFServiceAccessException(" Invalid Selling  Price ");
			}
			try {
				String avaqtystr = avlqtyarry[i];
				avaqty = Integer.parseInt(avaqtystr);
			} catch (Exception ex) {
				throw new RTFServiceAccessException(" Invalid Available qty ");
			}
			try {
				String soldqtystr = soldqtyarry[i];
				soldqty = Integer.parseInt(soldqtystr);
			} catch (Exception ex) {
				throw new RTFServiceAccessException(" Invalid Sold qty ");
			}

			try {
				String reqLyltptstr = reqltypntsarry[i];
				reqlyltypts = Integer.parseInt(reqLyltptstr);
			} catch (Exception ex) {
				ex.printStackTrace();
			}

			try {
				String pprcaftltypntstr = pprcaftltypntsarry[i];
				prcaftlyltypts = Double.parseDouble(pprcaftltypntstr);
			} catch (Exception ex) {
				ex.printStackTrace();
			}

			ProductInventoryDVO dvo = new ProductInventoryDVO();
			dvo.setClintId(dto.getClId());
			dvo.setSlrpitmsId(dto.getSlrpitmsId());
			dvo.setSlerId(dto.getSlerId());
			dvo.setSlrinvId(invId);
			dvo.setPregprc(regprc);
			dvo.setPselprc(selprc);
			dvo.setReqltypnts(reqlyltypts);
			dvo.setPprcaftltypnts(prcaftlyltypts);
			dvo.setAvlqty(avaqty);
			dvo.setSldqty(soldqty);
			dvo.setLupdBy(dto.getLupdBy());
			lst.add(dvo);
		}

		dto.setLst(lst);
		return dto;
	}

	/**
	 * Check all array qare equal.
	 *
	 * @param arrays the arrays
	 */
	public static void checkAllArrayQareEqual(Object... arrays) {
		if (arrays.length < 1) {
			return;
		}
		int expectedLength = Array.getLength(arrays[0]);
		for (int i = 1; i < arrays.length; i++) {
			int length = Array.getLength(arrays[i]);
			if (length != expectedLength) {
				throw new IllegalArgumentException(
						"Array " + i + " doesn't have expected length " + expectedLength + ": " + length);
			}
		}
	}

	/**
	 * Generate response.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}

}
