/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.inventory.variants.service;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.microsoft.sqlserver.jdbc.SQLServerException;
import com.rtf.inventory.variants.dao.ProductVariantNameDAO;
import com.rtf.inventory.variants.dto.VariantOptionValueDTO;
import com.rtf.inventory.variants.dvo.ProductItemVariantNameDVO;
import com.rtf.saas.sql.db.DatabaseConnections;

/**
 * The Class VariantOptionService.
 */
public class VariantOptionService {

	/**
	 * Save vaiant option values.
	 *
	 * @param dto the dto
	 * @return the variant option value DTO
	 * @throws Exception the exception
	 */
	public static VariantOptionValueDTO saveVaiantOptionValues(VariantOptionValueDTO dto) throws Exception {
		Connection conn = null;
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ProductItemVariantNameDVO dvo = dto.getDvo();
			Map<String, Map<String, String>> varOptValFileNameMap = dvo.getVariantNameOptValFileNameMap();
			for (Map.Entry<String, Map<String, String>> entry : varOptValFileNameMap.entrySet()) {
				String variantName = entry.getKey();
				Map<String, String> optValMap = varOptValFileNameMap.get(variantName);

				dvo.setVoptname(entry.getKey());
				Integer varNameId = ProductVariantNameDAO.createPrductVariantName(dvo, conn);

				if (varNameId == 0)
					throw new Exception(" Error Creating Variant Name ");
				dvo.setSlrpivarId(varNameId);
				for (Map.Entry<String, String> optvaluemapEntry : optValMap.entrySet()) {
					String optValue = optvaluemapEntry.getKey();
					String optvalFileUrl = optvaluemapEntry.getValue();
					dvo.setVoptvalue(optValue);
					dvo.setVoptvalueIimgurl(optvalFileUrl);
					Integer varOptionValId = ProductVariantNameDAO.createPrductVariantNameOptionValue(dvo, conn);
					if (varOptionValId == 0)
						throw new Exception(" Error Creating Variant Option Value ");
				}
			}
			dto.setSts(1);
			return dto;
		} catch (Exception ex) {
			dto.setSts(0);
			dto.setMsg(ex.getLocalizedMessage());
		} finally {
			try {
				conn.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return dto;
	}

	/**
	 * Save vaiant product matrix values.
	 *
	 * @param dto the dto
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer saveVaiantProductMatrixValues(VariantOptionValueDTO dto) throws Exception {

		Connection conn = null;
		CallableStatement stmt = null;
		ResultSet rs = null;
		Integer varIdListSize = 0;
		List<Integer> vaiIdList = new ArrayList<Integer>();
		try {

			ProductItemVariantNameDVO dvo = dto.getDvo();
			String query = "{call spGenSlerProdVariMatrix(?,?,?,?)}";
			conn = DatabaseConnections.getSaasAdminConnection();
			stmt = conn.prepareCall(query);
			stmt.setString(1, dvo.getClintId());
			stmt.setInt(2, Integer.parseInt(dvo.getSlerId()));
			stmt.setString(3, dvo.getSlrpitmsId());
			stmt.setString(4, dvo.getLupdBy());
			rs = stmt.executeQuery();
			while (rs.next()) {
				vaiIdList.add(rs.getInt("slrpivarcom_id"));
			}
			varIdListSize = vaiIdList.size();
			return varIdListSize;
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				rs.close();
				stmt.close();
				conn.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return varIdListSize;
	}

	/**
	 * Save vaiant product matrix values 1.
	 *
	 * @param dto the dto
	 * @return the variant option value DTO
	 * @throws Exception the exception
	 */
	public static VariantOptionValueDTO saveVaiantProductMatrixValues1(VariantOptionValueDTO dto) throws Exception {

		Connection conn = null;
		PreparedStatement stmt = null;

		ResultSet rs = null;
		ProductItemVariantNameDVO dvo = dto.getDvo();
		try {
			String SPsql = "exec spGenSlerProdVariMatrix ?,?,?,?"; // for stored proc taking 2 parameters
			conn = DatabaseConnections.getSaasAdminConnection();
			stmt = conn.prepareStatement(SPsql);
			stmt.setString(1, dvo.getClintId());
			stmt.setInt(2, Integer.parseInt(dvo.getSlerId()));
			stmt.setString(3, dvo.getSlrpitmsId());
			stmt.setString(4, dvo.getLupdBy());
			rs = stmt.executeQuery();
			Integer varId = -1;
			while (rs.next()) {
				varId = rs.getInt("slrpivarcom_id");
				System.out.println(varId);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			dto.setSts(0);
			dto.setMsg(ex.getLocalizedMessage());
		} finally {
			try {
				rs.close();
				stmt.close();
				conn.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return dto;
	}

	/**
	 * Save vaiant product matrix values 2.
	 *
	 * @param dto  the dto
	 * @param conn the conn
	 * @throws Exception the exception
	 */
	public static void saveVaiantProductMatrixValues2(VariantOptionValueDTO dto, Connection conn) throws Exception {

		String query = "{call spGenSlerProdVariMatrix(?,?,?,?)}";
		ProductItemVariantNameDVO dvo = dto.getDvo();
		try (CallableStatement cs = conn.prepareCall(query)) {
			cs.setString(1, dvo.getClintId());
			cs.setInt(2, Integer.parseInt(dvo.getSlerId()));
			cs.setString(3, dvo.getSlrpitmsId());
			cs.setString(4, dvo.getLupdBy());
			boolean resultSetAvailable = false;
			int numberOfResultsProcessed = 0;
			try {
				resultSetAvailable = cs.execute();
			} catch (SQLServerException sse) {
				numberOfResultsProcessed++;
			}
			int updateCount = -2;
			while (true) {
				boolean exceptionOccurred = true;
				do {
					try {
						if (numberOfResultsProcessed > 0) {
							resultSetAvailable = cs.getMoreResults();
						}
						exceptionOccurred = false;
						updateCount = cs.getUpdateCount();
					} catch (SQLServerException sse) {
					}
					numberOfResultsProcessed++;
				} while (exceptionOccurred);

				if ((!resultSetAvailable) && (updateCount == -1)) {
					break; // we're done
				}

				if (resultSetAvailable) {
					try (ResultSet rs = cs.getResultSet()) {
						try {
							while (rs.next()) {
								System.out.println(rs.getString(1));
							}
						} catch (SQLServerException sse) {
						}
					}
				} else {
					System.out.printf("Current result is an update count: %d %s affected%n", updateCount,
							updateCount == 1 ? "row was" : "rows were");
				}
			}
		}

	}

}
