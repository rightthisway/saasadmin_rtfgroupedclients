/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.inventory.variants.service;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.rtf.inventory.product.dao.ProductItemMasterDAO;
import com.rtf.inventory.product.dvo.ProductItemMasterDVO;
import com.rtf.inventory.variants.dao.ProductVariantNameDAO;
import com.rtf.inventory.variants.dao.VariantInventoryDAO;
import com.rtf.inventory.variants.dao.VariantMatrixDAO;
import com.rtf.inventory.variants.dto.ProductInventoryDTO;
import com.rtf.inventory.variants.dvo.ProductInventoryDVO;
import com.rtf.inventory.variants.dvo.VariantNameDVO;
import com.rtf.inventory.variants.dvo.VariantNameValueMapDVO;
import com.rtf.inventory.variants.dvo.VariantOptValueDVO;
import com.rtf.saas.exception.RTFServiceAccessException;
import com.rtf.saas.sql.db.DatabaseConnections;
import com.rtf.saas.util.GenUtil;

/**
 * The Class InventoryService.
 */
public class InventoryService {
	
	/**
	 * Process inventory.
	 *
	 * @param dto the dto
	 * @return the product inventory DTO
	 * @throws Exception the exception
	 */
	public static ProductInventoryDTO processInventory(ProductInventoryDTO dto) throws Exception {	
		Connection conn=null;
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			dto = saveInventoryRecords(dto,  conn);
		}catch(Exception ex) {
			dto.setMsg(ex.getLocalizedMessage());
			dto.setSts(0);			
		}finally {
			conn.close();
		}
		return dto;
		
	}
	
	/**
	 * Save inventory records.
	 *
	 * @param dto the dto
	 * @param conn the conn
	 * @return the product inventory DTO
	 * @throws Exception the exception
	 */
	public static ProductInventoryDTO saveInventoryRecords(ProductInventoryDTO dto, Connection conn) throws Exception {
		try {
			List<ProductInventoryDVO> lst = dto.getLst();
			if (lst == null || lst.size() == 0)
				return dto;
			for (ProductInventoryDVO dvo : lst) {
				dvo.setClintId(dto.getClId());
				dvo.setSlrpitmsId(dto.getSlrpitmsId());
				dvo.setSlerId(dto.getSlerId());
				dvo.setSlrpivarcomId(dto.getSlrpivarcomId());
				dvo.setSetuniqId(dto.getSetuniqId());
				dvo = VariantInventoryDAO.saveInventoryRecord(dvo, conn);
			}
			dto.setSts(1);
		} catch (Exception ex) {
			ex.printStackTrace();
			dto.setSts(0);
			dto.setMsg(ex.getLocalizedMessage());
		}
		return dto;
	}
	
	
	/**
	 * Fetch product item master for inventory matrix.
	 *
	 * @param dto the dto
	 * @return the product inventory DTO
	 * @throws Exception the exception
	 */
	public static ProductInventoryDTO fetchProductItemMasterForInventoryMatrix(ProductInventoryDTO dto) throws Exception {
		Connection conn = null;
		try {
			ProductItemMasterDVO prdmasterdvo = new ProductItemMasterDVO();
			prdmasterdvo.setClintId(dto.getClId());
			prdmasterdvo.setSlerId(dto.getSlerId());
			prdmasterdvo.setSlrpitmsId(dto.getSlrpitmsId());
			conn = DatabaseConnections.getSaasAdminConnection();
			prdmasterdvo= ProductItemMasterDAO.fetchProductItemForPK(prdmasterdvo , conn );			
			dto.setPimasterdvo(prdmasterdvo);			
			dto= fetchMatrixDVOList(dto,  conn );			
			dto.setSts(1);
		} catch (Exception ex) {
			ex.printStackTrace();
			dto.setSts(0);
			dto.setMsg(ex.getLocalizedMessage());
		}
		finally {
			DatabaseConnections.closeConnection(conn);
		}
		return dto;	
		
	}
	
	
	
	
	
	/**
	 * Fetch matrix DVO list.
	 *
	 * @param dto the dto
	 * @param conn the conn
	 * @return the product inventory DTO
	 * @throws Exception the exception
	 */
	public static  ProductInventoryDTO fetchMatrixDVOList(ProductInventoryDTO dto, Connection conn ) throws Exception {
		try {
			if(conn == null)  conn = DatabaseConnections.getSaasAdminConnection();
			 VariantMatrixDAO.fetchMatrixDVOList( dto,  conn ) ;
		}catch(Exception ex) {
			ex.printStackTrace();
			dto.setSts(0);
			dto.setMsg(ex.getLocalizedMessage());
		}
		return dto;
	}
	

	/**
	 * Creates the product variant inventory.
	 *
	 * @param dto the dto
	 * @param slrpivarcomIdarry the slrpivarcom idarry
	 * @param setuniqIdarry the setuniq idarry
	 * @param pregprcarry the pregprcarry
	 * @param pselprcarry the pselprcarry
	 * @param avlqtyarry the avlqtyarry
	 * @return the product inventory DTO
	 * @throws Exception the exception
	 */
	public static ProductInventoryDTO createProductVariantInventory(ProductInventoryDTO dto, String[] slrpivarcomIdarry,
			String[] setuniqIdarry, String[] pregprcarry, String[] pselprcarry, String[] avlqtyarry) throws Exception{
			
			ProductItemMasterDVO prdmasterdvo = new ProductItemMasterDVO();
			prdmasterdvo.setClintId(dto.getClId());
			prdmasterdvo.setSlerId(dto.getSlerId());
			prdmasterdvo.setSlrpitmsId(dto.getSlrpitmsId());
			Connection conn =  DatabaseConnections.getSaasAdminConnection();
			prdmasterdvo= ProductItemMasterDAO.fetchProductItemForPK(prdmasterdvo , conn );
		
			ProductInventoryDVO dvo = new ProductInventoryDVO();
			dvo.setClintId(dto.getClId());
			dvo.setSlerId(dto.getSlerId());
			dvo.setSlrpitmsId(dto.getSlrpitmsId());
			dvo.setLupdBy(dto.getLupdBy());			
			dto = VariantMatrixDAO.fetchMatrixDVOList(dto, conn );
			
			List<ProductInventoryDVO> invlst = dto.getLst();
			if(invlst == null || invlst.size() == 0 ) {
				dto.setSts(0);
				dto.setMsg("No Records Found for Given Inventory Criterias ");
				return dto;			
			}
			
		 Map<Integer, ProductInventoryDVO> pinvmap= preparedvoProcessMapwithUK(invlst);
		 List<ProductInventoryDVO> savelst = new ArrayList<ProductInventoryDVO>();
		 int arraySize = slrpivarcomIdarry.length;
		 for(int i = 0 ; i < arraySize; i++) {			 
			 String slrpivarcomIdstr = slrpivarcomIdarry[i];
			 String pregprcstr = pregprcarry[i];
			 String pselprcstr = pselprcarry[i];
			 String avlqtystr = avlqtyarry[i];
			 
			 Integer pivarcomId = Integer.parseInt(slrpivarcomIdstr);		
			 Double pregprc = Double.parseDouble(pregprcstr);
			 Double pselprc = Double.parseDouble(pselprcstr);
			 Integer avlqty = Integer.parseInt(avlqtystr);
			
			 if(pinvmap.containsKey(pivarcomId)) {
				 ProductInventoryDVO dbdvo = pinvmap.get(pivarcomId);
				 dbdvo.setLupdBy(dto.getLupdBy());
				 dbdvo.setPbrand(prdmasterdvo.getPbrand());
				 dbdvo.setPname(prdmasterdvo.getPname());
				 dbdvo.setPregprc(pregprc);
				 dbdvo.setPselprc(pselprc);
				 dbdvo.setAvlqty(avlqty);
				 dbdvo.setPrdstatus("ACTIVE");
				 if(GenUtil.isEmptyString(dbdvo.getPimg())){
					 dbdvo.setPimg(prdmasterdvo.getPimg()); 
				 }
				 savelst.add(dbdvo);
			 }
		 }
		 dto.setPimasterdvo(prdmasterdvo);
		 dto.setLst(savelst);		 
		 dto =  addNewInventory(dto,conn);		
		 return dto;
	}

	/**
	 * Adds the new inventory.
	 *
	 * @param dto the dto
	 * @param conn the conn
	 * @return the product inventory DTO
	 * @throws Exception the exception
	 */
	private static ProductInventoryDTO addNewInventory(ProductInventoryDTO dto,Connection conn) throws Exception {
		conn.setAutoCommit(false);
		List<ProductInventoryDVO> savelst = dto.getLst();
		try {
			for(ProductInventoryDVO dvo: savelst){	
				VariantInventoryDAO.saveInventoryRecord(dvo, conn);
			}
		conn.commit();
		conn.setAutoCommit(true);
		dto.setSts(1);
		}catch(Exception ex) {
			ex.printStackTrace();
			conn.rollback();
			conn.setAutoCommit(true);
			dto.setSts(0);
			throw ex;
		}
		finally {			
			conn.setAutoCommit(true);
		}		
		return dto;
	}

	/**
	 * Preparedvo process mapwith UK.
	 *
	 * @param invlst the invlst
	 * @return the map
	 */
	private static Map<Integer, ProductInventoryDVO> preparedvoProcessMapwithUK(List<ProductInventoryDVO> invlst) {
	
		Map<Integer, ProductInventoryDVO> map = new HashMap<Integer, ProductInventoryDVO>();		
		for(ProductInventoryDVO dvo : invlst ) {			
			map.put(dvo.getSlrpivarcomId(), dvo) ;
		}		
		return map;	
	}
	
	
	/**
	 * Fetch product inventory for prod id.
	 *
	 * @param dto the dto
	 * @return the product inventory DTO
	 * @throws Exception the exception
	 */
	public static ProductInventoryDTO fetchProductInventoryForProdId(ProductInventoryDTO dto) throws Exception {		
		Connection conn = null;
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			VariantInventoryDAO.fetchInventoryForProdId(dto,conn );
			
			ProductItemMasterDVO prdmasterdvo = new ProductItemMasterDVO();
			prdmasterdvo.setClintId(dto.getClId());
			prdmasterdvo.setSlerId(dto.getSlerId());
			prdmasterdvo.setSlrpitmsId(dto.getSlrpitmsId());		
			prdmasterdvo= ProductItemMasterDAO.fetchProductItemForPK(prdmasterdvo , conn );
			dto.setPimasterdvo(prdmasterdvo);			
			dto.setSts(1);
		}catch(Exception ex) {
			dto.setSts(0);
			dto.setMsg(ex.getLocalizedMessage());
		}
		finally {
			DatabaseConnections.closeConnection(conn);
		}
		return dto;		
	}
	
	/**
	 * Update product inventory for prod id.
	 *
	 * @param dto the dto
	 * @return the product inventory DTO
	 * @throws Exception the exception
	 */
	public static ProductInventoryDTO updateProductInventoryForProdId(ProductInventoryDTO dto) throws Exception {		
		Connection conn = null;
		try {
			
			List<ProductInventoryDVO> dvolst = dto.getLst();
			conn = DatabaseConnections.getSaasAdminConnection();			
			
			for(ProductInventoryDVO dvo : dvolst) {
				try {
				VariantInventoryDAO.editInventoryRecords(dvo,conn );
				}catch(Exception ex) {
					ex.printStackTrace();
					throw new RTFServiceAccessException(" Error Updating Inventory  Record for Inventory Id : " + dvo.getSlrinvId() + "[  " + ex.getLocalizedMessage() + " ]",ex);
				}
			}
			
			ProductItemMasterDVO prdmasterdvo = new ProductItemMasterDVO();
			prdmasterdvo.setClintId(dto.getClId());
			prdmasterdvo.setSlerId(dto.getSlerId());
			prdmasterdvo.setSlrpitmsId(dto.getSlrpitmsId());		
			prdmasterdvo= ProductItemMasterDAO.fetchProductItemForPK(prdmasterdvo , conn );
			dto.setPimasterdvo(prdmasterdvo);			
			dto.setSts(1);
		}catch(Exception ex) {
			dto.setSts(0);
			dto.setMsg(ex.getLocalizedMessage());
		}
		finally {
			DatabaseConnections.closeConnection(conn);
		}
		return dto;		
	}
	
	/**
	 * Fetch prod variants.
	 *
	 * @param dto the dto
	 * @return the product inventory DTO
	 * @throws Exception the exception
	 */
	public static ProductInventoryDTO fetchProdVariants(ProductInventoryDTO dto) throws Exception{
		Connection conn = null;
		List<VariantNameValueMapDVO> varNameOptvalMaplst = new ArrayList<VariantNameValueMapDVO>();
		try {	
		
		 conn = DatabaseConnections.getSaasAdminConnection();
		 List<VariantNameDVO>  varNameDVOlst =  ProductVariantNameDAO.fetchVariantNamesForProdId( dto,  conn );	
		 if(varNameDVOlst == null || varNameDVOlst.size() ==0) {
			 dto.setOptNamevalMap(varNameOptvalMaplst);
			 dto.setSts(1); // No Record . new  Variant Option Value Mapping ...
			 return dto;
		 }
		String clintId= dto.getClId(); 
	   for(VariantNameDVO vndvo : varNameDVOlst) {
		   
		   VariantNameValueMapDVO mapdvo = new VariantNameValueMapDVO();
		   List<String> optvalst = new ArrayList<String>();
		   List<String> imgUrlst = new ArrayList<String>();
		   mapdvo.setVoptname(vndvo.getVariantName());
		   Integer varNameId = vndvo.getSlrpivarId();
		   List<VariantOptValueDVO> optvallst =  ProductVariantNameDAO.fetchOptionValuesForVariant(clintId, varNameId,  conn );
		   for(VariantOptValueDVO optvaldvo:optvallst ) {
			   String optval = optvaldvo.getVoptvalue()==null?"":optvaldvo.getVoptvalue();
			   String imgurl = optvaldvo.getImgurl()==null?"":optvaldvo.getImgurl();
			   optvalst.add(optval);
			   imgUrlst.add(imgurl);			   
		   }
		   mapdvo.setVoptvalue(optvalst);
		   mapdvo.setVoptvalueIimgurl(imgUrlst);
		   varNameOptvalMaplst.add(mapdvo);
	   }
	    dto.setOptNamevalMap(varNameOptvalMaplst);		 
	    dto.setSts(1);
	    return dto;
		}catch(Exception ex) {
			ex.printStackTrace();
			dto.setSts(0);			
		}
		finally {
			DatabaseConnections.closeConnection(conn);
		}
		return null;
	}
}
