/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.inventory.variants.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.rtf.inventory.variants.dto.ProductInventoryDTO;
import com.rtf.inventory.variants.dvo.ProductInventoryDVO;
import com.rtf.saas.exception.RTFDataAccessException;

/**
 * The Class VariantInventoryDAO.
 */
public class VariantInventoryDAO {

	/**
	 * Save inventory record.
	 *
	 * @param dvo  the dvo
	 * @param conn the conn
	 * @return the product inventory DVO
	 * @throws Exception the exception
	 */
	public static ProductInventoryDVO saveInventoryRecord(ProductInventoryDVO dvo, Connection conn) throws Exception {

		PreparedStatement ps = null;
		StringBuffer sql = new StringBuffer();
		sql.append("insert into  rtf_seller_product_items_variants_inventory (")
		.append(" clintid, slrpitms_id, sler_id , slrpivarcom_id, set_uniq_id ")
		.append(" , pbrand ,vopt_name_val_com, pname, preg_prc, psel_prc   ")
		.append(" ,req_lty_pnts, pprc_aft_lty_pnts ,avl_qty,sld_qty,prd_status, pimg ")
		.append(" ,cat_id,subcat_id,exp_date ")
		.append(" ,vo_name_one,vo_name_two,vo_name_three,vo_name_four ")
		.append(" ,vo_value_one,vo_value_two,vo_value_three,vo_value_four ")
		.append(" ,slrpivar_id_one,slrpivar_id_two,slrpivar_id_three,slrpivar_id_four ")
		.append(" ,varoval_id_one,varoval_id_two,varoval_id_three,varoval_id_four ")
		.append(" ,lupd_by,lupd_date  ) ")
		.append(" values( ?, ?, ?, ?, ? , ?, ?, ?, ?, ? ,  ?, ?, ?, ?, ?, ? ,?, ?, ?,")
		.append("  ?, ?, ?, ? , ?, ?, ?, ? ,?, ?, ?, ? , ?, ?, ?, ? , ?, getDate() ) ");
		try {
			ps = conn.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);

			ps.setString(1, dvo.getClintId());
			ps.setString(2, dvo.getSlrpitmsId());
			ps.setInt(3, dvo.getSlerId());
			ps.setInt(4, dvo.getSlrpivarcomId());
			ps.setInt(5, dvo.getSetuniqId());

			ps.setString(6, dvo.getPbrand());
			ps.setString(7, dvo.getVoptnamevalcom());
			ps.setString(8, dvo.getPname());
			ps.setDouble(9, dvo.getPregprc());
			ps.setDouble(10, dvo.getPselprc());

			if (dvo.getReqltypnts() == null) {
				ps.setNull(11, Types.INTEGER);
			} else {
				ps.setInt(11, dvo.getReqltypnts());
			}

			if (dvo.getPprcaftltypnts() == null) {
				ps.setNull(12, Types.DOUBLE);
			} else {
				ps.setDouble(12, dvo.getPprcaftltypnts());
			}

			if (dvo.getAvlqty() == null) {
				ps.setNull(13, Types.INTEGER);
			} else {
				ps.setInt(13, dvo.getAvlqty());
			}

			if (dvo.getSldqty() == null) {
				ps.setNull(14, Types.INTEGER);
			} else {
				ps.setInt(14, dvo.getSldqty());
			}

			ps.setString(15, dvo.getPrdstatus());
			ps.setString(16, dvo.getPimg());

			if (dvo.getCatId() == null) {
				ps.setNull(17, Types.INTEGER);
			} else {
				ps.setInt(17, dvo.getCatId());
			}
			if (dvo.getSubcatId() == null) {
				ps.setNull(18, Types.INTEGER);
			} else {
				ps.setInt(18, dvo.getSubcatId());
			}
			if (dvo.getExpDate() == null) {
				ps.setNull(19, java.sql.Types.TIMESTAMP);
			} else {
				ps.setTimestamp(19, new java.sql.Timestamp(dvo.getExpDate()));
			}

			ps.setString(20, dvo.getVonameone());
			ps.setString(21, dvo.getVonametwo());
			ps.setString(22, dvo.getVonamethree());
			ps.setString(23, dvo.getVonamefour());

			ps.setString(24, dvo.getVovalueone());
			ps.setString(25, dvo.getVovaluetwo());
			ps.setString(26, dvo.getVovaluethree());
			ps.setString(27, dvo.getVovaluefour());

			if (dvo.getSlrpivarIdone() == null) {
				ps.setNull(28, Types.INTEGER);
			} else {
				ps.setInt(28, dvo.getSlrpivarIdone());
			}
			if (dvo.getSlrpivarIdtwo() == null) {
				ps.setNull(29, Types.INTEGER);
			} else {
				ps.setInt(29, dvo.getSlrpivarIdtwo());
			}
			if (dvo.getSlrpivarIdthree() == null) {
				ps.setNull(30, Types.INTEGER);
			} else {
				ps.setInt(30, dvo.getSlrpivarIdthree());
			}
			if (dvo.getSlrpivarIdfour() == null) {
				ps.setNull(31, Types.INTEGER);
			} else {
				ps.setInt(31, dvo.getSlrpivarIdfour());
			}

			if (dvo.getVarovalIdone() == null) {
				ps.setNull(32, Types.INTEGER);
			} else {
				ps.setInt(32, dvo.getVarovalIdone());
			}
			if (dvo.getVarovalIdtwo() == null) {
				ps.setNull(33, Types.INTEGER);
			} else {
				ps.setInt(33, dvo.getVarovalIdtwo());
			}
			if (dvo.getVarovalIdthree() == null) {
				ps.setNull(34, Types.INTEGER);
			} else {
				ps.setInt(34, dvo.getVarovalIdthree());
			}
			if (dvo.getVarovalIdfour() == null) {
				ps.setNull(35, Types.INTEGER);
			} else {
				ps.setInt(35, dvo.getVarovalIdfour());
			}
			ps.setString(36, dvo.getLupdBy());

			int affectedRows = ps.executeUpdate();
			if (affectedRows == 0) {
				throw new SQLException("Creating Product Item  Inventory record failed for Client Id " + dvo.getClintId());
			}
			ResultSet generatedKeysRs;
			try {
				generatedKeysRs = ps.getGeneratedKeys();
				if (generatedKeysRs.next()) {
					Integer genId = generatedKeysRs.getInt(1);
					dvo.setSlrinvId(genId);
				} else {
					throw new RTFDataAccessException(" Error Creating Inventory ");
				}
				generatedKeysRs.close();

			} catch (Exception ex) {
				ex.printStackTrace();
				throw ex;
			} finally {
				try {
					ps.close();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
		return dvo;
	}

	/**
	 * Fetch inventory for prod id.
	 *
	 * @param dto  the dto
	 * @param conn the conn
	 * @return the product inventory DTO
	 * @throws Exception the exception
	 */
	public static ProductInventoryDTO fetchInventoryForProdId(ProductInventoryDTO dto, Connection conn)
			throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<ProductInventoryDVO> lst = new ArrayList<ProductInventoryDVO>();
		StringBuffer sql = new StringBuffer();
		sql.append("select  " + "clintid,slrinv_id,slrpivarcom_id,set_uniq_id,slrpitms_id,sler_id ")
		.append(" ,vopt_name_val_com ,pbrand , pname,prd_status,pimg,exp_date ")
		.append(" ,preg_prc, psel_prc,req_lty_pnts	,pprc_aft_lty_pnts, avl_qty, sld_qty ")
		.append(" ,vo_name_one,vo_name_two, vo_name_three, vo_name_four")
		.append(" ,vo_value_one ,vo_value_two,vo_value_three, vo_value_four")
		.append(" ,slrpivar_id_one, slrpivar_id_two, slrpivar_id_three, slrpivar_id_four")
		.append(" ,varoval_id_one, varoval_id_two, varoval_id_three, varoval_id_four")
		.append(" ,lupd_date,lupd_by from rtf_seller_product_items_variants_inventory where ")
		.append(" clintid = ? and slrpitms_id=? and sler_id=? and prd_status=? order by slrinv_id ");

		try {
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, dto.getClId());
			ps.setString(2, dto.getSlrpitmsId());
			ps.setInt(3, dto.getSlerId());
			ps.setString(4, dto.getPrdstatus());
			rs = ps.executeQuery();
			while (rs.next()) {
				ProductInventoryDVO dvo = new ProductInventoryDVO();
				Date date = null;
				dvo.setClintId(rs.getString("clintid"));
				dvo.setSlrinvId(rs.getInt("slrinv_id"));
				dvo.setSlrpivarcomId(rs.getInt("slrpivarcom_id"));
				dvo.setSetuniqId(rs.getInt("set_uniq_id"));
				dvo.setSlrpitmsId(rs.getString("slrpitms_id"));
				dvo.setSlerId(rs.getInt("sler_id"));
				dvo.setVoptnamevalcom(rs.getString("vopt_name_val_com"));
				dvo.setPbrand(rs.getString("pbrand"));
				dvo.setPname(rs.getString("pname"));
				dvo.setPrdstatus(rs.getString("prd_status"));
				dvo.setPimg(rs.getString("pimg"));
				date = rs.getTimestamp("exp_date");
				dvo.setExpDate(date != null ? date.getTime() : null);

				dvo.setPregprc(rs.getDouble("preg_prc"));
				dvo.setPselprc(rs.getDouble("psel_prc"));
				dvo.setReqltypnts(rs.getInt("req_lty_pnts"));
				dvo.setPprcaftltypnts(rs.getDouble("pprc_aft_lty_pnts"));
				dvo.setAvlqty(rs.getInt("avl_qty"));
				dvo.setSldqty(rs.getInt("sld_qty"));

				dvo.setVonameone(rs.getString("vo_name_one"));
				dvo.setVonametwo(rs.getString("vo_name_two"));
				dvo.setVonamethree(rs.getString("vo_name_three"));
				dvo.setVonamefour(rs.getString("vo_name_four"));

				dvo.setVovalueone(rs.getString("vo_value_one"));
				dvo.setVovaluetwo(rs.getString("vo_value_two"));
				dvo.setVovaluethree(rs.getString("vo_value_three"));
				dvo.setVovaluefour(rs.getString("vo_value_four"));

				dvo.setSlrpivarIdone(rs.getInt("slrpivar_id_one"));
				dvo.setSlrpivarIdtwo(rs.getInt("slrpivar_id_two"));
				dvo.setSlrpivarIdthree(rs.getInt("slrpivar_id_three"));
				dvo.setSlrpivarIdfour(rs.getInt("slrpivar_id_four"));

				dvo.setVarovalIdone(rs.getInt("varoval_id_one"));
				dvo.setVarovalIdtwo(rs.getInt("varoval_id_two"));
				dvo.setVarovalIdthree(rs.getInt("varoval_id_three"));
				dvo.setVarovalIdfour(rs.getInt("varoval_id_four"));

				dvo.setLupdBy(rs.getString("lupd_by"));
				dvo.setPimg(rs.getString("pimg"));
				lst.add(dvo);
			}
			dto.setLst(lst);
			return dto;
		} catch (SQLException se) {
			se.printStackTrace();
			throw se;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			try {
				rs.close();
				ps.close();

			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	/**
	 * Edits the inventory records.
	 *
	 * @param dvo  the dvo
	 * @param conn the conn
	 * @return the product inventory DVO
	 * @throws Exception the exception
	 */
	public static ProductInventoryDVO editInventoryRecords(ProductInventoryDVO dvo, Connection conn) throws Exception {

		PreparedStatement ps = null;
		StringBuffer sql = new StringBuffer();
		sql.append(" update rtf_seller_product_items_variants_inventory set   preg_prc=?, psel_prc =?  ")
		.append(" , req_lty_pnts=?, pprc_aft_lty_pnts=? ,avl_qty=?,sld_qty=?  ")
		.append(" , lupd_by=?,lupd_date=getDate() ")
		.append(" where  clintid = ? and slrpitms_id=? and sler_id=? and slrinv_id=? "); 
		try {
			ps = conn.prepareStatement(sql.toString());
			ps.setDouble(1, dvo.getPregprc());
			ps.setDouble(2, dvo.getPselprc());

			if (dvo.getReqltypnts() == null) {
				ps.setNull(3, Types.INTEGER);
			} else {
				ps.setInt(3, dvo.getReqltypnts());
			}

			if (dvo.getPprcaftltypnts() == null) {
				ps.setNull(4, Types.DOUBLE);
			} else {
				ps.setDouble(4, dvo.getPprcaftltypnts());
			}

			if (dvo.getAvlqty() == null) {
				ps.setNull(5, Types.INTEGER);
			} else {
				ps.setInt(5, dvo.getAvlqty());
			}

			if (dvo.getSldqty() == null) {
				ps.setNull(6, Types.INTEGER);
			} else {
				ps.setInt(6, dvo.getSldqty());
			}
			ps.setString(7, dvo.getLupdBy());

			ps.setString(8, dvo.getClintId());
			ps.setString(9, dvo.getSlrpitmsId());
			ps.setInt(10, dvo.getSlerId());
			ps.setInt(11, dvo.getSlrinvId());
			int affectedRows = ps.executeUpdate();
			if (affectedRows == 0) {
				throw new SQLException("Updating Product Item  Inventory record failed for Client Id " + dvo.getClintId());
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		} finally {
			try {
				ps.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return dvo;
	}

	/**
	 * Save inventory recordwithou auto id.
	 *
	 * @param dvo  the dvo
	 * @param conn the conn
	 * @return the product inventory DVO
	 * @throws Exception the exception
	 */
	public static ProductInventoryDVO saveInventoryRecordwithouAutoId(ProductInventoryDVO dvo, Connection conn)
			throws Exception {

		PreparedStatement ps = null;
		StringBuffer sql = new StringBuffer();
		sql.append(" SET IDENTITY_INSERT rtf_seller_product_items_variants_inventory ON; insert into  rtf_seller_product_items_variants_inventory (")
		.append(" clintid, slrpitms_id, sler_id , slrpivarcom_id, set_uniq_id ")
		.append(" , pbrand ,vopt_name_val_com, pname, preg_prc, psel_prc   ")
		.append(" ,req_lty_pnts, pprc_aft_lty_pnts ,avl_qty,sld_qty,prd_status, pimg ")
		.append(" ,cat_id,subcat_id,exp_date ")
		.append(" ,vo_name_one,vo_name_two,vo_name_three,vo_name_four ")
		.append(" ,vo_value_one,vo_value_two,vo_value_three,vo_value_four ")
		.append(" ,slrpivar_id_one,slrpivar_id_two,slrpivar_id_three,slrpivar_id_four ")
		.append(" ,varoval_id_one,varoval_id_two,varoval_id_three,varoval_id_four ")
		.append(" ,lupd_by,lupd_date,slrinv_id ) ")
		.append(" values( ?, ?, ?, ?, ? , " + "  ?, ?, ?, ?, ? ," + "  ?, ?, ?, ?, ?, ? ," + "  ?, ?, ?,")
		.append("  ?, ?, ?, ? , " + "  ?, ?, ?, ? , " + "  ?, ?, ?, ? , " + "  ?, ?, ?, ? , ")
		.append("  ?, getDate(),? ) ; SET IDENTITY_INSERT rtf_seller_product_items_variants_inventory OFF;");
		try {
			ps = conn.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);

			ps.setString(1, dvo.getClintId());
			ps.setString(2, dvo.getSlrpitmsId());
			ps.setInt(3, dvo.getSlerId());
			ps.setInt(4, dvo.getSlrpivarcomId());
			ps.setInt(5, dvo.getSetuniqId());

			ps.setString(6, dvo.getPbrand());
			ps.setString(7, dvo.getVoptnamevalcom());
			ps.setString(8, dvo.getPname());
			ps.setDouble(9, dvo.getPregprc());
			ps.setDouble(10, dvo.getPselprc());

			if (dvo.getReqltypnts() == null) {
				ps.setNull(11, Types.INTEGER);
			} else {
				ps.setInt(11, dvo.getReqltypnts());
			}

			if (dvo.getPprcaftltypnts() == null) {
				ps.setNull(12, Types.DOUBLE);
			} else {
				ps.setDouble(12, dvo.getPprcaftltypnts());
			}

			if (dvo.getAvlqty() == null) {
				ps.setNull(13, Types.INTEGER);
			} else {
				ps.setInt(13, dvo.getAvlqty());
			}

			if (dvo.getSldqty() == null) {
				ps.setNull(14, Types.INTEGER);
			} else {
				ps.setInt(14, dvo.getSldqty());
			}

			ps.setString(15, dvo.getPrdstatus());
			ps.setString(16, dvo.getPimg());

			if (dvo.getCatId() == null) {
				ps.setNull(17, Types.INTEGER);
			} else {
				ps.setInt(17, dvo.getCatId());
			}
			if (dvo.getSubcatId() == null) {
				ps.setNull(18, Types.INTEGER);
			} else {
				ps.setInt(18, dvo.getSubcatId());
			}
			if (dvo.getExpDate() == null) {
				ps.setNull(19, java.sql.Types.TIMESTAMP);
			} else {
				ps.setTimestamp(19, new java.sql.Timestamp(dvo.getExpDate()));
			}

			ps.setString(20, dvo.getVonameone());
			ps.setString(21, dvo.getVonametwo());
			ps.setString(22, dvo.getVonamethree());
			ps.setString(23, dvo.getVonamefour());

			ps.setString(24, dvo.getVovalueone());
			ps.setString(25, dvo.getVovaluetwo());
			ps.setString(26, dvo.getVovaluethree());
			ps.setString(27, dvo.getVovaluefour());

			if (dvo.getSlrpivarIdone() == null) {
				ps.setNull(28, Types.INTEGER);
			} else {
				ps.setInt(28, dvo.getSlrpivarIdone());
			}
			if (dvo.getSlrpivarIdtwo() == null) {
				ps.setNull(29, Types.INTEGER);
			} else {
				ps.setInt(29, dvo.getSlrpivarIdtwo());
			}
			if (dvo.getSlrpivarIdthree() == null) {
				ps.setNull(30, Types.INTEGER);
			} else {
				ps.setInt(30, dvo.getSlrpivarIdthree());
			}
			if (dvo.getSlrpivarIdfour() == null) {
				ps.setNull(31, Types.INTEGER);
			} else {
				ps.setInt(31, dvo.getSlrpivarIdfour());
			}

			if (dvo.getVarovalIdone() == null) {
				ps.setNull(32, Types.INTEGER);
			} else {
				ps.setInt(32, dvo.getVarovalIdone());
			}
			if (dvo.getVarovalIdtwo() == null) {
				ps.setNull(33, Types.INTEGER);
			} else {
				ps.setInt(33, dvo.getVarovalIdtwo());
			}
			if (dvo.getVarovalIdthree() == null) {
				ps.setNull(34, Types.INTEGER);
			} else {
				ps.setInt(34, dvo.getVarovalIdthree());
			}
			if (dvo.getVarovalIdfour() == null) {
				ps.setNull(35, Types.INTEGER);
			} else {
				ps.setInt(35, dvo.getVarovalIdfour());
			}
			ps.setString(36, dvo.getLupdBy());

			ps.setInt(37, dvo.getSlrinvId());

			int affectedRows = ps.executeUpdate();
			if (affectedRows == 0) {
				throw new SQLException("Creating Product Item  Inventory record failed for Client Id " + dvo.getClintId());
			}
			ResultSet generatedKeysRs;
			try {
				generatedKeysRs = ps.getGeneratedKeys();
				if (generatedKeysRs.next()) {
					Integer genId = generatedKeysRs.getInt(1);
					dvo.setSlrinvId(genId);
				} else {
					throw new RTFDataAccessException(" Error Creating Inventory ");
				}
				generatedKeysRs.close();

			} catch (Exception ex) {
				ex.printStackTrace();
				throw ex;
			} finally {
				try {
					ps.close();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
		return dvo;
	}

}
