/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.inventory.variants.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.rtf.inventory.variants.dto.ProductInventoryDTO;
import com.rtf.inventory.variants.dvo.ProductInventoryDVO;

/**
 * The Class VariantMatrixDAO.
 */
public class VariantMatrixDAO {


	/**
	 * Fetch matrix DVO list.
	 *
	 * @param dto
	 *            the dto
	 * @param conn
	 *            the conn
	 * @return the product inventory DTO
	 * @throws Exception
	 *             the exception
	 */
	public static ProductInventoryDTO fetchMatrixDVOList(ProductInventoryDTO dto, Connection conn) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;

		List<ProductInventoryDVO> lst = new ArrayList<ProductInventoryDVO>();
		StringBuffer sql = new StringBuffer();
		sql.append("select clintid,slrpivarcom_id,set_uniq_id,slrpitms_id,sler_id")
		.append(" ,vopt_name_val_id_com,vopt_name_val_com")
		.append(" ,vo_name_one,vo_name_two,vo_name_three, vo_name_four")
		.append(" ,vo_value_one ,vo_value_two,vo_value_three,vo_value_four")
		.append(" ,slrpivar_id_one,slrpivar_id_two,slrpivar_id_three,slrpivar_id_four")
		.append(" ,varoval_id_one,varoval_id_two,varoval_id_three,varoval_id_four")
		.append(" ,pimg,lupd_date,lupd_by from rtf_seller_product_items_varia_matrix where ")
		.append(" clintid = ? and slrpitms_id=? and sler_id=?");

		try {
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, dto.getClId());
			ps.setString(2, dto.getSlrpitmsId());
			ps.setInt(3, dto.getSlerId());
			rs = ps.executeQuery();
			while (rs.next()) {
				ProductInventoryDVO dvo = new ProductInventoryDVO();
				dvo.setClintId(rs.getString("clintid"));
				dvo.setSlrpivarcomId(rs.getInt("slrpivarcom_id"));
				dvo.setSetuniqId(rs.getInt("set_uniq_id"));
				dvo.setSlrpitmsId(rs.getString("slrpitms_id"));
				dvo.setSlerId(rs.getInt("sler_id"));
				dvo.setVoptnamevalIdcom(rs.getString("vopt_name_val_id_com"));
				dvo.setVoptnamevalcom(rs.getString("vopt_name_val_com"));

				dvo.setVonameone(rs.getString("vo_name_one"));
				dvo.setVonametwo(rs.getString("vo_name_two"));
				dvo.setVonamethree(rs.getString("vo_name_three"));
				dvo.setVonamefour(rs.getString("vo_name_four"));

				dvo.setVovalueone(rs.getString("vo_value_one"));
				dvo.setVovaluetwo(rs.getString("vo_value_two"));
				dvo.setVovaluethree(rs.getString("vo_value_three"));
				dvo.setVovaluefour(rs.getString("vo_value_four"));

				dvo.setSlrpivarIdone(rs.getInt("slrpivar_id_one"));
				dvo.setSlrpivarIdtwo(rs.getInt("slrpivar_id_two"));
				dvo.setSlrpivarIdthree(rs.getInt("slrpivar_id_three"));
				dvo.setSlrpivarIdfour(rs.getInt("slrpivar_id_four"));

				dvo.setVarovalIdone(rs.getInt("varoval_id_one"));
				dvo.setVarovalIdtwo(rs.getInt("varoval_id_two"));
				dvo.setVarovalIdthree(rs.getInt("varoval_id_three"));
				dvo.setVarovalIdfour(rs.getInt("varoval_id_four"));

				dvo.setLupdBy(rs.getString("lupd_by"));
				dvo.setPimg(rs.getString("pimg"));
				lst.add(dvo);
			}
			dto.setLst(lst);
			return dto;
		} catch (SQLException se) {
			se.printStackTrace();
			throw se;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			try {
				rs.close();
				ps.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

}
