/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.inventory.variants.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.rtf.inventory.variants.dto.ProductInventoryDTO;
import com.rtf.inventory.variants.dvo.ProductItemVariantNameDVO;
import com.rtf.inventory.variants.dvo.VariantNameDVO;
import com.rtf.inventory.variants.dvo.VariantOptValueDVO;

/**
 * The Class ProductVariantNameDAO.
 */
public class ProductVariantNameDAO {

	/**
	 * Creates the prduct variant name.
	 *
	 * @param dvo the dvo
	 * @param conn the conn
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer createPrductVariantName(ProductItemVariantNameDVO dvo, Connection conn) throws Exception {			
		PreparedStatement ps = null;
		Integer genId = null;
		StringBuffer sql = new StringBuffer();
		sql.append(" insert into  rtf_seller_product_items_vari_opt_name ")
		.append(" ( clintid ,slrpitms_id ,sler_id,vopt_name , lupd_by ,lupd_date ) ")
		.append(" values(?, ?, ?, ?, ?, getdate()) ");		
		try {
			ps = conn.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, dvo.getClintId());
			ps.setString(2, dvo.getSlrpitmsId());
			ps.setString(3, dvo.getSlerId());
			ps.setString(4, dvo.getVoptname());
			ps.setString(5,dvo.getLupdBy());
			int affectedRows = ps.executeUpdate();
			if (affectedRows == 0) {
				throw new SQLException(
				"Creating Product Item  Variant record failed for Client Id " + dvo.getClintId() );
			}
					ResultSet generatedKeysRs;
				try {				
						generatedKeysRs = ps.getGeneratedKeys() ;
						if (generatedKeysRs.next()) {
							genId = generatedKeysRs.getInt(1);	
						} else {
							throw new SQLException(" Error Creating Variant Name ");
						}
						generatedKeysRs.close();
				} catch (Exception ex) {				
					ex.printStackTrace();
				}
			return genId;
		} catch (SQLException se) {
			se.printStackTrace();
			throw se;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			try {
				ps.close();			
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}
	
	/**
	 * Creates the prduct variant name option value.
	 *
	 * @param dvo the dvo
	 * @param conn the conn
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer createPrductVariantNameOptionValue(ProductItemVariantNameDVO dvo, Connection conn) throws Exception {			
		PreparedStatement ps = null;
		Integer genId = null;
		StringBuffer sql = new StringBuffer();
		sql.append(" insert into  rtf_seller_product_items_vari_opt_value ")
		.append(" ( clintid ,slrpivar_id, vopt_value ,vopt_value_img, lupd_by ,lupd_date ) ")
		.append(" values(?, ?, ?, ?, ?, getdate()) ");		
		try {
			ps = conn.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, dvo.getClintId());
			ps.setInt(2, dvo.getSlrpivarId());
			ps.setString(3, dvo.getVoptvalue());
			ps.setString(4, dvo.getVoptvalueIimgurl());		
			ps.setString(5,dvo.getLupdBy());
			int affectedRows = ps.executeUpdate();
			if (affectedRows == 0) {
				throw new SQLException("Creating Product Item  Variant record failed for Client Id " + dvo.getClintId() );
			}
			
			ResultSet generatedKeysRs;
			try {				
					generatedKeysRs = ps.getGeneratedKeys() ;
					if (generatedKeysRs.next()) {
						genId = generatedKeysRs.getInt(1);					
					} else {
						throw new SQLException(" Error Creating Variant Option Value for Variant " +  dvo.getVoptname());
					}
					generatedKeysRs.close();
			} catch (Exception ex) {				
				ex.printStackTrace();
			}
		return genId;		
		} catch (SQLException se) {
			se.printStackTrace();
			throw se;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			try {
				ps.close();			
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}
	
	
	/**
	 * Creates the prduct variant namewithout auto id.
	 *
	 * @param dvo the dvo
	 * @param conn the conn
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer createPrductVariantNamewithoutAutoId(ProductItemVariantNameDVO dvo, Connection conn) throws Exception {			
		PreparedStatement ps = null;
		Integer genId = null;
		StringBuffer sql = new StringBuffer();
		sql.append("SET IDENTITY_INSERT rtf_seller_product_items_vari_opt_name ON; insert into  rtf_seller_product_items_vari_opt_name ")
		.append(" ( clintid ,slrpitms_id ,sler_id,vopt_name , lupd_by ,lupd_date,slrpivar_id ) ")
		.append(" values(?, ?, ?, ?, ?, getdate(),?) ;SET IDENTITY_INSERT rtf_seller_product_items_vari_opt_name OFF;");		
		try {
			ps = conn.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, dvo.getClintId());
			ps.setString(2, dvo.getSlrpitmsId());
			ps.setString(3, dvo.getSlerId());
			ps.setString(4, dvo.getVoptname());
			ps.setString(5,dvo.getLupdBy());
			ps.setInt(6,dvo.getSlrpivarId());
			int affectedRows = ps.executeUpdate();
			if (affectedRows == 0) {
				throw new SQLException("Creating Product Item  Variant record failed for Client Id " + dvo.getClintId() );
			}
					ResultSet generatedKeysRs;
				try {				
						generatedKeysRs = ps.getGeneratedKeys() ;
						if (generatedKeysRs.next()) {
							genId = generatedKeysRs.getInt(1);	
						} else {
							throw new SQLException(" Error Creating Variant Name ");
						}
						generatedKeysRs.close();
				} catch (Exception ex) {				
					ex.printStackTrace();
				}
			return genId;
		} catch (SQLException se) {
			se.printStackTrace();
			throw se;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			try {
				ps.close();			
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}
	
	
	/**
	 * Creates the prduct variant name option valuewithout auto id.
	 *
	 * @param dvo the dvo
	 * @param conn the conn
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer createPrductVariantNameOptionValuewithoutAutoId(ProductItemVariantNameDVO dvo, Connection conn) throws Exception {			
		PreparedStatement ps = null;
		Integer genId = null;
		StringBuffer sql = new StringBuffer();
		sql.append(" SET IDENTITY_INSERT rtf_seller_product_items_vari_opt_value ON; insert into  rtf_seller_product_items_vari_opt_value ")
		.append(" ( clintid ,slrpivar_id, vopt_value ,vopt_value_img, lupd_by ,lupd_date,varoval_id ) ")
		.append(" values(?, ?, ?, ?, ?, getdate(),?) ; SET IDENTITY_INSERT rtf_seller_product_items_vari_opt_value OFF;");		
		try {
			ps = conn.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, dvo.getClintId());
			ps.setInt(2, dvo.getSlrpivarId());
			ps.setString(3, dvo.getVoptvalue());
			ps.setString(4, dvo.getVoptvalueIimgurl());		
			ps.setString(5,dvo.getLupdBy());
			ps.setInt(6,dvo.getVarovalId());
			int affectedRows = ps.executeUpdate();
			if (affectedRows == 0) {
				throw new SQLException("Creating Product Item  Variant record failed for Client Id " + dvo.getClintId() );
			}
			
			ResultSet generatedKeysRs;
			try {				
					generatedKeysRs = ps.getGeneratedKeys() ;
					if (generatedKeysRs.next()) {
						genId = generatedKeysRs.getInt(1);					
					} else {
						throw new SQLException(" Error Creating Variant Option Value for Variant " +  dvo.getVoptname());
					}
					generatedKeysRs.close();
			} catch (Exception ex) {				
				ex.printStackTrace();
			}
		return genId;		
		} catch (SQLException se) {
			se.printStackTrace();
			throw se;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			try {
				ps.close();			
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}
	

	/**
	 * Fetch variant names for prod id.
	 *
	 * @param dto the dto
	 * @param conn the conn
	 * @return the list
	 * @throws Exception the exception
	 */
	public static List<VariantNameDVO> fetchVariantNamesForProdId(ProductInventoryDTO dto, Connection conn ) throws Exception {	
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<VariantNameDVO> lst = new ArrayList<VariantNameDVO>();
		StringBuffer sql = new StringBuffer();
		sql.append("select clintid, slrpivar_id,slrpitms_id,sler_id,vopt_name from rtf_seller_product_items_vari_opt_name where ")
		.append(" clintid = ? and slrpitms_id=? and sler_id=? ");
		try {			
			ps = conn.prepareStatement(sql.toString());			
			ps.setString(1, dto.getClId());				
			ps.setString(2, dto.getSlrpitmsId());
			ps.setInt(3,dto.getSlerId());		
			rs = ps.executeQuery();				
			while (rs.next()) {
				VariantNameDVO dvo = new VariantNameDVO();					
				 dvo.setClintId(rs.getString("clintid"));
				 dvo.setSlrpivarId(rs.getInt("slrpivar_id")); 
				 dvo.setSlrpitmId(rs.getString("slrpitms_id"));
				 dvo.setSlerId(rs.getInt("sler_id"));
				 dvo.setVariantName(rs.getString("vopt_name"));
				 lst.add(dvo);					
			}		
			return lst;
		} catch (SQLException se) {
			se.printStackTrace();
			throw se;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			try {
				rs.close();
				ps.close();		
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}	
	}
	
	
	/**
	 * Fetch option values for variant.
	 *
	 * @param clId the cl id
	 * @param varNameId the var name id
	 * @param conn the conn
	 * @return the list
	 * @throws Exception the exception
	 */
	public static List<VariantOptValueDVO> fetchOptionValuesForVariant(String clId, Integer varNameId, Connection conn ) throws Exception {	
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<VariantOptValueDVO> lst = new ArrayList<VariantOptValueDVO>();	
		
		StringBuffer sql = new StringBuffer();
		sql.append("select clintid, slrpivar_id,varoval_id, vopt_value,vopt_value_img from rtf_seller_product_items_vari_opt_value where clintid = ? and slrpivar_id=?  ");
		try {			
			ps = conn.prepareStatement(sql.toString());			
			ps.setString(1,clId);				
			ps.setInt(2,varNameId);				
			rs = ps.executeQuery();				
			while (rs.next()) {
				VariantOptValueDVO dvo = new VariantOptValueDVO();					
				 dvo.setClintId(rs.getString("clintid"));
				 dvo.setSlrpivarId(rs.getInt("slrpivar_id")); 
				 dvo.setVarovalId(rs.getInt("varoval_id"));
				 dvo.setVoptvalue(rs.getString("vopt_value"));
				 dvo.setImgurl(rs.getString("vopt_value_img"));				 
				 lst.add(dvo);					
			}		
			return lst;
		} catch (SQLException se) {
			se.printStackTrace();
			throw se;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			try {
				rs.close();
				ps.close();			
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}	
	}
}
