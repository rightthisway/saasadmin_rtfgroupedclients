/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.firestore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.rtf.saas.service.ClientService;
import com.rtf.saas.sql.dvo.ClientFirestoreConfigDVO;

/**
 * The Class FireStoreReferenceHolder.
 */
public class FireStoreReferenceHolder {

	/** The client firestore map. */
	private static Map<String, List<LiveFirestore>> clientFirestoreMap = new HashMap<String, List<LiveFirestore>>();

	/**
	 * Gets the reference.
	 *
	 * @param clientId
	 *            the client id
	 * @return the reference
	 * @throws Exception
	 *             the exception
	 */
	public static List<LiveFirestore> getReference(String clientId) throws Exception {
		List<LiveFirestore> firestores = null;
		try {
			if (clientFirestoreMap == null || !clientFirestoreMap.containsKey(clientId)) {
				firestores = new ArrayList<LiveFirestore>();
				List<ClientFirestoreConfigDVO> configs = ClientService.getClientConfig(clientId);
				if (configs == null || configs.isEmpty()) {
					return null;
				}
				for (ClientFirestoreConfigDVO config : configs) {
					LiveFirestore firestore = new LiveFirestore();
					boolean isInit = firestore.initialiseFireStore(config);
					if (isInit) {
						firestores.add(firestore);
					}
				}
				clientFirestoreMap.put(clientId, firestores);
			} else if (clientFirestoreMap.containsKey(clientId)) {
				firestores = clientFirestoreMap.get(clientId);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return firestores;
	}

}
