/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.sql.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.rtf.livt.dvo.ContestParticipantsDVO;
import com.rtf.saas.sql.db.DatabaseConnections;

/**
 * The Class LivtContestParticipantsDAO.
 */
public class LivtContestParticipantsDAO {

	/**
	 * Gets the all contest participants by co id.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 * @return the all contest participants by co id
	 * @throws Exception
	 *             the exception
	 */
	public static List<ContestParticipantsDVO> getAllContestParticipantsByCoId(String clientId, String contestId)
			throws Exception {
		List<ContestParticipantsDVO> participants = new ArrayList<ContestParticipantsDVO>();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer sql = new StringBuffer();
		sql.append("select cp.custid,cc.userid,cc.custemail from pt_livvx_contest_participants cp")
		.append(" inner join pu_client_customer cc on cc.clintid=cp.clintid and cc.custid=cp.custid")
		.append(" where cp.clintid=? and conid=?");
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			ps.setString(2, contestId);
			rs = ps.executeQuery();

			ContestParticipantsDVO participant = null;
			while (rs.next()) {
				participant = new ContestParticipantsDVO();
				participant.setCuId(rs.getString("custid"));
				participant.setuId(rs.getString("userid"));
				participant.setEmail(rs.getString("custemail"));

				participants.add(participant);

			}

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return participants;
	}

}
