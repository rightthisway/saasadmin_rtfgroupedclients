/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.sql.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.datastax.driver.core.Row;
import com.datastax.driver.core.SimpleStatement;
import com.datastax.driver.core.Statement;
import com.datastax.driver.core.exceptions.DriverException;
import com.rtf.livt.dvo.ClusterNodeConfigDVO;
import com.rtf.livt.dvo.ContestJackpotWinnerDVO;
import com.rtf.livt.dvo.LiveContestUserIdConfDVO;
import com.rtf.saas.cass.db.CassandraConnector;
import com.rtf.saas.exception.RTFDataAccessException;
import com.rtf.saas.sql.db.DatabaseConnections;
import com.rtf.saas.util.GenUtil;

/**
 * The Class ClusterNodeConfigSQLDAO.
 */
public class ClusterNodeConfigSQLDAO {

	/**
	 * Gets the all active cluster node config.
	 *
	 * @param clientId
	 *            the client id
	 * @return the all active cluster node config
	 * @throws Exception
	 *             the exception
	 */
	public static List<ClusterNodeConfigDVO> getAllActiveClusterNodeConfig(String clientId) throws Exception {
		ClusterNodeConfigDVO config = null;
		List<ClusterNodeConfigDVO> configs = new ArrayList<ClusterNodeConfigDVO>();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "Select * from pc_livvx_contest_cluster_nodes_conf WHERE clintid=? AND status=? ORDER BY id";
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, clientId);
			ps.setInt(2, 1);
			rs = ps.executeQuery();

			while (rs.next()) {
				config = new ClusterNodeConfigDVO();
				config.setClId(rs.getString("clintid"));
				config.setId(rs.getInt("id"));
				config.setDirPath(rs.getString("directory_path"));
				config.setStatus(rs.getInt("status"));
				config.setUrl(rs.getString("url"));
				config.setIsPrimary(rs.getBoolean("isprimarynode"));
				config.setNodeId(rs.getString("nodeid"));
				configs.add(config);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return configs;
	}

	/**
	 * Gets the live contest user Id config.
	 *
	 * @param clientId
	 *            the client id
	 * @param nodeId
	 *            the node id
	 * @return the live contest userId config
	 * @throws Exception
	 *             the exception
	 */
	public static LiveContestUserIdConfDVO getLiveContestUserIdConf(String clientId, String nodeId) throws Exception {
		LiveContestUserIdConfDVO config = null;
		com.datastax.driver.core.ResultSet rSet = null;
		String sql = "Select * from pc_livvx_gen_userid WHERE clintid=? AND nodeid=?";
		try {
			rSet = CassandraConnector.getSession().execute(sql,new Object[] {clientId,nodeId});

			for (Row rs : rSet) {
				config = new LiveContestUserIdConfDVO();
				config.setClId(rs.getString("clintid"));
				config.setCounter(rs.getInt("usrcnt"));
				config.setNodeId(rs.getString("nodeid"));
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
		return config;
	}

	/**
	 * Save Live contest user ID config.
	 *
	 * @param LiveContestUserIdConfDVO
	 *            dvo
	 * @return the boolean
	 * @throws Exception
	 *             the exception
	 */
	public static Boolean saveLiveContestUserConf(LiveContestUserIdConfDVO dvo) throws Exception {

		String sql = "INSERT INTO pc_livvx_gen_userid(clintid,nodeid,usrcnt) values(?,?,?)";
		try {
			CassandraConnector.getSession().execute(sql, new Object[] {dvo.getClId(), dvo.getNodeId(), dvo.getCounter()});
			return true;
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(ex);
		}
	}
	
	
	
	
	/**
	 * Update Live contest user ID config.
	 *
	 * @param LiveContestUserIdConfDVO
	 *            dvo
	 * @return the boolean
	 * @throws Exception
	 *             the exception
	 */
	public static Boolean updateLiveContestUserConf(LiveContestUserIdConfDVO dvo) throws Exception {

		String sql = "UPDATE pc_livvx_gen_userid SET usrcnt=? WHERE clintid=? AND nodeid=?";
		try {
			CassandraConnector.getSession().execute(sql, new Object[] {dvo.getCounter(), dvo.getClId(), dvo.getNodeId()});
			return true;
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(ex);
		}
	}

}
