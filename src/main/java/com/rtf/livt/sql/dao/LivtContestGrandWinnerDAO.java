/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.sql.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.rtf.livt.dvo.ContestGrandWinnerDVO;
import com.rtf.saas.sql.db.DatabaseConnections;

/**
 * The Class LivtContestGrandWinnerDAO.
 */
public class LivtContestGrandWinnerDAO {

	/**
	 * Gets the all winners by co id.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 * @return the all winners by co id
	 * @throws Exception
	 *             the exception
	 */
	public static List<ContestGrandWinnerDVO> getAllWinnersByCoId(String clientId, String contestId) throws Exception {
		List<ContestGrandWinnerDVO> winners = new ArrayList<ContestGrandWinnerDVO>();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer sql = new StringBuffer();
		sql.append("select * from pt_livvx_contest_grand_winners g ")
		.append("left join pu_client_customer c on g.clintid=c.clintid and g.custid=c.custid WHERE g.clintid=? AND g.conid=? order by g.custid");
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			ps.setString(2, contestId);
			rs = ps.executeQuery();

			while (rs.next()) {
				winners.add(new ContestGrandWinnerDVO(rs.getString("clintid"), rs.getString("conid"),
						rs.getString("custid"), rs.getString("rwdtype"), rs.getDouble("rwdval"),
						rs.getString("custfname"), rs.getString("custlname"), rs.getString("userid"),
						rs.getString("custemail"), rs.getString("custphone")));
			}

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return winners;
	}

	/**
	 * Gets the all winners by client id.
	 *
	 * @param clientId
	 *            the client id
	 * @return the all winners by client id
	 * @throws Exception
	 *             the exception
	 */
	public static List<ContestGrandWinnerDVO> getAllWinnersByClientId(String clientId) throws Exception {
		List<ContestGrandWinnerDVO> winners = new ArrayList<ContestGrandWinnerDVO>();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "select * from pt_livvx_contest_grand_winners WHERE clintid=? order by custid";
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, clientId);
			rs = ps.executeQuery();

			while (rs.next()) {
				winners.add(new ContestGrandWinnerDVO(rs.getString("clintid"), rs.getString("conid"),
						rs.getString("custid"), rs.getString("rwdtype"), rs.getDouble("rwdval"), null, null, null, null,
						null));
			}

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return winners;
	}

}
