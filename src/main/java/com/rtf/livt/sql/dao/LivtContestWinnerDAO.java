/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.sql.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.rtf.livt.dvo.ContestWinnerDVO;
import com.rtf.saas.sql.db.DatabaseConnections;

/**
 * The Class LivtContestWinnerDAO.
 */
public class LivtContestWinnerDAO {

	/**
	 * Gets the all winners by co id.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 * @return the all winners by co id
	 * @throws Exception
	 *             the exception
	 */
	public static List<ContestWinnerDVO> getAllWinnersByCoId(String clientId, String contestId) throws Exception {
		List<ContestWinnerDVO> winners = new ArrayList<ContestWinnerDVO>();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer sql = new StringBuffer();
		sql.append( "select * from pt_livvx_contest_winner g ")
		.append("left join pu_client_customer c on g.clintid=c.clintid and g.custid=c.custid WHERE g.clintid = ? AND g.conid = ? ORDER BY g.custid");
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			ps.setString(2, contestId);
			rs = ps.executeQuery();

			while (rs.next()) {
				winners.add(new ContestWinnerDVO(rs.getString("clintid"), rs.getString("conid"), rs.getString("custid"),
						rs.getString("rwdtype"), rs.getDouble("rwdval"), rs.getString("custfname"),
						rs.getString("custlname"), rs.getString("userid"), rs.getString("custemail"),
						rs.getString("custphone")));
			}

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return winners;
	}

}
