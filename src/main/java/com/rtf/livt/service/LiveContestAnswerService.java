/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.service;

import java.util.Map;

import com.rtf.livt.cass.dao.LiveContestAnswerDAO;
import com.rtf.livt.dvo.LiveContestAnswerCountDVO;

/**
 * The Class LiveContestAnswerService.
 */
public class LiveContestAnswerService {

	/**
	 * Gets the contest question answer count.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 * @param questionId
	 *            the question id
	 * @param questionNo
	 *            the question no
	 * @return the contest question answer count
	 * @throws Exception
	 *             the exception
	 */
	public static LiveContestAnswerCountDVO getContestQuestionAnswerCount(String clientId, String contestId,
			Integer questionId, Integer questionNo) throws Exception {

		Map<String, Integer> cntMap = null;
		LiveContestAnswerCountDVO ansCountInfo = new LiveContestAnswerCountDVO();
		try {

			cntMap = LiveContestAnswerDAO.getAllAnswerOptionCntByQNo(clientId, contestId, questionNo);

			if (cntMap != null && !cntMap.isEmpty()) {
				for (String optKey : cntMap.keySet()) {

					if ("A".equalsIgnoreCase(optKey)) {
						Integer cnt = cntMap.get(optKey);
						if (cnt == null)
							cnt = 0;
						ansCountInfo.setOptACount(cnt);
					} else if ("B".equalsIgnoreCase(optKey)) {
						Integer cnt = cntMap.get(optKey);
						if (cnt == null)
							cnt = 0;
						ansCountInfo.setOptBCount(cnt);
					} else if ("C".equalsIgnoreCase(optKey)) {
						Integer cnt = cntMap.get(optKey);
						if (cnt == null)
							cnt = 0;
						ansCountInfo.setOptCCount(cnt);
					} else if ("D".equalsIgnoreCase(optKey)) {
						Integer cnt = cntMap.get(optKey);
						if (cnt == null)
							cnt = 0;
						ansCountInfo.setOptDCount(cnt);
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return ansCountInfo;
	}

	/**
	 * Gets the live contest total cust count.
	 *
	 * @param clId
	 *            the cl id
	 * @param coId
	 *            the co id
	 * @return the live contest total cust count
	 */
	public static Integer getLiveContestTotalCustCount(String clId, String coId) {
		Integer count = 0;
		try {
			count = LiveContestAnswerDAO.getLiveContestTotalCustCount(clId, coId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return count;
	}

}
