/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.service;

import java.util.List;

import com.rtf.livt.cass.dao.LivtContestQuestionCassDAO;
import com.rtf.livt.dto.LivtContestQuestionsDTO;
import com.rtf.livt.dvo.LivtContestQuestionDVO;
import com.rtf.livt.sql.dao.LivtContestQuestionsDAO;
import com.rtf.livt.util.LiveContestGridHeaderFilterUtil;
import com.rtf.saas.util.PaginationUtil;

/**
 * The Class LivtContestQuestionsService.
 */
public class LivtContestQuestionsService {

	/**
	 * Gets the contestquestions by question id.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 * @param questionId
	 *            the question id
	 * @return the contestquestions by question id
	 * @throws Exception
	 *             the exception
	 */
	public static LivtContestQuestionDVO getcontestquestionsByQuestionId(String clientId, String contestId,
			Integer questionId) throws Exception {
		LivtContestQuestionDVO contQuestion = null;
		try {
			contQuestion = LivtContestQuestionsDAO.getcontestquestionsByQuestionId(clientId, contestId, questionId);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return contQuestion;
	}

	/**
	 * Gets the contestquestions bycontest id.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 * @return the contestquestions bycontest id
	 * @throws Exception
	 *             the exception
	 */
	public static List<LivtContestQuestionDVO> getcontestquestionsBycontestId(String clientId, String contestId)
			throws Exception {
		List<LivtContestQuestionDVO> list = null;
		try {
			list = LivtContestQuestionsDAO.getcontestquestionsBycontestId(clientId, contestId);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return list;
	}

	/**
	 * Gets the contestquestions by contest idand question seq no.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 * @param seqno
	 *            the seqno
	 * @return the contestquestions by contest idand question seq no
	 * @throws Exception
	 *             the exception
	 */
	public static LivtContestQuestionDVO getcontestquestionsByContestIdandQuestionSeqNo(String clientId,
			String contestId, Integer seqno) throws Exception {
		LivtContestQuestionDVO newseqNocontestQuestion = null;
		try {
			newseqNocontestQuestion = LivtContestQuestionsDAO.getcontestquestionsByContestIdandQuestionSeqNo(clientId,
					contestId, seqno);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return newseqNocontestQuestion;
	}

	/**
	 * Gets the contest questions count.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 * @return the contest questions count
	 * @throws Exception
	 *             the exception
	 */
	public static Integer getContestQuestionsCount(String clientId, String contestId) throws Exception {
		Integer questionsCount = null;
		try {
			questionsCount = LivtContestQuestionsDAO.getContestQuestionsCount(clientId, contestId);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return questionsCount;
	}

	/**
	 * Gets the contest questions by contest id and question bank id.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 * @param questionBankId
	 *            the question bank id
	 * @return the contest questions by contest id and question bank id
	 * @throws Exception
	 *             the exception
	 */
	public static LivtContestQuestionDVO getcontestquestionsByContestIdandQuestionBankId(String clientId,
			String contestId, Integer questionBankId) throws Exception {
		LivtContestQuestionDVO contQuestion = null;
		try {
			contQuestion = LivtContestQuestionsDAO.getcontestquestionsByContestIdandQuestionBankId(clientId, contestId,
					questionBankId);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return contQuestion;
	}

	/**
	 * Gets the all contest questions by contest idand filter.
	 *
	 * @param respDTO
	 *            the resp DTO
	 * @param filter
	 *            the filter
	 * @param pgNo
	 *            the pg no
	 * @return the all contest questions by contest idand filter
	 * @throws Exception
	 *             the exception
	 */
	public static LivtContestQuestionsDTO getAllContestQuestionsByContestIdandFilter(LivtContestQuestionsDTO respDTO,
			String filter, String pgNo) throws Exception {
		List<LivtContestQuestionDVO> contQuestions = null;
		try {

			String filterQuery = LiveContestGridHeaderFilterUtil.getContestQuestionsFilterQuery(filter);
			contQuestions = LivtContestQuestionsDAO.getcontestquestionsBycontestIdAndFilter(respDTO.getClId(),
					respDTO.getCoId(), filterQuery, pgNo,filter);
			Integer count = LivtContestQuestionsDAO.getAllContestQuestionsCountByContestIdandFilter(respDTO.getClId(),
					respDTO.getCoId(), filterQuery);

			respDTO.setPagination(PaginationUtil.calculatePaginationParameter(count, pgNo));
			respDTO.setList(contQuestions);
			respDTO.setSts(1);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return respDTO;
	}

	/**
	 * Gets the all contest questions data to export.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 * @param filter
	 *            the filter
	 * @return the all contest questions data to export
	 * @throws Exception
	 *             the exception
	 */
	public static List<LivtContestQuestionDVO> getAllContestQuestionsDataToExport(String clientId, String contestId,
			String filter) throws Exception {
		List<LivtContestQuestionDVO> contQuestions = null;
		try {

			String filterQuery = LiveContestGridHeaderFilterUtil.getContestQuestionsFilterQuery(filter);
			contQuestions = LivtContestQuestionsDAO.getAllContestQuestionsDataToExport(clientId, contestId,
					filterQuery);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return contQuestions;
	}

	/**
	 * Save contest questions.
	 *
	 * @param clientId
	 *            the client id
	 * @param contQuestion
	 *            the cont question
	 * @return the integer
	 * @throws Exception
	 *             the exception
	 */
	public static Integer saveContestQuestions(String clientId, LivtContestQuestionDVO contQuestion) throws Exception {
		Integer id = null;
		try {
			id = LivtContestQuestionsDAO.saveContestQuestions(clientId, contQuestion);
			if (id != null) {
				contQuestion.setConqsnid(id);
				LivtContestQuestionCassDAO.saveContestquestions(contQuestion);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return id;
	}

	/**
	 * Update contest questions.
	 *
	 * @param clientId
	 *            the client id
	 * @param contQuestion
	 *            the cont question
	 * @return the integer
	 * @throws Exception
	 *             the exception
	 */
	public static Integer updateContestQuestions(String clientId, LivtContestQuestionDVO contQuestion)
			throws Exception {
		Integer updatecount = null;
		try {
			updatecount = LivtContestQuestionsDAO.updateContestQuestions(clientId, contQuestion);
			if (updatecount != null) {
				LivtContestQuestionCassDAO.updateContestquestions(contQuestion);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return updatecount;
	}

	/**
	 * Delete contest questions.
	 *
	 * @param clientId
	 *            the client id
	 * @param contQuestion
	 *            the cont question
	 * @return the integer
	 * @throws Exception
	 *             the exception
	 */
	public static Integer deleteContestQuestions(String clientId, LivtContestQuestionDVO contQuestion)
			throws Exception {
		Integer updateCount = null;
		try {
			updateCount = LivtContestQuestionsDAO.deleteContestQuestions(clientId, contQuestion);
			if (updateCount != null && updateCount > 0) {
				LivtContestQuestionCassDAO.deleteContestquestions(contQuestion);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return updateCount;
	}

	/**
	 * Update contest questions seqno.
	 *
	 * @param clientId
	 *            the client id
	 * @param contQuestionList
	 *            the cont question list
	 * @return the integer
	 * @throws Exception
	 *             the exception
	 */
	public static Integer updateContestQuestionsSeqno(String clientId, List<LivtContestQuestionDVO> contQuestionList)
			throws Exception {
		Integer updateCount = null;
		try {
			updateCount = LivtContestQuestionsDAO.updateContestQuestionsSeqno(clientId, contQuestionList);
			if (updateCount != null && updateCount > 0) {
				LivtContestQuestionCassDAO.updateContestQuestionsSeqNo(contQuestionList);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return updateCount;
	}

}
