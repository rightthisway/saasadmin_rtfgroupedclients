/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.service;

import java.util.Map;
import java.util.concurrent.Callable;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rtf.common.util.GsonCustomConfig;
import com.rtf.livt.dto.LivtCommonDTO;
import com.rtf.livt.util.HTTPUtil;

/**
 * The Class ClusterApiServiceExecutor.
 */
public class ClusterApiServiceExecutor implements Callable<Integer> {

	/** The apiurl. */
	private String apiurl;

	/** The data map. */
	private Map<String, String> dataMap;

	/**
	 * Instantiates a new cluster api service executor.
	 *
	 * @param apiurl
	 *            the apiurl
	 * @param dataMap
	 *            the data map
	 */
	public ClusterApiServiceExecutor(String apiurl, Map<String, String> dataMap) {
		this.apiurl = apiurl;
		this.dataMap = dataMap;
	}

	/**
	 * Call.
	 *
	 * @return the integer
	 * @throws Exception
	 *             the exception
	 */
	@Override
	public Integer call() throws Exception {
		final String data = HTTPUtil.execute(dataMap, apiurl);
		Gson gson = GsonCustomConfig.getGsonBuilder();
		JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
		final LivtCommonDTO resp = gson.fromJson(((JsonObject) jsonObject.get("livtresp")), LivtCommonDTO.class);
		if (resp == null)
			return 0;
		if (resp.getSts() == 0) {
			return 0;
		}
		return resp.getSts();
	}

	/**
	 * Gets the apiurl.
	 *
	 * @return the apiurl
	 */
	public String getApiurl() {
		return apiurl;
	}

	/**
	 * Sets the apiurl.
	 *
	 * @param apiurl
	 *            the new apiurl
	 */
	public void setApiurl(String apiurl) {
		this.apiurl = apiurl;
	}

	/**
	 * Gets the data map.
	 *
	 * @return the data map
	 */
	public Map<String, String> getDataMap() {
		return dataMap;
	}

	/**
	 * Sets the data map.
	 *
	 * @param dataMap
	 *            the data map
	 */
	public void setDataMap(Map<String, String> dataMap) {
		this.dataMap = dataMap;
	}

}
