/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.dvo;

import java.io.Serializable;

/**
 * The Class ContestAuditDVO.
 */
public class ContestAuditDVO implements Serializable {

	private static final long serialVersionUID = 1596439345108267739L;

	/** The co id. */
	private String coId;

	/** The cl id. */
	private String clId;

	/** The img U. */
	private String imgU;

	/** The seq no. */
	private Integer seqNo;

	/** The cat. */
	private String cat;

	/** The sub cat. */
	private String subCat;

	/** The cl img U. */
	private String clImgU;

	/** The st date. */
	private Long stDate;

	/** The name. */
	private String name;

	/** The ext name. */
	private String extName;

	/** The cr by. */
	private String crBy;

	/** The cr date. */
	private Long crDate;

	/** The is act. */
	private Integer isAct;

	/** The q size. */
	private Integer qSize;

	/** The up date. */
	private Long upDate;

	/** The up by. */
	private String upBy;

	/** The ans type. */
	private String ansType;

	/** The co type. */
	private String coType;

	/** The is elimination. */
	private Boolean isElimination;

	/** The is split summary. */
	private Boolean isSplitSummary;

	/** The sum rwd type. */
	private String sumRwdType;

	/** The sum rwd val. */
	private Double sumRwdVal;

	/** The is lot enbl. */
	private Boolean isLotEnbl;

	/** The win rwd type. */
	private String winRwdType;

	/** The win rwd val. */
	private Double winRwdVal;

	/** The que rwd type. */
	private String queRwdType;

	/** The parti rwd type. */
	private String partiRwdType;

	/** The parti rwd val. */
	private Double partiRwdVal;

	/** The winner count. */
	private Integer winnerCount;

	/** The thm img mob. */
	private String thmImgMob;

	/** The thm img desk. */
	private String thmImgDesk;

	/** The play game img. */
	private String playGameImg;

	/** The thm id. */
	private Integer thmId;

	/** The thm color. */
	private String thmColor;

	/** The is pwd. */
	private Boolean isPwd;

	/** The con pwd. */
	private String conPwd;

	/** The exp part cnt. */
	private Integer expPartCnt;

	/**
	 * Instantiates a new contest audit DVO.
	 *
	 * @param co
	 *            the co
	 */
	public ContestAuditDVO(ContestDVO co) {
		this.ansType = co.getAnsType();
		this.cat = co.getCat();
		this.clId = co.getClId();
		this.clImgU = co.getClImgU();
		this.coId = co.getCoId();
		this.coType = co.getCoType();
		this.crBy = co.getCrBy();
		this.crDate = co.getCrDate();
		this.extName = co.getExtName();
		this.imgU = co.getImgU();
		this.isAct = co.getIsAct();
		this.isElimination = co.getIsElimination();
		this.isLotEnbl = co.getIsLotEnbl();
		this.isSplitSummary = co.getIsSplitSummary();
		this.name = co.getName();
		this.partiRwdType = co.getPartiRwdType();
		this.partiRwdVal = co.getPartiRwdVal();
		this.playGameImg = co.getPlayGameImg();
		this.qSize = co.getqSize();
		this.queRwdType = co.getQueRwdType();
		this.seqNo = co.getSeqNo();
		this.stDate = co.getStDate();
		this.subCat = co.getSubCat();
		this.sumRwdType = co.getSumRwdType();
		this.sumRwdVal = co.getSumRwdVal();
		this.thmColor = co.getThmColor();
		this.thmId = co.getThmId();
		this.thmImgDesk = co.getThmImgDesk();
		this.thmImgMob = co.getThmImgMob();
		this.upBy = co.getUpBy();
		this.upDate = co.getUpDate();
		this.winnerCount = co.getWinnerCount();
		this.winRwdType = co.getWinRwdType();
		this.winRwdVal = co.getWinRwdVal();
		this.isPwd = co.getIsPwd();
		this.conPwd = co.getConPwd();
		this.expPartCnt = co.getExpPartCount();

	}

	/**
	 * Gets the co id.
	 *
	 * @return the co id
	 */
	public String getCoId() {
		return coId;
	}

	/**
	 * Sets the co id.
	 *
	 * @param coId
	 *            the new co id
	 */
	public void setCoId(String coId) {
		this.coId = coId;
	}

	/**
	 * Gets the cl id.
	 *
	 * @return the cl id
	 */
	public String getClId() {
		return clId;
	}

	/**
	 * Sets the cl id.
	 *
	 * @param clId
	 *            the new cl id
	 */
	public void setClId(String clId) {
		this.clId = clId;
	}

	/**
	 * Gets the img U.
	 *
	 * @return the img U
	 */
	public String getImgU() {
		return imgU;
	}

	/**
	 * Sets the img U.
	 *
	 * @param imgU
	 *            the new img U
	 */
	public void setImgU(String imgU) {
		this.imgU = imgU;
	}

	/**
	 * Gets the seq no.
	 *
	 * @return the seq no
	 */
	public Integer getSeqNo() {
		return seqNo;
	}

	/**
	 * Sets the seq no.
	 *
	 * @param seqNo
	 *            the new seq no
	 */
	public void setSeqNo(Integer seqNo) {
		this.seqNo = seqNo;
	}

	/**
	 * Gets the cat.
	 *
	 * @return the cat
	 */
	public String getCat() {
		return cat;
	}

	/**
	 * Sets the cat.
	 *
	 * @param cat
	 *            the new cat
	 */
	public void setCat(String cat) {
		this.cat = cat;
	}

	/**
	 * Gets the sub cat.
	 *
	 * @return the sub cat
	 */
	public String getSubCat() {
		return subCat;
	}

	/**
	 * Sets the sub cat.
	 *
	 * @param subCat
	 *            the new sub cat
	 */
	public void setSubCat(String subCat) {
		this.subCat = subCat;
	}

	/**
	 * Gets the cl img U.
	 *
	 * @return the cl img U
	 */
	public String getClImgU() {
		return clImgU;
	}

	/**
	 * Sets the cl img U.
	 *
	 * @param clImgU
	 *            the new cl img U
	 */
	public void setClImgU(String clImgU) {
		this.clImgU = clImgU;
	}

	/**
	 * Gets the st date.
	 *
	 * @return the st date
	 */
	public Long getStDate() {
		return stDate;
	}

	/**
	 * Sets the st date.
	 *
	 * @param stDate
	 *            the new st date
	 */
	public void setStDate(Long stDate) {
		this.stDate = stDate;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the ext name.
	 *
	 * @return the ext name
	 */
	public String getExtName() {
		return extName;
	}

	/**
	 * Sets the ext name.
	 *
	 * @param extName
	 *            the new ext name
	 */
	public void setExtName(String extName) {
		this.extName = extName;
	}

	/**
	 * Gets the cr by.
	 *
	 * @return the cr by
	 */
	public String getCrBy() {
		return crBy;
	}

	/**
	 * Sets the cr by.
	 *
	 * @param crBy
	 *            the new cr by
	 */
	public void setCrBy(String crBy) {
		this.crBy = crBy;
	}

	/**
	 * Gets the cr date.
	 *
	 * @return the cr date
	 */
	public Long getCrDate() {
		return crDate;
	}

	/**
	 * Sets the cr date.
	 *
	 * @param crDate
	 *            the new cr date
	 */
	public void setCrDate(Long crDate) {
		this.crDate = crDate;
	}

	/**
	 * Gets the checks if is act.
	 *
	 * @return the checks if is act
	 */
	public Integer getIsAct() {
		return isAct;
	}

	/**
	 * Sets the checks if is act.
	 *
	 * @param isAct
	 *            the new checks if is act
	 */
	public void setIsAct(Integer isAct) {
		this.isAct = isAct;
	}

	/**
	 * Gets the q size.
	 *
	 * @return the q size
	 */
	public Integer getqSize() {
		return qSize;
	}

	/**
	 * Sets the q size.
	 *
	 * @param qSize
	 *            the new q size
	 */
	public void setqSize(Integer qSize) {
		this.qSize = qSize;
	}

	/**
	 * Gets the up date.
	 *
	 * @return the up date
	 */
	public Long getUpDate() {
		return upDate;
	}

	/**
	 * Sets the up date.
	 *
	 * @param upDate
	 *            the new up date
	 */
	public void setUpDate(Long upDate) {
		this.upDate = upDate;
	}

	/**
	 * Gets the up by.
	 *
	 * @return the up by
	 */
	public String getUpBy() {
		return upBy;
	}

	/**
	 * Sets the up by.
	 *
	 * @param upBy
	 *            the new up by
	 */
	public void setUpBy(String upBy) {
		this.upBy = upBy;
	}

	/**
	 * Gets the ans type.
	 *
	 * @return the ans type
	 */
	public String getAnsType() {
		return ansType;
	}

	/**
	 * Sets the ans type.
	 *
	 * @param ansType
	 *            the new ans type
	 */
	public void setAnsType(String ansType) {
		this.ansType = ansType;
	}

	/**
	 * Gets the co type.
	 *
	 * @return the co type
	 */
	public String getCoType() {
		return coType;
	}

	/**
	 * Sets the co type.
	 *
	 * @param coType
	 *            the new co type
	 */
	public void setCoType(String coType) {
		this.coType = coType;
	}

	/**
	 * Gets the checks if is elimination.
	 *
	 * @return the checks if is elimination
	 */
	public Boolean getIsElimination() {
		return isElimination;
	}

	/**
	 * Sets the checks if is elimination.
	 *
	 * @param isElimination
	 *            the new checks if is elimination
	 */
	public void setIsElimination(Boolean isElimination) {
		this.isElimination = isElimination;
	}

	/**
	 * Gets the checks if is split summary.
	 *
	 * @return the checks if is split summary
	 */
	public Boolean getIsSplitSummary() {
		return isSplitSummary;
	}

	/**
	 * Sets the checks if is split summary.
	 *
	 * @param isSplitSummary
	 *            the new checks if is split summary
	 */
	public void setIsSplitSummary(Boolean isSplitSummary) {
		this.isSplitSummary = isSplitSummary;
	}

	/**
	 * Gets the sum rwd type.
	 *
	 * @return the sum rwd type
	 */
	public String getSumRwdType() {
		return sumRwdType;
	}

	/**
	 * Sets the sum rwd type.
	 *
	 * @param sumRwdType
	 *            the new sum rwd type
	 */
	public void setSumRwdType(String sumRwdType) {
		this.sumRwdType = sumRwdType;
	}

	/**
	 * Gets the sum rwd val.
	 *
	 * @return the sum rwd val
	 */
	public Double getSumRwdVal() {
		return sumRwdVal;
	}

	/**
	 * Sets the sum rwd val.
	 *
	 * @param sumRwdVal
	 *            the new sum rwd val
	 */
	public void setSumRwdVal(Double sumRwdVal) {
		this.sumRwdVal = sumRwdVal;
	}

	/**
	 * Gets the checks if is lot enbl.
	 *
	 * @return the checks if is lot enbl
	 */
	public Boolean getIsLotEnbl() {
		return isLotEnbl;
	}

	/**
	 * Sets the checks if is lot enbl.
	 *
	 * @param isLotEnbl
	 *            the new checks if is lot enbl
	 */
	public void setIsLotEnbl(Boolean isLotEnbl) {
		this.isLotEnbl = isLotEnbl;
	}

	/**
	 * Gets the win rwd type.
	 *
	 * @return the win rwd type
	 */
	public String getWinRwdType() {
		return winRwdType;
	}

	/**
	 * Sets the win rwd type.
	 *
	 * @param winRwdType
	 *            the new win rwd type
	 */
	public void setWinRwdType(String winRwdType) {
		this.winRwdType = winRwdType;
	}

	/**
	 * Gets the win rwd val.
	 *
	 * @return the win rwd val
	 */
	public Double getWinRwdVal() {
		return winRwdVal;
	}

	/**
	 * Sets the win rwd val.
	 *
	 * @param winRwdVal
	 *            the new win rwd val
	 */
	public void setWinRwdVal(Double winRwdVal) {
		this.winRwdVal = winRwdVal;
	}

	/**
	 * Gets the que rwd type.
	 *
	 * @return the que rwd type
	 */
	public String getQueRwdType() {
		return queRwdType;
	}

	/**
	 * Sets the que rwd type.
	 *
	 * @param queRwdType
	 *            the new que rwd type
	 */
	public void setQueRwdType(String queRwdType) {
		this.queRwdType = queRwdType;
	}

	/**
	 * Gets the parti rwd type.
	 *
	 * @return the parti rwd type
	 */
	public String getPartiRwdType() {
		return partiRwdType;
	}

	/**
	 * Sets the parti rwd type.
	 *
	 * @param partiRwdType
	 *            the new parti rwd type
	 */
	public void setPartiRwdType(String partiRwdType) {
		this.partiRwdType = partiRwdType;
	}

	/**
	 * Gets the parti rwd val.
	 *
	 * @return the parti rwd val
	 */
	public Double getPartiRwdVal() {
		return partiRwdVal;
	}

	/**
	 * Sets the parti rwd val.
	 *
	 * @param partiRwdVal
	 *            the new parti rwd val
	 */
	public void setPartiRwdVal(Double partiRwdVal) {
		this.partiRwdVal = partiRwdVal;
	}

	/**
	 * Gets the winner count.
	 *
	 * @return the winner count
	 */
	public Integer getWinnerCount() {
		return winnerCount;
	}

	/**
	 * Sets the winner count.
	 *
	 * @param winnerCount
	 *            the new winner count
	 */
	public void setWinnerCount(Integer winnerCount) {
		this.winnerCount = winnerCount;
	}

	/**
	 * Gets the thm img mob.
	 *
	 * @return the thm img mob
	 */
	public String getThmImgMob() {
		return thmImgMob;
	}

	/**
	 * Sets the thm img mob.
	 *
	 * @param thmImgMob
	 *            the new thm img mob
	 */
	public void setThmImgMob(String thmImgMob) {
		this.thmImgMob = thmImgMob;
	}

	/**
	 * Gets the thm img desk.
	 *
	 * @return the thm img desk
	 */
	public String getThmImgDesk() {
		return thmImgDesk;
	}

	/**
	 * Sets the thm img desk.
	 *
	 * @param thmImgDesk
	 *            the new thm img desk
	 */
	public void setThmImgDesk(String thmImgDesk) {
		this.thmImgDesk = thmImgDesk;
	}

	/**
	 * Gets the play game img.
	 *
	 * @return the play game img
	 */
	public String getPlayGameImg() {
		return playGameImg;
	}

	/**
	 * Sets the play game img.
	 *
	 * @param playGameImg
	 *            the new play game img
	 */
	public void setPlayGameImg(String playGameImg) {
		this.playGameImg = playGameImg;
	}

	/**
	 * Gets the thm id.
	 *
	 * @return the thm id
	 */
	public Integer getThmId() {
		return thmId;
	}

	/**
	 * Sets the thm id.
	 *
	 * @param thmId
	 *            the new thm id
	 */
	public void setThmId(Integer thmId) {
		this.thmId = thmId;
	}

	/**
	 * Gets the thm color.
	 *
	 * @return the thm color
	 */
	public String getThmColor() {
		return thmColor;
	}

	/**
	 * Sets the thm color.
	 *
	 * @param thmColor
	 *            the new thm color
	 */
	public void setThmColor(String thmColor) {
		this.thmColor = thmColor;
	}

	/**
	 * Gets the checks if is pwd.
	 *
	 * @return the checks if is pwd
	 */
	public Boolean getIsPwd() {
		return isPwd;
	}

	/**
	 * Sets the checks if is pwd.
	 *
	 * @param isPwd
	 *            the new checks if is pwd
	 */
	public void setIsPwd(Boolean isPwd) {
		this.isPwd = isPwd;
	}

	/**
	 * Gets the con pwd.
	 *
	 * @return the con pwd
	 */
	public String getConPwd() {
		return conPwd;
	}

	/**
	 * Sets the con pwd.
	 *
	 * @param conPwd
	 *            the new con pwd
	 */
	public void setConPwd(String conPwd) {
		this.conPwd = conPwd;
	}

	/**
	 * Gets the exp part cnt.
	 *
	 * @return the exp part cnt
	 */
	public Integer getExpPartCnt() {
		return expPartCnt;
	}

	/**
	 * Sets the exp part cnt.
	 *
	 * @param expPartCnt
	 *            the new exp part cnt
	 */
	public void setExpPartCnt(Integer expPartCnt) {
		this.expPartCnt = expPartCnt;
	}

}
