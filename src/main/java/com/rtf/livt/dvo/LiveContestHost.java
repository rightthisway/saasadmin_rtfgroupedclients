package com.rtf.livt.dvo;

import java.io.Serializable;

/**
 * The Class LiveContestHost.
 */
public class LiveContestHost implements Serializable{
	
	private static final long serialVersionUID = 3376718869544700077L;
	
	/** The client ID. */
	private String clId;
	
	/** The host ID. */
	private String hostId;
	
	/** The host first name. */
	private String fName;
	
	/** The host last name. */
	private String lName;
	
	/** The host full name */
	private String hostName;
	
	/** The host image URL. */
	private String hostImageUrl;
	
	
	
	/**
	 * Gets client ID.
	 *
	 * @return clId
	 */
	public String getClId() {
		return clId;
	}
	
	/**
	 * Sets client ID.
	 *
	 * @param clId
	 *            the client ID
	 */
	public void setClId(String clId) {
		this.clId = clId;
	}
	
	/**
	 * Gets host ID.
	 *
	 * @return hostId
	 */
	public String getHostId() {
		return hostId;
	}
	
	/**
	 * Sets host ID.
	 *
	 * @param hostId
	 */
	public void setHostId(String hostId) {
		this.hostId = hostId;
	}
	
	
	
	/**
	 * Gets host image URL.
	 *
	 * @return hostImageUrl
	 */
	public String getHostImageUrl() {
		return hostImageUrl;
	}
	
	/**
	 * Sets host image URL.
	 *
	 * @param hostImageUrl
	 */
	public void setHostImageUrl(String hostImageUrl) {
		this.hostImageUrl = hostImageUrl;
	}

	
	/**
	 * Gets host first name.
	 *
	 * @return fName
	 */
	public String getfName() {
		return fName;
	}

	/**
	 * Sets host first name.
	 *
	 * @param fName
	 */
	public void setfName(String fName) {
		this.fName = fName;
	}

	/**
	 * Gets host last name.
	 *
	 * @return lName
	 */
	public String getlName() {
		return lName;
	}

	/**
	 * Sets host last name.
	 *
	 * @param lName
	 */
	public void setlName(String lName) {
		this.lName = lName;
	}

	/**
	 * Gets host full name.
	 *
	 * @return hostName
	 */
	public String getHostName() {
		hostName = "";
		if(fName!=null && !fName.isEmpty()){
			hostName = fName+ " ";
		}
		if(lName!=null && !lName.isEmpty()){
			hostName += lName;
		}
		return hostName;
	}

	/**
	 * Sets host full name.
	 *
	 * @param hostName
	 */
	public void setHostName(String hostName) {
		this.hostName = hostName;
	}
	
	
	
	
}
