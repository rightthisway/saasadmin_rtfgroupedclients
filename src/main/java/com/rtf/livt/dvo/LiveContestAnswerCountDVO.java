/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.dvo;

import java.io.Serializable;

/**
 * The Class LiveContestAnswerCountDVO.
 */
public class LiveContestAnswerCountDVO implements Serializable {

	private static final long serialVersionUID = 4043135145829526291L;

	/** The opt A count. */
	private Integer optACount;

	/** The opt B count. */
	private Integer optBCount;

	/** The opt C count. */
	private Integer optCCount;

	/** The opt D count. */
	private Integer optDCount;

	/** The rwd val. */
	private Double rwdVal;

	/**
	 * Gets the opt A count.
	 *
	 * @return the opt A count
	 */
	public Integer getOptACount() {
		return optACount;
	}

	/**
	 * Sets the opt A count.
	 *
	 * @param optACount
	 *            the new opt A count
	 */
	public void setOptACount(Integer optACount) {
		this.optACount = optACount;
	}

	/**
	 * Gets the opt B count.
	 *
	 * @return the opt B count
	 */
	public Integer getOptBCount() {
		return optBCount;
	}

	/**
	 * Sets the opt B count.
	 *
	 * @param optBCount
	 *            the new opt B count
	 */
	public void setOptBCount(Integer optBCount) {
		this.optBCount = optBCount;
	}

	/**
	 * Gets the opt C count.
	 *
	 * @return the opt C count
	 */
	public Integer getOptCCount() {
		return optCCount;
	}

	/**
	 * Sets the opt C count.
	 *
	 * @param optCCount
	 *            the new opt C count
	 */
	public void setOptCCount(Integer optCCount) {
		this.optCCount = optCCount;
	}

	/**
	 * Gets the opt D count.
	 *
	 * @return the opt D count
	 */
	public Integer getOptDCount() {
		return optDCount;
	}

	/**
	 * Sets the opt D count.
	 *
	 * @param optDCount
	 *            the new opt D count
	 */
	public void setOptDCount(Integer optDCount) {
		this.optDCount = optDCount;
	}

	/**
	 * Gets the rwd val.
	 *
	 * @return the rwd val
	 */
	public Double getRwdVal() {
		return rwdVal;
	}

	/**
	 * Sets the rwd val.
	 *
	 * @param rwdVal
	 *            the new rwd val
	 */
	public void setRwdVal(Double rwdVal) {
		this.rwdVal = rwdVal;
	}

}
