/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.dvo;

import java.io.Serializable;

/**
 * The Class LiveExpectedParticipantsDVO.
 */
public class LiveExpectedParticipantsDVO implements Serializable {

	private static final long serialVersionUID = -5743433325451761565L;

	/** The id. */
	private Integer id;

	/** The exp cnt str. */
	private String expCntStr;

	/** The exp cnt. */
	private Integer expCnt;

	/** The is act. */
	private Boolean isAct;

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the new id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Gets the exp cnt str.
	 *
	 * @return the exp cnt str
	 */
	public String getExpCntStr() {
		return expCntStr;
	}

	/**
	 * Sets the exp cnt str.
	 *
	 * @param expCntStr
	 *            the new exp cnt str
	 */
	public void setExpCntStr(String expCntStr) {
		this.expCntStr = expCntStr;
	}

	/**
	 * Gets the exp cnt.
	 *
	 * @return the exp cnt
	 */
	public Integer getExpCnt() {
		return expCnt;
	}

	/**
	 * Sets the exp cnt.
	 *
	 * @param expCnt
	 *            the new exp cnt
	 */
	public void setExpCnt(Integer expCnt) {
		this.expCnt = expCnt;
	}

	/**
	 * Gets the checks if is act.
	 *
	 * @return the checks if is act
	 */
	public Boolean getIsAct() {
		return isAct;
	}

	/**
	 * Sets the checks if is act.
	 *
	 * @param isAct
	 *            the new checks if is act
	 */
	public void setIsAct(Boolean isAct) {
		this.isAct = isAct;
	}

}
