/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.dvo;

import java.io.Serializable;
import java.util.Date;

/**
 * The Class ContestParticipantsDVO.
 */
public class ContestParticipantsDVO implements Serializable {

	private static final long serialVersionUID = 1452684198722292391L;

	/** The co id. */
	private String coId;

	/** The cu id. */
	private String cuId;

	/** The cl id. */
	private String clId;

	/** The pfm. */
	private String pfm;

	/** The ip add. */
	private String ipAdd;

	/** The jn date. */
	private Date jnDate;

	/** The ex date. */
	private Date exDate;

	/** The status. */
	private String status;

	/** The u id. */
	private String uId;

	/** The email. */
	private String email;

	/**
	 * Instantiates a new contest participants DVO.
	 */
	public ContestParticipantsDVO() {
	}

	/**
	 * Instantiates a new contest participants DVO.
	 *
	 * @param coId
	 *            the co id
	 * @param clId
	 *            the cl id
	 * @param cuId
	 *            the cu id
	 * @param pfm
	 *            the pfm
	 * @param ipAdd
	 *            the ip add
	 * @param jnDate
	 *            the jn date
	 * @param exDate
	 *            the ex date
	 * @param status
	 *            the status
	 * @param uId
	 *            the u id
	 */
	public ContestParticipantsDVO(String coId, String clId, String cuId, String pfm, String ipAdd, Date jnDate,
			Date exDate, String status, String uId) {
		this.coId = coId;
		this.clId = clId;
		this.cuId = cuId;
		this.pfm = pfm;
		this.ipAdd = ipAdd;
		this.jnDate = jnDate;
		this.exDate = exDate;
		this.status = status;
		this.uId = uId;
	}

	/**
	 * Gets the co id.
	 *
	 * @return the co id
	 */
	public String getCoId() {
		return coId;
	}

	/**
	 * Sets the co id.
	 *
	 * @param coId
	 *            the new co id
	 */
	public void setCoId(String coId) {
		this.coId = coId;
	}

	/**
	 * Gets the cu id.
	 *
	 * @return the cu id
	 */
	public String getCuId() {
		return cuId;
	}

	/**
	 * Sets the cu id.
	 *
	 * @param cuId
	 *            the new cu id
	 */
	public void setCuId(String cuId) {
		this.cuId = cuId;
	}

	/**
	 * Gets the cl id.
	 *
	 * @return the cl id
	 */
	public String getClId() {
		return clId;
	}

	/**
	 * Sets the cl id.
	 *
	 * @param clId
	 *            the new cl id
	 */
	public void setClId(String clId) {
		this.clId = clId;
	}

	/**
	 * Gets the pfm.
	 *
	 * @return the pfm
	 */
	public String getPfm() {
		if (null == pfm) {
			pfm = "";
		}
		return pfm;
	}

	/**
	 * Sets the pfm.
	 *
	 * @param pfm
	 *            the new pfm
	 */
	public void setPfm(String pfm) {
		this.pfm = pfm;
	}

	/**
	 * Gets the ip add.
	 *
	 * @return the ip add
	 */
	public String getIpAdd() {
		if (null == ipAdd) {
			ipAdd = "";
		}
		return ipAdd;
	}

	/**
	 * Sets the ip add.
	 *
	 * @param ipAdd
	 *            the new ip add
	 */
	public void setIpAdd(String ipAdd) {
		this.ipAdd = ipAdd;
	}

	/**
	 * Gets the jn date.
	 *
	 * @return the jn date
	 */
	public Date getJnDate() {
		return jnDate;
	}

	/**
	 * Sets the jn date.
	 *
	 * @param jnDate
	 *            the new jn date
	 */
	public void setJnDate(Date jnDate) {
		this.jnDate = jnDate;
	}

	/**
	 * Gets the ex date.
	 *
	 * @return the ex date
	 */
	public Date getExDate() {
		return exDate;
	}

	/**
	 * Sets the ex date.
	 *
	 * @param exDate
	 *            the new ex date
	 */
	public void setExDate(Date exDate) {
		this.exDate = exDate;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status
	 *            the new status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Gets the u id.
	 *
	 * @return the u id
	 */
	public String getuId() {
		return uId;
	}

	/**
	 * Sets the u id.
	 *
	 * @param uId
	 *            the new u id
	 */
	public void setuId(String uId) {
		this.uId = uId;
	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Sets the email.
	 *
	 * @param email
	 *            the new email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

}
