/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.dvo;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.rtf.ott.util.DateFormatUtil;

/**
 * The Class ContestListDVO.
 */
public class ContestListDVO implements Serializable {

	private static final long serialVersionUID = 6370323556264296757L;

	/** The co id. */
	private String coId;

	/** The cl id. */
	@JsonIgnore
	private String clId;

	/** The st date. */
	@JsonIgnore
	private Long stDate;

	/** The name. */
	@JsonIgnore
	private String name;

	/** The st date time str. */
	@JsonIgnore
	private String stDateTimeStr;

	/** The dis text. */
	private String disText;

	/**
	 * Gets the co id.
	 *
	 * @return the co id
	 */
	public String getCoId() {
		return coId;
	}

	/**
	 * Sets the co id.
	 *
	 * @param coId
	 *            the new co id
	 */
	public void setCoId(String coId) {
		this.coId = coId;
	}

	/**
	 * Gets the cl id.
	 *
	 * @return the cl id
	 */
	public String getClId() {
		return clId;
	}

	/**
	 * Sets the cl id.
	 *
	 * @param clId
	 *            the new cl id
	 */
	public void setClId(String clId) {
		this.clId = clId;
	}

	/**
	 * Gets the st date.
	 *
	 * @return the st date
	 */
	public Long getStDate() {
		return stDate;
	}

	/**
	 * Sets the st date.
	 *
	 * @param stDate
	 *            the new st date
	 */
	public void setStDate(Long stDate) {
		this.stDate = stDate;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the st date time str.
	 *
	 * @return the st date time str
	 */
	public String getStDateTimeStr() {
		if (stDate != null) {
			stDateTimeStr = DateFormatUtil.getMMDDYYYYHHMMSSString(stDate);
		}

		return stDateTimeStr;
	}

	/**
	 * Sets the st date time str.
	 *
	 * @param stDateTimeStr
	 *            the new st date time str
	 */
	public void setStDateTimeStr(String stDateTimeStr) {
		this.stDateTimeStr = stDateTimeStr;
	}

	/**
	 * Gets the dis text.
	 *
	 * @return the dis text
	 */
	public String getDisText() {
		disText = getName() + " - " + getStDateTimeStr();
		return disText;
	}

	/**
	 * Sets the dis text.
	 *
	 * @param disText
	 *            the new dis text
	 */
	public void setDisText(String disText) {
		this.disText = disText;
	}

}
