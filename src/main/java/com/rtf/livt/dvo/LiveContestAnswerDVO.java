/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.dvo;

import java.io.Serializable;

/**
 * The Class LiveContestAnswerDVO.
 */
public class LiveContestAnswerDVO implements Serializable {

	private static final long serialVersionUID = 994075551322243429L;

	/** The cl id. */
	private String clId;

	/** The cu id. */
	private String cuId;

	/** The co id. */
	private String coId;

	/** The q id. */
	private Integer qId;

	/** The q no. */
	private Integer qNo;

	/** The ans. */
	private String ans;

	/** The is crt. */
	private Boolean isCrt = false;

	/** The is life. */
	private Boolean isLife;

	/** The a rwds. */
	private Double aRwds;

	/**
	 * Instantiates a new live contest answer DVO.
	 */
	public LiveContestAnswerDVO() {
	}

	/**
	 * Instantiates a new live contest answer DVO.
	 *
	 * @param clId
	 *            the cl id
	 * @param cuId
	 *            the cu id
	 * @param coId
	 *            the co id
	 * @param qId
	 *            the q id
	 * @param qNo
	 *            the q no
	 * @param ans
	 *            the ans
	 * @param isCrt
	 *            the is crt
	 * @param isLife
	 *            the is life
	 */
	public LiveContestAnswerDVO(String clId, String cuId, String coId, Integer qId, Integer qNo, String ans,
			Boolean isCrt, Boolean isLife) {
		this.clId = clId;
		this.cuId = cuId;
		this.coId = coId;
		this.qId = qId;
		this.qNo = qNo;
		this.ans = ans;
		this.isCrt = isCrt;
		this.isLife = isLife;
	}

	/**
	 * Gets the cu id.
	 *
	 * @return the cu id
	 */
	public String getCuId() {
		return cuId;
	}

	/**
	 * Sets the cu id.
	 *
	 * @param cuId
	 *            the new cu id
	 */
	public void setCuId(String cuId) {
		this.cuId = cuId;
	}

	/**
	 * Gets the cl id.
	 *
	 * @return the cl id
	 */
	public String getClId() {
		return clId;
	}

	/**
	 * Sets the cl id.
	 *
	 * @param clId
	 *            the new cl id
	 */
	public void setClId(String clId) {
		this.clId = clId;
	}

	/**
	 * Gets the co id.
	 *
	 * @return the co id
	 */
	public String getCoId() {
		return coId;
	}

	/**
	 * Sets the co id.
	 *
	 * @param coId
	 *            the new co id
	 */
	public void setCoId(String coId) {
		this.coId = coId;
	}

	/**
	 * Gets the q id.
	 *
	 * @return the q id
	 */
	public Integer getqId() {
		return qId;
	}

	/**
	 * Sets the q id.
	 *
	 * @param qId
	 *            the new q id
	 */
	public void setqId(Integer qId) {
		this.qId = qId;
	}

	/**
	 * Gets the q no.
	 *
	 * @return the q no
	 */
	public Integer getqNo() {
		return qNo;
	}

	/**
	 * Sets the q no.
	 *
	 * @param qNo
	 *            the new q no
	 */
	public void setqNo(Integer qNo) {
		this.qNo = qNo;
	}

	/**
	 * Gets the ans.
	 *
	 * @return the ans
	 */
	public String getAns() {
		return ans;
	}

	/**
	 * Sets the ans.
	 *
	 * @param ans
	 *            the new ans
	 */
	public void setAns(String ans) {
		this.ans = ans;
	}

	/**
	 * Gets the checks if is crt.
	 *
	 * @return the checks if is crt
	 */
	public Boolean getIsCrt() {
		return isCrt;
	}

	/**
	 * Sets the checks if is crt.
	 *
	 * @param isCrt
	 *            the new checks if is crt
	 */
	public void setIsCrt(Boolean isCrt) {
		this.isCrt = isCrt;
	}

	/**
	 * Gets the checks if is life.
	 *
	 * @return the checks if is life
	 */
	public Boolean getIsLife() {
		return isLife;
	}

	/**
	 * Sets the checks if is life.
	 *
	 * @param isLife
	 *            the new checks if is life
	 */
	public void setIsLife(Boolean isLife) {
		this.isLife = isLife;
	}

	/**
	 * Gets the a rwds.
	 *
	 * @return the a rwds
	 */
	public Double getaRwds() {
		if (aRwds == null) {
			aRwds = 0.0;
		}
		return aRwds;
	}

	/**
	 * Sets the a rwds.
	 *
	 * @param aRwds
	 *            the new a rwds
	 */
	public void setaRwds(Double aRwds) {
		this.aRwds = aRwds;
	}

}
