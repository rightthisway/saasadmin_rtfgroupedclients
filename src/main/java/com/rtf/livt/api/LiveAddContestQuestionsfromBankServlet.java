/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.common.util.RandomGenerator;
import com.rtf.livt.dto.LivtContestQuestionsDTO;
import com.rtf.livt.dvo.ContestDVO;
import com.rtf.livt.dvo.LivtContestQuestionDVO;
import com.rtf.livt.service.LiveContestService;
import com.rtf.livt.service.LivtContestQuestionsService;
import com.rtf.livt.util.MessageConstant;
import com.rtf.ott.sql.dao.QuestionBankSQLDAO;
import com.rtf.ott.sql.dvo.QuestionBankDVO;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;
import com.rtf.user.util.UserMsgConstants;

/**
 * The Class SNWAddContestQuestionsfromBankServlet.
 */
@WebServlet("/liveAddContQuestFromBank.json")
public class LiveAddContestQuestionsfromBankServlet extends RtfSaasBaseServlet {

	/**
	 * Serial version id
	 */
	private static final long serialVersionUID = -6113963614652432878L;

	/**
	 * Generate Http Response for live contest question Response data is sent in JSON format.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}

	/**
	 * Add Contest questions from question bank.
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String clId = request.getParameter("clId");
		String contId = request.getParameter("coId");
		String questionbnkIdStr = request.getParameter("qbId");
		String userName = request.getParameter("cau");

		LivtContestQuestionsDTO respDTO = new LivtContestQuestionsDTO();
		respDTO.setSts(0);
		respDTO.setClId(clId);

		try {
			if (clId == null || clId.isEmpty()) {
				setClientMessage(respDTO, UserMsgConstants.INVALID_CLIENT, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (contId == null || contId.isEmpty()) {
				setClientMessage(respDTO, MessageConstant.INVALID_CONTEST_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (questionbnkIdStr == null || questionbnkIdStr.isEmpty()) {
				setClientMessage(respDTO, MessageConstant.INVALID_QESTION_BANK_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (userName == null || userName.isEmpty()) {
				setClientMessage(respDTO, UserMsgConstants.INVALID_ADMIN_USER_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}
			Integer qbnkId = null;
			try {
				qbnkId = Integer.parseInt(questionbnkIdStr);
			} catch (Exception e) {

			}
			ContestDVO contest = LiveContestService.getSQLContestByContestId(clId, contId);
			if (contest == null) {
				setClientMessage(respDTO, MessageConstant.INVALID_CONTEST_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}

			QuestionBankDVO questionbank = QuestionBankSQLDAO.getOLTXQuestionsFromQBankByquestionId(clId, qbnkId);
			if (questionbank == null) {
				setClientMessage(respDTO, MessageConstant.INVALID_QESTION_BANK_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}
			LivtContestQuestionDVO contQuestionDb = LivtContestQuestionsService.getcontestquestionsByContestIdandQuestionBankId(clId, contId, qbnkId);
			if (contQuestionDb != null) {
				setClientMessage(respDTO, MessageConstant.QUESTION_BANK_ID_ALREADY_EXISTS, null);
				generateResponse(request, response, respDTO);
				return;
			}

			if (!contest.getAnsType().equals(questionbank.getAnstype())) {
				setClientMessage(respDTO, MessageConstant.QUESTION_TYPE_MISMATCH_WITH_CONT_QUEST_TYPE, null);
				generateResponse(request, response, respDTO);
				return;
			}
			
			Integer count = LivtContestQuestionsService.getContestQuestionsCount(clId, contId);
			if(count == null){
				count = 0;
			}
			
			if (contest.getqSize() == count || contest.getqSize().equals(count)) {
				setClientMessage(respDTO, MessageConstant.CONTEST_QUESTION_SIZE_REACHED, null);
				generateResponse(request, response, respDTO);
				return;
			}
			

			LivtContestQuestionDVO contQuestion = new LivtContestQuestionDVO();
			contQuestion.setClintid(clId);
			contQuestion.setConid(contId);
			contQuestion.setQsnseqno(count+1);
			contQuestion.setQsntext(questionbank.getQtx());
			contQuestion.setOpa(questionbank.getOpa());
			contQuestion.setOpb(questionbank.getOpb());
			contQuestion.setOpc(questionbank.getOpc());
			contQuestion.setOpd(questionbank.getOpd());
			contQuestion.setCorans(questionbank.getCans());
			contQuestion.setQsnorient(questionbank.getqOrientn());
			contQuestion.setQbid(qbnkId);
			contQuestion.setCreby(userName);
			contQuestion.setCredate(new Date());
			contQuestion.setEncKey(RandomGenerator.getAlphaNumericString(6));

			Integer id = LivtContestQuestionsService.saveContestQuestions(clId, contQuestion);
			if (id == null) {
				setClientMessage(respDTO, MessageConstant.ERROR_WHILE_ADDING_QUESTION, null);
				generateResponse(request, response, respDTO);
				return;
			}

			respDTO.setSts(1);
			setClientMessage(respDTO, null, MessageConstant.QUESTION_ADDED_TO_CONTEST);
			generateResponse(request, response, respDTO);

		} catch (Exception e) {
			e.printStackTrace();

			setClientMessage(respDTO, UserMsgConstants.COMMON_ERR_MSG, null);
			generateResponse(request, response, respDTO);
			return;
		}

	}

}
