/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.ott.cass.dvo.ContestDVO;
import com.rtf.ott.dto.OTTQuestionDTO;
import com.rtf.ott.service.OTTQuestionBankService;
import com.rtf.ott.sql.dao.ContestSQLDAO;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class LivtGetAllQuestionBankForContestServlet.
 */
@WebServlet("/livtgetallconquesbank.json")
public class LivtGetAllQuestionBankForContestServlet extends RtfSaasBaseServlet {

	private static final long serialVersionUID = -3339929108348675051L;

	/**
	 * Generate response.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param rtfSaasBaseDTO
	 *            the rtf saas base DTO
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * Process request.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String clId = request.getParameter("clId");
		String contId = request.getParameter("coId");
		String filter = request.getParameter("hfilter");
		String pgNo = request.getParameter("pgNo");
		String userName = request.getParameter("cau");

		OTTQuestionDTO respDTO = new OTTQuestionDTO();
		respDTO.setSts(0);
		respDTO.setClId(clId);
		respDTO.setCoId(contId);

		try {
			if (clId == null || clId.isEmpty()) {
				setClientMessage(respDTO, "Invalid client id.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (contId == null || contId.isEmpty()) {
				setClientMessage(respDTO, "Invalid Contest Id.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (pgNo == null || pgNo.isEmpty()) {
				setClientMessage(respDTO, "Invalid page no.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (userName == null || userName.isEmpty()) {
				setClientMessage(respDTO, "Invalid User Name.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			ContestDVO contest = ContestSQLDAO.getContestByContestId(clId, contId);
			if (contest == null) {
				setClientMessage(respDTO, "Contest Id not Exists", null);
				generateResponse(request, response, respDTO);
				return;
			}
			respDTO = OTTQuestionBankService.fetchOTTQuestionBankListforContestId(respDTO, filter, contest.getCat(),
					contest.getSubCat(), pgNo);
			if (respDTO.getSts() == 0) {
				if (respDTO.getMsg() == null || respDTO.getMsg().isEmpty()) {
					setClientMessage(respDTO, null, null);
				}
			}

			generateResponse(request, response, respDTO);
			return;
		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(respDTO, null, null);
			generateResponse(request, response, respDTO);
		}
		return;
	}

}
