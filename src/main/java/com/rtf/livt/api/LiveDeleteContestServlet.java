/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.livt.dto.LiveContestDTO;
import com.rtf.livt.dvo.ContestDVO;
import com.rtf.livt.firestore.FireStoreReferenceHolder;
import com.rtf.livt.firestore.LiveFirestore;
import com.rtf.livt.service.LiveContestService;
import com.rtf.livt.util.MessageConstant;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class LiveDeleteContestServlet.
 */
@WebServlet("/livedeletecontests.json")
public class LiveDeleteContestServlet extends RtfSaasBaseServlet {

	private static final long serialVersionUID = 6437921408633997911L;

	/**
	 * Generate response.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param rtfSaasBaseDTO
	 *            the rtf saas base DTO
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * Process request.Method used to Delete a contest
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String clId = request.getParameter("clId");
		String cau = request.getParameter("cau");
		String coId = request.getParameter("coId");
		LiveContestDTO respDTO = new LiveContestDTO();
		respDTO.setSts(0);
		respDTO.setClId(clId);

		try {
			if (clId == null || clId.isEmpty()) {
				setClientMessage(respDTO,MessageConstant.INVALID_CLIENT, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (coId == null || coId.isEmpty()) {
				setClientMessage(respDTO, MessageConstant.INVALID_CONTEST_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}
			ContestDVO contest = LiveContestService.getSQLContestByContestId(clId, coId);
			if (contest == null) {
				setClientMessage(respDTO, MessageConstant.INVALID_CONTEST_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}
			boolean isDeleted = LiveContestService.deleteContest(clId, coId, cau);
			if (isDeleted) {
				LiveContestService.saveContestAudit(contest);
				List<LiveFirestore> firestores = FireStoreReferenceHolder.getReference(clId);
				if (firestores == null || firestores.isEmpty()) {
					setClientMessage(respDTO, MessageConstant.ERR_NO_FIRESTORE, null);
					generateResponse(request, response, respDTO);
					return;
				}
				for (LiveFirestore firestore : firestores) {
					firestore.updateUpcomingContestDetails();
				}
				respDTO.setSts(1);
				respDTO.setMsg(MessageConstant.PROCESS_CONTEST_REM_SUCC);
				generateResponse(request, response, respDTO);
				return;
			} else {
				setClientMessage(respDTO, MessageConstant.CON_REM_ERR, null);
				generateResponse(request, response, respDTO);
				return;
			}
		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(respDTO, null, null);
			generateResponse(request, response, respDTO);
		}
		return;
	}
}
