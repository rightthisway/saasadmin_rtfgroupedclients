/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.livt.dto.LivePlayContestDTO;
import com.rtf.livt.dvo.ContestDVO;
import com.rtf.livt.dvo.LivtContestQuestionDVO;
import com.rtf.livt.service.LiveContestService;
import com.rtf.livt.service.LivtContestQuestionsService;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class LiveVerifyContestDataServlet.
 */
@WebServlet("/liveverifycontestdata.json")
public class LiveVerifyContestDataServlet extends RtfSaasBaseServlet {

	private static final long serialVersionUID = 2635313495318033388L;

	/**
	 * Generate response.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param rtfSaasBaseDTO
	 *            the rtf saas base DTO
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}

	/**
	 * Process request.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String clId = request.getParameter("clId");
		String coId = request.getParameter("coId");
		LivePlayContestDTO respDTO = new LivePlayContestDTO();
		respDTO.setSts(0);
		respDTO.setClId(clId);

		try {
			if (clId == null || clId.isEmpty()) {
				setClientMessage(respDTO, "Invalid client id.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (coId == null || coId.isEmpty()) {
				setClientMessage(respDTO, "Invalid contest id.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			ContestDVO contest = LiveContestService.getSQLContestByContestId(clId, coId);
			if (contest == null) {
				setClientMessage(respDTO, "Contest not found in system.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			List<LivtContestQuestionDVO> questions = LivtContestQuestionsService.getcontestquestionsBycontestId(clId,
					contest.getCoId());
			if (questions == null) {
				questions = new ArrayList<LivtContestQuestionDVO>();
			}
			if (contest.getqSize() > 0 && questions.isEmpty()) {
				setClientMessage(respDTO, "WARNING : Questions are not added to contest.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			Integer questionCount = questions.size();
			if (questionCount != contest.getqSize() || !questionCount.equals(contest.getqSize())) {
				setClientMessage(respDTO,
						"WARNING : No. of question declared at contest level and actual questions in contest are mismatching.",
						null);
				generateResponse(request, response, respDTO);
				return;
			}

			for (LivtContestQuestionDVO que : questions) {
				if (contest.getAnsType().equalsIgnoreCase("REGULAR")
						&& (que.getCorans() == null || que.getCorans().isEmpty())) {
					setClientMessage(respDTO,
							"WARNING : Question type REGULAR is configured at contest level and there are one or more FEEDBACK type(without answer) of questions in contest.",
							null);
					generateResponse(request, response, respDTO);
					return;
				} else if (contest.getAnsType().equalsIgnoreCase("FEEDBACK")
						&& (que.getCorans() != null && !que.getCorans().isEmpty())) {
					setClientMessage(respDTO,
							"WARNING : Question type FEEDBACK configured at contest level and there are one or more REGULAR type(with answer) of questions in contest.",
							null);
					generateResponse(request, response, respDTO);
					return;
				}
			}
			respDTO.setContest(contest);
			respDTO.setMsg("Contest configurations are verified, Contest is ready to play on scheduled date and time.");
			respDTO.setSts(1);
			generateResponse(request, response, respDTO);
			return;
		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(respDTO, null, null);
			generateResponse(request, response, respDTO);
		}
		return;
	}
}
