package com.rtf.livt.api;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.aws.AWSFileService;
import com.rtf.aws.AwsS3Response;
import com.rtf.common.util.SaaSAdminAppCache;
import com.rtf.livt.dto.LiveContestDTO;
import com.rtf.livt.dvo.ContestDVO;
import com.rtf.livt.service.LiveContestService;
import com.rtf.livt.util.MessageConstant;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;

@WebServlet("/dwnldliveconscript.json")
public class LiveDownloadContestScriptServlet  extends RtfSaasBaseServlet{

	/**
	 * Serial version id
	 */
	private static final long serialVersionUID = -7462423096717245194L;

	/**
	 * Generate File response.
	 *
	 * @param response the response
	 * @param workbook the workbook
	 * @throws Exception the exception
	 */
	public static void generateFileResponse(HttpServletResponse response, File file) throws Exception {
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		response.setContentType("APPLICATION/OCTET-STREAM");   
		//response.setHeader("Content-Type", "APPLICATION/OCTET-STREAM");
		response.setHeader("Content-Disposition","attachment; filename=\"" + file.getName() + "\"");   
		ServletOutputStream out = response.getOutputStream();
		FileInputStream fileInputStream = new FileInputStream(file);  
		int i;   
		while ((i=fileInputStream.read()) != -1) {  
			out.write(i);   
		}   
		fileInputStream.close();   
		out.close();   
	}

	/**
	 * Generate Http Response for contest Products Response data is sent in JSON format.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}
	
	
	
	
	/**
	 * download contest script file to S3 and write it to response.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
		String clId = request.getParameter("clId");
		String coId = request.getParameter("coId");
		String userId = request.getParameter("cau");
		
		LiveContestDTO dto = new LiveContestDTO();
		dto.setSts(0);
		dto.setClId(clId);
		
		try {
			if (GenUtil.isNullOrEmpty(clId)) {
				setClientMessage(dto, MessageConstant.INVALID_CLIENT, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(userId)) {
				setClientMessage(dto, MessageConstant.INVALID_USER, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(coId)) {
				setClientMessage(dto, MessageConstant.INVALID_CONTEST_ID, null);
				generateResponse(request, response, dto);
				return;
			}
			
			ContestDVO contest = LiveContestService.getSQLContestByContestId(clId, coId);
			if (contest == null) {
				setClientMessage(dto, MessageConstant.INVALID_CONTEST_ID, null);
				generateResponse(request, response, dto);
				return;
			}
			
			System.out.println("SCRIPT FILE : "+contest.getScrptFile());
			
			if(contest.getScrptFile()==null || contest.getScrptFile().isEmpty()){
				setClientMessage(dto, MessageConstant.CONTEST_SCRIPT_FILE_DOES_NOT_EXIST, null);
				generateResponse(request, response, dto);
				return;
			}
			
			String s3bucket = SaaSAdminAppCache.s3SecuredBucket;
			String scriptFolder = SaaSAdminAppCache.envfoldertype + "/" + SaaSAdminAppCache.s3ContestScriptFolder+ "/" + clId;
			
			AwsS3Response awsRsponse = AWSFileService.downLoadFileFromS3(s3bucket, contest.getScrptFile(), scriptFolder);
			if (null != awsRsponse && awsRsponse.getStatus() != 1) {
				setClientMessage(dto, null, null);
				generateResponse(request, response, dto);
				return;
			}
			File file = new File(awsRsponse.getFullFilePath());
			if(file.exists()){
				dto.setSts(1);
				generateFileResponse(response, file);
				return;
			}
			setClientMessage(dto, null, null);
			generateResponse(request, response, dto);
			return;
		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(dto, null, null);
			generateResponse(request, response, dto);
		}
	}  
}
