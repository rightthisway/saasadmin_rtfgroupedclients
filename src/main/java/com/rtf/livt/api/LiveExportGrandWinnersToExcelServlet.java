/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import com.rtf.livt.dvo.ContestGrandWinnerDVO;
import com.rtf.livt.sql.dao.LivtContestGrandWinnerDAO;
import com.rtf.reports.utils.ExcelUtil;
import com.rtf.reports.utils.ReportConstants;
import com.rtf.reports.utils.ReportsUtil;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class LiveExportGrandWinnersToExcelServlet.
 */
@WebServlet("/livegetgrandwinexcel.json")
public class LiveExportGrandWinnersToExcelServlet extends RtfSaasBaseServlet {

	private static final long serialVersionUID = -7513543813116281200L;

	/**
	 * Generate response.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param rtfSaasBaseDTO
	 *            the rtf saas base DTO
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * Generate excel response.
	 *
	 * @param response
	 *            the response
	 * @param workbook
	 *            the workbook
	 * @throws Exception
	 *             the exception
	 */
	public static void generateExcelResponse(HttpServletResponse response, SXSSFWorkbook workbook) throws Exception {
		response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=Lottery_Winners." + ReportsUtil.EXCEL_EXTENSION);
		workbook.write(response.getOutputStream());
		workbook.close();
	}

	/**
	 * Process request.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String clId = request.getParameter("clId");
		String cau = request.getParameter("cau");
		String coId = request.getParameter("coId");

		int startRowCnt = 0;
		RtfSaasBaseDTO respDTO = new RtfSaasBaseDTO();
		respDTO.setSts(0);

		String msg = null;
		try {
			if (clId == null || clId.isEmpty()) {
				msg = "Invalid client id.";
				setClientMessage(respDTO, msg, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (coId == null || coId.isEmpty()) {
				msg = "Invalid contest id.";
				setClientMessage(respDTO, msg, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (cau == null || cau.isEmpty()) {
				msg = "Invalid loggedin user.";
				setClientMessage(respDTO, msg, null);
				generateResponse(request, response, respDTO);
				return;
			}

			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet = workbook.createSheet("Categories");

			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));

			List<String> headerList = new ArrayList<String>();
			headerList = new ArrayList<String>();
			headerList = new ArrayList<String>();
			headerList.add("Client ID");
			headerList.add("Contest ID");
			headerList.add("Customer First Name");
			headerList.add("Customer Last Name");
			headerList.add("Customer User ID");
			headerList.add("Customer Email");
			headerList.add("Customer Phone");
			headerList.add("Reward Type");
			headerList.add("Reward Value");
			ExcelUtil.generateExcelHeaderRowWithStyles(headerList, ssSheet, workbook, startRowCnt);

			List<ContestGrandWinnerDVO> gwList = LivtContestGrandWinnerDAO.getAllWinnersByCoId(clId, coId);
			if (gwList == null || gwList.isEmpty()) {
				ExcelUtil.generateErrorMessageExcel(ssSheet, workbook, startRowCnt++, ReportConstants.NO_RECS_FOUND);
				generateExcelResponse(response, workbook);
				return;
			}

			int j = 0;
			int rowCount = startRowCnt + 1;
			for (ContestGrandWinnerDVO dvo : gwList) {
				Row rowhead = ssSheet.createRow((int) rowCount);
				rowCount++;
				j = 0;
				ReportsUtil.getExcelStringCell(dvo.getClId(), j, rowhead);
				j++;
				ReportsUtil.getExcelStringCell(dvo.getCoId(), j, rowhead);
				j++;
				ReportsUtil.getExcelStringCell(dvo.getfName(), j, rowhead);
				j++;
				ReportsUtil.getExcelStringCell(dvo.getlName(), j, rowhead);
				j++;
				ReportsUtil.getExcelStringCell(dvo.getuId(), j, rowhead);
				j++;
				ReportsUtil.getExcelStringCell(dvo.getCuId(), j, rowhead);
				j++;
				ReportsUtil.getExcelStringCell(dvo.getPhone(), j, rowhead);
				j++;
				ReportsUtil.getExcelStringCell(dvo.getRwdType(), j, rowhead);
				j++;
				ReportsUtil.getExcelDecimalCell(dvo.getRwdVal(), j, rowhead);
				j++;

			}
			generateExcelResponse(response, workbook);
			return;
		} catch (Exception e) {
			msg = "Error occured while getting contest winner.";
			e.printStackTrace();
			setClientMessage(respDTO, null, null);
			generateResponse(request, response, respDTO);
		} finally {

		}
		return;
	}
}
