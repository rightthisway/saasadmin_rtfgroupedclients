/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.livt.constants.LiveContestConst;
import com.rtf.livt.constants.LiveContestType;
import com.rtf.livt.dto.LivePlayContestDTO;
import com.rtf.livt.dvo.ContestDVO;
import com.rtf.livt.dvo.LiveContestAnswerCountDVO;
import com.rtf.livt.dvo.LivtContestQuestionDVO;
import com.rtf.livt.enums.LivePlayContest;
import com.rtf.livt.firestore.FireStoreReferenceHolder;
import com.rtf.livt.firestore.LiveFirestore;
import com.rtf.livt.service.LiveContestAnswerService;
import com.rtf.livt.service.LiveContestService;
import com.rtf.livt.service.LivtContestQuestionsService;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.service.SaaSApiTrackingService;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class LiveGetAnswerServlet.
 */
@WebServlet("/livegetquestionanswer.json")
public class LiveGetAnswerServlet extends RtfSaasBaseServlet {

	private static final long serialVersionUID = -6356656558452130935L;

	/**
	 * Generate response.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param rtfSaasBaseDTO
	 *            the rtf saas base DTO
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * Process request.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String clId = request.getParameter("clId");
		String cau = request.getParameter("cau");
		String coId = request.getParameter("coId");
		LivePlayContestDTO respDTO = new LivePlayContestDTO();
		respDTO.setSts(0);
		respDTO.setClId(clId);

		String msg = null;
		LivtContestQuestionDVO question = null;

		try {
			if (clId == null || clId.isEmpty()) {
				msg = "Invalid client id.";
				setClientMessage(respDTO, msg, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (coId == null || coId.isEmpty()) {
				msg = "Invalid contest id.";
				setClientMessage(respDTO, msg, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (cau == null || cau.isEmpty()) {
				msg = "Invalid loggedin user.";
				setClientMessage(respDTO, msg, null);
				generateResponse(request, response, respDTO);
				return;
			}
			ContestDVO contest = LiveContestService.getSQLContestByContestId(clId, coId);
			if (contest == null) {
				msg = "Contest not found with given identifier.";
				setClientMessage(respDTO, msg, null);
				generateResponse(request, response, respDTO);
				return;
			}
			int seqNo = contest.getLastQue();
			question = LivtContestQuestionsService.getcontestquestionsByContestIdandQuestionSeqNo(contest.getClId(),
					contest.getCoId(), seqNo);
			if (question == null) {
				msg = "Question not found in system.";
				setClientMessage(respDTO, msg, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (contest.getAnsType().equalsIgnoreCase("FEEDBACK")) {
				question.setCorans("ALL");
			}

			LivtContestQuestionDVO nxtQuestion = LivtContestQuestionsService
					.getcontestquestionsByContestIdandQuestionSeqNo(contest.getClId(), contest.getCoId(), seqNo + 1);

			contest.setUpBy(cau);
			contest.setLastAction(LivePlayContest.ANSWER.toString());
			contest.setLastQue(seqNo);

			Boolean isUpdated = LiveContestService.updateContestState(contest);
			LiveContestAnswerCountDVO answerCount = LiveContestAnswerService.getContestQuestionAnswerCount(
					contest.getClId(), contest.getCoId(), question.getConqsnid(), question.getQsnseqno());
			answerCount.setRwdVal(question.getAnsRwdVal());

			if (isUpdated) {
				List<LiveFirestore> firestores = FireStoreReferenceHolder.getReference(clId);
				if (firestores == null || firestores.isEmpty()) {
					setClientMessage(respDTO, "Firestore configuration settings not found in db.", null);
					generateResponse(request, response, respDTO);
					return;
				}
				Integer ansCount = null;
				if (question.getCorans().equalsIgnoreCase("A")) {
					ansCount = answerCount.getOptACount();
				} else if (question.getCorans().equalsIgnoreCase("B")) {
					ansCount = answerCount.getOptBCount();
				} else if (question.getCorans().equalsIgnoreCase("C")) {
					ansCount = answerCount.getOptCCount();
				} else if (question.getCorans().equalsIgnoreCase("D")) {
					ansCount = answerCount.getOptDCount();
				}
				for (LiveFirestore firestore : firestores) {
					firestore.setAnswerCount(answerCount);
					firestore.updateQuestion(question);
					firestore.updateHostNode(LivePlayContest.ANSWER, question.getCorans(), ansCount,
							question.getQsnseqno(), null, null, null);
				}
				if (seqNo == contest.getqSize()) {
					respDTO.setBtnText(LiveContestConst.SUMMARY_BTN);
					respDTO.setNxtApi(LiveContestConst.CONTEST_SIMMARY_API);
				} else {
					if (contest.getCoType().equalsIgnoreCase(LiveContestType.MINI.toString())) {
						if (question.getMjRwdType() != null && question.getMjRwdVal() != null
								&& question.getMjWinnersCount() > 0) {
							respDTO.setBtnText(LiveContestConst.MINIJACKPOT_BTN);
							respDTO.setNxtApi(LiveContestConst.CONTEST_MINIJACKPOT_API);
						}
					} else if (contest.getCoType().equalsIgnoreCase(LiveContestType.MEGA.toString())
							&& ((seqNo + 1) == contest.getqSize())) {
						if (question.getMjRwdType() != null && question.getMjRwdVal() != null
								&& question.getMjWinnersCount() > 0) {
							respDTO.setBtnText(LiveContestConst.MEGAJACKPOT_BTN);
							respDTO.setNxtApi(LiveContestConst.CONTEST_MEGAJACKPOT_API);
						}
					} else {
						respDTO.setBtnText(LiveContestConst.QUESTION_BTN + " - " + (seqNo + 1));
						respDTO.setNxtApi(LiveContestConst.CONTEST_QUESTION_API);
					}

				}
				respDTO.setQuestion(question);
				respDTO.setNxtQuestion(nxtQuestion);
				respDTO.setSts(1);
				msg = "Answer Invoked.";
				generateResponse(request, response, respDTO);
				return;
			} else {
				msg = "Error occured while getting question answer.";
				setClientMessage(respDTO, msg, null);
				generateResponse(request, response, respDTO);
				return;
			}
		} catch (Exception e) {
			msg = "Error occured while getting question answer.";
			e.printStackTrace();
			setClientMessage(respDTO, null, null);
			generateResponse(request, response, respDTO);
		} finally {
			SaaSApiTrackingService.saveContestTracking(clId, coId, LiveContestConst.CONTEST_ANSWER_API, cau, "WEB",
					request.getRemoteAddr(), LivePlayContest.ANSWER.toString(), msg, respDTO.getSts(),
					question != null ? question.getQsnseqno() : 0);
		}
		return;
	}

}
