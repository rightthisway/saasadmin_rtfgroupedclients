/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import com.rtf.livt.dvo.ContestDVO;
import com.rtf.livt.dvo.LivtContestQuestionDVO;
import com.rtf.livt.service.LiveContestService;
import com.rtf.livt.service.LivtContestQuestionsService;
import com.rtf.reports.utils.ExcelUtil;
import com.rtf.reports.utils.ReportConstants;
import com.rtf.reports.utils.ReportsUtil;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class LiveExportContestQuestionToExcelServlet.
 */
@WebServlet("/livegetconqueexcel.json")
public class LiveExportContestQuestionToExcelServlet extends RtfSaasBaseServlet {

	private static final long serialVersionUID = -755834681329093649L;

	/**
	 * Generate response.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param rtfSaasBaseDTO
	 *            the rtf saas base DTO
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * Generate excel response.
	 *
	 * @param response
	 *            the response
	 * @param workbook
	 *            the workbook
	 * @throws Exception
	 *             the exception
	 */
	public static void generateExcelResponse(HttpServletResponse response, SXSSFWorkbook workbook) throws Exception {
		response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=Contest_Questions." + ReportsUtil.EXCEL_EXTENSION);
		workbook.write(response.getOutputStream());
		workbook.close();
	}

	/**
	 * Process request.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String clId = request.getParameter("clId");
		String coId = request.getParameter("coId");
		String filter = request.getParameter("hfilter");
		String userName = request.getParameter("cau");

		int startRowCnt = 0;
		RtfSaasBaseDTO respDTO = new RtfSaasBaseDTO();
		respDTO.setSts(0);

		SXSSFWorkbook workbook = new SXSSFWorkbook();
		Sheet ssSheet = workbook.createSheet("Contest Questions");

		try {
			if (clId == null || clId.isEmpty()) {
				setClientMessage(respDTO, "Invalid client id.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (coId == null || coId.isEmpty()) {
				setClientMessage(respDTO, "Invalid contest id.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (userName == null || userName.isEmpty()) {
				setClientMessage(respDTO, "Invalid logged in user details.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			ContestDVO contest = LiveContestService.getSQLContestByContestId(clId, coId);
			if (contest == null) {
				setClientMessage(respDTO, "Contest id not exists", null);
				generateResponse(request, response, respDTO);
				return;
			}

			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));

			List<String> headerList = new ArrayList<String>();
			headerList = new ArrayList<String>();
			headerList.add("Client ID");
			headerList.add("Contest ID");
			headerList.add("Question ID");
			headerList.add("Question Text");
			headerList.add("Option A");
			headerList.add("Option B");
			headerList.add("Option C");
			headerList.add("Option D");
			headerList.add("Correct Answer");
			headerList.add("Sequence No.");
			headerList.add("Difficulty Level");
			headerList.add("Display Orientation");
			headerList.add("Answer Reward Value");
			headerList.add("Jackpot Reward Type");
			headerList.add("Jackpot Reward Value");
			headerList.add("Number Of Jackpot Winners");
			headerList.add("Created By");
			headerList.add("Created Date");
			headerList.add("Updated By");
			headerList.add("Updated Date");
			ExcelUtil.generateExcelHeaderRowWithStyles(headerList, ssSheet, workbook, startRowCnt);

			List<LivtContestQuestionDVO> questions = LivtContestQuestionsService
					.getAllContestQuestionsDataToExport(clId, coId, filter);
			if (questions == null || questions.isEmpty()) {
				ExcelUtil.generateErrorMessageExcel(ssSheet, workbook, startRowCnt++, ReportConstants.NO_RECS_FOUND);
				generateExcelResponse(response, workbook);
				return;
			}

			int j = 0;
			int rowCount = startRowCnt + 1;
			for (LivtContestQuestionDVO dvo : questions) {
				Row rowhead = ssSheet.createRow((int) rowCount);
				rowCount++;
				j = 0;
				ReportsUtil.getExcelStringCell(dvo.getClintid(), j, rowhead);
				j++;
				ReportsUtil.getExcelStringCell(dvo.getConid(), j, rowhead);
				j++;
				ReportsUtil.getExcelIntegerCell(dvo.getConqsnid(), j, rowhead);
				j++;
				ReportsUtil.getExcelStringCell(dvo.getQsntext(), j, rowhead);
				j++;
				ReportsUtil.getExcelStringCell(dvo.getOpa(), j, rowhead);
				j++;
				ReportsUtil.getExcelStringCell(dvo.getOpb(), j, rowhead);
				j++;
				ReportsUtil.getExcelStringCell(dvo.getOpc(), j, rowhead);
				j++;
				ReportsUtil.getExcelStringCell(dvo.getOpd(), j, rowhead);
				j++;
				ReportsUtil.getExcelStringCell(dvo.getCorans(), j, rowhead);
				j++;
				ReportsUtil.getExcelIntegerCell(dvo.getQsnseqno(), j, rowhead);
				j++;
				ReportsUtil.getExcelStringCell(dvo.getqDifLevel(), j, rowhead);
				j++;
				ReportsUtil.getExcelStringCell(dvo.getQsnorient(), j, rowhead);
				j++;
				ReportsUtil.getExcelDecimalCell(dvo.getAnsRwdVal(), j, rowhead);
				j++;
				ReportsUtil.getExcelStringCell(dvo.getMjRwdType(), j, rowhead);
				j++;
				ReportsUtil.getExcelDecimalCell(dvo.getMjRwdVal(), j, rowhead);
				j++;
				ReportsUtil.getExcelIntegerCell(dvo.getMjWinnersCount(), j, rowhead);
				j++;
				ReportsUtil.getExcelStringCell(dvo.getCreby(), j, rowhead);
				j++;
				ReportsUtil.getExcelDateCell(dvo.getCredateStr(), j, rowhead, cellStyleWithHourMinute);
				j++;
				ReportsUtil.getExcelStringCell(dvo.getUpdby(), j, rowhead);
				j++;
				ReportsUtil.getExcelDateCell(dvo.getUpddateStr(), j, rowhead, cellStyleWithHourMinute);
				j++;
			}

			generateExcelResponse(response, workbook);
			return;
		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(respDTO, "something went wrong please try again sometime later", null);
			generateResponse(request, response, respDTO);
		}
		return;
	}

}
