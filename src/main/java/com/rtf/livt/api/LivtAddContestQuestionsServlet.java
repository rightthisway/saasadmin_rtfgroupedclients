/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.common.util.RandomGenerator;
import com.rtf.livt.dto.LivtContestQuestionsDTO;
import com.rtf.livt.dvo.ContestDVO;
import com.rtf.livt.dvo.LivtContestQuestionDVO;
import com.rtf.livt.service.LiveContestService;
import com.rtf.livt.service.LivtContestQuestionsService;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class LivtAddContestQuestionsServlet.
 */
@WebServlet("/livtAddContQuest.json")
public class LivtAddContestQuestionsServlet extends RtfSaasBaseServlet {

	private static final long serialVersionUID = -8009763769651968464L;

	/**
	 * Generate response.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param rtfSaasBaseDTO
	 *            the rtf saas base DTO
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}

	/**
	 * Process request.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String clId = request.getParameter("clId");
		String contId = request.getParameter("coId");
		String qsttext = request.getParameter("qstText");
		String optA = request.getParameter("optA");
		String optB = request.getParameter("optB");
		String optC = request.getParameter("optC");
		String optD = request.getParameter("optD");
		String answer = request.getParameter("ans");
		String qsnOrient = request.getParameter("orient");
		String ansRwdValStr = request.getParameter("ansrwdval");
		String miniJackpotType = request.getParameter("mjRwdType");
		String mjWinners = request.getParameter("mjwcnt");
		String winnerRewards = request.getParameter("mjRwdVal");
		String qDifficultyLevel = request.getParameter("qdifLevel");
		String userName = request.getParameter("cau");
		String couponCode = request.getParameter("coupcde");
		String discounttype = request.getParameter("disctype");

		LivtContestQuestionsDTO respDTO = new LivtContestQuestionsDTO();
		respDTO.setSts(0);
		respDTO.setClId(clId);

		try {
			if (clId == null || clId.isEmpty()) {
				setClientMessage(respDTO, "Invalid Client id.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (contId == null || contId.isEmpty()) {
				setClientMessage(respDTO, "Invalid Contest id.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (userName == null || userName.isEmpty()) {
				setClientMessage(respDTO, "Invalid User Name.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (qsttext == null || qsttext.isEmpty()) {
				setClientMessage(respDTO, "Invalid Question text.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (optA == null || optA.isEmpty()) {
				setClientMessage(respDTO, "Invalid Option A.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (optB == null || optB.isEmpty()) {
				setClientMessage(respDTO, "Invalid Option B.", null);
				generateResponse(request, response, respDTO);
				return;
			}

			if (qsnOrient == null || qsnOrient.isEmpty()) {
				setClientMessage(respDTO, "Invalid Question Orient", null);
				generateResponse(request, response, respDTO);
				return;
			}

			ContestDVO contest = LiveContestService.getSQLContestByContestId(clId, contId);
			if (contest == null) {
				setClientMessage(respDTO, "Contest id not exists", null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (contest.getAnsType().equals("FEEDBACK")) {
				if (answer != null) {
					setClientMessage(respDTO, "Do not select answer for Feedback contest questions.", null);
					generateResponse(request, response, respDTO);
					return;
				}
			} else {
				if (answer == null || answer.isEmpty()) {
					setClientMessage(respDTO, "Invalid Answer.", null);
					generateResponse(request, response, respDTO);
					return;
				}
				if (answer.equals("C") || answer.equals("D")) {
					if (optC == null || optC.isEmpty()) {
						setClientMessage(respDTO, "Invalid Option C.", null);
						generateResponse(request, response, respDTO);
						return;
					}
				}
				if (answer.equals("D")) {
					if (optD == null || optD.isEmpty()) {
						setClientMessage(respDTO, "Invalid Option D.", null);
						generateResponse(request, response, respDTO);
						return;
					}
				}
			}
			if (qDifficultyLevel == null || qDifficultyLevel.isEmpty()) {
				setClientMessage(respDTO, "Invalid Question Difficulty Level", null);
				generateResponse(request, response, respDTO);
				return;
			}

			Double ansRewardVal = 0.0;
			if (contest.getQueRwdType() != null && !contest.getQueRwdType().isEmpty()) {
				try {
					ansRewardVal = Double.parseDouble(ansRwdValStr);
				} catch (Exception e) {
					setClientMessage(respDTO, "Invalid Answer Rewards", null);
					generateResponse(request, response, respDTO);
					return;
				}
			} else {
				if (ansRwdValStr != null && !ansRwdValStr.isEmpty()) {
					setClientMessage(respDTO,
							"Do not select answer reward values for None answer Reward Type contests.", null);
					generateResponse(request, response, respDTO);
					return;
				}
			}

			Integer mjWinnersCount = 0;
			Double mjRewardValue = 0.0;
			if (contest.getCoType().equals("MEGA")) {
				if (miniJackpotType == null || miniJackpotType.isEmpty()) {
					setClientMessage(respDTO, "Select Jackpot Type for Mega Jackpot Type", null);
					generateResponse(request, response, respDTO);
					return;
				}
				try {
					mjWinnersCount = Integer.parseInt(mjWinners);
				} catch (Exception e) {
					setClientMessage(respDTO, "Invalid Jackpot Winners Count", null);
					generateResponse(request, response, respDTO);
					return;
				}

				try {
					mjRewardValue = Double.parseDouble(winnerRewards);
				} catch (Exception e) {
					setClientMessage(respDTO, "Invalid Jackpot Reward Value", null);
					generateResponse(request, response, respDTO);
					return;
				}
			} else if (contest.getCoType().equals("MINI")) {
				if (miniJackpotType != null && !miniJackpotType.isEmpty()) {
					try {
						mjWinnersCount = Integer.parseInt(mjWinners);
					} catch (Exception e) {
						setClientMessage(respDTO, "Invalid Jackpot Winners Count", null);
						generateResponse(request, response, respDTO);
						return;
					}

					try {
						mjRewardValue = Double.parseDouble(winnerRewards);
					} catch (Exception e) {
						setClientMessage(respDTO, "Invalid Jackpot Reward Value", null);
						generateResponse(request, response, respDTO);
						return;
					}
				}
			} else {
				if (miniJackpotType != null && !miniJackpotType.isEmpty()) {
					setClientMessage(respDTO, "Do not select jackpot type for Regular Contest", null);
					generateResponse(request, response, respDTO);
					return;
				}
				if (mjWinners != null && !mjWinners.isEmpty()) {
					setClientMessage(respDTO, "Do not select jackpot Winners Count for Regular Contest", null);
					generateResponse(request, response, respDTO);
					return;
				}
				if (winnerRewards != null && !mjWinners.isEmpty()) {
					setClientMessage(respDTO, "Do not select Jackpot Reward Value for Regular Contest", null);
					generateResponse(request, response, respDTO);
					return;
				}
			}

			int qsize = contest.getqSize();
			Integer qestionsCount = LivtContestQuestionsService.getContestQuestionsCount(clId, contId);
			if (qestionsCount != null && qestionsCount >= qsize) {
				setClientMessage(respDTO, "Already " + qestionsCount + " questions created for this contest", null);
				generateResponse(request, response, respDTO);
				return;
			}

			LivtContestQuestionDVO contQuestion = new LivtContestQuestionDVO();
			contQuestion.setClintid(clId);
			contQuestion.setConid(contId);
			contQuestion.setQsntext(qsttext);
			contQuestion.setOpa(optA);
			contQuestion.setOpb(optB);
			contQuestion.setOpc(optC);
			contQuestion.setOpd(optD);
			contQuestion.setCorans(answer);
			contQuestion.setQsnorient(qsnOrient);
			contQuestion.setCreby(userName);
			contQuestion.setCredate(new Date());
			contQuestion.setQsnseqno((qestionsCount + 1));
			contQuestion.setMjRwdType(miniJackpotType);
			contQuestion.setMjRwdVal(mjRewardValue);
			contQuestion.setMjWinnersCount(mjWinnersCount);
			contQuestion.setAnsRwdVal(ansRewardVal);
			contQuestion.setqDifLevel(qDifficultyLevel);
			contQuestion.setEncKey(RandomGenerator.getAlphaNumericString(6));

			if ("DISCOUNT CODE".equalsIgnoreCase(contest.getQueRwdType())) {
				if (discounttype == null || discounttype.isEmpty()) {
					setClientMessage(respDTO, "Please Select discount type for Coupon", null);
					generateResponse(request, response, respDTO);
					return;
				}
				if (couponCode == null || couponCode.isEmpty()) {
					setClientMessage(respDTO, " Coupon Code is Mandatory", null);
					generateResponse(request, response, respDTO);
					return;
				}
				contQuestion.setCoupcde(couponCode);
				contQuestion.setCoupondiscType(discounttype);
			}

			Integer id = LivtContestQuestionsService.saveContestQuestions(clId, contQuestion);
			if (id == null) {
				setClientMessage(respDTO, "Contest questions not created.", null);
				generateResponse(request, response, respDTO);
				return;
			}

			respDTO.setSts(1);
			setClientMessage(respDTO, null, "Question Created successfully.");
			generateResponse(request, response, respDTO);

		} catch (Exception e) {
			e.printStackTrace();

			setClientMessage(respDTO, "Something went wrong, please try again sometime later", null);
			generateResponse(request, response, respDTO);
			return;
		}

	}

}
