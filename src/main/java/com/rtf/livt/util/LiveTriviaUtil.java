/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.util;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.time.DateUtils;

import com.rtf.livt.dvo.ContestDVO;
import com.rtf.livt.service.LiveContestService;
import com.rtf.saas.util.DateFormatUtil;

/**
 * The Class LiveTriviaUtil.
 */
public class LiveTriviaUtil {

	/** The Constant MAX_PARTICIPANT_COUNT. */
	private static final Integer MAX_PARTICIPANT_COUNT = 2000000;

	/**
	 * Calculate expected participant count.
	 *
	 * @param date
	 *            the date
	 * @param hours
	 *            the hours
	 * @param minutes
	 *            the minutes
	 * @param expPartCnt
	 *            the exp part cnt
	 * @return the boolean
	 */
	public static Boolean calculateExpectedParticipantCount(String date, Integer hours, Integer minutes,
			Integer expPartCnt) {
		Boolean flag = false;
		Integer totalCount = 0;
		try {
			String stDate = date + " " + hours + ":" + minutes + ":00";
			Date sDate = DateFormatUtil.getDateWithTwentyFourHourFormat(stDate);
			Date eDate = DateUtils.addMinutes(sDate, 30);

			java.sql.Timestamp startDate = new java.sql.Timestamp(sDate.getTime());
			java.sql.Timestamp endDate = new java.sql.Timestamp(eDate.getTime());

			totalCount = totalCount + expPartCnt;
			List<ContestDVO> allContests = LiveContestService.getAllActiveContestByDateAndTime(startDate, endDate);
			for (ContestDVO co : allContests) {
				totalCount = totalCount + (co.getExpPartCount() != null ? co.getExpPartCount() : 0);
			}

			if (totalCount <= MAX_PARTICIPANT_COUNT) {
				flag = true;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}

	/**
	 * Gets the rounded value string.
	 *
	 * @param value
	 *            the value
	 * @return the rounded value string
	 * @throws Exception
	 *             the exception
	 */
	public static String getRoundedValueString(Double value) throws Exception {
		if (value == null) {
			return "";
		}
		Integer result = value.intValue();
		if(value > result){
			return String.format( "%.2f", value);
		}
		return String.format( "%.0f", value);
	}

	/**
	 * Gets the integer value rounded up string.
	 *
	 * @param value
	 *            the value
	 * @return the integer value rounded up string
	 * @throws Exception
	 *             the exception
	 */
	public static String getIntegerValueRoundedUpString(Double value) throws Exception {
		Integer result = value.intValue();
		if (value > result) {
			result = result + 1;
		}
		return String.valueOf(result);
	}

}
