/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.Socket;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509ExtendedTrustManager;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rtf.common.util.GsonCustomConfig;

/**
 * The Class HTTPUtil.
 */
public class HTTPUtil {

	/**
	 * Execute.
	 *
	 * @param dataMap
	 *            the data map
	 * @param url
	 *            the url
	 * @return the string
	 * @throws Exception
	 *             the exception
	 */
	public static String execute(Map<String, String> dataMap, String url) throws Exception {
		try {
			TrustManager[] trustAllCerts = new TrustManager[] { new X509ExtendedTrustManager() {
				@Override
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					return null;
				}

				@Override
				public void checkClientTrusted(X509Certificate[] certs, String authType) {
				}

				@Override
				public void checkServerTrusted(X509Certificate[] certs, String authType) {
				}

				@Override
				public void checkClientTrusted(X509Certificate[] xcs, String string, Socket socket)
						throws CertificateException {

				}

				@Override
				public void checkServerTrusted(X509Certificate[] xcs, String string, Socket socket)
						throws CertificateException {

				}

				@Override
				public void checkClientTrusted(X509Certificate[] xcs, String string, SSLEngine ssle)
						throws CertificateException {

				}

				@Override
				public void checkServerTrusted(X509Certificate[] xcs, String string, SSLEngine ssle)
						throws CertificateException {

				}

			} };

			SSLContext context = SSLContext.getInstance("TLSv1.2");
			context.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpClient httpClient = HttpClients.custom().setSslcontext(context).build();
			HttpPost post = new HttpPost(url);
			post.setHeader("Accept", "application/json");

			List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
			//StringEntity requestEntity = new StringEntity( JSON_STRING,ContentType.APPLICATION_JSON);
			for (Map.Entry<String, String> entry : dataMap.entrySet()) {
				urlParameters.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
			}

			post.setEntity(new UrlEncodedFormEntity(urlParameters, "UTF-8"));

			HttpResponse response = httpClient.execute(post);

			BufferedReader responseData = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

			String result = org.apache.commons.io.IOUtils.toString(responseData);
			return result;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	
	/**
	 * Execute.
	 *
	 * @param dataMap
	 *            the data map
	 * @param url
	 *            the url
	 * @return the string
	 * @throws Exception
	 *             the exception
	 */
	public static String executeWowzaPostApi(String jsonString, String url) throws Exception {
		try {
			TrustManager[] trustAllCerts = new TrustManager[] { new X509ExtendedTrustManager() {
				@Override
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					return null;
				}

				@Override
				public void checkClientTrusted(X509Certificate[] certs, String authType) {
				}

				@Override
				public void checkServerTrusted(X509Certificate[] certs, String authType) {
				}

				@Override
				public void checkClientTrusted(X509Certificate[] xcs, String string, Socket socket)
						throws CertificateException {

				}

				@Override
				public void checkServerTrusted(X509Certificate[] xcs, String string, Socket socket)
						throws CertificateException {

				}

				@Override
				public void checkClientTrusted(X509Certificate[] xcs, String string, SSLEngine ssle)
						throws CertificateException {

				}

				@Override
				public void checkServerTrusted(X509Certificate[] xcs, String string, SSLEngine ssle)
						throws CertificateException {

				}

			} };

			SSLContext context = SSLContext.getInstance("TLSv1.2");
			context.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpClient httpClient = HttpClients.custom().setSslcontext(context).build();
			HttpPost post = new HttpPost(url);
			post.setHeader("Accept", "application/json");
			post.setHeader("wsc-access-key", "vOgv8jhIVQpGfdhK46YHmhT7xFQvWi7i8Yyl1fwXMrZxrlHRuhoH3nv7PuQC3628");
			post.setHeader("wsc-api-key", "Rkr2rpt7fKSULC5IpX6WJ0mJm78hQZQBDlDrMPDofmel9EZgTw0d34EA0Xmk3219");

			StringEntity requestEntity = new StringEntity( jsonString,ContentType.APPLICATION_JSON);

			post.setEntity(requestEntity);

			HttpResponse response = httpClient.execute(post);

			BufferedReader responseData = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

			String result = org.apache.commons.io.IOUtils.toString(responseData);
			return result;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	
	/**
	 * Execute.
	 *
	 * @param dataMap
	 *            the data map
	 * @param url
	 *            the url
	 * @return the string
	 * @throws Exception
	 *             the exception
	 */
	public static String executeWowzaDeleteApi(String jsonString, String url) throws Exception {
		try {
			TrustManager[] trustAllCerts = new TrustManager[] { new X509ExtendedTrustManager() {
				@Override
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					return null;
				}

				@Override
				public void checkClientTrusted(X509Certificate[] certs, String authType) {
				}

				@Override
				public void checkServerTrusted(X509Certificate[] certs, String authType) {
				}

				@Override
				public void checkClientTrusted(X509Certificate[] xcs, String string, Socket socket)
						throws CertificateException {

				}

				@Override
				public void checkServerTrusted(X509Certificate[] xcs, String string, Socket socket)
						throws CertificateException {

				}

				@Override
				public void checkClientTrusted(X509Certificate[] xcs, String string, SSLEngine ssle)
						throws CertificateException {

				}

				@Override
				public void checkServerTrusted(X509Certificate[] xcs, String string, SSLEngine ssle)
						throws CertificateException {

				}

			} };

			SSLContext context = SSLContext.getInstance("TLSv1.2");
			context.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpClient httpClient = HttpClients.custom().setSslcontext(context).build();
			HttpDelete post = new HttpDelete(url);
			post.setHeader("Accept", "application/json");
			post.setHeader("wsc-access-key", "vOgv8jhIVQpGfdhK46YHmhT7xFQvWi7i8Yyl1fwXMrZxrlHRuhoH3nv7PuQC3628");
			post.setHeader("wsc-api-key", "Rkr2rpt7fKSULC5IpX6WJ0mJm78hQZQBDlDrMPDofmel9EZgTw0d34EA0Xmk3219");

			//StringEntity requestEntity = new StringEntity( jsonString,ContentType.APPLICATION_JSON);

			//post.setEntity(requestEntity);

			HttpResponse response = httpClient.execute(post);

			BufferedReader responseData = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

			String result = org.apache.commons.io.IOUtils.toString(responseData);
			return result;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	/**
	 * Execute.
	 *
	 * @param dataMap
	 *            the data map
	 * @param url
	 *            the url
	 * @return the string
	 * @throws Exception
	 *             the exception
	 */
	public static String executeWowzaPatchApi(String jsonString, String url) throws Exception {
		try {
			TrustManager[] trustAllCerts = new TrustManager[] { new X509ExtendedTrustManager() {
				@Override
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					return null;
				}

				@Override
				public void checkClientTrusted(X509Certificate[] certs, String authType) {
				}

				@Override
				public void checkServerTrusted(X509Certificate[] certs, String authType) {
				}

				@Override
				public void checkClientTrusted(X509Certificate[] xcs, String string, Socket socket)
						throws CertificateException {

				}

				@Override
				public void checkServerTrusted(X509Certificate[] xcs, String string, Socket socket)
						throws CertificateException {

				}

				@Override
				public void checkClientTrusted(X509Certificate[] xcs, String string, SSLEngine ssle)
						throws CertificateException {

				}

				@Override
				public void checkServerTrusted(X509Certificate[] xcs, String string, SSLEngine ssle)
						throws CertificateException {

				}

			} };

			SSLContext context = SSLContext.getInstance("TLSv1.2");
			context.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpClient httpClient = HttpClients.custom().setSslcontext(context).build();
			HttpPatch patch = new HttpPatch(url);
			patch.setHeader("Accept", "application/json");
			patch.setHeader("wsc-access-key", "vOgv8jhIVQpGfdhK46YHmhT7xFQvWi7i8Yyl1fwXMrZxrlHRuhoH3nv7PuQC3628");
			patch.setHeader("wsc-api-key", "Rkr2rpt7fKSULC5IpX6WJ0mJm78hQZQBDlDrMPDofmel9EZgTw0d34EA0Xmk3219");

			StringEntity requestEntity = new StringEntity( jsonString,ContentType.APPLICATION_JSON);

			patch.setEntity(requestEntity);

			HttpResponse response = httpClient.execute(patch);

			BufferedReader responseData = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

			String result = org.apache.commons.io.IOUtils.toString(responseData);
			return result;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	
	/**
	 * Execute.
	 *
	 * @param dataMap
	 *            the data map
	 * @param url
	 *            the url
	 * @return the string
	 * @throws Exception
	 *             the exception
	 */
	public static String executeWowzaGetApi(String url) throws Exception {
		try {
			TrustManager[] trustAllCerts = new TrustManager[] { new X509ExtendedTrustManager() {
				@Override
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					return null;
				}

				@Override
				public void checkClientTrusted(X509Certificate[] certs, String authType) {
				}

				@Override
				public void checkServerTrusted(X509Certificate[] certs, String authType) {
				}

				@Override
				public void checkClientTrusted(X509Certificate[] xcs, String string, Socket socket)
						throws CertificateException {

				}

				@Override
				public void checkServerTrusted(X509Certificate[] xcs, String string, Socket socket)
						throws CertificateException {

				}

				@Override
				public void checkClientTrusted(X509Certificate[] xcs, String string, SSLEngine ssle)
						throws CertificateException {

				}

				@Override
				public void checkServerTrusted(X509Certificate[] xcs, String string, SSLEngine ssle)
						throws CertificateException {

				}

			} };

			SSLContext context = SSLContext.getInstance("TLSv1.2");
			context.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpClient httpClient = HttpClients.custom().setSslcontext(context).build();
			HttpGet get = new HttpGet(url);
			get.setHeader("Accept", "application/json");
			get.setHeader("wsc-access-key", "vOgv8jhIVQpGfdhK46YHmhT7xFQvWi7i8Yyl1fwXMrZxrlHRuhoH3nv7PuQC3628");
			get.setHeader("wsc-api-key", "Rkr2rpt7fKSULC5IpX6WJ0mJm78hQZQBDlDrMPDofmel9EZgTw0d34EA0Xmk3219");

			HttpResponse response = httpClient.execute(get);

			BufferedReader responseData = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

			String result = org.apache.commons.io.IOUtils.toString(responseData);
			return result;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	
	
	public static void main(String[] args) {
		try {
			//ADD TARGET
			/*JsonObject chilObj = new JsonObject();
			chilObj.addProperty("name", "FACE12345");
			chilObj.addProperty("primary_url", "rtmps://abc.com");
			chilObj.addProperty("provider","rtmps");
			chilObj.addProperty("stream_name", "rwr-erww-rw-rewr-ewr");
			JsonObject obj = new JsonObject();
			obj.add("stream_target_custom", chilObj);
			
			String data = HTTPUtil.executeWowzaApi(new Gson().toJson(obj), "https://api.cloud.wowza.com/api/v1.6/stream_targets/custom");
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			JsonObject respObj =  jsonObject.getAsJsonObject("stream_target_custom");
			String targetId  = respObj.get("id").getAsString();
			System.out.println("TARGET ID : "+targetId);
			
			
			//ASSIGN TARGET TO OUTPUT
			JsonObject chilObj1 = new JsonObject();
			chilObj1.addProperty("stream_target_id", targetId);
			chilObj1.addProperty("use_stream_target_backup_url", false);
			JsonObject obj1 = new JsonObject();
			obj1.add("output_stream_target", chilObj1);
			
			String data1 = HTTPUtil.executeWowzaApi(new Gson().toJson(obj1), "https://api.cloud.wowza.com/api/v1.6/transcoders/pcybp0ny/outputs/jwg0f8wh/output_stream_targets");
			Gson gson1 = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject1 = gson1.fromJson(data1, JsonObject.class);
			JsonObject respObj1 =  jsonObject1.getAsJsonObject("output_stream_target");
			String outStreamTargtId  = respObj1.get("id").getAsString();
			System.out.println("OUTPUT TARGET ID : "+outStreamTargtId);*/
			
			
			//REMOVE TARGET FROM OUTPUT
			/*String apiUrl = "https://api.cloud.wowza.com/api/v1.6/transcoders/pcybp0ny/outputs/jwg0f8wh/output_stream_targets/pj6wgg7f";
			String data2 = HTTPUtil.executeWowzaDeleteApi("",apiUrl);
			System.out.println("REMOVE TARGET API DATA : "+data2);*/
			
			
			/*JsonObject chilObj1 = new JsonObject();
			chilObj1.addProperty("stream_target_id", "jjsqnkrp");
			chilObj1.addProperty("use_stream_target_backup_url", false);
			
			JsonObject obj1 = new JsonObject();
			obj1.add("output_stream_target", chilObj1);
			String data1 = HTTPUtil.executeWowzaApi(new Gson().toJson(obj1), "https://api.cloud.wowza.com/api/v1.6/transcoders/pcybp0ny/outputs/jwg0f8wh/output_stream_targets");
			
			Gson gson1 = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject1 = gson1.fromJson(data1, JsonObject.class);
			JsonObject respObj1 =  jsonObject1.getAsJsonObject("output_stream_target");
			String outStreamTargtId  = respObj1.get("id").getAsString();
			
			System.out.println("OUTPUT TARGET ID : "+outStreamTargtId);*/
			
			
			JsonObject updObj = new JsonObject();
			JsonObject updChildObj = new JsonObject();
			updChildObj.addProperty("stream_name", "FFFFFFFFFF");
			updObj.add("stream_target_custom", updChildObj);
			
			
			
			
			/*String updData = HTTPUtil.executeWowzaPatchApi(new Gson().toJson(updObj), "https://api.cloud.wowza.com/api/v1.6/stream_targets/custom/xqhhvvs8");
			
			Gson updGson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject1 = updGson.fromJson(updData, JsonObject.class);
			JsonObject updResp =  jsonObject1.getAsJsonObject("stream_target_custom");
			
			String cutTrgId  = updResp.get("id").getAsString();
			System.out.println(cutTrgId);*/
			
			
			String data = HTTPUtil.executeWowzaGetApi("https://api.cloud.wowza.com/api/v1.6/transcoders/pcybp0ny/stats");
			System.out.println(data);
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			JsonObject respObj =  jsonObject.getAsJsonObject("transcoder");
				try {
					JsonObject fbStreamObj = respObj.getAsJsonObject("stream_target_status_jwg0f8wh_bzptk2vd");
					if(fbStreamObj != null){
						String fbStatus  = fbStreamObj.get("value").getAsString();
						System.out.println(fbStatus);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			
				try {
					JsonObject ytStreamObj = respObj.getAsJsonObject("stream_target_status_stream_target_status_jwg0f8wh_bzptk2vd");
					if(ytStreamObj != null){
						String ytStatus  = ytStreamObj.get("value").getAsString();
						System.out.println(ytStatus);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
	
	
	
	/**
	 * Execute.
	 *
	 * @param dataMap
	 *            the data map
	 * @param url
	 *            the url
	 * @return the string
	 * @throws Exception
	 *             the exception
	 */
	public static Map<String,String>  executeWithHeaderTokens(Map<String, String> dataMap, String url,Map<String, String> headerMap) throws Exception {
		try {
			TrustManager[] trustAllCerts = new TrustManager[] { new X509ExtendedTrustManager() {
				@Override
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					return null;
				}

				@Override
				public void checkClientTrusted(X509Certificate[] certs, String authType) {
				}

				@Override
				public void checkServerTrusted(X509Certificate[] certs, String authType) {
				}

				@Override
				public void checkClientTrusted(X509Certificate[] xcs, String string, Socket socket)
						throws CertificateException {

				}

				@Override
				public void checkServerTrusted(X509Certificate[] xcs, String string, Socket socket)
						throws CertificateException {

				}

				@Override
				public void checkClientTrusted(X509Certificate[] xcs, String string, SSLEngine ssle)
						throws CertificateException {

				}

				@Override
				public void checkServerTrusted(X509Certificate[] xcs, String string, SSLEngine ssle)
						throws CertificateException {

				}

			} };

			SSLContext context = SSLContext.getInstance("TLSv1.2");
			context.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpClient httpClient = HttpClients.custom().setSslcontext(context).build();
		

			List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();

			for (Map.Entry<String, String> entry : dataMap.entrySet()) {
				urlParameters.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
			}
			
			String paramString = URLEncodedUtils.format(urlParameters, "utf-8");
			
			if(!url.endsWith("?"))
		        url += "?";
			  url += paramString;
			  
			HttpGet get = new HttpGet(url);
			get.setHeader("Accept", "application/json");  
			
			for (Map.Entry<String, String> entry : headerMap.entrySet()) {				
				get.setHeader(entry.getKey(), entry.getValue());			
			}
			HttpResponse response = httpClient.execute(get);
			
			Map<String,String> respMap = new HashMap<String,String>();
			Header[] headerStr = response.getAllHeaders();
			for(int i = 0 ; i < headerStr.length;i++) {				
				//System.out.println(headerStr[i].getName()    + " ---- " + headerStr[i].getValue());				
				respMap.put(headerStr[i].getName(), headerStr[i].getValue());
			}
			
			BufferedReader responseData = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			String result = org.apache.commons.io.IOUtils.toString(responseData);
			respMap.put("HTTPRESPONSE_JSONDATA", result);
			return respMap;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
}
