/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.rtf.livt.dvo.ContestWinnerDVO;

/**
 * The Class LivtContestSessionUtil.
 */
public class LivtContestSessionUtil {

	/** The contest winner map. */
	public static Map<String, List<ContestWinnerDVO>> contestWinnerMap = new HashMap<String, List<ContestWinnerDVO>>();

	/** The contest winner list. */
	static List<ContestWinnerDVO> contestWinnerList = new ArrayList<ContestWinnerDVO>();

	/** The contest winner count map. */
	public static Map<String, Integer> contestWinnerCountMap = new HashMap<String, Integer>();

}
