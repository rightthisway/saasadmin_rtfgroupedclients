/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.util;

import com.rtf.livt.constants.SAASAPI;
import com.rtf.livt.dvo.ContestDVO;
import com.rtf.livt.service.LiveContestService;

/**
 * The Class MigrationAPIInvokerThread.
 */
public class MigrationAPIInvokerThread implements Runnable {

	/** The cl id. */
	private String clId;

	/** The co id. */
	private String coId;

	/**
	 * Instantiates a new migration API invoker thread.
	 *
	 * @param clId
	 *            the cl id
	 * @param coId
	 *            the co id
	 */
	public MigrationAPIInvokerThread(String clId, String coId) {
		this.clId = clId;
		this.coId = coId;
	}

	/**
	 * Run.
	 */
	@Override
	public void run() {
		Boolean isMigrated = false;
		try {
			ContestDVO contest = LiveContestService.getSQLContestByContestId(clId, coId);
			if (contest == null) {
				return;
			}
			if (contest.getIsAct() == 5 || contest.getIsAct().equals(5)) {
				return;
			}
			if (contest.getIsAct() == 3 || contest.getIsAct().equals(3)) {
				contest.setIsAct(5);
				LiveContestService.updateContestStatus(contest);
				isMigrated = SAASAPI.migrateContestData(clId, coId);
				contest.setIsAct(3);
				LiveContestService.updateContestStatus(contest);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
