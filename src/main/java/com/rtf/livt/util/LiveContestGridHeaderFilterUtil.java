/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.util;

import com.rtf.saas.util.DateFormatUtil;

/**
 * The Class LiveContestGridHeaderFilterUtil.
 */
public class LiveContestGridHeaderFilterUtil {

	/**
	 * Gets the contest filter query.
	 *
	 * @param filter
	 *            the filter
	 * @return the contest filter query
	 */
	public static String getContestFilterQuery(String filter) {
		StringBuffer sql = new StringBuffer();
		try {
			if (filter != null && !filter.isEmpty() && filter.contains(":") && filter.contains(",")) {
				String params[] = filter.split(",");
				for (String param : params) {
					String nameValue[] = param.split(":");
					if (nameValue.length == 2) {
						String name = nameValue[0];
						String value = nameValue[1];
						if (!validateParamValues(name, value)) {
							continue;
						}
						if (name.equalsIgnoreCase("name")) {
							sql.append(" AND c.conname like '%" + value + "%' ");
						} else if (name.equalsIgnoreCase("extName")) {
							sql.append(" AND c.extconname like '%" + value + "%' ");
						}else if (name.equalsIgnoreCase("stDateTimeStr")) {
							getDatePartQuery(sql, "c.consrtdate", value);
						} else if (name.equalsIgnoreCase("coType")) {
							sql.append(" AND c.contype like '%" + value + "%' ");
						} else if (name.equalsIgnoreCase("cat")) {
							sql.append(" AND c.cattype like '%" + value + "%' ");
						} else if (name.equalsIgnoreCase("subCat")) {
							sql.append(" AND c.subcattype like '%" + value + "%' ");
						} else if (name.equalsIgnoreCase("sumRwdType")) {
							sql.append("AND c.sumryrwdtype like '%" + value + "%' ");
						} else if (name.equalsIgnoreCase("ansType")) {
							sql.append(" AND c.anstype like '%" + value + "%' ");
						} else if (name.equalsIgnoreCase("isElimination")) {
							if (value.equalsIgnoreCase("YES")) {
								sql.append(" AND c.iselimtype=1 ");
							} else if (value.equalsIgnoreCase("NO")) {
								sql.append(" AND c.iselimtype=0 ");
							}
						} else if (name.equalsIgnoreCase("isSplitSummary")) {
							if (value.equalsIgnoreCase("YES")) {
								sql.append(" AND c.issumrysplitable=1 ");
							} else if (value.equalsIgnoreCase("NO")) {
								sql.append(" AND c.issumrysplitable=0 ");
							}
						} else if (name.equalsIgnoreCase("isLotEnbl")) {
							if (value.equalsIgnoreCase("YES")) {
								sql.append(" AND c.islotryenabled=1 ");
							} else if (value.equalsIgnoreCase("NO")) {
								sql.append(" AND c.islotryenabled=0 ");
							}
						} else if (name.equalsIgnoreCase("isPwd")) {
							if (value.equalsIgnoreCase("YES")) {
								sql.append(" AND c.ispwd=1 ");
							} else if (value.equalsIgnoreCase("NO")) {
								sql.append(" AND c.ispwd=0 ");
							}
						} else if (name.equalsIgnoreCase("conpwd")) {
							sql.append(" AND c.conpwd like '%" + value + "%' ");
						} else if (name.equalsIgnoreCase("qSize")) {
							sql.append(" AND c.noofqns =" + value);
						} else if (name.equalsIgnoreCase("seqNo")) {
							sql.append(" AND c.cardseqno =" + value);
						} else if (name.equalsIgnoreCase("crBy")) {
							sql.append(" AND c.creby like '%" + value + "%' ");
						} else if (name.equalsIgnoreCase("crDate")) {
							getDatePartQuery(sql, "c.credate", value);
						} else if (name.equalsIgnoreCase("upBy")) {
							sql.append(" AND c.updby like '%" + value + "%' ");
						} else if (name.equalsIgnoreCase("upDateTimeStr")) {
							getDatePartQuery(sql, "c.upddate", value);
						} else if (name.equalsIgnoreCase("status")) {
							sql.append(" AND s.isconactivetext  like '%" + value + "%' ");
						} else if (name.equalsIgnoreCase("sumRwdVal")) {
							sql.append(" AND c.sumryrwdval=" + value + " ");
						} else if (name.equalsIgnoreCase("winRwdVal")) {
							sql.append(" AND c.lotryrwdval=" + value + " ");
						} else if (name.equalsIgnoreCase("partiRwdVal")) {
							sql.append(" AND c.participantrwdval=" + value + " ");
						} else if (name.equalsIgnoreCase("winRwdType")) {
							sql.append(" AND c.lotryrwdtype  like '%" + value + "%' ");
						} else if (name.equalsIgnoreCase("partiRwdType")) {
							sql.append(" AND c.participantrwdtype  like '%" + value + "%' ");
						} else if (name.equalsIgnoreCase("migStatus")) {
							sql.append(" AND c.migrstatus  like '%" + value + "%' ");
						} else if (name.equalsIgnoreCase("lastAction")) {
							sql.append(" AND c.lastaction  like '%" + value + "%' ");
						} else if (name.equalsIgnoreCase("queRwdType")) {
							sql.append(" AND c.qsnrwdtype  like '%" + value + "%' ");
						} else if (name.equalsIgnoreCase("lastQue")) {
							sql.append(" AND c.lastqsn=" + value + " ");
						} else if (name.equalsIgnoreCase("expPartCntStr")) {
							sql.append(" AND pc.pcdisplay like '%" + value + "%' ");
						}else if (name.equalsIgnoreCase("winnerCount")) {
							sql.append(" AND c.ngrndwnrs =" + value);
						}

					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sql.toString();
	}

	/**
	 * Gets the contest config filter query.
	 *
	 * @param filter
	 *            the filter
	 * @return the contest config filter query
	 */
	public static String getContestConfigFilterQuery(String filter) {
		StringBuffer sql = new StringBuffer();
		try {
			if (filter != null && !filter.isEmpty() && filter.contains(":") && filter.contains(",")) {
				String params[] = filter.split(",");
				for (String param : params) {
					String nameValue[] = param.split(":");
					if (nameValue.length == 2) {
						String name = nameValue[0];
						String value = nameValue[1];
						if (!validateParamValues(name, value)) {
							continue;
						}
						if (name.equalsIgnoreCase("name")) {
							sql.append(" AND keyid like '%" + value + "%' ");
						} else if (name.equalsIgnoreCase("desc")) {
							sql.append(" AND descr like '%" + value + "%' ");
						} else if (name.equalsIgnoreCase("value")) {
							sql.append(" AND keyvalue =" + value + " ");
						} else if (name.equalsIgnoreCase("upBy")) {
							sql.append(" AND updby like '%" + value + "%' ");
						} else if (name.equalsIgnoreCase("upDate")) {
							getDatePartQuery(sql, "upddate", value);
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sql.toString();
	}

	/**
	 * Gets the contest questions filter query.
	 *
	 * @param filter
	 *            the filter
	 * @return the contest questions filter query
	 * @throws Exception
	 *             the exception
	 */
	public static String getContestQuestionsFilterQuery(String filter) throws Exception {
		StringBuffer sql = new StringBuffer();
		try {
			if (filter != null && !filter.isEmpty() && filter.contains(":") && filter.contains(",")) {
				String params[] = filter.split(",");
				for (String param : params) {
					try {
						String nameValue[] = param.split(":");
						if (nameValue.length == 2) {
							String name = nameValue[0];
							String value = nameValue[1];
							if (!validateParamValues(name, value)) {
								continue;
							}
							if (name.equalsIgnoreCase("qsntext")) {
								sql.append(" AND qsntext like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("opa")) {
								sql.append(" AND ansopta like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("opb")) {
								sql.append(" AND ansoptb like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("opc")) {
								sql.append(" AND ansoptc like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("opd")) {
								sql.append(" AND ansoptd like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("corans")) {
								sql.append("AND corans like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("qsnseqno")) {
								sql.append("AND qsnseqno like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("qsnorient")) {
								sql.append("AND qsnorient like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("ansRwdVal")) {
								sql.append("AND rwdval like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("mjRwdType")) {
								sql.append("AND jackpottype like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("mjRwdVal")) {
								sql.append("AND maxqtyperwinner like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("mjWinnersCount")) {
								sql.append("AND maxwinners like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("qDifLevel")) {
								sql.append("AND difficultlevel like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("crBy")) {
								sql.append(" AND creby like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("crDate")) {
								getDatePartQuery(sql, "credate", value);
							} else if (name.equalsIgnoreCase("upBy")) {
								sql.append(" AND updby like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("upDate")) {
								getDatePartQuery(sql, "upddate", value);
							}
						}
					} catch (Exception e) {

					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sql.toString();
	}

	/**
	 * Gets the all question bank questions for contest with filter query.
	 *
	 * @param filter
	 *            the filter
	 * @return the all question bank questions for contest with filter query
	 * @throws Exception
	 *             the exception
	 */
	public static String getAllQuestionBankQuestionsForContestWithFilterQuery(String filter) throws Exception {
		StringBuffer sql = new StringBuffer();
		try {
			if (filter != null && !filter.isEmpty() && filter.contains(":") && filter.contains(",")) {
				String params[] = filter.split(",");
				for (String param : params) {
					try {
						String nameValue[] = param.split(":");
						if (nameValue.length == 2) {
							String name = nameValue[0];
							String value = nameValue[1];
							if (!validateParamValues(name, value)) {
								continue;
							}
							if (name.equalsIgnoreCase("qtx")) {
								sql.append(" AND qsntext like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("opa")) {
								sql.append(" AND ansopta like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("opb")) {
								sql.append(" AND ansoptb like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("opc")) {
								sql.append(" AND ansoptc like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("opd")) {
								sql.append(" AND ansoptd like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("cans")) {
								sql.append("AND corans like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("anstype")) {
								sql.append("AND anstype like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("qOrientn")) {
								sql.append("AND qsnorient like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("ctyp")) {
								sql.append("AND cattype like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("sctyp")) {
								sql.append("AND subcattype like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("creBy")) {
								sql.append(" AND creby like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("creDate")) {
								getDatePartQuery(sql, "credate", value);
							} else if (name.equalsIgnoreCase("updBy")) {
								sql.append(" AND updby like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("updDt")) {
								getDatePartQuery(sql, "upddate", value);
							}
						}
					} catch (Exception e) {

					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sql.toString();
	}

	/**
	 * Gets the all categories filter query.
	 *
	 * @param filter
	 *            the filter
	 * @return the all categories filter query
	 * @throws Exception
	 *             the exception
	 */
	public static String getAllCategoriesFilterQuery(String filter) throws Exception {
		StringBuffer sql = new StringBuffer();
		try {
			if (filter != null && !filter.isEmpty() && filter.contains(":") && filter.contains(",")) {
				String params[] = filter.split(",");
				for (String param : params) {
					try {
						String nameValue[] = param.split(":");
						if (nameValue.length == 2) {
							String name = nameValue[0];
							String value = nameValue[1];
							if (!validateParamValues(name, value)) {
								continue;
							}
							if (name.equalsIgnoreCase("cattype")) {
								sql.append(" AND cattype like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("catdesc")) {
								sql.append(" AND catdesc like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("creby")) {
								sql.append(" AND creby like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("credate")) {
								getDatePartQuery(sql, "credate", value);
							} else if (name.equalsIgnoreCase("updby")) {
								sql.append(" AND updby like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("upddate")) {
								getDatePartQuery(sql, "upddate", value);
							}
						}
					} catch (Exception e) {

					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sql.toString();
	}

	/**
	 * Gets the all sub categories filter query.
	 *
	 * @param filter
	 *            the filter
	 * @return the all sub categories filter query
	 * @throws Exception
	 *             the exception
	 */
	public static String getAllSubCategoriesFilterQuery(String filter) throws Exception {
		StringBuffer sql = new StringBuffer();
		try {
			if (filter != null && !filter.isEmpty() && filter.contains(":") && filter.contains(",")) {
				String params[] = filter.split(",");
				for (String param : params) {
					try {
						String nameValue[] = param.split(":");
						if (nameValue.length == 2) {
							String name = nameValue[0];
							String value = nameValue[1];
							if (!validateParamValues(name, value)) {
								continue;
							}
							if (name.equalsIgnoreCase("cattype")) {
								sql.append(" AND cattype like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("subcattype")) {
								sql.append(" AND subcattype like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("subcatdesc")) {
								sql.append(" AND subcatdesc like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("creby")) {
								sql.append(" AND creby like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("credate")) {
								getDatePartQuery(sql, "credate", value);
							} else if (name.equalsIgnoreCase("updby")) {
								sql.append(" AND updby like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("upddate")) {
								getDatePartQuery(sql, "upddate", value);
							}
						}
					} catch (Exception e) {

					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sql.toString();
	}

	/**
	 * Gets the all reward types filter query.
	 *
	 * @param filter
	 *            the filter
	 * @return the all reward types filter query
	 * @throws Exception
	 *             the exception
	 */
	public static String getAllRewardTypesFilterQuery(String filter) throws Exception {
		StringBuffer sql = new StringBuffer();
		try {
			if (filter != null && !filter.isEmpty() && filter.contains(":") && filter.contains(",")) {
				String params[] = filter.split(",");
				for (String param : params) {
					try {
						String nameValue[] = param.split(":");
						if (nameValue.length == 2) {
							String name = nameValue[0];
							String value = nameValue[1];
							if (!validateParamValues(name, value)) {
								continue;
							}
							if (name.equalsIgnoreCase("rwdtype")) {
								sql.append(" AND rwdtype like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("rwddesc")) {
								sql.append(" AND rwddesc like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("rwdunimesr")) {
								sql.append(" AND rwdunimesr like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("creby")) {
								sql.append(" AND creby like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("credate")) {
								getDatePartQuery(sql, "credate", value);
							} else if (name.equalsIgnoreCase("updby")) {
								sql.append(" AND updby like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("upddate")) {
								getDatePartQuery(sql, "upddate", value);
							}
						}
					} catch (Exception e) {

					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sql.toString();
	}

	/**
	 * Gets the date part query.
	 *
	 * @param sql
	 *            the sql
	 * @param columnName
	 *            the column name
	 * @param value
	 *            the value
	 * @return the date part query
	 */
	private static void getDatePartQuery(StringBuffer sql, String columnName, String value) {
		try {
			if (DateFormatUtil.extractDateElement(value, "DAY") > 0) {
				sql.append(" AND DATEPART(day, " + columnName + ") = " + DateFormatUtil.extractDateElement(value, "DAY")
						+ " ");
			}
			if (DateFormatUtil.extractDateElement(value, "MONTH") > 0) {
				sql.append(" AND DATEPART(month, " + columnName + ") = "
						+ DateFormatUtil.extractDateElement(value, "MONTH") + " ");
			}
			if (DateFormatUtil.extractDateElement(value, "YEAR") > 0) {
				sql.append(" AND DATEPART(year, " + columnName + ") = "
						+ DateFormatUtil.extractDateElement(value, "YEAR") + " ");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Validate param values.
	 *
	 * @param name
	 *            the name
	 * @param value
	 *            the value
	 * @return true, if successful
	 */
	private static boolean validateParamValues(String name, String value) {
		if (name == null || name.isEmpty() || name.equalsIgnoreCase("undefined") || value == null || value.isEmpty()
				|| value.equalsIgnoreCase("undefined")) {
			return false;
		}
		return true;
	}
}
