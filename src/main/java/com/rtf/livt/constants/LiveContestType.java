/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.constants;

/**
 * The Enum LiveContestType.
 */
public enum LiveContestType {

	/** The normal. */
	NORMAL,
	/** The mini. */
	MINI,
	/** The mega. */
	MEGA;
}
