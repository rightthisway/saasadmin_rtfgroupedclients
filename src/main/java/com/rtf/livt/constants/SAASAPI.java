/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.constants;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rtf.common.util.GsonCustomConfig;
import com.rtf.livt.dto.LivtCommonDTO;
import com.rtf.livt.dvo.ClusterNodeConfigDVO;
import com.rtf.livt.dvo.ContestDVO;
import com.rtf.livt.dvo.LiveContestUserIdConfDVO;
import com.rtf.livt.dvo.LivtContestQuestionDVO;
import com.rtf.livt.service.ClusterNodeConfigService;
import com.rtf.livt.util.HTTPUtil;

/**
 * The Class SAASAPI.
 */
public class SAASAPI {

	/** The start contest api. */
	public static String START_CONTEST_API = "livtStartContest.json";

	/** The get question api. */
	public static String GET_QUESTION_API = "livtGetContQuestion.json";

	/** The get answer count api. */
	public static String GET_ANSWER_COUNT_API = "livtGetQuestAnsCount.json";

	/** The end contest api. */
	public static String END_CONTEST_API = "livtEndContest.json";

	/** The reset contest api. */
	public static String RESET_CONTEST_API = "livtResetContest.json";

	/** The migrate contest data api. */
	public static String MIGRATE_CONTEST_DATA_API = "livtmigrateCassData.json";

	/**
	 * Reset contest.
	 *
	 * @param contest
	 *            the contest
	 * @return the boolean
	 */
	public static Boolean resetContest(ContestDVO contest) {
		String data = null;
		LivtCommonDTO resp = null;
		try {
			Map<String, String> dataMap = new HashMap<String, String>();
			dataMap.put("clId", contest.getClId());
			dataMap.put("coId", contest.getCoId());
			dataMap.put("platForm", "WEB");

			List<ClusterNodeConfigDVO> configs = ClusterNodeConfigService
					.getAllActiveClusterNodeConfig(contest.getClId());
			for (ClusterNodeConfigDVO conf : configs) {
				if (conf.getIsPrimary()) {
					dataMap.put("clearDbData", "Y");
				}
				data = HTTPUtil.execute(dataMap, conf.getUrl() + RESET_CONTEST_API);
				Gson gson = GsonCustomConfig.getGsonBuilder();
				JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
				resp = gson.fromJson(((JsonObject) jsonObject.get("livtresp")), LivtCommonDTO.class);

				if (resp.getSts() == 0 && conf.getIsPrimary()) {
					return false;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * Start contest.
	 *
	 * @param contest
	 *            the contest
	 * @return the boolean
	 */
	public static Boolean startContest(ContestDVO contest) {
		String data = null;
		LivtCommonDTO resp = null;
		try {
			Map<String, String> dataMap = new HashMap<String, String>();
			dataMap.put("clId", contest.getClId());
			dataMap.put("coId", contest.getCoId());
			dataMap.put("platForm", "WEB");
			List<ClusterNodeConfigDVO> configs = ClusterNodeConfigService.getAllActiveClusterNodeConfig(contest.getClId());
			
			Integer expCnt = contest.getExpPartCount();
			Integer nodeCnt = configs.size();
			if(expCnt == null){
				expCnt = 0;
			}
			Double cnt = (expCnt * 1.5)/nodeCnt;
			Double startFrom = 0.00;
			
			LiveContestUserIdConfDVO config = null;
			
			for (ClusterNodeConfigDVO conf : configs) {
				dataMap.put("uIdstrt", String.valueOf(startFrom));
				dataMap.put("uIdend", String.valueOf(startFrom+cnt));
				System.out.println("RANGE :  ST = "+startFrom+"   =====  END = "+(startFrom+cnt));
				config = ClusterNodeConfigService.getLiveContestUserIdConf(conf.getClId(), conf.getNodeId());
				if(config == null){
					config = new LiveContestUserIdConfDVO();
					config.setClId(conf.getClId());
					config.setNodeId(conf.getNodeId());
					config.setCounter(startFrom.intValue());
					ClusterNodeConfigService.saveLiveContestUserConf(config);
				}else{
					config.setCounter(startFrom.intValue());
					ClusterNodeConfigService.updateLiveContestUserConf(config);
				}
				
				startFrom = startFrom + cnt + 1;
				data = HTTPUtil.execute(dataMap, conf.getUrl() + START_CONTEST_API);
				Gson gson = GsonCustomConfig.getGsonBuilder();
				JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
				resp = gson.fromJson(((JsonObject) jsonObject.get("livtresp")), LivtCommonDTO.class);

				if (resp.getSts() == 0 && conf.getIsPrimary()) {
					return false;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * Gets the question.
	 *
	 * @param contest
	 *            the contest
	 * @param que
	 *            the que
	 * @return the question
	 */
	public static Boolean getQuestion(ContestDVO contest, LivtContestQuestionDVO que) {
		String data = null;
		LivtCommonDTO resp = null;
		try {
			Map<String, String> dataMap = new HashMap<String, String>();
			dataMap.put("clId", contest.getClId());
			dataMap.put("coId", contest.getCoId());
			dataMap.put("platForm", "WEB");
			dataMap.put("qNo", String.valueOf(que.getQsnseqno()));
			dataMap.put("qId", String.valueOf(que.getConqsnid()));
			List<ClusterNodeConfigDVO> configs = ClusterNodeConfigService
					.getAllActiveClusterNodeConfig(contest.getClId());
			for (ClusterNodeConfigDVO conf : configs) {
				data = HTTPUtil.execute(dataMap, conf.getUrl() + GET_QUESTION_API);
				Gson gson = GsonCustomConfig.getGsonBuilder();
				JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
				resp = gson.fromJson(((JsonObject) jsonObject.get("livtresp")), LivtCommonDTO.class);

				if (resp.getSts() == 0 && conf.getIsPrimary()) {
					return false;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * Gets the answer count.
	 *
	 * @param contest
	 *            the contest
	 * @param que
	 *            the que
	 * @return the answer count
	 */
	public static Boolean getAnswerCount(ContestDVO contest, LivtContestQuestionDVO que) {
		String data = null;
		LivtCommonDTO resp = null;
		try {
			Map<String, String> dataMap = new HashMap<String, String>();
			dataMap.put("clId", contest.getClId());
			dataMap.put("coId", contest.getCoId());
			dataMap.put("platForm", "WEB");
			dataMap.put("qNo", String.valueOf(que.getQsnseqno()));
			dataMap.put("qId", String.valueOf(que.getConqsnid()));
			List<ClusterNodeConfigDVO> configs = ClusterNodeConfigService
					.getAllActiveClusterNodeConfig(contest.getClId());
			for (ClusterNodeConfigDVO conf : configs) {
				data = HTTPUtil.execute(dataMap, conf.getUrl() + GET_ANSWER_COUNT_API);
				Gson gson = GsonCustomConfig.getGsonBuilder();
				JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
				resp = gson.fromJson(((JsonObject) jsonObject.get("livtresp")), LivtCommonDTO.class);

				if (resp.getSts() == 0 && conf.getIsPrimary()) {
					return false;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * End contest.
	 *
	 * @param contest
	 *            the contest
	 * @return the boolean
	 */
	public static Boolean endContest(ContestDVO contest) {
		String data = null;
		LivtCommonDTO resp = null;
		try {
			Map<String, String> dataMap = new HashMap<String, String>();
			dataMap.put("clId", contest.getClId());
			dataMap.put("coId", contest.getCoId());
			dataMap.put("platForm", "WEB");
			List<ClusterNodeConfigDVO> configs = ClusterNodeConfigService
					.getAllActiveClusterNodeConfig(contest.getClId());
			for (ClusterNodeConfigDVO conf : configs) {
				data = HTTPUtil.execute(dataMap, conf.getUrl() + END_CONTEST_API);
				Gson gson = GsonCustomConfig.getGsonBuilder();
				JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
				resp = gson.fromJson(((JsonObject) jsonObject.get("livtresp")), LivtCommonDTO.class);

				if (resp.getSts() == 0 && conf.getIsPrimary()) {
					return false;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * Migrate contest data.
	 *
	 * @param clId
	 *            the cl id
	 * @param coId
	 *            the co id
	 * @return the boolean
	 */
	public static Boolean migrateContestData(String clId, String coId) {
		String data = null;
		LivtCommonDTO resp = null;
		try {
			Map<String, String> dataMap = new HashMap<String, String>();
			dataMap.put("clId", clId);
			dataMap.put("coId", coId);
			dataMap.put("platForm", "WEB");
			List<ClusterNodeConfigDVO> configs = ClusterNodeConfigService.getAllActiveClusterNodeConfig(clId);
			for (ClusterNodeConfigDVO conf : configs) {
				if (conf.getIsPrimary()) {
					data = HTTPUtil.execute(dataMap, conf.getUrl() + MIGRATE_CONTEST_DATA_API);
					Gson gson = GsonCustomConfig.getGsonBuilder();
					JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
					resp = gson.fromJson(((JsonObject) jsonObject.get("livtresp")), LivtCommonDTO.class);

					if (resp.getSts() == 0 && conf.getIsPrimary()) {
						return false;
					}
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
}
