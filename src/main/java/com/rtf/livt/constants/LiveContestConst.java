/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.constants;

/**
 * The Class LiveContestConst.
 */
public class LiveContestConst {

	/** The start contest api. */
	public static String START_CONTEST_API = "livestartcontest.json";

	/** The contest question api. */
	public static String CONTEST_QUESTION_API = "livegetquestion.json";

	/** The contest answer count api. */
	public static String CONTEST_ANSWER_COUNT_API = "livegetquestionanswercount.json";

	/** The contest answer api. */
	public static String CONTEST_ANSWER_API = "livegetquestionanswer.json";

	/** The contest simmary api. */
	public static String CONTEST_SIMMARY_API = "livegetcontestsummary.json";

	/** The contest show summary api. */
	public static String CONTEST_SHOW_SUMMARY_API = "liveshowcontestsummary.json";

	/** The contest minijackpot api. */
	public static String CONTEST_MINIJACKPOT_API = "livegetcontestminijackpot.json";

	/** The contest show minijackpot api. */
	public static String CONTEST_SHOW_MINIJACKPOT_API = "liveshowcontestminijackpot.json";

	/** The contest megajackpot api. */
	public static String CONTEST_MEGAJACKPOT_API = "livegetcontestmegajackpot.json";

	/** The contest show megajackpot api. */
	public static String CONTEST_SHOW_MEGAJACKPOT_API = "liveshowcontestmegajackpot.json";

	/** The contest lottery api. */
	public static String CONTEST_LOTTERY_API = "livescrollcontestsummary.json";

	/** The contest winner api. */
	public static String CONTEST_WINNER_API = "livegetcontestwinner.json";

	/** The contest show winner api. */
	public static String CONTEST_SHOW_WINNER_API = "liveshowcontestwinner.json";

	/** The contest end api. */
	public static String CONTEST_END_API = "liveendcontest.json";

	/** The start contest btn. */
	public static String START_CONTEST_BTN = "START CONTEST";

	/** The question btn. */
	public static String QUESTION_BTN = "SHOW QUESTION";

	/** The count btn. */
	public static String COUNT_BTN = "SHOW COUNT";

	/** The answer btn. */
	public static String ANSWER_BTN = "SHOW ANSWER";

	/** The minijackpot btn. */
	public static String MINIJACKPOT_BTN = "DECLARE MINI JACKPOT WINNER";

	/** The dsp minijackpot btn. */
	public static String DSP_MINIJACKPOT_BTN = "SHOW MINI JACKPOT WINNER";

	/** The megajackpot btn. */
	public static String MEGAJACKPOT_BTN = "DECLARE MEGA JACKPOT WINNER";

	/** The dsp megajackpot btn. */
	public static String DSP_MEGAJACKPOT_BTN = "SHOW MEGA JACKPOT WINNER";

	/** The summary btn. */
	public static String SUMMARY_BTN = "DECLARE SUMMARY";

	/** The dsp summary btn. */
	public static String DSP_SUMMARY_BTN = "SHOW SUMMARY";

	/** The lottery btn. */
	public static String LOTTERY_BTN = "RUN LOTTERY";

	/** The dl winner btn. */
	public static String DL_WINNER_BTN = "DECLARE WINNER";

	/** The dsp winner btn. */
	public static String DSP_WINNER_BTN = "SHOW WINNER";

	/** The end contest btn. */
	public static String END_CONTEST_BTN = "END CONTEST";

}
