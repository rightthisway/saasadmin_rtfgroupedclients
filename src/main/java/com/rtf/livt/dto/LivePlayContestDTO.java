/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.dto;

import java.util.List;

import com.rtf.livt.dvo.ContestDVO;
import com.rtf.livt.dvo.ContestGrandWinnerDVO;
import com.rtf.livt.dvo.ContestJackpotWinnerDVO;
import com.rtf.livt.dvo.ContestWinnerDVO;
import com.rtf.livt.dvo.LiveContestAnswerCountDVO;
import com.rtf.livt.dvo.LivtContestQuestionDVO;
import com.rtf.saas.arch.RtfSaasBaseDTO;

/**
 * The Class LivePlayContestDTO.
 */
public class LivePlayContestDTO extends RtfSaasBaseDTO {

	/** The contest. */
	private ContestDVO contest;

	/** The question. */
	private LivtContestQuestionDVO question;

	/** The nxt question. */
	private LivtContestQuestionDVO nxtQuestion;

	/** The btn text. */
	private String btnText;

	/** The nxt api. */
	private String nxtApi;

	/** The summary winners. */
	private List<ContestWinnerDVO> summaryWinners;

	/** The winner cnt. */
	private Integer winnerCnt;

	/** The grand winners. */
	private List<ContestGrandWinnerDVO> grandWinners;

	/** The answer count. */
	private LiveContestAnswerCountDVO answerCount;

	/** The grand winner cnt. */
	private Integer grandWinnerCnt;

	/** The jackpot winners. */
	private List<ContestJackpotWinnerDVO> jackpotWinners;

	/** The wtm. */
	private Integer wtm = 7;

	/** The is prod config. */
	private Boolean isProdConfig;

	/**
	 * Gets the contest.
	 *
	 * @return the contest
	 */
	public ContestDVO getContest() {
		return contest;
	}

	/**
	 * Sets the contest.
	 *
	 * @param contest
	 *            the new contest
	 */
	public void setContest(ContestDVO contest) {
		this.contest = contest;
	}

	/**
	 * Gets the btn text.
	 *
	 * @return the btn text
	 */
	public String getBtnText() {
		return btnText;
	}

	/**
	 * Sets the btn text.
	 *
	 * @param btnText
	 *            the new btn text
	 */
	public void setBtnText(String btnText) {
		this.btnText = btnText;
	}

	/**
	 * Gets the nxt api.
	 *
	 * @return the nxt api
	 */
	public String getNxtApi() {
		return nxtApi;
	}

	/**
	 * Sets the nxt api.
	 *
	 * @param nxtApi
	 *            the new nxt api
	 */
	public void setNxtApi(String nxtApi) {
		this.nxtApi = nxtApi;
	}

	/**
	 * Gets the question.
	 *
	 * @return the question
	 */
	public LivtContestQuestionDVO getQuestion() {
		return question;
	}

	/**
	 * Sets the question.
	 *
	 * @param question
	 *            the new question
	 */
	public void setQuestion(LivtContestQuestionDVO question) {
		this.question = question;
	}

	/**
	 * Gets the summary winners.
	 *
	 * @return the summary winners
	 */
	public List<ContestWinnerDVO> getSummaryWinners() {
		return summaryWinners;
	}

	/**
	 * Sets the summary winners.
	 *
	 * @param summaryWinners
	 *            the new summary winners
	 */
	public void setSummaryWinners(List<ContestWinnerDVO> summaryWinners) {
		this.summaryWinners = summaryWinners;
	}

	/**
	 * Gets the winner cnt.
	 *
	 * @return the winner cnt
	 */
	public Integer getWinnerCnt() {
		return winnerCnt;
	}

	/**
	 * Sets the winner cnt.
	 *
	 * @param winnerCnt
	 *            the new winner cnt
	 */
	public void setWinnerCnt(Integer winnerCnt) {
		this.winnerCnt = winnerCnt;
	}

	/**
	 * Gets the grand winners.
	 *
	 * @return the grand winners
	 */
	public List<ContestGrandWinnerDVO> getGrandWinners() {
		return grandWinners;
	}

	/**
	 * Sets the grand winners.
	 *
	 * @param grandWinners
	 *            the new grand winners
	 */
	public void setGrandWinners(List<ContestGrandWinnerDVO> grandWinners) {
		this.grandWinners = grandWinners;
	}

	/**
	 * Gets the grand winner cnt.
	 *
	 * @return the grand winner cnt
	 */
	public Integer getGrandWinnerCnt() {
		return grandWinnerCnt;
	}

	/**
	 * Sets the grand winner cnt.
	 *
	 * @param grandWinnerCnt
	 *            the new grand winner cnt
	 */
	public void setGrandWinnerCnt(Integer grandWinnerCnt) {
		this.grandWinnerCnt = grandWinnerCnt;
	}

	/**
	 * Gets the answer count.
	 *
	 * @return the answer count
	 */
	public LiveContestAnswerCountDVO getAnswerCount() {
		return answerCount;
	}

	/**
	 * Sets the answer count.
	 *
	 * @param answerCount
	 *            the new answer count
	 */
	public void setAnswerCount(LiveContestAnswerCountDVO answerCount) {
		this.answerCount = answerCount;
	}

	/**
	 * Gets the jackpot winners.
	 *
	 * @return the jackpot winners
	 */
	public List<ContestJackpotWinnerDVO> getJackpotWinners() {
		return jackpotWinners;
	}

	/**
	 * Sets the jackpot winners.
	 *
	 * @param jackpotWinners
	 *            the new jackpot winners
	 */
	public void setJackpotWinners(List<ContestJackpotWinnerDVO> jackpotWinners) {
		this.jackpotWinners = jackpotWinners;
	}

	/**
	 * Gets the wtm.
	 *
	 * @return the wtm
	 */
	public Integer getWtm() {
		if (wtm == null)
			wtm = 7;
		return wtm;
	}

	/**
	 * Sets the wtm.
	 *
	 * @param wtm
	 *            the new wtm
	 */
	public void setWtm(Integer wtm) {
		this.wtm = wtm;
	}

	/**
	 * Gets the nxt question.
	 *
	 * @return the nxt question
	 */
	public LivtContestQuestionDVO getNxtQuestion() {
		return nxtQuestion;
	}

	/**
	 * Sets the nxt question.
	 *
	 * @param nxtQuestion
	 *            the new nxt question
	 */
	public void setNxtQuestion(LivtContestQuestionDVO nxtQuestion) {
		this.nxtQuestion = nxtQuestion;
	}

	/**
	 * Gets the checks if is prod config.
	 *
	 * @return the checks if is prod config
	 */
	public Boolean getIsProdConfig() {
		return isProdConfig;
	}

	/**
	 * Sets the checks if is prod config.
	 *
	 * @param isProdConfig
	 *            the new checks if is prod config
	 */
	public void setIsProdConfig(Boolean isProdConfig) {
		this.isProdConfig = isProdConfig;
	}

}
