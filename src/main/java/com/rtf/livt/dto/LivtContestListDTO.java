/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.dto;

import java.util.List;

import com.rtf.livt.dvo.ContestListDVO;
import com.rtf.saas.arch.RtfSaasBaseDTO;

/**
 * The Class LivtContestListDTO.
 */
public class LivtContestListDTO extends RtfSaasBaseDTO {

	/** The list. */
	private List<ContestListDVO> list;

	/**
	 * Gets the list.
	 *
	 * @return the list
	 */
	public List<ContestListDVO> getList() {
		return list;
	}

	/**
	 * Sets the list.
	 *
	 * @param list
	 *            the new list
	 */
	public void setList(List<ContestListDVO> list) {
		this.list = list;
	}

}
