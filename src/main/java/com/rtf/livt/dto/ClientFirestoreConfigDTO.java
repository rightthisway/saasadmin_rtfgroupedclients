/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.dto;

import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.sql.dvo.ClientFirestoreConfigDVO;

/**
 * The Class ClientFirestoreConfigDTO.
 */
public class ClientFirestoreConfigDTO extends RtfSaasBaseDTO {

	/** The fs config. */
	private ClientFirestoreConfigDVO fsConfig;

	/**
	 * Gets the fs config.
	 *
	 * @return the fs config
	 */
	public ClientFirestoreConfigDVO getFsConfig() {
		return fsConfig;
	}

	/**
	 * Sets the fs config.
	 *
	 * @param fsConfig
	 *            the new fs config
	 */
	public void setFsConfig(ClientFirestoreConfigDVO fsConfig) {
		this.fsConfig = fsConfig;
	}

}
