/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.cass.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.datastax.driver.core.BatchStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.SimpleStatement;
import com.rtf.livt.dvo.ContestWinnerDVO;
import com.rtf.saas.cass.db.CassandraConnector;

/**
 * The Class CassContestWinnersDAO.
 */
public class CassContestWinnersDAO {

	/**
	 * Gets the contest winners by contest id.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 * @return the contest winners by contest id
	 */
	public static List<ContestWinnerDVO> getContestWinnersByContestId(String clientId, String contestId) {
		ResultSet results = CassandraConnector.getSession().execute(
				"SELECT * from pt_livvx_contest_grand_winners  WHERE clintid=? and coid=? ", clientId, contestId);
		List<ContestWinnerDVO> contestWinners = new ArrayList<ContestWinnerDVO>();

		if (results != null) {
			for (Row row : results) {
				contestWinners.add(
						new ContestWinnerDVO(row.getString("clintid"), row.getString("conid"), row.getString("custid"),
								row.getString("rwdtype"), row.getDouble("rwdval"), null, null, null, null, null));
			}
		}
		return contestWinners;
	}

	/**
	 * Gets the contest winners map by contest id.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 * @return the contest winners map by contest id
	 */
	public static Map<String, ContestWinnerDVO> getContestWinnersMapByContestId(String clientId, String contestId) {
		ResultSet results = CassandraConnector.getSession()
				.execute("SELECT * from pt_livvx_contest_winner  WHERE clintid=? and conid=? ", clientId, contestId);
		Map<String, ContestWinnerDVO> winnerMap = new HashMap<String, ContestWinnerDVO>();
		if (results != null) {
			for (Row row : results) {
				String cuId = row.getString("custid");
				winnerMap.put(cuId, new ContestWinnerDVO(row.getString("clintid"), row.getString("conid"), cuId,
						row.getString("rwdtype"), row.getDouble("rwdval"), null, null, null, null, null));
			}
		}
		return winnerMap;
	}

	/**
	 * Update contest winners rewards.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 * @param rewardsPerWinner
	 *            the rewards per winner
	 * @param rwdType
	 *            the rwd type
	 */
	public static void updateContestWinnersRewards(String clientId, String contestId, Double rewardsPerWinner,
			String rwdType) {

		SimpleStatement statement = new SimpleStatement(
				" UPDATE pt_livvx_contest_winner SET rwdval=?, rwdtype=? WHERE conid=? and clintid=? )",
				rewardsPerWinner, rwdType, contestId, clientId);
		CassandraConnector.getSession().executeAsync(statement);
	}

	/**
	 * Batch upsert winner details.
	 *
	 * @param winners
	 *            the winners
	 * @param rewardsPerWinner
	 *            the rewards per winner
	 * @param rwdType
	 *            the rwd type
	 * @return the boolean
	 */
	public static Boolean batchUpsertWinnerDetails(List<ContestWinnerDVO> winners, Double rewardsPerWinner,
			String rwdType) {
		Boolean isUpdated = Boolean.FALSE;
		Integer batchSize = 50000;
		try {
			Session session = CassandraConnector.getSession();
			BatchStatement batchStatement = new BatchStatement();
			PreparedStatement preparedStatement = session
					.prepare("INSERT INTO pt_livvx_contest_winner (clintid,custid,conid, rwdtype,rwdval ) VALUES (?,?, ?,?,?)");
			int count = 0;
			for (ContestWinnerDVO obj : winners) {
				batchStatement.add(
						preparedStatement.bind(obj.getClId(), obj.getCuId(), obj.getCoId(), rwdType, rewardsPerWinner));
				count++;
				if (count == batchSize) {
					session.execute(batchStatement);
					batchStatement.clear();
					count = 0;
				}
			}
			if (count > 0) {
				session.execute(batchStatement);
				batchStatement.clear();
				count = 0;
			}
			isUpdated = Boolean.TRUE;
		} catch (Exception ex) {
			ex.printStackTrace();
			isUpdated = Boolean.FALSE;
		}
		return isUpdated;
	}

	/**
	 * Truncate.
	 */
	public void truncate() {
		CassandraConnector.getSession().executeAsync("TRUNCATE TABLE liv_contest_winners");
	}

}