/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.cass.dao;

import java.util.Date;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.exceptions.DriverException;
import com.rtf.livt.dvo.ContestDVO;
import com.rtf.saas.cass.db.CassandraConnector;
import com.rtf.saas.exception.RTFDataAccessException;
import com.rtf.saas.util.GenUtil;

/**
 * The Class LiveContestCassDAO.
 */
public class LiveContestCassDAO {

	/**saveDisplayContest
	 * Gets the contest by contest id.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 * @return the contest by contest id
	 * @throws Exception
	 *             the exception
	 */
	public static ContestDVO getContestByContestId(String clientId, String contestId) throws Exception {
		ResultSet resultSet = null;
		ContestDVO contestDVO = null;
		try {
			resultSet = CassandraConnector.getSession().execute(
					"SELECT * from  pa_livvx_contest_mstr  WHERE clintid = ? AND conid=? ", clientId, contestId);
			if (resultSet != null) {
				for (Row rs : resultSet) {
					contestDVO = new ContestDVO();
					Date date = null;
					contestDVO.setCoId(rs.getString("conid"));
					contestDVO.setClId(rs.getString("clintid"));
					contestDVO.setAnsType(rs.getString("anstype"));
					contestDVO.setCoType(rs.getString("contype"));
					contestDVO.setClImgU(rs.getString("brndimgurl"));
					contestDVO.setImgU(rs.getString("cardimgurl"));
					contestDVO.setSeqNo(rs.getInt("cardseqno"));
					contestDVO.setCat(rs.getString("cattype"));
					contestDVO.setName(rs.getString("conname"));
					date = rs.getTimestamp("consrtdate");
					contestDVO.setStDate(date != null ? date.getTime() : null);
					contestDVO.setIsElimination(rs.getBool("iselimtype"));
					contestDVO.setqSize(rs.getInt("noofqns"));
					contestDVO.setSubCat(rs.getString("subcattype"));
					contestDVO.setThmColor(rs.getString("bgthemcolor"));
					contestDVO.setThmImgDesk(rs.getString("bgthemimgurl"));
					contestDVO.setThmImgMob(rs.getString("bgthemimgurlmob"));
					contestDVO.setPlayGameImg(rs.getString("playimgurl"));
					contestDVO.setThmId(rs.getInt("bgthembankid"));
					contestDVO.setIsSplitSummary(rs.getBool("issumrysplitable"));
					contestDVO.setIsLotEnbl(rs.getBool("islotryenabled"));
					contestDVO.setSumRwdType(rs.getString("sumryrwdtype"));
					contestDVO.setSumRwdVal(rs.getDouble("sumryrwdval"));
					contestDVO.setWinRwdType(rs.getString("lotryrwdtype"));
					contestDVO.setWinRwdVal(rs.getDouble("lotryrwdval"));
					contestDVO.setQueRwdType(rs.getString("qsnrwdtype"));
					contestDVO.setPartiRwdType(rs.getString("participantrwdtype"));
					contestDVO.setPartiRwdVal(rs.getDouble("participantrwdval"));
					contestDVO.setLastAction(rs.getString("lastaction"));
					contestDVO.setLastQue(rs.getInt("lastqsn"));
					contestDVO.setMigStatus(rs.getString("migrstatus"));
					contestDVO.setExtName(rs.getString("extconname"));
					contestDVO.setIsPwd(rs.getBool("ispwd"));
					contestDVO.setConPwd(rs.getString("conpwd"));

				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(ex);
		}
		return contestDVO;
	}

	/**
	 * Save contest cass.
	 *
	 * @param co
	 *            the co
	 * @return true, if successful
	 * @throws Exception
	 *             the exception
	 */
	public static boolean saveContestCass(ContestDVO co) throws Exception {
		boolean isInserted = false;
		StringBuffer sql = new StringBuffer();
		sql.append(
				"INSERT INTO pa_livvx_contest_mstr (clintid,conid,cattype,subcattype,conname,consrtdate,contype,anstype")
		.append(",iselimtype,noofqns,issumrysplitable,sumryrwdtype,sumryrwdval,islotryenabled")
		.append(",lotryrwdtype,lotryrwdval,brndimgurl,cardimgurl,cardseqno")
		.append(
				",bgthemcolor,bgthemimgurl,bgthemimgurlmob,bgthembankid,playimgurl,qsnrwdtype,participantrwdtype,participantrwdval")
		.append(",migrstatus,lastqsn,lastaction,extconname,isconactive,conrunningno,ispwd,conpwd) ")
		.append("values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		try {
			CassandraConnector.getSession().execute(sql.toString(),
					new Object[] { co.getClId(), co.getCoId(), co.getCat(), co.getSubCat(), co.getName(),
							new Date(co.getStDate()), co.getCoType(), co.getAnsType(), co.getIsElimination(),
							co.getqSize(), co.getIsSplitSummary(), co.getSumRwdType(), co.getSumRwdVal(),
							co.getIsLotEnbl(), co.getWinRwdType(), co.getWinRwdVal(), co.getClImgU(), co.getImgU(),
							co.getSeqNo(), co.getThmColor(), co.getThmImgDesk(), co.getThmImgMob(), co.getThmId(),
							co.getPlayGameImg(), co.getQueRwdType(), co.getPartiRwdType(), co.getPartiRwdVal(),
							co.getMigStatus(), co.getLastQue(), co.getLastAction(), co.getExtName(), co.getIsAct(),
							co.getRunCount(), co.getIsPwd(), co.getConPwd() });
			isInserted = true;
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(ex);
		}
		return isInserted;
	}
	
	

	/**
	 * Update contest cass.
	 *
	 * @param co
	 *            the co
	 * @return true, if successful
	 * @throws Exception
	 *             the exception
	 */
	public static boolean updateContestCass(ContestDVO co) throws Exception {
		boolean isUpdated = false;
		StringBuffer sql = new StringBuffer();
		sql.append("UPDATE pa_livvx_contest_mstr SET ")
		.append("cattype=?,subcattype=? ,conname=? ,consrtdate=?")
		.append(",contype=? ,anstype=? ,iselimtype=?")
		.append(",noofqns=? ,issumrysplitable=? ,sumryrwdtype=?")
		.append(",sumryrwdval=? ,islotryenabled=? ,lotryrwdtype=?")
		.append(",lotryrwdval=?  ,brndimgurl=?")
		.append(",cardimgurl=? ,cardseqno=? ,bgthemcolor=?")
		.append(",bgthemimgurl=? ,bgthemimgurlmob=? ,bgthembankid=?")
		.append(",playimgurl=? ,qsnrwdtype=?,ispwd=?,conpwd=? ")
		.append(",participantrwdtype=? ,participantrwdval=?, extconname=?, isconactive=? ")
		.append("WHERE clintid=? AND conid=?");
		try {
			CassandraConnector.getSession().execute(sql.toString(),
					new Object[] { co.getCat(), co.getSubCat(), co.getName(), new Date(co.getStDate()), co.getCoType(),
							co.getAnsType(), co.getIsElimination(), co.getqSize(), co.getIsSplitSummary(),
							co.getSumRwdType(), co.getSumRwdVal(), co.getIsLotEnbl(), co.getWinRwdType(),
							co.getWinRwdVal(), co.getClImgU(), co.getImgU(), co.getSeqNo(), co.getThmColor(),
							co.getThmImgDesk(), co.getThmImgMob(), co.getThmId(), co.getPlayGameImg(),
							co.getQueRwdType(), co.getIsPwd(), co.getConPwd(), co.getPartiRwdType(),
							co.getPartiRwdVal(), co.getExtName(), co.getIsAct(), co.getClId(), co.getCoId() });
			isUpdated = true;
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(ex);
		}
		return isUpdated;
	}

	/**
	 * Update contest status.
	 *
	 * @param co
	 *            the co
	 * @return true, if successful
	 * @throws Exception
	 *             the exception
	 */
	public static boolean updateContestStatus(ContestDVO co) throws Exception {
		boolean isUpdated = false;
		StringBuffer sql = new StringBuffer();
		sql.append("update pa_livvx_contest_mstr set ")
		.append("isconactive=? ")
		.append("WHERE clintid=? AND conid=?");
		try {
			CassandraConnector.getSession().execute(sql.toString(),
					new Object[] { co.getIsAct(), co.getClId(), co.getCoId() });
			isUpdated = true;
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(ex);
		}
		return isUpdated;
	}

	/**
	 * Update contest state.
	 *
	 * @param co
	 *            the co
	 * @return true, if successful
	 * @throws Exception
	 *             the exception
	 */
	public static boolean updateContestState(ContestDVO co) throws Exception {
		boolean isUpdated = false;
		StringBuffer sql = new StringBuffer();
		sql.append("update pa_livvx_contest_mstr set ")
		.append("isconactive=?, lastqsn=?, lastaction=? ")
		.append("WHERE clintid=? AND conid=?");
		try {
			CassandraConnector.getSession().execute(sql.toString(),
					new Object[] { co.getIsAct(), co.getLastQue(), co.getLastAction(), co.getClId(), co.getCoId() });
			isUpdated = true;
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(ex);
		}
		return isUpdated;
	}

	/**
	 * Delete contest cass.
	 *
	 * @param clientId
	 *            the client id
	 * @param coId
	 *            the co id
	 * @return true, if successful
	 * @throws Exception
	 *             the exception
	 */
	public static boolean deleteContestCass(String clientId, String coId) throws Exception {
		boolean isUpdated = false;
		StringBuffer sql = new StringBuffer();
		sql.append("delete from  pa_livvx_contest_mstr WHERE clintid=? AND conid=?");
		try {
			CassandraConnector.getSession().execute(sql.toString(), new Object[] { clientId, coId });
			isUpdated = true;
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(ex);
		}
		return isUpdated;
	}
	
	
	
	
	
	/**
	 * Save contest display properties to cassandra.
	 *
	 * @param co
	 *            the co
	 * @return true, if successful
	 * @throws Exception
	 *             the exception
	 */
	public static boolean saveDisplayContest(ContestDVO co) throws Exception {
		boolean isInserted = false;
		StringBuffer sql = new StringBuffer();
		sql.append(
				"INSERT INTO pa_livvx_contest_host(clintid ,conid ,conname,consrtdate ,hostname ,hostimg ,caty ,subcaty) ")
		.append("values (?,?,?,?,?,?,?,?)");
		try {
			CassandraConnector.getSession().execute(sql.toString(),
					new Object[] { co.getClId(), co.getCoId(),co.getName(), new Date(co.getStDate()),co.getHostedBy(),co.getHstImgUrl(),co.getCat(),co.getSubCat() });
			isInserted = true;
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(ex);
		}
		return isInserted;
	}
	
	
	
	/**
	 * Update display contest  to cassandra.
	 *
	 * @param co
	 *            the co
	 * @return true, if successful
	 * @throws Exception
	 *             the exception
	 */
	public static boolean updateDisplayContest(ContestDVO co) throws Exception {
		boolean isUpdated = false;
		StringBuffer sql = new StringBuffer();
		sql.append("UPDATE pa_livvx_contest_host SET ")
		.append("conname=?,consrtdate=? ,hostname=? ,hostimg=?")
		.append(",caty=? ,subcaty=? WHERE clintid=? AND conid=?");
		try {
			CassandraConnector.getSession().execute(sql.toString(),
					new Object[] { co.getName(), new Date(co.getStDate()),co.getHostedBy(),co.getHstImgUrl(),co.getCat(),co.getSubCat(), co.getClId(), co.getCoId() });
			isUpdated = true;
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(ex);
		}
		return isUpdated;
	}
	
	
	
	
	
	/**
	 * Delete display contest  to cassandra.
	 *
	 * @param clientId
	 *            the client id
	 * @param coId
	 *            the co id
	 * @return true, if successful
	 * @throws Exception
	 *             the exception
	 */
	public static boolean deleteDisplayContest(String clientId, String coId) throws Exception {
		boolean isUpdated = false;
		StringBuffer sql = new StringBuffer();
		sql.append("delete from  pa_livvx_contest_host WHERE clintid=? AND conid=?");
		try {
			CassandraConnector.getSession().execute(sql.toString(), new Object[] { clientId, coId });
			isUpdated = true;
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(ex);
		}
		return isUpdated;
	}
	
	
	
	
	/**
	 * Delete display contest  to cassandra.
	 *
	 * @param clientId
	 *            the client id
	 * @param coId
	 *            the co id
	 * @return true, if successful
	 * @throws Exception
	 *             the exception
	 */
	public static boolean deleteAllDisplayContest(String clientId) throws Exception {
		boolean isUpdated = false;
		StringBuffer sql = new StringBuffer();
		sql.append("delete from  pa_livvx_contest_host WHERE clintid=?");
		try {
			CassandraConnector.getSession().execute(sql.toString(), new Object[] { clientId });
			isUpdated = true;
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(ex);
		}
		return isUpdated;
	}
	

}
