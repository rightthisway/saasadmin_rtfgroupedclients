/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.cass.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.SimpleStatement;
import com.datastax.driver.core.Statement;
import com.rtf.livt.dvo.ContestJackpotWinnerDVO;
import com.rtf.saas.cass.db.CassandraConnector;

/**
 * The Class LiveContestJackpotWinnerCassDAO.
 */
public class LiveContestJackpotWinnerCassDAO {

	/**
	 * Gets the jackpot winner by Q no.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 * @param qId
	 *            the q id
	 * @return the jackpot winner by Q no
	 */
	public static Map<String, ContestJackpotWinnerDVO> getJackpotWinnerByQNo(String clientId, String contestId,
			Integer qId) {
		final ResultSet results = CassandraConnector.getSession().execute(
				"SELECT * from pt_livvx_contest_jakpot_winners WHERE clintid=? and conid=? and conqsnid=?", clientId,
				contestId, qId);
		Map<String, ContestJackpotWinnerDVO> winMap = new HashMap<String, ContestJackpotWinnerDVO>();
		Date date = null;
		if (results != null) {
			for (Row row : results) {
				ContestJackpotWinnerDVO win = new ContestJackpotWinnerDVO();
				win.setClId(row.getString("clintid"));
				win.setCoId(row.getString("conid"));
				win.setCuId(row.getString("custid"));
				date = row.getTimestamp("credate");
				win.setCrDate(date != null ? date.getTime() : null);
				win.setjType(row.getString("jcktype"));
				win.setqId(row.getInt("conqsnid"));
				win.setRwdType(row.getString("rwdType"));
				win.setqNo(row.getInt("qsnseqno"));
				win.setRwdVal(row.getDouble("rwdVal"));
				win.setStatus(row.getString("status"));
				winMap.put(win.getCuId(), win);

			}
		}
		return winMap;
	}

	/**
	 * Gets the jackpot winner by contest.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 * @return the jackpot winner by contest
	 */
	public static Map<String, ContestJackpotWinnerDVO> getJackpotWinnerByContest(String clientId, String contestId) {
		final ResultSet results = CassandraConnector.getSession().execute(
				"SELECT * from pt_livvx_contest_jakpot_winners WHERE clintid=? and conid=?", clientId, contestId);
		Map<String, ContestJackpotWinnerDVO> winMap = new HashMap<String, ContestJackpotWinnerDVO>();
		Date date = null;
		if (results != null) {
			for (Row row : results) {
				ContestJackpotWinnerDVO win = new ContestJackpotWinnerDVO();
				win.setClId(row.getString("clintid"));
				win.setCoId(row.getString("conid"));
				win.setCuId(row.getString("custid"));
				date = row.getTimestamp("credate");
				win.setCrDate(date != null ? date.getTime() : null);
				win.setjType(row.getString("jcktype"));
				win.setqId(row.getInt("conqsnid"));
				win.setRwdType(row.getString("rwdType"));
				win.setqNo(row.getInt("qsnseqno"));
				win.setRwdVal(row.getDouble("rwdVal"));
				win.setStatus(row.getString("status"));
				winMap.put(win.getCuId(), win);
			}
		}
		return winMap;
	}

	/**
	 * Save all.
	 *
	 * @param winners
	 *            the winners
	 */
	public static void saveAll(List<ContestJackpotWinnerDVO> winners) {

		for (ContestJackpotWinnerDVO obj : winners) {
			Statement statement = new SimpleStatement(
					"INSERT INTO pt_livvx_contest_jakpot_winners (clintid,conid,custid,conqsnid,jcktype,rwdtype,rwdval,credate,status ) VALUES (?,?,?,?,?,?,?,toTimestamp(now()),?)",
					obj.getClId(), obj.getCoId(), obj.getCuId(), obj.getqId(), obj.getjType(), obj.getRwdType(),
					obj.getRwdVal(), obj.getStatus());
			CassandraConnector.getSession().execute(statement);
		}
	}

}
