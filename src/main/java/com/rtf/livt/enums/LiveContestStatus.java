/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.enums;

/**
 * The Enum LiveContestStatus.
 */
public enum LiveContestStatus {

	/** The active. */
	ACTIVE,
	/** The live. */
	LIVE,
	/** The expired. */
	EXPIRED,
	/** The deleted. */
	DELETED;
}
