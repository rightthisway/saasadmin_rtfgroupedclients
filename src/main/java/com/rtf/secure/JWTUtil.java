/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.secure;

import java.time.Instant;
import java.util.Date;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;

/**
 * The Class JWTUtil.
 */
public class JWTUtil {

	/** The Constant secret. */
	private final static String secret = "evilftf8#2eDV1$@&290evilftf";

	/** The Constant expirywithinSecs. */
	private final static long expirywithinSecs = 60;

	/** The Constant JWT_HEADER_STRING. */
	public static final String JWT_HEADER_STRING = "X-AUTH-RTF";

	/**
	 * Creates the token.
	 *
	 * @param cau the logged in user Id
	 * @return the token string
	 */
	public static String createToken(String cau) {
		try {
			return JWT.create().withIssuer("rtflive").withClaim("cau", cau)
				//	.withExpiresAt(Date.from(Instant.now().plusSeconds(expirywithinSecs)))
					.sign(Algorithm.HMAC256(secret));
		} catch (JWTCreationException exception) {
			throw new RuntimeException("You need to enable Algorithm.HMAC256");
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}
	}

	/**
	 * Gets the client payload in token.
	 *
	 * @param token the token
	 * @return the client payload in token
	 */
	public static String getClientPayloadInToken(String token) {
		try {
			JWTVerifier verifier = JWT.require(Algorithm.HMAC256(secret)).withIssuer("rtflive").build();
			DecodedJWT jwt = verifier.verify(token);
			return jwt.getClaim("cau").asString();
		} catch (JWTDecodeException exception) {
			return null;
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Gets the client payload expiry date.
	 *
	 * @param token the token
	 * @return the client payload expiry date
	 */
	public static Date getClientPayloadExpiryDate(String token) {
		try {
			JWTVerifier verifier = JWT.require(Algorithm.HMAC256(secret)).withIssuer("rtflive").build();
			DecodedJWT jwt = verifier.verify(token);
			return jwt.getExpiresAt();
		} catch (JWTDecodeException exception) {
			return null;
		} catch (Exception e) {
			return null;
		}
	}

}