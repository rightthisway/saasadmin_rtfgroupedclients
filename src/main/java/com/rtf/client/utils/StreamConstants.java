/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.client.utils;

/**
 * The Class StreamConstants.
 */
public class StreamConstants {

	/** The facebook stream. */
	public static String FACEBOOK_STREAM = "FACEBOOK";
	
	/** The custom stream target enabled. */
	public static String CUSTOM_TARGET_ENABLED = "ENABLED";
	
	/** The custom stream target disabled. */
	public static String CUSTOM_TARGET_DISABLED = "DISABLED";
	
	/** The facebook stream. */
	public static String YOUTUBE_STREAM = "YOUTUBE";
	
	/** The stream action stop inprogress. */
	public static String STREAM_ACTION_STOP_INPROGRESS = "STOP_IN_PROGRESS";

	/** The stream action start inprogress. */
	public static String STREAM_ACTION_START_INPROGRESS = "START_IN_PROGRESS";

	/** The stream action stopped. */
	public static String STREAM_ACTION_STOPPED = "STOPPED";
	
	/** The stream action stopping. */
	public static String STREAM_ACTION_STOPPING = "STOPPING";

	/** The stream action started. */
	public static String STREAM_ACTION_STARTED = "STARTED";

	/** The stream action starting. */
	public static String STREAM_ACTION_STARTING = "STARTING";

	/** The stream action error. */
	public static String STREAM_ACTION_ERROR = "ERROR";

	/** The stream action stop. */
	public static String STREAM_ACTION_STOP = "STOP";

	/** The stream action start. */
	public static String STREAM_ACTION_START = "START";

	/** The invalid stream id. */
	public static String INVALID_STREAM_ID = "Invalid Stream Id";
	
	/** The invalid stream target ID. */
	public static String INVALID_STREAM_TARGET_ID = "Invalid Stream custom target ID.";
	
	/** The invalid stream target key. */
	public static String INVALID_STREAM_TARGET_KEY = "Invalid Stream custom target key.";
	
	/** The invalid stream target type. */
	public static String INVALID_STREAM_TARGET_TYPE = "Invalid Stream custom target type.";
	
	/** The invalid stream target url. */
	public static String INVALID_STREAM_TARGET_URL = "Invalid Stream custom target RTMP url.";
	
	/** The stream target created. */
	public static String STREAM_TARGET_CREATED = "Stream custom target created and attached to output succesfully.";
	
	/** The stream target update error. */
	public static String STREAM_TARGET_UPDATE_ERROR = "Error occured while updating stream custom target.";
	
	/** The stream target created. */
	public static String STREAM_TARGET_ASSIGNED = "Stream custom target  assigned to output successfully.";
	
	/** The stream target removed. */
	public static String STREAM_TARGET_DELETED = "Stream custom target removed from output successfully.";
	
	/** The stream target creation error. */
	public static String STREAM_TARGET_CREATION_ERROR = "Error occured while creating stream custom target.";
	
	/** The stream target deletion error. */
	public static String STREAM_TARGET_DELETION_ERROR = "Error occured while deleting stream custom target.";
	
	/** The stream target assignment error. */
	public static String STREAM_TARGET_ASSIGNMENT_ERROR = "Error occured while assigning stream custom target to output.";
	
	/** The stream target removing error. */
	public static String STREAM_TARGET_REMOVING_ERROR = "Error occured while removing stream custom target from output.";
	
	/** The stream target health error. */
	public static String STREAM_TARGET_HEALTH_ERROR = "Error occured while checking stream custom target health.";

}
