/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.client.utils;

import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

/**
 * The Class ExcelUtil - Utility class for generating excel file.
 */
public class ExcelUtil {

	/**
	 *  Create excel header cell with header data.
	 *
	 * @param headerList the header list of String
	 * @param ssSheet    the Sheet
	 */
	public static void generateExcelHeaderRow(List<String> headerList, Sheet ssSheet) {
		int j = 0;
		Row rowhead = ssSheet.createRow((int) 0);
		for (String header : headerList) {
			rowhead.createCell((int) j).setCellValue(header);
			j++;
		}
	}

	/**
	 * Create excel cell with data.
	 *
	 * @param dataList the data list of Object
	 * @param ssSheet  the Sheet
	 */
	public static void generateExcelData(List<Object[]> dataList, Sheet ssSheet) {
		int j = 0;
		int rowCount = 1;
		for (Object[] dataObj : dataList) {
			Row rowhead = ssSheet.createRow((int) rowCount);
			rowCount++;
			j = 0;
			for (Object data : dataObj) {
				if (data != null) {
					rowhead.createCell((int) j).setCellValue(data.toString());
				} else {
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
		}
	}

	/**
	 * Create the excel string cell with value.
	 *
	 * @param o       the Object
	 * @param j       the int
	 * @param rowhead the Row
	 * @return the excel string cell
	 */
	public static void getExcelStringCell(Object o, int j, Row rowhead) {
		try {
			if (o != null) {
				rowhead.createCell((int) j).setCellValue(o.toString());
			} else {
				rowhead.createCell((int) j).setCellValue("");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the excel cell with string value with specified style.
	 *
	 * @param o       the Object
	 * @param j       the int
	 * @param rowhead the Row
	 * @param style   the CellStyle
	 * @return the excel string cell red
	 */
	public static void getExcelStringCellRed(Object o, int j, Row rowhead, CellStyle style) {
		try {
			if (o != null) {
				Cell cell = rowhead.createCell((int) j);
				cell.setCellValue(o.toString());
				cell.setCellStyle(style);
			} else {
				Cell cell = rowhead.createCell((int) j);
				cell.setCellValue("");
				cell.setCellStyle(style);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the excel date cell with value with specified style.
	 *
	 * @param o       the Object
	 * @param j       the int
	 * @param rowhead the Row
	 * @param style   the CellStyle
	 * @return the excel date cell
	 */
	public static void getExcelDateCell(Object o, int j, Row rowhead, CellStyle style) {
		try {

			if (o != null) {
				Cell cell = rowhead.createCell((int) j);
				cell.setCellValue(o.toString());
				cell.setCellStyle(style);
			} else {
				rowhead.createCell((int) j).setCellValue("");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
