/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.client.utils;

import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import com.rtf.client.dvo.CustomerCartUADVO;

/**
 * The Class UAReportUtil.
 */
public class UAReportUtil {

	/**
	 * Generate customer cart detail report excel file to export.
	 *
	 * @param dataList the data list
	 * @param workbook the workbook
	 * @return the SXSSF workbook
	 */
	public static SXSSFWorkbook generateExcelForExport(List<CustomerCartUADVO> dataList, SXSSFWorkbook workbook) {
		try {
			int j = 0;
			int rowCount = 1;
			List<String> headerList = new ArrayList<String>();
			headerList.add("Customer ID");
			headerList.add("Product Id");
			headerList.add("Product Name");
			headerList.add("Mileage Points Used");
			Sheet ssSheet2 = workbook.createSheet("CartDetails");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet2);
			j = 0;
			rowCount = 1;
			for (CustomerCartUADVO obj : dataList) {
				Row rowhead = ssSheet2.createRow((int) rowCount);
				rowCount++;
				j = 0;
				ExcelUtil.getExcelStringCell(obj.getCuId(), j, rowhead);
				j++;
				ExcelUtil.getExcelStringCell(obj.getPid(), j, rowhead);
				j++;
				ExcelUtil.getExcelStringCell(obj.getPname(), j, rowhead);
				j++;
				ExcelUtil.getExcelStringCell(obj.getMlpt(), j, rowhead);
				j++;
			}
		} catch (Exception e) {
			e.printStackTrace();

		}
		return workbook;
	}

}
