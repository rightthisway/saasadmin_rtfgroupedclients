/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.client.service;

import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import com.rtf.client.dvo.ClientStreamDVO;
import com.rtf.client.sql.dao.ClientStreamDAO;
import com.rtf.saas.exception.RTFServiceAccessException;
import com.rtf.wowza.cloudsdk.client.model.IndexLiveStream;
import com.rtf.wowza.cloudsdk.client.model.LiveStream;
import com.rtf.wowza.cloudsdk.client.model.LiveStreamState;
import com.rtf.wowza.cloudsdk.client.model.LiveStreamState.StateEnum;
import com.rtf.wowza.cloudsdk.client.model.LiveStreams;
import com.rtf.wowza.cloudsdk.client.model.StreamTargetProperty;
import com.rtf.wowza.cloudsdk.client.reference.LiveStreams.CreateALiveStream;
import com.rtf.wowza.cloudsdk.client.reference.LiveStreams.FetchAllLiveStreams;
import com.rtf.wowza.cloudsdk.client.reference.StreamTargets.ConfigureAPropertyForAStreamTarget;

/**
 * The Class WowzaStreamService.
 */
public class WowzaStreamService {

	/**
	 * Createa live stream.
	 *
	 * @param clientId
	 *            the client id
	 * @return the string
	 * @throws Exception
	 *             the exception
	 */
	public static ClientStreamDVO createaLiveStream(String clientId) throws RTFServiceAccessException {
		LiveStream liveStream = null;
		ClientStreamDVO dvo = null;
		try {
			ClientStreamDVO clientStream = ClientStreamDAO.getActiveClientStreamByClientIdAndStreamEngine(clientId,
					"WOWZA");
			if (clientStream != null) {
				throw new RTFServiceAccessException("Stream Already created for this client : " + clientId);
			}

			liveStream = new LiveStream(); 
			liveStream.setName("RTF-" + clientId);
			liveStream.setAspectRatioHeight(1280);
			liveStream.setAspectRatioWidth(720);
			liveStream.setBillingMode(LiveStream.BillingModeEnum.PAY_AS_YOU_GO);
			liveStream.setBroadcastLocation(LiveStream.BroadcastLocationEnum.US_EAST_S_CAROLINA);
			liveStream.setClosedCaptionType(LiveStream.ClosedCaptionTypeEnum.NONE);
			liveStream.setDeliveryMethod(LiveStream.DeliveryMethodEnum.PUSH);
			liveStream.setDeliveryType(LiveStream.DeliveryTypeEnum.SINGLE_BITRATE);
			liveStream.setEncoder(LiveStream.EncoderEnum.OTHER_RTMP);
			liveStream.setHostedPage(false);

			liveStream.setTargetDeliveryProtocol(LiveStream.TargetDeliveryProtocolEnum.HTTPS);
			liveStream.setRecording(false);
			liveStream.setTranscoderType(LiveStream.TranscoderTypeEnum.TRANSCODED);

			liveStream.setUseStreamSource(false);
			List<String> deliveryProtocols = new ArrayList<String>();
			deliveryProtocols.add("rtmp");
			deliveryProtocols.add("rtsp");
			deliveryProtocols.add("wowz");
			liveStream.setDeliveryProtocols(deliveryProtocols);

			liveStream.lowLatency(true);
			liveStream.playerCountdown(false);
			liveStream.playerResponsive(false);
			liveStream.playerType("wowza_player");
			liveStream.playerWidth(640);
			liveStream.recording(false);
			liveStream.useStreamSource(false);
			liveStream.setTargetDeliveryProtocol(LiveStream.TargetDeliveryProtocolEnum.HTTPS);
			liveStream.setDisableAuthentication(true);

			liveStream = CreateALiveStream.createALiveStream(liveStream);

			dvo = new ClientStreamDVO();
			dvo.setClintid(clientId);
			dvo.setStreamengine("WOWZA");
			dvo.setStreamname(liveStream.getName());
			dvo.setStreamid(liveStream.getId());
			dvo.setTargetid(liveStream.getStreamTargets().get(0).getId());

			dvo.setSourceurl(liveStream.getSourceConnectionInformation().getPrimaryserver());
			dvo.setPort(liveStream.getSourceConnectionInformation().getHostport());
			dvo.setIsAuthDisabled(liveStream.getSourceConnectionInformation().getDisableauthentication());
			dvo.setUsrname(liveStream.getSourceConnectionInformation().getUsername());
			dvo.setPassword(liveStream.getSourceConnectionInformation().getPassword());

			dvo.setPlaybackurl(liveStream.getPlayerHlsPlaybackUrl());
			dvo.setHostedurl(liveStream.getHostedPageUrl());
			dvo.setStatus("ACTIVE");
			dvo.setStreamstatus("STOPPED");
			dvo.setCreBy("AUTO");
			dvo.setCreDate(new Date());
			ClientStreamDAO.saveClientStreamDets(clientId, dvo);

		} catch (Exception e) {
			throw new RTFServiceAccessException("Error occured while creating live stream.", e);
		}

		return dvo;
	}

	/**
	 * Update live stream target properties.
	 *
	 * @param streamId
	 *            the stream id
	 * @throws Exception
	 *             the exception
	 */
	public static void updateLiveStreamTargetProperties(String streamId) throws Exception {

		StreamTargetProperty newProp = new StreamTargetProperty();
		newProp.setKey(StreamTargetProperty.KeyEnum.CHUNKSIZE);
		newProp.setSection(StreamTargetProperty.SectionEnum.HLS);
		newProp.setValue("2");
		ConfigureAPropertyForAStreamTarget.configureAPropertyForAStreamTarget(streamId, newProp);

		newProp = new StreamTargetProperty();
		newProp.setKey(StreamTargetProperty.KeyEnum.PLAYLISTSECONDS);
		newProp.setSection(StreamTargetProperty.SectionEnum.PLAYLIST);
		newProp.setValue("6");
		ConfigureAPropertyForAStreamTarget.configureAPropertyForAStreamTarget(streamId, newProp);
	}

	/**
	 * Check running stream details.
	 *
	 * @throws Exception
	 *             the exception
	 */
	public static void checkRunningStreamDetails() throws Exception {

		Integer page = 1;
		Integer perPage = 100;
		List<ClientStreamDVO> runningStreams = new ArrayList<ClientStreamDVO>();
		boolean hasNexTpage = true;
		while (hasNexTpage) {
			LiveStreams liveStreams = FetchAllLiveStreams.fetchAllLiveStreams(page, perPage);
			List<IndexLiveStream> streamList = liveStreams.getLiveStreams();

			if (streamList != null && streamList.size() > 0) {
				SimpleDateFormat sdfAmerica = new SimpleDateFormat("dd-M-yyyy hh:mm:ss Z");
				sdfAmerica.setTimeZone(TimeZone.getTimeZone("America/New_York"));
				DateTimeFormatter format2 = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss Z");

				for (IndexLiveStream indexLiveStream : streamList) {
					LiveStreamState state = ClientStreamManagerService.fetchWOWZAStreamStatus(indexLiveStream.getId());
					if (!state.getState().toString().equals(StateEnum.STARTED.toString())) {
						ClientStreamDVO runningStream = ClientStreamDAO
								.getClientStreamByStreamIdandStreamEngine(indexLiveStream.getId(), "WOWZA");
						if (runningStream == null) {
							runningStream = new ClientStreamDVO();
							runningStream.setStreamengine("WOWZA");
							runningStream.setStreamname(indexLiveStream.getName());
							runningStream.setStreamid(indexLiveStream.getId());

						}
						String value = indexLiveStream.getUpdatedAt().format(format2);
						Date startedAt = sdfAmerica.parse(value);

						runningStream.setStreamstatus(state.getState().toString());
						runningStream.setUpdDt(startedAt);
						runningStreams.add(runningStream);
					}
				}
			}
			if (liveStreams.getLiveStreams() == null || liveStreams.getLiveStreams().size() < perPage) {
				hasNexTpage = false;
			} else {
				page = page + 1;
			}
		}
	}

	public static void main(String[] args) throws Exception {
		
		String clientId = "";
		//String streamName = "";
		ClientStreamDVO dvo = createaLiveStream(clientId);
		System.out.println("Stream Created : "+dvo.getStreamid()+" : "+dvo.getClintid());
		
		updateLiveStreamTargetProperties(dvo.getTargetid());
		
		System.out.println("Stream target Property Updated : "+dvo.getTargetid()+" : "+dvo.getStreamid()+" : "+dvo.getClintid());
		
	}
}
