package com.rtf.client.dvo;

/**
 * The Class ThemeFont.
 */
public class ThemeFont {
	
	/** The font name*/
	private String fntName;
	
	/** The font URL.*/
	private String fntUrl;

	/**
	 * Gets the fntName.
	 *
	 * @return the fntName.
	 */
	public String getFntName() {
		return fntName;
	}

	/**
	 * Sets the fntName.
	 *
	 * @param  the fntName. 
	 */
	public void setFntName(String fntName) {
		this.fntName = fntName;
	}

	/**
	 * Gets the fntUrl.
	 *
	 * @return the fntUrl.
	 */
	public String getFntUrl() {
		return fntUrl;
	}

	/**
	 * Sets the fntUrl.
	 *
	 * @param  the fntUrl. 
	 */
	public void setFntUrl(String fntUrl) {
		this.fntUrl = fntUrl;
	}
	

}
