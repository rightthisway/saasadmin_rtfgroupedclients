/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.client.dvo;

import com.rtf.saas.sql.dvo.SaasBaseDVO;

/**
 * The Class CustomerCartUADVO.
 */
public class CustomerCartUADVO extends SaasBaseDVO {

	/** The Product ID . */
	private String pid;

	/** The mlpt. */
	private Integer mlpt;

	/** The cu id. */
	private String cuId;

	/** The pname. */
	private String pname;

	/** The qty. */
	private String qty;

	/**
	 * Gets the pid.
	 *
	 * @return the pid
	 */
	public String getPid() {
		return pid;
	}

	/**
	 * Sets the pid.
	 *
	 * @param pid the new pid
	 */
	public void setPid(String pid) {
		this.pid = pid;
	}

	/**
	 * Gets the mlpt.
	 *
	 * @return the mlpt
	 */
	public Integer getMlpt() {
		return mlpt;
	}

	/**
	 * Sets the mlpt.
	 *
	 * @param mlpt the new mlpt
	 */
	public void setMlpt(Integer mlpt) {
		this.mlpt = mlpt;
	}

	/**
	 * Gets the cu id.
	 *
	 * @return the cu id
	 */
	public String getCuId() {
		return cuId;
	}

	/**
	 * Sets the cu id.
	 *
	 * @param cuId the new cu id
	 */
	public void setCuId(String cuId) {
		this.cuId = cuId;
	}

	/**
	 * Gets the pname.
	 *
	 * @return the pname
	 */
	public String getPname() {
		return pname;
	}

	/**
	 * Sets the pname.
	 *
	 * @param pname the new pname
	 */
	public void setPname(String pname) {
		this.pname = pname;
	}

	/**
	 * Gets the qty.
	 *
	 * @return the qty
	 */
	public String getQty() {
		return qty;
	}

	/**
	 * Sets the qty.
	 *
	 * @param qty the new qty
	 */
	public void setQty(String qty) {
		this.qty = qty;
	}
}
