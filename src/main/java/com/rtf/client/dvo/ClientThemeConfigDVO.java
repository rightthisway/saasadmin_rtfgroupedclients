package com.rtf.client.dvo;

import java.util.Date;

/**
 * The Class ClientThemeConfigDVO.
 */
public class ClientThemeConfigDVO {

	/** The Client ID.*/
	private String clId;
	
	/** The Product type.*/
	private String prodType;
	
	/** The Theme json.*/
	private String thmJson;
	
	/** The Theme json file URL.*/
	private String thmUrl;
	
	/** The Updated date.*/
	private Date updDate;
	
	/** The Updated by.*/
	private String updBy;
	
	
	
	/**
	 * Gets the clId.
	 *
	 * @return the clId.
	 */
	public String getClId() {
		return clId;
	}
	/**
	 * Sets the clId.
	 *
	 * @param  the clId. 
	 */
	public void setClId(String clId) {
		this.clId = clId;
	}
	
	/**
	 * Gets the prodType.
	 *
	 * @return the prodType.
	 */
	public String getProdType() {
		return prodType;
	}
	/**
	 * Sets the prodType.
	 *
	 * @param  prodType. 
	 */
	public void setProdType(String prodType) {
		this.prodType = prodType;
	}
	
	/**
	 * Gets the bscBgCol.
	 *
	 * @return the bscBgCol.
	 */
	public String getThmJson() {
		return thmJson;
	}
	/**
	 * Sets the bscBgCol.
	 *
	 * @param  the bscBgCol. 
	 */
	public void setThmJson(String thmJson) {
		this.thmJson = thmJson;
	}
	
	/**
	 * Gets the thmUrl.
	 *
	 * @return the thmUrl.
	 */
	public String getThmUrl() {
		return thmUrl;
	}
	/**
	 * Sets the updDate.
	 *
	 * @param  the updDate. 
	 */
	public void setThmUrl(String thmUrl) {
		this.thmUrl = thmUrl;
	}
	
	/**
	 * Gets the updDate.
	 *
	 * @return the updDate.
	 */
	public Date getUpdDate() {
		return updDate;
	}
	public void setUpdDate(Date updDate) {
		this.updDate = updDate;
	}
	
	/**
	 * Gets the updBy.
	 *
	 * @return the updBy.
	 */
	public String getUpdBy() {
		return updBy;
	}
	/**
	 * Sets the updBy.
	 *
	 * @param  the updBy. 
	 */
	public void setUpdBy(String updBy) {
		this.updBy = updBy;
	}
	
}
