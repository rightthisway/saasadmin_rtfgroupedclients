/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.client.dto;

import java.util.Date;

import com.rtf.saas.sql.dvo.SaasBaseDVO;

/**
 * The Class ClientStreamDetails.
 */
public class ClientStreamDetails extends SaasBaseDVO {

	/** The client ID. */
	private String clintid;

	/** The stream engine. */
	private String streamengine;

	/** The stream ID. */
	private String streamid;

	/** The stream name. */
	private String streamname;

	/** The target ID. */
	private String targetid;

	/** The source URL. */
	private String sourceurl;

	/** The port. */
	private Integer port;

	/** The is auth. */
	private Boolean isAuth;

	/** The username. */
	private String usrname;

	/** The password. */
	private String password;

	/** The playback URL. */
	private String playbackurl;

	/** The hosted URL. */
	private String hostedurl;

	/** The status. */
	private String status;

	/** The streamstatus. */
	private String streamstatus;

	/** The created date. */
	private Date credate;

	/** The upddated date. */
	private Date upddate;

	/** The created by. */
	private String creby;

	/** The updated by. */
	private String updby;

	/**
	 * Gets the client ID.
	 *
	 * @return the clintid
	 */
	public String getClintid() {
		return clintid;
	}

	/**
	 * Sets the client ID..
	 *
	 * @param clintid the new clintid
	 */
	public void setClintid(String clintid) {
		this.clintid = clintid;
	}

	/**
	 * Gets the streame ngine.
	 *
	 * @return the streamengine
	 */
	public String getStreamengine() {
		return streamengine;
	}

	/**
	 * Sets the stream engine.
	 *
	 * @param streamengine the new streamengine
	 */
	public void setStreamengine(String streamengine) {
		this.streamengine = streamengine;
	}

	/**
	 * Gets the stream ID.
	 *
	 * @return the streamid
	 */
	public String getStreamid() {
		return streamid;
	}

	/**
	 * Sets the stream ID.
	 *
	 * @param streamid the new streamid
	 */
	public void setStreamid(String streamid) {
		this.streamid = streamid;
	}

	/**
	 * Gets the stream name.
	 *
	 * @return the streamname
	 */
	public String getStreamname() {
		return streamname;
	}

	/**
	 * Sets the stream name.
	 *
	 * @param streamname the new streamname
	 */
	public void setStreamname(String streamname) {
		this.streamname = streamname;
	}

	/**
	 * Gets the target ID.
	 *
	 * @return the targetid
	 */
	public String getTargetid() {
		return targetid;
	}

	/**
	 * Sets the target ID.
	 *
	 * @param targetid the new targetid
	 */
	public void setTargetid(String targetid) {
		this.targetid = targetid;
	}

	/**
	 * Gets the source URL.
	 *
	 * @return the sourceurl
	 */
	public String getSourceurl() {
		return sourceurl;
	}

	/**
	 * Sets the source URL.
	 *
	 * @param sourceurl the new sourceurl
	 */
	public void setSourceurl(String sourceurl) {
		this.sourceurl = sourceurl;
	}

	/**
	 * Gets the port.
	 *
	 * @return the port
	 */
	public Integer getPort() {
		return port;
	}

	/**
	 * Sets the port.
	 *
	 * @param port the new port
	 */
	public void setPort(Integer port) {
		this.port = port;
	}

	/**
	 * Gets the checks if is auth.
	 *
	 * @return the checks if is auth
	 */
	public Boolean getIsAuth() {
		return isAuth;
	}

	/**
	 * Sets the checks if is auth.
	 *
	 * @param isAuth the new checks if is auth
	 */
	public void setIsAuth(Boolean isAuth) {
		this.isAuth = isAuth;
	}

	/**
	 * Gets the username.
	 *
	 * @return the usrname
	 */
	public String getUsrname() {
		return usrname;
	}

	/**
	 * Sets the username.
	 *
	 * @param usrname the new usrname
	 */
	public void setUsrname(String usrname) {
		this.usrname = usrname;
	}

	/**
	 * Gets the password.
	 *
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the password.
	 *
	 * @param password the new password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Gets the playback URL.
	 *
	 * @return the playbackurl
	 */
	public String getPlaybackurl() {
		return playbackurl;
	}

	/**
	 * Sets the playback URL.
	 *
	 * @param playbackurl the new playbackurl
	 */
	public void setPlaybackurl(String playbackurl) {
		this.playbackurl = playbackurl;
	}

	/**
	 * Gets the hosted URL.
	 *
	 * @return the hostedurl
	 */
	public String getHostedurl() {
		return hostedurl;
	}

	/**
	 * Sets the hosted URL.
	 *
	 * @param hostedurl the new hostedurl
	 */
	public void setHostedurl(String hostedurl) {
		this.hostedurl = hostedurl;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Gets the stream status.
	 *
	 * @return the streamstatus
	 */
	public String getStreamstatus() {
		return streamstatus;
	}

	/**
	 * Sets the stream status.
	 *
	 * @param streamstatus the new streamstatus
	 */
	public void setStreamstatus(String streamstatus) {
		this.streamstatus = streamstatus;
	}

	/**
	 * Gets the created date.
	 *
	 * @return the credate
	 */
	public Date getCredate() {
		return credate;
	}

	/**
	 * Sets the created date.
	 *
	 * @param credate the new credate
	 */
	public void setCredate(Date credate) {
		this.credate = credate;
	}

	/**
	 * Gets the updated date.
	 *
	 * @return the upddate
	 */
	public Date getUpddate() {
		return upddate;
	}

	/**
	 * Sets the updated date.
	 *
	 * @param upddate the new upddate
	 */
	public void setUpddate(Date upddate) {
		this.upddate = upddate;
	}

	/**
	 * Gets the created by.
	 *
	 * @return the creby
	 */
	public String getCreby() {
		return creby;
	}

	/**
	 * Sets the created by.
	 *
	 * @param creby the new creby
	 */
	public void setCreby(String creby) {
		this.creby = creby;
	}

	/**
	 * Gets the updated by.
	 *
	 * @return the updby
	 */
	public String getUpdby() {
		return updby;
	}

	/**
	 * Sets the updated by.
	 *
	 * @param updby the new updby
	 */
	public void setUpdby(String updby) {
		this.updby = updby;
	}

	/**
	 * Generate string format of all DVO properties.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "ClientStreamDVO [clintid=" + clintid + ", streamengine=" + streamengine + ", streamid=" + streamid
				+ ", streanname=" + streamname + ", targetid=" + targetid + ", sourceurl=" + sourceurl + ", port="
				+ port + ", isAuth=" + isAuth + ", usrname=" + usrname + ", password=" + password + ", playbackurl="
				+ playbackurl + ", hostedurl=" + hostedurl + ", status=" + status + ", streamstatus=" + streamstatus
				+ ", credate=" + credate + ", upddate=" + upddate + ", creby=" + creby + ", updby=" + updby + "]";
	}

}
