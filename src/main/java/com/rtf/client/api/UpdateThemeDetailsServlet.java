package com.rtf.client.api;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.xmlbeans.impl.jam.mutable.MSourcePosition;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rtf.aws.AWSFileService;
import com.rtf.aws.AwsS3Response;
import com.rtf.client.dto.ThemeDTO;
import com.rtf.client.dvo.ClientThemeConfigDVO;
import com.rtf.client.service.ClientThemeService;
import com.rtf.common.util.MessageConstant;
import com.rtf.common.util.RandomGenerator;
import com.rtf.common.util.SaaSAdminAppCache;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.FileUtil;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class StartWowzaStreamrServlet.
 */
@WebServlet("/updThmdtls.json")
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 10, // 10 MB
		maxFileSize = 1024 * 1024 * 20, // 20 MB
		maxRequestSize = 1024 * 1024 * 20) // 20 MB
public class UpdateThemeDetailsServlet extends RtfSaasBaseServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2619146508744324001L;

	/**
	 * Generate Http Response for Client.Response, data is sent in JSON format.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param rtfSaasBaseDTO
	 *            the rtf saas base DTO
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * update client theme details based on client id and product type.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String bscBgCol = request.getParameter("bscBgCol");
		String bscFntFmly = request.getParameter("bscFntFmly");
		String bscFntName = request.getParameter("bscFntName");
		String bscDotColor = request.getParameter("bscDotColor");

		String mdlBtnCol = request.getParameter("mdlBtnCol");
		String mdlBtnTxtCol = request.getParameter("mdlBtnTxtCol");

		String qsnStrkFnshCol = request.getParameter("qsnStrkFnshCol");
		String qsnStrkStrtngCol = request.getParameter("qsnStrkStrtngCol");
		String qsnBrdrCol = request.getParameter("qsnBrdrCol");
		String qsnAnsCol = request.getParameter("qsnAnsCol");
		String qsnRghtAnsCol = request.getParameter("qsnRghtAnsCol");
		String qsnWrngAnsCol = request.getParameter("qsnWrngAnsCol");
		String qsnSelAnsTxtCol = request.getParameter("qsnSelAnsTxtCol");
		String qsnRghtAnsTxtCol = request.getParameter("qsnRghtAnsTxtCol");
		String qsnWrngAnstxtCol = request.getParameter("qsnWrngAnstxtCol");

		String hdrCartCol = request.getParameter("hdrCartCol");
		String hdrUsrCntCol = request.getParameter("hdrUsrCntCol");
		String hdrLivIconHght = request.getParameter("hdrLivIconHght");

		String smrFntCol = request.getParameter("smrFntCol");
		String smrCrclCol = request.getParameter("smrCrclCol");
		String smrBoxCol = request.getParameter("smrBoxCol");
		String smrFntSize = request.getParameter("smrFntSize");
		String smrFntWght = request.getParameter("smrFntWght");
		String smrPrzFntSize = request.getParameter("smrPrzFntSize");
		String smrLogoX = request.getParameter("smrLogoX");
		String smrLogoY = request.getParameter("smrLogoY");
		String smrLogoScl = request.getParameter("smrLogoScl");
		String smrLogoSize = request.getParameter("smrLogoSize");
		String smrCrclFill = request.getParameter("smrCrclFill");
		String ltryWinCol = request.getParameter("ltryWinCol");
		String ltryCrclFill = request.getParameter("ltryCrclFill");

		String winFntCol = request.getParameter("winFntCol");
		String fnlstFntSize = request.getParameter("fnlstFntSize");
		String winFntSize = request.getParameter("winFntSize");
		String winFntWght = request.getParameter("winFntWght");
		String winBoxCol = request.getParameter("winBoxCol");
		String winCol = request.getParameter("winCol");
		String winPrizeFntCol = request.getParameter("winPrizeFntCol");

		String clId = request.getParameter("clId");
		String prodType = request.getParameter("prodType");
		String cau = request.getParameter("cau");

		ThemeDTO dto = new ThemeDTO();
		dto.setSts(0);

		try {

			if (GenUtil.isNullOrEmpty(clId)) {
				setClientMessage(dto, MessageConstant.INVALID_CLIENT, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(prodType)) {
				setClientMessage(dto, MessageConstant.INVALID_PROD_ID, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(cau)) {
				setClientMessage(dto, MessageConstant.INVALID_USER, null);
				generateResponse(request, response, dto);
				return;
			}

			if (GenUtil.isNullOrEmpty(bscBgCol)) {
				setClientMessage(dto, MessageConstant.INVALID_BASIC_BACKGROUND, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(bscFntFmly)) {
				setClientMessage(dto, MessageConstant.INVALID_BASIC_FONT_FAMILY, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(bscFntName)) {
				setClientMessage(dto, MessageConstant.INVALID_BASIC_FONT_NAME, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(bscDotColor)) {
				setClientMessage(dto, MessageConstant.INVALID_BASIC_DOT_COLOR, null);
				generateResponse(request, response, dto);
				return;
			}

			if (GenUtil.isNullOrEmpty(mdlBtnCol)) {
				setClientMessage(dto, MessageConstant.INVALID_MODAL_BTN_COLOR, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(mdlBtnTxtCol)) {
				setClientMessage(dto, MessageConstant.INVALID_MODAL_BTN_TEXT_COLOR, null);
				generateResponse(request, response, dto);
				return;
			}

			if (GenUtil.isNullOrEmpty(qsnStrkFnshCol)) {
				setClientMessage(dto, MessageConstant.INVALID_QUESTION_STROK_FINISHING, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(qsnStrkStrtngCol)) {
				setClientMessage(dto, MessageConstant.INVALID_QUESTION_STROK_STARTING, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(qsnBrdrCol)) {
				setClientMessage(dto, MessageConstant.INVALID_QUESTION_BORDER_COLOR, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(qsnAnsCol)) {
				setClientMessage(dto, MessageConstant.INVALID_QUESTION_ANSWER_COLOR, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(qsnRghtAnsCol)) {
				setClientMessage(dto, MessageConstant.INVALID_QUESTION_RIGHT_ANSWER_COLOR, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(qsnWrngAnsCol)) {
				setClientMessage(dto, MessageConstant.INVALID_QUESTION_WRONG_ANSWER_COLOR, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(qsnSelAnsTxtCol)) {
				setClientMessage(dto, MessageConstant.INVALID_SELECTED_ANSWER_TEXT_COLOR, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(qsnRghtAnsTxtCol)) {
				setClientMessage(dto, MessageConstant.INVALID_QUESTION_RIGHT_ANSWER_TEXT_COLOR, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(qsnWrngAnstxtCol)) {
				setClientMessage(dto, MessageConstant.INVALID_QUESTION_WRONG_ANSWER_TEXT_COLOR, null);
				generateResponse(request, response, dto);
				return;
			}

			if (GenUtil.isNullOrEmpty(hdrCartCol)) {
				setClientMessage(dto, MessageConstant.INVALID_HEADER_CART_COLOR, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(hdrUsrCntCol)) {
				setClientMessage(dto, MessageConstant.INVALID_HEADER_USER_COLOR, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(hdrLivIconHght)) {
				setClientMessage(dto, MessageConstant.INVALID_HEADER_HEIGHT, null);
				generateResponse(request, response, dto);
				return;
			}

			if (GenUtil.isNullOrEmpty(smrFntCol)) {
				setClientMessage(dto, MessageConstant.INVALID_SUMMARY_TEXT_COLOR, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(smrCrclCol)) {
				setClientMessage(dto, MessageConstant.INVALID_SUMMARY_CIRCLE_COLOR, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(smrBoxCol)) {
				setClientMessage(dto, MessageConstant.INVALID_SUMMARY_BOX, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(smrFntSize)) {
				setClientMessage(dto, MessageConstant.INVALID_SUMMARY_TEXT_FONT_SIZE, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(smrFntWght)) {
				setClientMessage(dto, MessageConstant.INVALID_SUMMARY_TEXT_WEIGHT, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(smrPrzFntSize)) {
				setClientMessage(dto, MessageConstant.INVALID_SUMMARY_WINNER_TEXT_SIZE, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(smrLogoX)) {
				setClientMessage(dto, MessageConstant.INVALID_SUMMARY_LOGO_X, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(smrLogoY)) {
				setClientMessage(dto, MessageConstant.INVALID_SUMMARY_LOGO_Y, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(smrLogoScl)) {
				setClientMessage(dto, MessageConstant.INVALID_SUMMARY_LOGO_SCALE, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(smrLogoSize)) {
				setClientMessage(dto, MessageConstant.INVALID_SUMMARY_LOGO_SIZE, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(smrCrclFill)) {
				setClientMessage(dto, MessageConstant.INVALID_SUMMARY_CIRCLE_FILL, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(ltryWinCol)) {
				setClientMessage(dto, MessageConstant.INVALID_LOTTERY_WINNER_COLOR, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(ltryCrclFill)) {
				setClientMessage(dto, MessageConstant.INVALID_LOTTERY_WINNER_FILL, null);
				generateResponse(request, response, dto);
				return;
			}

			if (GenUtil.isNullOrEmpty(winFntCol)) {
				setClientMessage(dto, MessageConstant.INVALID_WINNER_TEXT_COLOR, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(fnlstFntSize)) {
				setClientMessage(dto, MessageConstant.INVALID_FINALIST_FONT_SIZE, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(winFntSize)) {
				setClientMessage(dto, MessageConstant.INVALID_USER, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(winFntWght)) {
				setClientMessage(dto, MessageConstant.INVALID_WINNER_FONT_WEIGHT, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(winBoxCol)) {
				setClientMessage(dto, MessageConstant.INVALID_WINNER_BOX, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(winCol)) {
				setClientMessage(dto, MessageConstant.INVALID_LOTTERY_WINNER_COLOR, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(winPrizeFntCol)) {
				setClientMessage(dto, MessageConstant.INVALID_WINNER_PRIZE_TEXT, null);
				generateResponse(request, response, dto);
				return;
			}

			String sumLogoImg = request.getParameter("sumLogoImgURL");
			String videoPostrImg = request.getParameter("videoPostrImgURL");
			String logoColImg = request.getParameter("logoColImgURL");
			String logoCrclImg = request.getParameter("logoCrclImgURL");
			String logoWhtImg = request.getParameter("logoWhtImgURL");
			String dashbrdImg = request.getParameter("dashbrdImgURL");
			String nameTagImg = request.getParameter("nameTagImgURL");
			String cartImg = request.getParameter("cartImgURL");
			String userImg = request.getParameter("userImgURL");
			String cartColImg = request.getParameter("cartColImgURL");
			String userColImg = request.getParameter("userColImgURL");

			Map<String, String> fileMap = new HashMap<String, String>();

			try {
				for (Part part : request.getParts()) {
					Map<String, String> map = FileUtil.getOptValueFileNameMap(part);
					if (map == null) {
						continue;
					}
					for (Map.Entry<String, String> entry : map.entrySet()) {
						String key = entry.getKey();
						String val = entry.getValue();
						fileMap.put(key, val);
					}
				}
				System.out.println("MAP : =============== " + fileMap + "   ||||    " + fileMap.size());

			} catch (Exception e) {
				e.printStackTrace();
				setClientMessage(dto, MessageConstant.INVALID_THEME_IMGS, null);
				generateResponse(request, response, dto);
				return;
			}
			if (fileMap == null || fileMap.isEmpty()) {
				if (GenUtil.isNullOrEmpty(sumLogoImg) || GenUtil.isNullOrEmpty(videoPostrImg)
						|| GenUtil.isNullOrEmpty(logoColImg) || GenUtil.isNullOrEmpty(logoCrclImg)
						|| GenUtil.isNullOrEmpty(logoWhtImg) || GenUtil.isNullOrEmpty(dashbrdImg)
						|| GenUtil.isNullOrEmpty(nameTagImg) || GenUtil.isNullOrEmpty(cartImg)
						|| GenUtil.isNullOrEmpty(userImg) || GenUtil.isNullOrEmpty(cartColImg)
						|| GenUtil.isNullOrEmpty(userColImg)) {

					setClientMessage(dto, MessageConstant.THEME_IMG_MANDATORY, null);
					generateResponse(request, response, dto);
					return;
				}
			}

			if (fileMap.get("sumLogoImg") == null && (sumLogoImg == null || sumLogoImg.isEmpty())) {
				setClientMessage(dto, MessageConstant.SUMMARY_IMG_MANDATORY, null);
				generateResponse(request, response, dto);
				return;
			}
			if (fileMap.get("videoPostrImg") == null && (videoPostrImg == null || videoPostrImg.isEmpty())) {
				setClientMessage(dto, MessageConstant.VIDEO_POSTER_IMG_MANDATORY, null);
				generateResponse(request, response, dto);
				return;
			}
			if (fileMap.get("logoColImg") == null && (logoColImg == null || logoColImg.isEmpty())) {
				setClientMessage(dto, MessageConstant.LOGO_COLORED_IMG_MANDATORY, null);
				generateResponse(request, response, dto);
				return;
			}
			if (fileMap.get("logoCrclImg") == null && (logoCrclImg == null || logoCrclImg.isEmpty())) {
				setClientMessage(dto, MessageConstant.LOGO_CIRCLE_IMG_MANDATORY, null);
				generateResponse(request, response, dto);
				return;
			}
			if (fileMap.get("logoWhtImg") == null && (logoWhtImg == null || logoWhtImg.isEmpty())) {
				setClientMessage(dto, MessageConstant.LOGO_WHITE_IMG_MANDATORY, null);
				generateResponse(request, response, dto);
				return;
			}
			if (fileMap.get("dashbrdImg") == null && (dashbrdImg == null || dashbrdImg.isEmpty())) {
				setClientMessage(dto, MessageConstant.DASHBOARD_IMG_MANDATORY, null);
				generateResponse(request, response, dto);
				return;
			}
			if (fileMap.get("nameTagImg") == null && (nameTagImg == null || nameTagImg.isEmpty())) {
				setClientMessage(dto, MessageConstant.NAME_TAG_IMG_MANDATORY, null);
				generateResponse(request, response, dto);
				return;
			}
			if (fileMap.get("cartImg") == null && (cartImg == null || cartImg.isEmpty())) {
				setClientMessage(dto, MessageConstant.CART_IMG_MANDATORY, null);
				generateResponse(request, response, dto);
				return;
			}
			if (fileMap.get("userImg") == null && (userImg == null || userImg.isEmpty())) {
				setClientMessage(dto, MessageConstant.USER_IMG_MANDATORY, null);
				generateResponse(request, response, dto);
				return;
			}
			if (fileMap.get("cartColImg") == null && (cartColImg == null || cartColImg.isEmpty())) {
				setClientMessage(dto, MessageConstant.CART_COLORED_IMG_MANDATORY, null);
				generateResponse(request, response, dto);
				return;
			}
			if (fileMap.get("userColImg") == null && (userColImg == null || userColImg.isEmpty())) {
				setClientMessage(dto, MessageConstant.USER_COLORED_IMG_MANDATORY, null);
				generateResponse(request, response, dto);
				return;
			}

			JsonObject bscObj = new JsonObject();
			Map<String, String> bscMap = new HashMap<String, String>();
			bscObj.addProperty("background", bscBgCol);
			bscObj.addProperty("font_family", bscFntFmly);
			bscObj.addProperty("font_name", bscFntName);
			bscObj.addProperty("dot_color", bscDotColor);
			bscMap.put("background", bscBgCol);
			bscMap.put("font_family", bscFntFmly);
			bscMap.put("font_name", bscFntName);
			bscMap.put("dot_color", bscDotColor);

			JsonObject mdlObj = new JsonObject();
			Map<String, String> mdlMap = new HashMap<String, String>();
			mdlObj.addProperty("buttonColor", mdlBtnCol);
			mdlObj.addProperty("buttonText", mdlBtnTxtCol);
			mdlMap.put("buttonColor", mdlBtnCol);
			mdlMap.put("buttonText", mdlBtnTxtCol);

			JsonObject qsnObj = new JsonObject();
			Map<String, String> qsnMap = new HashMap<String, String>();
			qsnObj.addProperty("stroke_finishing", qsnStrkFnshCol);
			qsnObj.addProperty("stroke_starting", qsnStrkStrtngCol);
			qsnObj.addProperty("que_border_color", qsnBrdrCol);
			qsnObj.addProperty("active_answer_color", qsnAnsCol);
			qsnObj.addProperty("right_anser_color", qsnRghtAnsCol);
			qsnObj.addProperty("wrong_answer_color", qsnWrngAnsCol);
			qsnObj.addProperty("seleted_option_text", qsnSelAnsTxtCol);
			qsnObj.addProperty("correct_answer_text", qsnRghtAnsTxtCol);
			qsnObj.addProperty("wrong_answer_text", qsnWrngAnstxtCol);

			qsnMap.put("stroke_finishing", qsnStrkFnshCol);
			qsnMap.put("stroke_starting", qsnStrkStrtngCol);
			qsnMap.put("que_border_color", qsnBrdrCol);
			qsnMap.put("active_answer_color", qsnAnsCol);
			qsnMap.put("right_anser_color", qsnRghtAnsCol);
			qsnMap.put("wrong_answer_color", qsnWrngAnsCol);
			qsnMap.put("seleted_option_text", qsnSelAnsTxtCol);
			qsnMap.put("correct_answer_text", qsnRghtAnsTxtCol);
			qsnMap.put("wrong_answer_text", qsnWrngAnstxtCol);

			JsonObject hdrObj = new JsonObject();
			Map<String, String> hdrMap = new HashMap<String, String>();
			hdrObj.addProperty("cart_user_color", hdrCartCol);
			hdrObj.addProperty("cart_user", hdrUsrCntCol);
			hdrObj.addProperty("liveIconHeight", hdrLivIconHght);

			hdrMap.put("cart_user_color", hdrCartCol);
			hdrMap.put("cart_user", hdrUsrCntCol);
			hdrMap.put("liveIconHeight", hdrLivIconHght);

			JsonObject smrObj = new JsonObject();
			Map<String, String> smrMap = new HashMap<String, String>();
			smrObj.addProperty("summery_test_color", smrFntCol);
			smrObj.addProperty("summary_circle_color", smrCrclCol);
			smrObj.addProperty("summary_box", smrBoxCol);
			smrObj.addProperty("summary_name_Text_fontSize", smrFntSize);
			smrObj.addProperty("summary_name_Text_Weight", smrFntWght);
			smrObj.addProperty("summaryWinne_text_FontSize", smrPrzFntSize);
			smrObj.addProperty("logo_x", smrLogoX);
			smrObj.addProperty("logo_y", smrLogoY);
			smrObj.addProperty("logo_scale", smrLogoScl);
			smrObj.addProperty("logoiiconsize", smrLogoSize);
			smrObj.addProperty("summary_circle_fill", smrCrclFill);
			smrObj.addProperty("lottery_winner_color", ltryWinCol);
			smrObj.addProperty("lottery_circle_fill", ltryCrclFill);

			smrMap.put("summery_test_color", smrFntCol);
			smrMap.put("summary_circle_color", smrCrclCol);
			smrMap.put("summary_box", smrBoxCol);
			smrMap.put("summary_name_Text_fontSize", smrFntSize);
			smrMap.put("summary_name_Text_Weight", smrFntWght);
			smrMap.put("summaryWinne_text_FontSize", smrPrzFntSize);
			smrMap.put("logo_x", smrLogoX);
			smrMap.put("logo_y", smrLogoY);
			smrMap.put("logo_scale", smrLogoScl);
			smrMap.put("logoiiconsize", smrLogoSize);
			smrMap.put("summary_circle_fill", smrCrclFill);
			smrMap.put("lottery_winner_color", ltryWinCol);
			smrMap.put("lottery_circle_fill", ltryCrclFill);

			JsonObject winObj = new JsonObject();
			Map<String, String> winMap = new HashMap<String, String>();
			winObj.addProperty("grand_test_color", winFntCol);
			winObj.addProperty("finalist_text_fontsize", fnlstFntSize);
			winObj.addProperty("grandwinner_font_size", winFntSize);
			winObj.addProperty("grandwinner_font_weight", winFntWght);
			winObj.addProperty("grand_winner_box", winBoxCol);
			winObj.addProperty("grandPricewinner_text", winPrizeFntCol);
			winObj.addProperty("grandPricewinner", winCol);

			winMap.put("grand_test_color", winFntCol);
			winMap.put("finalist_text_fontsize", fnlstFntSize);
			winMap.put("grandwinner_font_size", winFntSize);
			winMap.put("grandwinner_font_weight", winFntWght);
			winMap.put("grand_winner_box", winBoxCol);
			winMap.put("grandPricewinner_text", winPrizeFntCol);
			winMap.put("grandPricewinner", winCol);

			String TMP_DIR = SaaSAdminAppCache.TMP_FOLDER;
			TMP_DIR = TMP_DIR + File.separator + clId + File.separator;
			String s3bucket = SaaSAdminAppCache.s3RtfMediaBucket;
			String imgFolder = SaaSAdminAppCache.s3saasfolder + "/" + clId + "/img";
			String jsonFolder = SaaSAdminAppCache.s3saasfolder + "/" + clId;
			String s3BaseUrl = "https://" + s3bucket + ".s3.amazonaws.com/";
			String clFrntUrl = "https://d3ohrbpndl3tex.cloudfront.net/";
			String jsonFilleName = clId + ".json";
			try {

				File fileSaveDir = new File(TMP_DIR);
				if (!fileSaveDir.exists()) {
					fileSaveDir.mkdirs();
				}

				for (Part part : request.getParts()) {
					Map<String, String> map = FileUtil.getOptValueFileNameMap(part);
					if (map == null) {
						continue;
					}
					Map.Entry<String, String> entry = map.entrySet().iterator().next();
					String key = entry.getKey();
					String val = entry.getValue();
					String EXTENSION = FileUtil.getFileExtension(val);
					String fileName = RandomGenerator.getAlphaNumericString(6)+"." + EXTENSION;

					part.write(TMP_DIR + File.separator + fileName);
					File tmpfile = new File(TMP_DIR + File.separator + fileName);
					AwsS3Response awsRsponse = AWSFileService.upLoadFileToS3(s3bucket, fileName, imgFolder, tmpfile);
					if (null != awsRsponse && awsRsponse.getStatus() != 1) {
						setClientMessage(dto, MessageConstant.THEME_IMG_UPLOAD_ERROR, null);
						generateResponse(request, response, dto);
						return;
					}
					if (key.equalsIgnoreCase("sumLogoImg")) {
						sumLogoImg = clFrntUrl + imgFolder + File.separator + fileName;
					} else if (key.equalsIgnoreCase("videoPostrImg")) {
						videoPostrImg = clFrntUrl + imgFolder + File.separator + fileName;
					} else if (key.equalsIgnoreCase("logoColImg")) {
						logoColImg = clFrntUrl + imgFolder + File.separator + fileName;
					} else if (key.equalsIgnoreCase("logoCrclImg")) {
						logoCrclImg = clFrntUrl + imgFolder + File.separator + fileName;
					} else if (key.equalsIgnoreCase("logoWhtImg")) {
						logoWhtImg = clFrntUrl + imgFolder + File.separator + fileName;
					} else if (key.equalsIgnoreCase("dashbrdImg")) {
						dashbrdImg = clFrntUrl + imgFolder + File.separator + fileName;
					} else if (key.equalsIgnoreCase("nameTagImg")) {
						nameTagImg = clFrntUrl + imgFolder + File.separator + fileName;
					} else if (key.equalsIgnoreCase("cartImg")) {
						cartImg = clFrntUrl + imgFolder + File.separator + fileName;
					} else if (key.equalsIgnoreCase("userImg")) {
						userImg = clFrntUrl + imgFolder + File.separator + fileName;
					} else if (key.equalsIgnoreCase("cartColImg")) {
						cartColImg = clFrntUrl + imgFolder + File.separator + fileName;
					} else if (key.equalsIgnoreCase("userColImg")) {
						userColImg = clFrntUrl + imgFolder + File.separator + fileName;
					}
				}
			} catch (Exception ex) {
				ex.printStackTrace();
				setClientMessage(dto, MessageConstant.USER_COLORED_IMG_MANDATORY, null);
				generateResponse(request, response, dto);
				return;
			}

			JsonObject imgObj = new JsonObject();
			Map<String, String> imgMap = new HashMap<String, String>();
			imgObj.addProperty("summary_logo", sumLogoImg);
			imgObj.addProperty("video_poster", videoPostrImg);
			imgObj.addProperty("logo_colored", logoColImg);
			imgObj.addProperty("logo_circle", logoCrclImg);
			imgObj.addProperty("logo_white", logoWhtImg);
			imgObj.addProperty("dashboardImg", dashbrdImg);
			imgObj.addProperty("name_tag", nameTagImg);
			imgObj.addProperty("cart", cartImg);
			imgObj.addProperty("user", userImg);
			imgObj.addProperty("cart_colored", cartColImg);
			imgObj.addProperty("user_colored", userColImg);

			imgMap.put("summary_logo", sumLogoImg);
			imgMap.put("video_poster", videoPostrImg);
			imgMap.put("logo_colored", logoColImg);
			imgMap.put("logo_circle", logoCrclImg);
			imgMap.put("logo_white", logoWhtImg);
			imgMap.put("dashboardImg", dashbrdImg);
			imgMap.put("name_tag", nameTagImg);
			imgMap.put("cart", cartImg);
			imgMap.put("user", userImg);
			imgMap.put("cart_colored", cartColImg);
			imgMap.put("user_colored", userColImg);

			JsonObject jObject = new JsonObject();
			Map<String, Map<String, String>> jMap = new HashMap<String, Map<String, String>>();
			jObject.add("basic", bscObj);
			jObject.add("modal", mdlObj);
			jObject.add("question_box", qsnObj);
			jObject.add("header", hdrObj);
			jObject.add("summary", smrObj);
			jObject.add("grandwinner", winObj);
			jObject.add("images", imgObj);

			jMap.put("basic", bscMap);
			jMap.put("modal", mdlMap);
			jMap.put("question_box", qsnMap);
			jMap.put("header", hdrMap);
			jMap.put("summary", smrMap);
			jMap.put("grandwinner", winMap);
			jMap.put("images", imgMap);

			ClientThemeConfigDVO themConfig = ClientThemeService.getClientThemeConfig(clId, prodType);
			if (themConfig == null) {
				themConfig = new ClientThemeConfigDVO();
				themConfig.setProdType(prodType);
				themConfig.setClId(clId);
				themConfig.setUpdBy(cau);
				themConfig.setUpdDate(new Date());
				themConfig.setThmJson(GsonUtil.getJasksonObjMapper().writeValueAsString(jMap));
				themConfig.setThmUrl(s3BaseUrl + jsonFolder + "/" + jsonFilleName);

				File tmpfile1 = new File(TMP_DIR + jsonFilleName);
				tmpfile1.createNewFile();
				FileUtils.writeStringToFile(tmpfile1, GsonUtil.getJasksonObjMapper().writeValueAsString(jMap));
				AwsS3Response awsRsponse = AWSFileService.upLoadFileToS3(s3bucket, jsonFilleName, jsonFolder, tmpfile1);
				if (null != awsRsponse && awsRsponse.getStatus() != 1) {
					setClientMessage(dto, MessageConstant.THEME_IMG_UPLOAD_ERROR, null);
					generateResponse(request, response, dto);
					return;
				}
				ClientThemeService.saveClientThemeConfig(themConfig);
			} else {
				themConfig.setUpdBy(cau);
				themConfig.setUpdDate(new Date());
				themConfig.setThmJson(GsonUtil.getJasksonObjMapper().writeValueAsString(jMap));
				themConfig.setThmUrl(s3BaseUrl + jsonFolder + "/" + jsonFilleName);

				File tmpfile1 = new File(TMP_DIR + jsonFilleName);
				tmpfile1.createNewFile();
				FileUtils.writeStringToFile(tmpfile1, GsonUtil.getJasksonObjMapper().writeValueAsString(jMap));
				AwsS3Response awsRsponse = AWSFileService.upLoadFileToS3(s3bucket, jsonFilleName, jsonFolder, tmpfile1);
				if (null != awsRsponse && awsRsponse.getStatus() != 1) {
					setClientMessage(dto, MessageConstant.THEME_IMG_UPLOAD_ERROR, null);
					generateResponse(request, response, dto);
					return;
				}
				ClientThemeService.updateClientThemeConfig(themConfig);
			}
			dto.setSts(1);
			dto.setMsg(MessageConstant.THEME_DATA_UPDATED);
			generateResponse(request, response, dto);
			return;
		} catch (

		Exception e) {
			e.printStackTrace();
			generateResponse(request, response, dto);
			return;
		}
	}
}
