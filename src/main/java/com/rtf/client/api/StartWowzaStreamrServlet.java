/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.client.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.client.dto.ClientStreamDTO;
import com.rtf.client.dvo.ClientStreamDVO;
import com.rtf.client.service.ClientStreamManagerService;
import com.rtf.client.sql.dao.ClientStreamDAO;
import com.rtf.client.utils.MessageConstant;
import com.rtf.livt.dvo.ContestDVO;
import com.rtf.livt.firestore.FireStoreReferenceHolder;
import com.rtf.livt.firestore.LiveFirestore;
import com.rtf.livt.service.LiveContestService;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;
import com.rtf.user.dvo.ClientUserDVO;
import com.rtf.user.sql.dao.ClientUserDAO;


/**
 * The Class StartWowzaStreamrServlet.
 */
@WebServlet("/ststrm.json")
public class StartWowzaStreamrServlet extends RtfSaasBaseServlet  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Generate Http Response for Client.Response, data is sent in JSON format.
	 *
	 * @param request the request
	 * @param response the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * Start live video stream on wowza for specified client.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
		
		String clientId = request.getParameter("clId");			
		String cau = request.getParameter("cau");	
		String streamId = request.getParameter("sid");
	
		ClientStreamDTO dto = new ClientStreamDTO();
		dto.setSts(0);	
		
		try {
			if(clientId == null || clientId.isEmpty()){
				setClientMessage(dto , MessageConstant.INVALID_CLIENT , null);
				generateResponse(request, response , dto );
				return;
			}
			if(streamId == null || streamId.isEmpty()){
				setClientMessage(dto , MessageConstant.INVALID_STREAM , null);
				generateResponse(request, response , dto );
				return;
			}
			if(cau == null || cau.isEmpty()){
				setClientMessage(dto , MessageConstant.INVALID_USER , null);
				generateResponse(request, response , dto );
				return;
			}		
			ClientUserDVO user = ClientUserDAO.getClientUserByClientIdAndUserId(clientId, cau);
			if(user == null || user.getUserid() == null) {
				setClientMessage(dto , MessageConstant.INVALID_USER , null);
				generateResponse(request, response , dto );
				return;
			}
			
			ContestDVO condvo = LiveContestService.getEarliestContestByClientId(clientId);
			if(condvo == null) {
				setClientMessage(dto , MessageConstant.CONTEST_NOT_FOUND , null);
				generateResponse(request, response , dto );
				return;
			}
			if(!cau.equals(condvo.getCrBy())) {
				
				System.out.println("NOT ALLOWED TO START STREAM [starttreamservlet : current user ]" + cau + " [BUT CONTEST CREATED BY ]"  + condvo.getCrBy());
				setClientMessage(dto , MessageConstant.NEXT_CONTEST_NOT_SCHEDULED , null);
				generateResponse(request, response , dto );
				return;	
			}
			ClientStreamDVO sdvo = ClientStreamDAO.getClientStreamById(clientId, streamId);
			if(sdvo == null || sdvo.getStreamid() == null) {
				setClientMessage(dto , MessageConstant.INVALID_STREAM , null);
				generateResponse(request, response , dto );
				return;
			}
			
			try {
			
			Instant oldContestDate = Instant.ofEpochMilli(condvo.getStDate());
			Instant now = Instant.now();

			Duration dur = Duration.between(now,oldContestDate);
			System.out.println("dur.toHours() :"+dur.toMinutes());

			if(dur.toMinutes() > 0 && dur.toMinutes() >= 5){
				setClientMessage(dto, "Stream can be started not earlier than 5 minutes from scheduled Show Start time", null);
				generateResponse(request, response, dto);
				return; 
			}
		
			if(dur.toMinutes() < 0 && dur.toMinutes() <= -35){
				setClientMessage(dto, "Sorry. It's 35 minutes past your scheduled Show Start Time. You are not allowed to Start so late", null);
				generateResponse(request, response, dto);
				return; 
			}
			}catch(Exception ex ) {
				setClientMessage(dto, null, null);
				generateResponse(request, response, null);
				return;
			}
			
			ClientStreamDVO dvo = new ClientStreamDVO();			
			dvo.setClintid(clientId);
			dvo.setUpdBy(cau);
			dvo.setStreamid(streamId);
			dto.setDvo(dvo);
			System.out.println(" [starttreamservlet : current user ]" + cau);
			dto = ClientStreamManagerService.startStream(clientId,dto);		
			if (dto.getSts() == 0) {
				setClientMessage(dto, null, null);
				generateResponse(request, response, dto);
				return;
			}
			dto.setMsg(MessageConstant.STREAM_STARTING);
			List<LiveFirestore> firestores = FireStoreReferenceHolder.getReference(clientId);
			if (firestores == null || firestores.isEmpty()) {
				setClientMessage(dto, "Firestore configuration settings not found in db.", null);
				generateResponse(request, response, dto);
				return;
			}
			if (sdvo.getPlaybackurl() == null ) {
				setClientMessage(dto, "Playback configuration settings not found in db.", null);
				generateResponse(request, response, dto);
				return;
			}
			for (LiveFirestore firestore : firestores) {
				firestore.updateLiveStreamUrl(sdvo.getPlaybackurl());
			}		
			generateResponse(request, response, dto);
			return;
		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(dto, null, null);
			generateResponse(request, response, dto);
		}
		return;
	}

}
