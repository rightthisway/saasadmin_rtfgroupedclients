package com.rtf.client.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rtf.client.dto.ClientStreamDTO;
import com.rtf.client.dvo.ClientStreamDVO;
import com.rtf.client.sql.dao.ClientStreamDAO;
import com.rtf.client.utils.StreamConstants;
import com.rtf.common.util.GsonCustomConfig;
import com.rtf.livt.util.HTTPUtil;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;
import com.rtf.user.util.UserMsgConstants;

/**
 * The Class CreateStreamCustomTargetServlet.
 */
@WebServlet("/crtstrmtrgt.json")
public class CreateStreamCustomTargetServlet extends RtfSaasBaseServlet {

	
	private final String CREATE_CUSTOM_TARGET_API = "https://api.cloud.wowza.com/api/v1.6/stream_targets/custom";
	
	/**
	 * Generate Http Response for Client.Response, data is sent in JSON format.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}
	
	
	
	/**
	 * Create Live stream custom target and assign it to output.
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String clientId = request.getParameter("clId");
		String cau = request.getParameter("cau");
		String streamId = request.getParameter("sId");
		String tType = request.getParameter("tType");
		String rtmpUrl = request.getParameter("rtmpUrl");
		String sKey = request.getParameter("sKey");

		ClientStreamDTO dto = new ClientStreamDTO();
		dto.setSts(0);

		try {
			if (clientId == null || clientId.isEmpty()) {
				setClientMessage(dto, UserMsgConstants.INVALID_CLIENT, null);
				generateResponse(request, response, dto);
				return;
			}
			System.out.println("STREAM ID : "+streamId);
			if (streamId == null || streamId.isEmpty()) {
				setClientMessage(dto, StreamConstants.INVALID_STREAM_ID, null);
				generateResponse(request, response, dto);
				return;
			}
			if (tType == null || tType.isEmpty()) {
				setClientMessage(dto, StreamConstants.INVALID_STREAM_TARGET_TYPE, null);
				generateResponse(request, response, dto);
				return;
			}
			if (cau == null || cau.isEmpty()) {
				setClientMessage(dto, UserMsgConstants.INVALID_USER, null);
				generateResponse(request, response, dto);
				return;
			}
			if (rtmpUrl == null || rtmpUrl.isEmpty()) {
				setClientMessage(dto, StreamConstants.INVALID_STREAM_TARGET_URL, null);
				generateResponse(request, response, dto);
				return;
			}
			if (sKey == null || sKey.isEmpty()) {
				setClientMessage(dto, StreamConstants.INVALID_STREAM_TARGET_KEY, null);
				generateResponse(request, response, dto);
				return;
			}
			ClientStreamDVO sdvo = ClientStreamDAO.getClientStreamById(clientId, streamId);
			if (sdvo == null || sdvo.getStreamid() == null) {
				System.out.println("STREAM ID : "+sdvo);
				setClientMessage(dto, StreamConstants.INVALID_STREAM_ID, null);
				generateResponse(request, response, dto);
				return;
			}
			
			
			JsonObject chilObj = new JsonObject();
			chilObj.addProperty("name", tType);
			chilObj.addProperty("primary_url", rtmpUrl);
			chilObj.addProperty("provider", tType.equalsIgnoreCase("FACEBOOK")?"rtmps":"rtmp");
			chilObj.addProperty("stream_name", sKey);
			
			JsonObject obj = new JsonObject();
			obj.add("stream_target_custom", chilObj);
			
			String data = HTTPUtil.executeWowzaPostApi(new Gson().toJson(obj), CREATE_CUSTOM_TARGET_API);
			System.out.println("CREATE TARGET  :"+data);
			if(data == null || data.isEmpty()){
				setClientMessage(dto, StreamConstants.STREAM_TARGET_CREATION_ERROR, null);
				generateResponse(request, response, dto);
				return;
			}
			
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			JsonObject respObj =  jsonObject.getAsJsonObject("stream_target_custom");
			if(respObj==null){
				setClientMessage(dto, StreamConstants.STREAM_TARGET_CREATION_ERROR, null);
				generateResponse(request, response, dto);
				return;
			}
			String targetId  = respObj.get("id").getAsString();
			if(targetId==null || targetId.isEmpty()){
				setClientMessage(dto, StreamConstants.STREAM_TARGET_CREATION_ERROR, null);
				generateResponse(request, response, dto);
				return;
			}
			
			
			
			
			
			JsonObject chilObj1 = new JsonObject();
			chilObj1.addProperty("stream_target_id", targetId);
			chilObj1.addProperty("use_stream_target_backup_url", false);
			
			JsonObject obj1 = new JsonObject();
			obj1.add("output_stream_target", chilObj1);
			
			String data1 = HTTPUtil.executeWowzaPostApi(new Gson().toJson(obj1), "https://api.cloud.wowza.com/api/v1.6/transcoders/"+sdvo.getTrnscdrId()+"/outputs/"+sdvo.getOutputId()+"/output_stream_targets");
			System.out.println("ASSING TARGET : "+sdvo.getTrnscdrId()+"   |    "+sdvo.getOutputId());
			System.out.println("ASSING TARGET : "+data1);
			if(data1 == null || data1.isEmpty()){
				setClientMessage(dto, StreamConstants.STREAM_TARGET_ASSIGNMENT_ERROR, null);
				generateResponse(request, response, dto);
				return;
			}
			
			Gson gson1 = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject1 = gson1.fromJson(data1, JsonObject.class);
			JsonObject respObj1 =  jsonObject1.getAsJsonObject("output_stream_target");
			if(respObj1==null){
				setClientMessage(dto, StreamConstants.STREAM_TARGET_ASSIGNMENT_ERROR, null);
				generateResponse(request, response, dto);
				return;
			}
			String outStreamTargtId  = respObj1.get("id").getAsString();
			if(outStreamTargtId==null || outStreamTargtId.isEmpty()){
				setClientMessage(dto, StreamConstants.STREAM_TARGET_ASSIGNMENT_ERROR, null);
				generateResponse(request, response, dto);
				return;
			}
			
			Boolean isUpd = false;
			if(tType.equalsIgnoreCase(StreamConstants.FACEBOOK_STREAM)){
				sdvo.setFbRtmpsUrl(rtmpUrl);
				sdvo.setFbStrmKey(sKey);
				sdvo.setFbStrmSts(StreamConstants.CUSTOM_TARGET_ENABLED);
				sdvo.setUpdBy(cau);
				sdvo.setFbTrgtId(targetId);
				sdvo.setFbOutputTrgtId(outStreamTargtId);
				isUpd = ClientStreamDAO.updateFacebookStreamConfig(sdvo);
			}else if(tType.equalsIgnoreCase(StreamConstants.YOUTUBE_STREAM)){
				sdvo.setYtRtmpUrl(rtmpUrl);
				sdvo.setYtStrmKey(sKey);
				sdvo.setYtStrmSts(StreamConstants.CUSTOM_TARGET_ENABLED);
				sdvo.setUpdBy(cau);
				sdvo.setYtTrgtId(targetId);
				sdvo.setYtOutputTrgtId(outStreamTargtId);
				isUpd = ClientStreamDAO.updateYoutubeStreamConfig(sdvo);
			}else{
				setClientMessage(dto, StreamConstants.INVALID_STREAM_TARGET_TYPE, null);
				generateResponse(request, response, dto);
				return;
			}
			
			if(isUpd){
				dto.setSts(1);
				dto.setDvo(sdvo);
				dto.setMsg(StreamConstants.STREAM_TARGET_CREATED);
				generateResponse(request, response, dto);
				return;
			}

			setClientMessage(dto, null, null);
			generateResponse(request, response, dto);
			return;
			
		}catch (Exception e) {
			e.printStackTrace();
			setClientMessage(dto, null, null);
			generateResponse(request, response, dto);
			return;
		}
	}
	
}
