package com.rtf.client.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rtf.client.dto.ThemeDTO;
import com.rtf.client.dvo.ClientThemeConfigDVO;
import com.rtf.client.dvo.ClientThemeDVO;
import com.rtf.client.dvo.ThemeFont;
import com.rtf.client.service.ClientThemeService;
import com.rtf.common.util.MessageConstant;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class StartWowzaStreamrServlet.
 */
@WebServlet("/getThmdtls.json")
public class GetThemeDetailsServlet extends RtfSaasBaseServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5002397026889270368L;

	
	/**
	 * Generate Http Response for Client.Response, data is sent in JSON format.
	 *
	 * @param request the request
	 * @param response the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
		
	}

	
	/**
	 * Get client theme details based on client id and product type.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
		
		String clientId = request.getParameter("clId");
		String cau = request.getParameter("cau");
		String prodType = request.getParameter("prodType");
		
		ThemeDTO dto = new ThemeDTO();
		dto.setClId(clientId);
		dto.setSts(0);
		try {
			
			if (clientId == null || clientId.isEmpty()) {
				setClientMessage(dto, MessageConstant.INVALID_CLIENT, null);
				generateResponse(request, response, dto);
				return;
			}
			if (prodType == null || prodType.isEmpty()) {
				setClientMessage(dto, MessageConstant.INVALID_PROD_ID, null);
				generateResponse(request, response, dto);
				return;
			}
			if (cau == null || cau.isEmpty()) {
				setClientMessage(dto, MessageConstant.INVALID_USER, null);
				generateResponse(request, response, dto);
				return;
			}
			
			ClientThemeConfigDVO themeConfig = ClientThemeService.getClientThemeConfig(clientId, prodType);
			if(themeConfig == null || themeConfig.getThmJson()==null || themeConfig.getThmJson().trim().isEmpty()){
				ClientThemeDVO theme = new ClientThemeDVO();
				dto.setTheme(theme);
				dto.setSts(1);
				generateResponse(request, response, dto);
				return;
			}
			JsonObject json = null;
			try {
				json = new Gson().fromJson(themeConfig.getThmJson(), JsonObject.class);
			} catch (Exception e) {
				e.printStackTrace();
				setClientMessage(dto, MessageConstant.INVALID_CLIENT_THEME_DETAILS, null);
				generateResponse(request, response, dto);
				return;
			}
			
			List<ThemeFont> fonts = new ArrayList<ThemeFont>();
			ThemeFont font = new ThemeFont();
			font.setFntName("Open Sans");
			font.setFntUrl("https://fonts.googleapis.com/css2?family=Open+Sans:ital@1&display=swap");
			fonts.add(font);
			
			
			ClientThemeDVO theme = new ClientThemeDVO(json);
			dto.setTheme(theme);
			dto.setFonts(fonts);
			dto.setSts(1);
			generateResponse(request, response, dto);
			return;
		} catch (Exception e) {
			e.printStackTrace();
			generateResponse(request, response, dto);
			return;
		}
	}

}
