package com.rtf.client.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rtf.client.dto.ClientStreamDTO;
import com.rtf.client.dvo.ClientStreamDVO;
import com.rtf.client.sql.dao.ClientStreamDAO;
import com.rtf.client.utils.StreamConstants;
import com.rtf.common.util.GsonCustomConfig;
import com.rtf.livt.util.HTTPUtil;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;
import com.rtf.user.util.UserMsgConstants;

/**
 * The Class CreateStreamCustomTargetServlet.
 */
@WebServlet("/enblestrmtrgt.json")
public class EnableStreamCustomTargetServlet extends RtfSaasBaseServlet{

	
	private static final long serialVersionUID = -8873259665187894912L;

	/**
	 * Generate Http Response for Client.Response, data is sent in JSON format.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}
	
	
	/**
	 * Enable live stream custom target and assign it to output.
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String clientId = request.getParameter("clId");
		String cau = request.getParameter("cau");
		String streamId = request.getParameter("sId");
		String tType = request.getParameter("tType");
		String sKey = request.getParameter("sKey");

		ClientStreamDTO dto = new ClientStreamDTO();
		dto.setSts(0);

		try {
			if (clientId == null || clientId.isEmpty()) {
				setClientMessage(dto, UserMsgConstants.INVALID_CLIENT, null);
				generateResponse(request, response, dto);
				return;
			}
			if (streamId == null || streamId.isEmpty()) {
				setClientMessage(dto, StreamConstants.INVALID_STREAM_ID, null);
				generateResponse(request, response, dto);
				return;
			}
			if (tType == null || tType.isEmpty()) {
				setClientMessage(dto, StreamConstants.INVALID_STREAM_TARGET_TYPE, null);
				generateResponse(request, response, dto);
				return;
			}
			if (cau == null || cau.isEmpty()) {
				setClientMessage(dto, UserMsgConstants.INVALID_USER, null);
				generateResponse(request, response, dto);
				return;
			}
			ClientStreamDVO sdvo = ClientStreamDAO.getClientStreamById(clientId, streamId);
			if (sdvo == null || sdvo.getStreamid() == null) {
				setClientMessage(dto, StreamConstants.INVALID_STREAM_ID, null);
				generateResponse(request, response, dto);
				return;
			}
			
			
			if(sKey!=null && !sKey.isEmpty()){
				JsonObject updObj = new JsonObject();
				JsonObject updChildObj = new JsonObject();
				updChildObj.addProperty("stream_name", sKey);
				updObj.add("stream_target_custom", updChildObj);
				
				String cutTrgId = null;
				if(tType.equalsIgnoreCase(StreamConstants.FACEBOOK_STREAM)){
					cutTrgId = sdvo.getFbTrgtId();
					sdvo.setFbStrmKey(sKey);
				}else if(tType.equalsIgnoreCase(StreamConstants.YOUTUBE_STREAM)){
					cutTrgId = sdvo.getYtTrgtId();
					sdvo.setYtStrmKey(sKey);
				}
				
				
				String updData = HTTPUtil.executeWowzaPatchApi(new Gson().toJson(updObj), "https://api.cloud.wowza.com/api/v1.6/stream_targets/custom/"+cutTrgId);
				System.out.println(cutTrgId);
				System.out.println("UPDATE TARGET : "+updData);
				if(updData == null || updData.isEmpty()){
					setClientMessage(dto, StreamConstants.STREAM_TARGET_UPDATE_ERROR, null);
					generateResponse(request, response, dto);
					return;
				}
				
				Gson updGson = GsonCustomConfig.getGsonBuilder();
				JsonObject jsonObject1 = updGson.fromJson(updData, JsonObject.class);
				JsonObject updResp =  jsonObject1.getAsJsonObject("stream_target_custom");
				if(updResp==null){
					setClientMessage(dto, StreamConstants.STREAM_TARGET_UPDATE_ERROR, null);
					generateResponse(request, response, dto);
					return;
				}
				cutTrgId  = updResp.get("id").getAsString();
				if(cutTrgId==null || cutTrgId.isEmpty()){
					setClientMessage(dto, StreamConstants.STREAM_TARGET_UPDATE_ERROR, null);
					generateResponse(request, response, dto);
					return;
				}
			}
			
			
			JsonObject chilObj1 = new JsonObject();
			chilObj1.addProperty("stream_target_id", tType.equalsIgnoreCase(StreamConstants.FACEBOOK_STREAM)?sdvo.getFbTrgtId():sdvo.getYtTrgtId());
			chilObj1.addProperty("use_stream_target_backup_url", false);
			
			JsonObject obj1 = new JsonObject();
			obj1.add("output_stream_target", chilObj1);
			
			String data1 = HTTPUtil.executeWowzaPostApi(new Gson().toJson(obj1), "https://api.cloud.wowza.com/api/v1.6/transcoders/"+sdvo.getTrnscdrId()+"/outputs/"+sdvo.getOutputId()+"/output_stream_targets");
			System.out.println("ASSING STREAM : "+data1);
			if(data1 == null || data1.isEmpty()){
				setClientMessage(dto, StreamConstants.STREAM_TARGET_ASSIGNMENT_ERROR, null);
				generateResponse(request, response, dto);
				return;
			}
			
			Gson gson1 = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject1 = gson1.fromJson(data1, JsonObject.class);
			JsonObject respObj1 =  jsonObject1.getAsJsonObject("output_stream_target");
			if(respObj1==null){
				setClientMessage(dto, StreamConstants.STREAM_TARGET_ASSIGNMENT_ERROR, null);
				generateResponse(request, response, dto);
				return;
			}
			String outStreamTargtId  = respObj1.get("id").getAsString();
			if(outStreamTargtId==null || outStreamTargtId.isEmpty()){
				setClientMessage(dto, StreamConstants.STREAM_TARGET_ASSIGNMENT_ERROR, null);
				generateResponse(request, response, dto);
				return;
			}
			
			Boolean isUpd = false;
			if(tType.equalsIgnoreCase(StreamConstants.FACEBOOK_STREAM)){
				sdvo.setFbStrmSts("ENABLED");
				sdvo.setFbOutputTrgtId(outStreamTargtId);
				sdvo.setUpdBy(cau);
				isUpd = ClientStreamDAO.updateFacebookStreamConfig(sdvo);
			}else if(tType.equalsIgnoreCase(StreamConstants.YOUTUBE_STREAM)){
				sdvo.setYtStrmSts("ENABLED");
				sdvo.setYtOutputTrgtId(outStreamTargtId);
				sdvo.setUpdBy(cau);
				isUpd = ClientStreamDAO.updateYoutubeStreamConfig(sdvo);
			}else{
				setClientMessage(dto, StreamConstants.INVALID_STREAM_TARGET_TYPE, null);
				generateResponse(request, response, dto);
				return;
			}
			
			if(isUpd){
				dto.setSts(1);
				dto.setDvo(sdvo);
				dto.setMsg(StreamConstants.STREAM_TARGET_ASSIGNED);
				generateResponse(request, response, dto);
				return;
			}

			setClientMessage(dto, null, null);
			generateResponse(request, response, dto);
			return;
			
		}catch (Exception e) {
			e.printStackTrace();
			setClientMessage(dto, null, null);
			generateResponse(request, response, dto);
			return;
		}
	}
}
