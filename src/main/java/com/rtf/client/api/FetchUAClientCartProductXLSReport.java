/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.client.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import com.rtf.client.dto.ClientRewardReptUADTO;
import com.rtf.client.dvo.CustomerCartUADVO;
import com.rtf.client.sql.dao.UAClientCartDAO;
import com.rtf.client.utils.MessageConstant;
import com.rtf.client.utils.UAReportUtil;
import com.rtf.livt.dvo.ContestDVO;
import com.rtf.livt.service.LiveContestService;
import com.rtf.reports.utils.ReportsUtil;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;
import com.rtf.user.dvo.ClientUserDVO;
import com.rtf.user.sql.dao.ClientUserDAO;
import com.rtf.user.util.UserMsgConstants;


/**
 * The Class FetchUAClientCartProductXLSReport.
 */
@WebServlet("/cartpdxlsrpt.json")
public class FetchUAClientCartProductXLSReport extends RtfSaasBaseServlet {

	private static final long serialVersionUID = -8204401835665558381L;

	/**
	 * Generate Error Response for Client.Response, data is sent in JSON format.
	 *
	 * @param request the request
	 * @param response the response
	 * @param dto the dto
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void generateErrorResponse(HttpServletRequest request, HttpServletResponse response,
			ClientRewardReptUADTO dto) throws ServletException, IOException {
		Map<String, ClientRewardReptUADTO> map = new HashMap<String, ClientRewardReptUADTO>();
		map.put("cartrep", dto);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * Generate excel file containing customer cart data report for specified contest and client.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String clientId = request.getParameter("clId");		
		String cau = request.getParameter("cau");	
		String coId = request.getParameter("coId");
		ClientRewardReptUADTO dto = new ClientRewardReptUADTO();
		dto.setSts(0);
		try {
			if (clientId == null || clientId.isEmpty()) {
				dto.setSts(0);
				dto.setMsg(UserMsgConstants.INVALID_CLIENT);			
				generateErrorResponse(request, response, dto);
				return;
			}		
			if (cau == null || cau.isEmpty()) {
				dto.setSts(0);
				dto.setMsg(UserMsgConstants.INVALID_USER);				
				generateErrorResponse(request, response, dto);
				return;
			}
			ClientUserDVO user = ClientUserDAO.getClientUserByClientIdAndUserId(clientId, cau);
			if (user == null || user.getUserid() == null) {
				dto.setSts(0);
				dto.setMsg(UserMsgConstants.INVALID_USER);
				generateErrorResponse(request, response, dto);
				return;
			}			
			if (coId == null) {
				dto.setSts(0);
				dto.setMsg(MessageConstant.INVALID_CONTEST);
				generateErrorResponse(request, response, dto);
				return;
			}			
			ContestDVO contest = LiveContestService.getSQLContestByContestId(clientId,coId);
			if(contest==null || contest.getCoId() == null ){
				dto.setSts(0);
				dto.setMsg(MessageConstant.CONTEST_NOT_FOUND);				
				generateErrorResponse(request, response, dto);				
				return;
			}
			
			try {			
				
						
				List<CustomerCartUADVO> dataList =  UAClientCartDAO.fetchCustomerCartByClientContestForExport(clientId, coId);
				if (dataList == null || dataList.isEmpty()) {
					dto.setSts(0);
					dto.setMsg(MessageConstant.NO_RECORDS_FOUND);				
					generateErrorResponse(request, response, dto);
					return;
				}	
				SXSSFWorkbook workbook = new SXSSFWorkbook();		
				workbook =  UAReportUtil.generateExcelForExport(dataList  ,  workbook);				
				SimpleDateFormat dateTime = new SimpleDateFormat("dd-MM-yyyy");				
				generateExcelResponse(response, workbook , dateTime.format(new Date()));
				workbook.close();
			}catch(Exception ex) {
				dto.setSts(0);
				ex.printStackTrace();
				dto.setMsg(MessageConstant.GENERIC_ERROR_MSG);
				generateErrorResponse(request, response, dto);	
				return;
			}
		} catch (Exception e) {
			dto.setSts(0);
			e.printStackTrace();
			dto.setMsg(MessageConstant.GENERIC_ERROR_MSG);
			generateErrorResponse(request, response, dto);
		}
		return;
	}
	
	/**
	 * Generate excel response.
	 *
	 * @param response the response
	 * @param workbook the workbook
	 * @param condte the condte
	 * @throws Exception the exception
	 */
	public static void generateExcelResponse(HttpServletResponse response,SXSSFWorkbook workbook,String condte) throws Exception {
		response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		String fileName = "CustomerCartReport_" + condte + "." + ReportsUtil.EXCEL_EXTENSION;
		response.setHeader("Content-disposition","attachment; filename="+fileName);
		workbook.write(response.getOutputStream());
		workbook.close();
	
	}
	
	/**
	 * Generate Http Response for Client.Response, data is sent in JSON format.
	 *
	 * @param request the request
	 * @param response the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

}
