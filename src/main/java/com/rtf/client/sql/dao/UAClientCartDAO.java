/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.client.sql.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.rtf.client.dvo.CustomerCartUADVO;
import com.rtf.saas.sql.db.DatabaseConnections;


/**
 * The Class UAClientCartDAO.
 */
public class UAClientCartDAO {
	

	/**
	 * Fetch customer cart details by client and contest ID for export.
	 *
	 * @param clientId the client id
	 * @param conId the contest id
	 * @return CustomerCartUADVO the list
	 * @throws Exception the exception
	 */
	public static List<CustomerCartUADVO> fetchCustomerCartByClientContestForExport(String clientId, String conId) throws Exception {
		Connection conn = null;		
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<CustomerCartUADVO> cartList = null;	
		StringBuffer sql = new StringBuffer();
		sql.append(" select a.custid,a.mercprodid,a.buyunits,")
		.append("  b.pname,b.psel_min_prc from ")
		.append("  pt_shopm_customer_shopping_cart a ")
		.append("  join rtf_seller_product_items_mstr b on")
		.append("  a.clintid=b.clintid and a.mercid=b.sler_id and  a.mercprodid=b.slrpitms_id ")
		.append("  where a.cartstatus='ACTIVE'  AND a.clintid=? and a.conid=? ");
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());		
			ps.setString(1, clientId);			
			ps.setString(2, conId);				
			rs = ps.executeQuery();
			cartList = new ArrayList<CustomerCartUADVO>();
			Integer mileagePts = 0;
			Double prc = 0.0;
			while (rs.next()) {
				CustomerCartUADVO cdvo = new CustomerCartUADVO();				
				cdvo.setCuId(rs.getString("custid"));
				cdvo.setPid(rs.getString("mercprodid"));
				cdvo.setQty(rs.getString("buyunits"));
				cdvo.setPname(rs.getString("pname"));
				prc = rs.getDouble("psel_min_prc");
				if(prc == null ){
					prc = 0.0;
				}
				mileagePts = prc.intValue();
				cdvo.setMlpt(mileagePts);				
				cartList.add(cdvo);
			}
			return cartList;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return cartList;
	}
	
	
	

}
