/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.client.sql.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.rtf.client.dvo.ClientRegistrationDVO;
import com.rtf.saas.sql.db.DatabaseConnections;

/**
 * The Class ClientRegistrationDAO.
 */
public class ClientRegistrationDAO {

	/**
	 * Get the client registration details by client id.
	 *
	 * @param clId   the client id
	 * @param status the status
	 * @return ClientRegistrationDVO the client registration by client id
	 * @throws Exception the exception
	 */
	public static ClientRegistrationDVO getClientRegistrationByClientId(String clId, String status) throws Exception {

		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		ClientRegistrationDVO dvo = null;
		String sql = "SELECT * from client_registration where clintid=? and isActive=?";

		try {
			conn = DatabaseConnections.getSaasClientDetailsConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, clId);
			ps.setString(2, status);
			rs = ps.executeQuery();
			dvo = new ClientRegistrationDVO();
			while (rs.next()) {
				dvo.setClintid(rs.getString("clintid"));
				dvo.setSaasweburl(rs.getString("saasweburl"));
			}
			return dvo;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;

	}

}
