/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.client.sql.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.rtf.client.dvo.ClientStreamAuditDVO;
import com.rtf.saas.sql.db.DatabaseConnections;

/**
 * The Class ClientStreamAuditDAO.
 */
public class ClientStreamAuditDAO {
	
	/**
	 * Save live video stream audit details.
	 *
	 * @param clientId the client id
	 * @param dvo the dvo
	 * @return the boolean
	 * @throws Exception the exception
	 */
	public static Boolean saveStreamAuditDets(String clientId, ClientStreamAuditDVO dvo) throws Exception {
		Boolean isUpdated = false;
		Connection conn = null;
		PreparedStatement statement = null;
		String sql = "insert into client_stream_audit (clintid, streamid, streamaction,creby, credate,description) values (?,?,?,?,getDate() , ?) ";
		try {
			conn = DatabaseConnections.getSaasClientDetailsConnection();
			statement = conn.prepareStatement(sql);
			statement.setString(1, dvo.getClintid());
			statement.setString(2, dvo.getStreamid());
			statement.setString(3, dvo.getStreamaction());			
			statement.setString(4, dvo.getCreBy());
			statement.setString(5, dvo.getActionResponse());
			int affectedRows = statement.executeUpdate();
			if (affectedRows > 0) {
				isUpdated = true;
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				statement.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return isUpdated;
	}

}
