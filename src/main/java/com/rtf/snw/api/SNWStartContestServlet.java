/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.snw.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.ott.service.OTTQuestionBankService;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;
import com.rtf.snw.dto.SNWContestDTO;
import com.rtf.snw.dvo.ContestDVO;
import com.rtf.snw.dvo.SNWContestQuestionsDVO;
import com.rtf.snw.service.SNWContestQuestionsService;
import com.rtf.snw.service.SNWContestService;
import com.rtf.snw.util.SNWMessageConstant;
import com.rtf.user.util.UserMsgConstants;

/**
 * The Class SNWStartContestServlet.
 */
@WebServlet("/snwstartcontest.json")
public class SNWStartContestServlet extends RtfSaasBaseServlet {

	/**
	 * Serial version id
	 */
	private static final long serialVersionUID = 6463958742315726434L;

	/**
	 * Generate Http Response for Scratch & win contest Response data is sent in JSON format.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * start contest based on contest id.
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String clId = request.getParameter("clId");
		String coId = request.getParameter("coId");
		String cau = request.getParameter("cau");
		SNWContestDTO respDTO = new SNWContestDTO();
		respDTO.setSts(0);
		respDTO.setClId(clId);

		try {
			if (clId == null || clId.isEmpty()) {
				setClientMessage(respDTO, UserMsgConstants.INVALID_CLIENT, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (coId == null || coId.isEmpty()) {
				setClientMessage(respDTO, SNWMessageConstant.INVALID_CONTEST_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}
			ContestDVO liveContest = SNWContestService.getLiveContestsByClientId(clId);
			if (liveContest != null) {
				setClientMessage(respDTO,SNWMessageConstant.ONE_CONTEST_ONLY_IN_LIVE, null);
				generateResponse(request, response, respDTO);
				return;
			}
			ContestDVO contest = SNWContestService.getSQLContestByContestId(clId, coId);
			if (contest == null) {
				setClientMessage(respDTO, SNWMessageConstant.CONTEST_ID_NOT_EXISTS, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (contest.getIsAct() == 2) {
				setClientMessage(respDTO, SNWMessageConstant.SELECTED_CONTEST_IS_ALREADY_LIVE, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (contest.getIsAct() != 1 || !contest.getIsAct().equals(1)) {
				setClientMessage(respDTO, SNWMessageConstant.ACTIVE_CONTEST_ONLY_CAN_START, null);
				generateResponse(request, response, respDTO);
				return;
			}
			Date today = new Date();
			if (contest.getStDate() > today.getTime()) {
				setClientMessage(respDTO, SNWMessageConstant.CANT_START_FUTURE_DATE_CONTEST, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (contest.getEnDate() < today.getTime()) {
				setClientMessage(respDTO, SNWMessageConstant.CANT_START_PAST_END_DATE_CONTEST, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (contest.getqMode().equalsIgnoreCase(SNWMessageConstant.CONT_QUEST_MODE_FIXED)) {
				List<SNWContestQuestionsDVO> list = SNWContestQuestionsService
						.getcontestquestionsBycontestId(contest.getClId(), contest.getCoId());
				if (list == null || list.isEmpty()) {
					setClientMessage(respDTO,SNWMessageConstant.FIXED_CONTEST_WITH_NO_QUESTIONS,null);
					generateResponse(request, response, respDTO);
					return;
				}
			} else if (contest.getqMode().equalsIgnoreCase(SNWMessageConstant.CONT_QUEST_MODE_RANDOM)) {
				Integer count = OTTQuestionBankService.getQuestionsCountByCategorySubcategoryAndQuestionType(
						contest.getClId(), contest.getCat(), contest.getSubCat(), contest.getAnsType());
				if (count <= 0) {
					setClientMessage(respDTO,SNWMessageConstant.RANDOM_CONTEST_WITH_NO_QUESTIONS_IN_BANK,null);
					generateResponse(request, response, respDTO);
					return;
				}
			}
			contest.setUpBy(cau);
			contest.setIsAct(2);
			Boolean isUpdated = SNWContestService.updateContestStatus(contest);
			if (isUpdated) {
				respDTO.setSts(1);
				setClientMessage(respDTO, SNWMessageConstant.CONTEST_START_SUCCESS_MSG, null);
				generateResponse(request, response, respDTO);
				return;
			} else {
				respDTO.setSts(0);
				setClientMessage(respDTO, UserMsgConstants.COMMON_ERR_MSG, null);
				generateResponse(request, response, respDTO);
				return;
			}
		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(respDTO, UserMsgConstants.COMMON_ERR_MSG, null);
			generateResponse(request, response, respDTO);
		}
		return;

	}

}
