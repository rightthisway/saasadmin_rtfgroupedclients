/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.snw.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.ott.sql.dao.QuestionBankSQLDAO;
import com.rtf.ott.sql.dvo.QuestionBankDVO;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;
import com.rtf.snw.dto.SNWContestQuestionsDTO;
import com.rtf.snw.dvo.ContestDVO;
import com.rtf.snw.dvo.SNWContestQuestionsDVO;
import com.rtf.snw.service.SNWContestQuestionsService;
import com.rtf.snw.service.SNWContestService;
import com.rtf.snw.util.SNWMessageConstant;
import com.rtf.user.util.UserMsgConstants;

/**
 * The Class SNWAddContestQuestionsfromBankServlet.
 */
@WebServlet("/snwAddContQuestFromBank.json")
public class SNWAddContestQuestionsfromBankServlet extends RtfSaasBaseServlet {

	/**
	 * Serial version id
	 */
	private static final long serialVersionUID = 3359472707834247046L;

	/**
	 * Generate Http Response for Scratch & win contest question Response data is sent in JSON format.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param rtfSaasBaseDTO the rtf saas base DTO
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}

	/**
	 * Add Contest questions from question bank.
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String clId = request.getParameter("clId");
		String contId = request.getParameter("coId");
		String questionbnkIdStr = request.getParameter("qbId");
		String userName = request.getParameter("cau");

		SNWContestQuestionsDTO respDTO = new SNWContestQuestionsDTO();
		respDTO.setSts(0);
		respDTO.setClId(clId);

		try {
			if (clId == null || clId.isEmpty()) {
				setClientMessage(respDTO, UserMsgConstants.INVALID_CLIENT, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (contId == null || contId.isEmpty()) {
				setClientMessage(respDTO, SNWMessageConstant.INVALID_CONTEST_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (questionbnkIdStr == null || questionbnkIdStr.isEmpty()) {
				setClientMessage(respDTO, SNWMessageConstant.INVALID_QESTION_BANK_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (userName == null || userName.isEmpty()) {
				setClientMessage(respDTO, UserMsgConstants.INVALID_ADMIN_USER_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}
			Integer qbnkId = null;
			try {
				qbnkId = Integer.parseInt(questionbnkIdStr);
			} catch (Exception e) {

			}
			ContestDVO contest = SNWContestService.getSQLContestByContestId(clId, contId);
			if (contest == null) {
				setClientMessage(respDTO, SNWMessageConstant.CONTEST_ID_NOT_EXISTS, null);
				generateResponse(request, response, respDTO);
				return;
			}

			QuestionBankDVO questionbank = QuestionBankSQLDAO.getOLTXQuestionsFromQBankByquestionId(clId, qbnkId);
			if (questionbank == null) {
				setClientMessage(respDTO, SNWMessageConstant.QUESTION_BANK_ID_NOT_EXISTS, null);
				generateResponse(request, response, respDTO);
				return;
			}
			SNWContestQuestionsDVO contQuestionDb = SNWContestQuestionsService
					.getcontestquestionsByContestIdandQuestionBankId(clId, contId, qbnkId);
			if (contQuestionDb != null) {
				setClientMessage(respDTO, SNWMessageConstant.QUESTION_BANK_ID_ALREADY_EXISTS, null);
				generateResponse(request, response, respDTO);
				return;
			}

			if (!contest.getAnsType().equals(questionbank.getAnstype())) {
				setClientMessage(respDTO, SNWMessageConstant.QUESTION_TYPE_MISMATCH_WITH_CONT_QUEST_TYPE, null);
				generateResponse(request, response, respDTO);
				return;
			}

			SNWContestQuestionsDVO contQuestion = new SNWContestQuestionsDVO();
			contQuestion.setClintid(clId);
			contQuestion.setConid(contId);
			contQuestion.setQsntext(questionbank.getQtx());
			contQuestion.setOpa(questionbank.getOpa());
			contQuestion.setOpb(questionbank.getOpb());
			contQuestion.setOpc(questionbank.getOpc());
			contQuestion.setOpd(questionbank.getOpd());
			contQuestion.setCorans(questionbank.getCans());
			contQuestion.setQsnorient(questionbank.getqOrientn());
			contQuestion.setQbid(qbnkId);
			contQuestion.setCreby(userName);
			contQuestion.setCredate(new Date());
			contQuestion.setIsactive(Boolean.TRUE);

			Integer id = SNWContestQuestionsService.saveContestQuestions(clId, contQuestion);
			if (id == null) {
				setClientMessage(respDTO, SNWMessageConstant.CONT_QUEST_NOT_CREATED, null);
				generateResponse(request, response, respDTO);
				return;
			}

			respDTO.setSts(1);
			setClientMessage(respDTO, null, SNWMessageConstant.CONT_QUEST_CREATED_SUCCESS_MSG);
			generateResponse(request, response, respDTO);

		} catch (Exception e) {
			e.printStackTrace();

			setClientMessage(respDTO, UserMsgConstants.COMMON_ERR_MSG, null);
			generateResponse(request, response, respDTO);
			return;
		}

	}

}
