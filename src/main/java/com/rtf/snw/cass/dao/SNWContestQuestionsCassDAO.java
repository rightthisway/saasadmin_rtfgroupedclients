/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.snw.cass.dao;

import java.util.ArrayList;
import java.util.List;

import com.datastax.driver.core.BatchStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.exceptions.DriverException;
import com.rtf.ott.cass.dvo.CustomerAnswerDVO;
import com.rtf.saas.cass.db.CassandraConnector;
import com.rtf.saas.exception.RTFDataAccessException;
import com.rtf.saas.util.GenUtil;
import com.rtf.snw.dvo.SNWContestQuestionsDVO;

/**
 * The Class SNWContestQuestionsCassDAO.
 */
public class SNWContestQuestionsCassDAO {

	/**
	 * Gets the contest questioncontest id and question id.
	 *
	 * @param clientId   the client id
	 * @param conId      the con id
	 * @param questionId the question id
	 * @return the contest questioncontest id and question id
	 * @throws Exception the exception
	 */

	public static SNWContestQuestionsDVO getContestQuestioncontestIdAndQuestionId(String clientId, String conId,
			Integer questionId) throws Exception {

		ResultSet resultSet = null;
		SNWContestQuestionsDVO dvo = null;

		try {
			String keySpace = CassandraConnector.getRtfSassCassKeySpace();
			StringBuffer sql = new StringBuffer("SELECT * from  " + keySpace);
			sql.append("pa_snwtx_contest_question  WHERE clintid=? and conid=? and conqsnid = ? ");
			resultSet = CassandraConnector.getSession().execute(sql.toString(), clientId, conId, questionId);
			dvo = new SNWContestQuestionsDVO();
			if (resultSet != null) {
				for (Row row : resultSet) {
					dvo.setClintid(row.getString("clintid"));
					dvo.setConid(row.getString("conid"));
					dvo.setConqsnid(row.getInt("conqsnid"));
					dvo.setQsntext(row.getString("qsntext"));
					dvo.setOpa(row.getString("ansopta"));
					dvo.setOpb(row.getString("ansoptb"));
					dvo.setOpc(row.getString("ansoptc"));
					dvo.setOpd(row.getString("ansoptd"));
					dvo.setCorans(row.getString("corans"));
					dvo.setIsactive(row.getBool("isactive"));
					dvo.setQbid(row.getInt("qsnbnkid"));
					dvo.setQsnorient(row.getString("qsnorient"));
					dvo.setQsnseqno(row.getInt("qsnseqno"));

				}
			}

		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(ex);
		}
		return dvo;
	}

	/**
	 * Gets the all contest questions by contest id.
	 *
	 * @param clientId  the client id
	 * @param contestId the contest id
	 * @return the all contest questions by contest id
	 * @throws Exception the exception
	 */
	public static List<SNWContestQuestionsDVO> getAllContestQuestionsByContestId(String clientId, String contestId)
			throws Exception {

		ResultSet resultSet = null;
		List<SNWContestQuestionsDVO> list = new ArrayList<SNWContestQuestionsDVO>();
		try {
			String keySpace = CassandraConnector.getRtfSassCassKeySpace();
			StringBuffer sql = new StringBuffer(
					"SELECT * from  " + keySpace + "pa_snwtx_contest_question  " + "WHERE clintid=? and conid=? ");
			resultSet = CassandraConnector.getSession().execute(sql.toString(), clientId, contestId);

			SNWContestQuestionsDVO dvo = null;
			if (resultSet != null) {
				for (Row row : resultSet) {
					dvo = new SNWContestQuestionsDVO();
					dvo.setClintid(row.getString("clintid"));
					dvo.setConid(row.getString("conid"));
					dvo.setConqsnid(row.getInt("conqsnid"));
					dvo.setQsntext(row.getString("qsntext"));
					dvo.setOpa(row.getString("ansopta"));
					dvo.setOpb(row.getString("ansoptb"));
					dvo.setOpc(row.getString("ansoptc"));
					dvo.setOpd(row.getString("ansoptd"));
					dvo.setCorans(row.getString("corans"));
					dvo.setIsactive(row.getBool("isactive"));
					dvo.setQbid(row.getInt("qsnbnkid"));
					dvo.setQsnorient(row.getString("qsnorient"));
					list.add(dvo);
				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(ex);
		}
		return list;
	}

	/**
	 * Save contestquestions.
	 *
	 * @param obj the obj
	 * @return the customer answer DVO
	 * @throws Exception the exception
	 */
	public static CustomerAnswerDVO saveContestquestions(SNWContestQuestionsDVO obj) throws Exception {

		try {

			String keySpace = CassandraConnector.getRtfSassCassKeySpace();
			StringBuffer sql = new StringBuffer(" insert into " + keySpace);
			sql.append("pa_snwtx_contest_question  (clintid,conid,conqsnid,qsntext,ansopta,ansoptb,");
			sql.append(
					"    ansoptc,ansoptd,corans,isactive,qsnbnkid,qsnorient)" + " values(?,?,?,?,?,?,?,?,?,?,?,?)  ");
			CassandraConnector.getSession().execute(sql.toString(), obj.getClintid(), obj.getConid(), obj.getConqsnid(),
					obj.getQsntext(), obj.getOpa(), obj.getOpb(), obj.getOpc(), obj.getOpd(), obj.getCorans(),
					obj.getIsactive(), obj.getQbid(), obj.getQsnorient());

		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(ex);
		}
		return null;
	}

	/**
	 * Update contestquestions.
	 *
	 * @param obj the obj
	 * @return the customer answer DVO
	 * @throws Exception the exception
	 */
	public static CustomerAnswerDVO updateContestquestions(SNWContestQuestionsDVO obj) throws Exception {

		try {
			String keySpace = CassandraConnector.getRtfSassCassKeySpace();
			StringBuffer sql = new StringBuffer(
					" update " + keySpace + "pa_snwtx_contest_question set qsntext=?,ansopta=?,ansoptb=?,");
			sql.append("    ansoptc=?,ansoptd=?,corans=?,isactive=?,qsnbnkid=?,qsnorient=? ");
			sql.append(" where clintid=? and conid=? and conqsnid=?  ");
			CassandraConnector.getSession().execute(sql.toString(), obj.getQsntext(), obj.getOpa(), obj.getOpb(),
					obj.getOpc(), obj.getOpd(), obj.getCorans(), obj.getIsactive(), obj.getQbid(), obj.getQsnorient(),
					obj.getClintid(), obj.getConid(), obj.getConqsnid());

		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(ex);
		}
		return null;
	}

	/**
	 * Update contestquestionsseq no.
	 *
	 * @param obj the obj
	 * @return the customer answer DVO
	 * @throws Exception the exception
	 */
	public static CustomerAnswerDVO updateContestquestionsseqNo(SNWContestQuestionsDVO obj) throws Exception {

		try {
			String keySpace = CassandraConnector.getRtfSassCassKeySpace();
			StringBuffer sql = new StringBuffer(" update " + keySpace);
			sql.append("pa_snwtx_contest_question set qsnseqno=? where clintid=? and conid=? and conqsnid=? ");
			CassandraConnector.getSession().execute(sql.toString(), obj.getQsnseqno(), obj.getClintid(), obj.getConid(),
					obj.getConqsnid());

		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(ex);
		}
		return null;
	}

	/**
	 * Update contest questions seq no.
	 *
	 * @param objList the obj list
	 * @throws Exception the exception
	 */
	public static void updateContestQuestionsSeqNo(List<SNWContestQuestionsDVO> objList) throws Exception {

		try {
			String keySpace = CassandraConnector.getRtfSassCassKeySpace();
			Session session = CassandraConnector.getSession();
			BatchStatement batchStatement = new BatchStatement();
			StringBuffer sql = new StringBuffer(" update " + keySpace);
			sql.append("pa_snwtx_contest_question set qsnseqno=? where clintid=? and conid=? and conqsnid=? ");
			PreparedStatement preparedStatement = session.prepare(sql.toString());
			for (SNWContestQuestionsDVO obj : objList) {
				batchStatement.add(
						preparedStatement.bind(obj.getQsnseqno(), obj.getClintid(), obj.getConid(), obj.getConqsnid()));
			}

			session.execute(batchStatement);
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(ex);
		}
	}

	/**
	 * Delete contestquestions.
	 *
	 * @param obj the obj
	 * @return the customer answer DVO
	 * @throws Exception the exception
	 */
	public static CustomerAnswerDVO deleteContestquestions(SNWContestQuestionsDVO obj) throws Exception {

		try {
			String keySpace = CassandraConnector.getRtfSassCassKeySpace();
			StringBuffer sql = new StringBuffer(" delete from  " + keySpace);
					sql.append("pa_snwtx_contest_question where clintid=? and conid=? and conqsnid=? ");
			CassandraConnector.getSession().execute(sql.toString(),
					obj.getClintid(), obj.getConid(), obj.getConqsnid());

		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(ex);
		}
		return null;
	}
}
