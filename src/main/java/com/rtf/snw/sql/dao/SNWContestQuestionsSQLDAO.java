/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.snw.sql.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.rtf.common.util.GridSortingUtil;
import com.rtf.saas.sql.db.DatabaseConnections;
import com.rtf.saas.util.PaginationUtil;
import com.rtf.snw.dvo.SNWContestQuestionsDVO;

/**
 * The Class SNWContestQuestionsSQLDAO.
 */
public class SNWContestQuestionsSQLDAO {

	/**
	 * Gets the contestquestions by question id.
	 *
	 * @param clientId   the client id
	 * @param contestId  the contest id
	 * @param questionId the question id
	 * @return the contestquestions by question id
	 */
	public static SNWContestQuestionsDVO getcontestquestionsByQuestionId(String clientId, String contestId,
			Integer questionId) {

		StringBuffer sql = new StringBuffer("SELECT * FROM " );
		sql.append("pa_snwtx_contest_question where  clintid=? and  conid=? and conqsnid = ?");

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			ps.setString(2, contestId);
			ps.setInt(3, questionId);
			rs = ps.executeQuery();

			SNWContestQuestionsDVO obj = null;
			while (rs.next()) {
				obj = new SNWContestQuestionsDVO();
				obj.setClintid(rs.getString("clintid"));
				obj.setConqsnid(rs.getInt("conqsnid"));
				obj.setConid(rs.getString("conid"));
				obj.setIsactive(rs.getBoolean("isactive"));
				obj.setOpa(rs.getString("ansopta"));
				obj.setOpb(rs.getString("ansoptb"));
				obj.setOpc(rs.getString("ansoptc"));
				obj.setOpd(rs.getString("ansoptd"));
				obj.setCorans(rs.getString("corans"));
				obj.setCreby(rs.getString("creby"));
				obj.setCredate(new Date(rs.getTimestamp("credate").getTime()));
				obj.setQbid(rs.getInt("qsnbnkid"));
				obj.setQsnorient(rs.getString("qsnorient"));
				obj.setQsntext(rs.getString("qsntext"));
				obj.setUpdby(rs.getString("updby"));
				if (rs.getTimestamp("upddate") != null) {
					obj.setUpddate(new Date(rs.getTimestamp("upddate").getTime()));
				}
			}
			return obj;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * Gets the contestquestions by contest idand question seq no.
	 *
	 * @param clientId  the client id
	 * @param contestId the contest id
	 * @param seqno     the seqno
	 * @return the contestquestions by contest idand question seq no
	 */
	public static SNWContestQuestionsDVO getcontestquestionsByContestIdandQuestionSeqNo(String clientId,
			String contestId, Integer seqno) {

		StringBuffer sql = new StringBuffer("SELECT * FROM ");
		sql.append("pa_snwtx_contest_question where  clintid=? and conid=? and qsnseqno = ?");

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			ps.setString(2, contestId);
			ps.setInt(3, seqno);

			rs = ps.executeQuery();

			SNWContestQuestionsDVO obj = null;
			while (rs.next()) {
				obj = new SNWContestQuestionsDVO();
				obj.setClintid(rs.getString("clintid"));
				obj.setConqsnid(rs.getInt("conqsnid"));
				obj.setConid(rs.getString("conid"));
				obj.setIsactive(rs.getBoolean("isactive"));
				obj.setOpa(rs.getString("ansopta"));
				obj.setOpb(rs.getString("ansoptb"));
				obj.setOpc(rs.getString("ansoptc"));
				obj.setOpd(rs.getString("ansoptd"));
				obj.setCorans(rs.getString("corans"));
				obj.setCreby(rs.getString("creby"));
				obj.setCredate(new Date(rs.getTimestamp("credate").getTime()));
				obj.setQbid(rs.getInt("qsnbnkid"));
				obj.setQsnorient(rs.getString("qsnorient"));
				obj.setQsntext(rs.getString("qsntext"));
				obj.setUpdby(rs.getString("updby"));
				if (rs.getTimestamp("upddate") != null) {
					obj.setUpddate(new Date(rs.getTimestamp("upddate").getTime()));
				}
			}
			return obj;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * Gets the contestquestions by contest idand question bank id.
	 *
	 * @param clientId       the client id
	 * @param contestId      the contest id
	 * @param questionBankId the question bank id
	 * @return the contestquestions by contest idand question bank id
	 */
	public static SNWContestQuestionsDVO getcontestquestionsByContestIdandQuestionBankId(String clientId,
			String contestId, Integer questionBankId) {

		StringBuffer sql = new StringBuffer("SELECT * FROM " );
		sql.append("pa_snwtx_contest_question where  clintid=? and conid=? and qsnbnkid = ?");

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			ps.setString(2, contestId);
			ps.setInt(3, questionBankId);
			rs = ps.executeQuery();

			SNWContestQuestionsDVO obj = null;
			while (rs.next()) {
				obj = new SNWContestQuestionsDVO();
				obj.setClintid(rs.getString("clintid"));
				obj.setConqsnid(rs.getInt("conqsnid"));
				obj.setConid(rs.getString("conid"));
				obj.setIsactive(rs.getBoolean("isactive"));
				obj.setOpa(rs.getString("ansopta"));
				obj.setOpb(rs.getString("ansoptb"));
				obj.setOpc(rs.getString("ansoptc"));
				obj.setOpd(rs.getString("ansoptd"));
				obj.setCorans(rs.getString("corans"));
				obj.setCreby(rs.getString("creby"));
				obj.setCredate(new Date(rs.getTimestamp("credate").getTime()));
				obj.setQbid(rs.getInt("qsnbnkid"));
				obj.setQsnorient(rs.getString("qsnorient"));
				obj.setQsntext(rs.getString("qsntext"));
				obj.setUpdby(rs.getString("updby"));
				if (rs.getTimestamp("upddate") != null) {
					obj.setUpddate(new Date(rs.getTimestamp("upddate").getTime()));
				}
			}
			return obj;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * Gets the contestquestions bycontest id.
	 *
	 * @param clientId  the client id
	 * @param contestId the contest id
	 * @return the contestquestions bycontest id
	 */
	public static List<SNWContestQuestionsDVO> getcontestquestionsBycontestId(String clientId, String contestId) {

		StringBuffer sql = new StringBuffer("SELECT * FROM ");
		sql.append("pa_snwtx_contest_question where  clintid=? and conid = ? order by conqsnid desc");

		List<SNWContestQuestionsDVO> list = new ArrayList<SNWContestQuestionsDVO>();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			ps.setString(2, contestId);
			rs = ps.executeQuery();

			SNWContestQuestionsDVO obj = null;
			while (rs.next()) {
				obj = new SNWContestQuestionsDVO();
				obj.setClintid(rs.getString("clintid"));
				obj.setConqsnid(rs.getInt("conqsnid"));
				obj.setConid(rs.getString("conid"));
				obj.setIsactive(rs.getBoolean("isactive"));
				obj.setOpa(rs.getString("ansopta"));
				obj.setOpb(rs.getString("ansoptb"));
				obj.setOpc(rs.getString("ansoptc"));
				obj.setOpd(rs.getString("ansoptd"));
				obj.setCorans(rs.getString("corans"));
				obj.setCreby(rs.getString("creby"));
				obj.setCredate(new Date(rs.getTimestamp("credate").getTime()));
				obj.setQbid(rs.getInt("qsnbnkid"));
				obj.setQsnorient(rs.getString("qsnorient"));
				obj.setQsntext(rs.getString("qsntext"));
				obj.setUpdby(rs.getString("updby"));
				if (rs.getTimestamp("upddate") != null) {
					obj.setUpddate(new Date(rs.getTimestamp("upddate").getTime()));
				}
				list.add(obj);

			}
			return list;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * Gets the contestquestions bycontest id and filter.
	 *
	 * @param clientId    the client id
	 * @param contestId   the contest id
	 * @param filterQuery the filter query
	 * @param pgNo        the pg no
	 * @return the contestquestions bycontest id and filter
	 */
	public static List<SNWContestQuestionsDVO> getcontestquestionsBycontestIdAndFilter(String clientId,
			String contestId, String filterQuery, String pgNo,String filter) {

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		StringBuffer sql = new StringBuffer("SELECT * FROM ");
		sql.append("pa_snwtx_contest_question where  clintid=? and conid = ? ");

		String paginationQuery = PaginationUtil.getPaginationQuery(pgNo);
		if (filterQuery != null && !filterQuery.isEmpty()) {
			sql.append(filterQuery);
		}
		String sortingSql = GridSortingUtil.getSNWContestQuestionsSortingQuery(filter);
		if(sortingSql!=null && !sortingSql.trim().isEmpty()){
			sql.append(sortingSql);
		}else{
			sql.append(" order by conqsnid desc ");
		}
		
		sql.append(paginationQuery);

		List<SNWContestQuestionsDVO> list = new ArrayList<SNWContestQuestionsDVO>();
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			ps.setString(2, contestId);
			rs = ps.executeQuery();

			SNWContestQuestionsDVO obj = null;
			while (rs.next()) {
				obj = new SNWContestQuestionsDVO();
				obj.setClintid(rs.getString("clintid"));
				obj.setConqsnid(rs.getInt("conqsnid"));
				obj.setConid(rs.getString("conid"));
				obj.setIsactive(rs.getBoolean("isactive"));
				obj.setOpa(rs.getString("ansopta"));
				obj.setOpb(rs.getString("ansoptb"));
				obj.setOpc(rs.getString("ansoptc"));
				obj.setOpd(rs.getString("ansoptd"));
				obj.setCorans(rs.getString("corans"));
				obj.setCreby(rs.getString("creby"));
				obj.setCredate(new Date(rs.getTimestamp("credate").getTime()));
				obj.setQbid(rs.getInt("qsnbnkid"));
				obj.setQsnorient(rs.getString("qsnorient"));
				obj.setQsntext(rs.getString("qsntext"));
				obj.setUpdby(rs.getString("updby"));
				if (rs.getTimestamp("upddate") != null) {
					obj.setUpddate(new Date(rs.getTimestamp("upddate").getTime()));
				}
				list.add(obj);

			}
			return list;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * Gets the all contest question data to export.
	 *
	 * @param clientId    the client id
	 * @param contestId   the contest id
	 * @param filterQuery the filter query
	 * @return the all contest question data to export
	 */
	public static List<SNWContestQuestionsDVO> getAllContestQuestionDataToExport(String clientId, String contestId,
			String filterQuery) {

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		StringBuffer sql = new StringBuffer("SELECT * FROM " );
		sql.append("pa_snwtx_contest_question where  clintid=? and conid = ? ");

		if (filterQuery != null && !filterQuery.isEmpty()) {
			sql.append(filterQuery);
		}
		sql.append("  order by conqsnid desc ");

		List<SNWContestQuestionsDVO> list = new ArrayList<SNWContestQuestionsDVO>();
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			ps.setString(2, contestId);
			rs = ps.executeQuery();

			SNWContestQuestionsDVO obj = null;
			while (rs.next()) {
				obj = new SNWContestQuestionsDVO();
				obj.setClintid(rs.getString("clintid"));
				obj.setConqsnid(rs.getInt("conqsnid"));
				obj.setConid(rs.getString("conid"));
				obj.setIsactive(rs.getBoolean("isactive"));
				obj.setOpa(rs.getString("ansopta"));
				obj.setOpb(rs.getString("ansoptb"));
				obj.setOpc(rs.getString("ansoptc"));
				obj.setOpd(rs.getString("ansoptd"));
				obj.setCorans(rs.getString("corans"));
				obj.setCreby(rs.getString("creby"));
				obj.setCredate(new Date(rs.getTimestamp("credate").getTime()));
				obj.setQbid(rs.getInt("qsnbnkid"));
				obj.setQsnorient(rs.getString("qsnorient"));
				obj.setQsntext(rs.getString("qsntext"));
				obj.setUpdby(rs.getString("updby"));
				if (rs.getTimestamp("upddate") != null) {
					obj.setUpddate(new Date(rs.getTimestamp("upddate").getTime()));
				}
				list.add(obj);

			}
			return list;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * Gets the all contest questions count by contest idand filter.
	 *
	 * @param clientId    the client id
	 * @param contestId   the contest id
	 * @param filterQuery the filter query
	 * @return the all contest questions count by contest idand filter
	 * @throws Exception the exception
	 */
	public static Integer getAllContestQuestionsCountByContestIdandFilter(String clientId, String contestId,
			String filterQuery) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Integer count = 0;
		try {

			StringBuffer sql = new StringBuffer(
					"SELECT count(*) FROM pa_snwtx_contest_question where  clintid=? ");
			sql.append(" and conid = ? ");

			if (filterQuery != null && !filterQuery.isEmpty()) {
				sql.append(filterQuery);
			}
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			ps.setString(2, contestId);

			rs = ps.executeQuery();
			while (rs.next()) {
				count = rs.getInt(1);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return count;
	}

	/**
	 * Save contest questions.
	 *
	 * @param clientId the client id
	 * @param obj      the obj
	 * @return the integer
	 */
	public static Integer saveContestQuestions(String clientId, SNWContestQuestionsDVO obj) {
		Integer id = null;

		StringBuffer sql = new StringBuffer("insert into ");
		sql.append("pa_snwtx_contest_question(clintid, conid,qsntext, ansopta, ansoptb, ansoptc, ansoptd, ");
		sql.append(" corans,isactive,qsnbnkid,qsnorient, creby, credate, qsnseqno,updby, upddate) ");
		sql.append("values(?,?,?,?,?,?,?,?,?,?,?,?,?,null,?,getDate())");

		Connection conn = null;
		PreparedStatement statement = null;
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			statement = conn.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, obj.getClintid());
			statement.setString(2, obj.getConid());
			statement.setString(3, obj.getQsntext());
			statement.setString(4, obj.getOpa());
			statement.setString(5, obj.getOpb());
			statement.setString(6, obj.getOpc());
			statement.setString(7, obj.getOpd());
			statement.setString(8, obj.getCorans());
			statement.setBoolean(9, obj.getIsactive());
			if (obj.getQbid() != null) {
				statement.setInt(10, obj.getQbid());
			} else {
				statement.setString(10, null);
			}
			statement.setString(11, obj.getQsnorient());
			statement.setString(12, obj.getCreby());
			statement.setTimestamp(13, new Timestamp(obj.getCredate().getTime()));
			statement.setString(14, obj.getCreby());

			int affectedRows = statement.executeUpdate();
			if (affectedRows == 0) {
				throw new SQLException("Creating contest question record failed, no rows affected.");
			}
			ResultSet generatedKeys;
			try {
				generatedKeys = statement.getGeneratedKeys();
				if (generatedKeys.next()) {
					id = generatedKeys.getInt(1);

				}
				generatedKeys.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				statement.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return id;

	}

	/**
	 * Update contest questions.
	 *
	 * @param clientId the client id
	 * @param obj      the obj
	 * @return the integer
	 */
	public static Integer updateContestQuestions(String clientId, SNWContestQuestionsDVO obj) {
		Integer affectedRows = null;

		StringBuffer sql = new StringBuffer("update ");
		sql.append("pa_snwtx_contest_question set qsntext =?, ansopta=?, ansoptb=?, ansoptc=?, ansoptd=?, ");
		sql.append(" corans=?, isactive=?, qsnbnkid=?, qsnorient=?, updby=?, upddate=? where  ");
		sql.append(" clintid=? and conid=? and conqsnid = ?");
		Connection conn = null;
		PreparedStatement statement = null;
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			statement = conn.prepareStatement(sql.toString());
			statement.setString(1, obj.getQsntext());
			statement.setString(2, obj.getOpa());
			statement.setString(3, obj.getOpb());
			statement.setString(4, obj.getOpc());
			statement.setString(5, obj.getOpd());
			statement.setString(6, obj.getCorans());
			statement.setBoolean(7, obj.getIsactive());
			if (obj.getQbid() != null) {
				statement.setInt(8, obj.getQbid());
			} else {
				statement.setString(8, null);
			}
			statement.setString(9, obj.getQsnorient());
			statement.setString(10, obj.getUpdby());
			statement.setTimestamp(11, new Timestamp(obj.getUpddate().getTime()));
			statement.setString(12, obj.getClintid());
			statement.setString(13, obj.getConid());
			statement.setInt(14, obj.getConqsnid());

			affectedRows = statement.executeUpdate();
			if (affectedRows == 0) {
				throw new SQLException("updating contest question record failed, no rows affected.");
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				statement.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return affectedRows;

	}

	/**
	 * Update contest questions seqno.
	 *
	 * @param clientId the client id
	 * @param list     the list
	 * @return the integer
	 */
	public static Integer updateContestQuestionsSeqno(String clientId, List<SNWContestQuestionsDVO> list) {
		Integer affectedRows = null;

		StringBuffer sql = new StringBuffer("update ");
		sql.append("pa_snwtx_contest_question set qsnseqno =?,  updby=?, upddate=? where  ");
		sql.append(" clintid=? and conid=? and conqsnid = ?");
		Connection conn = null;
		PreparedStatement statement = null;
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			statement = conn.prepareStatement(sql.toString());

			for (SNWContestQuestionsDVO obj : list) {
				statement.setInt(1, obj.getQsnseqno());
				statement.setString(2, obj.getUpdby());
				statement.setTimestamp(3, new Timestamp(obj.getUpddate().getTime()));
				statement.setString(4, obj.getClintid());
				statement.setString(5, obj.getConid());
				statement.setInt(6, obj.getConqsnid());

				statement.addBatch();
			}
			int[] rws = statement.executeBatch();
			if (rws == null || rws.length == 0) {
				throw new SQLException("updating contest question seqno record failed, no rows affected.");
			}
			affectedRows = rws.length;

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				statement.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return affectedRows;

	}

	/**
	 * Delete contest questions.
	 *
	 * @param clientId the client id
	 * @param obj      the obj
	 * @return the integer
	 */
	public static Integer deleteContestQuestions(String clientId, SNWContestQuestionsDVO obj) {
		Integer affectedRows = null;

		StringBuffer sql = new StringBuffer(
				"delete from pa_snwtx_contest_question where  ");
		sql.append(" clintid=? and conid=? and conqsnid = ?");
		Connection conn = null;
		PreparedStatement statement = null;
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			statement = conn.prepareStatement(sql.toString());
			statement.setString(1, obj.getClintid());
			statement.setString(2, obj.getConid());
			statement.setInt(3, obj.getConqsnid());

			affectedRows = statement.executeUpdate();
			if (affectedRows == 0) {
				throw new SQLException("delete contest question record failed, no rows affected.");
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				statement.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return affectedRows;

	}

	/**
	 * Gets the contest questions count.
	 *
	 * @param clientId  the client id
	 * @param contestId the contest id
	 * @return the contest questions count
	 */
	public static Integer getContestQuestionsCount(String clientId, String contestId) {

		StringBuffer sql = new StringBuffer(" SELECT  count(*) as qCount FROM ");
		sql.append("pa_snwtx_contest_question where  clintid=? and conid = ?");

		Integer qsize = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {

			conn = DatabaseConnections.getSaasAdminConnection();
			stmt = conn.prepareStatement(sql.toString());
			stmt.setString(1, clientId);
			stmt.setString(2, contestId);
			rs = stmt.executeQuery();
			while (rs.next()) {
				qsize = rs.getInt("qCount");
			}
			return qsize;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				stmt.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return qsize;
	}

}
