/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.snw.sql.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.rtf.common.util.GridSortingUtil;
import com.rtf.ott.util.GridHeaderFilterUtil;
import com.rtf.saas.sql.db.DatabaseConnections;
import com.rtf.saas.util.PaginationUtil;
import com.rtf.snw.dvo.ContestDVO;

/**
 * The Class ContestSQLDAO.
 */
public class ContestSQLDAO {

	/**
	 * Gets the contest by contest id.
	 *
	 * @param clientId  the client id
	 * @param contestId the contest id
	 * @return the contest by contest id
	 * @throws Exception the exception
	 */
	public static ContestDVO getContestByContestId(String clientId, String contestId) throws Exception {
		ContestDVO contestDVO = null;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer sql = new StringBuffer("SELECT * from  pa_snwtx_contest_mstr  WHERE clintid = ? AND conid = ? ORDER BY consrtdate");
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			ps.setString(2, contestId);
			rs = ps.executeQuery();

			while (rs.next()) {
				contestDVO = new ContestDVO();
				Date date = null;
				contestDVO.setCoId(rs.getString("conid"));
				contestDVO.setClId(rs.getString("clintid"));
				contestDVO.setAnsType(rs.getString("anstype"));
				contestDVO.setClImgU(rs.getString("brndimgurl"));
				contestDVO.setImgU(rs.getString("cardimgurl"));
				contestDVO.setSeqNo(rs.getInt("cardseqno"));
				contestDVO.setCat(rs.getString("cattype"));
				date = rs.getTimestamp("conenddate");
				contestDVO.setEnDate(date != null ? date.getTime() : null);
				contestDVO.setName(rs.getString("conname"));
				contestDVO.setPlayInterval(rs.getInt("playinterval"));
				date = rs.getTimestamp("consrtdate");
				contestDVO.setStDate(date != null ? date.getTime() : null);
				contestDVO.setCrBy(rs.getString("creby"));
				date = rs.getTimestamp("credate");
				contestDVO.setCrDate(date != null ? date.getTime() : null);
				contestDVO.setIsAct(rs.getInt("isconactive"));
				contestDVO.setqMode(rs.getString("qsnmode"));
				contestDVO.setSubCat(rs.getString("subcattype"));
				contestDVO.setUpBy(rs.getString("updby"));
				contestDVO.setThmColor(rs.getString("bgthemcolor"));
				contestDVO.setThmImgDesk(rs.getString("bgthemimgurldes"));
				contestDVO.setThmImgMob(rs.getString("bgthemimgurlmob"));
				contestDVO.setPlayGameImg(rs.getString("playimgurl"));
				contestDVO.setThmId(rs.getInt("bgthembankid"));
				date = rs.getTimestamp("upddate");
				contestDVO.setUpDate(date != null ? date.getTime() : null);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return contestDVO;
	}

	/**
	 * Gets the live contests by client id.
	 *
	 * @param clientId the client id
	 * @return the live contests by client id
	 * @throws Exception the exception
	 */
	public static ContestDVO getLiveContestsByClientId(String clientId) throws Exception {
		ContestDVO contestDVO = null;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer sql = new StringBuffer("SELECT * from  pa_snwtx_contest_mstr  WHERE clintid = ? AND isconactive = ? ORDER BY consrtdate");
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clientId);
			ps.setInt(2, 2);
			rs = ps.executeQuery();

			while (rs.next()) {
				contestDVO = new ContestDVO();
				Date date = null;
				contestDVO.setCoId(rs.getString("conid"));
				contestDVO.setClId(rs.getString("clintid"));
				contestDVO.setAnsType(rs.getString("anstype"));
				contestDVO.setClImgU(rs.getString("brndimgurl"));
				contestDVO.setImgU(rs.getString("cardimgurl"));
				contestDVO.setSeqNo(rs.getInt("cardseqno"));
				contestDVO.setCat(rs.getString("cattype"));
				date = rs.getTimestamp("conenddate");
				contestDVO.setEnDate(date != null ? date.getTime() : null);
				contestDVO.setName(rs.getString("conname"));
				contestDVO.setPlayInterval(rs.getInt("playinterval"));
				date = rs.getTimestamp("consrtdate");
				contestDVO.setStDate(date != null ? date.getTime() : null);
				contestDVO.setCrBy(rs.getString("creby"));
				date = rs.getTimestamp("credate");
				contestDVO.setCrDate(date != null ? date.getTime() : null);
				contestDVO.setIsAct(rs.getInt("isconactive"));
				contestDVO.setqMode(rs.getString("qsnmode"));
				contestDVO.setSubCat(rs.getString("subcattype"));
				contestDVO.setUpBy(rs.getString("updby"));
				contestDVO.setThmColor(rs.getString("bgthemcolor"));
				contestDVO.setThmImgDesk(rs.getString("bgthemimgurldes"));
				contestDVO.setThmImgMob(rs.getString("bgthemimgurlmob"));
				contestDVO.setPlayGameImg(rs.getString("playimgurl"));
				contestDVO.setThmId(rs.getInt("bgthembankid"));
				date = rs.getTimestamp("upddate");
				contestDVO.setUpDate(date != null ? date.getTime() : null);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return contestDVO;
	}

	/**
	 * Gets the all contest by statusand filter.
	 *
	 * @param clId   the cl id
	 * @param status the status
	 * @param filter the filter
	 * @param pgNo   the pg no
	 * @return the all contest by statusand filter
	 * @throws Exception the exception
	 */
	public static List<ContestDVO> getAllContestByStatusandFilter(String clId, String status, String filter,
			String pgNo) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<ContestDVO> list = new ArrayList<ContestDVO>();
		StringBuffer sql = new StringBuffer("SELECT c.clintid AS clintid,c.conid AS conid ");
		sql.append(",c.anstype AS anstype,c.brndimgurl AS brndimgurl ");
		sql.append(",c.cardimgurl AS cardimgurl,c.cardseqno AS cardseqno");
		sql.append(",c.cattype AS cattype,c.conenddate AS conenddate ");
		sql.append(",c.conimgurl AS conimgurl,c.conname AS conname " + ",c.consrtdate AS consrtdate,c.creby AS creby ");
		sql.append(",c.credate AS credate,c.isconactive AS isconactive ");
		sql.append(",c.qsnmode AS qsnmode,c.subcattype AS subcattype ");
		sql.append(",c.updby AS updby,c.upddate AS upddate,c.playinterval as playinterval ");
		sql.append(",c.bgthemcolor AS bgthemcolor,c.bgthemimgurldes AS bgthemimgurldes ");
		sql.append(",c.bgthemimgurlmob AS bgthemimgurlmob,c.bgthembankid AS bgthembankid ");
		sql.append(",c.playimgurl AS playimgurl,s.isconactivetext AS status ");
		sql.append("FROM pa_snwtx_contest_mstr c join sd_contest_isactive s on c.isconactive=s.isconactive ");
		sql.append("WHERE c.clintid = ? ");
		try {
			if (status.equalsIgnoreCase("ACTIVE")) {
				sql.append(" AND c.isconactive in (1,2) ");
			} else if (status.equalsIgnoreCase("LIVE")) {
				sql.append(" AND c.isconactive = 2 ");
			} else if (status.equalsIgnoreCase("EXPIRED")) {
				sql.append(" AND c.isconactive = 3 ");
			} else if (status.equalsIgnoreCase("DELETED")) {
				sql.append(" AND c.isconactive = 4 ");
			}
			String filterQuery = GridHeaderFilterUtil.getContestFilterQuery(filter);
			String paginationQuery = PaginationUtil.getPaginationQuery(pgNo);
			if (filterQuery != null && !filterQuery.isEmpty()) {
				sql.append(filterQuery);
			}
			String sortingSql = GridSortingUtil.getSNWContestSortingQuery(filter);
			if(sortingSql!=null && !sortingSql.trim().isEmpty()){
				sql.append(sortingSql);
			}else{
				sql.append(" ORDER BY c.consrtdate");
			}
			
			sql.append(paginationQuery);
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clId);
			rs = ps.executeQuery();

			while (rs.next()) {
				ContestDVO contestDVO = new ContestDVO();
				Date date = null;
				contestDVO.setCoId(rs.getString("conid"));
				contestDVO.setClId(rs.getString("clintid"));
				contestDVO.setAnsType(rs.getString("anstype"));
				contestDVO.setClImgU(rs.getString("brndimgurl"));
				contestDVO.setImgU(rs.getString("cardimgurl"));
				contestDVO.setSeqNo(rs.getInt("cardseqno"));
				contestDVO.setCat(rs.getString("cattype"));
				date = rs.getTimestamp("conenddate");
				contestDVO.setEnDate(date != null ? date.getTime() : null);
				contestDVO.setName(rs.getString("conname"));
				contestDVO.setPlayInterval(rs.getInt("playinterval"));
				date = rs.getTimestamp("consrtdate");
				contestDVO.setStDate(date != null ? date.getTime() : null);
				contestDVO.setCrBy(rs.getString("creby"));
				date = rs.getTimestamp("credate");
				contestDVO.setCrDate(date != null ? date.getTime() : null);
				contestDVO.setIsAct(rs.getInt("isconactive"));
				contestDVO.setqMode(rs.getString("qsnmode"));
				contestDVO.setSubCat(rs.getString("subcattype"));
				contestDVO.setUpBy(rs.getString("updby"));
				contestDVO.setThmColor(rs.getString("bgthemcolor"));
				contestDVO.setThmImgDesk(rs.getString("bgthemimgurldes"));
				contestDVO.setThmImgMob(rs.getString("bgthemimgurlmob"));
				contestDVO.setPlayGameImg(rs.getString("playimgurl"));
				contestDVO.setThmId(rs.getInt("bgthembankid"));
				contestDVO.setStatus(rs.getString("status"));
				date = rs.getTimestamp("upddate");
				contestDVO.setUpDate(date != null ? date.getTime() : null);
				list.add(contestDVO);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return list;
	}

	/**
	 * Gets the all contest data to export.
	 *
	 * @param clId   the cl id
	 * @param status the status
	 * @param filter the filter
	 * @return the all contest data to export
	 * @throws Exception the exception
	 */
	public static List<ContestDVO> getAllContestDataToExport(String clId, String status, String filter)
			throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<ContestDVO> list = new ArrayList<ContestDVO>();
		StringBuffer sql = new StringBuffer("SELECT c.clintid AS clintid,c.conid AS conid ");
		sql.append(",c.anstype AS anstype,c.brndimgurl AS brndimgurl ");
		sql.append(",c.cardimgurl AS cardimgurl,c.cardseqno AS cardseqno");
		sql.append(",c.cattype AS cattype,c.conenddate AS conenddate ");
		sql.append(",c.conimgurl AS conimgurl,c.conname AS conname " + ",c.consrtdate AS consrtdate,c.creby AS creby ");
		sql.append(",c.credate AS credate,c.isconactive AS isconactive ");
		sql.append(",c.qsnmode AS qsnmode,c.subcattype AS subcattype ");
		sql.append(",c.updby AS updby,c.upddate AS upddate,c.playinterval as playinterval ");
		sql.append(",c.bgthemcolor AS bgthemcolor,c.bgthemimgurldes AS bgthemimgurldes ");
		sql.append(",c.bgthemimgurlmob AS bgthemimgurlmob,c.bgthembankid AS bgthembankid ");
		sql.append(",c.playimgurl AS playimgurl,s.isconactivetext AS status ");
		sql.append("FROM pa_snwtx_contest_mstr c join sd_contest_isactive s on c.isconactive=s.isconactive ");
		sql.append("WHERE c.clintid = ? ");
		try {
			if (status.equalsIgnoreCase("ACTIVE")) {
				sql.append(" AND c.isconactive in (1,2) ");
			} else if (status.equalsIgnoreCase("LIVE")) {
				sql.append(" AND c.isconactive = 2 ");
			} else if (status.equalsIgnoreCase("EXPIRED")) {
				sql.append(" AND c.isconactive = 3 ");
			} else if (status.equalsIgnoreCase("DELETED")) {
				sql.append(" AND c.isconactive = 4 ");
			}
			String filterQuery = GridHeaderFilterUtil.getContestFilterQuery(filter);
			if (filterQuery != null && !filterQuery.isEmpty()) {
				sql.append(filterQuery);
			}
			sql.append(" ORDER BY c.consrtdate");
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clId);
			rs = ps.executeQuery();

			while (rs.next()) {
				ContestDVO contestDVO = new ContestDVO();
				Date date = null;
				contestDVO.setCoId(rs.getString("conid"));
				contestDVO.setClId(rs.getString("clintid"));
				contestDVO.setAnsType(rs.getString("anstype"));
				contestDVO.setClImgU(rs.getString("brndimgurl"));
				contestDVO.setImgU(rs.getString("cardimgurl"));
				contestDVO.setSeqNo(rs.getInt("cardseqno"));
				contestDVO.setCat(rs.getString("cattype"));
				date = rs.getTimestamp("conenddate");
				contestDVO.setEnDate(date != null ? date.getTime() : null);
				contestDVO.setName(rs.getString("conname"));
				contestDVO.setPlayInterval(rs.getInt("playinterval"));
				date = rs.getTimestamp("consrtdate");
				contestDVO.setStDate(date != null ? date.getTime() : null);
				contestDVO.setCrBy(rs.getString("creby"));
				date = rs.getTimestamp("credate");
				contestDVO.setCrDate(date != null ? date.getTime() : null);
				contestDVO.setIsAct(rs.getInt("isconactive"));
				contestDVO.setqMode(rs.getString("qsnmode"));
				contestDVO.setSubCat(rs.getString("subcattype"));
				contestDVO.setUpBy(rs.getString("updby"));
				contestDVO.setThmColor(rs.getString("bgthemcolor"));
				contestDVO.setThmImgDesk(rs.getString("bgthemimgurldes"));
				contestDVO.setThmImgMob(rs.getString("bgthemimgurlmob"));
				contestDVO.setPlayGameImg(rs.getString("playimgurl"));
				contestDVO.setThmId(rs.getInt("bgthembankid"));
				contestDVO.setStatus(rs.getString("status"));
				date = rs.getTimestamp("upddate");
				contestDVO.setUpDate(date != null ? date.getTime() : null);
				list.add(contestDVO);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return list;
	}

	/**
	 * Gets the all contest count by statusand filter.
	 *
	 * @param clId   the cl id
	 * @param status the status
	 * @param filter the filter
	 * @return the all contest count by statusand filter
	 * @throws Exception the exception
	 */
	public static Integer getAllContestCountByStatusandFilter(String clId, String status, String filter)
			throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Integer count = 0;
		StringBuffer sql = new StringBuffer("SELECT count(*) from  pa_snwtx_contest_mstr  c join sd_contest_isactive s on c.isconactive=s.isconactive WHERE c.clintid = ? ");
		try {
			if (status.equalsIgnoreCase("ACTIVE")) {
				sql.append(" AND c.isconactive in (1,2) ");
			} else if (status.equalsIgnoreCase("LIVE")) {
				sql.append(" AND c.isconactive = 2 ");
			} else if (status.equalsIgnoreCase("EXPIRED")) {
				sql.append(" AND c.isconactive = 3 ");
			} else if (status.equalsIgnoreCase("DELETED")) {
				sql.append(" AND c.isconactive = 4 ");
			}
			String filterQuery = GridHeaderFilterUtil.getContestFilterQuery(filter);
			if (filterQuery != null && !filterQuery.isEmpty()) {
				sql.append(filterQuery);
			}
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, clId);
			rs = ps.executeQuery();

			while (rs.next()) {
				count = rs.getInt(1);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return count;
	}

	/**
	 * Save contest SQL.
	 *
	 * @param co the co
	 * @return true, if successful
	 * @throws Exception the exception
	 */
	public static boolean saveContestSQL(ContestDVO co) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		StringBuffer sql = new StringBuffer();
		sql.append("insert into pa_snwtx_contest_mstr (clintid,conid,anstype,cardseqno,cattype,conenddate,conname,consrtdate,creby,credate,");
		sql.append("isconactive,qsnmode,subcattype,bgthemcolor,bgthemimgurldes,bgthemimgurlmob,bgthembankid,playimgurl,playinterval,updby,upddate) ");
		sql.append("values (?,?,?,?,?,?,?,?,?,getDate(),?,?,?,?,?,?,?,?,?,?,getDate())");
		Boolean isInserted = false;
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, co.getClId());
			ps.setString(2, co.getCoId());
			ps.setString(3, co.getAnsType());
			ps.setInt(4, co.getSeqNo());
			ps.setString(5, co.getCat());
			ps.setTimestamp(6, new java.sql.Timestamp(co.getEnDate()));
			ps.setString(7, co.getName());
			ps.setTimestamp(8, new java.sql.Timestamp(co.getStDate()));
			ps.setString(9, co.getCrBy());
			ps.setInt(10, co.getIsAct());
			ps.setString(11, co.getqMode());
			ps.setString(12, co.getSubCat());
			ps.setString(13, co.getThmColor());
			ps.setString(14, co.getThmImgDesk());
			ps.setString(15, co.getThmImgMob());
			ps.setInt(16, co.getThmId());
			ps.setString(17, co.getPlayGameImg());
			ps.setInt(18, co.getPlayInterval());
			ps.setString(19, co.getCrBy());
			int affectedRows = ps.executeUpdate();
			if (affectedRows > 0) {
				isInserted = true;
			}
			return isInserted;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return isInserted;
	}

	/**
	 * Update contest SQL.
	 *
	 * @param co the co
	 * @return true, if successful
	 * @throws Exception the exception
	 */
	public static boolean updateContestSQL(ContestDVO co) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		StringBuffer sql = new StringBuffer();
		sql.append("update pa_snwtx_contest_mstr set ");
		sql.append("anstype=?,cardseqno=?,cattype=?,");
		sql.append("conenddate=?,conname=?,consrtdate=?,");
		sql.append("updby=?,upddate=getDate(),");
		sql.append("isconactive=?,qsnmode=?,");
		sql.append(
				"subcattype=?,bgthemcolor=?,bgthemimgurldes=?, bgthemimgurlmob=?,bgthembankid=?,playimgurl=?,playinterval=? ");
		sql.append("WHERE clintid=? AND conid=?");
		Boolean isUpdated = false;
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, co.getAnsType());
			ps.setInt(2, co.getSeqNo());
			ps.setString(3, co.getCat());
			ps.setTimestamp(4, new java.sql.Timestamp(co.getEnDate()));
			ps.setString(5, co.getName());
			ps.setTimestamp(6, new java.sql.Timestamp(co.getStDate()));
			ps.setString(7, co.getUpBy());
			ps.setInt(8, co.getIsAct());
			ps.setString(9, co.getqMode());
			ps.setString(10, co.getSubCat());
			ps.setString(11, co.getThmColor());
			ps.setString(12, co.getThmImgDesk());
			ps.setString(13, co.getThmImgMob());
			ps.setInt(14, co.getThmId());
			ps.setString(15, co.getPlayGameImg());
			ps.setInt(16, co.getPlayInterval());
			ps.setString(17, co.getClId());
			ps.setString(18, co.getCoId());
			int affectedRows = ps.executeUpdate();
			if (affectedRows > 0) {
				isUpdated = true;
			}
			return isUpdated;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return isUpdated;
	}

	/**
	 * Delete contest SQL.
	 *
	 * @param clientId the client id
	 * @param coId     the co id
	 * @return true, if successful
	 * @throws Exception the exception
	 */
	public static boolean deleteContestSQL(String clientId, String coId) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		StringBuffer sql = new StringBuffer("update  pa_snwtx_contest_mstr set isconactive=? WHERE clintid=? AND conid=?");
		Boolean isUpdated = false;
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			ps.setInt(1, 4);
			ps.setString(2, clientId);
			ps.setString(3, coId);
			int affectedRows = ps.executeUpdate();
			if (affectedRows > 0) {
				isUpdated = true;
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return isUpdated;
	}

	/**
	 * Update contest status.
	 *
	 * @param co the co
	 * @return true, if successful
	 * @throws Exception the exception
	 */
	public static boolean updateContestStatus(ContestDVO co) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;

		StringBuffer sql = new StringBuffer();
		sql.append("update pa_snwtx_contest_mstr set ");
		sql.append("isconactive=?,upddate=getDate(),updby=? ");
		sql.append("WHERE clintid=? AND conid=?");
		Boolean isUpdated = false;
		try {
			conn = DatabaseConnections.getSaasAdminConnection();
			ps = conn.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			ps.setInt(1, co.getIsAct());
			ps.setString(2, co.getUpBy());
			ps.setString(3, co.getClId());
			ps.setString(4, co.getCoId());
			int affectedRows = ps.executeUpdate();
			if (affectedRows > 0) {
				isUpdated = true;
			}
			return isUpdated;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return isUpdated;
	}

}
