package com.rtf.snw.util;

public class SNWMessageConstant {
	

	public static final String NULL_STRING_VALUE = "null";
	
	public static final String INVALID_CONTEST_ID = "Invalid Contest id.";
	
	public static final String INVALID_QUESTION_ID = "Invalid Qestion id.";
	
	public static final String INVALID_QESTION_BANK_ID = "Invalid Question Bank Id.";
	
	public static final String CONTEST_ID_NOT_EXISTS = "Contest id not exists";
	
	public static final String QUESTION_ID_NOT_EXISTS = "Question id not exists";
	
	public static final String QUESTION_BANK_ID_NOT_EXISTS = "Question Bank Id not exists";
	
	public static final String QUESTION_BANK_ID_ALREADY_EXISTS = "Question Bank question already exist for this contest";
	
	public static final String QUESTION_TYPE_MISMATCH_WITH_CONT_QUEST_TYPE = "Question Type is mismatch with contest question type.";
	
	public static final String CONT_QUEST_NOT_CREATED = "Contest questions not created.";
	
	public static final String CONT_QUEST_NOT_UPDATED = "Contest questions not updated.";
	
	public static final String CONT_QUEST_CREATED_SUCCESS_MSG = "Question Created successfully.";
	
	public static final String CONT_QUEST_UPDATED_SUCCESS_MSG = "Question Updated successfully.";

	public static final String INVALID_QUEST_TEXT = "Invalid Question text.";
	
	public static final String INVALID_QUEST_OPT_A = "Invalid Option A";
	
	public static final String INVALID_QUEST_OPT_B = "Invalid Option B";
	
	public static final String INVALID_QUEST_OPT_C = "Invalid Option C";
	
	public static final String INVALID_QUEST_OPT_D = "Invalid Option D";
	
	public static final String INVALID_QUEST_ORIENT = "Invalid Question Orient";
	
	public static final String FEEDBACK_QUEST_ANSWER_INVALID = "Do not select answer for Feedback contest questions.";
	
	public static final String INVALID_ANSWER = "Invalid Answer";
	
	public static final String CONTEST_TYPE_FEEDBACK = "FEEDBACK";
	
	public static final String CONTEST_QUEST_OPT_A = "A";
	
	public static final String CONTEST_QUEST_OPT_B = "B";
	
	public static final String CONTEST_QUEST_OPT_C = "C";
	
	public static final String CONTEST_QUEST_OPT_D = "D";
	
	public static final String INVALID_START_DATE = "Invalid start date.";
	
	public static final String INVALID_CATEGORY = "Invalid Category.";
	
	public static final String INVALID_QUEST_MODE = "Invalid question mode.";
	
	public static final String INVALID_NATURE_OF_QUEST = "Invalid nature of question.";
	
	public static final String INVALID_PLAY_INTERVAL = "Invalid play interval minutes.";
	
	public static final String CONTEST_CREATED_SUCCESS_MSG = "Contest data saved successfully.";
	
	public static final String CONTEST_QUEST_DEETE_SUCCESS_MSG = "Questions Deleted successfully.";
	
	public static final String CONTEST_DEETE_SUCCESS_MSG = "Contest Deleted successfully.";
	
	public static final String CANT_END_NON_ACTIVE_CONTEST = "Cannot end contest, Only contest with Live status can be ended.";
	
	public static final String CONTEST_END_SUCCESS_MSG = "Contest Ended successfully.";
	
	public static final String CONTEST_UPDATE_SUCCESS_MSG = "Contest data updated successfully.";
	
	public static final String CONTEST_UPDATE_TO_ACTIVE_SUCCESS_MSG = "Contest updated to active successfully.";
	
	public static final String INVALID_CONT_STATUS = "Invalid contest status.";
	
	public static final String ONE_CONTEST_ONLY_IN_LIVE = "There is already live contest, Only one scratch and win contest can be live at a time.";
	
	public static final String SELECTED_CONTEST_IS_ALREADY_LIVE = "Selected contest is already Live.";
	
	public static final String ACTIVE_CONTEST_ONLY_CAN_START = "Cannot start contest, Only active contest can be started.";
	
	public static final String CANT_START_FUTURE_DATE_CONTEST = "Cannot start contest, Contest start date is future date.";
	
	public static final String CANT_START_PAST_END_DATE_CONTEST = "Cannot start contest, Contest end date is past date.";
	
	public static final String FIXED_CONTEST_WITH_NO_QUESTIONS = "Cannot start contest, Question mode is fixed and questions are not added in contest.";
	
	public static final String CONT_QUEST_MODE_FIXED = "FIXED";
	
	public static final String CONT_QUEST_MODE_RANDOM = "RANDOM";
	
	public static final String RANDOM_CONTEST_WITH_NO_QUESTIONS_IN_BANK = "Cannot start contest, Question bank does not have question with same category and subcategory mentiond at contest level.";
	
	public static final String CONTEST_START_SUCCESS_MSG = "Contest updated to live.";

	public static final String CANT_UPDATE_LIVE_CONTEST = "Not Allowed to update Live contest, Please end it first.";
	
	public static final String CONTEST_REWARD_UPDATE_SUCCESS_MSG = "Contest reward prizes updated successfully.";

	public static final String CONTEST_DATE_SHOULD_BE_TODAY_OR_FUTURE = "Contest end date should be today or future date.";
}
