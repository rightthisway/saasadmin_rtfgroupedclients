/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.snw.util;

import com.rtf.ott.util.DateFormatUtil;

/**
 * The Class SNWGridHeaderFilterUtil.
 */
public class SNWGridHeaderFilterUtil {

	/**
	 * Gets the contest questions filter query.
	 *
	 * @param filter the filter
	 * @return the contest questions filter query
	 * @throws Exception the exception
	 */
	public static String getContestQuestionsFilterQuery(String filter) throws Exception {
		StringBuffer sql = new StringBuffer();
		try {
			if (filter != null && !filter.isEmpty() && filter.contains(":") && filter.contains(",")) {
				String params[] = filter.split(",");
				for (String param : params) {
					try {
						String nameValue[] = param.split(":");
						if (nameValue.length == 2) {
							String name = nameValue[0];
							String value = nameValue[1];
							if (!validateParamValues(name, value)) {
								continue;
							}
							if (name.equalsIgnoreCase("qsntext")) {
								sql.append(" AND qsntext like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("opa")) {
								sql.append(" AND ansopta like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("opb")) {
								sql.append(" AND ansoptb like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("opc")) {
								sql.append(" AND ansoptc like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("opd")) {
								sql.append(" AND ansoptd like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("corans")) {
								sql.append("AND corans like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("qsnorient")) {
								sql.append("AND qsnorient like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("crBy")) {
								sql.append(" AND creby like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("credateStr")) {
								getDatePartQuery(sql, "credate", value);
							} else if (name.equalsIgnoreCase("upBy")) {
								sql.append(" AND updby like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("upddateStr")) {
								getDatePartQuery(sql, "upddate", value);
							}
						}
					} catch (Exception e) {

					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sql.toString();
	}

	/**
	 * Gets the all question bank questions for contest with filter query.
	 *
	 * @param filter the filter
	 * @return the all question bank questions for contest with filter query
	 * @throws Exception the exception
	 */
	public static String getAllQuestionBankQuestionsForContestWithFilterQuery(String filter) throws Exception {
		StringBuffer sql = new StringBuffer();
		try {
			if (filter != null && !filter.isEmpty() && filter.contains(":") && filter.contains(",")) {
				String params[] = filter.split(",");
				for (String param : params) {
					try {
						String nameValue[] = param.split(":");
						if (nameValue.length == 2) {
							String name = nameValue[0];
							String value = nameValue[1];
							if (!validateParamValues(name, value)) {
								continue;
							}
							if (name.equalsIgnoreCase("qtx")) {
								sql.append(" AND qsntext like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("opa")) {
								sql.append(" AND ansopta like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("opb")) {
								sql.append(" AND ansoptb like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("opc")) {
								sql.append(" AND ansoptc like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("opd")) {
								sql.append(" AND ansoptd like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("cans")) {
								sql.append("AND corans like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("anstype")) {
								sql.append("AND anstype like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("qOrientn")) {
								sql.append("AND qsnorient like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("ctyp")) {
								sql.append("AND cattype like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("sctyp")) {
								sql.append("AND subcattype like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("creBy")) {
								sql.append(" AND creby like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("creDate")) {
								getDatePartQuery(sql, "credate", value);
							} else if (name.equalsIgnoreCase("updBy")) {
								sql.append(" AND updby like '%" + value + "%' ");
							} else if (name.equalsIgnoreCase("updDt")) {
								getDatePartQuery(sql, "upddate", value);
							}
						}
					} catch (Exception e) {

					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sql.toString();
	}

	/**
	 * Gets the date part query.
	 *
	 * @param sql        the sql
	 * @param columnName the column name
	 * @param value      the value
	 * @return the date part query
	 */
	private static void getDatePartQuery(StringBuffer sql, String columnName, String value) {
		try {
			if (DateFormatUtil.extractDateElement(value, "DAY") > 0) {
				sql.append(" AND DATEPART(day, " + columnName + ") = " + DateFormatUtil.extractDateElement(value, "DAY")
						+ " ");
			}
			if (DateFormatUtil.extractDateElement(value, "MONTH") > 0) {
				sql.append(" AND DATEPART(month, " + columnName + ") = "
						+ DateFormatUtil.extractDateElement(value, "MONTH") + " ");
			}
			if (DateFormatUtil.extractDateElement(value, "YEAR") > 0) {
				sql.append(" AND DATEPART(year, " + columnName + ") = "
						+ DateFormatUtil.extractDateElement(value, "YEAR") + " ");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Validate param values.
	 *
	 * @param name  the name
	 * @param value the value
	 * @return true, if successful
	 */
	private static boolean validateParamValues(String name, String value) {
		if (name == null || name.isEmpty() || name.equalsIgnoreCase("undefined") || value == null || value.isEmpty()
				|| value.equalsIgnoreCase("undefined")) {
			return false;
		}
		return true;
	}
}
